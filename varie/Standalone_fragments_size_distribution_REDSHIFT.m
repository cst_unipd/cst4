% Standalone for fragments size distribution

clear all
close all
clc

%% Load Data CST

caso='d';

switch caso
%     case 'a'  % fl_min 0.001
%         name_file_result_REDSHIFT_baseline='E:\AO_8507Catastrophic\CST2_ESTEC\CST3_da_64GB_17122018_REDSHIFT_baseline2\Results\RedSHIFT_Satellite_Baseline_link_corretti_t0_01_flmin0_001\data\step_1473.mat';
%         name_file_result_REDSHIFT_weak_struct='';
%         name_file_result_REDSHIFT_c_mat='E:\AO_8507Catastrophic\CST2_ESTEC\CST3_da_64GB_17122018_REDSHIFT_c_mat2\Results\RedSHIFT_Satellite_c_mat_corretto_t0_01\data\step_2273.mat';
%         title_fig='(F_L_{min}=1e^{-3},r_B=2e^{-3},r_F=1e^{-3})';
%         
%     case 'b' % fl_min 0.01
%         name_file_result_REDSHIFT_baseline='E:\AO_8507Catastrophic\CST2_ESTEC\CST3_da_64GB_17122018_REDSHIFT_baseline2\Results\RedSHIFT_Satellite_Baseline_flmin0_01\data\step_2128.mat';
%         name_file_result_REDSHIFT_weak_struct='E:\AO_8507Catastrophic\CST2_ESTEC\CST3_da_64GB_17122018_REDSHIFT_weak_struct\Results\RedSHIFT_Satellite_Weak_struct_flmin0_01_step0_01\data\step_2771.mat';
%         name_file_result_REDSHIFT_c_mat='E:\AO_8507Catastrophic\CST2_ESTEC\CST3_da_64GB_17122018_REDSHIFT_c_mat2\Results\RedSHIFT_Satellite_c_mat_20190125T114852\data\step_3558.mat';
%         title_fig='(F_L_{min}=1e^{-2},r_B=2e^{-3},r_F=1e^{-3})';
%         
%    	case 'c' % fl_min 0.01 & q_loss_m
%         name_file_result_REDSHIFT_baseline='E:\AO_8507Catastrophic\CST2_ESTEC\CST3_da_64GB_17122018_REDSHIFT_baseline2\Results\RedSHIFT_Satellite_Baseline_20190128T095432\data\step_1729.mat';
%         name_file_result_REDSHIFT_weak_struct='E:\AO_8507Catastrophic\CST2_ESTEC\CST3_REDSHIFT_weak_struct\Results\RedSHIFT_Satellite_Weak_struct_20190128T130445\data\step_2010.mat';
%         name_file_result_REDSHIFT_c_mat='E:\AO_8507Catastrophic\CST2_ESTEC\CST3_da_64GB_17122018_REDSHIFT_c_mat2\Results\RedSHIFT_Satellite_c_mat_20190128T095513\data\step_3558.mat';
%         title_fig='(F_L_{min}=1e^{-2},r_B=2e^{-3},r_F=1e^{-3})';
        
    case 'd' % fl_min 0.01 & q_loss_m corretti
        name_file_result_REDSHIFT_baseline='E:\AO_8507Catastrophic\CST2_ESTEC\CST3_da_64GB_17122018_REDSHIFT_baseline2\Results\RedSHIFT_Satellite_Baseline_20190128T173353\data\step_1456.mat';
        name_file_result_REDSHIFT_weak_struct = 'E:\AO_8507Catastrophic\CST2_ESTEC\CST3_REDSHIFT_weak_struct\Results\RedSHIFT_Satellite_Weak_struct_20190128T173323\data\step_1937.mat';
        name_file_result_REDSHIFT_c_mat='E:\AO_8507Catastrophic\CST2_ESTEC\CST3_da_64GB_17122018_REDSHIFT_c_mat2\Results\RedSHIFT_Satellite_c_mat_20190128T173232\data\step_1747.mat';
        name_file_result_REDSHIFT_c_mat_over_5='E:\AO_8507Catastrophic\CST2_ESTEC\CST3_REDSHIFT_c_mat_over5\Results\RedSHIFT_Satellite_c_mat5_20190128T173756\data\step_2678.mat';
        name_file_result_REDSHIFT_k_mat='E:\AO_8507Catastrophic\CST2_ESTEC\CST3_REDSHIFT_c_mat_over5\Results\RedSHIFT_Satellite_k_mat5_20190129T064636\data\step_1869.mat';
        name_file_result_REDSHIFT_Weak_no_links='E:\AO_8507Catastrophic\CST2_ESTEC\CST3_REDSHIFT_weak_struct\Results\RedSHIFT_Satellite_Weak_struct_no_link_20190129T083252\data\step_3354.mat';
        name_file_result_REDSHIFT_baseline_no_links='E:\AO_8507Catastrophic\CST2_ESTEC\CST3_da_64GB_17122018_REDSHIFT_baseline2\Results\RedSHIFT_Satellite_Baseline_no_links_20190130T131251\data\step_2533.mat';
        name_file_result_REDSHIFT_baseline_k_mat_over_5='E:\AO_8507Catastrophic\CST2_ESTEC\CST3_REDSHIFT_baseline2\Results\RedSHIFT_Satellite_Baseline_k_mat_over5_20190130T131808\data\step_1988.mat';
        name_file_result_REDSHIFT_baseline_k_mat='E:\AO_8507Catastrophic\CST2_ESTEC\CST3_REDSHIFT_baseline\Results\RedSHIFT_Satellite_Baseline_k_matx5_20190130T131549\data\step_1884.mat';


        title_fig='30012019';
end

%% processing REDSHIFT_baseline scenario data
load(name_file_result_REDSHIFT_baseline,'FRAGMENTS','ME','BUBBLE','PROPAGATION_THRESHOLD','link_data','tf','RunTime')
name_mat=[name_file_result_REDSHIFT_baseline(1:strfind(name_file_result_REDSHIFT_baseline,'Results')-1),filesep,'set_up',filesep,'material_list.m'];
run(name_mat)

mass_REDSHIFT=0;

for i = 1:length(ME)-1
    mass_REDSHIFT = mass_REDSHIFT + ME(i).GEOMETRY_DATA.mass0;
end

mass_imp_1 = ME(length(ME)).GEOMETRY_DATA.mass0;
v_p_1 = ME(length(ME)).DYNAMICS_INITIAL_DATA.vel0/1000;


[n_frags_bubbles_REDSHIFT_baseline,s_REDSHIFT_baseline,N_REDSHIFT_baseline,Am_CST_REDSHIFT_baseline,delta_v_CST_REDSHIFT_baseline]=postprocess_data(FRAGMENTS,ME,BUBBLE,PROPAGATION_THRESHOLD,link_data);

tf_REDSHIFT_baseline = sprintf('%25.4f',tf);
if exist('RunTime','var')
    cputime_REDSHIFT_baseline = sprintf('%20.2f',RunTime/60);
else
    cputime_REDSHIFT_baseline = '                   -';
end
final_step_REDSHIFT_baseline = sprintf('%24d',str2double(name_file_result_REDSHIFT_baseline(strfind(name_file_result_REDSHIFT_baseline,'step_')+5:end-4)));
EMR_REDSHIFT_baseline = sprintf('%5.2f',1/2*mass_imp_1*norm(v_p_1*1000)^2/(mass_imp_1+mass_REDSHIFT)/1000);
clear FRAGMENTS ME BUBBLE PROPAGATION_THRESHOLD link_data tf RunTime



%% processing REDSHIFT_weak_struct scenario data
load(name_file_result_REDSHIFT_weak_struct,'FRAGMENTS','ME','BUBBLE','PROPAGATION_THRESHOLD','link_data','tf','RunTime')

[n_frags_bubbles_REDSHIFT_weak_struct,s_REDSHIFT_weak_struct,N_REDSHIFT_weak_struct,Am_CST_REDSHIFT_weak_struct,delta_v_CST_REDSHIFT_weak_struct]=postprocess_data(FRAGMENTS,ME,BUBBLE,PROPAGATION_THRESHOLD,link_data);

tf_REDSHIFT_weak_struct = sprintf('%25.4f',tf);
if exist('RunTime','var')
    cputime_REDSHIFT_weak_struct = sprintf('%20.2f',RunTime/60);
else
    cputime_REDSHIFT_weak_struct = '                   -';
end

final_step_REDSHIFT_weak_struct = sprintf('%24d',str2double(name_file_result_REDSHIFT_weak_struct(strfind(name_file_result_REDSHIFT_weak_struct,'step_')+5:end-4)));
clear FRAGMENTS ME BUBBLE PROPAGATION_THRESHOLD link_data tf RunTime

%% processing REDSHIFT_c_mat scenario data

load(name_file_result_REDSHIFT_c_mat,'FRAGMENTS','ME','BUBBLE','PROPAGATION_THRESHOLD','link_data','tf','RunTime')

[n_frags_bubbles_REDSHIFT_c_mat,s_REDSHIFT_c_mat,N_REDSHIFT_c_mat,Am_CST_REDSHIFT_c_mat,delta_v_CST_REDSHIFT_c_mat]=postprocess_data(FRAGMENTS,ME,BUBBLE,PROPAGATION_THRESHOLD,link_data);

tf_REDSHIFT_c_mat = sprintf('%25.4f',tf);
if exist('RunTime','var')
    cputime_REDSHIFT_c_mat = sprintf('%20.2f',RunTime/60);
else
    cputime_REDSHIFT_c_mat = '                   -';
end

final_step_REDSHIFT_c_mat = sprintf('%24d',str2double(name_file_result_REDSHIFT_c_mat(strfind(name_file_result_REDSHIFT_c_mat,'step_')+5:end-4)));
clear FRAGMENTS BUBBLE PROPAGATION_THRESHOLD link_data tf RunTime

%% processing REDSHIFT_Weak_no_links scenario data

load(name_file_result_REDSHIFT_Weak_no_links,'FRAGMENTS','ME','BUBBLE','PROPAGATION_THRESHOLD','link_data','tf','RunTime')

[n_frags_bubbles_REDSHIFT_Weak_no_links,s_REDSHIFT_Weak_no_links,N_REDSHIFT_Weak_no_links,Am_CST_REDSHIFT_Weak_no_links,delta_v_CST_REDSHIFT_Weak_no_links]=postprocess_data(FRAGMENTS,ME,BUBBLE,PROPAGATION_THRESHOLD,link_data);

tf_REDSHIFT_Weak_no_links = sprintf('%25.4f',tf);
if exist('RunTime','var')
    cputime_REDSHIFT_Weak_no_links = sprintf('%20.2f',RunTime/60);
else
    cputime_REDSHIFT_Weak_no_links = '                   -';
end

final_step_REDSHIFT_Weak_no_links = sprintf('%24d',str2double(name_file_result_REDSHIFT_Weak_no_links(strfind(name_file_result_REDSHIFT_Weak_no_links,'step_')+5:end-4)));
clear FRAGMENTS BUBBLE PROPAGATION_THRESHOLD link_data tf RunTime

%% processing REDSHIFT_baseline_no_links scenario data

load(name_file_result_REDSHIFT_baseline_no_links,'FRAGMENTS','ME','BUBBLE','PROPAGATION_THRESHOLD','link_data','tf','RunTime')

[n_frags_bubbles_REDSHIFT_baseline_no_links,s_REDSHIFT_baseline_no_links,N_REDSHIFT_baseline_no_links,Am_CST_REDSHIFT_baseline_no_links,delta_v_CST_REDSHIFT_baseline_no_links]=postprocess_data(FRAGMENTS,ME,BUBBLE,PROPAGATION_THRESHOLD,link_data);

tf_REDSHIFT_baseline_no_links = sprintf('%25.4f',tf);
if exist('RunTime','var')
    cputime_REDSHIFT_baseline_no_links = sprintf('%20.2f',RunTime/60);
else
    cputime_REDSHIFT_baseline_no_links = '                   -';
end

final_step_REDSHIFT_baseline_no_links = sprintf('%24d',str2double(name_file_result_REDSHIFT_baseline_no_links(strfind(name_file_result_REDSHIFT_baseline_no_links,'step_')+5:end-4)));
clear FRAGMENTS BUBBLE PROPAGATION_THRESHOLD link_data tf RunTime

%% processing REDSHIFT_k_mat scenario data

load(name_file_result_REDSHIFT_k_mat,'FRAGMENTS','ME','BUBBLE','PROPAGATION_THRESHOLD','link_data','tf','RunTime')

[n_frags_bubbles_REDSHIFT_k_mat,s_REDSHIFT_k_mat,N_REDSHIFT_k_mat,Am_CST_REDSHIFT_k_mat,delta_v_CST_REDSHIFT_k_mat]=postprocess_data(FRAGMENTS,ME,BUBBLE,PROPAGATION_THRESHOLD,link_data);

tf_REDSHIFT_k_mat = sprintf('%25.4f',tf);
if exist('RunTime','var')
    cputime_REDSHIFT_k_mat = sprintf('%20.2f',RunTime/60);
else
    cputime_REDSHIFT_k_mat = '                   -';
end

final_step_REDSHIFT_k_mat = sprintf('%24d',str2double(name_file_result_REDSHIFT_k_mat(strfind(name_file_result_REDSHIFT_k_mat,'step_')+5:end-4)));
clear FRAGMENTS BUBBLE PROPAGATION_THRESHOLD link_data tf RunTime

%% processing REDSHIFT_c_mat scenario data

load(name_file_result_REDSHIFT_c_mat_over_5,'FRAGMENTS','ME','BUBBLE','PROPAGATION_THRESHOLD','link_data','tf','RunTime')

[n_frags_bubbles_REDSHIFT_c_mat_over_5,s_REDSHIFT_c_mat_over_5,N_REDSHIFT_c_mat_over_5,Am_CST_REDSHIFT_c_mat_over_5,delta_v_CST_REDSHIFT_c_mat_over_5]=postprocess_data(FRAGMENTS,ME,BUBBLE,PROPAGATION_THRESHOLD,link_data);

tf_REDSHIFT_c_mat_over_5 = sprintf('%25.4f',tf);
if exist('RunTime','var')
    cputime_REDSHIFT_c_mat_over_5 = sprintf('%20.2f',RunTime/60);
else
    cputime_REDSHIFT_c_mat_over_5 = '                   -';
end

final_step_REDSHIFT_c_mat_over_5 = sprintf('%24d',str2double(name_file_result_REDSHIFT_c_mat_over_5(strfind(name_file_result_REDSHIFT_c_mat_over_5,'step_')+5:end-4)));
clear FRAGMENTS BUBBLE PROPAGATION_THRESHOLD link_data tf RunTime

%% processing REDSHIFT_baseline_k_mat_over_5 scenario data

load(name_file_result_REDSHIFT_baseline_k_mat_over_5,'FRAGMENTS','ME','BUBBLE','PROPAGATION_THRESHOLD','link_data','tf','RunTime')

[n_frags_bubbles_REDSHIFT_baseline_k_mat_over_5,s_REDSHIFT_baseline_k_mat_over_5,N_REDSHIFT_baseline_k_mat_over_5,Am_CST_REDSHIFT_baseline_k_mat_over_5,delta_v_CST_REDSHIFT_baseline_k_mat_over_5]=postprocess_data(FRAGMENTS,ME,BUBBLE,PROPAGATION_THRESHOLD,link_data);

tf_REDSHIFT_baseline_k_mat_over_5 = sprintf('%25.4f',tf);
if exist('RunTime','var')
    cputime_REDSHIFT_baseline_k_mat_over_5 = sprintf('%20.2f',RunTime/60);
else
    cputime_REDSHIFT_baseline_k_mat_over_5 = '                   -';
end

final_step_REDSHIFT_baseline_k_mat_over_5 = sprintf('%24d',str2double(name_file_result_REDSHIFT_baseline_k_mat_over_5(strfind(name_file_result_REDSHIFT_baseline_k_mat_over_5,'step_')+5:end-4)));
clear FRAGMENTS BUBBLE PROPAGATION_THRESHOLD link_data tf RunTime

%% processing REDSHIFT_baseline_k_mat scenario data

load(name_file_result_REDSHIFT_baseline_k_mat,'FRAGMENTS','ME','BUBBLE','PROPAGATION_THRESHOLD','link_data','tf','RunTime')

[n_frags_bubbles_REDSHIFT_baseline_k_mat,s_REDSHIFT_baseline_k_mat,N_REDSHIFT_baseline_k_mat,Am_CST_REDSHIFT_baseline_k_mat,delta_v_CST_REDSHIFT_baseline_k_mat]=postprocess_data(FRAGMENTS,ME,BUBBLE,PROPAGATION_THRESHOLD,link_data);

tf_REDSHIFT_baseline_k_mat = sprintf('%25.4f',tf);
if exist('RunTime','var')
    cputime_REDSHIFT_baseline_k_mat = sprintf('%20.2f',RunTime/60);
else
    cputime_REDSHIFT_baseline_k_mat = '                   -';
end

final_step_REDSHIFT_baseline_k_mat = sprintf('%24d',str2double(name_file_result_REDSHIFT_baseline_k_mat(strfind(name_file_result_REDSHIFT_baseline_k_mat,'step_')+5:end-4)));
clear FRAGMENTS BUBBLE PROPAGATION_THRESHOLD link_data tf RunTime

%% plot finale Lc
%{d
colorOrder =  [ ...
        0            0            0       ;...% 6 BLACK
        0            0            0.7       ;...% 1 BLUE
        0            0.7            0       ;...% 3 GREEN (pale)
        0.7            0            0.7       ;...% 5 MAGENTA (pale)
        0            0.8            0.8       ;...% 4 CYAN
        0.6          0.5          0.4     ;  % 15 BROWN (dark)
        1            1            0       ;...% 7 YELLOW (pale)
        0            0.75         0.75    ;...% 8 TURQUOISE
        0            0.5          0       ;...% 9 GREEN (dark)
        0.75         0.75         0       ;...% 10 YELLOW (dark)
        1            0.50         0.25    ;...% 11 ORANGE
        0.75         0            0.75    ;...% 12 MAGENTA (dark)
        0.7          0.7          0.7     ;...% 13 GREY
        0.8          0.7          0.6     ;...% 14 BROWN (pale)
        0.4          0.7          0.6     ;...
        0.4          0          0.6     ;...
        0.4          0.6          0     ;...
        0.6          0.5          0.4 ];  % 18 BROWN (dark)
    
    %}
    
%% Plot Lc REDSHIFT
min_LC=min([s_REDSHIFT_baseline,s_REDSHIFT_weak_struct,s_REDSHIFT_c_mat,...
    s_REDSHIFT_c_mat_over_5,s_REDSHIFT_k_mat,s_REDSHIFT_Weak_no_links,s_REDSHIFT_baseline_no_links,...
    s_REDSHIFT_baseline_k_mat_over_5,s_REDSHIFT_baseline_k_mat]);
max_LC=max([s_REDSHIFT_baseline,s_REDSHIFT_weak_struct,s_REDSHIFT_c_mat,...
    s_REDSHIFT_c_mat_over_5,s_REDSHIFT_k_mat,s_REDSHIFT_Weak_no_links,s_REDSHIFT_baseline_no_links,...
    s_REDSHIFT_baseline_k_mat_over_5,s_REDSHIFT_baseline_k_mat]);
Lc_NASA=[min_LC:(max_LC-min_LC)/100:max_LC,1];
m_tot=0;
for ii = 1:length(ME)
    m_tot=m_tot+ME(ii).GEOMETRY_DATA.mass0;
end
NASA_SBM_cat = 0.1*(m_tot)^0.75*Lc_NASA.^(-1.71);
NASA_SBM_sub = 0.1*(mass_imp_1*norm(v_p_1))^0.75*Lc_NASA.^(-1.71);

m_tot=0;
for ii = 1:length(ME)
    m_tot=m_tot+ME(ii).GEOMETRY_DATA.mass0;
end
NASA_SBM_cat = 0.1*(m_tot)^0.75*Lc_NASA.^(-1.71);

xx_lim = [3.e-3,0.35]; % [min_LC, max_LC]; %
yy_lim = [1,9000]; % [1,max(NASA_SBM_cat)]; %

figure('units','normalized','outerposition',[0 0 1 1],'Visible', 'on')
loglog(s_REDSHIFT_baseline,[N_REDSHIFT_baseline(1)+n_frags_bubbles_REDSHIFT_baseline,N_REDSHIFT_baseline(2:end)],'MarkerEdgeColor',colorOrder(11,:),'MarkerFaceColor',colorOrder(11,:),'Marker','o','LineStyle','none');
grid on
hold on
loglog(s_REDSHIFT_weak_struct,[N_REDSHIFT_weak_struct(1)+n_frags_bubbles_REDSHIFT_weak_struct,N_REDSHIFT_weak_struct(2:end)],'MarkerEdgeColor',colorOrder(5,:),'MarkerFaceColor',colorOrder(5,:),'Marker','^','LineStyle','none');
hold on
loglog(s_REDSHIFT_c_mat,[N_REDSHIFT_c_mat(1)+n_frags_bubbles_REDSHIFT_c_mat,N_REDSHIFT_c_mat(2:end)],'Color',colorOrder(7,:),'Marker','+','LineStyle','none','LineWidth',2)
hold on
loglog(s_REDSHIFT_c_mat_over_5,[N_REDSHIFT_c_mat_over_5(1)+n_frags_bubbles_REDSHIFT_c_mat_over_5,N_REDSHIFT_c_mat_over_5(2:end)],'Color',colorOrder(3,:),'Marker','s','LineStyle','none','LineWidth',2)
hold on
loglog(s_REDSHIFT_k_mat,[N_REDSHIFT_k_mat(1)+n_frags_bubbles_REDSHIFT_k_mat,N_REDSHIFT_k_mat(2:end)],'Color',colorOrder(4,:),'Marker','d','LineStyle','none','LineWidth',2)
hold on
loglog(s_REDSHIFT_Weak_no_links,[N_REDSHIFT_Weak_no_links(1)+n_frags_bubbles_REDSHIFT_Weak_no_links,N_REDSHIFT_Weak_no_links(2:end)],'Color',colorOrder(9,:),'Marker','*','LineStyle','none','LineWidth',2)
hold on
loglog(s_REDSHIFT_baseline_no_links,[N_REDSHIFT_baseline_no_links(1)+n_frags_bubbles_REDSHIFT_baseline_no_links,N_REDSHIFT_baseline_no_links(2:end)],'Color',colorOrder(2,:),'Marker','*','LineStyle','none','LineWidth',2)
hold on
loglog(s_REDSHIFT_baseline_k_mat,[N_REDSHIFT_baseline_k_mat(1)+n_frags_bubbles_REDSHIFT_baseline_k_mat,N_REDSHIFT_baseline_k_mat(2:end)],'Color',colorOrder(8,:),'Marker','+','LineStyle','none','LineWidth',2)
hold on
loglog(s_REDSHIFT_baseline_k_mat_over_5,[N_REDSHIFT_baseline_k_mat_over_5(1)+n_frags_bubbles_REDSHIFT_baseline_k_mat_over_5,N_REDSHIFT_baseline_k_mat_over_5(2:end)],'Color',colorOrder(1,:),'Marker','s','LineStyle','none','LineWidth',2)
hold on
set(gca,'FontName','Helvetica','FontSize',18,'FontWeight','bold')
loglog(Lc_NASA,NASA_SBM_sub,'--r','LineWidth',2)
loglog(Lc_NASA,NASA_SBM_cat,'-k','LineWidth',2)

lgd=legend('REDSHIFT baseline ','REDSHIFT weak struct ','REDSHIFT weak c_{mat}x5','REDSHIFT weak c_{mat}:5','REDSHIFT weak k_{mat}x5','REDSHIFT weak no links','REDSHIFT baseline k_{mat}x5','REDSHIFT baseline k_{mat}:5','REDSHIFT baseline no links','NASA SBM - SubCat','NASA - SBM Cat'); 
lgd.FontName='Helvetica';
lgd.FontSize=18;
lgd.FontWeight='bold';
xlabel('L_c [m]','FontName','Helvetica','FontSize',18,'FontWeight','bold')
ylabel('Cumulative number CN','FontName','Helvetica','FontSize',18,'FontWeight','bold')
title('CN(L_c) curves (F_L_{min}=1.e^{-2},r_B=2e^{-3},r_F=1e^{-3})','FontName','Helvetica','FontSize',18,'FontWeight','bold')

hold on
grid on
set(gca,'XLim',xx_lim)
set(gca,'YLim',yy_lim)
out_fig = ['RDESHIFT_',title_fig];
saveas(gcf,out_fig,'fig')
saveas(gcf,out_fig,'png')
%%

figure('units','normalized','outerposition',[0 0 1 1],'Visible', 'on')
loglog(s_REDSHIFT_baseline,[N_REDSHIFT_baseline(1)+n_frags_bubbles_REDSHIFT_baseline,N_REDSHIFT_baseline(2:end)],'MarkerEdgeColor',colorOrder(11,:),'MarkerFaceColor',colorOrder(11,:),'Marker','o','LineStyle','none');
grid on
hold on
loglog(s_REDSHIFT_baseline_no_links,[N_REDSHIFT_baseline_no_links(1)+n_frags_bubbles_REDSHIFT_baseline_no_links,N_REDSHIFT_baseline_no_links(2:end)],'Color',colorOrder(5,:),'Marker','*','LineStyle','none','LineWidth',2)
hold on
loglog(s_REDSHIFT_baseline_k_mat,[N_REDSHIFT_baseline_k_mat(1)+n_frags_bubbles_REDSHIFT_baseline_k_mat,N_REDSHIFT_baseline_k_mat(2:end)],'Color',colorOrder(7,:),'Marker','+','LineStyle','none','LineWidth',2)
hold on
loglog(s_REDSHIFT_baseline_k_mat_over_5,[N_REDSHIFT_baseline_k_mat_over_5(1)+n_frags_bubbles_REDSHIFT_baseline_k_mat_over_5,N_REDSHIFT_baseline_k_mat_over_5(2:end)],'Color',colorOrder(3,:),'Marker','s','LineStyle','none','LineWidth',2)
hold on
set(gca,'FontName','Helvetica','FontSize',18,'FontWeight','bold')
loglog(Lc_NASA,NASA_SBM_sub,'--r','LineWidth',2)
loglog(Lc_NASA,NASA_SBM_cat,'-k','LineWidth',2)

lgd=legend('REDSHIFT baseline ','REDSHIFT baseline k_{mat}x5','REDSHIFT baseline k_{mat}:5','REDSHIFT baseline no links','NASA SBM - SubCat','NASA - SBM Cat'); 
lgd.FontName='Helvetica';
lgd.FontSize=18;
lgd.FontWeight='bold';
xlabel('L_c [m]','FontName','Helvetica','FontSize',18,'FontWeight','bold')
ylabel('Cumulative number CN','FontName','Helvetica','FontSize',18,'FontWeight','bold')
title('CN(L_c) curves (F_L_{min}=1.e^{-2},r_B=2e^{-3},r_F=1e^{-3})','FontName','Helvetica','FontSize',18,'FontWeight','bold')

hold on
grid on
set(gca,'XLim',xx_lim)
set(gca,'YLim',yy_lim)

out_fig = ['RDESHIFT_baseline_',title_fig];
saveas(gcf,out_fig,'fig')
saveas(gcf,out_fig,'png')


%{
%% plot finale REDSHIFT A su M

xx_lim_Am = [-2,0.3];
yy_lim_Am = [0,0.09];
clear DAmSOC DAmSOC_matlab DAmSOC_NASA
lambda_c_min=log10(min([s_REDSHIFT_baseline s_REDSHIFT_c_mat s_REDSHIFT_weak_struct]));
lambda_c_max=log10(max([s_REDSHIFT_baseline s_REDSHIFT_c_mat s_REDSHIFT_weak_struct]));
lambda_c=lambda_c_min:(lambda_c_max-lambda_c_min)/20:lambda_c_max;
Am_REDSHIFT =[Am_CST_REDSHIFT_baseline Am_CST_REDSHIFT_c_mat Am_CST_REDSHIFT_weak_struct];
csi_min=log10(min(Am_REDSHIFT(Am_REDSHIFT>0)));
csi_max=log10(max(Am_REDSHIFT));
n=length(lambda_c);
csi_NASA=csi_min:(csi_max-csi_min)/(n-1):csi_max;

for i=1:1:n
        if  lambda_c(i)<=-3.5
            muSOC=-0.3;
            sigmaSOC=0.2;
            DAmSOC(i,:)=(1/(sigmaSOC*(2*pi)^0.5))*exp(-((csi_NASA-muSOC).^2)/(2*sigmaSOC^2));
            pd = makedist('Normal','mu',muSOC,'sigma',sigmaSOC);
%             DAmSOC_matlab(i,:) = pdf(pd,csi_NASA);
        elseif lambda_c(i)>-3.5 && lambda_c(i)<=-1.75
            muSOC=-0.3;
            sigmaSOC=0.2+0.1333*(lambda_c(i)+3.5);
            DAmSOC(i,:)=(1/(sigmaSOC*(2*pi)^0.5))*exp(-((csi_NASA-muSOC).^2)/(2*sigmaSOC^2));
            pd = makedist('Normal','mu',muSOC,'sigma',sigmaSOC);
%             DAmSOC_matlab(i,:) = pdf(pd,csi_NASA);
        elseif lambda_c(i)>-1.75 && lambda_c(i)<-1.25
            muSOC=-0.3-1.4*(lambda_c(i)+1.75);
            sigmaSOC=0.2+0.1333*(lambda_c(i)+3.5);
            DAmSOC(i,:)=(1/(sigmaSOC*(2*pi)^0.5))*exp(-((csi_NASA-muSOC).^2)/(2*sigmaSOC^2));
            pd = makedist('Normal','mu',muSOC,'sigma',sigmaSOC);
%             DAmSOC_matlab(i,:) = pdf(pd,csi_NASA);
        elseif lambda_c(i)>=-1.25
            muSOC=-1.0;
            sigmaSOC=0.2+0.1333*(lambda_c(i)+3.5);
            DAmSOC(i,:)=(1/(sigmaSOC*(2*pi)^0.5))*exp(-((csi_NASA-muSOC).^2)/(2*sigmaSOC^2));
            pd = makedist('Normal','mu',muSOC,'sigma',sigmaSOC);
%             DAmSOC_matlab(i,:) = pdf(pd,csi_NASA);
        end
end

for k=1:1:size(DAmSOC,2)
    DAmSOC_NASA(k)=sum(DAmSOC(:,k));
%     DAmSOC_matlab_NASA(k) = sum(DAmSOC_matlab(:,k));
end

figure('units','normalized','outerposition',[0 0 1 1],'Visible', 'on')
grid on
histogram(log10(Am_CST_REDSHIFT_baseline),'Normalization','probability','BinWidth',0.035)
hold on
histogram(log10(Am_CST_REDSHIFT_weak_struct),'Normalization','probability','BinWidth',0.035)
hold on
histogram(log10(Am_CST_REDSHIFT_c_mat),'Normalization','probability','BinWidth',0.035)
hold on
plot(csi_NASA,DAmSOC_NASA./sum(DAmSOC_NASA),'-b','LineWidth',2)
hold on
title(['Comparison among PDFs ',title_fig],'FontName','Helvetica','FontSize',18,'FontWeight','bold')
set(gca,'FontName','Helvetica','FontSize',18,'FontWeight','bold')
xlabel('log_{10}(A/m) [m^{2}/kg]','FontName','Helvetica','FontSize',18,'FontWeight','bold')
ylabel('Relative number N(A/m)','FontName','Helvetica','FontSize',18,'FontWeight','bold')
lgd=legend('REDSHIFT baseline','REDSHIFT weak struct','REDSHIFT c_{mat}','NASA - SBM','Location','northwest');
lgd.FontName='Helvetica';
lgd.FontSize=18;
lgd.FontWeight='bold';
set(gca,'XLim',xx_lim_Am)
set(gca,'YLim',yy_lim_Am)
out_fig = ['A_su_m_CST_REDSHIFT_data_new_Qloss_m_all2',title_fig];
saveas(gcf,out_fig,'fig')
saveas(gcf,out_fig,'png')
%}
%% plot finale of Delta_v
%{
clear DdvCOLL_NASA DdvCOLL_NASA_1

muEXP=0.9*csi_NASA+2.9;
sigmaEXP=0.4;
delta_v_lim=[0 3];
delta_v_NASA=delta_v_lim(1):abs(delta_v_lim(1)-delta_v_lim(2))/(n-1):delta_v_lim(2);

for i=1:1:n
    DdvCOLL_NASA(:,i)=(1/(sigmaEXP*(2*pi)^0.5))*exp(-((delta_v_NASA-muEXP(i)).^2)/(2*sigmaEXP^2));
end

for k=1:1:size(DdvCOLL_NASA,1)
    DdvCOLL_NASA_1(k)=sum(DdvCOLL_NASA(:,k));
end

figure
histogram(delta_v_CST_REDSHIFT_baseline,'Normalization','probability','BinWidth',1)
grid on
hold on
histogram(delta_v_CST_REDSHIFT_weak_struct,'Normalization','probability','BinWidth',1)
hold on
histogram(delta_v_CST_REDSHIFT_c_mat,'Normalization','probability','BinWidth',1)
hold on
% plot(delta_v_NASA,DdvCOLL_NASA_1./sum(DdvCOLL_NASA_1),'b','LineWidth',2)
title(['Comparison among PDFs ',title_fig])
xlabel('\DeltaV [m/s] ')
ylabel('Relative Number of Fragments per \DeltaV bin')
legend('CST REDSHIFT baseline','CST REDSHIFT weak struct','CST REDSHIFT c_mat','NASA SBM')
xlim([0 60000])
%}

% %% plot cpu time and simulation time
% if exist(['Data_Simulation_REDSHIFT_',title_fig,'.txt'],'file')
%     delete(['Data_Simulation_REDSHIFT_',title_fig,'.txt'])
% end
% diary(['Data_Simulation_REDSHIFT_',title_fig,'.txt'])
% diary on
% disp('--------------------------------------------------------------------------------------------')
% disp('SIMULATION                   CPUTIME (min)       SIMULATION TIME (s)             FINAL STEP')
% disp(['REDSHIFT baseline     ',cputime_REDSHIFT_baseline,tf_REDSHIFT_baseline,final_step_REDSHIFT_baseline])
% disp(['REDSHIFT weak struct  ',cputime_REDSHIFT_weak_struct,tf_REDSHIFT_weak_struct,final_step_REDSHIFT_weak_struct])
% disp(['REDSHIFT c_mat        ',cputime_REDSHIFT_c_mat,tf_REDSHIFT_c_mat,final_step_REDSHIFT_c_mat])
% disp('--------------------------------------------------------------------------------------------')
% diary off