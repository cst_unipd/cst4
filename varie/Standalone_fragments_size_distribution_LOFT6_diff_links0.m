% Standalone for fragments size distribution
clear all
close all

name_file_result_baseline='E:\AO_8507Catastrophic\CST2_ESTEC\CST3\Results\LOFT6_20181108T175437\data\step_2000.mat';
name_file_result_continuum='E:\AO_8507Catastrophic\CST2_ESTEC\CST3_LOFT6_all_continuum\Results\LOFT6_20181113T140812\data\step_2000.mat';

%% processing mixed link scenario data

load(name_file_result_baseline,'FRAGMENTS','ME')

mass_imp=0;
for i = 52:length(ME)
    mass_imp = mass_imp + ME(i).GEOMETRY_DATA.mass0;
    q_imp = ME(i).GEOMETRY_DATA.mass0*ME(i).DYNAMICS_INITIAL_DATA.vel0;
end
v_p = 1/mass_imp*q_imp/1000;
clear ME

FRAGMENTS_baseline_link=FRAGMENTS;
clear FRAGMENTS
n_size=length(FRAGMENTS_baseline_link);
for i=n_size:-1:1
    if FRAGMENTS_baseline_link(i).GEOMETRY_DATA.mass==0
        FRAGMENTS_baseline_link(i)=[];
    end
end
n_size=length(FRAGMENTS_baseline_link);

for i = 2:n_size
    Lc_CST_baseline(i-1)=mean(FRAGMENTS_baseline_link(i).GEOMETRY_DATA.dimensions);
    Am_CST_baseline(i-1)=FRAGMENTS_baseline_link(i).GEOMETRY_DATA.A_M_ratio;
    vel_CST_baseline(i-1)=norm(FRAGMENTS_baseline_link(i).DYNAMICS_DATA.vel);
    mass_CST_baseline(i-1)=FRAGMENTS_baseline_link(i).GEOMETRY_DATA.mass;
end

s_baseline=sort(Lc_CST_baseline);

if n_size==1 %patch to avoid only one fragment case
    n_size=n_size+1;
end

N_baseline=n_size-1:-1:1;

% data for plot delta V
q_cm=[0 0 0]';
for i=2:n_size
    q_cm=q_cm+mass_CST_baseline(i-1)*vel_CST_baseline(:,i-1);
end
v_cm = q_cm/sum(mass_CST_baseline);
for i=2:n_size
    delta_v_CST_baseline(i-1)=norm(v_cm-vel_CST_baseline(:,i-1));
end

clear v_cm q_cm n_size

%% processing continuum link scenario data

load(name_file_result_continuum,'FRAGMENTS')

FRAGMENTS_continuum_link=FRAGMENTS;
clear FRAGMENTS

n_size=length(FRAGMENTS_continuum_link);
for i=n_size:-1:1
    if FRAGMENTS_continuum_link(i).GEOMETRY_DATA.mass==0
        FRAGMENTS_continuum_link(i)=[];
    end
end
n_size=length(FRAGMENTS_continuum_link);

for i = 2:n_size
    Lc_CST_continuum(i-1)=mean(FRAGMENTS_continuum_link(i).GEOMETRY_DATA.dimensions);
    Am_CST_continuum(i-1)=FRAGMENTS_continuum_link(i).GEOMETRY_DATA.A_M_ratio;
    vel_CST_continuum(i-1)=norm(FRAGMENTS_continuum_link(i).DYNAMICS_DATA.vel);
    mass_CST_continuum(i-1)=FRAGMENTS_continuum_link(i).GEOMETRY_DATA.mass;
end

s_continuum=sort(Lc_CST_continuum);

if n_size==1 %patch to avoid only one fragment case
    n_size=n_size+1;
end

N_continuum=n_size-1:-1:1;

% data for plot delta V
q_cm=[0 0 0]';
for i=2:n_size
    q_cm=q_cm+mass_CST_continuum(i-1)*vel_CST_continuum(:,i-1);
end
v_cm = q_cm/sum(mass_CST_continuum);
for i=2:n_size
    delta_v_CST_continuum(i-1)=norm(v_cm-vel_CST_continuum(:,i-1));
end

clear v_cm q_cm n_size

%% processing continuum link scenario data

% load(name_file_result_bolted,'FRAGMENTS')
% 
% FRAGMENTS_bolted_link=FRAGMENTS;
% clear FRAGMENTS
% 
% n_size=length(FRAGMENTS_bolted_link);
% for i=n_size:-1:1
%     if FRAGMENTS_bolted_link(i).GEOMETRY_DATA.mass==0
%         FRAGMENTS_bolted_link(i)=[];
%     end
% end
% n_size=length(FRAGMENTS_bolted_link);
% 
% for i = 2:n_size
%     Lc_CST_bolted(i-1)=mean(FRAGMENTS_bolted_link(i).GEOMETRY_DATA.dimensions);
%     Am_CST_bolted(i-1)=FRAGMENTS_bolted_link(i).GEOMETRY_DATA.A_M_ratio;
%     vel_CST_bolted(i-1)=norm(FRAGMENTS_bolted_link(i).DYNAMICS_DATA.vel);
%     mass_CST_bolted(i-1)=FRAGMENTS_bolted_link(i).GEOMETRY_DATA.mass;
% end
% 
% % PLOT FRAGMENT DISTRIBUTIONS CST
% s_bolted=sort(Lc_CST_bolted);
% 
% if n_size==1 %patch to avoid only one fragment case (LO)
%     n_size=n_size+1;
% end
% 
% N_bolted=n_size-1:-1:1;
% 
% % data for plot delta V
% q_cm=[0 0 0]';
% for i=2:n_size
%     q_cm=q_cm+mass_CST_bolted(i-1)*vel_CST_bolted(:,i-1);
% end
% v_cm = q_cm/sum(mass_CST_bolted);
% for i=2:n_size
%     delta_v_CST_bolted(i-1)=norm(v_cm-vel_CST_bolted(:,i-1));
% end
% 
% clear v_cm q_cm n_size

%% plot finale Lc
%{s
NASA_SBM = 0.1*(mass_imp*norm(v_p))^0.75*s_baseline.^(-1.71);

h = figure('Position', [45   129   906   553],'Visible', 'on');
loglog(s_baseline,N_baseline,'+g','LineWidth',2)
grid on
hold on
loglog(s_continuum,N_continuum,'<b','LineWidth',2)
hold on
% loglog(s_bolted,N_bolted,'*k','LineWidth',2)
hold on
loglog(s_baseline,NASA_SBM,'--m','LineWidth',2)
hold on
title('Comparison CST results LOFT1')
xlabel('L_c [m]')
ylabel('Cumulative number N')
% legend('CST simulation','CST simulation (all continuum links)','CST simulation (all bolted links)','NASA - SBM')
legend('CST simulation','CST simulation (all continuum links)','NASA - SBM')

%% plot finale A su M

lambda_c=-3:0.01:0;
n=length(lambda_c);
csi_NASA=-2:0.12:2;
for i=1:1:n
        if  lambda_c(i)<=-3.5
            muSOC=-0.3;
            sigmaSOC=0.2;
            DAmSOC(i,:)=(1/(sigmaSOC*(2*pi)^0.5))*exp(-((csi_NASA-muSOC).^2)/(2*sigmaSOC^2));
            pd = makedist('Normal','mu',muSOC,'sigma',sigmaSOC);
            DAmSOC_matlab(i,:) = pdf(pd,csi_NASA);
        elseif lambda_c(i)>-3.5 && lambda_c(i)<=-1.75
            muSOC=-0.3;
            sigmaSOC=0.2+0.1333*(lambda_c(i)+3.5);
            DAmSOC(i,:)=(1/(sigmaSOC*(2*pi)^0.5))*exp(-((csi_NASA-muSOC).^2)/(2*sigmaSOC^2));
            pd = makedist('Normal','mu',muSOC,'sigma',sigmaSOC);
            DAmSOC_matlab(i,:) = pdf(pd,csi_NASA);
        elseif lambda_c(i)>-1.75 && lambda_c(i)<-1.25
            muSOC=-0.3-1.4*(lambda_c(i)+1.75);
            sigmaSOC=0.2+0.1333*(lambda_c(i)+3.5);
            DAmSOC(i,:)=(1/(sigmaSOC*(2*pi)^0.5))*exp(-((csi_NASA-muSOC).^2)/(2*sigmaSOC^2));
            pd = makedist('Normal','mu',muSOC,'sigma',sigmaSOC);
            DAmSOC_matlab(i,:) = pdf(pd,csi_NASA);
        elseif lambda_c(i)>=-1.25
            muSOC=-1.0;
            sigmaSOC=0.2+0.1333*(lambda_c(i)+3.5);
            DAmSOC(i,:)=(1/(sigmaSOC*(2*pi)^0.5))*exp(-((csi_NASA-muSOC).^2)/(2*sigmaSOC^2));
            pd = makedist('Normal','mu',muSOC,'sigma',sigmaSOC);
            DAmSOC_matlab(i,:) = pdf(pd,csi_NASA);
        end
end

for k=1:1:size(DAmSOC,2)
    DAmSOC_NASA(k)=sum(DAmSOC(:,k));
    DAmSOC_matlab_NASA(k) = sum(DAmSOC_matlab(:,k));
end

h = figure('Position', [604          45        1010         500],'Visible', 'on');
histogram(log10(Am_CST_baseline),'Normalization','probability','BinWidth',0.03) 
grid on
hold on
histogram(log10(Am_CST_continuum),'Normalization','probability','BinWidth',0.03) 
hold on
% histogram(log10(Am_CST_bolted),'Normalization','probability','BinWidth',0.03) 
hold on
plot(csi_NASA,DAmSOC_NASA./sum(DAmSOC_NASA),'b','LineWidth',2)
hold on
title('Comparison among PDFs')
set(gca,'FontSize',20,'FontWeight','bold')
xlabel('log_{10}(A/m) [m^{2}/kg]')
ylabel('CN(A/m)')
% legend('CST simulation','CST simulation (all continuum links)','CST simulation (all bolted links)','Nasa SBM','Location','northeast')
legend('CST simulation','CST simulation (all continuum links)','Nasa SBM','Location','northeast')
%}
%% plot finale of Delta_v

m=60;
csi_lim=[-10 10];
csi_NASA2=csi_lim(1):abs(csi_lim(1)-csi_lim(2))/m:csi_lim(2);
muEXP=0.9*csi_NASA2+2.9;
sigmaEXP=0.4;
delta_v_lim=[0 4000];
delta_v_NASA=delta_v_lim(1):abs(delta_v_lim(1)-delta_v_lim(2))/m:delta_v_lim(2);
n=length(csi_NASA2);

for i=1:1:n
    DdvCOLL_NASA(:,i)=(1/(sigmaEXP*(2*pi)^0.5))*exp(-((delta_v_NASA-muEXP(i)).^2)/(2*sigmaEXP^2));
end

for k=1:1:size(DdvCOLL_NASA,1)
    DdvCOLL_NASA_1(k)=sum(DdvCOLL_NASA(:,k));
end

figure
histogram(delta_v_CST_baseline,'Normalization','probability','BinWidth',1)
grid on
hold on
histogram(delta_v_CST_continuum,'Normalization','probability','BinWidth',1)
hold on
% histogram(delta_v_CST_bolted,'Normalization','probability','BinWidth',1)
hold on
% plot(delta_v_NASA,DdvCOLL_NASA_1./sum(DdvCOLL_NASA_1),'b','LineWidth',2)
title('Comparison among PDFs')
xlabel('\DeltaV [m/s] ')
ylabel('Relative Number of Fragments per \DeltaV bin')
% legend('CST simulation','CST simulation (all continuum links)','CST simulation (all bolted links)','NASA SBM')
legend('CST simulation','CST simulation (all continuum links)','NASA SBM')
% xlim([0 6000])
