for i=1:length(ME)
    v0=rquat(ME(i).DYNAMICS_INITIAL_DATA.quaternions0)'*ME(i).GEOMETRY_DATA.c_hull';
    v=repmat(ME(i).DYNAMICS_INITIAL_DATA.cm_coord0,1,size(ME(i).GEOMETRY_DATA.c_hull,1))'+v0';
    f=minConvexHull(v);
    drawMesh(v,f)
    hold on 
    pause(0.0001)
    clear v v0 f
end
for i=1:length(FRAGMENTS)
    v0=rquat(FRAGMENTS(i).DYNAMICS_INITIAL_DATA.quaternions0)'*FRAGMENTS(i).GEOMETRY_DATA.c_hull';
    v=repmat(FRAGMENTS(i).DYNAMICS_INITIAL_DATA.cm_coord0,1,size(FRAGMENTS(i).GEOMETRY_DATA.c_hull,1))'+v0';
    f=minConvexHull(v);
    drawMesh(v,f)
    hold on 
    pause(0.0001)
    clear v v0 f
end