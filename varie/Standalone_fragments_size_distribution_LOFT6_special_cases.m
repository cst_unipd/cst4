% Standalone for fragments size distribution
clear all
close all
clc

%% Load Data CST
name_file_result_LOFT6='N:\CST3\Results\LOFT6_Baseline\data\step_1281.mat';
name_file_result_LOFT6_c_mat5='N:\CST3\Results\LOFT6_Baseline_c_mat5\data\step_1670.mat';
name_file_result_LOFT6_cELOSS='N:\CST3\Results\LOFT6_Baseline_cELOSS_02\data\step_4000.mat';
% name_file_result_LOFT6_c_mat5_cELOSS=;
name_file_result_LOFT7_AF='N:\CST3\Results\LOFT7_AF\data\step_569.mat';
% name_file_result_LOFT8_AF=;
% name_file_result_LOFT8_MD=;

%% Load Data EMI
LOFT6=load('22_PDFsam_2018CSID_MSchimmerohn_SCDisrupt_crop_LOFT6_LOG.txt');

%% processing LOFT6 scenario data
load(name_file_result_LOFT6,'FRAGMENTS','ME','BUBBLE','PROPAGATION_THRESHOLD','link_data','tf','RunTime')
name_mat=[name_file_result_LOFT6(1:strfind(name_file_result_LOFT6,'Results')-1),filesep,'set_up',filesep,'material_list.m'];
run(name_mat)

mass_imp_1=0;
mass_LOFT=0;
q_imp_1 = [0 0 0]';

for i = 1:51
    mass_LOFT = mass_LOFT + ME(i).GEOMETRY_DATA.mass0;
end

for i = 52:length(ME)
    mass_imp_1 = mass_imp_1 + ME(i).GEOMETRY_DATA.mass0;
    q_imp_1 = q_imp_1 + ME(i).GEOMETRY_DATA.mass0*ME(i).DYNAMICS_INITIAL_DATA.vel0;
end
v_p_1 = 1/mass_imp_1*q_imp_1/1000;

load(name_file_result_LOFT6,'FRAGMENTS','ME','BUBBLE','PROPAGATION_THRESHOLD','link_data','tf','RunTime')

[n_frags_bubbles_LOFT6,s_LOFT6,N_LOFT6,Am_CST_LOFT6,delta_v_CST_LOFT6]=postprocess_data(FRAGMENTS,ME,BUBBLE,PROPAGATION_THRESHOLD,link_data);

tf_LOFT6 = sprintf('%25.4f',tf);
if exist('RunTime','var')
    cputime_LOFT6 = sprintf('%20.2f',RunTime/60);
else
    cputime_LOFT6 = '                   -';
end

final_step_LOFT6 = sprintf('%24d',str2double(name_file_result_LOFT6(strfind(name_file_result_LOFT6,'step_')+5:end-4)));
EMR_LOFT6 = sprintf('%5.2f',1/2*mass_imp_1*norm(v_p_1*1000)^2/(mass_imp_1+mass_LOFT)/1000);
clear FRAGMENTS ME BUBBLE PROPAGATION_THRESHOLD link_data tf RunTime

%% processing LOFT6 c_mat*5 scenario data

load(name_file_result_LOFT6_c_mat5,'FRAGMENTS','ME','BUBBLE','PROPAGATION_THRESHOLD','link_data','tf','RunTime')

[n_frags_bubbles_LOFT6_c_mat5,s_LOFT6_c_mat5,N_LOFT6_c_mat5,Am_CST_LOFT6_c_mat5,delta_v_CST_LOFT6_c_mat5]=postprocess_data(FRAGMENTS,ME,BUBBLE,PROPAGATION_THRESHOLD,link_data);

tf_LOFT6_c_mat5 = sprintf('%25.4f',tf);
if exist('RunTime','var')
    cputime_LOFT6_c_mat5 = sprintf('%20.2f',RunTime/60);
else
    cputime_LOFT6_c_mat5 = '                   -';
end

final_step_LOFT6_c_mat5 = sprintf('%24d',str2double(name_file_result_LOFT6_c_mat5(strfind(name_file_result_LOFT6_c_mat5,'step_')+5:end-4)));
clear FRAGMENTS ME BUBBLE PROPAGATION_THRESHOLD link_data tf RunTime

%% processing LOFT6 cELOSS=0.2 scenario data
%{r
load(name_file_result_LOFT6_cELOSS,'FRAGMENTS','ME','BUBBLE','PROPAGATION_THRESHOLD','link_data','tf','RunTime')

[n_frags_bubbles_LOFT6_cELOSS,s_LOFT6_cELOSS,N_LOFT6_cELOSS,Am_CST_LOFT6_cELOSS,delta_v_CST_LOFT6_cELOSS]=postprocess_data(FRAGMENTS,ME,BUBBLE,PROPAGATION_THRESHOLD,link_data);

tf_LOFT6_cELOSS = sprintf('%25.4f',tf);
if exist('RunTime','var')
    cputime_LOFT6_cELOSS = sprintf('%20.2f',RunTime/60);
else
    cputime_LOFT6_cELOSS = '                   -';
end

final_step_LOFT6_cELOSS = sprintf('%24d',str2double(name_file_result_LOFT6_cELOSS(strfind(name_file_result_LOFT6_cELOSS,'step_')+5:end-4)));
clear FRAGMENTS ME BUBBLE PROPAGATION_THRESHOLD link_data tf RunTime

%% processing LOFT6 c_mat*5 & cELOSS=0.2 scenario data
%{
load(name_file_result_LOFT6_c_mat5_cELOSS,'FRAGMENTS','ME','BUBBLE','PROPAGATION_THRESHOLD','link_data','tf','RunTime')

[n_frags_bubbles_LOFT6_c_mat5_cELOSS,s_LOFT6_c_mat5_cELOSS,N_LOFT6_c_mat5_cELOSS,Am_CST_LOFT6_c_mat5_cELOSS,delta_v_CST_LOFT6_c_mat5_cELOSS]=postprocess_data(FRAGMENTS,ME,BUBBLE,PROPAGATION_THRESHOLD,link_data);

tf_LOFT6_c_mat5_cELOSS = sprintf('%25.4f',tf);
if exist('RunTime','var')
    cputime_LOFT6_c_mat5_cELOSS = sprintf('%20.2f',RunTime/60);
else
    cputime_LOFT6_c_mat5_cELOSS = '                   -';
end

final_step_LOFT6_c_mat5_cELOSS = sprintf('%24d',str2double(name_file_result_LOFT6_c_mat5_cELOSS(strfind(name_file_result_LOFT6_c_mat5_cELOSS,'step_')+5:end-4)));
clear FRAGMENTS ME BUBBLE PROPAGATION_THRESHOLD link_data tf RunTime
%}
%% processing LOFT7_AF scenario data

load(name_file_result_LOFT7_AF,'FRAGMENTS','ME','BUBBLE','PROPAGATION_THRESHOLD','link_data','tf','RunTime')

[n_frags_bubbles_LOFT7_AF,s_LOFT7_AF,N_LOFT7_AF,Am_CST_LOFT7_AF,delta_v_CST_LOFT7_AF]=postprocess_data(FRAGMENTS,ME,BUBBLE,PROPAGATION_THRESHOLD,link_data);

tf_LOFT7_AF = sprintf('%25.4f',tf);
if exist('RunTime','var')
    cputime_LOFT7_AF = sprintf('%20.2f',RunTime/60);
else
    cputime_LOFT7_AF = '                   -';
end

final_step_LOFT7_AF = sprintf('%24d',str2double(name_file_result_LOFT7_AF(strfind(name_file_result_LOFT7_AF,'step_')+5:end-4)));
clear FRAGMENTS BUBBLE PROPAGATION_THRESHOLD link_data tf RunTime

%% processing LOFT8_AF scenario data

load(name_file_result_LOFT8_AF,'FRAGMENTS','ME','BUBBLE','PROPAGATION_THRESHOLD','link_data','tf','RunTime')

[n_frags_bubbles_LOFT8_AF,s_LOFT8_AF,N_LOFT8_AF,Am_CST_LOFT8_AF,delta_v_CST_LOFT8_AF]=postprocess_data(FRAGMENTS,ME,BUBBLE,PROPAGATION_THRESHOLD,link_data);

tf_LOFT8_AF = sprintf('%25.4f',tf);
if exist('RunTime','var')
    cputime_LOFT8_AF = sprintf('%20.2f',RunTime/60);
else
    cputime_LOFT8_AF = '                   -';
end

final_step_LOFT8_AF = sprintf('%24d',str2double(name_file_result_LOFT8_AF(strfind(name_file_result_LOFT8_AF,'step_')+5:end-4)));
clear FRAGMENTS BUBBLE PROPAGATION_THRESHOLD link_data tf RunTime

%% processing _LOFT8_MD scenario data

load(name_file_result__LOFT8_MD,'FRAGMENTS','ME','BUBBLE','PROPAGATION_THRESHOLD','link_data','tf','RunTime')

[n_frags_bubbles__LOFT8_MD,s__LOFT8_MD,N__LOFT8_MD,Am_CST__LOFT8_MD,delta_v_CST__LOFT8_MD]=postprocess_data(FRAGMENTS,ME,BUBBLE,PROPAGATION_THRESHOLD,link_data);

tf__LOFT8_MD = sprintf('%25.4f',tf);
if exist('RunTime','var')
    cputime__LOFT8_MD = sprintf('%20.2f',RunTime/60);
else
    cputime__LOFT8_MD = '                   -';
end

final_step__LOFT8_MD = sprintf('%24d',str2double(name_file_result__LOFT8_MD(strfind(name_file_result__LOFT8_MD,'step_')+5:end-4)));
clear FRAGMENTS BUBBLE PROPAGATION_THRESHOLD link_data tf RunTime

%% plot finale Lc
%{d
colorOrder =  [ ...
        0            0            0       ;...% 6 BLACK
        0            0            0.7       ;...% 1 BLUE
        0            0.7            0       ;...% 3 GREEN (pale)
        0.7            0            0.7       ;...% 5 MAGENTA (pale)
        0            0.8            0.8       ;...% 4 CYAN
        0.6          0.5          0.4     ;  % 15 BROWN (dark)
        1            1            0       ;...% 7 YELLOW (pale)
        0            0.75         0.75    ;...% 8 TURQUOISE
        0            0.5          0       ;...% 9 GREEN (dark)
        0.75         0.75         0       ;...% 10 YELLOW (dark)
        1            0.50         0.25    ;...% 11 ORANGE
        0.75         0            0.75    ;...% 12 MAGENTA (dark)
        0.7          0.7          0.7     ;...% 13 GREY
        0.8          0.7          0.6     ;...% 14 BROWN (pale)
        0.4          0.7          0.6     ;...
        0.4          0          0.6     ;...
        0.4          0.6          0     ;...
        0.6          0.5          0.4 ];  % 18 BROWN (dark)
    

Lc_NASA=[min(LOFT6(:,1)):(max(LOFT6(:,1))-min(LOFT6(:,1)))/100:max(LOFT6(:,1)),1];
NASA_SBM_10_sub = 0.1*(mass_imp_1*norm(v_p_1))^0.75*Lc_NASA.^(-1.71);
NASA_SBM_10_cat = 0.1*(mass_LOFT+mass_imp_1)^0.75*Lc_NASA.^(-1.71);

%% Plot Lc comparison LOFT6 CST

xx_lim = [2e-03,1e0];
yy_lim = [1e0,1e6];
figure('units','normalized','outerposition',[0 0 1 1],'Visible', 'on')
loglog(s_LOFT6,[N_LOFT6(1)+n_frags_bubbles_LOFT6,N_LOFT6(2:end)],'MarkerEdgeColor',colorOrder(9,:),'MarkerFaceColor',colorOrder(9,:),'Marker','*','LineStyle','none');
hold on
grid on
loglog(s_LOFT6_c_mat5,[N_LOFT6_c_mat5(1)+n_frags_bubbles_LOFT6_c_mat5,N_LOFT6_c_mat5(2:end)],'MarkerEdgeColor',colorOrder(11,:),'MarkerFaceColor',colorOrder(11,:),'Marker','o','LineStyle','none');
hold on
loglog(s_LOFT6_cELOSS,[N_LOFT6_cELOSS(1)+n_frags_bubbles_LOFT6_cELOSS,N_LOFT6_cELOSS(2:end)],'MarkerEdgeColor',colorOrder(5,:),'MarkerFaceColor',colorOrder(5,:),'Marker','^','LineStyle','none');
hold on
% loglog(s_LOFT6_c_mat5_cELOSS,[N_LOFT6_c_mat5_cELOSS(1)+n_frags_bubbles_LOFT6_c_mat5_cELOSS,N_LOFT6_c_mat5_cELOSS(2:end)],'MarkerEdgeColor',colorOrder(4,:),'MarkerFaceColor',colorOrder(4,:),'Marker','+','LineStyle','none');
hold on
loglog(s_LOFT7_AF,[N_LOFT7_AF(1)+n_frags_bubbles_LOFT7_AF,N_LOFT7_AF(2:end)],'MarkerEdgeColor',colorOrder(12,:),'MarkerFaceColor',colorOrder(12,:),'Marker','d','LineStyle','none');
hold on
% loglog(s_LOFT8_AF,[N_LOFT8_AF(1)+n_frags_bubbles_LOFT8_AF,N_LOFT8_AF(2:end)],'MarkerEdgeColor',colorOrder(12,:),'MarkerFaceColor',colorOrder(12,:),'Marker','d','LineStyle','none');
hold on
% loglog(s_LOFT8_MD,[N_LOFT8_MD(1)+n_frags_bubbles_LOFT8_MD,N_LOFT8_MD(2:end)],'MarkerEdgeColor',colorOrder(12,:),'MarkerFaceColor',colorOrder(12,:),'Marker','d','LineStyle','none');
hold on
loglog(LOFT6(:,1),LOFT6(:,2),'MarkerEdgeColor',colorOrder(10,:),'MarkerFaceColor',colorOrder(10,:),'Marker','^','LineStyle','none');
loglog(Lc_NASA,NASA_SBM_10_sub,'--r','LineWidth',2)
loglog(Lc_NASA,NASA_SBM_10_cat,'-k','LineWidth',2)
set(gca,'FontSize',20,'FontWeight','bold')
title(['Comparison CST results - EMI results LOFT6 (EMR = ',EMR_LOFT6,' J/g)'])
xlabel('L_c [m]')
ylabel('Cumulative number CN (L_c)')
% legend('CST-LOFT6','CST-LOFT6 cmat*5','CST-LOFT6 cELOSS=0.2','CST-LOFT6 cmat*5 & cELOSS=0.2','LOFT7 AF (48U Cubesat)','LOFT8 AF (48U Cubesat on tank)','LOFT8 MD (48U Cubesat vertical)','NASA - SBM','Location','northwest')
legend('CST-LOFT6','CST-LOFT6 cmat*5','CST-LOFT6 cELOSS=0.2','LOFT7 AF','NASA - SBM SubCat 10 kg','NASA - SBM Cat');
set(gca,'XLim',xx_lim)
set(gca,'YLim',yy_lim)

out_fig = 'Lc_EMIvsCST_LOFT6_data_special_cases';
saveas(gcf,out_fig,'fig')
saveas(gcf,out_fig,'png')

%% plot finale LOFT6 A su M
%{d
xx_lim_Am = [-2,0.3];
yy_lim_Am = [0,0.12];
clear DAmSOC DAmSOC_matlab DAmSOC_NASA csi_min csi_max csi_NASA n
lambda_c_min=log10(min([s_LOFT6 s_LOFT6_c_mat5 s_LOFT6_cELOSS s_LOFT7_AF])); %s_LOFT6_c_mat5_cELOSS]));
lambda_c_max=log10(max([s_LOFT6 s_LOFT6_c_mat5 s_LOFT6_cELOSS s_LOFT7_AF])); %s_LOFT6_c_mat5_cELOSS]));
lambda_c=lambda_c_min:(lambda_c_max-lambda_c_min)/30:lambda_c_max;
Am_LOFT6 =[Am_CST_LOFT6 Am_CST_LOFT6_c_mat5 Am_CST_LOFT6_cELOSS Am_CST_LOFT7_AF];% Am_CST_LOFT6_c_mat5_cELOSS];
csi_min=log10(min(Am_LOFT6(Am_LOFT6>0)));
csi_max=log10(max(Am_LOFT6));
n=length(lambda_c);
csi_NASA=csi_min:(csi_max-csi_min)/(n-1):csi_max;

for i=1:1:n
        if  lambda_c(i)<=-3.5
            muSOC=-0.3;
            sigmaSOC=0.2;
            DAmSOC(i,:)=(1/(sigmaSOC*(2*pi)^0.5))*exp(-((csi_NASA-muSOC).^2)/(2*sigmaSOC^2));
            pd = makedist('Normal','mu',muSOC,'sigma',sigmaSOC);
            DAmSOC_matlab(i,:) = pdf(pd,csi_NASA);
        elseif lambda_c(i)>-3.5 && lambda_c(i)<=-1.75
            muSOC=-0.3;
            sigmaSOC=0.2+0.1333*(lambda_c(i)+3.5);
            DAmSOC(i,:)=(1/(sigmaSOC*(2*pi)^0.5))*exp(-((csi_NASA-muSOC).^2)/(2*sigmaSOC^2));
            pd = makedist('Normal','mu',muSOC,'sigma',sigmaSOC);
            DAmSOC_matlab(i,:) = pdf(pd,csi_NASA);
        elseif lambda_c(i)>-1.75 && lambda_c(i)<-1.25
            muSOC=-0.3-1.4*(lambda_c(i)+1.75);
            sigmaSOC=0.2+0.1333*(lambda_c(i)+3.5);
            DAmSOC(i,:)=(1/(sigmaSOC*(2*pi)^0.5))*exp(-((csi_NASA-muSOC).^2)/(2*sigmaSOC^2));
            pd = makedist('Normal','mu',muSOC,'sigma',sigmaSOC);
            DAmSOC_matlab(i,:) = pdf(pd,csi_NASA);
        elseif lambda_c(i)>=-1.25
            muSOC=-1.0;
            sigmaSOC=0.2+0.1333*(lambda_c(i)+3.5);
            DAmSOC(i,:)=(1/(sigmaSOC*(2*pi)^0.5))*exp(-((csi_NASA-muSOC).^2)/(2*sigmaSOC^2));
            pd = makedist('Normal','mu',muSOC,'sigma',sigmaSOC);
            DAmSOC_matlab(i,:) = pdf(pd,csi_NASA);
        end
end

for k=1:1:size(DAmSOC,2)
    DAmSOC_NASA(k)=sum(DAmSOC(:,k));
    DAmSOC_matlab_NASA(k) = sum(DAmSOC_matlab(:,k));
end

figure('units','normalized','outerposition',[0 0 1 1],'Visible', 'on')
histogram(log10(Am_CST_LOFT6),'Normalization','probability','BinWidth',0.035)
grid on
hold on
histogram(log10(Am_CST_LOFT6_c_mat5),'Normalization','probability','BinWidth',0.035)
hold on
histogram(log10(Am_CST_LOFT6_cELOSS),'Normalization','probability','BinWidth',0.035)
hold on
% histogram(log10(Am_CST_LOFT6_c_mat5_cELOSS),'Normalization','probability','BinWidth',0.035)
hold on
histogram(log10(Am_CST_LOFT7_AF),'Normalization','probability','BinWidth',0.035)
hold on
% histogram(log10(Am_CST_LOFT8_AF),'Normalization','probability','BinWidth',0.035)
hold on
% histogram(log10(Am_CST_LOFT8_MD),'Normalization','probability','BinWidth',0.035)
hold on
plot(csi_NASA,DAmSOC_NASA./sum(DAmSOC_NASA),'-b','LineWidth',2)
hold on
title('Comparison among PDFs')
set(gca,'FontSize',20,'FontWeight','bold')
xlabel('log_{10}(A/m) [m^{2}/kg]')
ylabel('Relative number N(A/m)')
% legend('CST-LOFT6','CST-LOFT6 cmat*5','CST-LOFT6 cELOSS=0.2','CST-LOFT6 cmat*5 & cELOSS=0.2','LOFT7 AF (48U Cubesat)','LOFT8 AF (48U Cubesat on tank)','LOFT8 MD (48U Cubesat vertical)','NASA - SBM','Location','northwest')
legend('CST-LOFT6','CST-LOFT6 cmat*5','CST-LOFT6 cELOSS=0.2','LOFT7 AF','NASA - SBM','Location','northwest')
set(gca,'XLim',xx_lim_Am)
set(gca,'YLim',yy_lim_Am)

out_fig = 'A_su_m_CST_LOFT6_data_special_cases';
saveas(gcf,out_fig,'fig')
saveas(gcf,out_fig,'png')

%% comparison finale LOFT6 A su M CST vs EMI
data_Am_EMI_loft6 = [-1.1513973108357503, 121.81308648484992;
-1.044804757301462, 104.57860971965965;
-1.0416044174942145, 1050.814853873122;
-0.9468392067258402, 1014.8812139320999;
-0.9432141078595078, 1423.0893165773214;
-0.8563728505726926, 1424.1634199451237;
-0.8336995049359932, 2704.5922801261586;
-0.7469314819697104, 2798.429856167795;
-0.7482350528751796, 4449.619669762038;
-0.637739110056537, 4488.092099481502;
-0.6434806807862437, 11760.748357110078;
-0.5408793977209481, 11799.123141068832;
-0.5375032955444241, 12522.727050805091;
-0.4309107420101359, 12505.492574039901;
-0.4129903037759619, 4806.270810752752;
-0.31034508011834694, 4788.987511107207;
-0.30509417933620453, 3137.846520393319;
-0.21822362832117692, 3101.8152346915886;
-0.20475583677534637, 1042.612609973541;
-0.10208131938951892, 988.2239212584591;
-0.10541348097372394, 208.96192791790236;
-0.006744880920995566, 228.7351944615366;
-0.006598412279931409, 43.20824911386808;
0.17104607903447855, 26.852584195057716;
0.18274160002343454, 212.5259981837935;
0.4156340627471655, 215.40654812471803;
0.4196692738084775, 104.13920379646879;
0.5144198377127451, 86.7582583902149];
if data_Am_EMI_loft6(1,2)~=0
    data_Am_EMI_loft6=[data_Am_EMI_loft6(1,1) 0;data_Am_EMI_loft6];
end
if data_Am_EMI_loft6(end,2)~=0
    data_Am_EMI_loft6=[data_Am_EMI_loft6; data_Am_EMI_loft6(end,1) 0];
end
for i=2:2:size(data_Am_EMI_loft6,1)-1
    data_Am_EMI_loft6(i,2)=(data_Am_EMI_loft6(i,2)+data_Am_EMI_loft6(i+1,2))/2;
    data_Am_EMI_loft6(i+1,2)=data_Am_EMI_loft6(i,2);
end
for i=1:2:size(data_Am_EMI_loft6,1)-1
    data_Am_EMI_loft6(i,1)=(data_Am_EMI_loft6(i,1)+data_Am_EMI_loft6(i+1,1))/2;
    data_Am_EMI_loft6(i+1,1)=data_Am_EMI_loft6(i,1);
end
data_NASA_SBM_Am_EMI_loft6=[-1.1012537715675077, 1606.663346710804; % -2 sigma
-0.3081187567741748, 11968.977941822659; % mu
0.5013621583618941, 1626.4854361347934]; % + 2 sigma
mu = data_NASA_SBM_Am_EMI_loft6(2,1);
sigma = (data_NASA_SBM_Am_EMI_loft6(3,1)-data_NASA_SBM_Am_EMI_loft6(1,1))/4;
pd = makedist('Normal','mu',mu,'sigma',sigma);
x=-2:0.01:1.5;
NASA = pdf(pd,x);
n_frag_EMI=0;
for i=2:2:size(data_Am_EMI_loft6,1)-1
    n_frag_EMI=n_frag_EMI+data_Am_EMI_loft6(i,2);
end
figure('units','normalized','outerposition',[0 0 1 1],'Visible', 'on')
box on
grid on
hold on
histogram(log10(Am_CST_LOFT7_AF),'BinWidth',0.15)
hold on
plot(data_Am_EMI_loft6(:,1),data_Am_EMI_loft6(:,2),'-r','LineWidth',3)
hold on
plot(x,NASA*data_NASA_SBM_Am_EMI_loft6(2,2),'-b','LineWidth',3)
hold on
title('Comparison EMI-CST results LOFT 6')
xlabel('log_{10}(A/m) [m^2/kg]')
ylabel('N(A/m)')
legend('CST simulation LOFT7 AF','EMI','NASA - SBM','Location','northwest')
set(gca,'FontSize',20,'FontWeight','bold')
set(gca,'XLim',[xx_lim_Am(1) 0.55])
disp(' ')
disp(['For LOFT7 AF, EMI has ',num2str(ceil(n_frag_EMI),'%d'),' fragments,']);
disp(['while CST has ',num2str(length(N_LOFT6),'%d'),' fragments.'])
disp(' ')

out_fig = 'A_su_m_EMIvsCST_LOFT7_AF_data';
saveas(gcf,out_fig,'fig')
saveas(gcf,out_fig,'png')


%% plot cpu time and simulation time
if exist('Data_Simulation.txt','file')
    delete('Data_Simulation.txt')
end
diary Data_Simulation.txt
diary on
disp('--------------------------------------------------------------------------------------------')
disp('SIMULATION                CPUTIME (min)       SIMULATION TIME (s)             FINAL STEP')
disp(['LOFT6 baseline  ',cputime_LOFT6,tf_LOFT6,final_step_LOFT6])
disp(['LOFT6 c_mat*5   ',cputime_LOFT6_c_mat5,tf_LOFT6_c_mat5,final_step_LOFT6_c_mat5])
disp(['LOFT6 cELOSS=0.2',cputime_LOFT6_cELOSS,tf_LOFT6_cELOSS,final_step_LOFT6_cELOSS])
% disp(['LOFT6 c_matt*5 & CELOSS=0.2   ',cputime_LOFT6_c_mat5_cELOSS,tf_LOFT6_c_mat5_cELOSS,final_step_LOFT6_c_mat5_cELOSS])
disp(['LOFT7 AF        ',cputime_LOFT7_AF,tf_LOFT7_AF,final_step_LOFT7_AF])
% disp(['LOFT8 AF        ',cputime_LOFT8_AF,tf_LOFT8_AF,final_step_LOFT8_AF])
% disp(['LOFT8 MD        ',cputime_LOFT8_MD,tf_LOFT8_MD,final_step_LOFT8_MD])
disp('--------------------------------------------------------------------------------------------')
diary off