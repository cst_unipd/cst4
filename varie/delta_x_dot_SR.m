clear all
close all

load('workspace_SR.mat')
[n,m]=size(y);
x=zeros(n,m/6);
x_dot=zeros(n,m/6);
for i=1:n
    k=1;
    for j=1:3:m/2
        x(i,k)=norm(y(i,j:j+2));
        x_dot(i,k)=norm(y(i,m/2+j:m/2+j+2));
        k=k+1;
    end
end
delta_x_dot=zeros(1,k-1);
delta_x=zeros(1,k-1);
for i=1:k-1
    delta_x(1,i) = (max(x(:,i))-min(x(:,i)))/2;
    delta_x_dot(1,i) = (max(x_dot(:,i))-min(x_dot(:,i)))/2;
end
figure
subplot(121)
plot(delta_x,'ob')
subplot(122)
plot(delta_x_dot/0.001,'-r')

for ME_index=1:50
    figure
    subplot(121)
    plot(t,x(:,ME_index),'-b')
    subplot(122)
    plot(t,x_dot(:,ME_index),'*r')
    pause
    close all
end