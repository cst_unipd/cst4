clc
clear all
close all



v=0.5:0.05:12;
dp=zeros(1,length(v));
dp1=dp;
dp2=dp;
dp_SH=dp;
alpha=0;   % rad
tw=0.12;    % cm
ts=0.12;    % cm
sigmaw=10; % ksi
rhop=2.7;  % g/cm^3
rhob=2.7;  % g/cm^3
S=10;       % cm


%% TWO WALLS

for i=1:length(v)
    dp1(i)=((tw*sqrt(sigmaw/40)+ts)/(0.6*(cos(alpha)^(5/6))*rhop^0.5*v(i)^(2/3)))^(18/19);
    dp2(i)=3.918* tw^(2/3) * rhop^(-1/3) * rhob^(-1/9) * v(i)^(-2/3) * S^(1/3) * (sigmaw/70)^(1/3);
    if v(i)<3
        dp(i)=((tw*sqrt(sigmaw/40)+ts)/(0.6*(cos(alpha)^(5/6))*rhop^0.5*v(i)^(2/3)))^(18/19);
    elseif v(i)>7
        dp(i)=3.918* tw^(2/3) * rhop^(-1/3) * rhob^(-1/9) * v(i)^(-2/3) * S^(1/3) * (sigmaw/70)^(1/3);
    else
        dp7=3.918* tw^(2/3) * rhop^(-1/3) * rhob^(-1/9) * (7)^(-2/3) * S^(1/3) * (sigmaw/70)^(1/3);
        dp3=((tw*sqrt(sigmaw/40)+ts)/(0.6*(cos(alpha)^(5/6))*rhop^0.5*3^(2/3)))^(18/19);
        dp(i)= ((v(i)-3)/(7-3) *(dp7-dp3)) + dp3;
    end
end

figure('Position',[100 300 1100 600])
subplot(1,2,1)
% plot(v,dp1,'r--',v,dp2,'g-.')
hold on
plot(v,dp,'k')
grid on
ylim([0 1.5*max(dp)])


%% IMPLEMENTIN EQUATIONS BY REIMERDES NOLKE SCHAFER

k=1; %limit for perforation? TBD
KK=0.42;
flag1=0;
VL=0;
DL=1;

for i=1:length(v)
    if flag1==1
        F2=1;
        if v(i)>7
            dp_SH(i)= 3.918* F2^(-2/3) * tw^(2/3) * rhop^(-1/3) * rhop^(-1/9) * v(i)^(-2/3) * S^(1/3) * (sigmaw/70)^(1/3);
        else
            d7=3.918* F2^(-2/3) * tw^(2/3) * rhop^(-1/3) * rhop^(-1/9) * 7^(-2/3) * S^(1/3) * (sigmaw/70)^(1/3);
            dp_SH(i)= ((v(i)-VL)/(7-VL) *(d7-DL)) + DL;
        end
        if (ts/dp_SH(i))>0.2
            F2= 5-40*(ts/dp_SH(i))+100*((ts/dp_SH(i))^2);
            if v(i)>7
                dp_SH(i)= 3.918* F2^(-2/3) * tw^(2/3) * rhop^(-1/3) * rhop^(-1/9) * v(i)^(-2/3) * S^(1/3) * (sigmaw/70)^(1/3);
            else
                d7=3.918* F2^(-2/3) * tw^(2/3) * rhop^(-1/3) * rhop^(-1/9) * 7^(-2/3) * S^(1/3) * (sigmaw/70)^(1/3);
                dp_SH(i)= ((v(i)-VL)/(7-VL) *(d7-DL)) + DL;
            end
        end
    else % sono sotto v_LIM
        dp_SH(i)=((tw/k+ts)/(0.796*KK*rhop^0.518*v(i)^(2/3)))^(18/19);
        v_lim=1.853+0.397*(ts/dp(i));
        if v_lim<v(i)
            flag1=1;
            VL=v_lim;
            DL=dp_SH(i);
        end
    end
end

subplot(1,2,2)
plot(v,dp,'k',v,dp_SH,'b--')
grid on
ylim([0 1.5*max(dp)])