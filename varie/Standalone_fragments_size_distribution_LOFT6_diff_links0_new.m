% Standalone for fragments size distribution
clear all
close all

global MATERIAL_LIST

name_file_result_baseline='E:\AO_8507Catastrophic\CST2_ESTEC\CST3\Results\LOFT6_20181108T175437\data\step_2000.mat';
name_file_result_continuum='E:\AO_8507Catastrophic\CST2_ESTEC\CST3_LOFT6_all_continuum\Results\LOFT6_20181113T140812\data\step_2000.mat';
name_file_result_bolted='E:\AO_8507Catastrophic\CST2_ESTEC\CST3_LOFT6_all_bolted_MD\Results\LOFT6_20181119T170308\data\step_1980.mat';
name_file_result_no_links='E:\AO_8507Catastrophic\CST2_ESTEC\CST3_LOFT6_no_link\Results\LOFT6_20181116T165830\data\step_2000.mat';

LOFT6=load('22_PDFsam_2018CSID_MSchimmerohn_SCDisrupt_crop_LOFT6_LOG.txt');
LOFT6=sortrows(LOFT6);
%% processing mixed link scenario data
load(name_file_result_baseline,'FRAGMENTS','ME','BUBBLE','PROPAGATION_THRESHOLD','link_data')
name_mat=[name_file_result_baseline(1:strfind(name_file_result_baseline,'Results')-1),filesep,'set_up',filesep,'material_list.m'];
run(name_mat)

mass_imp=0;
q_imp = [0 0 0]';
for i = 52:length(ME)
    mass_imp = mass_imp + ME(i).GEOMETRY_DATA.mass0;
    q_imp = q_imp + ME(i).GEOMETRY_DATA.mass0*ME(i).DYNAMICS_INITIAL_DATA.vel0;
end
v_p = 1/mass_imp*q_imp/1000;

[n_frags_bubbles_baseline,s_baseline,N_baseline,Am_CST_baseline,delta_v_CST_baseline]=postprocess_data(FRAGMENTS,ME,BUBBLE,PROPAGATION_THRESHOLD,link_data);

clear FRAGMENTS ME BUBBLE PROPAGATION_THRESHOLD link_data

%% processing continuum link scenario data

load(name_file_result_continuum,'FRAGMENTS','ME','BUBBLE','PROPAGATION_THRESHOLD','link_data')

[n_frags_bubbles_continuum,s_continuum,N_continuum,Am_CST_continuum,delta_v_CST_continuum]=postprocess_data(FRAGMENTS,ME,BUBBLE,PROPAGATION_THRESHOLD,link_data);

clear FRAGMENTS ME BUBBLE PROPAGATION_THRESHOLD link_data

%% processing bolted link scenario data

load(name_file_result_bolted,'FRAGMENTS','ME','BUBBLE','PROPAGATION_THRESHOLD','link_data')

[n_frags_bubbles_bolted,s_bolted,N_bolted,Am_CST_bolted,delta_v_CST_bolted]=postprocess_data(FRAGMENTS,ME,BUBBLE,PROPAGATION_THRESHOLD,link_data);

clear FRAGMENTS ME BUBBLE PROPAGATION_THRESHOLD link_data

%% processing no link scenario data

load(name_file_result_no_links,'FRAGMENTS','ME','BUBBLE','PROPAGATION_THRESHOLD','link_data')

[n_frags_bubbles_no_links,s_no_links,N_no_links,Am_CST_no_links,delta_v_CST_no_links]=postprocess_data(FRAGMENTS,ME,BUBBLE,PROPAGATION_THRESHOLD,link_data);

clear FRAGMENTS BUBBLE PROPAGATION_THRESHOLD link_data

%% plot finale Lc

colorOrder =  [ ...
        0            0            0       ;...% 6 BLACK
        0            0            0.7       ;...% 1 BLUE
        0            0.7            0       ;...% 3 GREEN (pale)
        0.7            0            0.7       ;...% 5 MAGENTA (pale)
        0            0.8            0.8       ;...% 4 CYAN
        0.6          0.5          0.4     ;  % 15 BROWN (dark)
        1            1            0       ;...% 7 YELLOW (pale)
        0            0.75         0.75    ;...% 8 TURQUOISE
        0            0.5          0       ;...% 9 GREEN (dark)
        0.75         0.75         0       ;...% 10 YELLOW (dark)
        1            0.50         0.25    ;...% 11 ORANGE
        0.75         0            0.75    ;...% 12 MAGENTA (dark)
        0.7          0.7          0.7     ;...% 13 GREY
        0.8          0.7          0.6     ;...% 14 BROWN (pale)
        0.4          0.7          0.6     ;...
        0.4          0          0.6     ;...
        0.4          0.6          0     ;...
        0.6          0.5          0.4 ];  % 15 BROWN (dark)
    
NASA_SBM_sub = 0.1*(mass_imp*norm(v_p))^0.75*s_baseline.^(-1.71);

m_tot=0;
for ii = 1:length(ME)
    m_tot=m_tot+ME(ii).GEOMETRY_DATA.mass0;
end
NASA_SBM_cat = 0.1*(m_tot)^0.75*s_baseline.^(-1.71);

h = figure('Position', [45   129   906   553],'Visible', 'on');
loglog(s_baseline,[N_baseline(1)+n_frags_bubbles_baseline,N_baseline(2:end)],'+g','LineWidth',2)
grid on
hold on
loglog(s_continuum,[N_continuum(1)+n_frags_bubbles_continuum,N_continuum(2:end)],'MarkerEdgeColor',colorOrder(9,:),'Marker','*','LineStyle','none','LineWidth',2)
hold on
loglog(s_bolted,[N_bolted(1)+n_frags_bubbles_bolted,N_bolted(2:end)],'MarkerEdgeColor',colorOrder(11,:),'Marker','o','LineStyle','none','LineWidth',2)
hold on
loglog(s_no_links,[N_no_links(1)+n_frags_bubbles_no_links,N_no_links(2:end)],'MarkerEdgeColor',colorOrder(5,:),'Marker','^','LineStyle','none','LineWidth',2)
hold on
loglog(LOFT6(:,1),LOFT6(:,2),'-k','LineWidth',2);
hold on
loglog(s_baseline,NASA_SBM_sub,'-r','LineWidth',2)
hold on
loglog(s_baseline,NASA_SBM_cat,'--r','LineWidth',2)
hold on
set(gca,'FontSize',20,'FontWeight','bold')
title('Comparison CST results LOFT6')
xlabel('L_c [m]')
ylabel('Cumulative number CN(L_c)')
legend('CST baseline ','CST (all continuum links)','CST (all bolted links)','CST (no links)','EMI results LOFT6','NASA - SubCat-SBM 10kg','NASA - Cat-SBM')

%% plot finale A su M
%{s
clear DAmSOC DAmSOC_matlab DAmSOC_NASA
lambda_c_min=log10(min([s_baseline s_continuum s_bolted s_no_links]));
lambda_c_max=log10(max([s_baseline s_continuum s_bolted s_no_links]));
lambda_c=lambda_c_min:(lambda_c_max-lambda_c_min)/40:lambda_c_max;
csi_min=log10(min([Am_CST_baseline Am_CST_continuum Am_CST_bolted Am_CST_no_links]));
csi_max=log10(max([Am_CST_baseline Am_CST_continuum Am_CST_bolted Am_CST_no_links]));
n=length(lambda_c);
csi_NASA=csi_min:(csi_max-csi_min)/(n-1):csi_max;

for i=1:1:n
        if  lambda_c(i)<=-3.5
            muSOC=-0.3;
            sigmaSOC=0.2;
            DAmSOC(i,:)=(1/(sigmaSOC*(2*pi)^0.5))*exp(-((csi_NASA-muSOC).^2)/(2*sigmaSOC^2));
            pd = makedist('Normal','mu',muSOC,'sigma',sigmaSOC);
            DAmSOC_matlab(i,:) = pdf(pd,csi_NASA);
        elseif lambda_c(i)>-3.5 && lambda_c(i)<=-1.75
            muSOC=-0.3;
            sigmaSOC=0.2+0.1333*(lambda_c(i)+3.5);
            DAmSOC(i,:)=(1/(sigmaSOC*(2*pi)^0.5))*exp(-((csi_NASA-muSOC).^2)/(2*sigmaSOC^2));
            pd = makedist('Normal','mu',muSOC,'sigma',sigmaSOC);
            DAmSOC_matlab(i,:) = pdf(pd,csi_NASA);
        elseif lambda_c(i)>-1.75 && lambda_c(i)<-1.25
            muSOC=-0.3-1.4*(lambda_c(i)+1.75);
            sigmaSOC=0.2+0.1333*(lambda_c(i)+3.5);
            DAmSOC(i,:)=(1/(sigmaSOC*(2*pi)^0.5))*exp(-((csi_NASA-muSOC).^2)/(2*sigmaSOC^2));
            pd = makedist('Normal','mu',muSOC,'sigma',sigmaSOC);
            DAmSOC_matlab(i,:) = pdf(pd,csi_NASA);
        elseif lambda_c(i)>=-1.25
            muSOC=-1.0;
            sigmaSOC=0.2+0.1333*(lambda_c(i)+3.5);
            DAmSOC(i,:)=(1/(sigmaSOC*(2*pi)^0.5))*exp(-((csi_NASA-muSOC).^2)/(2*sigmaSOC^2));
            pd = makedist('Normal','mu',muSOC,'sigma',sigmaSOC);
            DAmSOC_matlab(i,:) = pdf(pd,csi_NASA);
        end
end

for k=1:1:size(DAmSOC,2)
    DAmSOC_NASA(k)=sum(DAmSOC(:,k));
    DAmSOC_matlab_NASA(k) = sum(DAmSOC_matlab(:,k));
end

h = figure('Position', [604          45        1010         500],'Visible', 'on');
histogram(log10(Am_CST_baseline),'Normalization','probability','BinWidth',0.03)
grid on
hold on
histogram(log10(Am_CST_continuum),'Normalization','probability','BinWidth',0.03)
hold on
histogram(log10(Am_CST_bolted),'Normalization','probability','BinWidth',0.03)
hold on
histogram(log10(Am_CST_no_links),'Normalization','probability','BinWidth',0.03)
hold on
plot(csi_NASA,DAmSOC_NASA./sum(DAmSOC_NASA),'-b','LineWidth',2)
hold on
title('Comparison among PDFs')
set(gca,'FontSize',20,'FontWeight','bold')
xlabel('log_{10}(A/m) [m^{2}/kg]')
ylabel('CN(A/m)')
legend('CST baseline','CST (all continuum links)','CST (all bolted links)','CST (no links)','Nasa - SBM','Location','northeast')

%% plot finale of Delta_v
%{
clear DdvCOLL_NASA DdvCOLL_NASA_1

muEXP=0.9*csi_NASA+2.9;
sigmaEXP=0.4;
delta_v_lim=[0 3];
delta_v_NASA=delta_v_lim(1):abs(delta_v_lim(1)-delta_v_lim(2))/(n-1):delta_v_lim(2);

for i=1:1:n
    DdvCOLL_NASA(:,i)=(1/(sigmaEXP*(2*pi)^0.5))*exp(-((delta_v_NASA-muEXP(i)).^2)/(2*sigmaEXP^2));
end

for k=1:1:size(DdvCOLL_NASA,1)
    DdvCOLL_NASA_1(k)=sum(DdvCOLL_NASA(:,k));
end

figure
histogram(delta_v_CST_baseline,'Normalization','probability','BinWidth',1)
grid on
hold on
histogram(delta_v_CST_continuum,'Normalization','probability','BinWidth',1)
hold on
histogram(delta_v_CST_bolted,'Normalization','probability','BinWidth',1)
hold on
histogram(delta_v_CST_no_links,'Normalization','probability','BinWidth',1)
hold on
% plot(delta_v_NASA,DdvCOLL_NASA_1./sum(DdvCOLL_NASA_1),'b','LineWidth',2)
title('Comparison among PDFs')
xlabel('\DeltaV [m/s] ')
ylabel('Relative Number of Fragments per \DeltaV bin')
legend('CST baseline','CST (all continuum links)','CST (all bolted links)','CST (no links)','NASA SBM')
xlim([0 60000])
%}