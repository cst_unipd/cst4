% function for computing the standard deviation between curves
% H_ref: reference curve [x(:),y(:)]
% H_i:   i-th curve to be compared [x(:),y(:)]
clear all
close all

TEST='CFRP_';

% GetDATAFromFig;

if TEST(1:4)=='7_Lc'
%     load('E:\AO_8507Catastrophic\CST_integration\CST3\Results\Sphere_vs_Simple_plate_20180919T103422\data\step_50.mat') %par 0.2
    load('E:\AO_8507Catastrophic\CST_integration\CST3\Results\Sphere_vs_Simple_plate_20180920T121943\data\step_50.mat') %par 0.1
    load('REF_test_7_Nishida.mat')
    
elseif TEST(1:4)=='7_Ae'
    load('E:\AO_8507Catastrophic\CST_integration\CST3\Results\Sphere_vs_Simple_plate_20180919T103422\data\step_50.mat') %par 0.2
%     load('E:\AO_8507Catastrophic\CST_integration\CST3\Results\Sphere_vs_Simple_plate_20180920T121943\data\step_50.mat') %par 0.1
    load('Test7_fit_sper_Ae.mat')
    H_ref=[X,Y];
    v_p=4.14; %[km/s]
    d_p=3.2; %[mm]
    
elseif TEST=='CFRP_'
%     load('E:\AO_8507Catastrophic\CST_integration\CST3\Results\Sphere_vs_Simple_plate_20180919T113838\data\step_50.mat') % par 0.2
    load('E:\AO_8507Catastrophic\CST_integration\CST3\Results\Sphere_vs_Simple_plate_20180920T141244\data\step_50.mat') %par 0.1
    uiopen('C:\Users\saregiu17613\Desktop\Nishida CFRP\Nishida_plot_digitizer\CFRP_Nishida_Lc_2.82.mat',1)
    Lc_nishida=X2;
    N_nishida=Y2;
    H_ref=[Lc_nishida,N_nishida];
    corr_coeff_Lc=1.67;
    corr_coeff_cumul=4.63;
    
elseif TEST=='10_Lc'
%     load('E:\AO_8507Catastrophic\CST_integration\CST3\Results\Sphere_vs_Simple_plate_20180919T105224\data\step_50.mat') %par 0.2
    load('E:\AO_8507Catastrophic\CST_integration\CST3\Results\Sphere_vs_Simple_plate_20180920T123317\data\step_50.mat') %par 0.1
    load('REF_test_10_Nishida.mat')
    
elseif TEST=='10_Ae'
    load('E:\AO_8507Catastrophic\CST_integration\CST3\Results\Sphere_vs_Simple_plate_20180919T105224\data\step_50.mat')     %par 0.2
%     load('E:\AO_8507Catastrophic\CST_integration\CST3\Results\Sphere_vs_Simple_plate_20180920T123317\data\step_50.mat') %par 0.1
    load('Test10_fit_sper_Ae.mat')
    H_ref=[X,Y];
    v_p=6.75; %[km/s]
    d_p=1.6; %[mm]
end

if exist('FRAGMENTS_FR','var')
    if length(FRAGMENTS_FR)>length(FRAGMENTS)
        FRAGMENTS=FRAGMENTS_FR;
        ME=ME_FR;
    end
end

%% DELETE FRAGMENTs & MEs WITH MASS == 0 AND DEFINE DIMENSIONS
n_size=length(FRAGMENTS);
Amean=zeros(1,n_size);
for i=n_size:-1:1
    if FRAGMENTS(i).GEOMETRY_DATA.mass==0
        FRAGMENTS(i)=[];
        Amean(i)=[];
    else
        box = boundingBox3d(FRAGMENTS(i).GEOMETRY_DATA.c_hull);
        lx = abs(box(2)-box(1));
        ly = abs(box(4)-box(3));
        lz = abs(box(6)-box(5));
        FRAGMENTS(i).GEOMETRY_DATA.dimensions=[lx ly lz];
        x(i)=max(FRAGMENTS(i).GEOMETRY_DATA.dimensions);
        Amean(i)=(lx*ly+ly*lz+lx*lz)/3;
    end
end
n_size=length(FRAGMENTS);
FRAGMENTS1=FRAGMENTS;

n_size1=length(ME);
for i=n_size1:-1:1
    if ME(i).GEOMETRY_DATA.mass==0
        ME(i)=[];
    end
end
n_size1=length(ME);

if ~isempty(ME)
    FRAGMENTS1(n_size+1:n_size+n_size1)=ME;
end

n_size=length(FRAGMENTS1);

%% CREATE Lc, AM RATIO AND VELOCITY DISTRIBUTIONS FROM CST RESULTS
s1 =zeros(1,n_size-1);
k=1;

for i = 2:n_size
    Lc_CST(i-1)=mean(FRAGMENTS1(i).GEOMETRY_DATA.dimensions);
    Am_CST(i-1)=((FRAGMENTS1(i).GEOMETRY_DATA.dimensions(1)*FRAGMENTS1(i).GEOMETRY_DATA.dimensions(2)+...
        FRAGMENTS1(i).GEOMETRY_DATA.dimensions(1)*FRAGMENTS1(i).GEOMETRY_DATA.dimensions(3)+...
        FRAGMENTS1(i).GEOMETRY_DATA.dimensions(2)*FRAGMENTS1(i).GEOMETRY_DATA.dimensions(3))/3/FRAGMENTS1(i).GEOMETRY_DATA.mass);
    vel_CST_wp(i-1)=norm(FRAGMENTS1(i).DYNAMICS_DATA.vel);
    mass_CST(i-1)=FRAGMENTS1(i).GEOMETRY_DATA.mass;
    if FRAGMENTS1(i).object_ID~=66
        vel_CST_np(k)=norm(FRAGMENTS1(i).DYNAMICS_DATA.vel);
        k=k+1;
    end
end

%% FRAGMENT DISTRIBUTIONS
if strcmp(TEST,'10_Lc') || strcmp(TEST,'7_Lc') || strcmp(TEST,'CFRP_')
    s=sort(Lc_CST);
    n_size=length(Lc_CST);
    if n_size==1 %patch to avoid only one fragment case (LO)
        n_size=n_size+1;
    end
    N=n_size:-1:1;
    
    if strcmp(TEST,'CFRP_')
        H_1=[s'*corr_coeff_Lc,N'*corr_coeff_cumul];
    else
        H_1=[s'*1000,N'];
    end
    
elseif strcmp(TEST,'10_Ae') || strcmp(TEST,'7_Ae')
    s=sort(Amean);
    n_size=length(Amean);
    if n_size==1 %patch to avoid only one fragment case (LO)
        n_size=n_size+1;
    end
    N=n_size:-1:1;
    
    H_1=[s*10^6/(pi*d_p^2/4); N/v_p^1.5]';
end

% windowing
global_min = min(H_ref(:,1));
global_max = max(H_ref(:,1));
H_1_new = H_1(and(H_1(:,1)>global_min,H_1(:,1)<global_max),:);

for j=1:length(H_ref)-1
    II1=[];
    II1=find(H_1_new(:,1)>= H_ref(j,1) & H_1_new(:,1)< H_ref(j+1,1));
    if ~isempty(II1)
        for i=1:length(II1)
            CN_1(II1(i),1)= H_1_new(II1(i),1);
            CN_1(II1(i),2)= H_ref(j,2)+(H_ref(j+1,2)-H_ref(j,2))/(H_ref(j+1,1)-H_ref(j,1))*(H_1_new(II1(i),1)-H_ref(j,1));
        end
    end
end
stn_dev1=sqrt(1/(length(CN_1(:,1))-1)*sum(((H_1_new(:,2)-CN_1(:,2)).^2)./((CN_1(:,2)).^2)))

%% figure

loglog(H_ref(:,1),H_ref(:,2),'*r');
hold on
loglog(H_1_new(:,1),H_1_new(:,2),'ob');
hold on
loglog(CN_1(:,1),CN_1(:,2),'xg');
hold on
grid on
if strcmp(TEST,'10_Lc') || strcmp(TEST,'7_Lc') || strcmp(TEST,'CFRP_')
xlabel('log(Lc)')
ylabel('Cumulative Number N')
title(['Standard deviation between CST results and Nishida experimental data for test ',TEST(1:strfind(TEST,'_')-1)])
legend('Nishida experimental results','CST results','CST results projected on Nishida experimental results')
elseif strcmp(TEST,'10_Ae') || strcmp(TEST,'7_Ae')
xlabel('log(A_e / (\pi D_p^2 / 4))')
ylabel('log(N(A_e) / V^1^.^5)')
title(['Standard deviation between CST results and Nishida experimental data for test ',TEST(1:strfind(TEST,'_')-1)])
legend('Nishida experimental results','CST results','CST results projected on Nishida experimental results')
end

if strcmp(TEST,'7_Lc')
   xlim([0.5 3.5])
elseif strcmp(TEST,'10_Lc')
   xlim([0.5 3])
end
if strcmp(TEST,'7_Lc') || strcmp(TEST,'10_Lc')
   ylim([1 10^3])
end

if strcmp(TEST,'7_Ae')
   xlim([0.01 1])
elseif strcmp(TEST,'10_Ae')
   xlim([0.02 1])
end
if strcmp(TEST,'10_Ae')
   ylim([10^-1 10^2])
elseif strcmp(TEST,'7_Ae')
   ylim([10^-1 10^2])
end