function [n_frags_bubbles,s,N,Am_CST,delta_v_CST]=postprocess_data(FRAGMENTS,ME,BUBBLE,PROPAGATION_THRESHOLD,link_data,l_target)

global MATERIAL_LIST

n_size=length(FRAGMENTS);
for i=n_size:-1:1
    if FRAGMENTS(i).GEOMETRY_DATA.mass==0
        FRAGMENTS(i)=[];
    else
        box = boundingBox3d(FRAGMENTS(i).GEOMETRY_DATA.c_hull);
        lx = abs(box(2)-box(1));
        ly = abs(box(4)-box(3));
        lz = abs(box(6)-box(5));
        FRAGMENTS(i).GEOMETRY_DATA.dimensions=[lx ly lz];
    end
end
n_size=length(FRAGMENTS);
connection=[];
ii=1;
for jj =length(link_data):-1:1
    if link_data(jj).state==0
        link_data(jj)=[];
    else
        connection(ii,1:2) = link_data(jj).ME_connect;
        ii=ii+1;
    end
end

n_size1=length(ME);
ME_temp = ME; % mi serve la struttura originale ~= da ME_temp
if isempty(connection) % non ci sono link: ogni ME vivo � un frammento
    for i=n_size1:-1:1
        if ME_temp(i).GEOMETRY_DATA.mass==0
            ME_temp(i)=[];
        else
            FRAGMENTS=[FRAGMENTS, ME_temp(i)];
        end
    end
else % bisogna distinguere tra framemnti da pi� ME e frammenti da 1 ME
    connection_orig=connection;
    s=0;
    for i=n_size1:-1:1
        if ME_temp(i).GEOMETRY_DATA.mass==0
            ME_temp(i)=[];
        else
            [connected_orig,~] = find(or(ismember(connection_orig(:,1),ME_temp(i).object_ID),ismember(connection_orig(:,2),ME_temp(i).object_ID)));
            connected = find(or(ismember(connection(:,1),ME_temp(i).object_ID),ismember(connection(:,2),ME_temp(i).object_ID)));
            if ~isempty(connected_orig) % � un frammentone formato da pi�
                if ~isempty(connected) % � un frammentone formato da pi�
                    trovato = 0;
                    for jj=1:s
                        if any(ismember(nn{jj,1},reshape(connection(connected,:),1,[])))
                            nn{jj,1}=[nn{jj,1},reshape(connection(connected,:),1,[])];
                            nn{jj,1}=unique(nn{jj,1});
                            connection(connected,:)=[];
                            trovato = 1;
                            break;
                        end
                        %                 if (i == 1) && any(ismember(nn{jj,1},1))
                        %                     trovato = 1;
                        %                     break;
                        %                 end
                    end
                    if trovato == 0
                        s=s+1;
                        nn{s,1}=reshape(connection(connected,:),1,[]);
                        nn{s,1}=unique(nn{s,1});
                        connection(connected,:)=[];
                    end
                    % else isempty(connected): � un frammentone formato da pi�,
                    % ora non lo trovo pi�, ma prima c'era, � gi�
                    % considerato, non devo fare niente,neanche cancellare
                    % connection
                end
            else % isempty(connected_orig): l'ME � un frammento per conto suo
                s=s+1;
                nn{s,1}=[ME_temp(i).object_ID];
            end
        end
    end
    for jj = 1:size(nn,1)
        ME_fragments(jj)=ME(nn{jj,1}(1));
        ME_fragments(jj).GEOMETRY_DATA.c_hull=ME_fragments(jj).DYNAMICS_DATA.cm_coord'+ME_fragments(jj).GEOMETRY_DATA.c_hull;
        ME_fragments(jj).DYNAMICS_DATA.cm_coord = ME_fragments(jj).DYNAMICS_DATA.cm_coord*ME_fragments(jj).GEOMETRY_DATA.mass;
        ME_fragments(jj).DYNAMICS_DATA.vel = ME_fragments(jj).DYNAMICS_DATA.vel*ME_fragments(jj).GEOMETRY_DATA.mass;
        for kk=2:length(nn{jj,1})
            ME_fragments(jj).DYNAMICS_DATA.cm_coord = ME_fragments(jj).DYNAMICS_DATA.cm_coord + ME(nn{jj,1}(kk)).DYNAMICS_DATA.cm_coord*ME(nn{jj,1}(kk)).GEOMETRY_DATA.mass;
            ME_fragments(jj).DYNAMICS_DATA.vel = ME_fragments(jj).DYNAMICS_DATA.vel + ME(nn{jj,1}(kk)).DYNAMICS_DATA.vel*ME(nn{jj,1}(kk)).GEOMETRY_DATA.mass;
            ME_fragments(jj).GEOMETRY_DATA.mass=ME_fragments(jj).GEOMETRY_DATA.mass+ME(nn{jj,1}(kk)).GEOMETRY_DATA.mass;
            ME_fragments(jj).GEOMETRY_DATA.c_hull=[ME_fragments(jj).GEOMETRY_DATA.c_hull; ME(nn{jj,1}(kk)).DYNAMICS_DATA.cm_coord'+ ME(nn{jj,1}(kk)).GEOMETRY_DATA.c_hull];
        end
        ME_fragments(jj).DYNAMICS_DATA.cm_coord = ME_fragments(jj).DYNAMICS_DATA.cm_coord/ME_fragments(jj).GEOMETRY_DATA.mass;
        ME_fragments(jj).DYNAMICS_DATA.vel = ME_fragments(jj).DYNAMICS_DATA.vel/ME_fragments(jj).GEOMETRY_DATA.mass;
        ME_fragments(jj).GEOMETRY_DATA.c_hull=ME_fragments(jj).GEOMETRY_DATA.c_hull-ME_fragments(jj).DYNAMICS_DATA.cm_coord';
        box = boundingBox3d(ME_fragments(jj).GEOMETRY_DATA.c_hull);
        lx = abs(box(2)-box(1));
        ly = abs(box(4)-box(3));
        lz = abs(box(6)-box(5));
        ME_fragments(jj).GEOMETRY_DATA.dimensions=[lx ly lz];
        ME_fragments(jj).GEOMETRY_DATA.A_M_ratio = mean([lx*ly;lx*lz;ly*lz])/ME_fragments(jj).GEOMETRY_DATA.mass;
    end
    if ~isempty(ME_fragments)
        FRAGMENTS(n_size+1:n_size+size(nn,1))=ME_fragments;
    end
end
%% frammenti totali
n_size=length(FRAGMENTS);

for i = 1:n_size
    Lc_CST(i)=mean(FRAGMENTS(i).GEOMETRY_DATA.dimensions);
    Am_CST(i)=FRAGMENTS(i).GEOMETRY_DATA.A_M_ratio;
    vel_CST(i)=norm(FRAGMENTS(i).DYNAMICS_DATA.vel);
    mass_CST(i)=FRAGMENTS(i).GEOMETRY_DATA.mass;
end

s=sort(Lc_CST);

if n_size==1 %patch to avoid only one fragment case
    n_size=n_size+1;
end

N=n_size:-1:1;

% data for plot delta V
q_cm=[0 0 0]';
for i=1:n_size
    q_cm=q_cm+mass_CST(i)*vel_CST(:,i);
end
v_cm = q_cm/sum(mass_CST);
for i=1:n_size
    delta_v_CST(i)=norm(v_cm-vel_CST(:,i));
end

n_frags_bubbles=0;
if ~isempty(PROPAGATION_THRESHOLD)
    for ii = 1:length(BUBBLE)
        if BUBBLE(ii).GEOMETRY_DATA.mass~=0
            n_frags_bubbles=n_frags_bubbles+BUBBLE(ii).GEOMETRY_DATA.mass/(4/3*PROPAGATION_THRESHOLD.bubble_radius_th^3*pi()*MATERIAL_LIST(BUBBLE(ii).material_ID).density);
        end
    end
end

%% frammenti target LOFT
FRAGMENTS_target=[];

for i=1:n_size
    if FRAGMENTS(i).object_ID <= l_target
        FRAGMENTS_target=[FRAGMENTS_target;FRAGMENTS(i)];
    end
end

if ~isempty(FRAGMENTS_target)
    
    n_size_target=length(FRAGMENTS_target);
    
    for i = 1:n_size_target
        Lc_CST_target(i)=mean(FRAGMENTS_target(i).GEOMETRY_DATA.dimensions);
        Am_CST_target(i)=FRAGMENTS_target(i).GEOMETRY_DATA.A_M_ratio;
        vel_CST_target(i)=norm(FRAGMENTS_target(i).DYNAMICS_DATA.vel);
        mass_CST_target(i)=FRAGMENTS_target(i).GEOMETRY_DATA.mass;
    end
    
    s_target=sort(Lc_CST_target);
    
    if n_size_target==1 %patch to avoid only one fragment case
        n_size_target=n_size_target+1;
    end
    
    N_target=n_size_target:-1:1;
    
    % data for plot delta V
    q_cm_target=[0 0 0]';
    for i=1:n_size_target
        q_cm_target=q_cm_target+mass_CST_target(i)*vel_CST_target(:,i);
    end
    v_cm_target = q_cm_target/sum(mass_CST_target);
    for i=1:n_size_target
        delta_v_CST_target(i)=norm(v_cm_target-vel_CST_target(:,i));
    end
    
else
    s_target=[];
    N_target=[];
    delta_v_CST_target=[];
    
end

%% bubble target
n_frags_bubbles_target=0;

if ~isempty(BUBBLE)
    BUBBLE_target = [];
    for i=1:length(BUBBLE)
        if BUBBLE(i).object_ID <= l_target
            BUBBLE_target=[BUBBLE_target;BUBBLE(i)];
        end
    end
    if ~isempty(BUBBLE_target)
        for ii = 1:length(BUBBLE_target)
            n_frags_bubbles_target=n_frags_bubbles_target+BUBBLE_target(ii).GEOMETRY_DATA.mass/(4/3*PROPAGATION_THRESHOLD.bubble_radius_th^3*pi()*MATERIAL_LIST(BUBBLE_target(ii).material_ID).density);
        end
    end
end

%% frammenti impactor

FRAGMENTS_impactor=[];

for i=1:n_size
    if FRAGMENTS(i).object_ID > l_target
        FRAGMENTS_impactor=[FRAGMENTS_impactor;FRAGMENTS(i)];
    end
end

if ~isempty(FRAGMENTS_impactor)
    
    n_size_impactor=length(FRAGMENTS_impactor);
    
    for i = 1:n_size_impactor
        Lc_CST_impactor(i)=mean(FRAGMENTS_impactor(i).GEOMETRY_DATA.dimensions);
        Am_CST_impactor(i)=FRAGMENTS_impactor(i).GEOMETRY_DATA.A_M_ratio;
        vel_CST_impactor(i)=norm(FRAGMENTS_impactor(i).DYNAMICS_DATA.vel);
        mass_CST_impactor(i)=FRAGMENTS_impactor(i).GEOMETRY_DATA.mass;
    end
    
    s_impactor=sort(Lc_CST_impactor);
    
    if n_size_impactor==1 %patch to avoid only one fragment case
        n_size_impactor=n_size_impactor+1;
    end
    
    N_impactor=n_size_impactor:-1:1;
    
    % data for plot delta V
    q_cm_impactor=[0 0 0]';
    for i=1:n_size_impactor
        q_cm_impactor=q_cm_impactor+mass_CST_impactor(i)*vel_CST_impactor(:,i);
    end
    v_cm_impactor = q_cm_impactor/sum(mass_CST_impactor);
    for i=1:n_size_impactor
        delta_v_CST_impactor(i)=norm(v_cm_impactor-vel_CST_impactor(:,i));
    end
    
else
    
    s_impactor=[];
    N_impactor=[];
    delta_v_CST_impactor=[];
    
end

%% bubble impactor
n_frags_bubbles_impactor=0;

if ~isempty(BUBBLE)
    BUBBLE_impactor = [];
    for i=1:length(BUBBLE)
        if BUBBLE(i).object_ID > l_target
            BUBBLE_impactor=[BUBBLE_impactor;BUBBLE(i)];
        end
    end
    if ~isempty(BUBBLE_impactor)
        for ii = 1:length(BUBBLE_impactor)
            n_frags_bubbles_impactor=n_frags_bubbles_impactor+BUBBLE_impactor(ii).GEOMETRY_DATA.mass/(4/3*PROPAGATION_THRESHOLD.bubble_radius_th^3*pi()*MATERIAL_LIST(BUBBLE_impactor(ii).material_ID).density);
        end
    end
end

%% output
s = [s;...
     s_target, zeros(1,length(s)-length(s_target));...
     s_impactor, zeros(1,length(s)-length(s_impactor))];

N = [N;...
     N_target, zeros(1,length(N)-length(N_target));...
     N_impactor, zeros(1,length(N)-length(N_impactor))];
 
Am_CST = [Am_CST;...
          Am_CST_target, zeros(1,length(Am_CST)-length(Am_CST_target));...
          Am_CST_impactor, zeros(1,length(Am_CST)-length(Am_CST_impactor))];
      
delta_v_CST = [delta_v_CST;...
               delta_v_CST_target, zeros(1,length(delta_v_CST)-length(delta_v_CST_target));...
               delta_v_CST_impactor, zeros(1,length(delta_v_CST)-length(delta_v_CST_impactor))];
           
n_frags_bubbles = [n_frags_bubbles;...
                   n_frags_bubbles_target, zeros(1,length(n_frags_bubbles)-length(n_frags_bubbles_target));...
                   n_frags_bubbles_impactor, zeros(1,length(n_frags_bubbles)-length(n_frags_bubbles_impactor))];
