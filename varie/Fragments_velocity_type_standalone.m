%> @file    Fragments_velocity_type_standalone.m
%> @brief   Standalone plot of fragments distributions
%> @author  Lorenzo Olivieri(lorenzo.olivieri@unipd.it)
%> @date    02 August 2018

%> Copyright 2017 CISAS - UNIVERSITY OF PADOVA
tic
if ~exist('FRAGMENTS','var')
    disp('Load the desired step before launching this file')
else
    disp('BLUE = ME1, GREEN = ME2, Red = ME3, BLACK = Other bodies')
    figure('Position',[100 300 1400 600])
    subplot(1,4,1)
    for i=1:length(FRAGMENTS)
        if(FRAGMENTS(i).object_ID==1)
            semilogy(i,norm(FRAGMENTS(i).DYNAMICS_DATA.vel),'bo')
        elseif(FRAGMENTS(i).object_ID==3)
            semilogy(i,norm(FRAGMENTS(i).DYNAMICS_DATA.vel),'rx')
        elseif(FRAGMENTS(i).object_ID==2)
            semilogy(i,norm(FRAGMENTS(i).DYNAMICS_DATA.vel),'gs')
        else
            semilogy(i,norm(FRAGMENTS(i).DYNAMICS_DATA.vel),'k+')
        end
        %     plot(i,norm(FRAGMENTS(i).DYNAMICS_DATA.vel),color)
        hold on
    end
    xlim([0 length(FRAGMENTS)])
    ylabel('Velocity, m/s')
    subplot(1,4,2)
    for i=1:length(FRAGMENTS)
        if(FRAGMENTS(i).object_ID==1)
            plot(i,atan2(FRAGMENTS(i).DYNAMICS_DATA.vel(3),norm(FRAGMENTS(i).DYNAMICS_DATA.vel(1:2)))*180/pi,'bo')
        elseif(FRAGMENTS(i).object_ID==3)
            plot(i,atan2(FRAGMENTS(i).DYNAMICS_DATA.vel(3),norm(FRAGMENTS(i).DYNAMICS_DATA.vel(1:2)))*180/pi,'rx')
        elseif(FRAGMENTS(i).object_ID==2)
            plot(i,atan2(FRAGMENTS(i).DYNAMICS_DATA.vel(3),norm(FRAGMENTS(i).DYNAMICS_DATA.vel(1:2)))*180/pi,'gs')
        else
            plot(i,atan2(FRAGMENTS(i).DYNAMICS_DATA.vel(3),norm(FRAGMENTS(i).DYNAMICS_DATA.vel(1:2)))*180/pi,'k+')
        end
        %     plot(i,norm(FRAGMENTS(i).DYNAMICS_DATA.vel),color)
        hold on
    end
    xlim([0 length(FRAGMENTS)])
    ylabel('Velocity direction wrt xy plane, deg')
    subplot(1,4,3)
    for i=1:length(FRAGMENTS)
        if(FRAGMENTS(i).object_ID==1) && (FRAGMENTS(i).GEOMETRY_DATA.mass ~= 0)
            semilogy(i,(FRAGMENTS(i).GEOMETRY_DATA.mass),'bo')
        elseif(FRAGMENTS(i).object_ID==3) && (FRAGMENTS(i).GEOMETRY_DATA.mass ~= 0)
            semilogy(i,(FRAGMENTS(i).GEOMETRY_DATA.mass),'rx')
        elseif(FRAGMENTS(i).object_ID==2) && (FRAGMENTS(i).GEOMETRY_DATA.mass ~= 0)
            semilogy(i,(FRAGMENTS(i).GEOMETRY_DATA.mass),'gs')
        else
            semilogy(i,(FRAGMENTS(i).GEOMETRY_DATA.mass),'k+')
        end
        %     plot(i,norm(FRAGMENTS(i).DYNAMICS_DATA.vel),color)
        hold on
    end
    xlim([0 length(FRAGMENTS)])
    ylabel('Mass, kg')
    subplot(1,4,4)
    for i=1:length(FRAGMENTS)
        if(FRAGMENTS(i).object_ID==1)
            semilogy(i,0.5*FRAGMENTS(i).GEOMETRY_DATA.mass*norm(FRAGMENTS(i).DYNAMICS_DATA.vel)^2,'bo')
        elseif(FRAGMENTS(i).object_ID==3)
            semilogy(i,0.5*FRAGMENTS(i).GEOMETRY_DATA.mass*norm(FRAGMENTS(i).DYNAMICS_DATA.vel)^2,'rx')
        elseif(FRAGMENTS(i).object_ID==2)
            semilogy(i,0.5*FRAGMENTS(i).GEOMETRY_DATA.mass*norm(FRAGMENTS(i).DYNAMICS_DATA.vel)^2,'gs')
        else
            semilogy(i,0.5*FRAGMENTS(i).GEOMETRY_DATA.mass*norm(FRAGMENTS(i).DYNAMICS_DATA.vel)^2,'k+')
        end
        %     plot(i,norm(FRAGMENTS(i).DYNAMICS_DATA.vel),color)
        hold on
    end
    xlim([0 length(FRAGMENTS)])
%     Ek3=ME(3).GEOMETRY_DATA.mass0*norm(ME(3).DYNAMICS_INITIAL_DATA.vel0);
%     plot(length(FRAGMENTS)+1,Ek3,'k+')
    ylabel('Kinetic Energy, J')
end
toc