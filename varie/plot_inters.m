function plot_inters(c_hull,ME_int,points_sys,caso)

figure
for zz=1:length(c_hull)-1
    hold on
    if ismember(zz,ME_int)
        drawMesh(c_hull(zz).c_hull_global,minConvexHull(c_hull(zz).c_hull_global),'facecolor','r','facealpha',0.8)
    else
        drawMesh(c_hull(zz).c_hull_global,minConvexHull(c_hull(zz).c_hull_global),'facecolor','c','facealpha',0.3)
    end
end
hold on
drawMesh(points_sys,minConvexHull(points_sys),'facecolor','g','facealpha',0.3)
hold on
drawMesh(c_hull(length(c_hull)).c_hull_global,minConvexHull(c_hull(length(c_hull)).c_hull_global),'facecolor','b','facealpha',1)
hold on
if isnumeric(caso) % LOFT
    if ismember(caso,[2,5])
        view(45,30)
    else
        view(225,30)
    end
    xlim([-5 5])
    ylim([-5 5])
    zlim([-6 4])
    out_fig = ['Configuration_LOFT_',num2str(caso_loft)];
else % ASI
    %     if ismember(caso,[2,5])
    %         view(45,30)
    %     else
    view(45,30)
    %     end
    xlim([-0.5 0.5])
    ylim([-0.5 1.5])
    zlim([-0.4 0.4])
    axis equal
    out_fig = ['Configuration_',caso];
end
saveas(gcf,out_fig,'fig')
saveas(gcf,out_fig,'png')
