Area = 0;

for i=1:6
    a(1)=ME(i).GEOMETRY_DATA.dimensions(1)*ME(i).GEOMETRY_DATA.dimensions(2);
    a(2)=ME(i).GEOMETRY_DATA.dimensions(1)*ME(i).GEOMETRY_DATA.dimensions(3);
    a(3)=ME(i).GEOMETRY_DATA.dimensions(2)*ME(i).GEOMETRY_DATA.dimensions(3);
    Area = Area + 2*sum(a) - min(a);
    clear a
end

for i=7:7
    a(1)=ME(i).GEOMETRY_DATA.dimensions(1)*ME(i).GEOMETRY_DATA.dimensions(2);
    a(2)=ME(i).GEOMETRY_DATA.dimensions(1)*ME(i).GEOMETRY_DATA.dimensions(3);
    a(3)=ME(i).GEOMETRY_DATA.dimensions(2)*ME(i).GEOMETRY_DATA.dimensions(3);
    Area = Area + 2*sum(a) - max(a);
    b = max(a);
    clear a
end

for i=8:13
    a(1)=ME(i).GEOMETRY_DATA.dimensions(1)*ME(i).GEOMETRY_DATA.dimensions(2);
    a(2)=ME(i).GEOMETRY_DATA.dimensions(1)*ME(i).GEOMETRY_DATA.dimensions(3);
    a(3)=ME(i).GEOMETRY_DATA.dimensions(2)*ME(i).GEOMETRY_DATA.dimensions(3);
    Area = Area + max(a);
    clear a
end

for i=15:20
    a(1)=ME(i).GEOMETRY_DATA.dimensions(1)*ME(i).GEOMETRY_DATA.dimensions(2);
    a(2)=ME(i).GEOMETRY_DATA.dimensions(1)*ME(i).GEOMETRY_DATA.dimensions(3);
    a(3)=ME(i).GEOMETRY_DATA.dimensions(2)*ME(i).GEOMETRY_DATA.dimensions(3);
    Area = Area + max(a);
    clear a
end

for i=21:28
    a(1)=ME(i).GEOMETRY_DATA.dimensions(1)*ME(i).GEOMETRY_DATA.dimensions(2);
    a(2)=ME(i).GEOMETRY_DATA.dimensions(1)*ME(i).GEOMETRY_DATA.dimensions(3);
    a(3)=ME(i).GEOMETRY_DATA.dimensions(2)*ME(i).GEOMETRY_DATA.dimensions(3);
    Area = Area + sum(a) + min(a) + max(a);
    if i==21
        c = sum(a) - min(a) - max(a);
    end
    clear a
end

for i=43:43
    Area = Area + pi*ME(i).GEOMETRY_DATA.dimensions(1)^2 - b - c;
    clear a
end

for i=44:44
    Area = Area + pi*ME(i).GEOMETRY_DATA.dimensions(1)^2 - 3/2*sqrt(3)*median(ME(8).GEOMETRY_DATA.dimensions)^2;
    clear a
end

for i=45:45
    Area = Area + pi*ME(i).GEOMETRY_DATA.dimensions(1)^2;
    clear a
end

A_I_su_A_T = sqrt(0.4^2*2+0.3^2)/Area;