% Standalone for fragments size distribution
clear all
close all
clc

global MATERIAL_LIST

%% Load data
% name_file_result_baseline='E:\AO_8507Catastrophic\CST_integration\CST3_21_11_2018_risultati_RILEVANTI\Results\LOFT1_Baseline_BEFORE_correction_velocity_model\data\step_2000.mat';
name_file_result_baseline='N:\CST3_baseline_22112018\Results\LOFT1_20181122T174013\data\step_113.mat';
name_file_result_EMI='E:\AO_8507Catastrophic\CST_integration\CST3_21_11_2018_risultati_RILEVANTI\Results\LOFT1_EMI_config_BEFORE_correction_velocity_model\data\step_2000.mat';

LOFT1=load('22_PDFsam_2018CSID_MSchimmerohn_SCDisrupt_crop_LOFT1_LOG.txt');
LOFT2=load('22_PDFsam_2018CSID_MSchimmerohn_SCDisrupt_crop_LOFT2_LOG.txt');
LOFT3=load('22_PDFsam_2018CSID_MSchimmerohn_SCDisrupt_crop_LOFT3_LOG.txt');
LOFT4=load('22_PDFsam_2018CSID_MSchimmerohn_SCDisrupt_crop_LOFT4_LOG.txt');
LOFT5=load('22_PDFsam_2018CSID_MSchimmerohn_SCDisrupt_crop_LOFT5_LOG.txt');
LOFT6=load('22_PDFsam_2018CSID_MSchimmerohn_SCDisrupt_crop_LOFT6_LOG.txt');

%% Settings

colorOrder =  [ ...
    0            0            1       ;...% 1 BLUE   
    1            0            0       ;...% 2 RED
    0            1            0       ;...% 3 GREEN (pale)
    0            1            1       ;...% 4 CYAN
    1            0            1       ;...% 5 MAGENTA (pale)
    0.6          0.5          0.4     ;  % 6 BROWN (dark)
    0            0            0       ;...% 7 BLACK
    1            1            0       ;...% 8 YELLOW (pale)
    0            0.75         0.75    ;...% 9 TURQUOISE
    0            0.5          0       ;...% 10 GREEN (dark)
    0.75         0.75         0       ;...% 11 YELLOW (dark)
    1            0.50         0.25    ;...% 12 ORANGE
    0.75         0            0.75    ;...% 13 MAGENTA (dark)
    0.7          0.7          0.7     ;...% 14 GREY
    0.8          0.7          0.6     ;...% 15 BROWN (pale)
    0.4          0.7          0.6     ;...
    0.4          0          0.6     ;...
    0.4          0.6          0     ;...
    0.6          0.5          0.4 ];  % 15 BROWN (dark)

%% processing baseline link scenario data

load(name_file_result_baseline,'FRAGMENTS','ME','BUBBLE','PROPAGATION_THRESHOLD')
name_mat=[name_file_result_baseline(1:strfind(name_file_result_baseline,'\Results')),'set_up',filesep,'material_list.m'];
run(name_mat)

n_size=length(FRAGMENTS);
for i=n_size:-1:1
    if FRAGMENTS(i).GEOMETRY_DATA.mass==0
        FRAGMENTS(i)=[];
    else
        box = boundingBox3d(FRAGMENTS(i).GEOMETRY_DATA.c_hull);
        lx = abs(box(2)-box(1));
        ly = abs(box(4)-box(3));
        lz = abs(box(6)-box(5));
        FRAGMENTS(i).GEOMETRY_DATA.dimensions=[lx ly lz];
        x(i)=max(FRAGMENTS(i).GEOMETRY_DATA.dimensions);
    end
end

mass_imp=0;
for i = 52:length(ME)
    mass_imp = mass_imp + ME(i).GEOMETRY_DATA.mass0;
    q_imp = ME(i).GEOMETRY_DATA.mass0*ME(i).DYNAMICS_INITIAL_DATA.vel0;
end
v_p = 1/mass_imp*q_imp/1000;
% clear ME

FRAGMENTS_baseline_link=FRAGMENTS;
clear FRAGMENTS
n_size=length(FRAGMENTS_baseline_link);
for i=n_size:-1:1
    if FRAGMENTS_baseline_link(i).GEOMETRY_DATA.mass==0
        FRAGMENTS_baseline_link(i)=[];
    end
end
n_size=length(FRAGMENTS_baseline_link);

for i = 2:n_size
    Lc_CST_baseline(i-1)=mean(FRAGMENTS_baseline_link(i).GEOMETRY_DATA.dimensions);
    Am_CST_baseline(i-1)=FRAGMENTS_baseline_link(i).GEOMETRY_DATA.A_M_ratio;
    vel_CST_baseline(i-1)=norm(FRAGMENTS_baseline_link(i).DYNAMICS_DATA.vel);
    mass_CST_baseline(i-1)=FRAGMENTS_baseline_link(i).GEOMETRY_DATA.mass;
end

s_baseline=sort(Lc_CST_baseline);

if n_size==1 %patch to avoid only one fragment case
    n_size=n_size+1;
end

N_baseline=n_size-1:-1:1;

% data for plot delta V
q_cm=[0 0 0]';
for i=2:n_size
    q_cm=q_cm+mass_CST_baseline(i-1)*vel_CST_baseline(:,i-1);
end
v_cm = q_cm/sum(mass_CST_baseline);
for i=2:n_size
    delta_v_CST_baseline(i-1)=norm(v_cm-vel_CST_baseline(:,i-1));
end

clear v_cm q_cm n_size

%% processing EMI link scenario data

load(name_file_result_EMI,'FRAGMENTS')

n_size=length(FRAGMENTS);
for i=n_size:-1:1
    if FRAGMENTS(i).GEOMETRY_DATA.mass==0
        FRAGMENTS(i)=[];
    else
        box = boundingBox3d(FRAGMENTS(i).GEOMETRY_DATA.c_hull);
        lx = abs(box(2)-box(1));
        ly = abs(box(4)-box(3));
        lz = abs(box(6)-box(5));
        FRAGMENTS(i).GEOMETRY_DATA.dimensions=[lx ly lz];
        x(i)=max(FRAGMENTS(i).GEOMETRY_DATA.dimensions);
    end
end
FRAGMENTS_EMI_link=FRAGMENTS;
clear FRAGMENTS

n_size=length(FRAGMENTS_EMI_link);
for i=n_size:-1:1
    if FRAGMENTS_EMI_link(i).GEOMETRY_DATA.mass==0
        FRAGMENTS_EMI_link(i)=[];
    end
end
n_size=length(FRAGMENTS_EMI_link);

for i = 2:n_size
    Lc_CST_EMI(i-1)=mean(FRAGMENTS_EMI_link(i).GEOMETRY_DATA.dimensions);
    Am_CST_EMI(i-1)=FRAGMENTS_EMI_link(i).GEOMETRY_DATA.A_M_ratio;
    vel_CST_EMI(i-1)=norm(FRAGMENTS_EMI_link(i).DYNAMICS_DATA.vel);
    mass_CST_EMI(i-1)=FRAGMENTS_EMI_link(i).GEOMETRY_DATA.mass;
end

s_EMI=sort(Lc_CST_EMI);

if n_size==1 %patch to avoid only one fragment case
    n_size=n_size+1;
end

N_EMI=n_size-1:-1:1;

% data for plot delta V
q_cm=[0 0 0]';
for i=2:n_size
    q_cm=q_cm+mass_CST_EMI(i-1)*vel_CST_EMI(:,i-1);
end
v_cm = q_cm/sum(mass_CST_EMI);
for i=2:n_size
    delta_v_CST_EMI(i-1)=norm(v_cm-vel_CST_EMI(:,i-1));
end

clear v_cm q_cm n_size

%% plot LOFT cases
Lc_NASA=min(LOFT6(:,1)):(max(LOFT6(:,1))-min(LOFT6(:,1)))/100:max(LOFT6(:,1));
NASA_SBM_01_sub = 0.1*(0.1*11)^0.75*Lc_NASA.^(-1.71);
NASA_SBM_1_sub = 0.1*(1*11)^0.75*Lc_NASA.^(-1.71);
NASA_SBM_10_sub = 0.1*(10*11)^0.75*Lc_NASA.^(-1.71);

m_tot=0;
for ii = 1:length(ME)
    m_tot=m_tot+ME(ii).GEOMETRY_DATA.mass0;
end
NASA_SBM_01_cat = 0.1*(m_tot)^0.75*Lc_NASA.^(-1.71);
NASA_SBM_1_cat = 0.1*(m_tot+0.9)^0.75*Lc_NASA.^(-1.71);
NASA_SBM_10_cat = 0.1*(m_tot+9.9)^0.75*Lc_NASA.^(-1.71);

figure
loglog(LOFT1(:,1),LOFT1(:,2),'MarkerFaceColor',colorOrder(9,:),'Marker','*','LineStyle','none');
hold on
loglog(LOFT2(:,1),LOFT2(:,2),'MarkerFaceColor',colorOrder(12,:),'Marker','+','LineStyle','none');
hold on
loglog(LOFT3(:,1),LOFT3(:,2),'MarkerFaceColor',colorOrder(11,:),'Marker','o','LineStyle','none');
hold on
loglog(LOFT4(:,1),LOFT4(:,2),'MarkerFaceColor',colorOrder(5,:),'Marker','^','LineStyle','none');
hold on
loglog(LOFT5(:,1),LOFT5(:,2),'MarkerFaceColor',colorOrder(10,:),'Marker','<','LineStyle','none');
hold on
loglog(LOFT6(:,1),LOFT6(:,2),'MarkerFaceColor',colorOrder(4,:),'Marker','x','LineStyle','none');
set(gca,'FontSize',20,'FontWeight','bold')
loglog(Lc_NASA,NASA_SBM_01_sub,'--r','LineWidth',2)
loglog(Lc_NASA,NASA_SBM_1_sub,':r','LineWidth',2)
loglog(Lc_NASA,NASA_SBM_10_sub,'-r','LineWidth',2)
% loglog(Lc_NASA,NASA_SBM_01_cat,'-k','LineWidth',2)
% loglog(Lc_NASA,NASA_SBM_1_cat,'-k','LineWidth',2)
loglog(Lc_NASA,NASA_SBM_10_cat,'-k','LineWidth',2)

legend('EMI-LOFT1','EMI-LOFT2','EMI-LOFT3','EMI-LOFT4','EMI-LOFT5','EMI-LOFT6','NASA - SubCat-SBM 0.1 kg','NASA - SubCat-SBM 1 kg','NASA - SubCat-SBM 10 kg','NASA - Cat-SBM');
xlabel('L_c [m]')
ylabel('Cumulative number N')
hold on
grid on
set(gca,'XLim',[2e-03,1e0])
set(gca,'YLim',[1e0,1e6])

%% plot finale Lc

%{r
Lc_NASA=min(Lc_CST_EMI):(max(Lc_CST_EMI)-min(Lc_CST_EMI))/100:max(Lc_CST_EMI);
NASA_SBM_sub = 0.1*(mass_imp*norm(v_p))^0.75*Lc_NASA.^(-1.71);
m_tot=0;
for ii = 1:length(ME)
    m_tot=m_tot+ME(ii).GEOMETRY_DATA.mass0;
end
NASA_SBM_cat = 0.1*(m_tot)^0.75*Lc_NASA.^(-1.71);

n_frags_bubbles=0;
for ii = 1:length(BUBBLE)
    n_frags_bubbles=n_frags_bubbles+BUBBLE(ii).GEOMETRY_DATA.mass/(4/3*PROPAGATION_THRESHOLD.bubble_radius_th^3*pi()*MATERIAL_LIST(BUBBLE(ii).material_ID).density);
end

h = figure('Position', [45   129   906   553],'Visible', 'on');
loglog(s_baseline,[N_baseline(1)+n_frags_bubbles,N_baseline(2:end)],'+g','LineWidth',2)
grid on
hold on
loglog(s_EMI,[N_EMI(1)+n_frags_bubbles,N_EMI(2:end)],'MarkerFaceColor',colorOrder(9,:),'Marker','*','LineStyle','none')
loglog(LOFT1(:,1),LOFT1(:,2),'MarkerFaceColor',colorOrder(5,:),'Marker','^','LineStyle','none');
hold on
loglog(Lc_NASA,NASA_SBM_sub,'-r','LineWidth',2)
hold on
loglog(Lc_NASA,NASA_SBM_cat,'--r','LineWidth',2)
hold on
set(gca,'FontSize',20,'FontWeight','bold')
title('Comparison CST-EMI results LOFT1')
xlabel('L_c [m]')
ylabel('Cumulative Number N')
legend('CST LOFT1 baseline','CST LOFT1 in EMI configuration','EMI Results','NASA - SubCat-SBM','NASA - Cat-SBM')
set(gca,'XLim',[3e-03,1e0])
set(gca,'YLim',[1e0,1e6])
%}

%% plot finale A su M

%{s
clear DAmSOC DAmSOC_matlab DAmSOC_NASA
lambda_c_min=log10(min([s_baseline s_EMI]));
lambda_c_max=log10(max([s_baseline s_EMI]));
lambda_c=lambda_c_min:(lambda_c_max-lambda_c_min)/30:lambda_c_max;
csi_min=log10(min([Am_CST_baseline Am_CST_EMI]));
csi_max=log10(max([Am_CST_baseline Am_CST_EMI]));
n=length(lambda_c);
csi_NASA=csi_min:(csi_max-csi_min)/(n-1):csi_max;

for i=1:1:n
        if  lambda_c(i)<=-3.5
            muSOC=-0.3;
            sigmaSOC=0.2;
            DAmSOC(i,:)=(1/(sigmaSOC*(2*pi)^0.5))*exp(-((csi_NASA-muSOC).^2)/(2*sigmaSOC^2));
            pd = makedist('Normal','mu',muSOC,'sigma',sigmaSOC);
            DAmSOC_matlab(i,:) = pdf(pd,csi_NASA);
        elseif lambda_c(i)>-3.5 && lambda_c(i)<=-1.75
            muSOC=-0.3;
            sigmaSOC=0.2+0.1333*(lambda_c(i)+3.5);
            DAmSOC(i,:)=(1/(sigmaSOC*(2*pi)^0.5))*exp(-((csi_NASA-muSOC).^2)/(2*sigmaSOC^2));
            pd = makedist('Normal','mu',muSOC,'sigma',sigmaSOC);
            DAmSOC_matlab(i,:) = pdf(pd,csi_NASA);
        elseif lambda_c(i)>-1.75 && lambda_c(i)<-1.25
            muSOC=-0.3-1.4*(lambda_c(i)+1.75);
            sigmaSOC=0.2+0.1333*(lambda_c(i)+3.5);
            DAmSOC(i,:)=(1/(sigmaSOC*(2*pi)^0.5))*exp(-((csi_NASA-muSOC).^2)/(2*sigmaSOC^2));
            pd = makedist('Normal','mu',muSOC,'sigma',sigmaSOC);
            DAmSOC_matlab(i,:) = pdf(pd,csi_NASA);
        elseif lambda_c(i)>=-1.25
            muSOC=-1.0;
            sigmaSOC=0.2+0.1333*(lambda_c(i)+3.5);
            DAmSOC(i,:)=(1/(sigmaSOC*(2*pi)^0.5))*exp(-((csi_NASA-muSOC).^2)/(2*sigmaSOC^2));
            pd = makedist('Normal','mu',muSOC,'sigma',sigmaSOC);
            DAmSOC_matlab(i,:) = pdf(pd,csi_NASA);
        end
end

for k=1:1:size(DAmSOC,2)
    DAmSOC_NASA(k)=sum(DAmSOC(:,k));
    DAmSOC_matlab_NASA(k) = sum(DAmSOC_matlab(:,k));
end

h = figure('Position', [604          45        1010         500],'Visible', 'on');
histogram(log10(Am_CST_baseline),'Normalization','probability','BinWidth',0.01) 
grid on
hold on
histogram(log10(Am_CST_EMI),'Normalization','probability','BinWidth',0.01) 
hold on
plot(csi_NASA,DAmSOC_NASA./sum(DAmSOC_NASA),'b','LineWidth',2)
hold on
title('Comparison among PDFs')
set(gca,'FontSize',20,'FontWeight','bold')
xlabel('log_{10}(A/m) [m^{2}/kg]')
ylabel('CN(A/m)')
legend('CST LOFT1 baseline','CST LOFT1 in EMI configuration','NASA - SBM')
%}

%% plot finale of Delta_v

%{
clear DdvCOLL_NASA DdvCOLL_NASA_1

muEXP=0.9*csi_NASA+2.9;
sigmaEXP=0.4;
delta_v_lim=[0 3];
delta_v_NASA=delta_v_lim(1):abs(delta_v_lim(1)-delta_v_lim(2))/(n-1):delta_v_lim(2);

for i=1:1:n
    DdvCOLL_NASA(:,i)=(1/(sigmaEXP*(2*pi)^0.5))*exp(-((delta_v_NASA-muEXP(i)).^2)/(2*sigmaEXP^2));
end

for k=1:1:size(DdvCOLL_NASA,1)
    DdvCOLL_NASA_1(k)=sum(DdvCOLL_NASA(:,k));
end

figure
histogram(delta_v_CST_baseline,'Normalization','probability','BinWidth',1)
grid on
hold on
histogram(delta_v_CST_continuum,'Normalization','probability','BinWidth',1)
hold on
histogram(delta_v_CST_bolted,'Normalization','probability','BinWidth',1)
hold on
histogram(delta_v_CST_no_links,'Normalization','probability','BinWidth',1)
hold on
% histogram(delta_v_CST_weak_links,'Normalization','probability','BinWidth',1)
% hold on
% plot(delta_v_NASA,DdvCOLL_NASA_1./sum(DdvCOLL_NASA_1),'b','LineWidth',2)
title('Comparison among PDFs')
xlabel('\DeltaV [m/s] ')
ylabel('Relative Number of Fragments per \DeltaV bin')
legend('CST baseline','CST (all continuum links)','CST (all bolted links)','CST (no links)','CST (all weak links)','NASA SBM')
xlim([0 60000])
%}
