% Standalone for fragments size distribution
clear all
close all
clc

%% Load Data CST
% name_file_result_LOFT1='N:\CST3\Results\LOFT1_Baseline_BEFORE_correction_v_loss\data\step_113.mat';
name_file_result_LOFT1='N:\CST3\Results\LOFT1_Baseline_AFTER_correction_v_loss\data\step_113.mat';
% name_file_result_LOFT1_all_continuum='N:\CST3\Results\LOFT1_Baseline_BEFORE_correction_v_loss_all_CONTINUUM_links\data\step_113.mat';
name_file_result_LOFT1_all_continuum='N:\CST3\Results\LOFT1_Baseline_AFTER_correction_v_loss_all_CONTINUUM_links\data\step_113.mat';
% name_file_result_LOFT1_no_links='N:\CST3\Results\LOFT1_Baseline_BEFORE_correction_v_loss_NO_links\data\step_113.mat';
name_file_result_LOFT1_no_links='N:\CST3\Results\LOFT1_Baseline_AFTER_correction_v_loss_NO_links\data\step_113.mat';
% name_file_result_LOFT1_EMI_config='N:\CST3\Results\LOFT1_EMI_config_BEFORE_correction_v_loss\data\step_230.mat';
name_file_result_LOFT1_EMI_config='N:\CST3\Results\LOFT1_EMI_config_AFTER_correction_v_loss\data\step_230.mat';
% name_file_result_LOFT2='N:\CST3\Results\LOFT2_Baseline_BEFORE_correction_v_loss\data\step_318.mat';
name_file_result_LOFT2='N:\CST3\Results\LOFT2_Baseline_AFTER_correction_v_loss\data\step_318.mat';
% name_file_result_LOFT3='N:\CST3\Results\LOFT3_Baseline_BEFORE_correction_v_loss\data\step_342.mat';
name_file_result_LOFT3='N:\CST3\Results\LOFT3_Baseline_AFTER_correction_v_loss\data\step_293.mat';
% name_file_result_LOFT4='N:\CST3\Results\LOFT4_Baseline_BEFORE_correction_v_loss\data\step_267.mat';
name_file_result_LOFT4='N:\CST3\Results\LOFT4_Baseline_AFTER_correction_v_loss\data\step_267.mat';
% name_file_result_LOFT5='N:\CST3\Results\LOFT5_Baseline_BEFORE_correction_v_loss\data\step_454.mat';
name_file_result_LOFT5='N:\CST3\Results\LOFT5_Baseline_AFTER_correction_v_loss\data\step_454.mat';
name_file_result_LOFT6='N:\CST3\Results\LOFT6_Baseline\data\step_1281.mat';
% name_file_result_LOFT6_all_continuum='N:\CST3\Results\LOFT6_Continuum_OLD\data\step_1252.mat';
name_file_result_LOFT6_all_continuum='N:\CST3\Results\LOFT6_Continuum_NEW\data\step_1252.mat';
name_file_result_LOFT6_no_links='N:\CST3\Results\LOFT6_NO_links\data\step_1791.mat';
% name_file_result_LOFT6_c_mat2='N:\CST3\Results\LOFT6_Baseline_c_mat2_OLD\data\step_1739.mat';
name_file_result_LOFT6_c_mat2 ='N:\CST3\Results\LOFT6_Baseline_c_mat2_NEW\data\step_1739.mat';

%% Load Data EMI
LOFT1=load('22_PDFsam_2018CSID_MSchimmerohn_SCDisrupt_crop_LOFT1_LOG.txt');
LOFT2=load('22_PDFsam_2018CSID_MSchimmerohn_SCDisrupt_crop_LOFT2_LOG.txt');
LOFT3=load('22_PDFsam_2018CSID_MSchimmerohn_SCDisrupt_crop_LOFT3_LOG.txt');
LOFT4=load('22_PDFsam_2018CSID_MSchimmerohn_SCDisrupt_crop_LOFT4_LOG.txt');
LOFT5=load('22_PDFsam_2018CSID_MSchimmerohn_SCDisrupt_crop_LOFT5_LOG.txt');
LOFT6=load('22_PDFsam_2018CSID_MSchimmerohn_SCDisrupt_crop_LOFT6_LOG.txt');

%% processing LOFT1 scenario data
load(name_file_result_LOFT1,'FRAGMENTS','ME','BUBBLE','PROPAGATION_THRESHOLD','link_data','tf','RunTime')
name_mat=[name_file_result_LOFT1(1:strfind(name_file_result_LOFT1,'Results')-1),filesep,'set_up',filesep,'material_list.m'];
run(name_mat)

mass_imp_1=0;
mass_LOFT=0;
q_imp_1 = [0 0 0]';

for i = 1:51
    mass_LOFT = mass_LOFT + ME(i).GEOMETRY_DATA.mass0;
end

for i = 52:length(ME)
    mass_imp_1 = mass_imp_1 + ME(i).GEOMETRY_DATA.mass0;
    q_imp_1 = q_imp_1 + ME(i).GEOMETRY_DATA.mass0*ME(i).DYNAMICS_INITIAL_DATA.vel0;
end
v_p_1 = 1/mass_imp_1*q_imp_1/1000;

[n_frags_bubbles_LOFT1,s_LOFT1,N_LOFT1,Am_CST_LOFT1,delta_v_CST_LOFT1]=postprocess_data(FRAGMENTS,ME,BUBBLE,PROPAGATION_THRESHOLD,link_data);

tf_LOFT1 = sprintf('%25.4f',tf);
if exist('RunTime','var')
    cputime_LOFT1 = sprintf('%20.2f',RunTime/60);
else
    cputime_LOFT1 = '                   -';
end
final_step_LOFT1 = sprintf('%24d',str2double(name_file_result_LOFT1(strfind(name_file_result_LOFT1,'step_')+5:end-4)));
EMR_LOFT1 = sprintf('%5.2f',1/2*mass_imp_1*norm(v_p_1*1000)^2/(mass_imp_1+mass_LOFT)/1000);
clear FRAGMENTS ME BUBBLE PROPAGATION_THRESHOLD link_data tf RunTime

%% processing LOFT1 all continuum scenario data

load(name_file_result_LOFT1_all_continuum,'FRAGMENTS','ME','BUBBLE','PROPAGATION_THRESHOLD','link_data','tf','RunTime')

[n_frags_bubbles_LOFT1_all_continuum,s_LOFT1_all_continuum,N_LOFT1_all_continuum,Am_CST_LOFT1_all_continuum,delta_v_CST_LOFT1_all_continuum]=postprocess_data(FRAGMENTS,ME,BUBBLE,PROPAGATION_THRESHOLD,link_data);

tf_LOFT1_all_continuum = sprintf('%25.4f',tf);
if exist('RunTime','var')
    cputime_LOFT1_all_continuum = sprintf('%20.2f',RunTime/60);
else
    cputime_LOFT1_all_continuum = '                   -';
end
final_step_LOFT1_all_continuum = sprintf('%24d',str2double(name_file_result_LOFT1_all_continuum(strfind(name_file_result_LOFT1_all_continuum,'step_')+5:end-4)));
clear FRAGMENTS ME BUBBLE PROPAGATION_THRESHOLD link_data tf RunTime

%% processing LOFT1 no links scenario data

load(name_file_result_LOFT1_no_links,'FRAGMENTS','ME','BUBBLE','PROPAGATION_THRESHOLD','link_data','tf','RunTime')

[n_frags_bubbles_LOFT1_no_links,s_LOFT1_no_links,N_LOFT1_no_links,Am_CST_LOFT1_no_links,delta_v_CST_LOFT1_no_links]=postprocess_data(FRAGMENTS,ME,BUBBLE,PROPAGATION_THRESHOLD,link_data);

tf_LOFT1_no_links = sprintf('%25.4f',tf);
if exist('RunTime','var')
    cputime_LOFT1_no_links = sprintf('%20.2f',RunTime/60);
else
    cputime_LOFT1_no_links = '                   -';
end

final_step_LOFT1_no_links = sprintf('%24d',str2double(name_file_result_LOFT1_no_links(strfind(name_file_result_LOFT1_no_links,'step_')+5:end-4)));
clear FRAGMENTS ME BUBBLE PROPAGATION_THRESHOLD link_data tf RunTime

%% processing LOFT1 EMI configuration scenario data

load(name_file_result_LOFT1_EMI_config,'FRAGMENTS','ME','BUBBLE','PROPAGATION_THRESHOLD','link_data','tf','RunTime')

[n_frags_bubbles_LOFT1_EMI_config,s_LOFT1_EMI_config,N_LOFT1_EMI_config,Am_CST_LOFT1_EMI_config,delta_v_CST_LOFT1_EMI_config]=postprocess_data(FRAGMENTS,ME,BUBBLE,PROPAGATION_THRESHOLD,link_data);

tf_LOFT1_EMI_config = sprintf('%25.4f',tf);
if exist('RunTime','var')
    cputime_LOFT1_EMI_config = sprintf('%20.2f',RunTime/60);
else
    cputime_LOFT1_EMI_config = '                   -';
end

final_step_LOFT1_EMI_config = sprintf('%24d',str2double(name_file_result_LOFT1_EMI_config(strfind(name_file_result_LOFT1_EMI_config,'step_')+5:end-4)));
clear FRAGMENTS ME BUBBLE PROPAGATION_THRESHOLD link_data tf RunTime

%% processing LOFT2 scenario data

load(name_file_result_LOFT2,'FRAGMENTS','ME','BUBBLE','PROPAGATION_THRESHOLD','link_data','tf','RunTime')

mass_imp_2=0;
q_imp_2 = [0 0 0]';

for i = 52:length(ME)
    mass_imp_2 = mass_imp_2 + ME(i).GEOMETRY_DATA.mass0;
    q_imp_2 = q_imp_2 + ME(i).GEOMETRY_DATA.mass0*ME(i).DYNAMICS_INITIAL_DATA.vel0;
end
v_p_2 = 1/mass_imp_2*q_imp_2/1000;

[n_frags_bubbles_LOFT2,s_LOFT2,N_LOFT2,Am_CST_LOFT2,delta_v_CST_LOFT2]=postprocess_data(FRAGMENTS,ME,BUBBLE,PROPAGATION_THRESHOLD,link_data);

tf_LOFT2 = sprintf('%25.4f',tf);
if exist('RunTime','var')
    cputime_LOFT2 = sprintf('%20.2f',RunTime/60);
else
    cputime_LOFT2 = '                   -';
end

final_step_LOFT2 = sprintf('%24d',str2double(name_file_result_LOFT2(strfind(name_file_result_LOFT2,'step_')+5:end-4)));
EMR_LOFT2 = sprintf('%5.2f',1/2*mass_imp_2*norm(v_p_2*1000)^2/(mass_imp_2+mass_LOFT)/1000);

clear FRAGMENTS ME BUBBLE PROPAGATION_THRESHOLD link_data tf RunTime

%% processing LOFT3 scenario data

load(name_file_result_LOFT3,'FRAGMENTS','ME','BUBBLE','PROPAGATION_THRESHOLD','link_data','tf','RunTime')

mass_imp_3=0;
q_imp_3 = [0 0 0]';

for i = 52:length(ME)
    mass_imp_3 = mass_imp_3 + ME(i).GEOMETRY_DATA.mass0;
    q_imp_3 = q_imp_3 + ME(i).GEOMETRY_DATA.mass0*ME(i).DYNAMICS_INITIAL_DATA.vel0;
end
v_p_3 = 1/mass_imp_3*q_imp_3/1000;

[n_frags_bubbles_LOFT3,s_LOFT3,N_LOFT3,Am_CST_LOFT3,delta_v_CST_LOFT3]=postprocess_data(FRAGMENTS,ME,BUBBLE,PROPAGATION_THRESHOLD,link_data);

tf_LOFT3 = sprintf('%25.4f',tf);
if exist('RunTime','var')
    cputime_LOFT3 = sprintf('%20.2f',RunTime/60);
else
    cputime_LOFT3 = '                   -';
end

final_step_LOFT3 = sprintf('%24d',str2double(name_file_result_LOFT3(strfind(name_file_result_LOFT3,'step_')+5:end-4)));
EMR_LOFT3 = sprintf('%5.2f',1/2*mass_imp_3*norm(v_p_3*1000)^2/(mass_imp_3+mass_LOFT)/1000);
clear FRAGMENTS ME BUBBLE PROPAGATION_THRESHOLD link_data tf RunTime

%% processing LOFT4 scenario data

load(name_file_result_LOFT4,'FRAGMENTS','ME','BUBBLE','PROPAGATION_THRESHOLD','link_data','tf','RunTime')

[n_frags_bubbles_LOFT4,s_LOFT4,N_LOFT4,Am_CST_LOFT4,delta_v_CST_LOFT4]=postprocess_data(FRAGMENTS,ME,BUBBLE,PROPAGATION_THRESHOLD,link_data);

tf_LOFT4 = sprintf('%25.4f',tf);
if exist('RunTime','var')
    cputime_LOFT4 = sprintf('%20.2f',RunTime/60);
else
    cputime_LOFT4 = '                   -';
end

final_step_LOFT4 = sprintf('%24d',str2double(name_file_result_LOFT4(strfind(name_file_result_LOFT4,'step_')+5:end-4)));
EMR_LOFT4 = EMR_LOFT3;
clear FRAGMENTS ME BUBBLE PROPAGATION_THRESHOLD link_data tf RunTime

%% processing LOFT5 scenario data

load(name_file_result_LOFT5,'FRAGMENTS','ME','BUBBLE','PROPAGATION_THRESHOLD','link_data','tf','RunTime')

[n_frags_bubbles_LOFT5,s_LOFT5,N_LOFT5,Am_CST_LOFT5,delta_v_CST_LOFT5]=postprocess_data(FRAGMENTS,ME,BUBBLE,PROPAGATION_THRESHOLD,link_data);

tf_LOFT5 = sprintf('%25.4f',tf);
if exist('RunTime','var')
    cputime_LOFT5 = sprintf('%20.2f',RunTime/60);
else
    cputime_LOFT5 = '                   -';
end

final_step_LOFT5 = sprintf('%24d',str2double(name_file_result_LOFT5(strfind(name_file_result_LOFT5,'step_')+5:end-4)));
EMR_LOFT5 = EMR_LOFT3;
clear FRAGMENTS ME BUBBLE PROPAGATION_THRESHOLD link_data tf RunTime

%% processing LOFT6 scenario data

load(name_file_result_LOFT6,'FRAGMENTS','ME','BUBBLE','PROPAGATION_THRESHOLD','link_data','tf','RunTime')

[n_frags_bubbles_LOFT6,s_LOFT6,N_LOFT6,Am_CST_LOFT6,delta_v_CST_LOFT6]=postprocess_data(FRAGMENTS,ME,BUBBLE,PROPAGATION_THRESHOLD,link_data);

tf_LOFT6 = sprintf('%25.4f',tf);
if exist('RunTime','var')
    cputime_LOFT6 = sprintf('%20.2f',RunTime/60);
else
    cputime_LOFT6 = '                   -';
end

final_step_LOFT6 = sprintf('%24d',str2double(name_file_result_LOFT6(strfind(name_file_result_LOFT6,'step_')+5:end-4)));
EMR_LOFT6 = EMR_LOFT3;
clear FRAGMENTS ME BUBBLE PROPAGATION_THRESHOLD link_data tf RunTime

%% processing LOFT6 all continuum scenario data

load(name_file_result_LOFT6_all_continuum,'FRAGMENTS','ME','BUBBLE','PROPAGATION_THRESHOLD','link_data','tf','RunTime')

[n_frags_bubbles_LOFT6_all_continuum,s_LOFT6_all_continuum,N_LOFT6_all_continuum,Am_CST_LOFT6_all_continuum,delta_v_CST_LOFT6_all_continuum]=postprocess_data(FRAGMENTS,ME,BUBBLE,PROPAGATION_THRESHOLD,link_data);

tf_LOFT6_all_continuum = sprintf('%25.4f',tf);
if exist('RunTime','var')
    cputime_LOFT6_all_continuum = sprintf('%20.2f',RunTime/60);
else
    cputime_LOFT6_all_continuum = '                   -';
end

final_step_LOFT6_all_continuum = sprintf('%24d',str2double(name_file_result_LOFT6_all_continuum(strfind(name_file_result_LOFT6_all_continuum,'step_')+5:end-4)));
clear FRAGMENTS ME BUBBLE PROPAGATION_THRESHOLD link_data tf RunTime

%% processing LOFT6 no links scenario data

load(name_file_result_LOFT6_no_links,'FRAGMENTS','ME','BUBBLE','PROPAGATION_THRESHOLD','link_data','tf','RunTime')

[n_frags_bubbles_LOFT6_no_links,s_LOFT6_no_links,N_LOFT6_no_links,Am_CST_LOFT6_no_links,delta_v_CST_LOFT6_no_links]=postprocess_data(FRAGMENTS,ME,BUBBLE,PROPAGATION_THRESHOLD,link_data);

tf_LOFT6_no_links = sprintf('%25.4f',tf);
if exist('RunTime','var')
    cputime_LOFT6_no_links = sprintf('%20.2f',RunTime/60);
else
    cputime_LOFT6_no_links = '                   -';
end

final_step_LOFT6_no_links = sprintf('%24d',str2double(name_file_result_LOFT6_no_links(strfind(name_file_result_LOFT6_no_links,'step_')+5:end-4)));
clear FRAGMENTS ME BUBBLE PROPAGATION_THRESHOLD link_data tf RunTime

%% processing LOFT6 c_mat4 scenario data
% 
% load(name_file_result_LOFT6_c_mat4,'FRAGMENTS','ME','BUBBLE','PROPAGATION_THRESHOLD','link_data','tf','RunTime')
% 
% [n_frags_bubbles_LOFT6_c_mat4,s_LOFT6_c_mat4,N_LOFT6_c_mat4,Am_CST_LOFT6_c_mat4,delta_v_CST_LOFT6_c_mat4]=postprocess_data(FRAGMENTS,ME,BUBBLE,PROPAGATION_THRESHOLD,link_data);
% 
% tf_LOFT6_c_mat4 = sprintf('%25.4f',tf);
% if exist('RunTime','var')
%     cputime_LOFT6_c_mat4 = sprintf('%20.2f',RunTime/60);
% else
%     cputime_LOFT6_c_mat4 = '                   -';
% end
% 
% final_step_LOFT6_c_mat4 = sprintf('%24d',str2double(name_file_result_LOFT6_c_mat4(strfind(name_file_result_LOFT6_c_mat4,'step_')+5:end-4)));
% clear FRAGMENTS ME BUBBLE PROPAGATION_THRESHOLD link_data tf RunTime

%% processing LOFT6 c_mat2 scenario data

load(name_file_result_LOFT6_c_mat2,'FRAGMENTS','ME','BUBBLE','PROPAGATION_THRESHOLD','link_data','tf','RunTime')

[n_frags_bubbles_LOFT6_c_mat2,s_LOFT6_c_mat2,N_LOFT6_c_mat2,Am_CST_LOFT6_c_mat2,delta_v_CST_LOFT6_c_mat2]=postprocess_data(FRAGMENTS,ME,BUBBLE,PROPAGATION_THRESHOLD,link_data);

tf_LOFT6_c_mat2 = sprintf('%25.4f',tf);
if exist('RunTime','var')
    cputime_LOFT6_c_mat2 = sprintf('%20.2f',RunTime/60);
else
    cputime_LOFT6_c_mat2 = '                   -';
end

final_step_LOFT6_c_mat2 = sprintf('%24d',str2double(name_file_result_LOFT6_c_mat2(strfind(name_file_result_LOFT6_c_mat2,'step_')+5:end-4)));
clear FRAGMENTS BUBBLE PROPAGATION_THRESHOLD link_data tf RunTime

%% plot finale Lc
%{d
colorOrder =  [ ...
        0            0            0       ;...% 6 BLACK
        0            0            0.7       ;...% 1 BLUE
        0            0.7            0       ;...% 3 GREEN (pale)
        0.7            0            0.7       ;...% 5 MAGENTA (pale)
        0            0.8            0.8       ;...% 4 CYAN
        0.6          0.5          0.4     ;  % 15 BROWN (dark)
        1            1            0       ;...% 7 YELLOW (pale)
        0            0.75         0.75    ;...% 8 TURQUOISE
        0            0.5          0       ;...% 9 GREEN (dark)
        0.75         0.75         0       ;...% 10 YELLOW (dark)
        1            0.50         0.25    ;...% 11 ORANGE
        0.75         0            0.75    ;...% 12 MAGENTA (dark)
        0.7          0.7          0.7     ;...% 13 GREY
        0.8          0.7          0.6     ;...% 14 BROWN (pale)
        0.4          0.7          0.6     ;...
        0.4          0          0.6     ;...
        0.4          0.6          0     ;...
        0.6          0.5          0.4 ];  % 18 BROWN (dark)
    
m_tot=0;
for ii = 1:length(ME)
    m_tot=m_tot+ME(ii).GEOMETRY_DATA.mass0;
end
NASA_SBM_cat = 0.1*(m_tot)^0.75*s_LOFT1.^(-1.71);

%% Plot Lc LOFT cases EMI

Lc_NASA=[min(LOFT6(:,1)):(max(LOFT6(:,1))-min(LOFT6(:,1)))/100:max(LOFT6(:,1)),1];
NASA_SBM_01_sub = 0.1*(mass_imp_1*norm(v_p_1))^0.75*Lc_NASA.^(-1.71);
NASA_SBM_1_sub = 0.1*(mass_imp_2*norm(v_p_2))^0.75*Lc_NASA.^(-1.71);
NASA_SBM_10_sub = 0.1*(mass_imp_3*norm(v_p_3))^0.75*Lc_NASA.^(-1.71);

m_tot=0;
for ii = 1:length(ME)
    m_tot=m_tot+ME(ii).GEOMETRY_DATA.mass0;
end
NASA_SBM_01_cat = 0.1*(m_tot)^0.75*Lc_NASA.^(-1.71);
NASA_SBM_1_cat = 0.1*(m_tot+0.9)^0.75*Lc_NASA.^(-1.71);
NASA_SBM_10_cat = 0.1*(m_tot+9.9)^0.75*Lc_NASA.^(-1.71);

xx_lim = [2e-03,1e0];
yy_lim = [1e0,1e6];

figure('units','normalized','outerposition',[0 0 1 1],'Visible', 'on')
loglog(LOFT1(:,1),LOFT1(:,2),'MarkerEdgeColor',colorOrder(9,:),'MarkerFaceColor',colorOrder(9,:),'Marker','*','LineStyle','none');
hold on
loglog(LOFT2(:,1),LOFT2(:,2),'MarkerEdgeColor',colorOrder(12,:),'MarkerFaceColor',colorOrder(12,:),'Marker','+','LineStyle','none');
hold on
loglog(LOFT3(:,1),LOFT3(:,2),'MarkerEdgeColor',colorOrder(11,:),'MarkerFaceColor',colorOrder(11,:),'Marker','o','LineStyle','none');
hold on
loglog(LOFT4(:,1),LOFT4(:,2),'MarkerEdgeColor',colorOrder(5,:),'MarkerFaceColor',colorOrder(5,:),'Marker','^','LineStyle','none');
hold on
loglog(LOFT5(:,1),LOFT5(:,2),'MarkerEdgeColor',colorOrder(10,:),'MarkerFaceColor',colorOrder(10,:),'Marker','<','LineStyle','none');
hold on
loglog(LOFT6(:,1),LOFT6(:,2),'MarkerFaceColor',colorOrder(4,:),'Marker','x','LineStyle','none');
set(gca,'FontSize',20,'FontWeight','bold')
loglog(Lc_NASA,NASA_SBM_01_sub,'--r','LineWidth',2)
loglog(Lc_NASA,NASA_SBM_1_sub,':r','LineWidth',2)
loglog(Lc_NASA,NASA_SBM_10_sub,'-r','LineWidth',2)
loglog(Lc_NASA,NASA_SBM_10_cat,'-k','LineWidth',2)

legend('EMI-LOFT1','EMI-LOFT2','EMI-LOFT3','EMI-LOFT4','EMI-LOFT5','EMI-LOFT6','NASA - SBM SubCat 0.1 kg','NASA - SBM SubCat 1 kg','NASA SBM - SubCat 10 kg','NASA - SBM Cat');
xlabel('L_c [m]')
ylabel('Cumulative number CN')
hold on
grid on
set(gca,'XLim',xx_lim)
set(gca,'YLim',yy_lim)

out_fig = 'Lc_EMI_data';
saveas(gcf,out_fig,'fig')
saveas(gcf,out_fig,'png')

%% Plot Lc LOFT cases CST

figure('units','normalized','outerposition',[0 0 1 1],'Visible', 'on')
loglog(s_LOFT1,[N_LOFT1(1)+n_frags_bubbles_LOFT1,N_LOFT1(2:end)],'MarkerEdgeColor',colorOrder(9,:),'MarkerFaceColor',colorOrder(9,:),'Marker','*','LineStyle','none')
grid on
hold on
loglog(s_LOFT2,[N_LOFT2(1)+n_frags_bubbles_LOFT2,N_LOFT2(2:end)],'MarkerEdgeColor',colorOrder(12,:),'MarkerFaceColor',colorOrder(12,:),'Marker','+','LineStyle','none')
hold on
loglog(s_LOFT3,[N_LOFT3(1)+n_frags_bubbles_LOFT3,N_LOFT3(2:end)],'MarkerEdgeColor',colorOrder(11,:),'MarkerFaceColor',colorOrder(11,:),'Marker','o','LineStyle','none')
hold on
loglog(s_LOFT4,[N_LOFT4(1)+n_frags_bubbles_LOFT4,N_LOFT4(2:end)],'MarkerEdgeColor',colorOrder(5,:),'MarkerFaceColor',colorOrder(5,:),'Marker','^','LineStyle','none')
hold on
loglog(s_LOFT5,[N_LOFT5(1)+n_frags_bubbles_LOFT5,N_LOFT5(2:end)],'MarkerEdgeColor',colorOrder(10,:),'MarkerFaceColor',colorOrder(10,:),'Marker','<','LineStyle','none')
hold on
loglog(s_LOFT6,[N_LOFT6(1)+n_frags_bubbles_LOFT6,N_LOFT6(2:end)],'MarkerFaceColor',colorOrder(4,:),'Marker','x','LineStyle','none')
hold on
loglog(Lc_NASA,NASA_SBM_01_sub,'--r','LineWidth',2)
loglog(Lc_NASA,NASA_SBM_1_sub,':r','LineWidth',2)
loglog(Lc_NASA,NASA_SBM_10_sub,'-r','LineWidth',2)
loglog(Lc_NASA,NASA_SBM_10_cat,'-k','LineWidth',2)
set(gca,'FontSize',20,'FontWeight','bold')
title('CST results LOFT cases')
xlabel('L_c [m]')
ylabel('Cumulative number CN')
legend(['CST-LOFT1 (EMR = ',EMR_LOFT1,' J/g)'],['CST-LOFT2 (EMR = ',EMR_LOFT2,' J/g)'],['CST-LOFT3 (EMR = ',EMR_LOFT3,' J/g)'],['CST-LOFT4 (EMR = ',EMR_LOFT4,' J/g)'],['CST-LOFT5 (EMR = ',EMR_LOFT5,' J/g)'],['CST-LOFT6 (EMR = ',EMR_LOFT6,' J/g)'],'NASA - SBM SubCat 0.1 kg','NASA - SBM SubCat 1 kg','NASA SBM - SubCat 10 kg','NASA - SBM Cat');
set(gca,'XLim',xx_lim)
set(gca,'YLim',yy_lim)

out_fig = 'Lc_CST_baseline_data';
saveas(gcf,out_fig,'fig')
saveas(gcf,out_fig,'png')

%% Plot LOFT cases CST-EMI
%{
figure('units','normalized','outerposition',[0 0 1 1],'Visible', 'on')
loglog(s_LOFT1,[N_LOFT1(1)+n_frags_bubbles_LOFT1,N_LOFT1(2:end)],'Color',colorOrder(9,:),'Marker','none','LineStyle','-','LineWidth',2)
hold on
grid on
loglog(LOFT1(:,1),LOFT1(:,2),'MarkerEdgeColor',colorOrder(9,:),'MarkerFaceColor',colorOrder(9,:),'Marker','*','LineStyle','none');
hold on
loglog(s_LOFT2,[N_LOFT2(1)+n_frags_bubbles_LOFT2,N_LOFT2(2:end)],'Color',colorOrder(12,:),'Marker','none','LineStyle','-','LineWidth',2)
hold on
loglog(LOFT2(:,1),LOFT2(:,2),'MarkerEdgeColor',colorOrder(12,:),'MarkerFaceColor',colorOrder(12,:),'Marker','+','LineStyle','none');
hold on
loglog(s_LOFT3,[N_LOFT3(1)+n_frags_bubbles_LOFT3,N_LOFT3(2:end)],'Color',colorOrder(11,:),'Marker','none','LineStyle','-','LineWidth',2)
hold on
loglog(LOFT3(:,1),LOFT3(:,2),'MarkerEdgeColor',colorOrder(11,:),'MarkerFaceColor',colorOrder(11,:),'Marker','o','LineStyle','none');
hold on
loglog(s_LOFT4,[N_LOFT4(1)+n_frags_bubbles_LOFT4,N_LOFT4(2:end)],'Color',colorOrder(5,:),'Marker','none','LineStyle','-','LineWidth',2)
hold on
loglog(LOFT4(:,1),LOFT4(:,2),'MarkerEdgeColor',colorOrder(5,:),'MarkerFaceColor',colorOrder(5,:),'Marker','^','LineStyle','none');
hold on
loglog(s_LOFT5,[N_LOFT5(1)+n_frags_bubbles_LOFT5,N_LOFT5(2:end)],'Color',colorOrder(10,:),'Marker','none','LineStyle','-','LineWidth',2)
hold on
loglog(LOFT5(:,1),LOFT5(:,2),'MarkerEdgeColor',colorOrder(10,:),'MarkerFaceColor',colorOrder(10,:),'Marker','<','LineStyle','none');
hold on
loglog(s_LOFT6,[N_LOFT6(1)+n_frags_bubbles_LOFT6,N_LOFT6(2:end)],'Color',colorOrder(18,:),'Marker','none','LineStyle','-','LineWidth',2)
hold on
loglog(LOFT6(:,1),LOFT6(:,2),'MarkerEdgeColor',colorOrder(18,:),'MarkerFaceColor',colorOrder(18,:),'Marker','x','LineStyle','none');
hold on
loglog(Lc_NASA,NASA_SBM_01_sub,'--r','LineWidth',2)
loglog(Lc_NASA,NASA_SBM_1_sub,':r','LineWidth',2)
loglog(Lc_NASA,NASA_SBM_10_sub,'-r','LineWidth',2)
loglog(Lc_NASA,NASA_SBM_10_cat,'-k','LineWidth',2)
set(gca,'FontSize',20,'FontWeight','bold')
title('Comparison CST results - EMI results LOFT cases')
xlabel('L_c [m]')
ylabel('Cumulative number N')
legend('CST-LOFT1','EMI-LOFT1','CST-LOFT2','EMI-LOFT2','CST-LOFT3','EMI-LOFT3','CST-LOFT4','EMI-LOFT4','CST-LOFT5','EMI-LOFT5','CST-LOFT6','EMI-LOFT6','NASA - SBM SubCat 0.1 kg','NASA - SBM SubCat 1 kg','NASA SBM - SubCat 10 kg','NASA - SBM Cat');
set(gca,'XLim',xx_lim)
set(gca,'YLim',yy_lim)

out_fig = 'Lc_EMIvsCST_baseline_data';
saveas(gcf,out_fig,'fig')
saveas(gcf,out_fig,'png')
%}
%% Plot Lc comparison LOFT1 CST-EMI

figure('units','normalized','outerposition',[0 0 1 1],'Visible', 'on')
loglog(s_LOFT1,[N_LOFT1(1)+n_frags_bubbles_LOFT1,N_LOFT1(2:end)],'MarkerEdgeColor',colorOrder(9,:),'MarkerFaceColor',colorOrder(9,:),'Marker','*','LineStyle','none');
hold on
grid on
loglog(s_LOFT1_EMI_config,[N_LOFT1_EMI_config(1)+n_frags_bubbles_LOFT1_EMI_config,N_LOFT1_EMI_config(2:end)],'MarkerEdgeColor',colorOrder(12,:),'MarkerFaceColor',colorOrder(12,:),'Marker','+','LineStyle','none');
hold on
loglog(s_LOFT1_all_continuum,[N_LOFT1_all_continuum(1)+n_frags_bubbles_LOFT1_all_continuum,N_LOFT1_all_continuum(2:end)],'MarkerEdgeColor',colorOrder(11,:),'MarkerFaceColor',colorOrder(11,:),'Marker','o','LineStyle','none');
hold on
loglog(s_LOFT1_no_links,[N_LOFT1_no_links(1)+n_frags_bubbles_LOFT1_no_links,N_LOFT1_no_links(2:end)],'MarkerEdgeColor',colorOrder(5,:),'MarkerFaceColor',colorOrder(5,:),'Marker','^','LineStyle','none');
hold on
loglog(LOFT1(:,1),LOFT1(:,2),'MarkerEdgeColor',colorOrder(10,:),'MarkerFaceColor',colorOrder(10,:),'Marker','^','LineStyle','none');
loglog(Lc_NASA,NASA_SBM_01_sub,'--r','LineWidth',2)
loglog(Lc_NASA,NASA_SBM_10_cat,'-k','LineWidth',2)
set(gca,'FontSize',20,'FontWeight','bold')
title(['Comparison CST results - EMI results LOFT1 (EMR = ',EMR_LOFT1,' J/g)'])
xlabel('L_c [m]')
ylabel('Cumulative number CN (L_c)')
legend('CST-LOFT1','CST-LOFT1 in EMI configuration','CST-LOFT1 all CONTINUUM links','CST-LOFT1 no links','EMI-LOFT1','NASA - SBM SubCat 0.1 kg','NASA - SBM Cat');
set(gca,'XLim',xx_lim)
set(gca,'YLim',yy_lim)

out_fig = 'Lc_EMIvsCST_LOFT1_data';
saveas(gcf,out_fig,'fig')
saveas(gcf,out_fig,'png')

%% Plot Lc comparison LOFT2 CST-EMI

figure('units','normalized','outerposition',[0 0 1 1],'Visible', 'on')
loglog(s_LOFT2,[N_LOFT2(1)+n_frags_bubbles_LOFT2,N_LOFT2(2:end)],'MarkerEdgeColor',colorOrder(9,:),'MarkerFaceColor',colorOrder(9,:),'Marker','*','LineStyle','none');
hold on
grid on
loglog(LOFT2(:,1),LOFT2(:,2),'MarkerEdgeColor',colorOrder(10,:),'MarkerFaceColor',colorOrder(10,:),'Marker','^','LineStyle','none');
loglog(Lc_NASA,NASA_SBM_1_sub,'--r','LineWidth',2)
loglog(Lc_NASA,NASA_SBM_10_cat,'-k','LineWidth',2)
set(gca,'FontSize',20,'FontWeight','bold')
title(['Comparison CST results - EMI results LOFT2 (EMR = ',EMR_LOFT2,' J/g)'])
xlabel('L_c [m]')
ylabel('Cumulative number CN (L_c)')
legend('CST-LOFT2','EMI-LOFT2','NASA - SBM SubCat 1 kg','NASA - SBM Cat');
set(gca,'XLim',xx_lim)
set(gca,'YLim',yy_lim)

out_fig = 'Lc_EMIvsCST_LOFT2_data';
saveas(gcf,out_fig,'fig')
saveas(gcf,out_fig,'png')

%% Plot Lc comparison LOFT3 CST-EMI

figure('units','normalized','outerposition',[0 0 1 1],'Visible', 'on')
loglog(s_LOFT3,[N_LOFT3(1)+n_frags_bubbles_LOFT3,N_LOFT3(2:end)],'MarkerEdgeColor',colorOrder(9,:),'MarkerFaceColor',colorOrder(9,:),'Marker','*','LineStyle','none');
hold on
grid on
loglog(LOFT3(:,1),LOFT3(:,2),'MarkerEdgeColor',colorOrder(10,:),'MarkerFaceColor',colorOrder(10,:),'Marker','^','LineStyle','none');
loglog(Lc_NASA,NASA_SBM_10_sub,'--r','LineWidth',2)
loglog(Lc_NASA,NASA_SBM_10_cat,'-k','LineWidth',2)
set(gca,'FontSize',20,'FontWeight','bold')
title(['Comparison CST results - EMI results LOFT3 (EMR = ',EMR_LOFT3,' J/g)'])
xlabel('L_c [m]')
ylabel('Cumulative number CN (L_c)')
legend('CST-LOFT3','EMI-LOFT3','NASA - SBM SubCat 10 kg','NASA - SBM Cat');
set(gca,'XLim',xx_lim)
set(gca,'YLim',yy_lim)

out_fig = 'Lc_EMIvsCST_LOFT3_data';
saveas(gcf,out_fig,'fig')
saveas(gcf,out_fig,'png')

%% Plot Lc comparison LOFT4 CST-EMI

figure('units','normalized','outerposition',[0 0 1 1],'Visible', 'on')
loglog(s_LOFT4,[N_LOFT4(1)+n_frags_bubbles_LOFT4,N_LOFT4(2:end)],'MarkerEdgeColor',colorOrder(9,:),'MarkerFaceColor',colorOrder(9,:),'Marker','*','LineStyle','none');
hold on
grid on
loglog(LOFT4(:,1),LOFT4(:,2),'MarkerEdgeColor',colorOrder(10,:),'MarkerFaceColor',colorOrder(10,:),'Marker','^','LineStyle','none');
loglog(Lc_NASA,NASA_SBM_10_sub,'--r','LineWidth',2)
loglog(Lc_NASA,NASA_SBM_10_cat,'-k','LineWidth',2)
set(gca,'FontSize',20,'FontWeight','bold')
title(['Comparison CST results - EMI results LOFT4 (EMR = ',EMR_LOFT4,' J/g)'])
xlabel('L_c [m]')
ylabel('Cumulative number CN (L_c)')
legend('CST-LOFT4','EMI-LOFT4','NASA - SBM SubCat 10 kg','NASA - SBM Cat');
set(gca,'XLim',xx_lim)
set(gca,'YLim',yy_lim)

out_fig = 'Lc_EMIvsCST_LOFT4_data';
saveas(gcf,out_fig,'fig')
saveas(gcf,out_fig,'png')

%% Plot Lc comparison LOFT5 CST-EMI

figure('units','normalized','outerposition',[0 0 1 1],'Visible', 'on')
loglog(s_LOFT5,[N_LOFT5(1)+n_frags_bubbles_LOFT5,N_LOFT5(2:end)],'MarkerEdgeColor',colorOrder(9,:),'MarkerFaceColor',colorOrder(9,:),'Marker','*','LineStyle','none');
hold on
grid on
loglog(LOFT5(:,1),LOFT5(:,2),'MarkerEdgeColor',colorOrder(10,:),'MarkerFaceColor',colorOrder(10,:),'Marker','^','LineStyle','none');
loglog(Lc_NASA,NASA_SBM_10_sub,'--r','LineWidth',2)
loglog(Lc_NASA,NASA_SBM_10_cat,'-k','LineWidth',2)
set(gca,'FontSize',20,'FontWeight','bold')
title(['Comparison CST results - EMI results LOFT5 (EMR = ',EMR_LOFT5,' J/g)'])
xlabel('L_c [m]')
ylabel('Cumulative number CN (L_c)')
legend('CST-LOFT5','EMI-LOFT5','NASA - SBM SubCat 10 kg','NASA - SBM Cat');
set(gca,'XLim',xx_lim)
set(gca,'YLim',yy_lim)

out_fig = 'Lc_EMIvsCST_LOFT5_data';
saveas(gcf,out_fig,'fig')
saveas(gcf,out_fig,'png')

%% Plot Lc comparison LOFT6 CST

figure('units','normalized','outerposition',[0 0 1 1],'Visible', 'on')
loglog(s_LOFT6,[N_LOFT6(1)+n_frags_bubbles_LOFT6,N_LOFT6(2:end)],'MarkerEdgeColor',colorOrder(9,:),'MarkerFaceColor',colorOrder(9,:),'Marker','*','LineStyle','none');
hold on
grid on
loglog(s_LOFT6_all_continuum,[N_LOFT6_all_continuum(1)+n_frags_bubbles_LOFT6_all_continuum,N_LOFT6_all_continuum(2:end)],'MarkerEdgeColor',colorOrder(11,:),'MarkerFaceColor',colorOrder(11,:),'Marker','o','LineStyle','none');
hold on
loglog(s_LOFT6_no_links,[N_LOFT6_no_links(1)+n_frags_bubbles_LOFT6_no_links,N_LOFT6_no_links(2:end)],'MarkerEdgeColor',colorOrder(5,:),'MarkerFaceColor',colorOrder(5,:),'Marker','^','LineStyle','none');
hold on
loglog(s_LOFT6_c_mat2,[N_LOFT6_c_mat2(1)+n_frags_bubbles_LOFT6_c_mat2,N_LOFT6_c_mat2(2:end)],'MarkerEdgeColor',colorOrder(4,:),'MarkerFaceColor',colorOrder(4,:),'Marker','+','LineStyle','none');
hold on
% loglog(s_LOFT6_c_mat4,[N_LOFT6_c_mat4(1)+n_frags_bubbles_LOFT6_c_mat4,N_LOFT6_c_mat4(2:end)],'MarkerEdgeColor',colorOrder(12,:),'MarkerFaceColor',colorOrder(12,:),'Marker','d','LineStyle','none');
% hold on
loglog(LOFT6(:,1),LOFT6(:,2),'MarkerEdgeColor',colorOrder(10,:),'MarkerFaceColor',colorOrder(10,:),'Marker','^','LineStyle','none');
loglog(Lc_NASA,NASA_SBM_10_sub,'--r','LineWidth',2)
loglog(Lc_NASA,NASA_SBM_10_cat,'-k','LineWidth',2)
set(gca,'FontSize',20,'FontWeight','bold')
title(['Comparison CST results - EMI results LOFT6 (EMR = ',EMR_LOFT6,' J/g)'])
xlabel('L_c [m]')
ylabel('Cumulative number CN (L_c)')
legend('CST-LOFT6','CST-LOFT6 all CONTINUUM links','CST-LOFT6 no links','CST-LOFT6 2xdiss. links','EMI-LOFT6','NASA - SBM SubCat 10 kg','NASA - SBM Cat');%,'CST-LOFT6 4xdiss. links','EMI-LOFT6','NASA - SBM SubCat 10 kg','NASA - SBM Cat');
set(gca,'XLim',xx_lim)
set(gca,'YLim',yy_lim)

out_fig = 'Lc_EMIvsCST_LOFT6_data';
saveas(gcf,out_fig,'fig')
saveas(gcf,out_fig,'png')

%% plot finale LOFT1 A su M

xx_lim_Am = [-2,0.3];
yy_lim_Am = [0,0.12];
clear DAmSOC DAmSOC_matlab DAmSOC_NASA
lambda_c_min=log10(min([s_LOFT1 s_LOFT1_EMI_config s_LOFT1_all_continuum s_LOFT1_all_continuum s_LOFT1_no_links]));
lambda_c_max=log10(max([s_LOFT1 s_LOFT1_EMI_config s_LOFT1_all_continuum s_LOFT1_all_continuum s_LOFT1_no_links]));
lambda_c=lambda_c_min:(lambda_c_max-lambda_c_min)/15:lambda_c_max;
Am_LOFT1 =[Am_CST_LOFT1 Am_CST_LOFT1_EMI_config Am_CST_LOFT1_all_continuum Am_CST_LOFT1_all_continuum Am_CST_LOFT1_no_links];
csi_min=log10(min(Am_LOFT1(Am_LOFT1>0)));
csi_max=log10(max(Am_LOFT1));
n=length(lambda_c);
csi_NASA=csi_min:(csi_max-csi_min)/(n-1):csi_max;

for i=1:1:n
        if  lambda_c(i)<=-3.5
            muSOC=-0.3;
            sigmaSOC=0.2;
            DAmSOC(i,:)=(1/(sigmaSOC*(2*pi)^0.5))*exp(-((csi_NASA-muSOC).^2)/(2*sigmaSOC^2));
            pd = makedist('Normal','mu',muSOC,'sigma',sigmaSOC);
            DAmSOC_matlab(i,:) = pdf(pd,csi_NASA);
        elseif lambda_c(i)>-3.5 && lambda_c(i)<=-1.75
            muSOC=-0.3;
            sigmaSOC=0.2+0.1333*(lambda_c(i)+3.5);
            DAmSOC(i,:)=(1/(sigmaSOC*(2*pi)^0.5))*exp(-((csi_NASA-muSOC).^2)/(2*sigmaSOC^2));
            pd = makedist('Normal','mu',muSOC,'sigma',sigmaSOC);
            DAmSOC_matlab(i,:) = pdf(pd,csi_NASA);
        elseif lambda_c(i)>-1.75 && lambda_c(i)<-1.25
            muSOC=-0.3-1.4*(lambda_c(i)+1.75);
            sigmaSOC=0.2+0.1333*(lambda_c(i)+3.5);
            DAmSOC(i,:)=(1/(sigmaSOC*(2*pi)^0.5))*exp(-((csi_NASA-muSOC).^2)/(2*sigmaSOC^2));
            pd = makedist('Normal','mu',muSOC,'sigma',sigmaSOC);
            DAmSOC_matlab(i,:) = pdf(pd,csi_NASA);
        elseif lambda_c(i)>=-1.25
            muSOC=-1.0;
            sigmaSOC=0.2+0.1333*(lambda_c(i)+3.5);
            DAmSOC(i,:)=(1/(sigmaSOC*(2*pi)^0.5))*exp(-((csi_NASA-muSOC).^2)/(2*sigmaSOC^2));
            pd = makedist('Normal','mu',muSOC,'sigma',sigmaSOC);
            DAmSOC_matlab(i,:) = pdf(pd,csi_NASA);
        end
end

for k=1:1:size(DAmSOC,2)
    DAmSOC_NASA(k)=sum(DAmSOC(:,k));
    DAmSOC_matlab_NASA(k) = sum(DAmSOC_matlab(:,k));
end

figure('units','normalized','outerposition',[0 0 1 1],'Visible', 'on')
histogram(log10(Am_CST_LOFT1),'Normalization','probability','BinWidth',0.035)
grid on
hold on
histogram(log10(Am_CST_LOFT1_EMI_config),'Normalization','probability','BinWidth',0.035)
hold on
histogram(log10(Am_CST_LOFT1_all_continuum),'Normalization','probability','BinWidth',0.035)
hold on
histogram(log10(Am_CST_LOFT1_no_links),'Normalization','probability','BinWidth',0.035)
hold on
plot(csi_NASA,DAmSOC_NASA./sum(DAmSOC_NASA),'-b','LineWidth',2)
hold on
title('Comparison among PDFs')
set(gca,'FontSize',20,'FontWeight','bold')
xlabel('log_{10}(A/m) [m^{2}/kg]')
ylabel('Relative number N(A/m)')
legend('CST-LOFT1','CST-LOFT1 in EMI configuration','CST-LOFT1 all CONTINUUM links','CST-LOFT1 no links','NASA - SBM','Location','northwest')
set(gca,'XLim',xx_lim_Am)
set(gca,'YLim',yy_lim_Am)

out_fig = 'A_su_m_CST_LOFT1_data';
saveas(gcf,out_fig,'fig')
saveas(gcf,out_fig,'png')

%% plot finale LOFT6 A su M
%{d
clear DAmSOC DAmSOC_matlab DAmSOC_NASA csi_min csi_max csi_NASA n
lambda_c_min=log10(min([s_LOFT6 s_LOFT6_all_continuum s_LOFT6_no_links]));
lambda_c_max=log10(max([s_LOFT6 s_LOFT6_all_continuum s_LOFT6_no_links]));
lambda_c=lambda_c_min:(lambda_c_max-lambda_c_min)/30:lambda_c_max;
Am_LOFT6 =[Am_CST_LOFT6 Am_CST_LOFT6_all_continuum Am_CST_LOFT6_no_links];
csi_min=log10(min(Am_LOFT6(Am_LOFT6>0)));
csi_max=log10(max(Am_LOFT6));
n=length(lambda_c);
csi_NASA=csi_min:(csi_max-csi_min)/(n-1):csi_max;

for i=1:1:n
        if  lambda_c(i)<=-3.5
            muSOC=-0.3;
            sigmaSOC=0.2;
            DAmSOC(i,:)=(1/(sigmaSOC*(2*pi)^0.5))*exp(-((csi_NASA-muSOC).^2)/(2*sigmaSOC^2));
            pd = makedist('Normal','mu',muSOC,'sigma',sigmaSOC);
            DAmSOC_matlab(i,:) = pdf(pd,csi_NASA);
        elseif lambda_c(i)>-3.5 && lambda_c(i)<=-1.75
            muSOC=-0.3;
            sigmaSOC=0.2+0.1333*(lambda_c(i)+3.5);
            DAmSOC(i,:)=(1/(sigmaSOC*(2*pi)^0.5))*exp(-((csi_NASA-muSOC).^2)/(2*sigmaSOC^2));
            pd = makedist('Normal','mu',muSOC,'sigma',sigmaSOC);
            DAmSOC_matlab(i,:) = pdf(pd,csi_NASA);
        elseif lambda_c(i)>-1.75 && lambda_c(i)<-1.25
            muSOC=-0.3-1.4*(lambda_c(i)+1.75);
            sigmaSOC=0.2+0.1333*(lambda_c(i)+3.5);
            DAmSOC(i,:)=(1/(sigmaSOC*(2*pi)^0.5))*exp(-((csi_NASA-muSOC).^2)/(2*sigmaSOC^2));
            pd = makedist('Normal','mu',muSOC,'sigma',sigmaSOC);
            DAmSOC_matlab(i,:) = pdf(pd,csi_NASA);
        elseif lambda_c(i)>=-1.25
            muSOC=-1.0;
            sigmaSOC=0.2+0.1333*(lambda_c(i)+3.5);
            DAmSOC(i,:)=(1/(sigmaSOC*(2*pi)^0.5))*exp(-((csi_NASA-muSOC).^2)/(2*sigmaSOC^2));
            pd = makedist('Normal','mu',muSOC,'sigma',sigmaSOC);
            DAmSOC_matlab(i,:) = pdf(pd,csi_NASA);
        end
end

for k=1:1:size(DAmSOC,2)
    DAmSOC_NASA(k)=sum(DAmSOC(:,k));
    DAmSOC_matlab_NASA(k) = sum(DAmSOC_matlab(:,k));
end

figure('units','normalized','outerposition',[0 0 1 1],'Visible', 'on')
histogram(log10(Am_CST_LOFT6),'Normalization','probability','BinWidth',0.035)
grid on
hold on
histogram(log10(Am_CST_LOFT6_all_continuum),'Normalization','probability','BinWidth',0.035)
hold on
histogram(log10(Am_CST_LOFT6_no_links),'Normalization','probability','BinWidth',0.035)
hold on
histogram(log10(Am_CST_LOFT6_c_mat2),'Normalization','probability','BinWidth',0.035)
hold on
% histogram(log10(Am_CST_LOFT6_c_mat4),'Normalization','probability','BinWidth',0.035)
% hold on
plot(csi_NASA,DAmSOC_NASA./sum(DAmSOC_NASA),'-b','LineWidth',2)
hold on
title('Comparison among PDFs')
set(gca,'FontSize',20,'FontWeight','bold')
xlabel('log_{10}(A/m) [m^{2}/kg]')
ylabel('Relative number N(A/m)')
legend('CST-LOFT6','CST-LOFT6 all CONTINUUM links','CST-LOFT6 no links','CST-LOFT6 2xdiss. links','NASA - SBM','Location','northwest')
set(gca,'XLim',xx_lim_Am)
set(gca,'YLim',yy_lim_Am)

out_fig = 'A_su_m_CST_LOFT6_data';
saveas(gcf,out_fig,'fig')
saveas(gcf,out_fig,'png')


%% comparison finale LOFT6 A su M CST vs EMI
data_Am_EMI_loft6 = [-1.1513973108357503, 121.81308648484992;
-1.044804757301462, 104.57860971965965;
-1.0416044174942145, 1050.814853873122;
-0.9468392067258402, 1014.8812139320999;
-0.9432141078595078, 1423.0893165773214;
-0.8563728505726926, 1424.1634199451237;
-0.8336995049359932, 2704.5922801261586;
-0.7469314819697104, 2798.429856167795;
-0.7482350528751796, 4449.619669762038;
-0.637739110056537, 4488.092099481502;
-0.6434806807862437, 11760.748357110078;
-0.5408793977209481, 11799.123141068832;
-0.5375032955444241, 12522.727050805091;
-0.4309107420101359, 12505.492574039901;
-0.4129903037759619, 4806.270810752752;
-0.31034508011834694, 4788.987511107207;
-0.30509417933620453, 3137.846520393319;
-0.21822362832117692, 3101.8152346915886;
-0.20475583677534637, 1042.612609973541;
-0.10208131938951892, 988.2239212584591;
-0.10541348097372394, 208.96192791790236;
-0.006744880920995566, 228.7351944615366;
-0.006598412279931409, 43.20824911386808;
0.17104607903447855, 26.852584195057716;
0.18274160002343454, 212.5259981837935;
0.4156340627471655, 215.40654812471803;
0.4196692738084775, 104.13920379646879;
0.5144198377127451, 86.7582583902149];
if data_Am_EMI_loft6(1,2)~=0
    data_Am_EMI_loft6=[data_Am_EMI_loft6(1,1) 0;data_Am_EMI_loft6];
end
if data_Am_EMI_loft6(end,2)~=0
    data_Am_EMI_loft6=[data_Am_EMI_loft6; data_Am_EMI_loft6(end,1) 0];
end
for i=2:2:size(data_Am_EMI_loft6,1)-1
    data_Am_EMI_loft6(i,2)=(data_Am_EMI_loft6(i,2)+data_Am_EMI_loft6(i+1,2))/2;
    data_Am_EMI_loft6(i+1,2)=data_Am_EMI_loft6(i,2);
end
for i=1:2:size(data_Am_EMI_loft6,1)-1
    data_Am_EMI_loft6(i,1)=(data_Am_EMI_loft6(i,1)+data_Am_EMI_loft6(i+1,1))/2;
    data_Am_EMI_loft6(i+1,1)=data_Am_EMI_loft6(i,1);
end
data_NASA_SBM_Am_EMI_loft6=[-1.1012537715675077, 1606.663346710804; % -2 sigma
-0.3081187567741748, 11968.977941822659; % mu
0.5013621583618941, 1626.4854361347934]; % + 2 sigma
mu = data_NASA_SBM_Am_EMI_loft6(2,1);
sigma = (data_NASA_SBM_Am_EMI_loft6(3,1)-data_NASA_SBM_Am_EMI_loft6(1,1))/4;
pd = makedist('Normal','mu',mu,'sigma',sigma);
x=-2:0.01:1.5;
NASA = pdf(pd,x);
n_frag_EMI=0;
for i=2:2:size(data_Am_EMI_loft6,1)-1
    n_frag_EMI=n_frag_EMI+data_Am_EMI_loft6(i,2);
end
figure('units','normalized','outerposition',[0 0 1 1],'Visible', 'on')
box on
grid on
hold on
histogram(log10(Am_CST_LOFT6),'BinWidth',0.15)
hold on
plot(data_Am_EMI_loft6(:,1),data_Am_EMI_loft6(:,2),'-r','LineWidth',3)
hold on
plot(x,NASA*data_NASA_SBM_Am_EMI_loft6(2,2),'-b','LineWidth',3)
hold on
title('Comparison EMI-CST results LOFT 6')
xlabel('log_{10}(A/m) [m^2/kg]')
ylabel('N(A/m)')
legend('CST simulation','EMI','NASA - SBM','Location','northwest')
set(gca,'FontSize',20,'FontWeight','bold')
set(gca,'XLim',[xx_lim_Am(1) 0.55])
disp(' ')
disp(['For LOFT6, EMI has ',num2str(ceil(n_frag_EMI),'%d'),' fragments,']);
disp(['while CST has ',num2str(length(N_LOFT6),'%d'),' fragments.'])
disp(' ')

out_fig = 'A_su_m_EMIvsCST_LOFT6_data';
saveas(gcf,out_fig,'fig')
saveas(gcf,out_fig,'png')

%% Plot LOFT cases CST A su M

clear DAmSOC DAmSOC_matlab DAmSOC_NASA csi_min csi_max csi_NASA n
lambda_c_min=log10(min([s_LOFT1 s_LOFT2 s_LOFT3 s_LOFT4 s_LOFT5 s_LOFT6]));
lambda_c_max=log10(max([s_LOFT1 s_LOFT2 s_LOFT3 s_LOFT4 s_LOFT5 s_LOFT6]));
lambda_c=lambda_c_min:(lambda_c_max-lambda_c_min)/35:lambda_c_max;
Am_LOFT =[Am_CST_LOFT1 Am_CST_LOFT2 Am_CST_LOFT3 Am_CST_LOFT4 Am_CST_LOFT5 Am_CST_LOFT6];
csi_min=log10(min(Am_LOFT(Am_LOFT>0)));
csi_max=log10(max(Am_LOFT));
n=length(lambda_c);
csi_NASA=csi_min:(csi_max-csi_min)/(n-1):csi_max;

for i=1:1:n
        if  lambda_c(i)<=-3.5
            muSOC=-0.3;
            sigmaSOC=0.2;
            DAmSOC(i,:)=(1/(sigmaSOC*(2*pi)^0.5))*exp(-((csi_NASA-muSOC).^2)/(2*sigmaSOC^2));
            pd = makedist('Normal','mu',muSOC,'sigma',sigmaSOC);
            DAmSOC_matlab(i,:) = pdf(pd,csi_NASA);
        elseif lambda_c(i)>-3.5 && lambda_c(i)<=-1.75
            muSOC=-0.3;
            sigmaSOC=0.2+0.1333*(lambda_c(i)+3.5);
            DAmSOC(i,:)=(1/(sigmaSOC*(2*pi)^0.5))*exp(-((csi_NASA-muSOC).^2)/(2*sigmaSOC^2));
            pd = makedist('Normal','mu',muSOC,'sigma',sigmaSOC);
            DAmSOC_matlab(i,:) = pdf(pd,csi_NASA);
        elseif lambda_c(i)>-1.75 && lambda_c(i)<-1.25
            muSOC=-0.3-1.4*(lambda_c(i)+1.75);
            sigmaSOC=0.2+0.1333*(lambda_c(i)+3.5);
            DAmSOC(i,:)=(1/(sigmaSOC*(2*pi)^0.5))*exp(-((csi_NASA-muSOC).^2)/(2*sigmaSOC^2));
            pd = makedist('Normal','mu',muSOC,'sigma',sigmaSOC);
            DAmSOC_matlab(i,:) = pdf(pd,csi_NASA);
        elseif lambda_c(i)>=-1.25
            muSOC=-1.0;
            sigmaSOC=0.2+0.1333*(lambda_c(i)+3.5);
            DAmSOC(i,:)=(1/(sigmaSOC*(2*pi)^0.5))*exp(-((csi_NASA-muSOC).^2)/(2*sigmaSOC^2));
            pd = makedist('Normal','mu',muSOC,'sigma',sigmaSOC);
            DAmSOC_matlab(i,:) = pdf(pd,csi_NASA);
        end
end

for k=1:1:size(DAmSOC,2)
    DAmSOC_NASA(k)=sum(DAmSOC(:,k));
    DAmSOC_matlab_NASA(k) = sum(DAmSOC_matlab(:,k));
end

figure('units','normalized','outerposition',[0 0 1 1],'Visible', 'on')
histogram(log10(Am_CST_LOFT1),'Normalization','probability','BinWidth',0.035)
grid on
hold on
histogram(log10(Am_CST_LOFT2),'Normalization','probability','BinWidth',0.035)
hold on
histogram(log10(Am_CST_LOFT3),'Normalization','probability','BinWidth',0.035)
hold on
histogram(log10(Am_CST_LOFT4),'Normalization','probability','BinWidth',0.035)
hold on
histogram(log10(Am_CST_LOFT5),'Normalization','probability','BinWidth',0.035)
hold on
histogram(log10(Am_CST_LOFT6),'Normalization','probability','BinWidth',0.035)
hold on
plot(csi_NASA,DAmSOC_NASA./sum(DAmSOC_NASA),'-b','LineWidth',2)
hold on
title('Comparison among PDFs')
set(gca,'FontSize',20,'FontWeight','bold')
xlabel('log_{10}(A/m) [m^{2}/kg]')
ylabel('Relative number N(A/m)')
legend('CST-LOFT1','CST-LOFT2','CST-LOFT3','CST-LOFT4','CST-LOFT5','CST-LOFT6','NASA - SBM');
set(gca,'XLim',xx_lim_Am)
set(gca,'YLim',yy_lim_Am)

out_fig = 'A_su_m_CST_baseline_data';
saveas(gcf,out_fig,'fig')
saveas(gcf,out_fig,'png')

%% Plot LOFT2 CST A su M
figure('units','normalized','outerposition',[0 0 1 1],'Visible', 'on')
histogram(log10(Am_CST_LOFT2),'Normalization','probability','BinWidth',0.035)
grid on
hold on
plot(csi_NASA,DAmSOC_NASA./sum(DAmSOC_NASA),'-b','LineWidth',2)
hold on
title('Comparison among PDFs')
set(gca,'FontSize',20,'FontWeight','bold')
xlabel('log_{10}(A/m) [m^{2}/kg]')
ylabel('Relative number N(A/m)')
legend('CST-LOFT2','NASA - SBM');
set(gca,'XLim',xx_lim_Am)
set(gca,'YLim',yy_lim_Am)

out_fig = 'A_su_m_CST_LOFT2_data';
saveas(gcf,out_fig,'fig')
saveas(gcf,out_fig,'png')

%% Plot LOFT3 CST A su M
figure('units','normalized','outerposition',[0 0 1 1],'Visible', 'on')
histogram(log10(Am_CST_LOFT3),'Normalization','probability','BinWidth',0.035)
grid on
hold on
plot(csi_NASA,DAmSOC_NASA./sum(DAmSOC_NASA),'-b','LineWidth',2)
hold on
title('Comparison among PDFs')
set(gca,'FontSize',20,'FontWeight','bold')
xlabel('log_{10}(A/m) [m^{2}/kg]')
ylabel('Relative number N(A/m)')
legend('CST-LOFT3','NASA - SBM');
set(gca,'XLim',xx_lim_Am)
set(gca,'YLim',yy_lim_Am)

out_fig = 'A_su_m_CST_LOFT3_data';
saveas(gcf,out_fig,'fig')
saveas(gcf,out_fig,'png')

%% Plot LOFT4 CST A su M
figure('units','normalized','outerposition',[0 0 1 1],'Visible', 'on')
histogram(log10(Am_CST_LOFT4),'Normalization','probability','BinWidth',0.035)
grid on
hold on
plot(csi_NASA,DAmSOC_NASA./sum(DAmSOC_NASA),'-b','LineWidth',2)
hold on
title('Comparison among PDFs')
set(gca,'FontSize',20,'FontWeight','bold')
xlabel('log_{10}(A/m) [m^{2}/kg]')
ylabel('Relative number N(A/m)')
legend('CST-LOFT4','NASA - SBM');
set(gca,'XLim',xx_lim_Am)
set(gca,'YLim',yy_lim_Am)

out_fig = 'A_su_m_CST_LOFT4_data';
saveas(gcf,out_fig,'fig')
saveas(gcf,out_fig,'png')

%% Plot LOFT5 CST A su M
figure('units','normalized','outerposition',[0 0 1 1],'Visible', 'on')
histogram(log10(Am_CST_LOFT5),'Normalization','probability','BinWidth',0.035)
grid on
hold on
plot(csi_NASA,DAmSOC_NASA./sum(DAmSOC_NASA),'-b','LineWidth',2)
hold on
title('Comparison among PDFs')
set(gca,'FontSize',20,'FontWeight','bold')
xlabel('log_{10}(A/m) [m^{2}/kg]')
ylabel('Relative number N(A/m)')
legend('CST-LOFT5','NASA - SBM');
set(gca,'XLim',xx_lim_Am)
set(gca,'YLim',yy_lim_Am)

out_fig = 'A_su_m_CST_LOFT1_data';
saveas(gcf,out_fig,'fig')
saveas(gcf,out_fig,'png')


%% plot finale of Delta_v
%{
clear DdvCOLL_NASA DdvCOLL_NASA_1

muEXP=0.9*csi_NASA+2.9;
sigmaEXP=0.4;
delta_v_lim=[0 3];
delta_v_NASA=delta_v_lim(1):abs(delta_v_lim(1)-delta_v_lim(2))/(n-1):delta_v_lim(2);

for i=1:1:n
    DdvCOLL_NASA(:,i)=(1/(sigmaEXP*(2*pi)^0.5))*exp(-((delta_v_NASA-muEXP(i)).^2)/(2*sigmaEXP^2));
end

for k=1:1:size(DdvCOLL_NASA,1)
    DdvCOLL_NASA_1(k)=sum(DdvCOLL_NASA(:,k));
end

figure
histogram(delta_v_CST_baseline,'Normalization','probability','BinWidth',1)
grid on
hold on
histogram(delta_v_CST_continuum,'Normalization','probability','BinWidth',1)
hold on
histogram(delta_v_CST_bolted,'Normalization','probability','BinWidth',1)
hold on
histogram(delta_v_CST_no_links,'Normalization','probability','BinWidth',1)
hold on
% plot(delta_v_NASA,DdvCOLL_NASA_1./sum(DdvCOLL_NASA_1),'b','LineWidth',2)
title('Comparison among PDFs')
xlabel('\DeltaV [m/s] ')
ylabel('Relative Number of Fragments per \DeltaV bin')
legend('CST baseline','CST (all continuum links)','CST (all bolted links)','CST (no links)','NASA SBM')
xlim([0 60000])
%}

%% plot cpu time and simulation time
if exist('Data_Simulation.txt','file')
    delete('Data_Simulation.txt')
end
diary Data_Simulation.txt
diary on
disp('--------------------------------------------------------------------------------------------')
disp('SIMULATION                   CPUTIME (min)       SIMULATION TIME (s)             FINAL STEP')
disp(['LOFT1 all continuum   ',cputime_LOFT1_all_continuum,tf_LOFT1_all_continuum,final_step_LOFT1_all_continuum])
disp(['LOFT1 no links        ',cputime_LOFT1_no_links,tf_LOFT1_no_links,final_step_LOFT1_no_links])
disp(['LOFT1 EMI config      ',cputime_LOFT1_EMI_config,tf_LOFT1_EMI_config,final_step_LOFT1_EMI_config])
disp(['LOFT1 baseline        ',cputime_LOFT1,tf_LOFT1,final_step_LOFT1])
disp(['LOFT2 baseline        ',cputime_LOFT2,tf_LOFT2,final_step_LOFT2])
disp(['LOFT3 baseline        ',cputime_LOFT3,tf_LOFT3,final_step_LOFT3])
disp(['LOFT4 baseline        ',cputime_LOFT4,tf_LOFT4,final_step_LOFT4])
disp(['LOFT5 baseline        ',cputime_LOFT5,tf_LOFT5,final_step_LOFT5])
disp(['LOFT6 baseline        ',cputime_LOFT6,tf_LOFT6,final_step_LOFT6])
disp(['LOFT6 all continuum   ',cputime_LOFT6_all_continuum,tf_LOFT6_all_continuum,final_step_LOFT6_all_continuum])
disp(['LOFT6 no links        ',cputime_LOFT6_no_links,tf_LOFT6_no_links,final_step_LOFT6_no_links])
disp(['LOFT6 2xdiss. links   ',cputime_LOFT6_c_mat2,tf_LOFT6_c_mat2,final_step_LOFT6_c_mat2])
% disp(['LOFT6 4xdiss. links   ',cputime_LOFT6_c_mat4,tf_LOFT6_c_mat4,final_step_LOFT6_c_mat4])
disp('--------------------------------------------------------------------------------------------')
diary off