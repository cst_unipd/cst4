function Plot_Fragmentation_Gaussian_Cartesian(num,Frag_Volume,Frag_Data,Impact_Data,zv,seeds_F,Frag_Volume_i_centros,Delta_v,imin,Frag_ME)

figure
drawMesh(Frag_ME.v,Frag_ME.f,'facecolor','c','facealpha',0.1);
hold on;
drawMesh(Frag_Volume{num}.v,Frag_Volume{num}.f,'facecolor','r','facealpha',0.3);
hold on;
% axis equal
plot3(Frag_Volume{num}.v(:,1),Frag_Volume{num}.v(:,2),...
    Frag_Volume{num}.v(:,3),'ok')
hold on;
if ~iscell(Frag_Volume{num}.f)
    plot3(Frag_Volume{num}.v(Frag_Volume{num}.f(imin,:),1),Frag_Volume{num}.v(Frag_Volume{num}.f(imin,:),2),...
        Frag_Volume{num}.v(Frag_Volume{num}.f(imin,:),3),'dk',...
        'MarkerSize',20,'MarkerFaceColor','k');
else
    plot3(Frag_Volume{num}.v(Frag_Volume{num}.f{imin},1),Frag_Volume{num}.v(Frag_Volume{num}.f{imin},2),...
        Frag_Volume{num}.v(Frag_Volume{num}.f{imin},3),'dk',...
        'MarkerSize',20,'MarkerFaceColor','k');
end
hold on;
plot3(Frag_Volume_i_centros(:,1),Frag_Volume_i_centros(:,2),...
    Frag_Volume_i_centros(:,3),'sk')
hold on;
line([Frag_Data(num,1) Frag_Data(num,1)+Delta_v(1)],...
    [Frag_Data(num,2) Frag_Data(num,2)+Delta_v(2)],...
    [Frag_Data(num,3) Frag_Data(num,3)+Delta_v(3)],'Color','b');
hold on;
pause % plot impact point
plot3(Frag_Data(num,1),Frag_Data(num,2),Frag_Data(num,3),'s','MarkerSize',30,...
    'MarkerFaceColor','r')
hold on; 
text(Frag_Data(num,1),Frag_Data(num,2),Frag_Data(num,3),'Impact Point',...
    'FontSize',24,'FontWeight','bold');
pause % plot normal vector
hold on; 
line([Frag_Data(num,1) Frag_Data(num,1)+0.01*zv(1)],...
    [Frag_Data(num,2) Frag_Data(num,2)+0.01*zv(2)],...
    [Frag_Data(num,3) Frag_Data(num,3)+0.01*zv(3)],'Color','k');
hold on; 
text(Frag_Data(num,1)+0.005*zv(1),Frag_Data(num,2)+0.005*zv(2),...
    Frag_Data(num,3)+0.005*zv(3),'zv','FontSize',24,'FontWeight','bold');
pause % plot impact velocity
av=norm(Impact_Data.I_VEL_F(num,:));
av=Impact_Data.I_VEL_F(num,:)/norm(Impact_Data.I_VEL_F(num,:));
hold on; 
line([Frag_Data(num,1) Frag_Data(num,1)+0.01*av(1)],...
    [Frag_Data(num,2) Frag_Data(num,2)+0.01*av(2)],...
    [Frag_Data(num,3) Frag_Data(num,3)+0.01*av(3)],'Color','b');
hold on; 
text(Frag_Data(num,1)+0.005*av(1),Frag_Data(num,2)+0.005*av(2),...
    Frag_Data(num,3)+0.005*av(3),'Velocit�','FontSize',24,'FontWeight','bold');
pause % plot seeds
plot3(seeds_F(:,1),seeds_F(:,2),seeds_F(:,3),'*b')
hold on; 