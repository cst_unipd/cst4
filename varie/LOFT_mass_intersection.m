function [m_tot,volume_tot]=LOFT_mass_intersection(i_impactor_1,ME,caso_loft)
n_ME=length(ME);

ME_c_hull=struct('c_hull_global',[]);
for i=1:n_ME
    q0=ME(i).DYNAMICS_INITIAL_DATA.quaternions0(1);
    q1=ME(i).DYNAMICS_INITIAL_DATA.quaternions0(2);
    q2=ME(i).DYNAMICS_INITIAL_DATA.quaternions0(3);
    q3=ME(i).DYNAMICS_INITIAL_DATA.quaternions0(4);
    Rot_l2g=[q0^2+q1^2-q2^2-q3^2, 2*(q1*q2-q0*q3), 2*(q0*q2+q1*q3);
        2*(q1*q2+q0*q3)    , q0^2-q1^2+q2^2-q3^2, 2*(q2*q3-q0*q1);
        2*(q1*q3-q0*q2)    , 2*(q0*q1+q2*q3),     q0^2-q1^2-q2^2+q3^2];
    
    Rot_l2g=Rot_l2g'; %transpose the matrix
    c_hull_global=ME(i).GEOMETRY_DATA.c_hull*0;
    for jj=1:length(ME(i).GEOMETRY_DATA.c_hull)
        c_hull_global(jj,:)=ME(i).DYNAMICS_INITIAL_DATA.cm_coord0+Rot_l2g*[ME(i).GEOMETRY_DATA.c_hull(jj,:)'];
    end
    ME_c_hull(i).c_hull_global=c_hull_global; %c_hull vertices in global system
end

%% bounding box of the entire system
c_hull_system=[];
for i=1:n_ME
   c_hull_system=[c_hull_system; ME_c_hull(i).c_hull_global]; %c_hull vertices in global system
end

box_system = boundingBox3d(c_hull_system);
c_hull_system=[];
[c_hull_system, ~, ~] = createBox(box_system(1),box_system(3),box_system(5),box_system(2)-box_system(1),box_system(4)-box_system(3),box_system(6)-box_system(5));

%% bounding box of the impactor
for i=i_impactor_1+1:n_ME
    ME_c_hull(i_impactor_1).c_hull_global=[ME_c_hull(i_impactor_1).c_hull_global;ME_c_hull(i).c_hull_global]; %c_hull vertices in global system
end

box_impactor = boundingBox3d(ME_c_hull(i_impactor_1).c_hull_global);
ME_c_hull(i_impactor_1:end)=[];
[ME_c_hull(i_impactor_1).c_hull_global, ~, ~] = createBox(box_impactor(1),box_impactor(3),box_impactor(5),box_impactor(2)-box_impactor(1),box_impactor(4)-box_impactor(3),box_impactor(6)-box_impactor(5));

%% creation of the prismatic volume of impactor projections on the system bounding box
points_sys=[];
for j=1:length(ME_c_hull(i_impactor_1).c_hull_global)
    % create lines from each impactor c_hull vertex with
    % inclination vel0
    impactor_lines(j,:)=createLine3d(ME_c_hull(i_impactor_1).c_hull_global(j,1),...
        ME_c_hull(i_impactor_1).c_hull_global(j,2),ME_c_hull(i_impactor_1).c_hull_global(j,3),...
        ME(i_impactor_1).DYNAMICS_INITIAL_DATA.vel0(1),ME(i_impactor_1).DYNAMICS_INITIAL_DATA.vel0(2),ME(i_impactor_1).DYNAMICS_INITIAL_DATA.vel0(3));
    points_sys = [points_sys; intersectLineConvexHull(impactor_lines(j,:),c_hull_system)]; %finding intersections between impactor lines and c_hull_systems
end

CENTRO = polyhedronCentroid(unique(ME_c_hull(i_impactor_1).c_hull_global,'rows'), minConvexHull(unique(ME_c_hull(i_impactor_1).c_hull_global,'rows')));
PLANE = createPlane(CENTRO, ME(i_impactor_1).DYNAMICS_INITIAL_DATA.vel0'/norm(ME(i_impactor_1).DYNAMICS_INITIAL_DATA.vel0));
c_hull_imp = projPointOnPlane(unique(ME_c_hull(i_impactor_1).c_hull_global,'rows'), PLANE);
points_sys2 = [];
for jj=1:size(points_sys,1)
    valid_point = ((points_sys(jj,:)-CENTRO)*ME(i_impactor_1).DYNAMICS_INITIAL_DATA.vel0)>0;
    if valid_point
        points_sys2=[points_sys2; points_sys(jj,:)];
    end
end
% for iii=1:size(ME_c_hull(i_impactor_1).c_hull_global,1)
%     minim(iii)=(ME_c_hull(i_impactor_1).c_hull_global(iii,:)-ME_c_hull(i_impactor_1).c_hull_global(2,:))*ME(i_impactor_1).DYNAMICS_INITIAL_DATA.vel0;
% end
% [~,I] = mink(minim,round(length(minim)/2));

points_sys = [];
% points_sys = [points_sys2; ME_c_hull(i_impactor_1).c_hull_global(I,:)];
points_sys = [points_sys2; c_hull_imp];
if false
    plot3(ME_c_hull(i_impactor_1).c_hull_global(:,1),ME_c_hull(i_impactor_1).c_hull_global(:,2),ME_c_hull(i_impactor_1).c_hull_global(:,3),'ok')
    hold on
    plot3(c_hull_system(:,1),c_hull_system(:,2),c_hull_system(:,3),'or')
    hold on
    plot3(points_sys(:,1),points_sys(:,2),points_sys(:,3),'*k')
    hold on
    plot3(bnd_pnts(:,1),bnd_pnts(:,2),bnd_pnts(:,3),'db')
    hold on
    drawMesh(points_sys,minConvexHull(points_sys))
    hold on
    drawMesh(ME_c_hull(i).c_hull_global,minConvexHull(ME_c_hull(i).c_hull_global),'facecolor','c')
    hold on
end

%%

% ME_c_hull(i_impactor_1:end)=[];
% ME_c_hull(i_impactor_1).c_hull_global=c_hull_imp;

% volume_intersection=[];
% volume_tot=0;
% m_tot=0;
% for i=1:i_impactor_1-1
%     [bnd_pnts, flag] = getBoundariesIntersection(points_sys,ME_c_hull(i).c_hull_global);
%     rho=0;
%     if(~isempty(bnd_pnts))
%         volume_intersection(i)=0;
%         m_intersection(i)=0;
%         [~,volume_intersection(i)] = polyhedronCentroidVolume(unique(bnd_pnts,'rows'));
%         [~,volume_ME] = polyhedronCentroidVolume(ME_c_hull(i).c_hull_global);
%         rho = ME(i).GEOMETRY_DATA.mass0/volume_ME;
%         m_intersection(i)=volume_intersection(i)*rho;
%         disp(['Intersecting ME(',num2str(i),'): volume = ',num2str(volume_intersection(i)),' m^3; mass = ',num2str(m_intersection(i)),' kg']);
%         m_tot=m_tot+m_intersection(i);
%         volume_tot=volume_tot+volume_intersection(i);
%     end

% end

%%
if false
    a=ME_c_hull(i).c_hull_global;
    hold on
    plot3(a(:,1),a(:,2),a(:,3),'db')
end

volume_intersection=[];
volume_tot=0;
m_tot=0;
k=1;
% for jj=i_impactor_1:length(ME)
i_impactor=i_impactor_1;
for i=1:i_impactor_1-1
    impactor_lines=[];
    points=[];
    for j=1:length(ME_c_hull(i_impactor).c_hull_global)
        if (ME(i_impactor).DYNAMICS_INITIAL_DATA.vel0'*(ME(i).DYNAMICS_INITIAL_DATA.cm_coord0-ME(i_impactor).DYNAMICS_INITIAL_DATA.cm_coord0)>0) %% se l'ME si trova nel verso della velocit�
            % create lines from each impactor c_hull vertex with
            % inclination vel0
            impactor_lines(j,:)=createLine3d(ME_c_hull(i_impactor).c_hull_global(j,1),...
                ME_c_hull(i_impactor).c_hull_global(j,2),ME_c_hull(i_impactor_1).c_hull_global(j,3),...
                ME(i_impactor).DYNAMICS_INITIAL_DATA.vel0(1),ME(i_impactor_1).DYNAMICS_INITIAL_DATA.vel0(2),ME(i_impactor).DYNAMICS_INITIAL_DATA.vel0(3));
            points = [points; intersectLineConvexHull(impactor_lines(j,:),ME_c_hull(i).c_hull_global)]; %finding intersections between impactor lines and ME c_hull
        end
    end
    rho=0;
    if(~isempty(points))
        ME_int(k) = i;
        volume_intersection(k)=0;
        m_intersection(k)=0;
        warning off
        [~,volume_intersection(k)] = polyhedronCentroidVolume(unique(points,'rows'));
        warning on
        if volume_intersection(k) <= 0 %intersecanti di striscio
            [bnd_pnts, ~] = getBoundariesIntersection(points_sys,ME_c_hull(i).c_hull_global);
            [~,volume_intersection(k)] = polyhedronCentroidVolume(unique(bnd_pnts,'rows'));
        end
        [~,volume_ME] = polyhedronCentroidVolume(ME_c_hull(i).c_hull_global);
        rho = ME(i).GEOMETRY_DATA.mass0/volume_ME;
        m_intersection(k)=volume_intersection(k)*rho;
        disp(['Intersecting ME(',num2str(i),'): volume = ',num2str(volume_intersection(k)),' m^3; mass = ',num2str(m_intersection(k)),' kg']);
        m_tot=m_tot+m_intersection(k);
        volume_tot=volume_tot+volume_intersection(k);
        k=k+1;
    end
end
if true
    plot_inters(ME_c_hull,ME_int,points_sys,caso_loft)
end
disp(['Total_mass = ',num2str(m_tot),' kg']);
disp(['Total_volume = ',num2str(volume_tot),' m3']);
end