% Standalone for fragments size distribution
clear all
close all
clc

%% Load Data CST
name_file_result_LOFT8_pre='N:\CST3_da_64GB_17122018_LOFT8_AF\Results\LOFT8_AF_20190109T093714\data\step_1119.mat';
name_file_result_LOFT8_post='N:\CST3_da_64GB_17122018_LOFT8_AF\Results\LOFT8_AF_20190110T120312\data\step_1343.mat';


%% processing LOFT8 pre scenario data
load(name_file_result_LOFT8_pre,'FRAGMENTS','ME','BUBBLE','PROPAGATION_THRESHOLD','link_data','tf','RunTime')
name_mat=[name_file_result_LOFT8_pre(1:strfind(name_file_result_LOFT8_pre,'Results')-1),filesep,'set_up',filesep,'material_list.m'];
run(name_mat)

mass_imp_1=0;
mass_LOFT=0;
q_imp_1 = [0 0 0]';

for i = 1:51
    mass_LOFT = mass_LOFT + ME(i).GEOMETRY_DATA.mass0;
end

for i = 52:length(ME)
    mass_imp_1 = mass_imp_1 + ME(i).GEOMETRY_DATA.mass0;
    q_imp_1 = q_imp_1 + ME(i).GEOMETRY_DATA.mass0*ME(i).DYNAMICS_INITIAL_DATA.vel0;
end
v_p_1 = 1/mass_imp_1*q_imp_1/1000;

load(name_file_result_LOFT8_pre,'FRAGMENTS','ME','BUBBLE','PROPAGATION_THRESHOLD','link_data','tf','RunTime')

[n_frags_bubbles_LOFT8_pre,s_LOFT8_pre,N_LOFT8_pre,Am_CST_LOFT8_pre,delta_v_CST_LOFT8_pre]=postprocess_data(FRAGMENTS,ME,BUBBLE,PROPAGATION_THRESHOLD,link_data);

tf_LOFT8_pre = sprintf('%25.4f',tf);
if exist('RunTime','var')
    cputime_LOFT8_pre = sprintf('%20.2f',RunTime/60);
else
    cputime_LOFT8_pre = '                   -';
end

final_step_LOFT8_pre = sprintf('%24d',str2double(name_file_result_LOFT8_pre(strfind(name_file_result_LOFT8_pre,'step_')+5:end-4)));
EMR_LOFT8_pre = sprintf('%5.2f',1/2*mass_imp_1*norm(v_p_1*1000)^2/(mass_imp_1+mass_LOFT)/1000);
clear FRAGMENTS ME BUBBLE PROPAGATION_THRESHOLD link_data tf RunTime

%% processing LOFT8 post scenario data

load(name_file_result_LOFT8_post,'FRAGMENTS','ME','BUBBLE','PROPAGATION_THRESHOLD','link_data','tf','RunTime')

[n_frags_bubbles_LOFT8_post,s_LOFT8_post,N_LOFT8_post,Am_CST_LOFT8_post,delta_v_CST_LOFT8_post]=postprocess_data(FRAGMENTS,ME,BUBBLE,PROPAGATION_THRESHOLD,link_data);

tf_LOFT8_post = sprintf('%25.4f',tf);
if exist('RunTime','var')
    cputime_LOFT8_post = sprintf('%20.2f',RunTime/60);
else
    cputime_LOFT8_post = '                   -';
end

final_step_LOFT8_post = sprintf('%24d',str2double(name_file_result_LOFT8_post(strfind(name_file_result_LOFT8_post,'step_')+5:end-4)));
clear FRAGMENTS BUBBLE PROPAGATION_THRESHOLD link_data tf RunTime

%% plot finale Lc
%{d
colorOrder =  [ ...
        0            0            0       ;...% 6 BLACK
        0            0            0.7       ;...% 1 BLUE
        0            0.7            0       ;...% 3 GREEN (pale)
        0.7            0            0.7       ;...% 5 MAGENTA (pale)
        0            0.8            0.8       ;...% 4 CYAN
        0.6          0.5          0.4     ;  % 15 BROWN (dark)
        1            1            0       ;...% 7 YELLOW (pale)
        0            0.75         0.75    ;...% 8 TURQUOISE
        0            0.5          0       ;...% 9 GREEN (dark)
        0.75         0.75         0       ;...% 10 YELLOW (dark)
        1            0.50         0.25    ;...% 11 ORANGE
        0.75         0            0.75    ;...% 12 MAGENTA (dark)
        0.7          0.7          0.7     ;...% 13 GREY
        0.8          0.7          0.6     ;...% 14 BROWN (pale)
        0.4          0.7          0.6     ;...
        0.4          0          0.6     ;...
        0.4          0.6          0     ;...
        0.6          0.5          0.4 ];  % 18 BROWN (dark)
    

Lc_NASA=[min([s_LOFT8_post s_LOFT8_pre]):(max([s_LOFT8_post s_LOFT8_pre])-min([s_LOFT8_post s_LOFT8_pre]))/100:max([s_LOFT8_post s_LOFT8_pre])];
NASA_SBM_10_sub = 0.1*(mass_imp_1*norm(v_p_1))^0.75*Lc_NASA.^(-1.71);
NASA_SBM_10_cat = 0.1*(mass_LOFT+mass_imp_1)^0.75*Lc_NASA.^(-1.71);

%% Plot Lc comparison LOFT6 CST

% xx_lim = [2e-03,1e0];
% yy_lim = [1e0,1e6];
figure('units','normalized','outerposition',[0 0 1 1],'Visible', 'on')
loglog(s_LOFT8_pre,[N_LOFT8_pre(1)+n_frags_bubbles_LOFT8_pre,N_LOFT8_pre(2:end)],'MarkerEdgeColor',colorOrder(9,:),'MarkerFaceColor',colorOrder(9,:),'Marker','*','LineStyle','none');
hold on
grid on
loglog(s_LOFT8_post,[N_LOFT8_post(1)+n_frags_bubbles_LOFT8_post,N_LOFT8_post(2:end)],'MarkerEdgeColor',colorOrder(11,:),'MarkerFaceColor',colorOrder(11,:),'Marker','o','LineStyle','none');
hold on
loglog(Lc_NASA,NASA_SBM_10_sub,'--r','LineWidth',2)
loglog(Lc_NASA,NASA_SBM_10_cat,'-k','LineWidth',2)
set(gca,'FontSize',20,'FontWeight','bold')
title(['Comparison CST results '])
xlabel('L_c [m]')
ylabel('Cumulative number CN (L_c)')
% legend('CST-LOFT6','CST-LOFT6 cmat*5','CST-LOFT6 cELOSS=0.2','CST-LOFT6 cmat*5 & cELOSS=0.2','LOFT7 AF','NASA - SBM SubCat 10 kg','NASA - SBM Cat');
legend('CST-LOFT8 pre','CST-LOFT8 post','NASA - SBM SubCat 10 kg','NASA - SBM Cat');
% set(gca,'XLim',xx_lim)
% set(gca,'YLim',yy_lim)

% out_fig = 'Lc_EMIvsCST_LOFT6_data_special_cases';
% saveas(gcf,out_fig,'fig')
% saveas(gcf,out_fig,'png')


%% plot cpu time and simulation time
% if exist('Data_Simulation.txt','file')
%     delete('Data_Simulation.txt')
% end
% diary Data_Simulation.txt
% diary on
disp('--------------------------------------------------------------------------------------------')
disp('SIMULATION                CPUTIME (min)       SIMULATION TIME (s)             FINAL STEP')
disp(['LOFT8 pre        ',cputime_LOFT8_pre,tf_LOFT8_pre,final_step_LOFT8_pre])
disp(['LOFT8 post       ',cputime_LOFT8_post,tf_LOFT8_post,final_step_LOFT8_post])
disp('--------------------------------------------------------------------------------------------')
% diary off