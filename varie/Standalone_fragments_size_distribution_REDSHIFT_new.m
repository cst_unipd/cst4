% Standalone for fragments size distribution

clear all
close all
clc

%% Load Data CST
name_file_result_REDSHIFT_baseline='E:\AO_8507Catastrophic\CST2_ESTEC\CST4_REDSHIFT_baseline\Results\RedSHIFT_Satellite_Baseline_20190220T133202\data\step_1997.mat';
name_file_result_REDSHIFT_weak_struct='E:\AO_8507Catastrophic\CST2_ESTEC\CST4_REDSHIFT_weak_struct\Results\RedSHIFT_Satellite_Weak_struct_20190220T133210\data\step_2229.mat';
name_file_result_REDSHIFT_weak_no_links='E:\AO_8507Catastrophic\CST2_ESTEC\CST4_REDSHIFT_weak_struct_no_links\Results\RedSHIFT_Satellite_Weak_struct_no_links_20190220T133220\data\step_1841.mat';
% name_file_result_REDSHIFT_baseline_no_links='E:\AO_8507Catastrophic\CST2_ESTEC\CST4_REDSHIFT_baseline_no_links\Results\RedSHIFT_Satellite_Baseline_no_links_20190220T133227\data\step_1464.mat';
title_fig='21022019_central_all';

% name_file_result_REDSHIFT_baseline='E:\AO_8507Catastrophic\CST2_ESTEC\CST4_REDSHIFT_baseline\Results\RedSHIFT_Satellite_Baseline_glancing_20190225T140920\data\step_2648.mat';
% name_file_result_REDSHIFT_weak_struct='E:\AO_8507Catastrophic\CST2_ESTEC\CST4_REDSHIFT_weak_struct\Results\RedSHIFT_Satellite_Weak_struct_glancing_20190225T141447\data\step_1772.mat';
% name_file_result_REDSHIFT_weak_no_links='E:\AO_8507Catastrophic\CST2_ESTEC\CST4_REDSHIFT_weak_struct_no_links\Results\RedSHIFT_Satellite_Weak_struct_no_links_glancing_20190225T143152\data\step_1638.mat';
% % name_file_result_REDSHIFT_baseline_no_links='E:\AO_8507Catastrophic\CST2_ESTEC\CST4_REDSHIFT_baseline_no_links\Results\RedSHIFT_Satellite_Baseline_no_links_glancing_20190225T141258\data\step_1400.mat';
% title_fig='26022019_glancing_all';


%% processing REDSHIFT_baseline scenario data
load(name_file_result_REDSHIFT_baseline,'FRAGMENTS','ME','BUBBLE','PROPAGATION_THRESHOLD','link_data','tf','RunTime')
name_mat=[name_file_result_REDSHIFT_baseline(1:strfind(name_file_result_REDSHIFT_baseline,'Results')-1),filesep,'set_up',filesep,'material_list.m'];
run(name_mat)
length_REDSHIFT_baseline=25;
mass_REDSHIFT_baseline=0;
mass_imp_1U=0;
for i = 1:length_REDSHIFT_baseline
    mass_REDSHIFT_baseline = mass_REDSHIFT_baseline + ME(i).GEOMETRY_DATA.mass0;
end

for i = length_REDSHIFT_baseline+1:length(ME)
    mass_imp_1U = mass_imp_1U + ME(i).GEOMETRY_DATA.mass0;
end

v_p_1U = ME(length(ME)).DYNAMICS_INITIAL_DATA.vel0/1000;

[n_frags_bubbles_REDSHIFT_baseline,s_REDSHIFT_baseline,N_REDSHIFT_baseline,Am_CST_REDSHIFT_baseline,delta_v_CST_REDSHIFT_baseline]=postprocess_data(FRAGMENTS,ME,BUBBLE,PROPAGATION_THRESHOLD,link_data,length_REDSHIFT_baseline);

tf_REDSHIFT_baseline = sprintf('%17.4f',tf);
if exist('RunTime','var')
    cputime_REDSHIFT_baseline = sprintf('%10.2f',RunTime/60);
else
    cputime_REDSHIFT_baseline = '         -';
end
final_step_REDSHIFT_baseline = sprintf('%16d',str2double(name_file_result_REDSHIFT_baseline(strfind(name_file_result_REDSHIFT_baseline,'step_')+5:end-4)));
EMR_REDSHIFT_baseline = sprintf('%5.2f',1/2*mass_imp_1U*norm(v_p_1U*1000)^2/(mass_imp_1U+mass_REDSHIFT_baseline)/1000);
clear FRAGMENTS ME BUBBLE PROPAGATION_THRESHOLD link_data tf RunTime



%% processing REDSHIFT_weak_struct scenario data
load(name_file_result_REDSHIFT_weak_struct,'FRAGMENTS','ME','BUBBLE','PROPAGATION_THRESHOLD','link_data','tf','RunTime')

length_REDSHIFT_weak=46;

[n_frags_bubbles_REDSHIFT_weak_struct,s_REDSHIFT_weak_struct,N_REDSHIFT_weak_struct,Am_CST_REDSHIFT_weak_struct,delta_v_CST_REDSHIFT_weak_struct]=postprocess_data(FRAGMENTS,ME,BUBBLE,PROPAGATION_THRESHOLD,link_data,length_REDSHIFT_weak);

tf_REDSHIFT_weak_struct = sprintf('%17.4f',tf);
if exist('RunTime','var')
    cputime_REDSHIFT_weak_struct = sprintf('%10.2f',RunTime/60);
else
    cputime_REDSHIFT_weak_struct = '         -';
end

final_step_REDSHIFT_weak_struct = sprintf('%16d',str2double(name_file_result_REDSHIFT_weak_struct(strfind(name_file_result_REDSHIFT_weak_struct,'step_')+5:end-4)));
clear FRAGMENTS ME BUBBLE PROPAGATION_THRESHOLD link_data tf RunTime

%% processing REDSHIFT_weak_no_links scenario data

load(name_file_result_REDSHIFT_weak_no_links,'FRAGMENTS','ME','BUBBLE','PROPAGATION_THRESHOLD','link_data','tf','RunTime')

[n_frags_bubbles_REDSHIFT_weak_no_links,s_REDSHIFT_weak_no_links,N_REDSHIFT_weak_no_links,Am_CST_REDSHIFT_weak_no_links,delta_v_CST_REDSHIFT_weak_no_links]=postprocess_data(FRAGMENTS,ME,BUBBLE,PROPAGATION_THRESHOLD,link_data,length_REDSHIFT_weak);

tf_REDSHIFT_weak_no_links = sprintf('%17.4f',tf);
if exist('RunTime','var')
    cputime_REDSHIFT_weak_no_links = sprintf('%10.2f',RunTime/60);
else
    cputime_REDSHIFT_weak_no_links = '         -';
end

final_step_REDSHIFT_weak_no_links = sprintf('%16d',str2double(name_file_result_REDSHIFT_weak_no_links(strfind(name_file_result_REDSHIFT_weak_no_links,'step_')+5:end-4)));
clear FRAGMENTS BUBBLE PROPAGATION_THRESHOLD link_data tf RunTime

% %% processing REDSHIFT_baseline_no_links scenario data
% 
% load(name_file_result_REDSHIFT_baseline_no_links,'FRAGMENTS','ME','BUBBLE','PROPAGATION_THRESHOLD','link_data','tf','RunTime')
% 
% [n_frags_bubbles_REDSHIFT_baseline_no_links,s_REDSHIFT_baseline_no_links,N_REDSHIFT_baseline_no_links,Am_CST_REDSHIFT_baseline_no_links,delta_v_CST_REDSHIFT_baseline_no_links]=postprocess_data(FRAGMENTS,ME,BUBBLE,PROPAGATION_THRESHOLD,link_data,length_REDSHIFT_baseline);
% 
% tf_REDSHIFT_baseline_no_links = sprintf('%17.4f',tf);
% if exist('RunTime','var')
%     cputime_REDSHIFT_baseline_no_links = sprintf('%10.2f',RunTime/60);
% else
%     cputime_REDSHIFT_baseline_no_links = '         -';
% end
% 
% final_step_REDSHIFT_baseline_no_links = sprintf('%16d',str2double(name_file_result_REDSHIFT_baseline_no_links(strfind(name_file_result_REDSHIFT_baseline_no_links,'step_')+5:end-4)));
% clear FRAGMENTS BUBBLE PROPAGATION_THRESHOLD link_data tf RunTime

%% plot finale Lc
%{t
colorOrder =  [ ...
        0            0            0       ;...% 1 BLACK
        0            0            0.7     ;...% 2 BLUE
        0            0.7          0       ;...% 3 GREEN (pale)
        0.7          0            0.7     ;...% 4 MAGENTA (pale)
        0            0.5         0.8     ;...% 5 CYAN
        0.6          0.5          0.4     ;  % 6 BROWN (dark)
        1            1            0       ;...% 7 YELLOW (pale)
        0            0.75         0.75    ;...% 8 TURQUOISE
        0            0.5          0       ;...% 9 GREEN (dark)
        0.75         0.75         0       ;...% 10 YELLOW (dark)
        1            0.50         0.25    ;...% 11 ORANGE
        0.75         0            0.75    ;...% 12 MAGENTA (dark)
        0.7          0.7          0.7     ;...% 13 GREY
        0.8          0.7          0.6     ;...% 14 BROWN (pale)
        0.9          0.4          0.2     ;...% 15 ORANGE (dark)
        1            0.7          0.4     ;...% 16 ORANGE (pale)
        0            0.8          0.8     ;...% 17 BLUE (PALE)
        0            1            0       ;...% 18 GREEN 
        0.4          0.7          0.6     ;...
        0.4          0            0.6     ;...
        0.4          0.6          0       ;...
        0.6          0.5          0.4 ];  % 18 BROWN (dark)
    
   
    
%% Plot Lc REDSHIFT
min_LC=0.004; %min([s_REDSHIFT_baseline(1,:),s_REDSHIFT_weak_struct(1,:),...
%     s_REDSHIFT_weak_no_links(1,:)]);%,s_REDSHIFT_baseline_no_links(1,:)]);
max_LC=0.2;%max([s_REDSHIFT_baseline(1,:),s_REDSHIFT_weak_struct(1,:),...
%     s_REDSHIFT_weak_no_links(1,:)]);%,s_REDSHIFT_baseline_no_links(1,:)]);
Lc_NASA=[min_LC:(max_LC-min_LC)/100:max_LC,1];
m_tot=0;
for ii = 1:length(ME)
    m_tot=m_tot+ME(ii).GEOMETRY_DATA.mass0;
end
NASA_SBM_cat = 0.1*(m_tot)^0.75*Lc_NASA.^(-1.71);
NASA_SBM_sub = 0.1*(mass_imp_1U*norm(v_p_1U))^0.75*Lc_NASA.^(-1.71);

xx_lim = [min_LC, max_LC]; %[3.e-3,0.35]; % 
yy_lim = [1,max(NASA_SBM_cat)]; %[1,9000]; % 
a=N_REDSHIFT_baseline(1,s_REDSHIFT_baseline(1,:)>min_LC);
b=N_REDSHIFT_weak_struct(1,s_REDSHIFT_weak_struct(1,:)>min_LC);
c=N_REDSHIFT_weak_no_links(1,s_REDSHIFT_weak_no_links(1,:)>min_LC);
figure('units','normalized','outerposition',[0 0 1 1],'Visible', 'on')
loglog(s_REDSHIFT_baseline(1,:),[N_REDSHIFT_baseline(1,1)+n_frags_bubbles_REDSHIFT_baseline(1),N_REDSHIFT_baseline(1,2:end)],'MarkerEdgeColor',colorOrder(11,:),'MarkerFaceColor',colorOrder(11,:),'Marker','o','LineStyle','none');
grid on
hold on
loglog(s_REDSHIFT_baseline(2,:),[N_REDSHIFT_baseline(2,1)+n_frags_bubbles_REDSHIFT_baseline(2),N_REDSHIFT_baseline(2,2:end)],'MarkerEdgeColor',colorOrder(16,:),'MarkerFaceColor',colorOrder(16,:),'Marker','o','LineStyle','none');
hold on
loglog(s_REDSHIFT_baseline(3,:),[N_REDSHIFT_baseline(3,1)+n_frags_bubbles_REDSHIFT_baseline(3),N_REDSHIFT_baseline(3,2:end)],'MarkerEdgeColor',colorOrder(15,:),'MarkerFaceColor',colorOrder(15,:),'Marker','o','LineStyle','none');
hold on
loglog(s_REDSHIFT_weak_struct(1,:),[N_REDSHIFT_weak_struct(1,1)+n_frags_bubbles_REDSHIFT_weak_struct(1),N_REDSHIFT_weak_struct(1,2:end)],'MarkerEdgeColor',colorOrder(5,:),'MarkerFaceColor',colorOrder(5,:),'Marker','^','LineStyle','none');
hold on
loglog(s_REDSHIFT_weak_struct(2,:),[N_REDSHIFT_weak_struct(2,1)+n_frags_bubbles_REDSHIFT_weak_struct(2),N_REDSHIFT_weak_struct(2,2:end)],'MarkerEdgeColor',colorOrder(17,:),'MarkerFaceColor',colorOrder(17,:),'Marker','^','LineStyle','none');
hold on
loglog(s_REDSHIFT_weak_struct(3,:),[N_REDSHIFT_weak_struct(3,1)+n_frags_bubbles_REDSHIFT_weak_struct(3),N_REDSHIFT_weak_struct(3,2:end)],'MarkerEdgeColor',colorOrder(2,:),'MarkerFaceColor',colorOrder(2,:),'Marker','^','LineStyle','none');
hold on
loglog(s_REDSHIFT_weak_no_links(1,:),[N_REDSHIFT_weak_no_links(1,1)+n_frags_bubbles_REDSHIFT_weak_no_links(1),N_REDSHIFT_weak_no_links(1,2:end)],'Color',colorOrder(3,:),'Marker','*','LineStyle','none','LineWidth',2)
hold on
loglog(s_REDSHIFT_weak_no_links(2,:),[N_REDSHIFT_weak_no_links(2,1)+n_frags_bubbles_REDSHIFT_weak_no_links(2),N_REDSHIFT_weak_no_links(2,2:end)],'Color',colorOrder(18,:),'Marker','*','LineStyle','none','LineWidth',2)
hold on
loglog(s_REDSHIFT_weak_no_links(3,:),[N_REDSHIFT_weak_no_links(3,1)+n_frags_bubbles_REDSHIFT_weak_no_links(3),N_REDSHIFT_weak_no_links(3,2:end)],'Color',colorOrder(9,:),'Marker','*','LineStyle','none','LineWidth',2)
hold on
set(gca,'FontName','Helvetica','FontSize',18,'FontWeight','bold')
loglog(Lc_NASA,NASA_SBM_sub,'--r','LineWidth',2)
loglog(Lc_NASA,NASA_SBM_cat,'-k','LineWidth',2)

lgd=legend('REDSHIFT baseline ','target baseline ','impactor baseline ','REDSHIFT weakened','target weakened ','impactor weakened ','REDSHIFT weakened without links','target weakened without links','impactor weakened without links','NASA SBM - SubCat','NASA SBM - Cat');  %'REDSHIFT baseline no links',
lgd.FontName='Helvetica';
lgd.FontSize=18;
lgd.FontWeight='bold';
xlabel('L_c [m]','FontName','Helvetica','FontSize',18,'FontWeight','bold')
ylabel('Cumulative number CN','FontName','Helvetica','FontSize',18,'FontWeight','bold')
title('CN(L_c) curves','FontName','Helvetica','FontSize',18,'FontWeight','bold') %  (F_L_{min}=1e^{-2},r_B=2e^{-3},r_F=1e^{-3})

hold on
grid on
set(gca,'XLim',xx_lim)
set(gca,'YLim',yy_lim)
out_fig = ['REDSHIFT_',title_fig];
saveas(gcf,out_fig,'fig')
saveas(gcf,out_fig,'png')
 %}
%%
%{
%% plot finale REDSHIFT A su M

xx_lim_Am = [-2,0.3];
yy_lim_Am = [0,0.09];
clear DAmSOC DAmSOC_matlab DAmSOC_NASA
lambda_c_min=log10(min([s_REDSHIFT_baseline s_REDSHIFT_c_mat s_REDSHIFT_weak_struct]));
lambda_c_max=log10(max([s_REDSHIFT_baseline s_REDSHIFT_c_mat s_REDSHIFT_weak_struct]));
lambda_c=lambda_c_min:(lambda_c_max-lambda_c_min)/20:lambda_c_max;
Am_REDSHIFT =[Am_CST_REDSHIFT_baseline Am_CST_REDSHIFT_c_mat Am_CST_REDSHIFT_weak_struct];
csi_min=log10(min(Am_REDSHIFT(Am_REDSHIFT>0)));
csi_max=log10(max(Am_REDSHIFT));
n=length(lambda_c);
csi_NASA=csi_min:(csi_max-csi_min)/(n-1):csi_max;

for i=1:1:n
        if  lambda_c(i)<=-3.5
            muSOC=-0.3;
            sigmaSOC=0.2;
            DAmSOC(i,:)=(1/(sigmaSOC*(2*pi)^0.5))*exp(-((csi_NASA-muSOC).^2)/(2*sigmaSOC^2));
            pd = makedist('Normal','mu',muSOC,'sigma',sigmaSOC);
%             DAmSOC_matlab(i,:) = pdf(pd,csi_NASA);
        elseif lambda_c(i)>-3.5 && lambda_c(i)<=-1.75
            muSOC=-0.3;
            sigmaSOC=0.2+0.1333*(lambda_c(i)+3.5);
            DAmSOC(i,:)=(1/(sigmaSOC*(2*pi)^0.5))*exp(-((csi_NASA-muSOC).^2)/(2*sigmaSOC^2));
            pd = makedist('Normal','mu',muSOC,'sigma',sigmaSOC);
%             DAmSOC_matlab(i,:) = pdf(pd,csi_NASA);
        elseif lambda_c(i)>-1.75 && lambda_c(i)<-1.25
            muSOC=-0.3-1.4*(lambda_c(i)+1.75);
            sigmaSOC=0.2+0.1333*(lambda_c(i)+3.5);
            DAmSOC(i,:)=(1/(sigmaSOC*(2*pi)^0.5))*exp(-((csi_NASA-muSOC).^2)/(2*sigmaSOC^2));
            pd = makedist('Normal','mu',muSOC,'sigma',sigmaSOC);
%             DAmSOC_matlab(i,:) = pdf(pd,csi_NASA);
        elseif lambda_c(i)>=-1.25
            muSOC=-1.0;
            sigmaSOC=0.2+0.1333*(lambda_c(i)+3.5);
            DAmSOC(i,:)=(1/(sigmaSOC*(2*pi)^0.5))*exp(-((csi_NASA-muSOC).^2)/(2*sigmaSOC^2));
            pd = makedist('Normal','mu',muSOC,'sigma',sigmaSOC);
%             DAmSOC_matlab(i,:) = pdf(pd,csi_NASA);
        end
end

for k=1:1:size(DAmSOC,2)
    DAmSOC_NASA(k)=sum(DAmSOC(:,k));
%     DAmSOC_matlab_NASA(k) = sum(DAmSOC_matlab(:,k));
end

figure('units','normalized','outerposition',[0 0 1 1],'Visible', 'on')
grid on
histogram(log10(Am_CST_REDSHIFT_baseline),'Normalization','probability','BinWidth',0.035)
hold on
histogram(log10(Am_CST_REDSHIFT_weak_struct),'Normalization','probability','BinWidth',0.035)
hold on
histogram(log10(Am_CST_REDSHIFT_c_mat),'Normalization','probability','BinWidth',0.035)
hold on
plot(csi_NASA,DAmSOC_NASA./sum(DAmSOC_NASA),'-b','LineWidth',2)
hold on
title(['Comparison among PDFs ',title_fig],'FontName','Helvetica','FontSize',18,'FontWeight','bold')
set(gca,'FontName','Helvetica','FontSize',18,'FontWeight','bold')
xlabel('log_{10}(A/m) [m^{2}/kg]','FontName','Helvetica','FontSize',18,'FontWeight','bold')
ylabel('Relative number N(A/m)','FontName','Helvetica','FontSize',18,'FontWeight','bold')
lgd=legend('REDSHIFT baseline','REDSHIFT weak struct','REDSHIFT c_{mat}','NASA - SBM','Location','northwest');
lgd.FontName='Helvetica';
lgd.FontSize=18;
lgd.FontWeight='bold';
set(gca,'XLim',xx_lim_Am)
set(gca,'YLim',yy_lim_Am)
out_fig = ['A_su_m_CST_REDSHIFT_data_new_Qloss_m_all2',title_fig];
saveas(gcf,out_fig,'fig')
saveas(gcf,out_fig,'png')
%}
%% plot finale of Delta_v
%{
clear DdvCOLL_NASA DdvCOLL_NASA_1

muEXP=0.9*csi_NASA+2.9;
sigmaEXP=0.4;
delta_v_lim=[0 3];
delta_v_NASA=delta_v_lim(1):abs(delta_v_lim(1)-delta_v_lim(2))/(n-1):delta_v_lim(2);

for i=1:1:n
    DdvCOLL_NASA(:,i)=(1/(sigmaEXP*(2*pi)^0.5))*exp(-((delta_v_NASA-muEXP(i)).^2)/(2*sigmaEXP^2));
end

for k=1:1:size(DdvCOLL_NASA,1)
    DdvCOLL_NASA_1(k)=sum(DdvCOLL_NASA(:,k));
end

figure
histogram(delta_v_CST_REDSHIFT_baseline,'Normalization','probability','BinWidth',1)
grid on
hold on
histogram(delta_v_CST_REDSHIFT_weak_struct,'Normalization','probability','BinWidth',1)
hold on
histogram(delta_v_CST_REDSHIFT_c_mat,'Normalization','probability','BinWidth',1)
hold on
% plot(delta_v_NASA,DdvCOLL_NASA_1./sum(DdvCOLL_NASA_1),'b','LineWidth',2)
title(['Comparison among PDFs ',title_fig])
xlabel('\DeltaV [m/s] ')
ylabel('Relative Number of Fragments per \DeltaV bin')
legend('CST REDSHIFT baseline','CST REDSHIFT weak struct','CST REDSHIFT c_mat','NASA SBM')
xlim([0 60000])
%}

%% plot cpu time and simulation time
if exist(['Data_Simulation_REDSHIFT_',title_fig,'.txt'],'file')
    delete(['Data_Simulation_REDSHIFT_',title_fig,'.txt'])
end
diary(['Data_Simulation_REDSHIFT_',title_fig,'.txt'])
diary on
disp('------------------------------------------------------------------------------')
disp('SIMULATION                     CPUTIME (min)    SIM. TIME (s)     FINAL STEP')
disp(['REDSHIFT baseline                ',cputime_REDSHIFT_baseline,tf_REDSHIFT_baseline,final_step_REDSHIFT_baseline, num2str(a(1))])
% disp(['REDSHIFT baseline no links       ',cputime_REDSHIFT_baseline_no_links,tf_REDSHIFT_baseline_no_links,final_step_REDSHIFT_baseline_no_links])
disp(['REDSHIFT weak struct             ',cputime_REDSHIFT_weak_struct,tf_REDSHIFT_weak_struct,final_step_REDSHIFT_weak_struct, num2str(b(1))])
disp(['REDSHIFT weak struct no links    ',cputime_REDSHIFT_weak_no_links,tf_REDSHIFT_weak_no_links,final_step_REDSHIFT_weak_no_links, num2str(c(1))])
disp('------------------------------------------------------------------------------')
diary off