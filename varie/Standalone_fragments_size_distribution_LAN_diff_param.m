% Standalone for fragments size distribution
clear all
% close all
clc

global MATERIAL_LIST

%% Load data

name_file_result_baseline_LAN='E:\AO_8507Catastrophic\CST_integration\CST3_21_11_2018_risultati_RILEVANTI\Results\Lan_Satellite_Finale_Baseline_AGOSTO2018\data\step_1000.mat';
% name_file_result_LAN_new_diff_links='E:\AO_8507Catastrophic\CST_integration\CST3_21_11_2018_risultati_RILEVANTI\Results\Lan_Satellite_BEFORE_correction_velocity_model_NEW_links_tfc_070\data\step_1296.mat';
name_file_result_LAN_tfc_055_FLc_085='E:\AO_8507Catastrophic\CST_integration\CST3_LAN_07\Results\Lan_Satellite_20181122T095134\data\step_538.mat';
name_file_result_LAN_tfc_070_FLc_085='E:\AO_8507Catastrophic\CST_integration\CST3_LAN_07\Results\Lan_Satellite_20181122T120453\data\step_569.mat';
name_file_result_LAN_tfc_070_FLc_050_Frt_1e6='E:\AO_8507Catastrophic\CST_integration\CST3_LAN_07_2\Results\Lan_Satellite_20181122T121107\data\step_530.mat';
% name_file_result_LAN_new_diff_links='E:\AO_8507Catastrophic\CST_integration\CST3_LAN_07\Results\Lan_Satellite_20181122T133830\data\step_437.mat';
name_file_result_LAN_new_diff_links='E:\AO_8507Catastrophic\CST_integration\CST3_LAN_07\Results\Lan_Satellite_20181122T145014\data\step_777.mat';

Data_Lan_CN
%% Settings

colorOrder =  [ ...
    0            0            1       ;...% 1 BLUE   
    1            0            0       ;...% 2 RED
    0            1            0       ;...% 3 GREEN (pale)
    0            1            1       ;...% 4 CYAN
    1            0            1       ;...% 5 MAGENTA (pale)
    0.6          0.5          0.4     ;  % 6 BROWN (dark)
    0            0            0       ;...% 7 BLACK
    1            1            0       ;...% 8 YELLOW (pale)
    0            0.75         0.75    ;...% 9 TURQUOISE
    0            0.5          0       ;...% 10 GREEN (dark)
    0.75         0.75         0       ;...% 11 YELLOW (dark)
    1            0.50         0.25    ;...% 12 ORANGE
    0.75         0            0.75    ;...% 13 MAGENTA (dark)
    0.7          0.7          0.7     ;...% 14 GREY
    0.8          0.7          0.6     ;...% 15 BROWN (pale)
    0.4          0.7          0.6     ;...
    0.4          0          0.6     ;...
    0.4          0.6          0     ;...
    0.6          0.5          0.4 ];  % 15 BROWN (dark)

%% processing baseline LAN scenario data

load(name_file_result_baseline_LAN,'FRAGMENTS','ME')

n_size=length(FRAGMENTS);
for i=n_size:-1:1
    if FRAGMENTS(i).GEOMETRY_DATA.mass==0
        FRAGMENTS(i)=[];
    else
        box = boundingBox3d(FRAGMENTS(i).GEOMETRY_DATA.c_hull);
        lx = abs(box(2)-box(1));
        ly = abs(box(4)-box(3));
        lz = abs(box(6)-box(5));
        FRAGMENTS(i).GEOMETRY_DATA.dimensions=[lx ly lz];
        x(i)=max(FRAGMENTS(i).GEOMETRY_DATA.dimensions);
    end
end
n_size=length(FRAGMENTS);
FRAGMENTS1=FRAGMENTS;

mass_imp=0;
for i = 66:length(ME)
    mass_imp = mass_imp + ME(i).GEOMETRY_DATA.mass0;
    q_imp = ME(i).GEOMETRY_DATA.mass0*ME(i).DYNAMICS_INITIAL_DATA.vel0;
end
v_p = 1/mass_imp*q_imp/1000;
% clear ME

n_size1=length(ME);
for i=n_size1:-1:1
    if ME(i).GEOMETRY_DATA.mass==0
        ME(i)=[];
    end
end
n_size1=length(ME);

if ~isempty(ME)
    FRAGMENTS1(n_size+1:n_size+n_size1)=ME;
end

n_size=length(FRAGMENTS1);

s1 =zeros(1,n_size-1);
k=1;
for i = 2:n_size
    Lc_CST_LAN(i-1)=mean(FRAGMENTS1(i).GEOMETRY_DATA.dimensions);
    Am_CST_LAN(i-1)=((FRAGMENTS1(i).GEOMETRY_DATA.dimensions(1)*FRAGMENTS1(i).GEOMETRY_DATA.dimensions(2)+...
        FRAGMENTS1(i).GEOMETRY_DATA.dimensions(1)*FRAGMENTS1(i).GEOMETRY_DATA.dimensions(3)+...
        FRAGMENTS1(i).GEOMETRY_DATA.dimensions(2)*FRAGMENTS1(i).GEOMETRY_DATA.dimensions(3))/3/FRAGMENTS1(i).GEOMETRY_DATA.mass);
    vel_CST_LAN(i-1)=norm(FRAGMENTS1(i).DYNAMICS_DATA.vel);
    mass_CST_LAN(i-1)=FRAGMENTS1(i).GEOMETRY_DATA.mass;
    if FRAGMENTS1(i).object_ID~=66
       vel_CST_np(k)=norm(FRAGMENTS1(i).DYNAMICS_DATA.vel);
       k=k+1;
    end
end

s_LAN=sort(Lc_CST_LAN);

if n_size==1 %patch to avoid only one fragment case
    n_size=n_size+1;
end

N_LAN=n_size-1:-1:1;

% data for plot delta V
q_cm=[0 0 0]';
for i=2:n_size
    q_cm=q_cm+mass_CST_LAN(i-1)*vel_CST_LAN(:,i-1);
end
v_cm = q_cm/sum(mass_CST_LAN);
for i=2:n_size
    delta_v_CST_LAN(i-1)=norm(v_cm-vel_CST_LAN(:,i-1));
end

clear v_cm q_cm n_size FRAGMENTS ME

%% processing LAN different links configuration

load(name_file_result_LAN_new_diff_links,'FRAGMENTS','ME','BUBBLE','PROPAGATION_THRESHOLD')
name_mat=[name_file_result_LAN_new_diff_links(1:strfind(name_file_result_LAN_new_diff_links,'\Results')),'set_up',filesep,'material_list.m'];
run(name_mat)

n_size=length(FRAGMENTS);
for i=n_size:-1:1
    if FRAGMENTS(i).GEOMETRY_DATA.mass==0
        FRAGMENTS(i)=[];
    else
        box = boundingBox3d(FRAGMENTS(i).GEOMETRY_DATA.c_hull);
        lx = abs(box(2)-box(1));
        ly = abs(box(4)-box(3));
        lz = abs(box(6)-box(5));
        FRAGMENTS(i).GEOMETRY_DATA.dimensions=[lx ly lz];
        x(i)=max(FRAGMENTS(i).GEOMETRY_DATA.dimensions);
    end
end
n_size=length(FRAGMENTS);
FRAGMENTS_LAN_new_diff_links=FRAGMENTS;
clear FRAGMENTS

n_size=length(FRAGMENTS_LAN_new_diff_links);
for i=n_size:-1:1
    if FRAGMENTS_LAN_new_diff_links(i).GEOMETRY_DATA.mass==0
        FRAGMENTS_LAN_new_diff_links(i)=[];
    end
end
n_size=length(FRAGMENTS_LAN_new_diff_links);

n_size1=length(ME);
for i=n_size1:-1:1
    if ME(i).GEOMETRY_DATA.mass==0
        ME(i)=[];
    end
end
n_size1=length(ME);

if ~isempty(ME)
    FRAGMENTS_LAN_new_diff_links(n_size+1:n_size+n_size1)=ME;
end

for i = 2:n_size
    Lc_CST_LAN_new_diff_links(i-1)=mean(FRAGMENTS_LAN_new_diff_links(i).GEOMETRY_DATA.dimensions);
    Am_CST_LAN_new_diff_links(i-1)=FRAGMENTS_LAN_new_diff_links(i).GEOMETRY_DATA.A_M_ratio;
    vel_CST_LAN_new_diff_links(i-1)=norm(FRAGMENTS_LAN_new_diff_links(i).DYNAMICS_DATA.vel);
    mass_CST_LAN_new_diff_links(i-1)=FRAGMENTS_LAN_new_diff_links(i).GEOMETRY_DATA.mass;
end

s_LAN_new_diff_links=sort(Lc_CST_LAN_new_diff_links);

if n_size==1 %patch to avoid only one fragment case
    n_size=n_size+1;
end

N_LAN_new_diff_links=n_size-1:-1:1;

% data for plot delta V
q_cm=[0 0 0]';
for i=2:n_size
    q_cm=q_cm+mass_CST_LAN_new_diff_links(i-1)*vel_CST_LAN_new_diff_links(:,i-1);
end
v_cm = q_cm/sum(mass_CST_LAN_new_diff_links);
for i=2:n_size
    delta_v_CST_LAN_new_diff_links(i-1)=norm(v_cm-vel_CST_LAN_new_diff_links(:,i-1));
end

clear v_cm q_cm n_size FRAGMENTS

%% processing LAN tfc=0.6

load(name_file_result_LAN_tfc_055_FLc_085,'FRAGMENTS','ME')

n_size=length(FRAGMENTS);
for i=n_size:-1:1
    if FRAGMENTS(i).GEOMETRY_DATA.mass==0
        FRAGMENTS(i)=[];
    else
        box = boundingBox3d(FRAGMENTS(i).GEOMETRY_DATA.c_hull);
        lx = abs(box(2)-box(1));
        ly = abs(box(4)-box(3));
        lz = abs(box(6)-box(5));
        FRAGMENTS(i).GEOMETRY_DATA.dimensions=[lx ly lz];
        x(i)=max(FRAGMENTS(i).GEOMETRY_DATA.dimensions);
    end
end
n_size=length(FRAGMENTS);
FRAGMENTS_LAN_tfc_055_FLc_085=FRAGMENTS;
clear FRAGMENTS

n_size=length(FRAGMENTS_LAN_tfc_055_FLc_085);
for i=n_size:-1:1
    if FRAGMENTS_LAN_tfc_055_FLc_085(i).GEOMETRY_DATA.mass==0
        FRAGMENTS_LAN_tfc_055_FLc_085(i)=[];
    end
end
n_size=length(FRAGMENTS_LAN_tfc_055_FLc_085);

n_size1=length(ME);
for i=n_size1:-1:1
    if ME(i).GEOMETRY_DATA.mass==0
        ME(i)=[];
    end
end
n_size1=length(ME);

if ~isempty(ME)
    FRAGMENTS_LAN_tfc_055_FLc_085(n_size+1:n_size+n_size1)=ME;
end

for i = 2:n_size
    Lc_CST_LAN_tfc_055_FLc_085(i-1)=mean(FRAGMENTS_LAN_tfc_055_FLc_085(i).GEOMETRY_DATA.dimensions);
    Am_CST_LAN_tfc_055_FLc_085(i-1)=FRAGMENTS_LAN_tfc_055_FLc_085(i).GEOMETRY_DATA.A_M_ratio;
    vel_CST_LAN_tfc_055_FLc_085(i-1)=norm(FRAGMENTS_LAN_tfc_055_FLc_085(i).DYNAMICS_DATA.vel);
    mass_CST_LAN_tfc_055_FLc_085(i-1)=FRAGMENTS_LAN_tfc_055_FLc_085(i).GEOMETRY_DATA.mass;
end

s_LAN_tfc_055_FLc_085=sort(Lc_CST_LAN_tfc_055_FLc_085);

if n_size==1 %patch to avoid only one fragment case
    n_size=n_size+1;
end

N_LAN_tfc_055_FLc_085=n_size-1:-1:1;

% data for plot delta V
q_cm=[0 0 0]';
for i=2:n_size
    q_cm=q_cm+mass_CST_LAN_tfc_055_FLc_085(i-1)*vel_CST_LAN_tfc_055_FLc_085(:,i-1);
end
v_cm = q_cm/sum(mass_CST_LAN_tfc_055_FLc_085);
for i=2:n_size
    delta_v_CST_LAN_tfc_055_FLc_085(i-1)=norm(v_cm-vel_CST_LAN_tfc_055_FLc_085(:,i-1));
end

clear v_cm q_cm n_size FRAGMENTS

%% processing LAN tfc=0.65

load(name_file_result_LAN_tfc_070_FLc_085,'FRAGMENTS','ME')

n_size=length(FRAGMENTS);
for i=n_size:-1:1
    if FRAGMENTS(i).GEOMETRY_DATA.mass==0
        FRAGMENTS(i)=[];
    else
        box = boundingBox3d(FRAGMENTS(i).GEOMETRY_DATA.c_hull);
        lx = abs(box(2)-box(1));
        ly = abs(box(4)-box(3));
        lz = abs(box(6)-box(5));
        FRAGMENTS(i).GEOMETRY_DATA.dimensions=[lx ly lz];
        x(i)=max(FRAGMENTS(i).GEOMETRY_DATA.dimensions);
    end
end
n_size=length(FRAGMENTS);
FRAGMENTS_LAN_tfc_070_FLc_085=FRAGMENTS;
clear FRAGMENTS

n_size=length(FRAGMENTS_LAN_tfc_070_FLc_085);
for i=n_size:-1:1
    if FRAGMENTS_LAN_tfc_070_FLc_085(i).GEOMETRY_DATA.mass==0
        FRAGMENTS_LAN_tfc_070_FLc_085(i)=[];
    end
end
n_size=length(FRAGMENTS_LAN_tfc_070_FLc_085);

n_size1=length(ME);
for i=n_size1:-1:1
    if ME(i).GEOMETRY_DATA.mass==0
        ME(i)=[];
    end
end
n_size1=length(ME);

if ~isempty(ME)
    FRAGMENTS_LAN_tfc_070_FLc_085(n_size+1:n_size+n_size1)=ME;
end

for i = 2:n_size
    Lc_CST_LAN_tfc_070_FLc_085(i-1)=mean(FRAGMENTS_LAN_tfc_070_FLc_085(i).GEOMETRY_DATA.dimensions);
    Am_CST_LAN_tfc_070_FLc_085(i-1)=FRAGMENTS_LAN_tfc_070_FLc_085(i).GEOMETRY_DATA.A_M_ratio;
    vel_CST_LAN_tfc_070_FLc_085(i-1)=norm(FRAGMENTS_LAN_tfc_070_FLc_085(i).DYNAMICS_DATA.vel);
    mass_CST_LAN_tfc_070_FLc_085(i-1)=FRAGMENTS_LAN_tfc_070_FLc_085(i).GEOMETRY_DATA.mass;
end

s_LAN_tfc_070_FLc_085=sort(Lc_CST_LAN_tfc_070_FLc_085);

if n_size==1 %patch to avoid only one fragment case
    n_size=n_size+1;
end

N_LAN_tfc_070_FLc_085=n_size-1:-1:1;

% data for plot delta V
q_cm=[0 0 0]';
for i=2:n_size
    q_cm=q_cm+mass_CST_LAN_tfc_070_FLc_085(i-1)*vel_CST_LAN_tfc_070_FLc_085(:,i-1);
end
v_cm = q_cm/sum(mass_CST_LAN_tfc_070_FLc_085);
for i=2:n_size
    delta_v_CST_LAN_tfc_070_FLc_085(i-1)=norm(v_cm-vel_CST_LAN_tfc_070_FLc_085(:,i-1));
end

clear v_cm q_cm n_size FRAGMENTS

%% processing LAN tfc=0.7

load(name_file_result_LAN_tfc_070_FLc_050_Frt_1e6,'FRAGMENTS','ME')

n_size=length(FRAGMENTS);
for i=n_size:-1:1
    if FRAGMENTS(i).GEOMETRY_DATA.mass==0
        FRAGMENTS(i)=[];
    else
        box = boundingBox3d(FRAGMENTS(i).GEOMETRY_DATA.c_hull);
        lx = abs(box(2)-box(1));
        ly = abs(box(4)-box(3));
        lz = abs(box(6)-box(5));
        FRAGMENTS(i).GEOMETRY_DATA.dimensions=[lx ly lz];
        x(i)=max(FRAGMENTS(i).GEOMETRY_DATA.dimensions);
    end
end
n_size=length(FRAGMENTS);
FRAGMENTS_LAN_tfc_070_FLc_050_Frt_1e6=FRAGMENTS;
clear FRAGMENTS

n_size=length(FRAGMENTS_LAN_tfc_070_FLc_050_Frt_1e6);
for i=n_size:-1:1
    if FRAGMENTS_LAN_tfc_070_FLc_050_Frt_1e6(i).GEOMETRY_DATA.mass==0
        FRAGMENTS_LAN_tfc_070_FLc_050_Frt_1e6(i)=[];
    end
end
n_size=length(FRAGMENTS_LAN_tfc_070_FLc_050_Frt_1e6);

n_size1=length(ME);
for i=n_size1:-1:1
    if ME(i).GEOMETRY_DATA.mass==0
        ME(i)=[];
    end
end
n_size1=length(ME);

if ~isempty(ME)
    FRAGMENTS_LAN_tfc_070_FLc_050_Frt_1e6(n_size+1:n_size+n_size1)=ME;
end

for i = 2:n_size
    Lc_CST_LAN_tfc_070_FLc_050_Frt_1e6(i-1)=mean(FRAGMENTS_LAN_tfc_070_FLc_050_Frt_1e6(i).GEOMETRY_DATA.dimensions);
    Am_CST_LAN_tfc_070_FLc_050_Frt_1e6(i-1)=FRAGMENTS_LAN_tfc_070_FLc_050_Frt_1e6(i).GEOMETRY_DATA.A_M_ratio;
    vel_CST_LAN_tfc_070_FLc_050_Frt_1e6(i-1)=norm(FRAGMENTS_LAN_tfc_070_FLc_050_Frt_1e6(i).DYNAMICS_DATA.vel);
    mass_CST_LAN_tfc_070_FLc_050_Frt_1e6(i-1)=FRAGMENTS_LAN_tfc_070_FLc_050_Frt_1e6(i).GEOMETRY_DATA.mass;
end

s_LAN_tfc_070_FLc_050_Frt_1e6=sort(Lc_CST_LAN_tfc_070_FLc_050_Frt_1e6);

if n_size==1 %patch to avoid only one fragment case
    n_size=n_size+1;
end

N_LAN_tfc_070_FLc_050_Frt_1e6=n_size-1:-1:1;

% data for plot delta V
q_cm=[0 0 0]';
for i=2:n_size
    q_cm=q_cm+mass_CST_LAN_tfc_070_FLc_050_Frt_1e6(i-1)*vel_CST_LAN_tfc_070_FLc_050_Frt_1e6(:,i-1);
end
v_cm = q_cm/sum(mass_CST_LAN_tfc_070_FLc_050_Frt_1e6);
for i=2:n_size
    delta_v_CST_LAN_tfc_070_FLc_050_Frt_1e6(i-1)=norm(v_cm-vel_CST_LAN_tfc_070_FLc_050_Frt_1e6(:,i-1));
end

clear v_cm q_cm n_size FRAGMENTS

%% plot finale Lc

%{s
Lc_NASA=min(Lan_CN(:,1)):(max(Lan_CN(:,1))-min(Lan_CN(:,1)))/100:max(Lan_CN(:,1));
NASA_SBM_sub = 0.1*(mass_imp*norm(v_p))^0.75*Lc_NASA.^(-1.71);
m_tot=0;
for ii = 1:length(ME)
    m_tot=m_tot+ME(ii).GEOMETRY_DATA.mass0;
end
NASA_SBM_cat = 0.1*(m_tot)^0.75*Lc_NASA.^(-1.71);

n_frags_bubbles=0;
for ii = 1:length(BUBBLE)
    n_frags_bubbles=n_frags_bubbles+BUBBLE(ii).GEOMETRY_DATA.mass/(4/3*PROPAGATION_THRESHOLD.bubble_radius_th^3*pi()*MATERIAL_LIST(BUBBLE(ii).material_ID).density);
end

h = figure('Position', [45   129   906   553],'Visible', 'on');
loglog(s_LAN,N_LAN,'+g','LineWidth',2)
grid on
hold on
loglog(s_LAN_new_diff_links,[N_LAN_new_diff_links(1)+n_frags_bubbles,N_LAN_new_diff_links(2:end)],'MarkerFaceColor',colorOrder(9,:),'Marker','*','LineStyle','none')
hold on
loglog(s_LAN_tfc_055_FLc_085,[N_LAN_tfc_055_FLc_085(1)+n_frags_bubbles,N_LAN_tfc_055_FLc_085(2:end)],'MarkerFaceColor',colorOrder(11,:),'Marker','o','LineStyle','none')
hold on
loglog(s_LAN_tfc_070_FLc_085,[N_LAN_tfc_070_FLc_085(1)+n_frags_bubbles,N_LAN_tfc_070_FLc_085(2:end)],'MarkerFaceColor',colorOrder(5,:),'Marker','^','LineStyle','none')
hold on
loglog(s_LAN_tfc_070_FLc_050_Frt_1e6,[N_LAN_tfc_070_FLc_050_Frt_1e6(1)+n_frags_bubbles,N_LAN_tfc_070_FLc_050_Frt_1e6(2:end)],'MarkerFaceColor',colorOrder(10,:),'Marker','<','LineStyle','none')
hold on
loglog(Lan_CN(:,1),Lan_CN(:,2),'-k','LineWidth',2)
hold on
loglog(Lc_NASA,NASA_SBM_sub,'-r','LineWidth',2)
hold on
loglog(Lc_NASA,NASA_SBM_cat,'--r','LineWidth',2)
hold on
set(gca,'FontSize',20,'FontWeight','bold')
title('Comparison CST results & LAN')
xlabel('L_c [m]')
ylabel('Cumulative number N')
% set(gca,'FontSize',14,'FontWeight','bold')
legend('CST LAN OLD (PM4)','CST LAN NEW (no correction to velocity model)','CST LAN NEW (correction to velocity model & tfc=0.55 & FLc=0.85)','CST LAN NEW (correction to velocity model & tfc=0.70 & FLc=0.85)','CST LAN NEW (correction to velocity model & tfc=0.70 & FLc=0.55 & Frt=1e^{-6})','LAN experimental results','NASA - SubCat-SBM','NASA - Cat-SBM')
% xlim([6*10^-4 1])
xlim([4*10^-3 1])
ylim([10^0 10^4])

%% plot finale A su M

clear DAmSOC DAmSOC_matlab DAmSOC_NASA
lambda_c_min=log10(min([s_LAN s_LAN_new_diff_links s_LAN_tfc_055_FLc_085 s_LAN_tfc_070_FLc_085 s_LAN_tfc_070_FLc_050_Frt_1e6]));
lambda_c_max=log10(max([s_LAN s_LAN_new_diff_links s_LAN_tfc_055_FLc_085 s_LAN_tfc_070_FLc_085 s_LAN_tfc_070_FLc_050_Frt_1e6]));
lambda_c=lambda_c_min:(lambda_c_max-lambda_c_min)/18:lambda_c_max;
csi_min=log10(min([Am_CST_LAN Am_CST_LAN_new_diff_links Am_CST_LAN_tfc_055_FLc_085 Am_CST_LAN_tfc_070_FLc_085 Am_CST_LAN_tfc_070_FLc_050_Frt_1e6]));
csi_max=log10(max([Am_CST_LAN Am_CST_LAN_new_diff_links Am_CST_LAN_tfc_055_FLc_085 Am_CST_LAN_tfc_070_FLc_085 Am_CST_LAN_tfc_070_FLc_050_Frt_1e6]));
n=length(lambda_c);
csi_NASA=csi_min:(csi_max-csi_min)/(n-1):csi_max;

for i=1:1:n
        if  lambda_c(i)<=-3.5
            muSOC=-0.3;
            sigmaSOC=0.2;
            DAmSOC(i,:)=(1/(sigmaSOC*(2*pi)^0.5))*exp(-((csi_NASA-muSOC).^2)/(2*sigmaSOC^2));
            pd = makedist('Normal','mu',muSOC,'sigma',sigmaSOC);
            DAmSOC_matlab(i,:) = pdf(pd,csi_NASA);
        elseif lambda_c(i)>-3.5 && lambda_c(i)<=-1.75
            muSOC=-0.3;
            sigmaSOC=0.2+0.1333*(lambda_c(i)+3.5);
            DAmSOC(i,:)=(1/(sigmaSOC*(2*pi)^0.5))*exp(-((csi_NASA-muSOC).^2)/(2*sigmaSOC^2));
            pd = makedist('Normal','mu',muSOC,'sigma',sigmaSOC);
            DAmSOC_matlab(i,:) = pdf(pd,csi_NASA);
        elseif lambda_c(i)>-1.75 && lambda_c(i)<-1.25
            muSOC=-0.3-1.4*(lambda_c(i)+1.75);
            sigmaSOC=0.2+0.1333*(lambda_c(i)+3.5);
            DAmSOC(i,:)=(1/(sigmaSOC*(2*pi)^0.5))*exp(-((csi_NASA-muSOC).^2)/(2*sigmaSOC^2));
            pd = makedist('Normal','mu',muSOC,'sigma',sigmaSOC);
            DAmSOC_matlab(i,:) = pdf(pd,csi_NASA);
        elseif lambda_c(i)>=-1.25
            muSOC=-1.0;
            sigmaSOC=0.2+0.1333*(lambda_c(i)+3.5);
            DAmSOC(i,:)=(1/(sigmaSOC*(2*pi)^0.5))*exp(-((csi_NASA-muSOC).^2)/(2*sigmaSOC^2));
            pd = makedist('Normal','mu',muSOC,'sigma',sigmaSOC);
            DAmSOC_matlab(i,:) = pdf(pd,csi_NASA);
        end
end

for k=1:1:size(DAmSOC,2)
    DAmSOC_NASA(k)=sum(DAmSOC(:,k));
    DAmSOC_matlab_NASA(k) = sum(DAmSOC_matlab(:,k));
end

%% LOAD LAN RESULTS (obtained from paper)
load('LAN_A_m_ratio.mat')
Am_LAN=10.^Y;
Lc_LAN=10.^X;

%LAN results
alpha1=0.0060;
sigma1=0.1270;
mu1=-1.180;
alpha2=0.0360;
sigma2=0.1050;
mu2=-0.695;
alpha3=0.0150;
sigma3=0.1450;
mu3=-0.295;
csi=log10(sort(Am_LAN));

P=alpha1*(1/(sigma1*(2*pi)^0.5))*exp(-((csi-mu1).^2)/(2*sigma1^2))+...
    alpha2*(1/(sigma2*(2*pi)^0.5))*exp(-((csi-mu2).^2)/(2*sigma2^2))+...
    alpha3*(1/(sigma3*(2*pi)^0.5))*exp(-((csi-mu3).^2)/(2*sigma3^2));

%{

h = figure('Position', [45   129   906   553],'Visible', 'on');
histogram(log10(Am_CST_LAN),'Normalization','probability','BinWidth',0.05)
grid on
hold on
histogram(log10(Am_CST_LAN_new_diff_links),'Normalization','probability','BinWidth',0.05)
hold on
histogram(log10(Am_CST_LAN_tfc_055_FLc_085),'Normalization','probability','BinWidth',0.05)
hold on
histogram(log10(Am_CST_LAN_tfc_070_FLc_085),'Normalization','probability','BinWidth',0.05)
hold on
histogram(log10(Am_CST_LAN_tfc_070_FLc_050_Frt_1e6),'Normalization','probability','BinWidth',0.05)
hold on
plot(csi,P,'-k','LineWidth',2)
hold on
plot(csi_NASA,DAmSOC_NASA./sum(DAmSOC_NASA),'-b','LineWidth',2)
hold on
title('Comparison among PDFs')
set(gca,'FontSize',20,'FontWeight','bold')
xlabel('log_{10}(A/m) [m^{2}/kg]')
ylabel('CN(A/m)')
% set(gca,'FontSize',14,'FontWeight','bold')
legend('CST LAN OLD (PM4)','CST LAN NEW (no correction to velocity model)','CST LAN NEW (correction to velocity model & tfc=0.55 & FLc=0.85)','CST LAN NEW (correction to velocity model & tfc=0.70 & FLc=0.85)','CST LAN NEW (correction to velocity model & tfc=0.70 & FLc=0.55 & Frt=1e^{-6})','LAN PDF','NASA-SBM')

%{e
h = figure('Position', [45   129   906   553],'Visible', 'on');
histogram(log10(Am_CST_LAN),'Normalization','probability','BinWidth',0.05)
grid on
hold on
histogram(log10(Am_CST_LAN_new_diff_links),'Normalization','probability','BinWidth',0.05)
hold on
histogram(log10(Am_CST_LAN_tfc_070_FLc_085),'Normalization','probability','BinWidth',0.05)
hold on
plot(csi,P,'-k','LineWidth',2)
hold on
title('Comparison among PDFs')
set(gca,'FontSize',20,'FontWeight','bold')
xlabel('log_{10}(A/m) [m^{2}/kg]')
ylabel('CN(A/m)')
% set(gca,'FontSize',14,'FontWeight','bold')
legend('CST LAN OLD (PM4)','CST LAN NEW (no correction to velocity model)','CST LAN NEW (correction to velocity model & tfc=0.70 & FLc=0.85)','LAN PDF')

h = figure('Position', [45   129   906   553],'Visible', 'on');
histogram(log10(Am_CST_LAN),'Normalization','probability','BinWidth',0.05)
grid on
hold on
histogram(log10(Am_CST_LAN_tfc_070_FLc_050_Frt_1e6),'Normalization','probability','BinWidth',0.05)
hold on
plot(csi,P,'-k','LineWidth',2)
hold on
title('Comparison among PDFs')
set(gca,'FontSize',20,'FontWeight','bold')
xlabel('log_{10}(A/m) [m^{2}/kg]')
ylabel('CN(A/m)')
% set(gca,'FontSize',14,'FontWeight','bold')
legend('CST LAN OLD (PM4)','CST LAN NEW (correction to velocity model & tfc=0.70 & FLc=0.55 & Frt=1e^{-6})','LAN PDF')

%}
%% plot finale of Delta_v

%{
m=length(csi_NASA);
muEXP=0.9*csi_NASA+2.9;
sigmaEXP=0.4;
vel=sort([vel_CST_LAN vel_CST_LAN_new_diff_links vel_CST_LAN_tfc_055_FLc_085 vel_CST_LAN_tfc_070_FLc_085 vel_CST_LAN_tfc_070_FLc_050_Frt_1e6]);
vel(vel==0)=[];
delta_v_min=vel(1);
delta_v_max=max([vel_CST_LAN vel_CST_LAN_new_diff_links vel_CST_LAN_tfc_055_FLc_085 vel_CST_LAN_tfc_070_FLc_085 vel_CST_LAN_tfc_070_FLc_050_Frt_1e6]);
delta_v_NASA=delta_v_min:abs(delta_v_min-delta_v_max)/(m-1):delta_v_max;

for i=1:1:m
    DdvCOLL_NASA(:,i)=(1/(sigmaEXP*(2*pi)^0.5))*exp(-((delta_v_NASA-muEXP(i)).^2)/(2*sigmaEXP^2));
end

for k=1:1:m
    DdvCOLL_NASA_1(k)=sum(DdvCOLL_NASA(:,k));
end

h = figure('Position', [45   129   906   553],'Visible', 'on');
histogram(delta_v_CST_LAN/1000,'Normalization','probability','BinWidth',0.05)
grid on
hold on
histogram(delta_v_CST_LAN_new_diff_links/1000,'Normalization','probability','BinWidth',0.05)
hold on
histogram(delta_v_CST_LAN_tfc_055_FLc_085/1000,'Normalization','probability','BinWidth',0.05)
hold on
histogram(delta_v_CST_LAN_tfc_070_FLc_085/1000,'Normalization','probability','BinWidth',0.05)
hold on
histogram(delta_v_CST_LAN_tfc_070_FLc_050_Frt_1e6/1000,'Normalization','probability','BinWidth',0.05)
hold on
plot(delta_v_NASA/1000,DdvCOLL_NASA_1./sum(DdvCOLL_NASA_1),'b','LineWidth',2)
set(gca,'FontSize',20,'FontWeight','bold')
title('Comparison among PDFs')
xlabel('\DeltaV [km/s] ')
ylabel('Relative Number of Fragments per \DeltaV bin')
legend('CST LAN OLD (PM4)','CST LAN NEW (no correction to velocity model)','CST LAN NEW (correction to velocity model & tfc=0.6)','CST LAN NEW (correction to velocity model & tfc=0.65)','CST LAN NEW (correction to velocity model & tfc=0.7)','NASA-SBM')
%  xlim([0 6000])
%}
