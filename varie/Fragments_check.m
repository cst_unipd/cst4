clear
LOFT1='N:\CST3\Results\LOFT1_Baseline_AFTER_correction_v_loss\data\step_113.mat';
LOFT2='N:\CST3\Results\LOFT2_Baseline_AFTER_correction_v_loss\data\step_318.mat';
LOFT3='N:\CST3\Results\LOFT3_Baseline_AFTER_correction_v_loss\data\step_293.mat';
LOFT4='N:\CST3\Results\LOFT4_Baseline_AFTER_correction_v_loss\data\step_267.mat';
LOFT5='N:\CST3\Results\LOFT5_Baseline_AFTER_correction_v_loss\data\step_454.mat';
LOFT6='N:\CST3\Results\LOFT6_Baseline\data\step_1281.mat';
LOFT7='N:\CST3_da_64GB_17122018_LOFT8_AF\Results\LOFT8_AF_20190115T204132\data\step_1744.mat';
LOFT8='N:\CST3_da_64GB_17122018_LOFT8_MD\Results\LOFT8_MD_doppia_velocita\data\step_3602.mat';

load(LOFT5,'FRAGMENTS')
FR=[FRAGMENTS.DYNAMICS_INITIAL_DATA];
FF=[FRAGMENTS.GEOMETRY_DATA];
FR2=[FR.vel0];
[a,b]=find(vectorNorm3d(FR2')>1.5e4);
a
if ~isempty(a)
    for j=1:length(a)
        aa(j)=norm(FRAGMENTS(a(j)).DYNAMICS_INITIAL_DATA.vel0');
        bb(j)=FF(a(j)).mass0;
    end
    [c,cc]=max(aa)
    bb(cc)
    length(a)
    length(FRAGMENTS)
else
    disp('No abnormal fragments')
end

for i=1:48
inn=['step_',num2str(i),'.mat'];
load(inn,'FRAGMENTS')
disp([i, length(FRAGMENTS)])
end