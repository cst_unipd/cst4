% Standalone for fragments size distribution
% FRAG_DIST=[size cumulative_number]

clear all
close all
%{
% path_initialization();
%}

%% load Lan experimental data (Lan_CN)
% The data are stored as a two column matrix in the form (X',Y')
Data_Lan_CN

%% load CST Lan data
load('E:\AO_8507Catastrophic\CST2_ESTEC\CST3_Lan_settaggi_vecchi\Results\Lan_Satellite_20180920T154551\data\step_1895.mat')


%% DELETE FRAGMENTs & MEs WITH MASS == 0 AND DEFINE DIMENSIONS
n_size=length(FRAGMENTS);
FRAG_for_Plots = FRAGMENTS;
for i=n_size:-1:1
    if FRAG_for_Plots(i).GEOMETRY_DATA.mass==0
        FRAG_for_Plots(i)=[];
    else
        box = boundingBox3d(FRAG_for_Plots(i).GEOMETRY_DATA.c_hull);
        lx = abs(box(2)-box(1));
        ly = abs(box(4)-box(3));
        lz = abs(box(6)-box(5));
        FRAG_for_Plots(i).GEOMETRY_DATA.dimensions=[lx ly lz];
        x(i)=max(FRAG_for_Plots(i).GEOMETRY_DATA.dimensions);
    end
end
n_size=length(FRAG_for_Plots);

ME_for_Plots = ME;
n_size1=length(ME_for_Plots);
for i=n_size1:-1:1
    if ME_for_Plots(i).GEOMETRY_DATA.mass==0
        ME_for_Plots(i)=[];
    end
end
n_size1=length(ME_for_Plots);

if ~isempty(ME_for_Plots)
    FRAG_for_Plots(n_size+1:n_size+n_size1)=ME_for_Plots;
end

n_size=length(FRAG_for_Plots);

%% CREATE Lc, AM RATIO AND VELOCITY DISTRIBUTIONS FROM CST RESULTS
s1 =zeros(1,n_size-1);
k=1;
for i = 2:n_size
    Lc_CST(i-1)=mean(FRAG_for_Plots(i).GEOMETRY_DATA.dimensions);
end

%% data for plot Lc and standard deviation
s=sort(Lc_CST);
n_size=length(Lc_CST);
N=n_size:-1:1;

H_1=[s',N'];
H_ref = Hanada_Lc;

% windowing
global_min = min(H_ref(:,1));
global_max = max(H_ref(:,1));
H_1_new = H_1(and(H_1(:,1)>global_min,H_1(:,1)<global_max),:);

for j=1:length(H_ref)-1
    II1=[];
    II1=find(H_1_new(:,1)>= H_ref(j,1) & H_1_new(:,1)< H_ref(j+1,1));
    if ~isempty(II1)
        for i=1:length(II1)
            CN_1(II1(i),1)= H_1_new(II1(i),1);
            CN_1(II1(i),2)= H_ref(j,2)+(H_ref(j+1,2)-H_ref(j,2))/(H_ref(j+1,1)-H_ref(j,1))*(H_1_new(II1(i),1)-H_ref(j,1));
        end
    end
end
std_dev_matlab=std((H_1_new(:,2)-CN_1(:,2))./(CN_1(:,2)));
stn_dev1=sqrt(1/(length(CN_1(:,1))-1)*sum(((H_1_new(:,2)-CN_1(:,2)).^2)./((CN_1(:,2)).^2)));
disp(['The standard deviation computed with our formula is ',num2str(stn_dev1*100,4.2),'%']);
disp(['The standard deviation computed with Matlab built-in formula is ',num2str(std_dev_matlab*100,4.2),'%']);

%%

h = figure('Position', [100, 50, 500, 500],'Visible', 'on');
loglog(s,N,'+r','LineWidth',2)
grid on
hold on
loglog(Lan_CN(:,1),Lan_CN(:,2),'*k','LineWidth',2)
hold on
title('Comparison among PDFs')
xlabel('Characteristic length L_c [m]')
ylabel('CN(L_c)')
legend('CST Fragments','LAN PDF')
ylim([0.9 10000])
xlim([0.0006 0.6])
