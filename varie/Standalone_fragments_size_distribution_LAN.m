%% plot CN(L_c)
clear all
close all
global MATERIAL_LIST
%% load Autodyn Data
load('dati_Lan2.mat')

%% load Lan experimental data (Lan_CN)
% The data are stored as a two column matrix in the form (X',Y')
Data_Lan_CN

%% load CST Lan data
% load('E:\AO_8507Catastrophic\CST2_ESTEC\CST3_Lan_settaggi_vecchi\Results\Lan_Satellite_20180920T154551\data\step_1895.mat')
% load('E:\AO_8507Catastrophic\CST2_ESTEC\CST3_Lan_settaggi_vecchi\Results\Lan_Satellite_20180920T211449\data\step_82.mat')
% load('E:\AO_8507Catastrophic\CST2_ESTEC\CST3_Lan_settaggi_vecchi\Results\Lan_Satellite_20180920T154551\data\step_1895.mat')
name_file = 'E:\AO_8507Catastrophic\CST2_ESTEC\CST4\Results\Lan_Satellite_20190219T153517\data\step_1396.mat';
load(name_file)
name_mat=[name_file(1:strfind(name_file,'Results')-1),filesep,'set_up',filesep,'material_list.m'];
run(name_mat)
%% DELETE FRAGMENTs & MEs WITH MASS == 0 AND DEFINE DIMENSIONS
n_size=length(FRAGMENTS);
FRAG_for_Plots = FRAGMENTS;
for i=n_size:-1:1
    if FRAG_for_Plots(i).GEOMETRY_DATA.mass==0
        FRAG_for_Plots(i)=[];
    else
        box = boundingBox3d(FRAG_for_Plots(i).GEOMETRY_DATA.c_hull);
        lx = abs(box(2)-box(1));
        ly = abs(box(4)-box(3));
        lz = abs(box(6)-box(5));
        FRAG_for_Plots(i).GEOMETRY_DATA.dimensions=[lx ly lz];
        x(i)=max(FRAG_for_Plots(i).GEOMETRY_DATA.dimensions);
    end
end
n_size=length(FRAG_for_Plots);

ME_for_Plots = ME;
m_tot=0;
for ii=1:length(ME_for_Plots)
    m_tot=m_tot+ME_for_Plots(ii).GEOMETRY_DATA.mass0;
end
n_size1=length(ME_for_Plots);
for i=n_size1:-1:1
    if ME_for_Plots(i).GEOMETRY_DATA.mass==0
        ME_for_Plots(i)=[];
    end
end
n_size1=length(ME_for_Plots);

if ~isempty(ME_for_Plots)
    FRAG_for_Plots(n_size+1:n_size+n_size1)=ME_for_Plots;
end

n_size=length(FRAG_for_Plots);

%% CREATE Lc, AM RATIO AND VELOCITY DISTRIBUTIONS FROM CST RESULTS
s1 =zeros(1,n_size-1);
k=1;
for i = 2:n_size
    Lc_CST(i-1)=mean(FRAG_for_Plots(i).GEOMETRY_DATA.dimensions);
end

n_frags_bubbles=0;
if ~isempty(PROPAGATION_THRESHOLD)
    for ii = 1:length(BUBBLE)
        if BUBBLE(ii).GEOMETRY_DATA.mass~=0
            n_frags_bubbles=n_frags_bubbles+BUBBLE(ii).GEOMETRY_DATA.mass/(4/3*PROPAGATION_THRESHOLD.bubble_radius_th^3*pi()*MATERIAL_LIST(BUBBLE(ii).material_ID).density);
        end
    end
end

%% data for plot Lc and standard deviation
s=sort(Lc_CST);
n_size=length(Lc_CST);
N=n_size:-1:1;
NASA_SBM = 0.1*(m_tot)^0.75*Lan_CN(:,1).^(-1.71);
H_1=[s',N'];
H_ref = Lan_CN;

% windowing
global_min = min(H_ref(:,1));
global_max = max(H_ref(:,1));
H_1_new = H_1(and(H_1(:,1)>global_min,H_1(:,1)<global_max),:);

for j=1:length(H_ref)-1
    II1=[];
    II1=find(H_1_new(:,1)>= H_ref(j,1) & H_1_new(:,1)< H_ref(j+1,1));
    if ~isempty(II1)
        for i=1:length(II1)
            CN_1(II1(i),1)= H_1_new(II1(i),1);
            CN_1(II1(i),2)= H_ref(j,2)+(H_ref(j+1,2)-H_ref(j,2))/(H_ref(j+1,1)-H_ref(j,1))*(H_1_new(II1(i),1)-H_ref(j,1));
        end
    end
end
std_dev_matlab=std((H_1_new(:,2)-CN_1(:,2))./(CN_1(:,2)));
stn_dev1=sqrt(1/(length(CN_1(:,1))-1)*sum(((H_1_new(:,2)-CN_1(:,2)).^2)./((CN_1(:,2)).^2)));
disp(['The standard deviation computed with our formula is ',num2str(stn_dev1*100,4.2),'%']);
disp(['The standard deviation computed with Matlab built-in formula is ',num2str(std_dev_matlab*100,4.2),'%']);

%% plot Lc
scrsz = get(0,'ScreenSize');
xyfontsize=24;
symbols=['o','s','d','^','p','h','x','.','<','>'];
figure('Position',[5 5 0.6*scrsz(3) 0.9*scrsz(4)])
jj=1;
kk=1;
leg_text='';
plots=[1 2];
for ii=1:length(plots)
    vel=strrep(plot_f(plots(ii)).name(strfind(plot_f(plots(ii)).name,'_v')+2:strfind(plot_f(plots(ii)).name,'_r')-1),'_','.');
    velocity(kk+1)=str2double(vel);
    radius=strrep(plot_f(plots(ii)).name(strfind(plot_f(plots(ii)).name,'_r')+2:strfind(plot_f(plots(ii)).name,'_h')-1),'_','.');
    int = plot_f(plots(ii)).TT(:,1)>(0.004) & plot_f(plots(ii)).TT(:,1)< max(plot_f(plots(ii)).TT(:,1));
    h_plot(jj)=plot(plot_f(plots(ii)).TT(int,1),plot_f(plots(ii)).TT(int,4),'MarkerEdgeColor',colorOrder(kk+1,:),'Marker',symbols(kk+1),'LineStyle','none','Color',colorOrder(kk+1,:),'LineWidth',lwidth);
    leg_text=strvcat(leg_text,['SPH @ \Delta x = ',num2str(plot_f(plots(ii)).sph),' mm at ',num2str(plot_f(plots(ii)).tl),' ms']);
    kk=kk+1;
    jj=jj+1;
    hold on
    grid on
end
xx_Lan = Lan_CN(Lan_CN(:,1)>0.004,1);
yy_Lan = Lan_CN(Lan_CN(:,1)>0.004,2);
h_plot(jj)=plot(xx_Lan,yy_Lan,'MarkerEdgeColor',colorOrder(1,:),'Marker','none','LineStyle','-','Color',colorOrder(1,:),'LineWidth',lwidth+2);
leg_text=strvcat(leg_text,'Lan Experimental Distribution');
h_plot(jj+1)=plot([s(1),s],[N(1)+n_frags_bubbles,N],'MarkerEdgeColor','r','Marker','none','LineStyle','-','Color','r','LineWidth',lwidth+2);
leg_text=strvcat(leg_text,['CST Distribution at t = ',num2str(tf,3),' s']);
h_plot(jj+2)=plot(Lan_CN(:,1),NASA_SBM,'MarkerEdgeColor','m','Marker','none','LineStyle',':','Color','m','LineWidth',2);
leg_text=strvcat(leg_text,'NASA SBM');
xlabel('Characteristic Length L_c [m]','FontSize',xyfontsize,'FontWeight','bold')
ylabel('CN(L_c)','FontSize',xyfontsize,'FontWeight','bold')
set(gca,'fontsize',xyfontsize,'FontWeight','bold')
legend(h_plot,leg_text,'Location','northeast')
set(gca,'XScale', 'log','YScale', 'log')
hold on
yticks([1 10 100 1000 10000])
ylim([0.9 10000])
xticks([0.001 0.01 0.1 1])
xlim([4.e-3 1.1])
hold on