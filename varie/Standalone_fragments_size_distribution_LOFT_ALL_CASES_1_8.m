% Standalone for fragments size distribution
clear all
close all
clc
diary LOFT_Simulation_Q_loss_Q.txt
diary on
%% Load Data CST
name_file_result_LOFT1='E:\AO_8507Catastrophic\CST2_ESTEC\CST3_ADV_SHIELD\Results\Q_loss_Q\LOFT1\step_1007.mat';
name_file_result_LOFT2='E:\AO_8507Catastrophic\CST2_ESTEC\CST3_ADV_SHIELD\Results\Q_loss_Q\LOFT2\step_1092.mat';
name_file_result_LOFT3='E:\AO_8507Catastrophic\CST2_ESTEC\CST3_ADV_SHIELD\Results\Q_loss_Q\LOFT3\step_1093.mat';
name_file_result_LOFT4='E:\AO_8507Catastrophic\CST2_ESTEC\CST3_ADV_SHIELD\Results\Q_loss_Q\LOFT4\step_2362.mat';
name_file_result_LOFT5='E:\AO_8507Catastrophic\CST2_ESTEC\CST3_ADV_SHIELD\Results\Q_loss_Q\LOFT5\step_1027.mat';
name_file_result_LOFT6='E:\AO_8507Catastrophic\CST2_ESTEC\CST3_ADV_SHIELD\Results\Q_loss_Q\LOFT6\step_1450.mat';
name_file_result_LOFT7='E:\AO_8507Catastrophic\CST2_ESTEC\CST3_ADV_SHIELD\Results\Q_loss_Q\LOFT7\step_2860.mat';
name_file_result_LOFT8='E:\AO_8507Catastrophic\CST2_ESTEC\CST3_ADV_SHIELD\Results\Q_loss_Q\LOFT8\step_3805.mat';

%% processing LOFT1 scenario data
load(name_file_result_LOFT1,'FRAGMENTS','ME','BUBBLE','PROPAGATION_THRESHOLD','link_data','tf','RunTime')
name_mat=[name_file_result_LOFT1(1:strfind(name_file_result_LOFT1,'Results')-1),filesep,'set_up',filesep,'material_list.m'];
run(name_mat)

mass_imp_1=0;
mass_LOFT=0;
q_imp_1 = [0 0 0]';
length_LOFT_ME = 51;
for i = 1:length_LOFT_ME 
    mass_LOFT = mass_LOFT + ME(i).GEOMETRY_DATA.mass0;
end

for i = (length_LOFT_ME+1):length(ME)
    mass_imp_1 = mass_imp_1 + ME(i).GEOMETRY_DATA.mass0;
    q_imp_1 = q_imp_1 + ME(i).GEOMETRY_DATA.mass0*ME(i).DYNAMICS_INITIAL_DATA.vel0;
end
v_p_1 = 1/mass_imp_1*q_imp_1/1000;

[n_frags_bubbles_LOFT1,s_LOFT1,N_LOFT1,Am_CST_LOFT1,delta_v_CST_LOFT1]=postprocess_data(FRAGMENTS,ME,BUBBLE,PROPAGATION_THRESHOLD,link_data,length_LOFT_ME);

[m_tot_LOFT1,volume_tot_LOFT1]=LOFT_mass_intersection(length_LOFT_ME+1,ME,1);

% % [Area_LOFT]=Proj_Area_LOFT2(length_LOFT_ME,ME);
load('Area_LOFT_proj.mat')
Area_imp1=[0.1*0.1,0.1*0.1,0.004*sqrt(2)*0.1]';
m_star1=mass_LOFT*mean(Area_imp1)/mean(Area_LOFT);

tf_LOFT1 = sprintf('%25.4f',tf);
if exist('RunTime','var')
    cputime_LOFT1 = sprintf('%20.2f',RunTime/60);
else
    cputime_LOFT1 = '                   -';
end
final_step_LOFT1 = sprintf('%24d',str2double(name_file_result_LOFT1(strfind(name_file_result_LOFT1,'step_')+5:end-4)));
EMR_LOFT1 = sprintf('%5.2f',1/2*mass_imp_1*norm(v_p_1*1000)^2/(mass_imp_1+mass_LOFT)/1000);

clear FRAGMENTS ME BUBBLE PROPAGATION_THRESHOLD link_data tf RunTime

%% processing LOFT2 scenario data

load(name_file_result_LOFT2,'FRAGMENTS','ME','BUBBLE','PROPAGATION_THRESHOLD','link_data','tf','RunTime')

mass_imp_2=0;
q_imp_2 = [0 0 0]';

for i = (length_LOFT_ME+1):length(ME)
    mass_imp_2 = mass_imp_2 + ME(i).GEOMETRY_DATA.mass0;
    q_imp_2 = q_imp_2 + ME(i).GEOMETRY_DATA.mass0*ME(i).DYNAMICS_INITIAL_DATA.vel0;
end
v_p_2 = 1/mass_imp_2*q_imp_2/1000;

[n_frags_bubbles_LOFT2,s_LOFT2,N_LOFT2,Am_CST_LOFT2,delta_v_CST_LOFT2]=postprocess_data(FRAGMENTS,ME,BUBBLE,PROPAGATION_THRESHOLD,link_data,length_LOFT_ME);

[m_tot_LOFT2,volume_tot_LOFT2]=LOFT_mass_intersection(length_LOFT_ME+1,ME,2);
Area_imp2=[0.1*0.1,0.1*0.1,0.1*0.1]';
m_star2=mass_LOFT*mean(Area_imp2)/mean(Area_LOFT);

tf_LOFT2 = sprintf('%25.4f',tf);
if exist('RunTime','var')
    cputime_LOFT2 = sprintf('%20.2f',RunTime/60);
else
    cputime_LOFT2 = '                   -';
end

final_step_LOFT2 = sprintf('%24d',str2double(name_file_result_LOFT2(strfind(name_file_result_LOFT2,'step_')+5:end-4)));
EMR_LOFT2 = sprintf('%5.2f',1/2*mass_imp_2*norm(v_p_2*1000)^2/(mass_imp_2+mass_LOFT)/1000);

clear FRAGMENTS ME BUBBLE PROPAGATION_THRESHOLD link_data tf RunTime

%% processing LOFT3 scenario data

load(name_file_result_LOFT3,'FRAGMENTS','ME','BUBBLE','PROPAGATION_THRESHOLD','link_data','tf','RunTime')

mass_imp_3=0;
q_imp_3 = [0 0 0]';

for i = (length_LOFT_ME+1):length(ME)
    mass_imp_3 = mass_imp_3 + ME(i).GEOMETRY_DATA.mass0;
    q_imp_3 = q_imp_3 + ME(i).GEOMETRY_DATA.mass0*ME(i).DYNAMICS_INITIAL_DATA.vel0;
end
v_p_3 = 1/mass_imp_3*q_imp_3/1000;

[n_frags_bubbles_LOFT3,s_LOFT3,N_LOFT3,Am_CST_LOFT3,delta_v_CST_LOFT3]=postprocess_data(FRAGMENTS,ME,BUBBLE,PROPAGATION_THRESHOLD,link_data,length_LOFT_ME);

[m_tot_LOFT3,volume_tot_LOFT3]=LOFT_mass_intersection(length_LOFT_ME+1,ME,3);
Area_imp3=[0.2*0.2,0.2*0.3,0.2*0.3]';
m_star3=mass_LOFT*mean(Area_imp3)/mean(Area_LOFT);

tf_LOFT3 = sprintf('%25.4f',tf);
if exist('RunTime','var')
    cputime_LOFT3 = sprintf('%20.2f',RunTime/60);
else
    cputime_LOFT3 = '                   -';
end

final_step_LOFT3 = sprintf('%24d',str2double(name_file_result_LOFT3(strfind(name_file_result_LOFT3,'step_')+5:end-4)));
EMR_LOFT3 = sprintf('%5.2f',1/2*mass_imp_3*norm(v_p_3*1000)^2/(mass_imp_3+mass_LOFT)/1000);
clear FRAGMENTS ME BUBBLE PROPAGATION_THRESHOLD link_data tf RunTime

%% processing LOFT4 scenario data

load(name_file_result_LOFT4,'FRAGMENTS','ME','BUBBLE','PROPAGATION_THRESHOLD','link_data','tf','RunTime')

[n_frags_bubbles_LOFT4,s_LOFT4,N_LOFT4,Am_CST_LOFT4,delta_v_CST_LOFT4]=postprocess_data(FRAGMENTS,ME,BUBBLE,PROPAGATION_THRESHOLD,link_data,length_LOFT_ME);

mass_imp_4 = mass_imp_3;

[m_tot_LOFT4,volume_tot_LOFT4]=LOFT_mass_intersection(length_LOFT_ME+1,ME,4);
Area_imp4=Area_imp3;
m_star4=mass_LOFT*mean(Area_imp4)/mean(Area_LOFT);

tf_LOFT4 = sprintf('%25.4f',tf);
if exist('RunTime','var')
    cputime_LOFT4 = sprintf('%20.2f',RunTime/60);
else
    cputime_LOFT4 = '                   -';
end

final_step_LOFT4 = sprintf('%24d',str2double(name_file_result_LOFT4(strfind(name_file_result_LOFT4,'step_')+5:end-4)));
EMR_LOFT4 = EMR_LOFT3;
clear FRAGMENTS ME BUBBLE PROPAGATION_THRESHOLD link_data tf RunTime

%% processing LOFT5 scenario data

load(name_file_result_LOFT5,'FRAGMENTS','ME','BUBBLE','PROPAGATION_THRESHOLD','link_data','tf','RunTime')

[n_frags_bubbles_LOFT5,s_LOFT5,N_LOFT5,Am_CST_LOFT5,delta_v_CST_LOFT5]=postprocess_data(FRAGMENTS,ME,BUBBLE,PROPAGATION_THRESHOLD,link_data,length_LOFT_ME);

mass_imp_5 = mass_imp_3;

[m_tot_LOFT5,volume_tot_LOFT5]=LOFT_mass_intersection(length_LOFT_ME+1,ME,5);
Area_imp5=Area_imp3;
m_star5=mass_LOFT*mean(Area_imp5)/mean(Area_LOFT);

tf_LOFT5 = sprintf('%25.4f',tf);
if exist('RunTime','var')
    cputime_LOFT5 = sprintf('%20.2f',RunTime/60);
else
    cputime_LOFT5 = '                   -';
end

final_step_LOFT5 = sprintf('%24d',str2double(name_file_result_LOFT5(strfind(name_file_result_LOFT5,'step_')+5:end-4)));
EMR_LOFT5 = EMR_LOFT3;
clear FRAGMENTS ME BUBBLE PROPAGATION_THRESHOLD link_data tf RunTime

%% processing LOFT6 scenario data

load(name_file_result_LOFT6,'FRAGMENTS','ME','BUBBLE','PROPAGATION_THRESHOLD','link_data','tf','RunTime')

[n_frags_bubbles_LOFT6,s_LOFT6,N_LOFT6,Am_CST_LOFT6,delta_v_CST_LOFT6]=postprocess_data(FRAGMENTS,ME,BUBBLE,PROPAGATION_THRESHOLD,link_data,length_LOFT_ME);

mass_imp_6 = mass_imp_3;

[m_tot_LOFT6,volume_tot_LOFT6]=LOFT_mass_intersection(length_LOFT_ME+1,ME,6);
Area_imp6=Area_imp3;
m_star6=mass_LOFT*mean(Area_imp6)/mean(Area_LOFT);

tf_LOFT6 = sprintf('%25.4f',tf);
if exist('RunTime','var')
    cputime_LOFT6 = sprintf('%20.2f',RunTime/60);
else
    cputime_LOFT6 = '                   -';
end

final_step_LOFT6 = sprintf('%24d',str2double(name_file_result_LOFT6(strfind(name_file_result_LOFT6,'step_')+5:end-4)));
EMR_LOFT6 = EMR_LOFT3;
clear FRAGMENTS ME BUBBLE PROPAGATION_THRESHOLD link_data tf RunTime
%{d
%% processing LOFT7 scenario data

load(name_file_result_LOFT7,'FRAGMENTS','ME','BUBBLE','PROPAGATION_THRESHOLD','link_data','tf','RunTime')

[n_frags_bubbles_LOFT7,s_LOFT7,N_LOFT7,Am_CST_LOFT7,delta_v_CST_LOFT7]=postprocess_data(FRAGMENTS,ME,BUBBLE,PROPAGATION_THRESHOLD,link_data,length_LOFT_ME);

mass_imp_7=0;
q_imp_7 = [0 0 0]';

for i = (length_LOFT_ME+1):length(ME)
    mass_imp_7 = mass_imp_7 + ME(i).GEOMETRY_DATA.mass0;
    q_imp_7 = q_imp_7 + ME(i).GEOMETRY_DATA.mass0*ME(i).DYNAMICS_INITIAL_DATA.vel0;
end
v_p_7 = 1/mass_imp_7*q_imp_7/1000;

[m_tot_LOFT7,volume_tot_LOFT7]=LOFT_mass_intersection(length_LOFT_ME+1,ME,7);
Area_imp7=[0.4*0.4,0.4*0.3,0.4*0.3];
m_star7=mass_LOFT*mean(Area_imp7)/mean(Area_LOFT);

tf_LOFT7 = sprintf('%25.4f',tf);
if exist('RunTime','var')
    cputime_LOFT7 = sprintf('%20.2f',RunTime/60);
else
    cputime_LOFT7 = '                   -';
end

final_step_LOFT7 = sprintf('%24d',str2double(name_file_result_LOFT7(strfind(name_file_result_LOFT7,'step_')+5:end-4)));
EMR_LOFT7 = sprintf('%5.2f',1/2*mass_imp_7*norm(v_p_7*1000)^2/(mass_imp_7+mass_LOFT)/1000);
clear FRAGMENTS ME BUBBLE PROPAGATION_THRESHOLD link_data tf RunTime

%% processing LOFT8 scenario data

load(name_file_result_LOFT8,'FRAGMENTS','ME','BUBBLE','PROPAGATION_THRESHOLD','link_data','tf','RunTime')

[n_frags_bubbles_LOFT8,s_LOFT8,N_LOFT8,Am_CST_LOFT8,delta_v_CST_LOFT8]=postprocess_data(FRAGMENTS,ME,BUBBLE,PROPAGATION_THRESHOLD,link_data,length_LOFT_ME);

mass_imp_8=0;
q_imp_8 = [0 0 0]';

for i = (length_LOFT_ME+1):length(ME)
    mass_imp_8 = mass_imp_8 + ME(i).GEOMETRY_DATA.mass0;
    q_imp_8 = q_imp_8 + ME(i).GEOMETRY_DATA.mass0*ME(i).DYNAMICS_INITIAL_DATA.vel0;
end
v_p_8 = 1/mass_imp_8*q_imp_8/1000;

[m_tot_LOFT8,volume_tot_LOFT8]=LOFT_mass_intersection(length_LOFT_ME+1,ME,8);
Area_imp8=Area_imp7;
m_star8=mass_LOFT*mean(Area_imp8)/mean(Area_LOFT);

tf_LOFT8 = sprintf('%25.4f',tf);
if exist('RunTime','var')
    cputime_LOFT8 = sprintf('%20.2f',RunTime/60);
else
    cputime_LOFT8 = '                   -';
end

final_step_LOFT8 = sprintf('%24d',str2double(name_file_result_LOFT8(strfind(name_file_result_LOFT8,'step_')+5:end-4)));
EMR_LOFT8 = sprintf('%5.2f',1/2*mass_imp_8*norm(v_p_8*1000)^2/(mass_imp_8+mass_LOFT)/1000);
clear FRAGMENTS ME BUBBLE PROPAGATION_THRESHOLD link_data tf RunTime
%{x
%}
%% plot finale Lc
%{d
colorOrder =  [ ...
        0            0            0       ;...% 6 BLACK
        0            0            0.7       ;...% 1 BLUE
        0            0.7            0       ;...% 3 GREEN (pale)
        0.7            0            0.7       ;...% 5 MAGENTA (pale)
        0            0.8            0.8       ;...% 4 CYAN
        0.6          0.5          0.4     ;  % 15 BROWN (dark)
        1            1            0       ;...% 7 YELLOW (pale)
        0            0.75         0.75    ;...% 8 TURQUOISE
        0            0.5          0       ;...% 9 GREEN (dark)
        0.75         0.75         0       ;...% 10 YELLOW (dark)
        1            0.50         0.25    ;...% 11 ORANGE
        0.75         0            0.75    ;...% 12 MAGENTA (dark)
        0.7          0.7          0.7     ;...% 13 GREY
        0.8          0.7          0.6     ;...% 14 BROWN (pale)
        0.4          0.7          0.6     ;...
        0.4          0          0.6     ;...
        0.4          0.6          0     ;...
        0.6          0.5          0.4 ];  % 18 BROWN (dark)
    


%% Plot Lc LOFT tutti
LOFT_all=[s_LOFT1(1,:),s_LOFT2(1,:),s_LOFT3(1,:),s_LOFT4(1,:),s_LOFT5(1,:),s_LOFT6(1,:),s_LOFT7(1,:),s_LOFT8(1,:)];
Lc_NASA=[min(LOFT_all):(max(LOFT_all)-min(LOFT_all))/100:max(LOFT_all),1];
NASA_SBM_01_sub = 0.1*(mass_imp_1*norm(v_p_1))^0.75*Lc_NASA.^(-1.71);
NASA_SBM_1_sub = 0.1*(mass_imp_2*norm(v_p_2))^0.75*Lc_NASA.^(-1.71);
NASA_SBM_10_sub = 0.1*(mass_imp_3*norm(v_p_3))^0.75*Lc_NASA.^(-1.71);
NASA_SBM_40_sub = 0.1*(mass_imp_7*norm(v_p_7))^0.75*Lc_NASA.^(-1.71);
% NASA_SBM_LOFT8_sub = 0.1*(mass_imp_8*norm(v_p_8))^0.75*Lc_NASA.^(-1.71);

load(name_file_result_LOFT1,'ME')
m_tot=0;
for ii = 1:length(ME)
    m_tot=m_tot+ME(ii).GEOMETRY_DATA.mass0;
end
NASA_SBM_01_cat = 0.1*(m_tot)^0.75*Lc_NASA.^(-1.71);
NASA_SBM_1_cat = 0.1*(m_tot-mass_imp_1+mass_imp_2)^0.75*Lc_NASA.^(-1.71);
NASA_SBM_10_cat = 0.1*(m_tot-mass_imp_1+mass_imp_3)^0.75*Lc_NASA.^(-1.71);
NASA_SBM_40_cat = 0.1*(m_tot-mass_imp_1+mass_imp_7)^0.75*Lc_NASA.^(-1.71);
Proj_Area_LOFT;
NASA_SBM_LOFT7_cat_mod = 0.1*((m_tot-mass_imp_1)*A_I_su_A_T+mass_imp_7)^0.75*Lc_NASA.^(-1.71);

xx_lim = [min(Lc_NASA) max(Lc_NASA)];
yy_lim = [1e0,1e6];

fontsizexy=24;
fontsizetitle=24;
diary off
adj_title = '_QlossQ_20190130';
%{f
% 
%% Plot Lc LOFT cases 1-6 CST

figure('units','normalized','outerposition',[0 0 1 1],'Visible', 'on')
loglog(s_LOFT1(1,:),[N_LOFT1(1,1)+n_frags_bubbles_LOFT1(1),N_LOFT1(1,2:end)],'MarkerFaceColor',colorOrder(17,:),'Marker','d','LineStyle','none')
hold on
loglog(s_LOFT2(1,:),[N_LOFT2(1,1)+n_frags_bubbles_LOFT2(1),N_LOFT2(1,2:end)],'MarkerFaceColor',colorOrder(14,:),'Marker','*','LineStyle','none')
hold on
loglog(s_LOFT3(1,:),[N_LOFT3(1,1)+n_frags_bubbles_LOFT3(1),N_LOFT3(1,2:end)],'MarkerEdgeColor',colorOrder(11,:),'MarkerFaceColor',colorOrder(11,:),'Marker','o','LineStyle','none')
grid on
hold on
loglog(s_LOFT4(1,:),[N_LOFT4(1,1)+n_frags_bubbles_LOFT4(1),N_LOFT4(1,2:end)],'MarkerEdgeColor',colorOrder(5,:),'MarkerFaceColor',colorOrder(5,:),'Marker','^','LineStyle','none')
hold on
loglog(s_LOFT5(1,:),[N_LOFT5(1,1)+n_frags_bubbles_LOFT5(1),N_LOFT5(1,2:end)],'MarkerEdgeColor',colorOrder(10,:),'MarkerFaceColor',colorOrder(10,:),'Marker','<','LineStyle','none')
hold on
loglog(s_LOFT6(1,:),[N_LOFT6(1,1)+n_frags_bubbles_LOFT6(1),N_LOFT6(1,2:end)],'MarkerFaceColor',colorOrder(4,:),'Marker','x','LineStyle','none')
hold on
loglog(Lc_NASA,NASA_SBM_01_sub,'--r','LineWidth',2)
loglog(Lc_NASA,NASA_SBM_1_sub,':r','LineWidth',2)
loglog(Lc_NASA,NASA_SBM_10_sub,'-.r','LineWidth',2)
loglog(Lc_NASA,NASA_SBM_10_cat,'-k','LineWidth',2)
set(gca,'FontName','Helvetica','FontSize',fontsizexy,'FontWeight','bold')
title('CST results LOFT 1-6 modified','FontName','Helvetica','FontSize',fontsizetitle,'FontWeight','bold')
xlabel('L_c [m]','FontName','Helvetica','FontSize',fontsizexy)
ylabel('Cumulative number CN','FontName','Helvetica','FontSize',fontsizexy)
lgd=legend(['CST-LOFT1 (EMR = ',EMR_LOFT1,' J/g)'],['CST-LOFT2 (EMR = ',EMR_LOFT2,' J/g)'],['CST-LOFT3 (EMR = ',EMR_LOFT3,' J/g)'],['CST-LOFT4 (EMR = ',EMR_LOFT4,' J/g)'],['CST-LOFT5 (EMR = ',EMR_LOFT5,' J/g)'],['CST-LOFT6 (EMR = ',EMR_LOFT6,' J/g)'],'NASA SBM - SubCat 0.1 kg','NASA SBM - SubCat 1 kg','NASA SBM - SubCat 10 kg','NASA SBM - Cat');
lgd.FontName='Helvetica';
lgd.FontSize=fontsizexy;
set(gca,'XLim',xx_lim)
set(gca,'YLim',yy_lim)

out_fig = ['LOFT_1-6',adj_title];
saveas(gcf,out_fig,'fig')
saveas(gcf,out_fig,'png')

%% Plot Lc LOFT cases CST

figure('units','normalized','outerposition',[0 0 1 1],'Visible', 'on')
loglog(s_LOFT3(1,:),[N_LOFT3(1,1)+n_frags_bubbles_LOFT3(1),N_LOFT3(1,2:end)],'MarkerEdgeColor',colorOrder(11,:),'MarkerFaceColor',colorOrder(11,:),'Marker','o','LineStyle','none')
grid on
hold on
loglog(s_LOFT4(1,:),[N_LOFT4(1,1)+n_frags_bubbles_LOFT4(1),N_LOFT4(1,2:end)],'MarkerEdgeColor',colorOrder(5,:),'MarkerFaceColor',colorOrder(5,:),'Marker','^','LineStyle','none')
hold on
loglog(s_LOFT5(1,:),[N_LOFT5(1,1)+n_frags_bubbles_LOFT5(1),N_LOFT5(1,2:end)],'MarkerEdgeColor',colorOrder(10,:),'MarkerFaceColor',colorOrder(10,:),'Marker','<','LineStyle','none')
hold on
loglog(s_LOFT6(1,:),[N_LOFT6(1,1)+n_frags_bubbles_LOFT6(1),N_LOFT6(1,2:end)],'MarkerFaceColor',colorOrder(4,:),'Marker','x','LineStyle','none')
hold on
loglog(s_LOFT7(1,:),[N_LOFT7(1,1)+n_frags_bubbles_LOFT7(1),N_LOFT7(1,2:end)],'MarkerFaceColor',colorOrder(17,:),'Marker','d','LineStyle','none')
hold on
loglog(s_LOFT8(1,:),[N_LOFT8(1,1)+n_frags_bubbles_LOFT8(1),N_LOFT8(1,2:end)],'MarkerFaceColor',colorOrder(14,:),'Marker','d','LineStyle','none')
hold on
loglog(Lc_NASA,NASA_SBM_10_sub,'-.r','LineWidth',2)
loglog(Lc_NASA,NASA_SBM_40_sub,'-r','LineWidth',2)
loglog(Lc_NASA,NASA_SBM_10_cat,'-k','LineWidth',2)
set(gca,'FontName','Helvetica','FontSize',fontsizexy,'FontWeight','bold')
title('CST results LOFT 3-8 modified','FontName','Helvetica','FontSize',fontsizetitle,'FontWeight','bold')
xlabel('L_c [m]','FontName','Helvetica','FontSize',fontsizexy)
ylabel('Cumulative number CN','FontName','Helvetica','FontSize',fontsizexy)
lgd=legend(['CST-LOFT3 (EMR = ',EMR_LOFT3,' J/g)'],['CST-LOFT4 (EMR = ',EMR_LOFT4,' J/g)'],['CST-LOFT5 (EMR = ',EMR_LOFT5,' J/g)'],['CST-LOFT6 (EMR = ',EMR_LOFT6,' J/g)'],['CST-LOFT7 (EMR = ',EMR_LOFT7,' J/g)'],['CST-LOFT8 (EMR = ',EMR_LOFT8,' J/g)'],'NASA SBM - SubCat 10 kg','NASA SBM - SubCat 40 kg','NASA SBM - Cat');
lgd.FontName='Helvetica';
lgd.FontSize=fontsizexy;
set(gca,'XLim',xx_lim)
set(gca,'YLim',yy_lim)

out_fig = ['LOFT_3-8',adj_title];
saveas(gcf,out_fig,'fig')
saveas(gcf,out_fig,'png')

%% Plot Lc LOFT 1 modified CST
clear lgd
NASA_SBM_LOFT1_cat_mod= 0.1*(mass_imp_1+m_tot_LOFT1)^0.75*Lc_NASA.^(-1.71); 
figure('units','normalized','outerposition',[0 0 1 1],'Visible', 'on')
loglog(s_LOFT1(1,:),[N_LOFT1(1,1)+n_frags_bubbles_LOFT1(1),N_LOFT1(1,2:end)],'MarkerFaceColor',colorOrder(17,:),'Marker','d','LineStyle','none')
hold on
grid on
if nnz(s_LOFT1(2,:))>1
loglog(s_LOFT1(2,1:nnz(s_LOFT1(2,:))),[N_LOFT1(2,1)+n_frags_bubbles_LOFT1(2),N_LOFT1(2,2:1:nnz(s_LOFT1(2,:)))],'MarkerFaceColor',colorOrder(4,:),'Marker','^','LineStyle','none')
hold on
end
if nnz(s_LOFT1(3,:))>1
loglog(s_LOFT1(3,:),[N_LOFT1(3,1)+n_frags_bubbles_LOFT1(3),N_LOFT1(3,2:end)],'MarkerFaceColor',colorOrder(8,:),'Marker','*','LineStyle','none')
hold on
end
loglog(Lc_NASA,NASA_SBM_01_sub,'--r','LineWidth',2)
loglog(Lc_NASA,NASA_SBM_01_cat,'-k','LineWidth',2)
loglog(Lc_NASA,NASA_SBM_LOFT1_cat_mod,'-b','LineWidth',2)
set(gca,'FontName','Helvetica','FontSize',fontsizexy,'FontWeight','bold')
title('CST results LOFT 1 modified','FontName','Helvetica','FontSize',fontsizetitle,'FontWeight','bold')
xlabel('L_c [m]','FontName','Helvetica','FontSize',fontsizexy)
ylabel('Cumulative number CN','FontName','Helvetica','FontSize',fontsizexy)
if nnz(s_LOFT1(2,:))>1 && nnz(s_LOFT1(3,:))>1
    lgd=legend(['CST-LOFT1 (EMR = ',EMR_LOFT1,' J/g)'],'CST-LOFT1 target','CST-LOFT1 impactor','NASA SBM - SubCat 0.1 kg','NASA SBM - Cat','NASA SBM - Cat LOFT1 modified');
elseif nnz(s_LOFT1(2,:))>1 && nnz(s_LOFT1(3,:))<2    
    lgd=legend(['CST-LOFT1 (EMR = ',EMR_LOFT1,' J/g)'],'CST-LOFT1 target','NASA SBM - SubCat 0.1 kg','NASA SBM - Cat','NASA SBM - Cat LOFT1 modified');
elseif nnz(s_LOFT1(2,:))<2 && nnz(s_LOFT1(3,:))>1
    lgd=legend(['CST-LOFT1 (EMR = ',EMR_LOFT1,' J/g)'],'CST-LOFT1 impactor','NASA SBM - SubCat 0.1 kg','NASA SBM - Cat','NASA SBM - Cat LOFT1 modified');
else
    lgd=legend(['CST-LOFT1 (EMR = ',EMR_LOFT1,' J/g)'],'NASA SBM - SubCat 0.1 kg','NASA SBM - Cat','NASA SBM - Cat LOFT1 modified');
end
lgd.FontName='Helvetica';
lgd.FontSize=fontsizexy;
set(gca,'XLim',xx_lim)
set(gca,'YLim',yy_lim)

out_fig = ['LOFT_1newmodel',adj_title];
saveas(gcf,out_fig,'fig')
saveas(gcf,out_fig,'png')

%% Plot Lc LOFT 2 modified CST
clear lgd
NASA_SBM_LOFT2_cat_mod= 0.1*(mass_imp_2+m_tot_LOFT2)^0.75*Lc_NASA.^(-1.71); 
figure('units','normalized','outerposition',[0 0 1 1],'Visible', 'on')
loglog(s_LOFT2(1,:),[N_LOFT2(1,1)+n_frags_bubbles_LOFT2(1),N_LOFT2(1,2:end)],'MarkerFaceColor',colorOrder(17,:),'Marker','d','LineStyle','none')
hold on
grid on
if nnz(s_LOFT2(2,:))>1
loglog(s_LOFT2(2,1:nnz(s_LOFT2(2,:))),[N_LOFT2(2,1)+n_frags_bubbles_LOFT2(2),N_LOFT2(2,2:1:nnz(s_LOFT2(2,:)))],'MarkerFaceColor',colorOrder(4,:),'Marker','^','LineStyle','none')
hold on
end
if nnz(s_LOFT2(3,:))>1
loglog(s_LOFT2(3,:),[N_LOFT2(3,1)+n_frags_bubbles_LOFT2(3),N_LOFT2(3,2:end)],'MarkerFaceColor',colorOrder(8,:),'Marker','*','LineStyle','none')
hold on
end
loglog(Lc_NASA,NASA_SBM_1_sub,':r','LineWidth',2)
loglog(Lc_NASA,NASA_SBM_1_cat,'-k','LineWidth',2)
loglog(Lc_NASA,NASA_SBM_LOFT2_cat_mod,'-b','LineWidth',2)
set(gca,'FontName','Helvetica','FontSize',fontsizexy,'FontWeight','bold')
title('CST results LOFT 2 modified','FontName','Helvetica','FontSize',fontsizetitle,'FontWeight','bold')
xlabel('L_c [m]','FontName','Helvetica','FontSize',fontsizexy)
ylabel('Cumulative number CN','FontName','Helvetica','FontSize',fontsizexy)
if nnz(s_LOFT2(2,:))>1 && nnz(s_LOFT2(3,:))>1
    lgd=legend(['CST-LOFT2 (EMR = ',EMR_LOFT2,' J/g)'],'CST-LOFT2 target','CST-LOFT2 impactor','NASA SBM - SubCat 1 kg','NASA SBM - Cat','NASA SBM - Cat LOFT2 modified');
elseif nnz(s_LOFT2(2,:))>1 && nnz(s_LOFT2(3,:))<2    
    lgd=legend(['CST-LOFT2 (EMR = ',EMR_LOFT2,' J/g)'],'CST-LOFT2 target','NASA SBM - SubCat 1 kg','NASA SBM - Cat','NASA SBM - Cat LOFT2 modified');
elseif nnz(s_LOFT2(2,:))<2 && nnz(s_LOFT2(3,:))>1
    lgd=legend(['CST-LOFT2 (EMR = ',EMR_LOFT2,' J/g)'],'CST-LOFT2 impactor','NASA SBM - SubCat 1 kg','NASA SBM - Cat','NASA SBM - Cat LOFT2 modified');
else
    lgd=legend(['CST-LOFT2 (EMR = ',EMR_LOFT2,' J/g)'],'NASA SBM - SubCat 1 kg','NASA SBM - Cat','NASA SBM - Cat LOFT2 modified');
end
lgd.FontName='Helvetica';
lgd.FontSize=fontsizexy;
set(gca,'XLim',xx_lim)
set(gca,'YLim',yy_lim)

out_fig = ['LOFT_2newmodel',adj_title];
saveas(gcf,out_fig,'fig')
saveas(gcf,out_fig,'png')

%% Plot Lc LOFT 3 modified CST
NASA_SBM_LOFT3_cat_mod= 0.1*(mass_imp_3+m_tot_LOFT3)^0.75*Lc_NASA.^(-1.71); 
figure('units','normalized','outerposition',[0 0 1 1],'Visible', 'on')
loglog(s_LOFT3(1,:),[N_LOFT3(1,1)+n_frags_bubbles_LOFT3(1),N_LOFT3(1,2:end)],'MarkerFaceColor',colorOrder(17,:),'Marker','d','LineStyle','none')
hold on
grid on
if nnz(s_LOFT3(2,:))>1
loglog(s_LOFT3(2,1:nnz(s_LOFT3(2,:))),[N_LOFT3(2,1)+n_frags_bubbles_LOFT3(2),N_LOFT3(2,2:1:nnz(s_LOFT3(2,:)))],'MarkerFaceColor',colorOrder(4,:),'Marker','^','LineStyle','none')
hold on
end
if nnz(s_LOFT3(3,:))>1
loglog(s_LOFT3(3,:),[N_LOFT3(3,1)+n_frags_bubbles_LOFT3(3),N_LOFT3(3,2:end)],'MarkerFaceColor',colorOrder(8,:),'Marker','*','LineStyle','none')
hold on
end
loglog(Lc_NASA,NASA_SBM_10_sub,'-.r','LineWidth',2)
loglog(Lc_NASA,NASA_SBM_10_cat,'-k','LineWidth',2)
loglog(Lc_NASA,NASA_SBM_LOFT3_cat_mod,'-b','LineWidth',2)
set(gca,'FontName','Helvetica','FontSize',fontsizexy,'FontWeight','bold')
title('CST results LOFT 3 modified','FontName','Helvetica','FontSize',fontsizetitle,'FontWeight','bold')
xlabel('L_c [m]','FontName','Helvetica','FontSize',fontsizexy)
ylabel('Cumulative number CN','FontName','Helvetica','FontSize',fontsizexy)
if nnz(s_LOFT3(2,:))>1 && nnz(s_LOFT3(3,:))>1
    lgd=legend(['CST-LOFT3 (EMR = ',EMR_LOFT3,' J/g)'],'CST-LOFT3 target','CST-LOFT3 impactor','NASA SBM - SubCat 10 kg','NASA SBM - Cat','NASA SBM - Cat LOFT3 modified');
elseif nnz(s_LOFT3(2,:))>1 && nnz(s_LOFT3(3,:))<2    
    lgd=legend(['CST-LOFT3 (EMR = ',EMR_LOFT3,' J/g)'],'CST-LOFT3 target','NASA SBM - SubCat 10 kg','NASA SBM - Cat','NASA SBM - Cat LOFT3 modified');
elseif nnz(s_LOFT3(2,:))<2 && nnz(s_LOFT3(3,:))>1
    lgd=legend(['CST-LOFT3 (EMR = ',EMR_LOFT3,' J/g)'],'CST-LOFT3 impactor','NASA SBM - SubCat 10 kg','NASA SBM - Cat','NASA SBM - Cat LOFT3 modified');
else
    lgd=legend(['CST-LOFT3 (EMR = ',EMR_LOFT3,' J/g)'],'NASA SBM - SubCat 10 kg','NASA SBM - Cat','NASA SBM - Cat LOFT3 modified');
end
lgd.FontName='Helvetica';
lgd.FontSize=fontsizexy;
set(gca,'XLim',xx_lim)
set(gca,'YLim',yy_lim)

out_fig = ['LOFT_3newmodel',adj_title];
saveas(gcf,out_fig,'fig')
saveas(gcf,out_fig,'png')

%% Plot Lc LOFT 4 modified CST
NASA_SBM_LOFT4_cat_mod= 0.1*(mass_imp_4+m_tot_LOFT4)^0.75*Lc_NASA.^(-1.71); 
figure('units','normalized','outerposition',[0 0 1 1],'Visible', 'on')
loglog(s_LOFT4(1,:),[N_LOFT4(1,1)+n_frags_bubbles_LOFT4(1),N_LOFT4(1,2:end)],'MarkerFaceColor',colorOrder(17,:),'Marker','d','LineStyle','none')
hold on
grid on
if nnz(s_LOFT4(2,:))>1
loglog(s_LOFT4(2,1:nnz(s_LOFT4(2,:))),[N_LOFT4(2,1)+n_frags_bubbles_LOFT4(2),N_LOFT4(2,2:1:nnz(s_LOFT4(2,:)))],'MarkerFaceColor',colorOrder(4,:),'Marker','^','LineStyle','none')
hold on
end
if nnz(s_LOFT4(3,:))>1
loglog(s_LOFT4(3,:),[N_LOFT4(3,1)+n_frags_bubbles_LOFT4(3),N_LOFT4(3,2:end)],'MarkerFaceColor',colorOrder(8,:),'Marker','*','LineStyle','none')
hold on
end
loglog(Lc_NASA,NASA_SBM_10_sub,'-.r','LineWidth',2)
loglog(Lc_NASA,NASA_SBM_10_cat,'-k','LineWidth',2)
loglog(Lc_NASA,NASA_SBM_LOFT4_cat_mod,'-b','LineWidth',2)
set(gca,'FontName','Helvetica','FontSize',fontsizexy,'FontWeight','bold')
title('CST results LOFT 4 modified','FontName','Helvetica','FontSize',fontsizetitle,'FontWeight','bold')
xlabel('L_c [m]','FontName','Helvetica','FontSize',fontsizexy)
ylabel('Cumulative number CN','FontName','Helvetica','FontSize',fontsizexy)
if nnz(s_LOFT4(2,:))>1 && nnz(s_LOFT4(3,:))>1
    lgd=legend(['CST-LOFT4 (EMR = ',EMR_LOFT4,' J/g)'],'CST-LOFT4 target','CST-LOFT4 impactor','NASA SBM - SubCat 10 kg','NASA SBM - Cat','NASA SBM - Cat LOFT4 modified');
elseif nnz(s_LOFT4(2,:))>1 && nnz(s_LOFT4(3,:))<2    
    lgd=legend(['CST-LOFT4 (EMR = ',EMR_LOFT4,' J/g)'],'CST-LOFT4 target','NASA SBM - SubCat 10 kg','NASA SBM - Cat','NASA SBM - Cat LOFT4 modified');
elseif nnz(s_LOFT4(2,:))<2 && nnz(s_LOFT4(3,:))>1
    lgd=legend(['CST-LOFT4 (EMR = ',EMR_LOFT4,' J/g)'],'CST-LOFT4 impactor','NASA SBM - SubCat 10 kg','NASA SBM - Cat','NASA SBM - Cat LOFT4 modified');
else
    lgd=legend(['CST-LOFT4 (EMR = ',EMR_LOFT4,' J/g)'],'NASA SBM - SubCat 10 kg','NASA SBM - Cat','NASA SBM - Cat LOFT4 modified');
end
lgd.FontName='Helvetica';
lgd.FontSize=fontsizexy;
set(gca,'XLim',xx_lim)
set(gca,'YLim',yy_lim)

out_fig = ['LOFT_4newmodel',adj_title];
saveas(gcf,out_fig,'fig')
saveas(gcf,out_fig,'png')

%% Plot Lc LOFT 5 modified CST
NASA_SBM_LOFT5_cat_mod= 0.1*(mass_imp_5+m_tot_LOFT5)^0.75*Lc_NASA.^(-1.71); 
figure('units','normalized','outerposition',[0 0 1 1],'Visible', 'on')
loglog(s_LOFT5(1,:),[N_LOFT5(1,1)+n_frags_bubbles_LOFT5(1),N_LOFT5(1,2:end)],'MarkerFaceColor',colorOrder(17,:),'Marker','d','LineStyle','none')
hold on
grid on
if nnz(s_LOFT5(2,:))>1
loglog(s_LOFT5(2,1:nnz(s_LOFT5(2,:))),[N_LOFT5(2,1)+n_frags_bubbles_LOFT5(2),N_LOFT5(2,2:1:nnz(s_LOFT5(2,:)))],'MarkerFaceColor',colorOrder(4,:),'Marker','^','LineStyle','none')
hold on
end
if nnz(s_LOFT5(3,:))>1
loglog(s_LOFT5(3,:),[N_LOFT5(3,1)+n_frags_bubbles_LOFT5(3),N_LOFT5(3,2:end)],'MarkerFaceColor',colorOrder(8,:),'Marker','*','LineStyle','none')
hold on
end
loglog(Lc_NASA,NASA_SBM_10_sub,'-.r','LineWidth',2)
loglog(Lc_NASA,NASA_SBM_10_cat,'-k','LineWidth',2)
loglog(Lc_NASA,NASA_SBM_LOFT5_cat_mod,'-b','LineWidth',2)
set(gca,'FontName','Helvetica','FontSize',fontsizexy,'FontWeight','bold')
title('CST results LOFT 5 modified','FontName','Helvetica','FontSize',fontsizetitle,'FontWeight','bold')
xlabel('L_c [m]','FontName','Helvetica','FontSize',fontsizexy)
ylabel('Cumulative number CN','FontName','Helvetica','FontSize',fontsizexy)
if nnz(s_LOFT5(2,:))>1 && nnz(s_LOFT5(3,:))>1
    lgd=legend(['CST-LOFT5 (EMR = ',EMR_LOFT5,' J/g)'],'CST-LOFT5 target','CST-LOFT5 impactor','NASA SBM - SubCat 10 kg','NASA SBM - Cat','NASA SBM - Cat LOFT5 modified');
elseif nnz(s_LOFT5(2,:))>1 && nnz(s_LOFT5(3,:))<2    
    lgd=legend(['CST-LOFT5 (EMR = ',EMR_LOFT5,' J/g)'],'CST-LOFT5 target','NASA SBM - SubCat 10 kg','NASA SBM - Cat','NASA SBM - Cat LOFT5 modified');
elseif nnz(s_LOFT5(2,:))<2 && nnz(s_LOFT5(3,:))>1
    lgd=legend(['CST-LOFT5 (EMR = ',EMR_LOFT5,' J/g)'],'CST-LOFT5 impactor','NASA SBM - SubCat 10 kg','NASA SBM - Cat','NASA SBM - Cat LOFT5 modified');
else
    lgd=legend(['CST-LOFT5 (EMR = ',EMR_LOFT5,' J/g)'],'NASA SBM - SubCat 10 kg','NASA SBM - Cat','NASA SBM - Cat LOFT5 modified');
end
lgd.FontName='Helvetica';
lgd.FontSize=fontsizexy;
set(gca,'XLim',xx_lim)
set(gca,'YLim',yy_lim)

out_fig = ['LOFT_5newmodel',adj_title];
saveas(gcf,out_fig,'fig')
saveas(gcf,out_fig,'png')
%}d

%% Plot Lc LOFT 6 modified CST
NASA_SBM_LOFT6_cat_mod= 0.1*(mass_imp_6+m_tot_LOFT6)^0.75*Lc_NASA.^(-1.71); 
figure('units','normalized','outerposition',[0 0 1 1],'Visible', 'on')
loglog(s_LOFT6(1,:),[N_LOFT6(1,1)+n_frags_bubbles_LOFT6(1),N_LOFT6(1,2:end)],'MarkerFaceColor',colorOrder(17,:),'Marker','d','LineStyle','none')
hold on
grid on
if nnz(s_LOFT6(2,:))>1
loglog(s_LOFT6(2,1:nnz(s_LOFT6(2,:))),[N_LOFT6(2,1)+n_frags_bubbles_LOFT6(2),N_LOFT6(2,2:1:nnz(s_LOFT6(2,:)))],'MarkerFaceColor',colorOrder(4,:),'Marker','^','LineStyle','none')
hold on
end
if nnz(s_LOFT6(3,:))>1
loglog(s_LOFT6(3,:),[N_LOFT6(3,1)+n_frags_bubbles_LOFT6(3),N_LOFT6(3,2:end)],'MarkerFaceColor',colorOrder(8,:),'Marker','*','LineStyle','none')
hold on
end
loglog(Lc_NASA,NASA_SBM_10_sub,'-.r','LineWidth',2)
loglog(Lc_NASA,NASA_SBM_10_cat,'-k','LineWidth',2)
loglog(Lc_NASA,NASA_SBM_LOFT6_cat_mod,'-b','LineWidth',2)
set(gca,'FontName','Helvetica','FontSize',fontsizexy,'FontWeight','bold')
title('CST results LOFT 6 modified','FontName','Helvetica','FontSize',fontsizetitle,'FontWeight','bold')
xlabel('L_c [m]','FontName','Helvetica','FontSize',fontsizexy)
ylabel('Cumulative number CN','FontName','Helvetica','FontSize',fontsizexy)
if nnz(s_LOFT6(2,:))>1 && nnz(s_LOFT6(3,:))>1
    lgd=legend(['CST-LOFT6 (EMR = ',EMR_LOFT6,' J/g)'],'CST-LOFT6 target','CST-LOFT6 impactor','NASA SBM - SubCat 10 kg','NASA SBM - Cat','NASA SBM - Cat LOFT6 modified');
elseif nnz(s_LOFT6(2,:))>1 && nnz(s_LOFT6(3,:))<2    
    lgd=legend(['CST-LOFT6 (EMR = ',EMR_LOFT6,' J/g)'],'CST-LOFT6 target','NASA SBM - SubCat 10 kg','NASA SBM - Cat','NASA SBM - Cat LOFT6 modified');
elseif nnz(s_LOFT6(2,:))<2 && nnz(s_LOFT6(3,:))>1
    lgd=legend(['CST-LOFT6 (EMR = ',EMR_LOFT6,' J/g)'],'CST-LOFT6 impactor','NASA SBM - SubCat 10 kg','NASA SBM - Cat','NASA SBM - Cat LOFT6 modified');
else
    lgd=legend(['CST-LOFT6 (EMR = ',EMR_LOFT6,' J/g)'],'NASA SBM - SubCat 10 kg','NASA SBM - Cat','NASA SBM - Cat LOFT6 modified');
end
lgd.FontName='Helvetica';
lgd.FontSize=fontsizexy;
set(gca,'XLim',xx_lim)
set(gca,'YLim',yy_lim)

out_fig = ['LOFT_6newmodel',adj_title];
saveas(gcf,out_fig,'fig')
saveas(gcf,out_fig,'png')

%% Plot Lc LOFT 7 modified CST
NASA_SBM_LOFT7_cat_mod2 = 0.1*(mass_imp_7+m_tot_LOFT7)^0.75*Lc_NASA.^(-1.71);

figure('units','normalized','outerposition',[0 0 1 1],'Visible', 'on')
loglog(s_LOFT7(1,:),[N_LOFT7(1,1)+n_frags_bubbles_LOFT7(1),N_LOFT7(1,2:end)],'MarkerFaceColor',colorOrder(17,:),'Marker','d','LineStyle','none')
hold on
grid on
if nnz(s_LOFT7(2,:))>1
loglog(s_LOFT7(2,1:nnz(s_LOFT7(2,:))),[N_LOFT7(2,1)+n_frags_bubbles_LOFT7(2),N_LOFT7(2,2:1:nnz(s_LOFT7(2,:)))],'MarkerFaceColor',colorOrder(4,:),'Marker','^','LineStyle','none')
hold on
end
if nnz(s_LOFT7(3,:))>1
loglog(s_LOFT7(3,:),[N_LOFT7(3,1)+n_frags_bubbles_LOFT7(3),N_LOFT7(3,2:end)],'MarkerFaceColor',colorOrder(8,:),'Marker','*','LineStyle','none')
hold on
end
loglog(Lc_NASA,NASA_SBM_40_sub,'-r','LineWidth',2)
loglog(Lc_NASA,NASA_SBM_10_cat,'-k','LineWidth',2)
loglog(Lc_NASA,NASA_SBM_LOFT7_cat_mod2,'-b','LineWidth',2)
set(gca,'FontName','Helvetica','FontSize',fontsizexy,'FontWeight','bold')
title('CST results LOFT 7','FontName','Helvetica','FontSize',fontsizetitle,'FontWeight','bold')
xlabel('L_c [m]','FontName','Helvetica','FontSize',fontsizexy)
ylabel('Cumulative number CN','FontName','Helvetica','FontSize',fontsizexy)
if nnz(s_LOFT7(2,:))>1 && nnz(s_LOFT7(3,:))>1
    lgd=legend(['CST-LOFT7 (EMR = ',EMR_LOFT7,' J/g)'],'CST-LOFT7 target','CST-LOFT7 impactor','NASA SBM - SubCat 40 kg','NASA SBM - Cat','NASA SBM - Cat LOFT7 modified');
elseif nnz(s_LOFT7(2,:))>1 && nnz(s_LOFT7(3,:))<2
    lgd=legend(['CST-LOFT7 (EMR = ',EMR_LOFT7,' J/g)'],'CST-LOFT7 target','NASA SBM - SubCat 40 kg','NASA SBM - Cat','NASA SBM - Cat LOFT7 modified');
elseif nnz(s_LOFT7(2,:))<2 && nnz(s_LOFT7(3,:))>1
    lgd=legend(['CST-LOFT7 (EMR = ',EMR_LOFT7,' J/g)'],'CST-LOFT7 impactor','NASA SBM - SubCat 40 kg','NASA SBM - Cat','NASA SBM - Cat LOFT7 modified');
else
    lgd=legend(['CST-LOFT7 (EMR = ',EMR_LOFT7,' J/g)'],'NASA SBM - SubCat 40 kg','NASA SBM - Cat','NASA SBM - Cat LOFT7 modified');
end
lgd.FontName='Helvetica';
lgd.FontSize=fontsizexy;
set(gca,'XLim',xx_lim)
set(gca,'YLim',yy_lim)

out_fig = ['LOFT_7newmodel',adj_title];
saveas(gcf,out_fig,'fig')
saveas(gcf,out_fig,'png')
%}
%% Plot Lc LOFT 8 modified CST
NASA_SBM_LOFT8_cat_mod = 0.1*(mass_imp_8+m_tot_LOFT8)^0.75*Lc_NASA.^(-1.71);

figure('units','normalized','outerposition',[0 0 1 1],'Visible', 'on')
loglog(s_LOFT8(1,:),[N_LOFT8(1,1)+n_frags_bubbles_LOFT8(1),N_LOFT8(1,2:end)],'MarkerFaceColor',colorOrder(17,:),'Marker','d','LineStyle','none')
hold on
grid on
if nnz(s_LOFT8(2,:))>1
loglog(s_LOFT8(2,1:nnz(s_LOFT8(2,:))),[N_LOFT8(2,1)+n_frags_bubbles_LOFT8(2),N_LOFT8(2,2:1:nnz(s_LOFT8(2,:)))],'MarkerFaceColor',colorOrder(4,:),'Marker','^','LineStyle','none')
hold on
end
if nnz(s_LOFT8(3,:))>1
loglog(s_LOFT8(3,:),[N_LOFT8(3,1)+n_frags_bubbles_LOFT8(3),N_LOFT8(3,2:end)],'MarkerFaceColor',colorOrder(8,:),'Marker','*','LineStyle','none')
hold on
end
loglog(Lc_NASA,NASA_SBM_40_sub,'-r','LineWidth',2)
loglog(Lc_NASA,NASA_SBM_40_cat,'-k','LineWidth',2)
loglog(Lc_NASA,NASA_SBM_LOFT8_cat_mod,'-b','LineWidth',2)
set(gca,'FontName','Helvetica','FontSize',fontsizexy,'FontWeight','bold')
title('CST results LOFT 8','FontName','Helvetica','FontSize',fontsizetitle,'FontWeight','bold')
xlabel('L_c [m]','FontName','Helvetica','FontSize',fontsizexy)
ylabel('Cumulative number CN','FontName','Helvetica','FontSize',fontsizexy)
if nnz(s_LOFT8(2,:))>1 && nnz(s_LOFT8(3,:))>1
    lgd=legend(['CST-LOFT8 (EMR = ',EMR_LOFT8,' J/g)'],'CST-LOFT8 target','CST-LOFT8 impactor','NASA SBM - SubCat 40 kg','NASA SBM - Cat','NASA SBM - Cat LOFT8 modified');
elseif nnz(s_LOFT8(2,:))>1 && nnz(s_LOFT8(3,:))<2
    lgd=legend(['CST-LOFT8 (EMR = ',EMR_LOFT8,' J/g)'],'CST-LOFT8 target','NASA SBM - SubCat 40 kg','NASA SBM - Cat','NASA SBM - Cat LOFT8 modified');
elseif nnz(s_LOFT8(2,:))<2 && nnz(s_LOFT8(3,:))>1
    lgd=legend(['CST-LOFT8 (EMR = ',EMR_LOFT8,' J/g)'],'CST-LOFT8 impactor','NASA SBM - SubCat 40 kg','NASA SBM - Cat','NASA SBM - Cat LOFT8 modified');
else
    lgd=legend(['CST-LOFT8 (EMR = ',EMR_LOFT8,' J/g)'],'NASA SBM - SubCat 40 kg','NASA SBM - Cat','NASA SBM - Cat LOFT8 modified');   
end
lgd.FontName='Helvetica';
lgd.FontSize=fontsizexy;
set(gca,'XLim',xx_lim)
set(gca,'YLim',yy_lim)

out_fig = ['LOFT_8newmodel',adj_title];
saveas(gcf,out_fig,'fig')
saveas(gcf,out_fig,'png')
%{
%{
%% plot finale LOFT1 A su M

xx_lim_Am = [-2,0.3];
yy_lim_Am = [0,0.12];
clear DAmSOC DAmSOC_matlab DAmSOC_NASA
lambda_c_min=log10(min([s_LOFT1 s_LOFT1_EMI_config s_LOFT1_all_continuum s_LOFT1_all_continuum s_LOFT1_no_links]));
lambda_c_max=log10(max([s_LOFT1 s_LOFT1_EMI_config s_LOFT1_all_continuum s_LOFT1_all_continuum s_LOFT1_no_links]));
lambda_c=lambda_c_min:(lambda_c_max-lambda_c_min)/15:lambda_c_max;
Am_LOFT1 =[Am_CST_LOFT1 Am_CST_LOFT1_EMI_config Am_CST_LOFT1_all_continuum Am_CST_LOFT1_all_continuum Am_CST_LOFT1_no_links];
csi_min=log10(min(Am_LOFT1(Am_LOFT1>0)));
csi_max=log10(max(Am_LOFT1));
n=length(lambda_c);
csi_NASA=csi_min:(csi_max-csi_min)/(n-1):csi_max;

for i=1:1:n
        if  lambda_c(i)<=-3.5
            muSOC=-0.3;
            sigmaSOC=0.2;
            DAmSOC(i,:)=(1/(sigmaSOC*(2*pi)^0.5))*exp(-((csi_NASA-muSOC).^2)/(2*sigmaSOC^2));
            pd = makedist('Normal','mu',muSOC,'sigma',sigmaSOC);
            DAmSOC_matlab(i,:) = pdf(pd,csi_NASA);
        elseif lambda_c(i)>-3.5 && lambda_c(i)<=-1.75
            muSOC=-0.3;
            sigmaSOC=0.2+0.1333*(lambda_c(i)+3.5);
            DAmSOC(i,:)=(1/(sigmaSOC*(2*pi)^0.5))*exp(-((csi_NASA-muSOC).^2)/(2*sigmaSOC^2));
            pd = makedist('Normal','mu',muSOC,'sigma',sigmaSOC);
            DAmSOC_matlab(i,:) = pdf(pd,csi_NASA);
        elseif lambda_c(i)>-1.75 && lambda_c(i)<-1.25
            muSOC=-0.3-1.4*(lambda_c(i)+1.75);
            sigmaSOC=0.2+0.1333*(lambda_c(i)+3.5);
            DAmSOC(i,:)=(1/(sigmaSOC*(2*pi)^0.5))*exp(-((csi_NASA-muSOC).^2)/(2*sigmaSOC^2));
            pd = makedist('Normal','mu',muSOC,'sigma',sigmaSOC);
            DAmSOC_matlab(i,:) = pdf(pd,csi_NASA);
        elseif lambda_c(i)>=-1.25
            muSOC=-1.0;
            sigmaSOC=0.2+0.1333*(lambda_c(i)+3.5);
            DAmSOC(i,:)=(1/(sigmaSOC*(2*pi)^0.5))*exp(-((csi_NASA-muSOC).^2)/(2*sigmaSOC^2));
            pd = makedist('Normal','mu',muSOC,'sigma',sigmaSOC);
            DAmSOC_matlab(i,:) = pdf(pd,csi_NASA);
        end
end

for k=1:1:size(DAmSOC,2)
    DAmSOC_NASA(k)=sum(DAmSOC(:,k));
    DAmSOC_matlab_NASA(k) = sum(DAmSOC_matlab(:,k));
end

figure('units','normalized','outerposition',[0 0 1 1],'Visible', 'on')
histogram(log10(Am_CST_LOFT1),'Normalization','probability','BinWidth',0.035)
grid on
hold on
histogram(log10(Am_CST_LOFT1_EMI_config),'Normalization','probability','BinWidth',0.035)
hold on
histogram(log10(Am_CST_LOFT1_all_continuum),'Normalization','probability','BinWidth',0.035)
hold on
histogram(log10(Am_CST_LOFT1_no_links),'Normalization','probability','BinWidth',0.035)
hold on
plot(csi_NASA,DAmSOC_NASA./sum(DAmSOC_NASA),'-b','LineWidth',2)
hold on
title('Comparison among PDFs')
set(gca,'FontSize',20,'FontWeight','bold')
xlabel('log_{10}(A/m) [m^{2}/kg]')
ylabel('Relative number N(A/m)')
lgd=legend('CST-LOFT1','CST-LOFT1 in EMI configuration','CST-LOFT1 all CONTINUUM links','CST-LOFT1 no links','NASA - SBM','Location','northwest')
set(gca,'XLim',xx_lim_Am)
set(gca,'YLim',yy_lim_Am)

out_fig = 'A_su_m_CST_LOFT1_data';
saveas(gcf,out_fig,'fig')
saveas(gcf,out_fig,'png')

%% plot finale LOFT6 A su M
%{d
clear DAmSOC DAmSOC_matlab DAmSOC_NASA csi_min csi_max csi_NASA n
lambda_c_min=log10(min([s_LOFT6(1,:) s_LOFT6_all_continuum s_LOFT6_no_links]));
lambda_c_max=log10(max([s_LOFT6(1,:) s_LOFT6_all_continuum s_LOFT6_no_links]));
lambda_c=lambda_c_min:(lambda_c_max-lambda_c_min)/30:lambda_c_max;
Am_LOFT6 =[Am_CST_LOFT6 Am_CST_LOFT6_all_continuum Am_CST_LOFT6_no_links];
csi_min=log10(min(Am_LOFT6(Am_LOFT6>0)));
csi_max=log10(max(Am_LOFT6));
n=length(lambda_c);
csi_NASA=csi_min:(csi_max-csi_min)/(n-1):csi_max;

for i=1:1:n
        if  lambda_c(i)<=-3.5
            muSOC=-0.3;
            sigmaSOC=0.2;
            DAmSOC(i,:)=(1/(sigmaSOC*(2*pi)^0.5))*exp(-((csi_NASA-muSOC).^2)/(2*sigmaSOC^2));
            pd = makedist('Normal','mu',muSOC,'sigma',sigmaSOC);
            DAmSOC_matlab(i,:) = pdf(pd,csi_NASA);
        elseif lambda_c(i)>-3.5 && lambda_c(i)<=-1.75
            muSOC=-0.3;
            sigmaSOC=0.2+0.1333*(lambda_c(i)+3.5);
            DAmSOC(i,:)=(1/(sigmaSOC*(2*pi)^0.5))*exp(-((csi_NASA-muSOC).^2)/(2*sigmaSOC^2));
            pd = makedist('Normal','mu',muSOC,'sigma',sigmaSOC);
            DAmSOC_matlab(i,:) = pdf(pd,csi_NASA);
        elseif lambda_c(i)>-1.75 && lambda_c(i)<-1.25
            muSOC=-0.3-1.4*(lambda_c(i)+1.75);
            sigmaSOC=0.2+0.1333*(lambda_c(i)+3.5);
            DAmSOC(i,:)=(1/(sigmaSOC*(2*pi)^0.5))*exp(-((csi_NASA-muSOC).^2)/(2*sigmaSOC^2));
            pd = makedist('Normal','mu',muSOC,'sigma',sigmaSOC);
            DAmSOC_matlab(i,:) = pdf(pd,csi_NASA);
        elseif lambda_c(i)>=-1.25
            muSOC=-1.0;
            sigmaSOC=0.2+0.1333*(lambda_c(i)+3.5);
            DAmSOC(i,:)=(1/(sigmaSOC*(2*pi)^0.5))*exp(-((csi_NASA-muSOC).^2)/(2*sigmaSOC^2));
            pd = makedist('Normal','mu',muSOC,'sigma',sigmaSOC);
            DAmSOC_matlab(i,:) = pdf(pd,csi_NASA);
        end
end

for k=1:1:size(DAmSOC,2)
    DAmSOC_NASA(k)=sum(DAmSOC(:,k));
    DAmSOC_matlab_NASA(k) = sum(DAmSOC_matlab(:,k));
end


%% Plot LOFT cases CST A su M

clear DAmSOC DAmSOC_matlab DAmSOC_NASA csi_min csi_max csi_NASA n
lambda_c_min=log10(min([s_LOFT1 s_LOFT2 s_LOFT3(1,:) s_LOFT4(1,:) s_LOFT5(1,:) s_LOFT6(1,:)]));
lambda_c_max=log10(max([s_LOFT1 s_LOFT2 s_LOFT3(1,:) s_LOFT4(1,:) s_LOFT5(1,:) s_LOFT6(1,:)]));
lambda_c=lambda_c_min:(lambda_c_max-lambda_c_min)/35:lambda_c_max;
Am_LOFT =[Am_CST_LOFT1 Am_CST_LOFT2 Am_CST_LOFT3(1,:) Am_CST_LOFT4(1,:) Am_CST_LOFT5(1,:) Am_CST_LOFT6(1,:)];
csi_min=log10(min(Am_LOFT(Am_LOFT>0)));
csi_max=log10(max(Am_LOFT));
n=length(lambda_c);
csi_NASA=csi_min:(csi_max-csi_min)/(n-1):csi_max;

for i=1:1:n
        if  lambda_c(i)<=-3.5
            muSOC=-0.3;
            sigmaSOC=0.2;
            DAmSOC(i,:)=(1/(sigmaSOC*(2*pi)^0.5))*exp(-((csi_NASA-muSOC).^2)/(2*sigmaSOC^2));
            pd = makedist('Normal','mu',muSOC,'sigma',sigmaSOC);
            DAmSOC_matlab(i,:) = pdf(pd,csi_NASA);
        elseif lambda_c(i)>-3.5 && lambda_c(i)<=-1.75
            muSOC=-0.3;
            sigmaSOC=0.2+0.1333*(lambda_c(i)+3.5);
            DAmSOC(i,:)=(1/(sigmaSOC*(2*pi)^0.5))*exp(-((csi_NASA-muSOC).^2)/(2*sigmaSOC^2));
            pd = makedist('Normal','mu',muSOC,'sigma',sigmaSOC);
            DAmSOC_matlab(i,:) = pdf(pd,csi_NASA);
        elseif lambda_c(i)>-1.75 && lambda_c(i)<-1.25
            muSOC=-0.3-1.4*(lambda_c(i)+1.75);
            sigmaSOC=0.2+0.1333*(lambda_c(i)+3.5);
            DAmSOC(i,:)=(1/(sigmaSOC*(2*pi)^0.5))*exp(-((csi_NASA-muSOC).^2)/(2*sigmaSOC^2));
            pd = makedist('Normal','mu',muSOC,'sigma',sigmaSOC);
            DAmSOC_matlab(i,:) = pdf(pd,csi_NASA);
        elseif lambda_c(i)>=-1.25
            muSOC=-1.0;
            sigmaSOC=0.2+0.1333*(lambda_c(i)+3.5);
            DAmSOC(i,:)=(1/(sigmaSOC*(2*pi)^0.5))*exp(-((csi_NASA-muSOC).^2)/(2*sigmaSOC^2));
            pd = makedist('Normal','mu',muSOC,'sigma',sigmaSOC);
            DAmSOC_matlab(i,:) = pdf(pd,csi_NASA);
        end
end

for k=1:1:size(DAmSOC,2)
    DAmSOC_NASA(k)=sum(DAmSOC(:,k));
    DAmSOC_matlab_NASA(k) = sum(DAmSOC_matlab(:,k));
end

figure('units','normalized','outerposition',[0 0 1 1],'Visible', 'on')
histogram(log10(Am_CST_LOFT1),'Normalization','probability','BinWidth',0.035)
grid on
hold on
histogram(log10(Am_CST_LOFT2),'Normalization','probability','BinWidth',0.035)
hold on
histogram(log10(Am_CST_LOFT3(1,:)),'Normalization','probability','BinWidth',0.035)
hold on
histogram(log10(Am_CST_LOFT4(1,:)),'Normalization','probability','BinWidth',0.035)
hold on
histogram(log10(Am_CST_LOFT5(1,:)),'Normalization','probability','BinWidth',0.035)
hold on
histogram(log10(Am_CST_LOFT6(1,:)),'Normalization','probability','BinWidth',0.035)
hold on
plot(csi_NASA,DAmSOC_NASA./sum(DAmSOC_NASA),'-b','LineWidth',2)
hold on
title('Comparison among PDFs')
set(gca,'FontSize',20,'FontWeight','bold')
xlabel('log_{10}(A/m) [m^{2}/kg]')
ylabel('Relative number N(A/m)')
lgd=legend('CST-LOFT1','CST-LOFT2','CST-LOFT3','CST-LOFT4','CST-LOFT5','CST-LOFT6','NASA - SBM');
set(gca,'XLim',xx_lim_Am)
set(gca,'YLim',yy_lim_Am)

out_fig = 'A_su_m_CST_baseline_data';
saveas(gcf,out_fig,'fig')
saveas(gcf,out_fig,'png')

%% Plot LOFT2 CST A su M
figure('units','normalized','outerposition',[0 0 1 1],'Visible', 'on')
histogram(log10(Am_CST_LOFT2),'Normalization','probability','BinWidth',0.035)
grid on
hold on
plot(csi_NASA,DAmSOC_NASA./sum(DAmSOC_NASA),'-b','LineWidth',2)
hold on
title('Comparison among PDFs')
set(gca,'FontSize',20,'FontWeight','bold')
xlabel('log_{10}(A/m) [m^{2}/kg]')
ylabel('Relative number N(A/m)')
lgd=legend('CST-LOFT2','NASA - SBM');
set(gca,'XLim',xx_lim_Am)
set(gca,'YLim',yy_lim_Am)

out_fig = 'A_su_m_CST_LOFT2_data';
saveas(gcf,out_fig,'fig')
saveas(gcf,out_fig,'png')

%% Plot LOFT3 CST A su M
figure('units','normalized','outerposition',[0 0 1 1],'Visible', 'on')
histogram(log10(Am_CST_LOFT3(1,:)),'Normalization','probability','BinWidth',0.035)
grid on
hold on
plot(csi_NASA,DAmSOC_NASA./sum(DAmSOC_NASA),'-b','LineWidth',2)
hold on
title('Comparison among PDFs')
set(gca,'FontSize',20,'FontWeight','bold')
xlabel('log_{10}(A/m) [m^{2}/kg]')
ylabel('Relative number N(A/m)')
lgd=legend('CST-LOFT3','NASA - SBM');
set(gca,'XLim',xx_lim_Am)
set(gca,'YLim',yy_lim_Am)

out_fig = 'A_su_m_CST_LOFT3_data';
saveas(gcf,out_fig,'fig')
saveas(gcf,out_fig,'png')

%% Plot LOFT4 CST A su M
figure('units','normalized','outerposition',[0 0 1 1],'Visible', 'on')
histogram(log10(Am_CST_LOFT4(1,:)),'Normalization','probability','BinWidth',0.035)
grid on
hold on
plot(csi_NASA,DAmSOC_NASA./sum(DAmSOC_NASA),'-b','LineWidth',2)
hold on
title('Comparison among PDFs')
set(gca,'FontSize',20,'FontWeight','bold')
xlabel('log_{10}(A/m) [m^{2}/kg]')
ylabel('Relative number N(A/m)')
lgd=legend('CST-LOFT4','NASA - SBM');
set(gca,'XLim',xx_lim_Am)
set(gca,'YLim',yy_lim_Am)

out_fig = 'A_su_m_CST_LOFT4_data';
saveas(gcf,out_fig,'fig')
saveas(gcf,out_fig,'png')

%% Plot LOFT5 CST A su M
figure('units','normalized','outerposition',[0 0 1 1],'Visible', 'on')
histogram(log10(Am_CST_LOFT5(1,:)),'Normalization','probability','BinWidth',0.035)
grid on
hold on
plot(csi_NASA,DAmSOC_NASA./sum(DAmSOC_NASA),'-b','LineWidth',2)
hold on
title('Comparison among PDFs')
set(gca,'FontSize',20,'FontWeight','bold')
xlabel('log_{10}(A/m) [m^{2}/kg]')
ylabel('Relative number N(A/m)')
lgd=legend('CST-LOFT5','NASA - SBM');
set(gca,'XLim',xx_lim_Am)
set(gca,'YLim',yy_lim_Am)

out_fig = 'A_su_m_CST_LOFT1_data';
saveas(gcf,out_fig,'fig')
saveas(gcf,out_fig,'png')


%% plot finale of Delta_v
%{
clear DdvCOLL_NASA DdvCOLL_NASA_1

muEXP=0.9*csi_NASA+2.9;
sigmaEXP=0.4;
delta_v_lim=[0 3];
delta_v_NASA=delta_v_lim(1):abs(delta_v_lim(1)-delta_v_lim(2))/(n-1):delta_v_lim(2);

for i=1:1:n
    DdvCOLL_NASA(:,i)=(1/(sigmaEXP*(2*pi)^0.5))*exp(-((delta_v_NASA-muEXP(i)).^2)/(2*sigmaEXP^2));
end

for k=1:1:size(DdvCOLL_NASA,1)
    DdvCOLL_NASA_1(k)=sum(DdvCOLL_NASA(:,k));
end

figure
histogram(delta_v_CST_baseline,'Normalization','probability','BinWidth',1)
grid on
hold on
histogram(delta_v_CST_continuum,'Normalization','probability','BinWidth',1)
hold on
histogram(delta_v_CST_bolted,'Normalization','probability','BinWidth',1)
hold on
histogram(delta_v_CST_no_links,'Normalization','probability','BinWidth',1)
hold on
% plot(delta_v_NASA,DdvCOLL_NASA_1./sum(DdvCOLL_NASA_1),'b','LineWidth',2)
title('Comparison among PDFs')
xlabel('\DeltaV [m/s] ')
ylabel('Relative Number of Fragments per \DeltaV bin')
lgd=legend('CST baseline','CST (all continuum links)','CST (all bolted links)','CST (no links)','NASA SBM')
xlim([0 60000])
%}
%}
%}
%% plot cpu time and simulation time
if exist('Data_Simulation_LOFT.txt','file')
    delete('Data_Simulation_LOFT.txt')
end
diary Data_Simulation_LOFT_mod.txt
diary on
disp('---------------------------------------------------------')
disp('SIMULATION    |  m_TARGET* (kg)  |    m_TARGET_proj* (kg)')
disp(['LOFT1         |  ',sprintf('%14.2f',m_tot_LOFT1),'  |  ',sprintf('%21.2f', m_star1)])
disp(['LOFT2         |  ',sprintf('%14.2f',m_tot_LOFT2),'  |  ',sprintf('%21.2f', m_star2)])
disp(['LOFT3         |  ',sprintf('%14.2f',m_tot_LOFT3),'  |  ',sprintf('%21.2f', m_star3)])
disp(['LOFT4         |  ',sprintf('%14.2f',m_tot_LOFT4),'  |  ',sprintf('%21.2f', m_star4)])
disp(['LOFT5         |  ',sprintf('%14.2f',m_tot_LOFT5),'  |  ',sprintf('%21.2f', m_star5)])
disp(['LOFT6         |  ',sprintf('%14.2f',m_tot_LOFT6),'  |  ',sprintf('%21.2f', m_star6)])
disp(['LOFT7         |  ',sprintf('%14.2f',m_tot_LOFT7),'  |  ',sprintf('%21.2f', m_star7)])
disp(['LOFT8         |  ',sprintf('%14.2f',m_tot_LOFT8),'  |  ',sprintf('%21.2f', m_star8)])
disp('---------------------------------------------------------')
diary off
