colorOrder =  [ ...
    0            0            1       ;...% 1 BLUE   
    1            0            0       ;...% 2 RED
    0            1            0       ;...% 3 GREEN (pale)
    0            1            1       ;...% 4 CYAN
    1            0            1       ;...% 5 MAGENTA (pale)
    0.6          0.5          0.4     ;  % 6 BROWN (dark)
    0            0            0       ;...% 7 BLACK
    1            1            0       ;...% 8 YELLOW (pale)
    0            0.75         0.75    ;...% 9 TURQUOISE
    0            0.5          0       ;...% 10 GREEN (dark)
    0.75         0.75         0       ;...% 11 YELLOW (dark)
    1            0.50         0.25    ;...% 12 ORANGE
    0.75         0            0.75    ;...% 13 MAGENTA (dark)
    0.7          0.7          0.7     ;...% 14 GREY
    0.8          0.7          0.6     ;...% 15 BROWN (pale)
    0.4          0.7          0.6     ;...
    0.4          0          0.6     ;...
    0.4          0.6          0     ;...
    0.6          0.5          0.4 ];  % 15 BROWN (dark)

LOFT1=load('22_PDFsam_2018CSID_MSchimmerohn_SCDisrupt_crop_LOFT1_LOG.txt');
LOFT2=load('22_PDFsam_2018CSID_MSchimmerohn_SCDisrupt_crop_LOFT2_LOG.txt');
LOFT3=load('22_PDFsam_2018CSID_MSchimmerohn_SCDisrupt_crop_LOFT3_LOG.txt');
LOFT4=load('22_PDFsam_2018CSID_MSchimmerohn_SCDisrupt_crop_LOFT4_LOG.txt');
LOFT5=load('22_PDFsam_2018CSID_MSchimmerohn_SCDisrupt_crop_LOFT5_LOG.txt');
LOFT6=load('22_PDFsam_2018CSID_MSchimmerohn_SCDisrupt_crop_LOFT6_LOG.txt');

figure
loglog(LOFT1(:,1),LOFT1(:,2),'MarkerFaceColor',colorOrder(9,:),'Marker','.','LineStyle','none');
hold on
loglog(LOFT2(:,1),LOFT2(:,2),'MarkerFaceColor',colorOrder(12,:),'Marker','.','LineStyle','none');
hold on
loglog(LOFT3(:,1),LOFT3(:,2),'MarkerFaceColor',colorOrder(11,:),'Marker','.','LineStyle','none');
hold on
loglog(LOFT4(:,1),LOFT4(:,2),'MarkerFaceColor',colorOrder(5,:),'Marker','.','LineStyle','none');
hold on
loglog(LOFT5(:,1),LOFT5(:,2),'MarkerFaceColor',colorOrder(10,:),'Marker','.','LineStyle','none');
hold on
loglog(LOFT6(:,1),LOFT6(:,2),'MarkerFaceColor',colorOrder(4,:),'Marker','.','LineStyle','none');

legend(['EMI-LOFT1';'EMI-LOFT2';'EMI-LOFT3';'EMI-LOFT4';'EMI-LOFT5';'EMI-LOFT6']);

xlabel('Fragment Size L_c(m)')
ylabel('Cumulative Fragment Number N>L_c')
set(gca,'XLim',[1e-03,1e0])
set(gca,'YLim',[1e0,1e6])