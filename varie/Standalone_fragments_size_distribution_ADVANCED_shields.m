% Standalone for fragments size distribution
clear all
close all
clc

%% Load Data CST Al
% name_file_result_ADV_1_1a='N:\CST3\Results\Advanced_Shields_1_1a_Al\data\step_2355.mat';
name_file_result_ADV_1_1a='N:\CST3_ADV_SHIELDS_CASE1aAl\Results\Advanced_Shields_CASE1_Case1_a_Al_20181214T134417\data\step_2433.mat';
name_file_result_ADV_1_1a2='N:\CST3\Results\Advanced_Shields_1_1a2_Al\data\step_2000.mat';
% name_file_result_ADV_1_1b='N:\CST3\Results\Advanced_Shields_1_1b_Al\data\step_1551.mat';
name_file_result_ADV_1_1b='N:\CST3_ADV_SHIELDS_CASE1bAl\Results\Advanced_Shields_CASE1_Case1_b_Al_20181214T134505\data\step_2629.mat';
% name_file_result_ADV_1_1c='N:\CST3\Results\Advanced_Shields_1_1c_Al\data\step_2000.mat';
name_file_result_ADV_1_1c='N:\CST3_ADV_SHIELDS_CASE1cAl\Results\Advanced_Shields_CASE1_Case1_c_Al_20181214T134535\data\step_1694.mat';
name_file_result_ADV_1_1d='N:\CST3\Results\Advanced_Shields_1_1d_Al\data\step_2000.mat';
% name_file_result_ADV_1_1d='N:\CST3_ADV_SHIELDS_CASE1dAl\Results\Advanced_Shields_CASE1_Case1_d_Al_20181214T134606\data\step_284.mat';

% name_file_result_ADV_2_1a='N:\CST3\Results\Advanced_Shields_2_1a_Al\data\step_1212.mat';
name_file_result_ADV_2_1a='N:\CST3_ADV_SHIELDS_CASE2aAl\Results\Advanced_Shields_CASE2_Case1_a_Al_20181214T140920\data\step_1212.mat';
% name_file_result_ADV_2_1b='N:\CST3\Results\Advanced_Shields_2_1b_Al\data\step_1318.mat';
name_file_result_ADV_2_1b='N:\CST3_ADV_SHIELDS_CASE2bAl\Results\Advanced_Shields_CASE2_Case1_b_Al_20181214T140936\data\step_1338.mat';
% name_file_result_ADV_2_1c='N:\CST3\Results\Advanced_Shields_2_1c_Al\data\step_1324.mat';
name_file_result_ADV_2_1c='N:\CST3_ADV_SHIELDS_CASE2cAl\Results\Advanced_Shields_CASE2_Case1_c_Al_20181214T140947\data\step_1391.mat';
% name_file_result_ADV_2_1d='N:\CST3\Results\Advanced_Shields_2_1d_Al\data\step_1261.mat';
name_file_result_ADV_2_1d='N:\CST3_ADV_SHIELDS_CASE2dAl\Results\Advanced_Shields_CASE2_Case1_d_Al_20181214T140952\data\step_1257.mat';

%% Load Data CST Nylon 6
% name_file_result_ADV_1_1aN='N:\CST3\Results\Advanced_Shields_1_1a_Nylon_6\data\step_175.mat';
name_file_result_ADV_1_1aN='N:\CST3_ADV_SHIELDS_CASE1aN6\Results\Advanced_Shields_CASE1_Case1_a_Nylon_6_20181214T140627\data\step_1681.mat';
% name_file_result_ADV_1_1bN='N:\CST3\Results\Advanced_Shields_1_1b_Nylon_6\data\step_1409.mat';
name_file_result_ADV_1_1bN='N:\CST3_ADV_SHIELDS_CASE1bN6\Results\Advanced_Shields_CASE1_Case1_b_Nylon_6_20181214T140637\data\step_1538.mat';
% name_file_result_ADV_1_1cN='N:\CST3\Results\Advanced_Shields_1_1c_Nylon_6\data\step_1287.mat';
name_file_result_ADV_1_1cN='N:\CST3_ADV_SHIELDS_CASE1cN6\Results\Advanced_Shields_CASE1_Case1_c_Nylon_6_20181214T140647\data\step_2000.mat';
name_file_result_ADV_1_1dN='N:\CST3\Results\Advanced_Shields_1_1d_Nylon_6\data\step_1944.mat';
% name_file_result_ADV_1_1dN='N:\CST3_ADV_SHIELDS_CASE1dN6\Results\Advanced_Shields_CASE1_Case1_d_Nylon_6_20181214T140700\data\step_1944.mat';

% name_file_result_ADV_2_1aN='N:\CST3\Results\Advanced_Shields_2_1a_Nylon_6\data\step_1201.mat';
name_file_result_ADV_2_1aN='N:\CST3_ADV_SHIELDS_CASE2aN6\Results\Advanced_Shields_CASE2_Case1_a_Nylon_6_20181214T141141\data\step_1201.mat';
% name_file_result_ADV_2_1bN='N:\CST3\Results\Advanced_Shields_2_1b_Nylon_6\data\step_1272.mat';
name_file_result_ADV_2_1bN='N:\CST3_ADV_SHIELDS_CASE2bN6\Results\Advanced_Shields_CASE2_Case1_b_Nylon_6_20181214T141208\data\step_1272.mat';
% name_file_result_ADV_2_1cN='N:\CST3\Results\Advanced_Shields_2_1c_Nylon_6\data\step_1287.mat';
name_file_result_ADV_2_1cN='N:\CST3_ADV_SHIELDS_CASE2cN6\Results\Advanced_Shields_CASE2_Case1_c_Nylon_6_20181214T141234\data\step_1287.mat';
% name_file_result_ADV_2_1dN='N:\CST3\Results\Advanced_Shields_2_1d_Nylon_6\data\step_1314.mat';
name_file_result_ADV_2_1dN='N:\CST3_ADV_SHIELDS_CASE2dN6\Results\Advanced_Shields_CASE2_Case1_d_Nylon_6_20181214T141308\data\step_1314.mat';

%% processing Advanced scields 1a Al scenario data

load(name_file_result_ADV_1_1a,'FRAGMENTS','ME','BUBBLE','PROPAGATION_THRESHOLD','link_data','tf','RunTime')
name_mat=[name_file_result_ADV_1_1a(1:strfind(name_file_result_ADV_1_1a,'Results')-1),filesep,'set_up',filesep,'material_list.m'];
run(name_mat)

mass_imp_1a=0;
mass_shield_1=0;
q_imp_1a = [0 0 0]';

for i = 2:34
    mass_shield_1 = mass_shield_1 + ME(i).GEOMETRY_DATA.mass0;
end

mass_imp_1a = mass_imp_1a + ME(1).GEOMETRY_DATA.mass0;
q_imp_1a = q_imp_1a + ME(1).GEOMETRY_DATA.mass0*ME(1).DYNAMICS_INITIAL_DATA.vel0;
v_p_1a = 1/mass_imp_1a*q_imp_1a/1000;

[n_frags_bubbles_ADV_1_1a,s_ADV_1_1a,N_ADV_1_1a,Am_CST_ADV_1_1a,delta_v_CST_ADV_1_1a]=postprocess_data(FRAGMENTS,ME,BUBBLE,PROPAGATION_THRESHOLD,link_data);

tf_ADV_1_1a = sprintf('%24.4f',tf);
if exist('RunTime','var')
    cputime_ADV_1_1a = sprintf('%13.2f',RunTime/60);
else
    cputime_ADV_1_1a = '                   -';
end

final_step_ADV_1_1a = sprintf('%20d',str2double(name_file_result_ADV_1_1a(strfind(name_file_result_ADV_1_1a,'step_')+5:end-4)));
EMR_ADV_1_1a = sprintf('%5.2f',1/2*mass_imp_1a*norm(v_p_1a*1000)^2/(mass_imp_1a+mass_shield_1)/1000);
clear FRAGMENTS ME BUBBLE PROPAGATION_THRESHOLD link_data tf RunTime

%% processing Advanced scields 1a double thickness Al scenario data

load(name_file_result_ADV_1_1a2,'FRAGMENTS','ME','BUBBLE','PROPAGATION_THRESHOLD','link_data','tf','RunTime')

[n_frags_bubbles_ADV_1_1a2,s_ADV_1_1a2,N_ADV_1_1a2,Am_CST_ADV_1_1a2,delta_v_CST_ADV_1_1a2]=postprocess_data(FRAGMENTS,ME,BUBBLE,PROPAGATION_THRESHOLD,link_data);

tf_ADV_1_1a2 = sprintf('%24.4f',tf);
if exist('RunTime','var')
    cputime_ADV_1_1a2 = sprintf('%13.2f',RunTime/60);
else
    cputime_ADV_1_1a2 = '                   -';
end

final_step_ADV_1_1a2 = sprintf('%20d',str2double(name_file_result_ADV_1_1a2(strfind(name_file_result_ADV_1_1a,'step_')+5:end-4)));
EMR_ADV_1_1a2 = sprintf('%5.2f',1/2*mass_imp_1a*norm(v_p_1a*1000)^2/(mass_imp_1a+mass_shield_1)/1000);
clear FRAGMENTS ME BUBBLE PROPAGATION_THRESHOLD link_data tf RunTime

%% processing Advanced scields 1b Al scenario data

load(name_file_result_ADV_1_1b,'FRAGMENTS','ME','BUBBLE','PROPAGATION_THRESHOLD','link_data','tf','RunTime')

mass_imp_1b=0;
q_imp_1b = [0 0 0]';
mass_imp_1b = mass_imp_1b + ME(1).GEOMETRY_DATA.mass0;
q_imp_1b = q_imp_1b + ME(1).GEOMETRY_DATA.mass0*ME(1).DYNAMICS_INITIAL_DATA.vel0;
v_p_1b = 1/mass_imp_1b*q_imp_1b/1000;

[n_frags_bubbles_ADV_1_1b,s_ADV_1_1b,N_ADV_1_1b,Am_CST_ADV_1_1b,delta_v_CST_ADV_1_1b]=postprocess_data(FRAGMENTS,ME,BUBBLE,PROPAGATION_THRESHOLD,link_data);

tf_ADV_1_1b = sprintf('%24.4f',tf);
if exist('RunTime','var')
    cputime_ADV_1_1b = sprintf('%13.2f',RunTime/60);
else
    cputime_ADV_1_1b = '                   -';
end

final_step_ADV_1_1b = sprintf('%20d',str2double(name_file_result_ADV_1_1b(strfind(name_file_result_ADV_1_1b,'step_')+5:end-4)));
EMR_ADV_1_1b = sprintf('%5.2f',1/2*mass_imp_1b*norm(v_p_1b*1000)^2/(mass_imp_1b+mass_shield_1)/1000);
clear FRAGMENTS ME BUBBLE PROPAGATION_THRESHOLD link_data tf RunTime

%% processing Advanced scields 1c Al scenario data

load(name_file_result_ADV_1_1c,'FRAGMENTS','ME','BUBBLE','PROPAGATION_THRESHOLD','link_data','tf','RunTime')

mass_imp_1c=0;
q_imp_1c = [0 0 0]';
mass_imp_1c = mass_imp_1c + ME(1).GEOMETRY_DATA.mass0;
q_imp_1c = q_imp_1c + ME(1).GEOMETRY_DATA.mass0*ME(1).DYNAMICS_INITIAL_DATA.vel0;
v_p_1c = 1/mass_imp_1c*q_imp_1c/1000;

[n_frags_bubbles_ADV_1_1c,s_ADV_1_1c,N_ADV_1_1c,Am_CST_ADV_1_1c,delta_v_CST_ADV_1_1c]=postprocess_data(FRAGMENTS,ME,BUBBLE,PROPAGATION_THRESHOLD,link_data);

tf_ADV_1_1c = sprintf('%24.4f',tf);
if exist('RunTime','var')
    cputime_ADV_1_1c = sprintf('%13.2f',RunTime/60);
else
    cputime_ADV_1_1c = '                   -';
end

final_step_ADV_1_1c = sprintf('%20d',str2double(name_file_result_ADV_1_1c(strfind(name_file_result_ADV_1_1c,'step_')+5:end-4)));
EMR_ADV_1_1c = sprintf('%5.2f',1/2*mass_imp_1c*norm(v_p_1c*1000)^2/(mass_imp_1c+mass_shield_1)/1000);
clear FRAGMENTS ME BUBBLE PROPAGATION_THRESHOLD link_data tf RunTime

%% processing Advanced scields 1d Al scenario data

load(name_file_result_ADV_1_1d,'FRAGMENTS','ME','BUBBLE','PROPAGATION_THRESHOLD','link_data','tf','RunTime')

mass_imp_1d=0;
q_imp_1d = [0 0 0]';
mass_imp_1d = mass_imp_1d + ME(1).GEOMETRY_DATA.mass0;
q_imp_1d = q_imp_1d + ME(1).GEOMETRY_DATA.mass0*ME(1).DYNAMICS_INITIAL_DATA.vel0;
v_p_1d = 1/mass_imp_1d*q_imp_1d/1000;

[n_frags_bubbles_ADV_1_1d,s_ADV_1_1d,N_ADV_1_1d,Am_CST_ADV_1_1d,delta_v_CST_ADV_1_1d]=postprocess_data(FRAGMENTS,ME,BUBBLE,PROPAGATION_THRESHOLD,link_data);

tf_ADV_1_1d = sprintf('%24.4f',tf);
if exist('RunTime','var')
    cputime_ADV_1_1d = sprintf('%13.2f',RunTime/60);
else
    cputime_ADV_1_1d = '                   -';
end

final_step_ADV_1_1d = sprintf('%20d',str2double(name_file_result_ADV_1_1d(strfind(name_file_result_ADV_1_1d,'step_')+5:end-4)));
EMR_ADV_1_1d = sprintf('%5.2f',1/2*mass_imp_1d*norm(v_p_1d*1000)^2/(mass_imp_1d+mass_shield_1)/1000);
clear FRAGMENTS ME BUBBLE PROPAGATION_THRESHOLD link_data tf RunTime

%% processing Advanced scields 1a Nylon 6 scenario data
%{y
load(name_file_result_ADV_1_1aN,'FRAGMENTS','ME','BUBBLE','PROPAGATION_THRESHOLD','link_data','tf','RunTime')

mass_imp_1aN=0;
q_imp_1aN = [0 0 0]';
mass_imp_1aN = mass_imp_1aN + ME(1).GEOMETRY_DATA.mass0;
q_imp_1aN = q_imp_1aN + ME(1).GEOMETRY_DATA.mass0*ME(1).DYNAMICS_INITIAL_DATA.vel0;
v_p_1aN = 1/mass_imp_1aN*q_imp_1aN/1000;

[n_frags_bubbles_ADV_1_1aN,s_ADV_1_1aN,N_ADV_1_1aN,Am_CST_ADV_1_1aN,delta_v_CST_ADV_1_1aN]=postprocess_data(FRAGMENTS,ME,BUBBLE,PROPAGATION_THRESHOLD,link_data);

tf_ADV_1_1aN = sprintf('%24.4f',tf);
if exist('RunTime','var')
    cputime_ADV_1_1aN = sprintf('%10.2f',RunTime/60);
else
    cputime_ADV_1_1aN = '                   -';
end

final_step_ADV_1_1aN = sprintf('%20d',str2double(name_file_result_ADV_1_1aN(strfind(name_file_result_ADV_1_1aN,'step_')+5:end-4)));
EMR_ADV_1_1aN = sprintf('%5.2f',1/2*mass_imp_1aN*norm(v_p_1aN*1000)^2/(mass_imp_1aN+mass_shield_1)/1000);
clear FRAGMENTS ME BUBBLE PROPAGATION_THRESHOLD link_data tf RunTime

%% processing Advanced scields 1b Nylon 6 scenario data

load(name_file_result_ADV_1_1bN,'FRAGMENTS','ME','BUBBLE','PROPAGATION_THRESHOLD','link_data','tf','RunTime')

mass_imp_1bN=0;
q_imp_1bN = [0 0 0]';
mass_imp_1bN = mass_imp_1bN + ME(1).GEOMETRY_DATA.mass0;
q_imp_1bN = q_imp_1bN + ME(1).GEOMETRY_DATA.mass0*ME(1).DYNAMICS_INITIAL_DATA.vel0;
v_p_1bN = 1/mass_imp_1bN*q_imp_1bN/1000;

[n_frags_bubbles_ADV_1_1bN,s_ADV_1_1bN,N_ADV_1_1bN,Am_CST_ADV_1_1bN,delta_v_CST_ADV_1_1bN]=postprocess_data(FRAGMENTS,ME,BUBBLE,PROPAGATION_THRESHOLD,link_data);

tf_ADV_1_1bN = sprintf('%24.4f',tf);
if exist('RunTime','var')
    cputime_ADV_1_1bN = sprintf('%10.2f',RunTime/60);
else
    cputime_ADV_1_1bN = '                   -';
end

final_step_ADV_1_1bN = sprintf('%20d',str2double(name_file_result_ADV_1_1bN(strfind(name_file_result_ADV_1_1bN,'step_')+5:end-4)));
EMR_ADV_1_1bN = sprintf('%5.2f',1/2*mass_imp_1bN*norm(v_p_1bN*1000)^2/(mass_imp_1bN+mass_shield_1)/1000);
clear FRAGMENTS ME BUBBLE PROPAGATION_THRESHOLD link_data tf RunTime

%% processing Advanced scields 1c Nylon 6 scenario data

load(name_file_result_ADV_1_1cN,'FRAGMENTS','ME','BUBBLE','PROPAGATION_THRESHOLD','link_data','tf','RunTime')

mass_imp_1cN=0;
q_imp_1cN = [0 0 0]';
mass_imp_1cN = mass_imp_1cN + ME(1).GEOMETRY_DATA.mass0;
q_imp_1cN = q_imp_1cN + ME(1).GEOMETRY_DATA.mass0*ME(1).DYNAMICS_INITIAL_DATA.vel0;
v_p_1cN = 1/mass_imp_1cN*q_imp_1cN/1000;

[n_frags_bubbles_ADV_1_1cN,s_ADV_1_1cN,N_ADV_1_1cN,Am_CST_ADV_1_1cN,delta_v_CST_ADV_1_1cN]=postprocess_data(FRAGMENTS,ME,BUBBLE,PROPAGATION_THRESHOLD,link_data);

tf_ADV_1_1cN = sprintf('%24.4f',tf);
if exist('RunTime','var')
    cputime_ADV_1_1cN = sprintf('%10.2f',RunTime/60);
else
    cputime_ADV_1_1cN = '                   -';
end

final_step_ADV_1_1cN = sprintf('%20d',str2double(name_file_result_ADV_1_1cN(strfind(name_file_result_ADV_1_1cN,'step_')+5:end-4)));
EMR_ADV_1_1cN = sprintf('%5.2f',1/2*mass_imp_1cN*norm(v_p_1cN*1000)^2/(mass_imp_1cN+mass_shield_1)/1000);
clear FRAGMENTS ME BUBBLE PROPAGATION_THRESHOLD link_data tf RunTime

%% processing Advanced scields 1d Nylon 6 scenario data

load(name_file_result_ADV_1_1dN,'FRAGMENTS','ME','BUBBLE','PROPAGATION_THRESHOLD','link_data','tf','RunTime')

mass_imp_1dN=0;
q_imp_1dN = [0 0 0]';
mass_imp_1dN = mass_imp_1dN + ME(1).GEOMETRY_DATA.mass0;
q_imp_1dN = q_imp_1dN + ME(1).GEOMETRY_DATA.mass0*ME(1).DYNAMICS_INITIAL_DATA.vel0;
v_p_1dN = 1/mass_imp_1dN*q_imp_1dN/1000;

[n_frags_bubbles_ADV_1_1dN,s_ADV_1_1dN,N_ADV_1_1dN,Am_CST_ADV_1_1dN,delta_v_CST_ADV_1_1dN]=postprocess_data(FRAGMENTS,ME,BUBBLE,PROPAGATION_THRESHOLD,link_data);

tf_ADV_1_1dN = sprintf('%24.4f',tf);
if exist('RunTime','var')
    cputime_ADV_1_1dN = sprintf('%10.2f',RunTime/60);
else
    cputime_ADV_1_1dN = '                   -';
end

final_step_ADV_1_1dN = sprintf('%20d',str2double(name_file_result_ADV_1_1dN(strfind(name_file_result_ADV_1_1dN,'step_')+5:end-4)));
EMR_ADV_1_1dN = sprintf('%5.2f',1/2*mass_imp_1dN*norm(v_p_1dN*1000)^2/(mass_imp_1dN+mass_shield_1)/1000);
clear FRAGMENTS ME BUBBLE PROPAGATION_THRESHOLD link_data tf RunTime

%% processing Advanced scields 2a Al scenario data

load(name_file_result_ADV_2_1a,'FRAGMENTS','ME','BUBBLE','PROPAGATION_THRESHOLD','link_data','tf','RunTime')

mass_shield_2=0;

for i = 2:24
    mass_shield_2 = mass_shield_2 + ME(i).GEOMETRY_DATA.mass0;
end

[n_frags_bubbles_ADV_2_1a,s_ADV_2_1a,N_ADV_2_1a,Am_CST_ADV_2_1a,delta_v_CST_ADV_2_1a]=postprocess_data(FRAGMENTS,ME,BUBBLE,PROPAGATION_THRESHOLD,link_data);

tf_ADV_2_1a = sprintf('%24.4f',tf);
if exist('RunTime','var')
    cputime_ADV_2_1a = sprintf('%13.2f',RunTime/60);
else
    cputime_ADV_2_1a = '                   -';
end

final_step_ADV_2_1a = sprintf('%20d',str2double(name_file_result_ADV_2_1a(strfind(name_file_result_ADV_2_1a,'step_')+5:end-4)));
EMR_ADV_2_1a = sprintf('%5.2f',1/2*mass_imp_1a*norm(v_p_1a*1000)^2/(mass_imp_1a+mass_shield_2)/1000);
clear FRAGMENTS ME BUBBLE PROPAGATION_THRESHOLD link_data tf RunTime

%% processing Advanced scields 2b Al scenario data

load(name_file_result_ADV_2_1b,'FRAGMENTS','ME','BUBBLE','PROPAGATION_THRESHOLD','link_data','tf','RunTime')

[n_frags_bubbles_ADV_2_1b,s_ADV_2_1b,N_ADV_2_1b,Am_CST_ADV_2_1b,delta_v_CST_ADV_2_1b]=postprocess_data(FRAGMENTS,ME,BUBBLE,PROPAGATION_THRESHOLD,link_data);

tf_ADV_2_1b = sprintf('%24.4f',tf);
if exist('RunTime','var')
    cputime_ADV_2_1b = sprintf('%13.2f',RunTime/60);
else
    cputime_ADV_2_1b = '                   -';
end

final_step_ADV_2_1b = sprintf('%20d',str2double(name_file_result_ADV_2_1b(strfind(name_file_result_ADV_2_1b,'step_')+5:end-4)));
EMR_ADV_2_1b = sprintf('%5.2f',1/2*mass_imp_1b*norm(v_p_1b*1000)^2/(mass_imp_1b+mass_shield_2)/1000);
clear FRAGMENTS ME BUBBLE PROPAGATION_THRESHOLD link_data tf RunTime

%% processing Advanced scields 2c Al scenario data

load(name_file_result_ADV_2_1c,'FRAGMENTS','ME','BUBBLE','PROPAGATION_THRESHOLD','link_data','tf','RunTime')

[n_frags_bubbles_ADV_2_1c,s_ADV_2_1c,N_ADV_2_1c,Am_CST_ADV_2_1c,delta_v_CST_ADV_2_1c]=postprocess_data(FRAGMENTS,ME,BUBBLE,PROPAGATION_THRESHOLD,link_data);

tf_ADV_2_1c = sprintf('%24.4f',tf);
if exist('RunTime','var')
    cputime_ADV_2_1c = sprintf('%13.2f',RunTime/60);
else
    cputime_ADV_2_1c = '                   -';
end

final_step_ADV_2_1c = sprintf('%20d',str2double(name_file_result_ADV_2_1c(strfind(name_file_result_ADV_2_1c,'step_')+5:end-4)));
EMR_ADV_2_1c = sprintf('%5.2f',1/2*mass_imp_1c*norm(v_p_1c*1000)^2/(mass_imp_1c+mass_shield_2)/1000);
clear FRAGMENTS ME BUBBLE PROPAGATION_THRESHOLD link_data tf RunTime

%% processing Advanced scields 2d Al scenario data

load(name_file_result_ADV_2_1d,'FRAGMENTS','ME','BUBBLE','PROPAGATION_THRESHOLD','link_data','tf','RunTime')

[n_frags_bubbles_ADV_2_1d,s_ADV_2_1d,N_ADV_2_1d,Am_CST_ADV_2_1d,delta_v_CST_ADV_2_1d]=postprocess_data(FRAGMENTS,ME,BUBBLE,PROPAGATION_THRESHOLD,link_data);

tf_ADV_2_1d = sprintf('%24.4f',tf);
if exist('RunTime','var')
    cputime_ADV_2_1d = sprintf('%13.2f',RunTime/60);
else
    cputime_ADV_2_1d = '                   -';
end

final_step_ADV_2_1d = sprintf('%20d',str2double(name_file_result_ADV_2_1d(strfind(name_file_result_ADV_2_1d,'step_')+5:end-4)));
EMR_ADV_2_1d = sprintf('%5.2f',1/2*mass_imp_1d*norm(v_p_1d*1000)^2/(mass_imp_1d+mass_shield_2)/1000);
clear FRAGMENTS ME BUBBLE PROPAGATION_THRESHOLD link_data tf RunTime

%% processing Advanced scields 2a Nylon 6 scenario data

load(name_file_result_ADV_2_1aN,'FRAGMENTS','ME','BUBBLE','PROPAGATION_THRESHOLD','link_data','tf','RunTime')

[n_frags_bubbles_ADV_2_1aN,s_ADV_2_1aN,N_ADV_2_1aN,Am_CST_ADV_2_1aN,delta_v_CST_ADV_2_1aN]=postprocess_data(FRAGMENTS,ME,BUBBLE,PROPAGATION_THRESHOLD,link_data);

tf_ADV_2_1aN = sprintf('%24.4f',tf);
if exist('RunTime','var')
    cputime_ADV_2_1aN = sprintf('%10.2f',RunTime/60);
else
    cputime_ADV_2_1aN = '                   -';
end

final_step_ADV_2_1aN = sprintf('%20d',str2double(name_file_result_ADV_2_1aN(strfind(name_file_result_ADV_2_1aN,'step_')+5:end-4)));
EMR_ADV_2_1aN = sprintf('%5.2f',1/2*mass_imp_1aN*norm(v_p_1aN*1000)^2/(mass_imp_1aN+mass_shield_2)/1000);
clear FRAGMENTS ME BUBBLE PROPAGATION_THRESHOLD link_data tf RunTime

%% processing Advanced scields 2b Nylon 6 scenario data

load(name_file_result_ADV_2_1bN,'FRAGMENTS','ME','BUBBLE','PROPAGATION_THRESHOLD','link_data','tf','RunTime')

[n_frags_bubbles_ADV_2_1bN,s_ADV_2_1bN,N_ADV_2_1bN,Am_CST_ADV_2_1bN,delta_v_CST_ADV_2_1bN]=postprocess_data(FRAGMENTS,ME,BUBBLE,PROPAGATION_THRESHOLD,link_data);

tf_ADV_2_1bN = sprintf('%24.4f',tf);
if exist('RunTime','var')
    cputime_ADV_2_1bN = sprintf('%10.2f',RunTime/60);
else
    cputime_ADV_2_1bN = '                   -';
end

final_step_ADV_2_1bN = sprintf('%20d',str2double(name_file_result_ADV_2_1bN(strfind(name_file_result_ADV_2_1bN,'step_')+5:end-4)));
EMR_ADV_2_1bN = sprintf('%5.2f',1/2*mass_imp_1bN*norm(v_p_1bN*1000)^2/(mass_imp_1bN+mass_shield_2)/1000);
clear FRAGMENTS ME BUBBLE PROPAGATION_THRESHOLD link_data tf RunTime

%% processing Advanced scields 2c Nylon 6 scenario data

load(name_file_result_ADV_2_1cN,'FRAGMENTS','ME','BUBBLE','PROPAGATION_THRESHOLD','link_data','tf','RunTime')

[n_frags_bubbles_ADV_2_1cN,s_ADV_2_1cN,N_ADV_2_1cN,Am_CST_ADV_2_1cN,delta_v_CST_ADV_2_1cN]=postprocess_data(FRAGMENTS,ME,BUBBLE,PROPAGATION_THRESHOLD,link_data);

tf_ADV_2_1cN = sprintf('%24.4f',tf);
if exist('RunTime','var')
    cputime_ADV_2_1cN = sprintf('%10.2f',RunTime/60);
else
    cputime_ADV_2_1cN = '                   -';
end

final_step_ADV_2_1cN = sprintf('%20d',str2double(name_file_result_ADV_2_1cN(strfind(name_file_result_ADV_2_1cN,'step_')+5:end-4)));
EMR_ADV_2_1cN = sprintf('%5.2f',1/2*mass_imp_1cN*norm(v_p_1cN*1000)^2/(mass_imp_1cN+mass_shield_2)/1000);
clear FRAGMENTS ME BUBBLE PROPAGATION_THRESHOLD link_data tf RunTime

%% processing Advanced scields 2d Nylon 6 scenario data

load(name_file_result_ADV_2_1dN,'FRAGMENTS','ME','BUBBLE','PROPAGATION_THRESHOLD','link_data','tf','RunTime')

[n_frags_bubbles_ADV_2_1dN,s_ADV_2_1dN,N_ADV_2_1dN,Am_CST_ADV_2_1dN,delta_v_CST_ADV_2_1dN]=postprocess_data(FRAGMENTS,ME,BUBBLE,PROPAGATION_THRESHOLD,link_data);

tf_ADV_2_1dN = sprintf('%24.4f',tf);
if exist('RunTime','var')
    cputime_ADV_2_1dN = sprintf('%10.2f',RunTime/60);
else
    cputime_ADV_2_1dN = '                   -';
end

final_step_ADV_2_1dN = sprintf('%20d',str2double(name_file_result_ADV_2_1dN(strfind(name_file_result_ADV_2_1dN,'step_')+5:end-4)));
EMR_ADV_2_1dN = sprintf('%5.2f',1/2*mass_imp_1dN*norm(v_p_1dN*1000)^2/(mass_imp_1dN+mass_shield_2)/1000);
clear FRAGMENTS ME BUBBLE PROPAGATION_THRESHOLD link_data tf RunTime

%% plot finale Lc

colorOrder =  [ ...
        0            0            0       ;...% 6 BLACK
        0            0            0.7       ;...% 1 BLUE
        0            0.7            0       ;...% 3 GREEN (pale)
        0.7            0            0.7       ;...% 5 MAGENTA (pale)
        0            0.8            0.8       ;...% 4 CYAN
        0.6          0.5          0.4     ;  % 15 BROWN (dark)
        1            1            0       ;...% 7 YELLOW (pale)
        0            0.75         0.75    ;...% 8 TURQUOISE
        0            0.5          0       ;...% 9 GREEN (dark)
        0.75         0.75         0       ;...% 10 YELLOW (dark)
        1            0.50         0.25    ;...% 11 ORANGE
        0.75         0            0.75    ;...% 12 MAGENTA (dark)
        0.7          0.7          0.7     ;...% 13 GREY
        0.8          0.7          0.6     ;...% 14 BROWN (pale)
        0.4          0.7          0.6     ;...
        0.4          0          0.6     ;...
        0.4          0.6          0     ;...
        0.6          0.5          0.4 ];  % 18 BROWN (dark)
    
%Al ADV1
Lc_NASA_02=min(s_ADV_1_1a):(max(s_ADV_1_1a)-min(s_ADV_1_1a))/100:max(s_ADV_1_1a);
NASA_SBM_sub_02 = 0.1*(mass_imp_1a*norm(v_p_1a))^0.75*Lc_NASA_02.^(-1.71);
NASA_SBM_cat_02 = 0.1*(mass_shield_1+mass_imp_1a)^0.75*Lc_NASA_02.^(-1.71);

Lc_NASA_03=min(s_ADV_1_1b):(max(s_ADV_1_1b)-min(s_ADV_1_1b))/100:max(s_ADV_1_1b);
NASA_SBM_sub_03 = 0.1*(mass_imp_1b*norm(v_p_1b))^0.75*Lc_NASA_03.^(-1.71);
NASA_SBM_cat_03 = 0.1*(mass_shield_1+mass_imp_1b)^0.75*Lc_NASA_03.^(-1.71);

Lc_NASA_04=min(s_ADV_1_1c):(max(s_ADV_1_1c)-min(s_ADV_1_1c))/100:max(s_ADV_1_1c);
NASA_SBM_sub_04 = 0.1*(mass_imp_1c*norm(v_p_1c))^0.75*Lc_NASA_04.^(-1.71);
NASA_SBM_cat_04 = 0.1*(mass_shield_1+mass_imp_1c)^0.75*Lc_NASA_04.^(-1.71);

Lc_NASA_05=min(s_ADV_1_1d):(max(s_ADV_1_1d)-min(s_ADV_1_1d))/100:max(s_ADV_1_1d);
NASA_SBM_sub_05 = 0.1*(mass_imp_1d*norm(v_p_1d))^0.75*Lc_NASA_05.^(-1.71);
NASA_SBM_cat_05 = 0.1*(mass_shield_1+mass_imp_1d)^0.75*Lc_NASA_05.^(-1.71);

%Al ADV2
Lc_NASA_022=min(s_ADV_2_1a):(max(s_ADV_2_1a)-min(s_ADV_2_1a))/100:max(s_ADV_2_1a);
NASA_SBM_sub_022 = 0.1*(mass_imp_1a*norm(v_p_1a))^0.75*Lc_NASA_022.^(-1.71);
NASA_SBM_cat_022 = 0.1*(mass_shield_2+mass_imp_1a)^0.75*Lc_NASA_022.^(-1.71);

Lc_NASA_032=min(s_ADV_2_1b):(max(s_ADV_2_1b)-min(s_ADV_2_1b))/100:max(s_ADV_2_1b);
NASA_SBM_sub_032 = 0.1*(mass_imp_1b*norm(v_p_1b))^0.75*Lc_NASA_032.^(-1.71);
NASA_SBM_cat_032 = 0.1*(mass_shield_2+mass_imp_1b)^0.75*Lc_NASA_032.^(-1.71);

Lc_NASA_042=min(s_ADV_2_1c):(max(s_ADV_2_1c)-min(s_ADV_2_1c))/100:max(s_ADV_2_1c);
NASA_SBM_sub_042 = 0.1*(mass_imp_1c*norm(v_p_1c))^0.75*Lc_NASA_042.^(-1.71);
NASA_SBM_cat_042 = 0.1*(mass_shield_2+mass_imp_1c)^0.75*Lc_NASA_042.^(-1.71);

Lc_NASA_052=min(s_ADV_2_1d):(max(s_ADV_2_1d)-min(s_ADV_2_1d))/100:max(s_ADV_2_1d);
NASA_SBM_sub_052 = 0.1*(mass_imp_1d*norm(v_p_1d))^0.75*Lc_NASA_052.^(-1.71);
NASA_SBM_cat_052 = 0.1*(mass_shield_2+mass_imp_1d)^0.75*Lc_NASA_052.^(-1.71);


%Nylon 6 ADV1
Lc_NASA_02N=min(s_ADV_1_1aN):(max(s_ADV_1_1aN)-min(s_ADV_1_1aN))/100:max(s_ADV_1_1aN);
NASA_SBM_sub_02N = 0.1*(mass_imp_1aN*norm(v_p_1aN))^0.75*Lc_NASA_02N.^(-1.71);
NASA_SBM_cat_02N = 0.1*(mass_shield_1+mass_imp_1aN)^0.75*Lc_NASA_02N.^(-1.71);

Lc_NASA_03N=min(s_ADV_1_1bN):(max(s_ADV_1_1bN)-min(s_ADV_1_1bN))/100:max(s_ADV_1_1bN);
NASA_SBM_sub_03N = 0.1*(mass_imp_1bN*norm(v_p_1bN))^0.75*Lc_NASA_03N.^(-1.71);
NASA_SBM_cat_03N = 0.1*(mass_shield_1+mass_imp_1bN)^0.75*Lc_NASA_03N.^(-1.71);

Lc_NASA_04N=min(s_ADV_1_1cN):(max(s_ADV_1_1cN)-min(s_ADV_1_1cN))/100:max(s_ADV_1_1cN);
NASA_SBM_sub_04N = 0.1*(mass_imp_1cN*norm(v_p_1cN))^0.75*Lc_NASA_04N.^(-1.71);
NASA_SBM_cat_04N = 0.1*(mass_shield_1+mass_imp_1cN)^0.75*Lc_NASA_04N.^(-1.71);

Lc_NASA_05N=min(s_ADV_1_1dN):(max(s_ADV_1_1dN)-min(s_ADV_1_1dN))/100:max(s_ADV_1_1dN);
NASA_SBM_sub_05N = 0.1*(mass_imp_1dN*norm(v_p_1dN))^0.75*Lc_NASA_05N.^(-1.71);
NASA_SBM_cat_05N = 0.1*(mass_shield_1+mass_imp_1dN)^0.75*Lc_NASA_05N.^(-1.71);

%Nylon 6 ADV2
Lc_NASA_02N2=min(s_ADV_2_1aN):(max(s_ADV_2_1aN)-min(s_ADV_2_1aN))/100:max(s_ADV_2_1aN);
NASA_SBM_sub_02N2 = 0.1*(mass_imp_1aN*norm(v_p_1aN))^0.75*Lc_NASA_02N2.^(-1.71);
NASA_SBM_cat_02N2 = 0.1*(mass_shield_2+mass_imp_1aN)^0.75*Lc_NASA_02N2.^(-1.71);

Lc_NASA_03N2=min(s_ADV_2_1bN):(max(s_ADV_2_1bN)-min(s_ADV_2_1bN))/100:max(s_ADV_2_1bN);
NASA_SBM_sub_03N2 = 0.1*(mass_imp_1bN*norm(v_p_1bN))^0.75*Lc_NASA_03N2.^(-1.71);
NASA_SBM_cat_03N2 = 0.1*(mass_shield_2+mass_imp_1bN)^0.75*Lc_NASA_03N2.^(-1.71);

Lc_NASA_04N2=min(s_ADV_2_1cN):(max(s_ADV_2_1cN)-min(s_ADV_2_1cN))/100:max(s_ADV_2_1cN);
NASA_SBM_sub_04N2 = 0.1*(mass_imp_1cN*norm(v_p_1cN))^0.75*Lc_NASA_04N2.^(-1.71);
NASA_SBM_cat_04N2 = 0.1*(mass_shield_2+mass_imp_1cN)^0.75*Lc_NASA_04N2.^(-1.71);

Lc_NASA_05N2=min(s_ADV_2_1dN):(max(s_ADV_2_1dN)-min(s_ADV_2_1dN))/100:max(s_ADV_2_1dN);
NASA_SBM_sub_05N2 = 0.1*(mass_imp_1dN*norm(v_p_1dN))^0.75*Lc_NASA_05N2.^(-1.71);
NASA_SBM_cat_05N2 = 0.1*(mass_shield_2+mass_imp_1dN)^0.75*Lc_NASA_05N2.^(-1.71);

%% Plot Lc ADV_1 Al

figure('units','normalized','outerposition',[0 0 1 1],'Visible', 'on')
loglog(s_ADV_1_1a,[N_ADV_1_1a(1)+n_frags_bubbles_ADV_1_1a,N_ADV_1_1a(2:end)],'MarkerEdgeColor',colorOrder(9,:),'MarkerFaceColor',colorOrder(9,:),'Marker','*','LineStyle','none');
hold on
grid on
loglog(s_ADV_1_1b,[N_ADV_1_1b(1)+n_frags_bubbles_ADV_1_1b,N_ADV_1_1b(2:end)],'MarkerEdgeColor',colorOrder(11,:),'MarkerFaceColor',colorOrder(11,:),'Marker','o','LineStyle','none');
hold on
loglog(s_ADV_1_1c,[N_ADV_1_1c(1)+n_frags_bubbles_ADV_1_1c,N_ADV_1_1c(2:end)],'MarkerEdgeColor',colorOrder(5,:),'MarkerFaceColor',colorOrder(11,:),'Marker','^','LineStyle','none');
hold on
loglog(s_ADV_1_1d,[N_ADV_1_1d(1)+n_frags_bubbles_ADV_1_1d,N_ADV_1_1d(2:end)],'MarkerEdgeColor',colorOrder(2,:),'MarkerFaceColor',colorOrder(11,:),'Marker','+','LineStyle','none');
hold on
loglog(Lc_NASA_04,NASA_SBM_cat_04,'-k','LineWidth',2)
% loglog(Lc_NASA_05,NASA_SBM_cat_05,'-k','LineWidth',2)
set(gca,'FontSize',20,'FontWeight','bold')
title('Comparison Advanced shields 1 VS Al projectiles')
xlabel('L_c [m]')
ylabel('Cumulative number CN (L_c)')
legend('Advanced shield 1 (d=0.2)','Advanced shield 1 (d=0.3)','Advanced shield 1 (d=0.4)','Advanced shield 1 (d=0.5)','NASA - SBM Cat');
xx_lim = [4e-03,1e-1];
yy_lim = [1e0,3e2];
set(gca,'XLim',xx_lim)
set(gca,'YLim',yy_lim)

out_fig = 'Lc_ADVANCED_shield_1_Al';
saveas(gcf,out_fig,'fig')
saveas(gcf,out_fig,'png')

%% Plot Lc ADV_1 Nylon 6

figure('units','normalized','outerposition',[0 0 1 1],'Visible', 'on')
loglog(s_ADV_1_1aN,[N_ADV_1_1aN(1)+n_frags_bubbles_ADV_1_1aN,N_ADV_1_1aN(2:end)],'MarkerEdgeColor',colorOrder(9,:),'MarkerFaceColor',colorOrder(9,:),'Marker','*','LineStyle','none');
hold on
grid on
loglog(s_ADV_1_1bN,[N_ADV_1_1bN(1)+n_frags_bubbles_ADV_1_1bN,N_ADV_1_1bN(2:end)],'MarkerEdgeColor',colorOrder(11,:),'MarkerFaceColor',colorOrder(11,:),'Marker','o','LineStyle','none');
hold on
loglog(s_ADV_1_1cN,[N_ADV_1_1cN(1)+n_frags_bubbles_ADV_1_1cN,N_ADV_1_1cN(2:end)],'MarkerEdgeColor',colorOrder(5,:),'MarkerFaceColor',colorOrder(11,:),'Marker','^','LineStyle','none');
hold on
loglog(s_ADV_1_1dN,[N_ADV_1_1dN(1)+n_frags_bubbles_ADV_1_1dN,N_ADV_1_1dN(2:end)],'MarkerEdgeColor',colorOrder(2,:),'MarkerFaceColor',colorOrder(11,:),'Marker','+','LineStyle','none');
hold on
loglog(Lc_NASA_05N,NASA_SBM_cat_05N,'-k','LineWidth',2)
% loglog(Lc_NASA_05N,NASA_SBM_cat_05N,'-k','LineWidth',2)
set(gca,'FontSize',20,'FontWeight','bold')
title('Comparison Advanced shields 1 VS Nylon 6 projectiles')
xlabel('L_c [m]')
ylabel('Cumulative number CN (L_c)')
legend('Advanced shield 1 (d=0.2)','Advanced shield 1 (d=0.3)','Advanced shield 1 (d=0.4)','Advanced shield 1 (d=0.5)','NASA - SBM Cat');
xx_lim = [4e-03,1e-1];
yy_lim = [1e0,3e2];
set(gca,'XLim',xx_lim)
set(gca,'YLim',yy_lim)

out_fig = 'Lc_ADVANCED_shield_1_Nylon_6';
saveas(gcf,out_fig,'fig')
saveas(gcf,out_fig,'png')

%% Plot Lc ADV_2 Al

figure('units','normalized','outerposition',[0 0 1 1],'Visible', 'on')
loglog(s_ADV_2_1a,[N_ADV_2_1a(1)+n_frags_bubbles_ADV_2_1a,N_ADV_2_1a(2:end)],'MarkerEdgeColor',colorOrder(9,:),'MarkerFaceColor',colorOrder(9,:),'Marker','*','LineStyle','none');
hold on
grid on
loglog(s_ADV_2_1b,[N_ADV_2_1b(1)+n_frags_bubbles_ADV_2_1b,N_ADV_2_1b(2:end)],'MarkerEdgeColor',colorOrder(11,:),'MarkerFaceColor',colorOrder(11,:),'Marker','o','LineStyle','none');
hold on
loglog(s_ADV_2_1c,[N_ADV_2_1c(1)+n_frags_bubbles_ADV_2_1c,N_ADV_2_1c(2:end)],'MarkerEdgeColor',colorOrder(5,:),'MarkerFaceColor',colorOrder(11,:),'Marker','^','LineStyle','none');
hold on
loglog(s_ADV_2_1d,[N_ADV_2_1d(1)+n_frags_bubbles_ADV_2_1d,N_ADV_2_1d(2:end)],'MarkerEdgeColor',colorOrder(2,:),'MarkerFaceColor',colorOrder(11,:),'Marker','+','LineStyle','none');
hold on
% loglog(Lc_NASA_022,NASA_SBM_cat_022,'--k','LineWidth',2)
loglog(Lc_NASA_052,NASA_SBM_cat_052,'-k','LineWidth',2)
set(gca,'FontSize',20,'FontWeight','bold')
title('Comparison Advanced shields 2 VS Al projectiles')
xlabel('L_c [m]')
ylabel('Cumulative number CN (L_c)')
legend('Advanced shield 2 (d=0.2)','Advanced shield 2 (d=0.3)','Advanced shield 2 (d=0.4)','Advanced shield 2 (d=0.5)','NASA - SBM Cat');
xx_lim = [4e-03,1e-1];
yy_lim = [1e0,3e2];
set(gca,'XLim',xx_lim)
set(gca,'YLim',yy_lim)

out_fig = 'Lc_ADVANCED_shield_2_Al';
saveas(gcf,out_fig,'fig')
saveas(gcf,out_fig,'png')

%% Plot Lc ADV_2 Nylon 6

figure('units','normalized','outerposition',[0 0 1 1],'Visible', 'on')
loglog(s_ADV_2_1aN,[N_ADV_2_1aN(1)+n_frags_bubbles_ADV_2_1aN,N_ADV_2_1aN(2:end)],'MarkerEdgeColor',colorOrder(9,:),'MarkerFaceColor',colorOrder(9,:),'Marker','*','LineStyle','none');
hold on
grid on
loglog(s_ADV_2_1bN,[N_ADV_2_1bN(1)+n_frags_bubbles_ADV_2_1bN,N_ADV_2_1bN(2:end)],'MarkerEdgeColor',colorOrder(11,:),'MarkerFaceColor',colorOrder(11,:),'Marker','o','LineStyle','none');
hold on
loglog(s_ADV_2_1cN,[N_ADV_2_1cN(1)+n_frags_bubbles_ADV_2_1cN,N_ADV_2_1cN(2:end)],'MarkerEdgeColor',colorOrder(5,:),'MarkerFaceColor',colorOrder(11,:),'Marker','^','LineStyle','none');
hold on
loglog(s_ADV_2_1dN,[N_ADV_2_1dN(1)+n_frags_bubbles_ADV_2_1dN,N_ADV_2_1dN(2:end)],'MarkerEdgeColor',colorOrder(2,:),'MarkerFaceColor',colorOrder(11,:),'Marker','+','LineStyle','none');
hold on
% loglog(Lc_NASA_02N2,NASA_SBM_cat_02N2,'--k','LineWidth',2)
loglog(Lc_NASA_03N2,NASA_SBM_cat_03N2,'-k','LineWidth',2)
set(gca,'FontSize',20,'FontWeight','bold')
title('Comparison Advanced shields 2 VS Nylon 6 projectiles')
xlabel('L_c [m]')
ylabel('Cumulative number CN (L_c)')
legend('Advanced shield 2 (d=0.2)','Advanced shield 2 (d=0.3)','Advanced shield 2 (d=0.4)','Advanced shield 2 (d=0.5)','NASA - SBM Cat');
xx_lim = [4e-03,1e-1];
yy_lim = [1e0,3e2];
set(gca,'XLim',xx_lim)
set(gca,'YLim',yy_lim)

out_fig = 'Lc_ADVANCED_shield_2_Nylon_6';
saveas(gcf,out_fig,'fig')
saveas(gcf,out_fig,'png')

%% Plot Comparison Lc projectiles d=0.2
%{r
figure('units','normalized','outerposition',[0 0 1 1],'Visible', 'on')
loglog(s_ADV_1_1a,[N_ADV_1_1a(1)+n_frags_bubbles_ADV_1_1a,N_ADV_1_1a(2:end)],'MarkerEdgeColor',colorOrder(9,:),'MarkerFaceColor',colorOrder(9,:),'Marker','*','LineStyle','none');
hold on
grid on
loglog(s_ADV_1_1aN,[N_ADV_1_1aN(1)+n_frags_bubbles_ADV_1_1aN,N_ADV_1_1aN(2:end)],'MarkerEdgeColor',colorOrder(11,:),'MarkerFaceColor',colorOrder(11,:),'Marker','o','LineStyle','none');
hold on
loglog(s_ADV_2_1a,[N_ADV_2_1a(1)+n_frags_bubbles_ADV_2_1a,N_ADV_2_1a(2:end)],'MarkerEdgeColor',colorOrder(5,:),'MarkerFaceColor',colorOrder(11,:),'Marker','^','LineStyle','none');
hold on
loglog(s_ADV_2_1aN,[N_ADV_2_1aN(1)+n_frags_bubbles_ADV_2_1aN,N_ADV_2_1aN(2:end)],'MarkerEdgeColor',colorOrder(2,:),'MarkerFaceColor',colorOrder(11,:),'Marker','+','LineStyle','none');
hold on
% loglog(Lc_NASA_02,NASA_SBM_sub_02,'--k','LineWidth',2)
loglog(Lc_NASA_02,NASA_SBM_cat_02,'-k','LineWidth',2)
set(gca,'FontSize',20,'FontWeight','bold')
title('Comparison Advanced shields 1&2 VS projectiles with d=0.2 cm')
xlabel('L_c [m]')
ylabel('Cumulative number CN (L_c)')
legend('Advanced shield 1 (Al)','Advanced shield 1 (Nylon 6)','Advanced shield 2 (Al)','Advanced shield 2 (Nylon 6)','NASA - SBM Cat');
xx_lim = [4e-03,1e-1];
yy_lim = [1e0,3e2];
set(gca,'XLim',xx_lim)
set(gca,'YLim',yy_lim)

out_fig = 'Lc_ADVANCED_shield_1&2_d02';
saveas(gcf,out_fig,'fig')
saveas(gcf,out_fig,'png')

%% Plot Comparison Lc projectiles d=0.2

figure('units','normalized','outerposition',[0 0 1 1],'Visible', 'on')
loglog(s_ADV_1_1a,[N_ADV_1_1a(1)+n_frags_bubbles_ADV_1_1a,N_ADV_1_1a(2:end)],'MarkerEdgeColor',colorOrder(9,:),'MarkerFaceColor',colorOrder(9,:),'Marker','*','LineStyle','none');
hold on
grid on
loglog(s_ADV_1_1a2,[N_ADV_1_1a2(1)+n_frags_bubbles_ADV_1_1a2,N_ADV_1_1a2(2:end)],'MarkerEdgeColor',colorOrder(11,:),'MarkerFaceColor',colorOrder(11,:),'Marker','o','LineStyle','none');
hold on
loglog(Lc_NASA_02,NASA_SBM_cat_02,'-k','LineWidth',2)
set(gca,'FontSize',20,'FontWeight','bold')
title('Comparison Advanced shields 1&2 VS projectiles with d=0.2 cm')
xlabel('L_c [m]')
ylabel('Cumulative number CN (L_c)')
legend('Advanced shield 1 (Al)','Advanced shield 1 (Al double thickness)','NASA - SBM Cat');
xx_lim = [4e-03,1e-1];
yy_lim = [1e0,3e2];
set(gca,'XLim',xx_lim)
set(gca,'YLim',yy_lim)

out_fig = 'Lc_ADVANCED_shield_1&2_d02';
saveas(gcf,out_fig,'fig')
saveas(gcf,out_fig,'png')

%% Plot Comparison Lc projectiles d=0.3

figure('units','normalized','outerposition',[0 0 1 1],'Visible', 'on')
loglog(s_ADV_1_1b,[N_ADV_1_1b(1)+n_frags_bubbles_ADV_1_1b,N_ADV_1_1b(2:end)],'MarkerEdgeColor',colorOrder(9,:),'MarkerFaceColor',colorOrder(9,:),'Marker','*','LineStyle','none');
hold on
grid on
loglog(s_ADV_1_1bN,[N_ADV_1_1bN(1)+n_frags_bubbles_ADV_1_1bN,N_ADV_1_1bN(2:end)],'MarkerEdgeColor',colorOrder(11,:),'MarkerFaceColor',colorOrder(11,:),'Marker','o','LineStyle','none');
hold on
loglog(s_ADV_2_1b,[N_ADV_2_1b(1)+n_frags_bubbles_ADV_2_1b,N_ADV_2_1b(2:end)],'MarkerEdgeColor',colorOrder(5,:),'MarkerFaceColor',colorOrder(11,:),'Marker','^','LineStyle','none');
hold on
loglog(s_ADV_2_1bN,[N_ADV_2_1bN(1)+n_frags_bubbles_ADV_2_1bN,N_ADV_2_1bN(2:end)],'MarkerEdgeColor',colorOrder(2,:),'MarkerFaceColor',colorOrder(11,:),'Marker','+','LineStyle','none');
hold on
loglog(Lc_NASA_03N2,NASA_SBM_cat_03N2,'-k','LineWidth',2)
% loglog(Lc_NASA_03N,NASA_SBM_cat_03N,'-k','LineWidth',2)
set(gca,'FontSize',20,'FontWeight','bold')
title('Comparison Advanced shields 1&2 VS projectiles with d=0.3 cm')
xlabel('L_c [m]')
ylabel('Cumulative number CN (L_c)')
legend('Advanced shield 1 (Al)','Advanced shield 1 (Nylon 6)','Advanced shield 2 (Al)','Advanced shield 2 (Nylon 6)','NASA - SBM Cat');
xx_lim = [4e-03,1e-1];
yy_lim = [1e0,3e2];
set(gca,'XLim',xx_lim)
set(gca,'YLim',yy_lim)

out_fig = 'Lc_ADVANCED_shield_1&2_d03';
saveas(gcf,out_fig,'fig')
saveas(gcf,out_fig,'png')

%% Plot Comparison Lc projectiles d=0.4

figure('units','normalized','outerposition',[0 0 1 1],'Visible', 'on')
loglog(s_ADV_1_1c,[N_ADV_1_1c(1)+n_frags_bubbles_ADV_1_1c,N_ADV_1_1c(2:end)],'MarkerEdgeColor',colorOrder(9,:),'MarkerFaceColor',colorOrder(9,:),'Marker','*','LineStyle','none');
hold on
grid on
loglog(s_ADV_1_1cN,[N_ADV_1_1cN(1)+n_frags_bubbles_ADV_1_1cN,N_ADV_1_1cN(2:end)],'MarkerEdgeColor',colorOrder(11,:),'MarkerFaceColor',colorOrder(11,:),'Marker','o','LineStyle','none');
hold on
loglog(s_ADV_2_1c,[N_ADV_2_1c(1)+n_frags_bubbles_ADV_2_1c,N_ADV_2_1c(2:end)],'MarkerEdgeColor',colorOrder(5,:),'MarkerFaceColor',colorOrder(11,:),'Marker','^','LineStyle','none');
hold on
loglog(s_ADV_2_1cN,[N_ADV_2_1cN(1)+n_frags_bubbles_ADV_2_1cN,N_ADV_2_1cN(2:end)],'MarkerEdgeColor',colorOrder(2,:),'MarkerFaceColor',colorOrder(11,:),'Marker','+','LineStyle','none');
hold on
loglog(Lc_NASA_04,NASA_SBM_cat_04,'-k','LineWidth',2)
% loglog(Lc_NASA_04N,NASA_SBM_cat_04N,'-k','LineWidth',2)
set(gca,'FontSize',20,'FontWeight','bold')
title('Comparison Advanced shields 1&2 VS projectiles with d=0.4 cm')
xlabel('L_c [m]')
ylabel('Cumulative number CN (L_c)')
legend('Advanced shield 1 (Al)','Advanced shield 1 (Nylon 6)','Advanced shield 2 (Al)','Advanced shield 2 (Nylon 6)','NASA - SBM Cat');
xx_lim = [4e-03,1e-1];
yy_lim = [1e0,3e2];
set(gca,'XLim',xx_lim)
set(gca,'YLim',yy_lim)

out_fig = 'Lc_ADVANCED_shield_1&2_d04';
saveas(gcf,out_fig,'fig')
saveas(gcf,out_fig,'png')

%% Plot Comparison Lc projectiles d=0.5

figure('units','normalized','outerposition',[0 0 1 1],'Visible', 'on')
loglog(s_ADV_1_1d,[N_ADV_1_1d(1)+n_frags_bubbles_ADV_1_1d,N_ADV_1_1d(2:end)],'MarkerEdgeColor',colorOrder(9,:),'MarkerFaceColor',colorOrder(9,:),'Marker','*','LineStyle','none');
hold on
grid on
loglog(s_ADV_1_1dN,[N_ADV_1_1dN(1)+n_frags_bubbles_ADV_1_1dN,N_ADV_1_1dN(2:end)],'MarkerEdgeColor',colorOrder(11,:),'MarkerFaceColor',colorOrder(11,:),'Marker','o','LineStyle','none');
hold on
loglog(s_ADV_2_1d,[N_ADV_2_1d(1)+n_frags_bubbles_ADV_2_1d,N_ADV_2_1d(2:end)],'MarkerEdgeColor',colorOrder(5,:),'MarkerFaceColor',colorOrder(11,:),'Marker','^','LineStyle','none');
hold on
loglog(s_ADV_2_1dN,[N_ADV_2_1dN(1)+n_frags_bubbles_ADV_2_1dN,N_ADV_2_1dN(2:end)],'MarkerEdgeColor',colorOrder(2,:),'MarkerFaceColor',colorOrder(11,:),'Marker','+','LineStyle','none');
hold on
% loglog(Lc_NASA_05,NASA_SBM_cat_05,'--k','LineWidth',2)
loglog(Lc_NASA_05N,NASA_SBM_cat_05N,'-k','LineWidth',2)
set(gca,'FontSize',20,'FontWeight','bold')
title('Comparison Advanced shields 1&2 VS projectiles with d=0.5 cm')
xlabel('L_c [m]')
ylabel('Cumulative number CN (L_c)')
legend('Advanced shield 1 (Al)','Advanced shield 1 (Nylon 6)','Advanced shield 2 (Al)','Advanced shield 2 (Nylon 6)','NASA - SBM Cat');
xx_lim = [4e-03,1e-1];
yy_lim = [1e0,3e2];
set(gca,'XLim',xx_lim)
set(gca,'YLim',yy_lim)

out_fig = 'Lc_ADVANCED_shield_1&2_d05';
saveas(gcf,out_fig,'fig')
saveas(gcf,out_fig,'png')

%% plot finale ADV_1 Al A su M
%{t
clear DAmSOC DAmSOC_matlab DAmSOC_NASA csi_min csi_max csi_NASA n
lambda_c_min=log10(min([s_ADV_1_1a s_ADV_1_1b s_ADV_1_1c s_ADV_1_1d]));
lambda_c_max=log10(max([s_ADV_1_1a s_ADV_1_1b s_ADV_1_1c s_ADV_1_1d]));
lambda_c=lambda_c_min:(lambda_c_max-lambda_c_min)/30:lambda_c_max;
Am_ADV_1_1d =[Am_CST_ADV_1_1a Am_CST_ADV_1_1b Am_CST_ADV_1_1c Am_CST_ADV_1_1d];
csi_min=log10(min(Am_ADV_1_1d(Am_ADV_1_1d>0)));
csi_max=log10(max(Am_ADV_1_1d));
n=length(lambda_c);
csi_NASA=csi_min:(csi_max-csi_min)/(n-1):csi_max;

for i=1:1:n
        if  lambda_c(i)<=-3.5
            muSOC=-0.3;
            sigmaSOC=0.2;
            DAmSOC(i,:)=(1/(sigmaSOC*(2*pi)^0.5))*exp(-((csi_NASA-muSOC).^2)/(2*sigmaSOC^2));
            pd = makedist('Normal','mu',muSOC,'sigma',sigmaSOC);
            DAmSOC_matlab(i,:) = pdf(pd,csi_NASA);
        elseif lambda_c(i)>-3.5 && lambda_c(i)<=-1.75
            muSOC=-0.3;
            sigmaSOC=0.2+0.1333*(lambda_c(i)+3.5);
            DAmSOC(i,:)=(1/(sigmaSOC*(2*pi)^0.5))*exp(-((csi_NASA-muSOC).^2)/(2*sigmaSOC^2));
            pd = makedist('Normal','mu',muSOC,'sigma',sigmaSOC);
            DAmSOC_matlab(i,:) = pdf(pd,csi_NASA);
        elseif lambda_c(i)>-1.75 && lambda_c(i)<-1.25
            muSOC=-0.3-1.4*(lambda_c(i)+1.75);
            sigmaSOC=0.2+0.1333*(lambda_c(i)+3.5);
            DAmSOC(i,:)=(1/(sigmaSOC*(2*pi)^0.5))*exp(-((csi_NASA-muSOC).^2)/(2*sigmaSOC^2));
            pd = makedist('Normal','mu',muSOC,'sigma',sigmaSOC);
            DAmSOC_matlab(i,:) = pdf(pd,csi_NASA);
        elseif lambda_c(i)>=-1.25
            muSOC=-1.0;
            sigmaSOC=0.2+0.1333*(lambda_c(i)+3.5);
            DAmSOC(i,:)=(1/(sigmaSOC*(2*pi)^0.5))*exp(-((csi_NASA-muSOC).^2)/(2*sigmaSOC^2));
            pd = makedist('Normal','mu',muSOC,'sigma',sigmaSOC);
            DAmSOC_matlab(i,:) = pdf(pd,csi_NASA);
        end
end

for k=1:1:size(DAmSOC,2)
    DAmSOC_NASA(k)=sum(DAmSOC(:,k));
    DAmSOC_matlab_NASA(k) = sum(DAmSOC_matlab(:,k));
end

%%
figure('units','normalized','outerposition',[0 0 1 1],'Visible', 'on')
histogram(log10(Am_CST_ADV_1_1a),'Normalization','probability','BinWidth',0.035)
grid on
hold on
histogram(log10(Am_CST_ADV_1_1b),'Normalization','probability','BinWidth',0.035)
hold on
histogram(log10(Am_CST_ADV_1_1c),'Normalization','probability','BinWidth',0.035)
hold on
histogram(log10(Am_CST_ADV_1_1d),'Normalization','probability','BinWidth',0.035)
hold on
plot(csi_NASA,DAmSOC_NASA./sum(DAmSOC_NASA),'-b','LineWidth',2)
hold on
title('Comparison among PDFs Advanced Shields 1 with Al projectiles')
set(gca,'FontSize',20,'FontWeight','bold')
xlabel('log_{10}(A/m) [m^{2}/kg]')
ylabel('Relative number N(A/m)')
legend('Advanced shield 1 (d=0.2)','Advanced shield 1 (d=0.3)','Advanced shield 1 (d=0.4)','Advanced shield 1 (d=0.5)','NASA - SBM');
% xx_lim_Am = [-2,0.3];
% yy_lim_Am = [0,0.12];
% set(gca,'XLim',xx_lim_Am)
% set(gca,'YLim',yy_lim_Am)

out_fig = 'A_su_m_CST_ADV_1_Al';
saveas(gcf,out_fig,'fig')
saveas(gcf,out_fig,'png')

%% plot finale ADV_1 Nylon 6 su M

clear DAmSOC DAmSOC_matlab DAmSOC_NASA csi_min csi_max csi_NASA n
lambda_c_minN=log10(min([s_ADV_1_1aN s_ADV_1_1bN s_ADV_1_1cN s_ADV_1_1dN]));
lambda_c_maxN=log10(max([s_ADV_1_1aN s_ADV_1_1bN s_ADV_1_1cN s_ADV_1_1dN]));
lambda_c=lambda_c_minN:(lambda_c_maxN-lambda_c_minN)/30:lambda_c_maxN;
Am_ADV_1N =[Am_CST_ADV_1_1aN Am_CST_ADV_1_1bN Am_CST_ADV_1_1cN Am_CST_ADV_1_1dN];
csi_minN=log10(min(Am_ADV_1N(Am_ADV_1N>0)));
csi_maxN=log10(max(Am_ADV_1N));
n=length(lambda_c);
csi_NASAN=csi_minN:(csi_maxN-csi_minN)/(n-1):csi_maxN;

for i=1:1:n
        if  lambda_c(i)<=-3.5
            muSOC=-0.3;
            sigmaSOC=0.2;
            DAmSOC(i,:)=(1/(sigmaSOC*(2*pi)^0.5))*exp(-((csi_NASAN-muSOC).^2)/(2*sigmaSOC^2));
            pd = makedist('Normal','mu',muSOC,'sigma',sigmaSOC);
            DAmSOC_matlabN(i,:) = pdf(pd,csi_NASAN);
        elseif lambda_c(i)>-3.5 && lambda_c(i)<=-1.75
            muSOC=-0.3;
            sigmaSOC=0.2+0.1333*(lambda_c(i)+3.5);
            DAmSOC(i,:)=(1/(sigmaSOC*(2*pi)^0.5))*exp(-((csi_NASAN-muSOC).^2)/(2*sigmaSOC^2));
            pd = makedist('Normal','mu',muSOC,'sigma',sigmaSOC);
            DAmSOC_matlabN(i,:) = pdf(pd,csi_NASAN);
        elseif lambda_c(i)>-1.75 && lambda_c(i)<-1.25
            muSOC=-0.3-1.4*(lambda_c(i)+1.75);
            sigmaSOC=0.2+0.1333*(lambda_c(i)+3.5);
            DAmSOC(i,:)=(1/(sigmaSOC*(2*pi)^0.5))*exp(-((csi_NASAN-muSOC).^2)/(2*sigmaSOC^2));
            pd = makedist('Normal','mu',muSOC,'sigma',sigmaSOC);
            DAmSOC_matlabN(i,:) = pdf(pd,csi_NASAN);
        elseif lambda_c(i)>=-1.25
            muSOC=-1.0;
            sigmaSOC=0.2+0.1333*(lambda_c(i)+3.5);
            DAmSOC(i,:)=(1/(sigmaSOC*(2*pi)^0.5))*exp(-((csi_NASAN-muSOC).^2)/(2*sigmaSOC^2));
            pd = makedist('Normal','mu',muSOC,'sigma',sigmaSOC);
            DAmSOC_matlabN(i,:) = pdf(pd,csi_NASAN);
        end
end

for k=1:1:size(DAmSOC,2)
    DAmSOC_NASAN(k)=sum(DAmSOC(:,k));
    DAmSOC_matlab_NASAN(k) = sum(DAmSOC_matlabN(:,k));
end

figure('units','normalized','outerposition',[0 0 1 1],'Visible', 'on')
histogram(log10(Am_CST_ADV_1_1aN),'Normalization','probability','BinWidth',0.035)
grid on
hold on
histogram(log10(Am_CST_ADV_1_1bN),'Normalization','probability','BinWidth',0.035)
hold on
histogram(log10(Am_CST_ADV_1_1cN),'Normalization','probability','BinWidth',0.035)
hold on
histogram(log10(Am_CST_ADV_1_1dN),'Normalization','probability','BinWidth',0.035)
hold on
plot(csi_NASAN,DAmSOC_NASAN./sum(DAmSOC_NASAN),'-b','LineWidth',2)
hold on
title('Comparison among PDFs Advanced Shields 1 with Nylon 6 projectiles')
set(gca,'FontSize',20,'FontWeight','bold')
xlabel('log_{10}(A/m) [m^{2}/kg]')
ylabel('Relative number N(A/m)')
legend('Advanced shield 1 (d=0.2)','Advanced shield 1 (d=0.3)','Advanced shield 1 (d=0.4)','Advanced shield 1 (d=0.5)','NASA - SBM');
% xx_lim_Am = [-2,0.3];
% yy_lim_Am = [0,0.12];
% set(gca,'XLim',xx_lim_Am)
% set(gca,'YLim',yy_lim_Am)

out_fig = 'A_su_m_CST_ADV_1_Nylon_6';
saveas(gcf,out_fig,'fig')
saveas(gcf,out_fig,'png')
%}

%% plot finale ADV_2 Al A su M
%{t
clear DAmSOC DAmSOC_matlab DAmSOC_NASA csi_min csi_max csi_NASA n
lambda_c_min=log10(min([s_ADV_2_1a s_ADV_2_1b s_ADV_2_1c s_ADV_2_1d]));
lambda_c_max=log10(max([s_ADV_2_1a s_ADV_2_1b s_ADV_2_1c s_ADV_2_1d]));
lambda_c=lambda_c_min:(lambda_c_max-lambda_c_min)/30:lambda_c_max;
Am_ADV_2_1d =[Am_CST_ADV_2_1a Am_CST_ADV_2_1b Am_CST_ADV_2_1c Am_CST_ADV_2_1d];
csi_min=log10(min(Am_ADV_2_1d(Am_ADV_2_1d>0)));
csi_max=log10(max(Am_ADV_2_1d));
n=length(lambda_c);
csi_NASA=csi_min:(csi_max-csi_min)/(n-1):csi_max;

for i=1:1:n
        if  lambda_c(i)<=-3.5
            muSOC=-0.3;
            sigmaSOC=0.2;
            DAmSOC(i,:)=(1/(sigmaSOC*(2*pi)^0.5))*exp(-((csi_NASA-muSOC).^2)/(2*sigmaSOC^2));
            pd = makedist('Normal','mu',muSOC,'sigma',sigmaSOC);
            DAmSOC_matlab(i,:) = pdf(pd,csi_NASA);
        elseif lambda_c(i)>-3.5 && lambda_c(i)<=-1.75
            muSOC=-0.3;
            sigmaSOC=0.2+0.1333*(lambda_c(i)+3.5);
            DAmSOC(i,:)=(1/(sigmaSOC*(2*pi)^0.5))*exp(-((csi_NASA-muSOC).^2)/(2*sigmaSOC^2));
            pd = makedist('Normal','mu',muSOC,'sigma',sigmaSOC);
            DAmSOC_matlab(i,:) = pdf(pd,csi_NASA);
        elseif lambda_c(i)>-1.75 && lambda_c(i)<-1.25
            muSOC=-0.3-1.4*(lambda_c(i)+1.75);
            sigmaSOC=0.2+0.1333*(lambda_c(i)+3.5);
            DAmSOC(i,:)=(1/(sigmaSOC*(2*pi)^0.5))*exp(-((csi_NASA-muSOC).^2)/(2*sigmaSOC^2));
            pd = makedist('Normal','mu',muSOC,'sigma',sigmaSOC);
            DAmSOC_matlab(i,:) = pdf(pd,csi_NASA);
        elseif lambda_c(i)>=-1.25
            muSOC=-1.0;
            sigmaSOC=0.2+0.1333*(lambda_c(i)+3.5);
            DAmSOC(i,:)=(1/(sigmaSOC*(2*pi)^0.5))*exp(-((csi_NASA-muSOC).^2)/(2*sigmaSOC^2));
            pd = makedist('Normal','mu',muSOC,'sigma',sigmaSOC);
            DAmSOC_matlab(i,:) = pdf(pd,csi_NASA);
        end
end

for k=1:1:size(DAmSOC,2)
    DAmSOC_NASA(k)=sum(DAmSOC(:,k));
    DAmSOC_matlab_NASA(k) = sum(DAmSOC_matlab(:,k));
end

%%
figure('units','normalized','outerposition',[0 0 1 1],'Visible', 'on')
histogram(log10(Am_CST_ADV_2_1a),'Normalization','probability','BinWidth',0.035)
grid on
hold on
histogram(log10(Am_CST_ADV_2_1b),'Normalization','probability','BinWidth',0.035)
hold on
histogram(log10(Am_CST_ADV_2_1c),'Normalization','probability','BinWidth',0.035)
hold on
histogram(log10(Am_CST_ADV_2_1d),'Normalization','probability','BinWidth',0.035)
hold on
plot(csi_NASA,DAmSOC_NASA./sum(DAmSOC_NASA),'-b','LineWidth',2)
hold on
title('Comparison among PDFs Advanced Shields 2 with Al projectiles')
set(gca,'FontSize',20,'FontWeight','bold')
xlabel('log_{10}(A/m) [m^{2}/kg]')
ylabel('Relative number N(A/m)')
legend('Advanced shield 2 (d=0.2)','Advanced shield 2 (d=0.3)','Advanced shield 2 (d=0.4)','Advanced shield 2 (d=0.5)','NASA - SBM');
% xx_lim_Am = [-2,0.3];
% yy_lim_Am = [0,0.12];
% set(gca,'XLim',xx_lim_Am)
% set(gca,'YLim',yy_lim_Am)

out_fig = 'A_su_m_CST_ADV_2_Al';
saveas(gcf,out_fig,'fig')
saveas(gcf,out_fig,'png')

%% plot finale ADV_2 Nylon 6 su M

clear DAmSOC DAmSOC_matlab DAmSOC_NASA csi_min csi_max csi_NASA n
lambda_c_minN=log10(min([s_ADV_2_1aN s_ADV_2_1bN s_ADV_2_1cN s_ADV_2_1dN]));
lambda_c_maxN=log10(max([s_ADV_2_1aN s_ADV_2_1bN s_ADV_2_1cN s_ADV_2_1dN]));
lambda_c=lambda_c_minN:(lambda_c_maxN-lambda_c_minN)/30:lambda_c_maxN;
Am_ADV_2N =[Am_CST_ADV_2_1aN Am_CST_ADV_2_1bN Am_CST_ADV_2_1cN Am_CST_ADV_2_1dN];
csi_minN=log10(min(Am_ADV_2N(Am_ADV_2N>0)));
csi_maxN=log10(max(Am_ADV_2N));
n=length(lambda_c);
csi_NASAN=csi_minN:(csi_maxN-csi_minN)/(n-1):csi_maxN;

for i=1:1:n
        if  lambda_c(i)<=-3.5
            muSOC=-0.3;
            sigmaSOC=0.2;
            DAmSOC(i,:)=(1/(sigmaSOC*(2*pi)^0.5))*exp(-((csi_NASAN-muSOC).^2)/(2*sigmaSOC^2));
            pd = makedist('Normal','mu',muSOC,'sigma',sigmaSOC);
            DAmSOC_matlabN(i,:) = pdf(pd,csi_NASAN);
        elseif lambda_c(i)>-3.5 && lambda_c(i)<=-1.75
            muSOC=-0.3;
            sigmaSOC=0.2+0.1333*(lambda_c(i)+3.5);
            DAmSOC(i,:)=(1/(sigmaSOC*(2*pi)^0.5))*exp(-((csi_NASAN-muSOC).^2)/(2*sigmaSOC^2));
            pd = makedist('Normal','mu',muSOC,'sigma',sigmaSOC);
            DAmSOC_matlabN(i,:) = pdf(pd,csi_NASAN);
        elseif lambda_c(i)>-1.75 && lambda_c(i)<-1.25
            muSOC=-0.3-1.4*(lambda_c(i)+1.75);
            sigmaSOC=0.2+0.1333*(lambda_c(i)+3.5);
            DAmSOC(i,:)=(1/(sigmaSOC*(2*pi)^0.5))*exp(-((csi_NASAN-muSOC).^2)/(2*sigmaSOC^2));
            pd = makedist('Normal','mu',muSOC,'sigma',sigmaSOC);
            DAmSOC_matlabN(i,:) = pdf(pd,csi_NASAN);
        elseif lambda_c(i)>=-1.25
            muSOC=-1.0;
            sigmaSOC=0.2+0.1333*(lambda_c(i)+3.5);
            DAmSOC(i,:)=(1/(sigmaSOC*(2*pi)^0.5))*exp(-((csi_NASAN-muSOC).^2)/(2*sigmaSOC^2));
            pd = makedist('Normal','mu',muSOC,'sigma',sigmaSOC);
            DAmSOC_matlabN(i,:) = pdf(pd,csi_NASAN);
        end
end

for k=1:1:size(DAmSOC,2)
    DAmSOC_NASAN(k)=sum(DAmSOC(:,k));
    DAmSOC_matlab_NASAN(k) = sum(DAmSOC_matlabN(:,k));
end

figure('units','normalized','outerposition',[0 0 1 1],'Visible', 'on')
histogram(log10(Am_CST_ADV_2_1aN),'Normalization','probability','BinWidth',0.035)
grid on
hold on
histogram(log10(Am_CST_ADV_2_1bN),'Normalization','probability','BinWidth',0.035)
hold on
histogram(log10(Am_CST_ADV_2_1cN),'Normalization','probability','BinWidth',0.035)
hold on
histogram(log10(Am_CST_ADV_2_1dN),'Normalization','probability','BinWidth',0.035)
hold on
plot(csi_NASAN,DAmSOC_NASAN./sum(DAmSOC_NASAN),'-b','LineWidth',2)
hold on
title('Comparison among PDFs Advanced Shields 2 with Nylon 6 projectiles')
set(gca,'FontSize',20,'FontWeight','bold')
xlabel('log_{10}(A/m) [m^{2}/kg]')
ylabel('Relative number N(A/m)')
legend('Advanced shield 1 (d=0.2)','Advanced shield 1 (d=0.3)','Advanced shield 1 (d=0.4)','Advanced shield 1 (d=0.5)','NASA - SBM');
% xx_lim_Am = [-2,0.3];
% yy_lim_Am = [0,0.12];
% set(gca,'XLim',xx_lim_Am)
% set(gca,'YLim',yy_lim_Am)

out_fig = 'A_su_m_CST_ADV_2_Nylon 6';
saveas(gcf,out_fig,'fig')
saveas(gcf,out_fig,'png')


%% plot cpu time and simulation time
if exist('Data_Simulation.txt','file')
    delete('Data_Simulation.txt')
end
diary Data_Simulation.txt
diary on
disp('----------------------------------------------------------------------------------------------------------')
disp('SIMULATION                            CPUTIME (min)       SIMULATION TIME (s)       FINAL STEP')
disp(['ADVANCED shield type 1 (Al, d=0.2)',cputime_ADV_1_1a,tf_ADV_1_1a,final_step_ADV_1_1a])
disp(['ADVANCED shield type 1 (Al, d=0.3)',cputime_ADV_1_1b,tf_ADV_1_1b,final_step_ADV_1_1b])
disp(['ADVANCED shield type 1 (Al, d=0.4)',cputime_ADV_1_1c,tf_ADV_1_1c,final_step_ADV_1_1c])
disp(['ADVANCED shield type 1 (Al, d=0.5)',cputime_ADV_1_1d,tf_ADV_1_1d,final_step_ADV_1_1d])
disp('----------------------------------------------------------------------------------------------------------')
disp(['ADVANCED shield type 1 (Nylon, d=0.2)',cputime_ADV_1_1aN,tf_ADV_1_1aN,final_step_ADV_1_1aN])
disp(['ADVANCED shield type 1 (Nylon, d=0.3)',cputime_ADV_1_1bN,tf_ADV_1_1bN,final_step_ADV_1_1bN])
disp(['ADVANCED shield type 1 (Nylon, d=0.4)',cputime_ADV_1_1cN,tf_ADV_1_1cN,final_step_ADV_1_1cN])
disp(['ADVANCED shield type 1 (Nylon, d=0.5)',cputime_ADV_1_1dN,tf_ADV_1_1dN,final_step_ADV_1_1dN])
disp('----------------------------------------------------------------------------------------------------------')
disp(['ADVANCED shield type 2 (Al, d=0.2)',cputime_ADV_2_1a,tf_ADV_2_1a,final_step_ADV_2_1a])
disp(['ADVANCED shield type 2 (Al, d=0.3)',cputime_ADV_2_1b,tf_ADV_2_1b,final_step_ADV_2_1b])
disp(['ADVANCED shield type 2 (Al, d=0.4)',cputime_ADV_2_1c,tf_ADV_2_1c,final_step_ADV_2_1c])
disp(['ADVANCED shield type 2 (Al, d=0.5)',cputime_ADV_2_1d,tf_ADV_2_1d,final_step_ADV_2_1d])
disp('----------------------------------------------------------------------------------------------------------')
disp(['ADVANCED shield type 2 (Nylon, d=0.2)',cputime_ADV_2_1aN,tf_ADV_2_1aN,final_step_ADV_2_1aN])
disp(['ADVANCED shield type 2 (Nylon, d=0.3)',cputime_ADV_2_1bN,tf_ADV_2_1bN,final_step_ADV_2_1bN])
disp(['ADVANCED shield type 2 (Nylon, d=0.4)',cputime_ADV_2_1cN,tf_ADV_2_1cN,final_step_ADV_2_1cN])
disp(['ADVANCED shield type 2 (Nylon, d=0.5)',cputime_ADV_2_1dN,tf_ADV_2_1dN,final_step_ADV_2_1dN])
disp('----------------------------------------------------------------------------------------------------------')
diary off
%}