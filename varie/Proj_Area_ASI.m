function [Area_ASI]=Proj_Area_ASI(length_ASI,ME)

%% Area ASI
ME_c_hull=struct('c_hull_global',[]);
for i=1:length_ASI
    q0=ME(i).DYNAMICS_INITIAL_DATA.quaternions0(1);
    q1=ME(i).DYNAMICS_INITIAL_DATA.quaternions0(2);
    q2=ME(i).DYNAMICS_INITIAL_DATA.quaternions0(3);
    q3=ME(i).DYNAMICS_INITIAL_DATA.quaternions0(4);
    Rot_l2g=[q0^2+q1^2-q2^2-q3^2, 2*(q1*q2-q0*q3), 2*(q0*q2+q1*q3);
        2*(q1*q2+q0*q3)    , q0^2-q1^2+q2^2-q3^2, 2*(q2*q3-q0*q1);
        2*(q1*q3-q0*q2)    , 2*(q0*q1+q2*q3),     q0^2-q1^2-q2^2+q3^2];
    
    Rot_l2g=Rot_l2g'; %transpose the matrix
    c_hull_global=ME(i).GEOMETRY_DATA.c_hull*0;
    for jj=1:length(ME(i).GEOMETRY_DATA.c_hull)
        c_hull_global(jj,:)=ME(i).DYNAMICS_INITIAL_DATA.cm_coord0+Rot_l2g*[ME(i).GEOMETRY_DATA.c_hull(jj,:)'];
    end
    ME_c_hull(i).c_hull_global=c_hull_global; %c_hull vertices in global system
end

c_hull_ASI=[];
for i=1:length_ASI
   c_hull_ASI=[c_hull_ASI; ME_c_hull(i).c_hull_global]; %c_hull vertices in global system
end

c_hull_ASI_x = [zeros(size(c_hull_ASI,1),1),c_hull_ASI(:,2:3)];
c_hull_ASI_y = [c_hull_ASI(:,1),zeros(size(c_hull_ASI,1),1),c_hull_ASI(:,3)];
c_hull_ASI_z = [c_hull_ASI(:,1:2),zeros(size(c_hull_ASI,1),1)];

%% Area X
figure
for zz=1:length(ME_c_hull)-1
    plot(c_hull_ASI_x(:,2),c_hull_ASI_x(:,3))
    hold on
end
[xi,yi] = getpts;
polyin = polyshape([xi,yi]);
plot(polyin)
grid on
axis equal
title('ASI - X view')
saveas(gcf,'ASI - X view','fig')
saveas(gcf,'ASI - X view','png')
Area_ASI(1) = area(polyin)
% Area_ASI(1) = polyArea3d([zeros(length(xi),1),xi,yi],[1 0 0]);
clear xi yi polyin
close all
%% Area Y
figure
for zz=1:length(ME_c_hull)-1
    plot(c_hull_ASI_y(:,1),c_hull_ASI_y(:,3))
    hold on
end
[xi,yi] = getpts;
polyin = polyshape([xi,yi]);
plot(polyin)
grid on
axis equal
title('ASI - Y view')
saveas(gcf,'ASI - Y view','fig')
saveas(gcf,'ASI - Y view','png')
Area_ASI(2) = area(polyin);
% Area_ASI(2) = polyArea3d([xi,zeros(length(xi),1),yi],[0 1 0]);
clear xi yi polyin
close all
%% Area Z
figure
for zz=1:length(ME_c_hull)-1
    plot(c_hull_ASI_z(:,1),c_hull_ASI_z(:,2))
    hold on
end
[xi,yi] = getpts;
polyin = polyshape([xi,yi]);
plot(polyin)
grid on
axis equal
title('ASI - Z view')
saveas(gcf,'ASI - Z view','fig')
saveas(gcf,'ASI - Z view','png')
Area_ASI(3) = area(polyin)
% Area_ASI(3) = polyArea3d([xi,yi,zeros(length(xi),1)],[0 0 1]);
close all

% function a = polyArea3d(v, normal)
% 
% nv = size(v, 1);
% v0 = repmat(v(1,:), nv, 1);
% products = sum(cross(v-v0, v([2:end 1], :)-v0, 2), 1);
% a = abs(dot(products, normal, 2))/2;
