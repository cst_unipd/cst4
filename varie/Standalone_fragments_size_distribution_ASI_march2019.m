% Standalone for fragments size distribution

clear all
close all
clc

%% Load Data CST

name_file_result_ASI_Lateral_on_central_body_v1='E:\AO_8507Catastrophic\CST2_ESTEC\CST4_ASI_Lateral_on_central_body\Results\ASI_SatelliteSP_Lateral_on_central_body_v1_20190321T173343\data\step_3240.mat';
name_file_result_ASI_Edge_on_central_body_v1 = 'E:\AO_8507Catastrophic\CST2_ESTEC\CST4_ASI_Edge_on_central_body\Results\ASI_SatelliteSP_Edge_on_central_body_v1_20190321T095850\data\step_1000.mat';
name_file_result_ASI_Central_on_solar_panel_v1='E:\AO_8507Catastrophic\CST2_ESTEC\CST4_ASI_Central_on_solar_panel\Results\ASI_SatelliteSP_Central_on_solar_panel_v1_20190321T095757\data\step_394.mat';
name_file_result_ASI_Lateral_on_solar_panel_v1='E:\AO_8507Catastrophic\CST2_ESTEC\CST4_ASI_Lateral_on_solar_panel\Results\ASI_SatelliteSP_Lateral_on_solar_panel_v1_20190321T214458\data\step_3238.mat';
name_file_result_ASI_Lateral_on_central_body_v2='E:\AO_8507Catastrophic\CST2_ESTEC\CST4_ASI_Lateral_on_central_body\Results\ASI_SatelliteSP_Lateral_on_central_body_v1_20190321T173343\data\step_3240.mat';
name_file_result_ASI_Edge_on_central_body_v2 = 'E:\AO_8507Catastrophic\CST2_ESTEC\CST4_ASI_Edge_on_central_body\Results\ASI_SatelliteSP_Edge_on_central_body_v1_20190321T095850\data\step_1000.mat';
name_file_result_ASI_Central_on_solar_panel_v2='E:\AO_8507Catastrophic\CST2_ESTEC\CST4_ASI_Central_on_solar_panel\Results\ASI_SatelliteSP_Central_on_solar_panel_v1_20190321T095757\data\step_394.mat';
name_file_result_ASI_Lateral_on_solar_panel_v2='E:\AO_8507Catastrophic\CST2_ESTEC\CST4_ASI_Lateral_on_solar_panel\Results\ASI_SatelliteSP_Lateral_on_solar_panel_v2_20190321T084802\data\step_100.mat';

title_fig=datestr(now,'dd-mm-yyyy');


%% processing ASI_Lateral_on_central_body_v1 scenario data
load(name_file_result_ASI_Lateral_on_central_body_v1,'FRAGMENTS','ME','BUBBLE','PROPAGATION_THRESHOLD','link_data','tf','RunTime')
% load material list
name_mat=[name_file_result_ASI_Lateral_on_central_body_v1(1:strfind(name_file_result_ASI_Lateral_on_central_body_v1,'Results')-1),filesep,'set_up',filesep,'material_list.m'];
run(name_mat)

% compute mass and velocity 
mass_ASI=0;
mass_imp_ASI=0;
length_target_ASI=66;

for i = 1:length_target_ASI
    mass_ASI = mass_ASI + ME(i).GEOMETRY_DATA.mass0;
end

for i = length_target_ASI+1:length(ME)
    mass_imp_ASI = mass_imp_ASI + ME(i).GEOMETRY_DATA.mass0;
end

v1 = ME(length(ME)).DYNAMICS_INITIAL_DATA.vel0/1000; % v1 in km/s

% mass interaction
[m_tot_ASI_Lateral_on_central_body_v1,volume_tot_ASI_Lateral_on_central_body_v1]=ASI_mass_intersection(length_target_ASI+1,ME,'ASI_Lateral_on_central_body_v1');

% area interaction
% [Area_ASI]=Proj_Area_ASI(length_target_ASI+1,ME);
load('Area_ASI_proj.mat','Area_ASI')
Area_imp_ASI_Lateral_on_central_body_v1=[0.1*0.1,0.1*0.1,0.1*0.1]'; 
m_ASI_Lateral_on_central_body_v1=mass_ASI*mean(Area_imp_ASI_Lateral_on_central_body_v1)/mean(Area_ASI);

[n_frags_bubbles_ASI_Lateral_on_central_body_v1,s_ASI_Lateral_on_central_body_v1,N_ASI_Lateral_on_central_body_v1,Am_CST_ASI_Lateral_on_central_body_v1,delta_v_CST_ASI_Lateral_on_central_body_v1]=postprocess_data(FRAGMENTS,ME,BUBBLE,PROPAGATION_THRESHOLD,link_data,length_target_ASI);

tf_ASI_Lateral_on_central_body_v1 = sprintf('%25.4f',tf);
if exist('RunTime','var')
    cputime_ASI_Lateral_on_central_body_v1 = sprintf('%20.2f',RunTime/60);
else
    cputime_ASI_Lateral_on_central_body_v1 = '                   -';
end
final_step_ASI_Lateral_on_central_body_v1 = sprintf('%24d',str2double(name_file_result_ASI_Lateral_on_central_body_v1(strfind(name_file_result_ASI_Lateral_on_central_body_v1,'step_')+5:end-4)));
EMR_ASI_v1 = sprintf('%5.2f',1/2*mass_imp_ASI*norm(v1*1000)^2/(mass_imp_ASI+mass_ASI)/1000);
clear FRAGMENTS ME BUBBLE PROPAGATION_THRESHOLD link_data tf RunTime ME

%% processing ASI_Edge_on_central_body_v1 scenario data
load(name_file_result_ASI_Edge_on_central_body_v1,'FRAGMENTS','ME','BUBBLE','PROPAGATION_THRESHOLD','link_data','tf','RunTime')

[n_frags_bubbles_ASI_Edge_on_central_body_v1,s_ASI_Edge_on_central_body_v1,N_ASI_Edge_on_central_body_v1,Am_CST_ASI_Edge_on_central_body_v1,delta_v_CST_ASI_Edge_on_central_body_v1]=postprocess_data(FRAGMENTS,ME,BUBBLE,PROPAGATION_THRESHOLD,link_data,length_target_ASI);

% mass interaction
[m_tot_ASI_Edge_on_central_body_v1,volume_tot_ASI_Edge_on_central_body_v1]=ASI_mass_intersection(length_target_ASI+1,ME,'ASI_Edge_on_central_body_v1');
% area interaction
Area_imp_ASI_Edge_on_central_body_v1=[0.1*0.1,0.1*0.1,0.1*0.1]'; 
m_ASI_Edge_on_central_body_v1=mass_ASI*mean(Area_imp_ASI_Edge_on_central_body_v1)/mean(Area_ASI);

tf_ASI_Edge_on_central_body_v1 = sprintf('%25.4f',tf);
if exist('RunTime','var')
    cputime_ASI_Edge_on_central_body_v1 = sprintf('%20.2f',RunTime/60);
else
    cputime_ASI_Edge_on_central_body_v1 = '                   -';
end

final_step_ASI_Edge_on_central_body_v1 = sprintf('%24d',str2double(name_file_result_ASI_Edge_on_central_body_v1(strfind(name_file_result_ASI_Edge_on_central_body_v1,'step_')+5:end-4)));
clear FRAGMENTS ME BUBBLE PROPAGATION_THRESHOLD link_data tf RunTime ME

%% processing ASI_Central_on_solar_panel_v1 scenario data

load(name_file_result_ASI_Central_on_solar_panel_v1,'FRAGMENTS','ME','BUBBLE','PROPAGATION_THRESHOLD','link_data','tf','RunTime')

[n_frags_bubbles_ASI_Central_on_solar_panel_v1,s_ASI_Central_on_solar_panel_v1,N_ASI_Central_on_solar_panel_v1,Am_CST_ASI_Central_on_solar_panel_v1,delta_v_CST_ASI_Central_on_solar_panel_v1]=postprocess_data(FRAGMENTS,ME,BUBBLE,PROPAGATION_THRESHOLD,link_data,length_target_ASI);

% mass interaction
[m_tot_ASI_Central_on_solar_panel_v1,volume_tot_ASI_Central_on_solar_panel_v1]=ASI_mass_intersection(length_target_ASI+1,ME,'ASI_Central_on_solar_panel_v1');
% area interaction
Area_imp_ASI_Central_on_solar_panel_v1=[0.1*0.1,0.1*0.1,0.1*0.1]'; 
m_ASI_Central_on_solar_panel_v1=mass_ASI*mean(Area_imp_ASI_Central_on_solar_panel_v1)/mean(Area_ASI);

tf_ASI_Central_on_solar_panel_v1 = sprintf('%25.4f',tf);
if exist('RunTime','var')
    cputime_ASI_Central_on_solar_panel_v1 = sprintf('%20.2f',RunTime/60);
else
    cputime_ASI_Central_on_solar_panel_v1 = '                   -';
end

final_step_ASI_Central_on_solar_panel_v1 = sprintf('%24d',str2double(name_file_result_ASI_Central_on_solar_panel_v1(strfind(name_file_result_ASI_Central_on_solar_panel_v1,'step_')+5:end-4)));
clear FRAGMENTS BUBBLE PROPAGATION_THRESHOLD link_data tf RunTime ME

%% processing ASI_Lateral_on_solar_panel_v1 scenario data

load(name_file_result_ASI_Lateral_on_solar_panel_v1,'FRAGMENTS','ME','BUBBLE','PROPAGATION_THRESHOLD','link_data','tf','RunTime')

[n_frags_bubbles_ASI_Lateral_on_solar_panel_v1,s_ASI_Lateral_on_solar_panel_v1,N_ASI_Lateral_on_solar_panel_v1,Am_CST_ASI_Lateral_on_solar_panel_v1,delta_v_CST_ASI_Lateral_on_solar_panel_v1]=postprocess_data(FRAGMENTS,ME,BUBBLE,PROPAGATION_THRESHOLD,link_data,length_target_ASI);

% mass interaction
[m_tot_ASI_Lateral_on_solar_panel_v1,volume_tot_ASI_Lateral_on_solar_panel_v1]=ASI_mass_intersection(length_target_ASI+1,ME,'ASI_Lateral_on_solar_panel_v1');
% area interaction
Area_imp_ASI_Lateral_on_solar_panel_v1=[0.1*0.1,0.1*0.1,0.1*0.1]'; 
m_ASI_Lateral_on_solar_panel_v1=mass_ASI*mean(Area_imp_ASI_Lateral_on_solar_panel_v1)/mean(Area_ASI);

tf_ASI_Lateral_on_solar_panel_v1 = sprintf('%25.4f',tf);
if exist('RunTime','var')
    cputime_ASI_Lateral_on_solar_panel_v1 = sprintf('%20.2f',RunTime/60);
else
    cputime_ASI_Lateral_on_solar_panel_v1 = '                   -';
end

final_step_ASI_Lateral_on_solar_panel_v1 = sprintf('%24d',str2double(name_file_result_ASI_Lateral_on_solar_panel_v1(strfind(name_file_result_ASI_Lateral_on_solar_panel_v1,'step_')+5:end-4)));
clear FRAGMENTS BUBBLE PROPAGATION_THRESHOLD link_data tf RunTime ME

%% processing ASI_Lateral_on_central_body_v2 scenario data
load(name_file_result_ASI_Lateral_on_central_body_v2,'FRAGMENTS','ME','BUBBLE','PROPAGATION_THRESHOLD','link_data','tf','RunTime')

[n_frags_bubbles_ASI_Lateral_on_central_body_v2,s_ASI_Lateral_on_central_body_v2,N_ASI_Lateral_on_central_body_v2,Am_CST_ASI_Lateral_on_central_body_v2,delta_v_CST_ASI_Lateral_on_central_body_v2]=postprocess_data(FRAGMENTS,ME,BUBBLE,PROPAGATION_THRESHOLD,link_data,length_target_ASI);

v2 = ME(length(ME)).DYNAMICS_INITIAL_DATA.vel0/1000; % v2 in km/s

% mass interaction
[m_tot_ASI_Lateral_on_central_body_v2,volume_tot_ASI_Lateral_on_central_body_v2]=ASI_mass_intersection(length_target_ASI+1,ME,'ASI_Lateral_on_central_body_v2');
% area interaction
Area_imp_ASI_Lateral_on_central_body_v2=[0.1*0.1,0.1*0.1,0.1*0.1]'; 
m_ASI_Lateral_on_central_body_v2=mass_ASI*mean(Area_imp_ASI_Lateral_on_central_body_v2)/mean(Area_ASI);


tf_ASI_Lateral_on_central_body_v2 = sprintf('%25.4f',tf);
if exist('RunTime','var')
    cputime_ASI_Lateral_on_central_body_v2 = sprintf('%20.2f',RunTime/60);
else
    cputime_ASI_Lateral_on_central_body_v2 = '                   -';
end

final_step_ASI_Lateral_on_central_body_v2 = sprintf('%24d',str2double(name_file_result_ASI_Lateral_on_central_body_v2(strfind(name_file_result_ASI_Lateral_on_central_body_v2,'step_')+5:end-4)));
EMR_ASI_v2 = sprintf('%5.2f',1/2*mass_imp_ASI*norm(v2*1000)^2/(mass_imp_ASI+mass_ASI)/1000);
clear FRAGMENTS ME BUBBLE PROPAGATION_THRESHOLD link_data tf RunTime ME

%% processing ASI_Edge_on_central_body_v2 scenario data
load(name_file_result_ASI_Edge_on_central_body_v2,'FRAGMENTS','ME','BUBBLE','PROPAGATION_THRESHOLD','link_data','tf','RunTime')

[n_frags_bubbles_ASI_Edge_on_central_body_v2,s_ASI_Edge_on_central_body_v2,N_ASI_Edge_on_central_body_v2,Am_CST_ASI_Edge_on_central_body_v2,delta_v_CST_ASI_Edge_on_central_body_v2]=postprocess_data(FRAGMENTS,ME,BUBBLE,PROPAGATION_THRESHOLD,link_data,length_target_ASI);

% mass interaction
[m_tot_ASI_Edge_on_central_body_v2,volume_tot_ASI_Edge_on_central_body_v2]=ASI_mass_intersection(length_target_ASI+1,ME,'ASI_Edge_on_central_body_v2');
% area interaction
Area_imp_ASI_Edge_on_central_body_v2=[0.1*0.1,0.1*0.1,0.1*0.1]'; 
m_ASI_Edge_on_central_body_v2=mass_ASI*mean(Area_imp_ASI_Edge_on_central_body_v2)/mean(Area_ASI);

tf_ASI_Edge_on_central_body_v2 = sprintf('%25.4f',tf);
if exist('RunTime','var')
    cputime_ASI_Edge_on_central_body_v2 = sprintf('%20.2f',RunTime/60);
else
    cputime_ASI_Edge_on_central_body_v2 = '                   -';
end

final_step_ASI_Edge_on_central_body_v2 = sprintf('%24d',str2double(name_file_result_ASI_Edge_on_central_body_v2(strfind(name_file_result_ASI_Edge_on_central_body_v2,'step_')+5:end-4)));
clear FRAGMENTS ME BUBBLE PROPAGATION_THRESHOLD link_data tf RunTime ME

%% processing ASI_Central_on_solar_panel_v2 scenario data

load(name_file_result_ASI_Central_on_solar_panel_v2,'FRAGMENTS','ME','BUBBLE','PROPAGATION_THRESHOLD','link_data','tf','RunTime')

[n_frags_bubbles_ASI_Central_on_solar_panel_v2,s_ASI_Central_on_solar_panel_v2,N_ASI_Central_on_solar_panel_v2,Am_CST_ASI_Central_on_solar_panel_v2,delta_v_CST_ASI_Central_on_solar_panel_v2]=postprocess_data(FRAGMENTS,ME,BUBBLE,PROPAGATION_THRESHOLD,link_data,length_target_ASI);

% mass interaction
[m_tot_ASI_Central_on_solar_panel_v2,volume_tot_ASI_Central_on_solar_panel_v2]=ASI_mass_intersection(length_target_ASI+1,ME,'ASI_Central_on_solar_panel_v2');
% area interaction
Area_imp_ASI_Central_on_solar_panel_v2=[0.1*0.1,0.1*0.1,0.1*0.1]'; 
m_ASI_Central_on_solar_panel_v2=mass_ASI*mean(Area_imp_ASI_Central_on_solar_panel_v2)/mean(Area_ASI);

tf_ASI_Central_on_solar_panel_v2 = sprintf('%25.4f',tf);
if exist('RunTime','var')
    cputime_ASI_Central_on_solar_panel_v2 = sprintf('%20.2f',RunTime/60);
else
    cputime_ASI_Central_on_solar_panel_v2 = '                   -';
end

final_step_ASI_Central_on_solar_panel_v2 = sprintf('%24d',str2double(name_file_result_ASI_Central_on_solar_panel_v2(strfind(name_file_result_ASI_Central_on_solar_panel_v2,'step_')+5:end-4)));
clear FRAGMENTS BUBBLE PROPAGATION_THRESHOLD link_data tf RunTime ME

%% processing ASI_Lateral_on_solar_panel_v2 scenario data

load(name_file_result_ASI_Lateral_on_solar_panel_v2,'FRAGMENTS','ME','BUBBLE','PROPAGATION_THRESHOLD','link_data','tf','RunTime')

[n_frags_bubbles_ASI_Lateral_on_solar_panel_v2,s_ASI_Lateral_on_solar_panel_v2,N_ASI_Lateral_on_solar_panel_v2,Am_CST_ASI_Lateral_on_solar_panel_v2,delta_v_CST_ASI_Lateral_on_solar_panel_v2]=postprocess_data(FRAGMENTS,ME,BUBBLE,PROPAGATION_THRESHOLD,link_data,length_target_ASI);

% mass interaction
[m_tot_ASI_Lateral_on_solar_panel_v2,volume_tot_ASI_Lateral_on_solar_panel_v2]=ASI_mass_intersection(length_target_ASI+1,ME,'ASI_Lateral_on_solar_panel_v2');
% area interaction
Area_imp_ASI_Lateral_on_solar_panel_v2=[0.1*0.1,0.1*0.1,0.1*0.1]'; 
m_ASI_Lateral_on_solar_panel_v2=mass_ASI*mean(Area_imp_ASI_Lateral_on_solar_panel_v2)/mean(Area_ASI);

tf_ASI_Lateral_on_solar_panel_v2 = sprintf('%25.4f',tf);
if exist('RunTime','var')
    cputime_ASI_Lateral_on_solar_panel_v2 = sprintf('%20.2f',RunTime/60);
else
    cputime_ASI_Lateral_on_solar_panel_v2 = '                   -';
end

final_step_ASI_Lateral_on_solar_panel_v2 = sprintf('%24d',str2double(name_file_result_ASI_Lateral_on_solar_panel_v2(strfind(name_file_result_ASI_Lateral_on_solar_panel_v2,'step_')+5:end-4)));
clear FRAGMENTS BUBBLE PROPAGATION_THRESHOLD link_data tf RunTime

%% plot finale Lc
%{d
colorOrder =  [ ...
        0            0            0       ;...% 6 BLACK
        0            0            0.7       ;...% 1 BLUE
        0            0.7            0       ;...% 3 GREEN (pale)
        0.7            0            0.7       ;...% 5 MAGENTA (pale)
        0            0.8            0.8       ;...% 4 CYAN
        0.6          0.5          0.4     ;  % 15 BROWN (dark)
        1            1            0       ;...% 7 YELLOW (pale)
        0            0.75         0.75    ;...% 8 TURQUOISE
        0            0.5          0       ;...% 9 GREEN (dark)
        0.75         0.75         0       ;...% 10 YELLOW (dark)
        1            0.50         0.25    ;...% 11 ORANGE
        0.75         0            0.75    ;...% 12 MAGENTA (dark)
        0.7          0.7          0.7     ;...% 13 GREY
        0.8          0.7          0.6     ;...% 14 BROWN (pale)
        0.4          0.7          0.6     ;...
        0.4          0          0.6     ;...
        0.4          0.6          0     ;...
        0.6          0.5          0.4 ];  % 18 BROWN (dark)
    
    %}
fontsizexy=18; 
fontsizetitle=24;
%% Plot Lc ASI
min_LC=min([s_ASI_Lateral_on_central_body_v1(1,:),s_ASI_Edge_on_central_body_v1(1,:),...
    s_ASI_Central_on_solar_panel_v1(1,:),s_ASI_Lateral_on_solar_panel_v1(1,:),...
    s_ASI_Lateral_on_central_body_v2(1,:),s_ASI_Edge_on_central_body_v2(1,:),...
    s_ASI_Central_on_solar_panel_v2(1,:),s_ASI_Lateral_on_solar_panel_v2(1,:)]);
max_LC=max([s_ASI_Lateral_on_central_body_v1(1,:),s_ASI_Edge_on_central_body_v1(1,:),...
    s_ASI_Central_on_solar_panel_v1(1,:),s_ASI_Lateral_on_solar_panel_v1(1,:),...
    s_ASI_Lateral_on_central_body_v2(1,:),s_ASI_Edge_on_central_body_v2(1,:),...
    s_ASI_Central_on_solar_panel_v2(1,:),s_ASI_Lateral_on_solar_panel_v2(1,:)]);

Lc_NASA=[min_LC:(max_LC-min_LC)/100:max_LC,1];
m_tot=0;
for ii = 1:length(ME)
    m_tot=m_tot+ME(ii).GEOMETRY_DATA.mass0;
end
NASA_SBM_cat = 0.1*(m_tot)^0.75*Lc_NASA.^(-1.71);
NASA_SBM_sub_v1 = 0.1*(mass_imp_ASI*norm(v1))^0.75*Lc_NASA.^(-1.71);
NASA_SBM_sub_v2 = 0.1*(mass_imp_ASI*norm(v2))^0.75*Lc_NASA.^(-1.71);

xx_lim = [min_LC, max_LC];
yy_lim = [1,max(NASA_SBM_cat)];

figure('units','normalized','outerposition',[0 0 1 1],'Visible', 'on')
loglog(s_ASI_Lateral_on_central_body_v1(1,:),[N_ASI_Lateral_on_central_body_v1(1,1)+n_frags_bubbles_ASI_Lateral_on_central_body_v1(1),N_ASI_Lateral_on_central_body_v1(1,2:end)],'MarkerEdgeColor',colorOrder(11,:),'MarkerFaceColor',colorOrder(11,:),'Marker','o','LineStyle','none','LineWidth',2);
grid on
hold on
loglog(s_ASI_Edge_on_central_body_v1(1,:),[N_ASI_Edge_on_central_body_v1(1,1)+n_frags_bubbles_ASI_Edge_on_central_body_v1(1),N_ASI_Edge_on_central_body_v1(1,2:end)],'MarkerEdgeColor',colorOrder(5,:),'MarkerFaceColor',colorOrder(5,:),'Marker','^','LineStyle','none','LineWidth',2);
hold on
loglog(s_ASI_Central_on_solar_panel_v1(1,:),[N_ASI_Central_on_solar_panel_v1(1,1)+n_frags_bubbles_ASI_Central_on_solar_panel_v1(1),N_ASI_Central_on_solar_panel_v1(1,2:end)],'MarkerEdgeColor',colorOrder(7,:),'MarkerFaceColor',colorOrder(7,:),'Marker','+','LineStyle','none','LineWidth',2);
hold on
loglog(s_ASI_Lateral_on_solar_panel_v1(1,:),[N_ASI_Lateral_on_solar_panel_v1(1,1)+n_frags_bubbles_ASI_Lateral_on_solar_panel_v1(1),N_ASI_Lateral_on_solar_panel_v1(1,2:end)],'MarkerEdgeColor',colorOrder(3,:),'MarkerFaceColor',colorOrder(3,:),'Marker','s','LineStyle','none','LineWidth',2);
hold on
loglog(s_ASI_Lateral_on_central_body_v2(1,:),[N_ASI_Lateral_on_central_body_v2(1,1)+n_frags_bubbles_ASI_Lateral_on_central_body_v2(1),N_ASI_Lateral_on_central_body_v2(1,2:end)],'LineStyle','-','Color',colorOrder(11,:),'LineWidth',2);
hold on
loglog(s_ASI_Edge_on_central_body_v2(1,:),[N_ASI_Edge_on_central_body_v2(1,1)+n_frags_bubbles_ASI_Edge_on_central_body_v2(1),N_ASI_Edge_on_central_body_v2(1,2:end)],'Color',colorOrder(5,:),'LineStyle','-','LineWidth',2);
hold on
loglog(s_ASI_Central_on_solar_panel_v2(1,:),[N_ASI_Central_on_solar_panel_v2(1,1)+n_frags_bubbles_ASI_Central_on_solar_panel_v2(1),N_ASI_Central_on_solar_panel_v2(1,2:end)],'Color',colorOrder(7,:),'LineStyle','-','LineWidth',2);
hold on
loglog(s_ASI_Lateral_on_solar_panel_v2(1,:),[N_ASI_Lateral_on_solar_panel_v2(1,1)+n_frags_bubbles_ASI_Lateral_on_solar_panel_v2(1),N_ASI_Lateral_on_solar_panel_v2(1,2:end)],'Color',colorOrder(3,:),'LineStyle','-','LineWidth',2);
hold on
set(gca,'FontName','Helvetica','FontSize',fontsizexy,'FontWeight','bold')
loglog(Lc_NASA,NASA_SBM_sub_v1,':r','LineWidth',2)
loglog(Lc_NASA,NASA_SBM_sub_v2,'--b','LineWidth',2)
loglog(Lc_NASA,NASA_SBM_cat,'-k','LineWidth',2)

lgd=legend('Lateral on central body (v1)','Edge on central body (v1)','Central on solar panel (v1)','Lateral on solar panel (v1)',...
    'Lateral on central body (v2)','Edge on central body (v2)','Central on solar panel (v2)','Lateral on solar panel (v2)',...
    'NASA SBM - SubCat v1','NASA SBM - SubCat v2','NASA SBM - Cat'); 
lgd.FontName='Helvetica';
lgd.FontSize=18;
lgd.FontWeight='bold';
xlabel('L_c [m]','FontName','Helvetica','FontSize',18,'FontWeight','bold')
ylabel('Cumulative number CN','FontName','Helvetica','FontSize',18,'FontWeight','bold')
title('CN(L_c) curves ','FontName','Helvetica','FontSize',fontsizetitle,'FontWeight','bold')

hold on
grid on
set(gca,'XLim',xx_lim)
set(gca,'YLim',yy_lim)
out_fig = ['ASI_all_',title_fig];
saveas(gcf,out_fig,'fig')
saveas(gcf,out_fig,'png')

%% Plot Lc ASI_Lateral_on_central_body_v1 modified CST
clear lgd
NASA_SBM_ASI_Lateral_on_central_body_v1_cat_mod= 0.1*(mass_imp_ASI+m_tot_ASI_Lateral_on_central_body_v1)^0.75*Lc_NASA.^(-1.71); 
figure('units','normalized','outerposition',[0 0 1 1],'Visible', 'on')
loglog(s_ASI_Lateral_on_central_body_v1(1,:),[N_ASI_Lateral_on_central_body_v1(1,1)+n_frags_bubbles_ASI_Lateral_on_central_body_v1(1),N_ASI_Lateral_on_central_body_v1(1,2:end)],'MarkerFaceColor',colorOrder(17,:),'Marker','d','LineStyle','none')
hold on
grid on
if nnz(s_ASI_Lateral_on_central_body_v1(2,:))>1
loglog(s_ASI_Lateral_on_central_body_v1(2,1:nnz(s_ASI_Lateral_on_central_body_v1(2,:))),[N_ASI_Lateral_on_central_body_v1(2,1)+n_frags_bubbles_ASI_Lateral_on_central_body_v1(2),N_ASI_Lateral_on_central_body_v1(2,2:1:nnz(s_ASI_Lateral_on_central_body_v1(2,:)))],'MarkerFaceColor',colorOrder(4,:),'Marker','^','LineStyle','none')
hold on
end
if nnz(s_ASI_Lateral_on_central_body_v1(3,:))>1
loglog(s_ASI_Lateral_on_central_body_v1(3,:),[N_ASI_Lateral_on_central_body_v1(3,1)+n_frags_bubbles_ASI_Lateral_on_central_body_v1(3),N_ASI_Lateral_on_central_body_v1(3,2:end)],'MarkerFaceColor',colorOrder(8,:),'Marker','*','LineStyle','none')
hold on
end
loglog(Lc_NASA,NASA_SBM_sub_v1,'--r','LineWidth',2)
loglog(Lc_NASA,NASA_SBM_cat,'-k','LineWidth',2)
loglog(Lc_NASA,NASA_SBM_ASI_Lateral_on_central_body_v1_cat_mod,'-b','LineWidth',2)
set(gca,'FontName','Helvetica','FontSize',fontsizexy,'FontWeight','bold')
title('CST results ASI Lateral on central body (v1) modified','FontName','Helvetica','FontSize',fontsizetitle,'FontWeight','bold')
xlabel('L_c [m]','FontName','Helvetica','FontSize',fontsizexy)
ylabel('Cumulative number CN','FontName','Helvetica','FontSize',fontsizexy)
if nnz(s_ASI_Lateral_on_central_body_v1(2,:))>1 && nnz(s_ASI_Lateral_on_central_body_v1(3,:))>1
    lgd=legend(['CST-ASI Lateral on central body (v1) (EMR = ',EMR_ASI_v1,' J/g)'],'CST-ASI Lateral on central body (v1) target','CST-ASI Lateral on central body (v1) impactor','NASA SBM - SubCat v1','NASA SBM - Cat','NASA SBM - Cat ASI Lateral on central body (v1) modified');
elseif nnz(s_ASI_Lateral_on_central_body_v1(2,:))>1 && nnz(s_ASI_Lateral_on_central_body_v1(3,:))<2    
    lgd=legend(['CST-ASI Lateral on central body (v1) (EMR = ',EMR_ASI_v1,' J/g)'],'CST-ASI Lateral on central body (v1) target','NASA SBM - SubCat v1','NASA SBM - Cat','NASA SBM - Cat ASI Lateral on central body (v1) modified');
elseif nnz(s_ASI_Lateral_on_central_body_v1(2,:))<2 && nnz(s_ASI_Lateral_on_central_body_v1(3,:))>1
    lgd=legend(['CST-ASI Lateral on central body (v1) (EMR = ',EMR_ASI_v1,' J/g)'],'CST-ASI Lateral on central body (v1) impactor','NASA SBM - SubCat v1','NASA SBM - Cat','NASA SBM - Cat ASI Lateral on central body (v1) modified');
else
    lgd=legend(['CST-ASI Lateral on central body (v1) (EMR = ',EMR_ASI_v1,' J/g)'],'NASA SBM - SubCat v1','NASA SBM - Cat','NASA SBM - Cat ASI Lateral on central body (v1) modified');
end
lgd.FontName='Helvetica';
lgd.FontSize=fontsizexy;
set(gca,'XLim',xx_lim)
set(gca,'YLim',yy_lim)

out_fig = ['ASI_Lateral_on_central_body_v1_',title_fig];
saveas(gcf,out_fig,'fig')
saveas(gcf,out_fig,'png')

%% Plot Lc ASI_Edge_on_central_body_v1 modified CST
clear lgd
NASA_SBM_ASI_Edge_on_central_body_v1_cat_mod= 0.1*(mass_imp_ASI+m_tot_ASI_Edge_on_central_body_v1)^0.75*Lc_NASA.^(-1.71); 
figure('units','normalized','outerposition',[0 0 1 1],'Visible', 'on')
loglog(s_ASI_Edge_on_central_body_v1(1,:),[N_ASI_Edge_on_central_body_v1(1,1)+n_frags_bubbles_ASI_Edge_on_central_body_v1(1),N_ASI_Edge_on_central_body_v1(1,2:end)],'MarkerFaceColor',colorOrder(17,:),'Marker','d','LineStyle','none')
hold on
grid on
if nnz(s_ASI_Edge_on_central_body_v1(2,:))>1
loglog(s_ASI_Edge_on_central_body_v1(2,1:nnz(s_ASI_Edge_on_central_body_v1(2,:))),[N_ASI_Edge_on_central_body_v1(2,1)+n_frags_bubbles_ASI_Edge_on_central_body_v1(2),N_ASI_Edge_on_central_body_v1(2,2:1:nnz(s_ASI_Edge_on_central_body_v1(2,:)))],'MarkerFaceColor',colorOrder(4,:),'Marker','^','LineStyle','none')
hold on
end
if nnz(s_ASI_Edge_on_central_body_v1(3,:))>1
loglog(s_ASI_Edge_on_central_body_v1(3,:),[N_ASI_Edge_on_central_body_v1(3,1)+n_frags_bubbles_ASI_Edge_on_central_body_v1(3),N_ASI_Edge_on_central_body_v1(3,2:end)],'MarkerFaceColor',colorOrder(8,:),'Marker','*','LineStyle','none')
hold on
end
loglog(Lc_NASA,NASA_SBM_sub_v1,'--r','LineWidth',2)
loglog(Lc_NASA,NASA_SBM_cat,'-k','LineWidth',2)
loglog(Lc_NASA,NASA_SBM_ASI_Edge_on_central_body_v1_cat_mod,'-b','LineWidth',2)
set(gca,'FontName','Helvetica','FontSize',fontsizexy,'FontWeight','bold')
title('CST results ASI Edge on central body (v1) modified','FontName','Helvetica','FontSize',fontsizetitle,'FontWeight','bold')
xlabel('L_c [m]','FontName','Helvetica','FontSize',fontsizexy)
ylabel('Cumulative number CN','FontName','Helvetica','FontSize',fontsizexy)
if nnz(s_ASI_Edge_on_central_body_v1(2,:))>1 && nnz(s_ASI_Edge_on_central_body_v1(3,:))>1
    lgd=legend(['CST-ASI Edge on central body (v1) (EMR = ',EMR_ASI_v1,' J/g)'],'CST-ASI Edge on central body (v1) target','CST-ASI Edge on central body (v1) impactor','NASA SBM - SubCat v1','NASA SBM - Cat','NASA SBM - Cat ASI Edge on central body (v1) modified');
elseif nnz(s_ASI_Edge_on_central_body_v1(2,:))>1 && nnz(s_ASI_Edge_on_central_body_v1(3,:))<2    
    lgd=legend(['CST-ASI Edge on central body (v1) (EMR = ',EMR_ASI_v1,' J/g)'],'CST-ASI Edge on central body (v1) target','NASA SBM - SubCat v1','NASA SBM - Cat','NASA SBM - Cat ASI Edge on central body (v1) modified');
elseif nnz(s_ASI_Edge_on_central_body_v1(2,:))<2 && nnz(s_ASI_Edge_on_central_body_v1(3,:))>1
    lgd=legend(['CST-ASI Edge on central body (v1) (EMR = ',EMR_ASI_v1,' J/g)'],'CST-ASI Edge on central body (v1) impactor','NASA SBM - SubCat v1','NASA SBM - Cat','NASA SBM - Cat ASI Edge on central body (v1) modified');
else
    lgd=legend(['CST-ASI Edge on central body (v1) (EMR = ',EMR_ASI_v1,' J/g)'],'NASA SBM - SubCat v1','NASA SBM - Cat','NASA SBM - Cat ASI Edge on central body (v1) modified');
end
lgd.FontName='Helvetica';
lgd.FontSize=fontsizexy;
set(gca,'XLim',xx_lim)
set(gca,'YLim',yy_lim)

out_fig = ['ASI_Edge_on_central_body_v1_',title_fig];
saveas(gcf,out_fig,'fig')
saveas(gcf,out_fig,'png')

%% Plot Lc ASI_Central_on_solar_panel_v1 modified CST
clear lgd
NASA_SBM_ASI_Central_on_solar_panel_v1_cat_mod= 0.1*(mass_imp_ASI+m_tot_ASI_Central_on_solar_panel_v1)^0.75*Lc_NASA.^(-1.71); 
figure('units','normalized','outerposition',[0 0 1 1],'Visible', 'on')
loglog(s_ASI_Central_on_solar_panel_v1(1,:),[N_ASI_Central_on_solar_panel_v1(1,1)+n_frags_bubbles_ASI_Central_on_solar_panel_v1(1),N_ASI_Central_on_solar_panel_v1(1,2:end)],'MarkerFaceColor',colorOrder(17,:),'Marker','d','LineStyle','none')
hold on
grid on
if nnz(s_ASI_Central_on_solar_panel_v1(2,:))>1
loglog(s_ASI_Central_on_solar_panel_v1(2,1:nnz(s_ASI_Central_on_solar_panel_v1(2,:))),[N_ASI_Central_on_solar_panel_v1(2,1)+n_frags_bubbles_ASI_Central_on_solar_panel_v1(2),N_ASI_Central_on_solar_panel_v1(2,2:1:nnz(s_ASI_Central_on_solar_panel_v1(2,:)))],'MarkerFaceColor',colorOrder(4,:),'Marker','^','LineStyle','none')
hold on
end
if nnz(s_ASI_Central_on_solar_panel_v1(3,:))>1
loglog(s_ASI_Central_on_solar_panel_v1(3,:),[N_ASI_Central_on_solar_panel_v1(3,1)+n_frags_bubbles_ASI_Central_on_solar_panel_v1(3),N_ASI_Central_on_solar_panel_v1(3,2:end)],'MarkerFaceColor',colorOrder(8,:),'Marker','*','LineStyle','none')
hold on
end
loglog(Lc_NASA,NASA_SBM_sub_v1,'--r','LineWidth',2)
loglog(Lc_NASA,NASA_SBM_cat,'-k','LineWidth',2)
loglog(Lc_NASA,NASA_SBM_ASI_Central_on_solar_panel_v1_cat_mod,'-b','LineWidth',2)
set(gca,'FontName','Helvetica','FontSize',fontsizexy,'FontWeight','bold')
title('CST results ASI Central on solar panel (v1) modified','FontName','Helvetica','FontSize',fontsizetitle,'FontWeight','bold')
xlabel('L_c [m]','FontName','Helvetica','FontSize',fontsizexy)
ylabel('Cumulative number CN','FontName','Helvetica','FontSize',fontsizexy)
if nnz(s_ASI_Central_on_solar_panel_v1(2,:))>1 && nnz(s_ASI_Central_on_solar_panel_v1(3,:))>1
    lgd=legend(['CST-ASI Central on solar panel (v1) (EMR = ',EMR_ASI_v1,' J/g)'],'CST-ASI Central on solar panel (v1) target','CST-ASI Central on solar panel (v1) impactor','NASA SBM - SubCat v1','NASA SBM - Cat','NASA SBM - Cat ASI Central on solar panel (v1) modified');
elseif nnz(s_ASI_Central_on_solar_panel_v1(2,:))>1 && nnz(s_ASI_Central_on_solar_panel_v1(3,:))<2    
    lgd=legend(['CST-ASI Central on solar panel (v1) (EMR = ',EMR_ASI_v1,' J/g)'],'CST-ASI Central on solar panel (v1) target','NASA SBM - SubCat v1','NASA SBM - Cat','NASA SBM - Cat ASI Central on solar panel (v1) modified');
elseif nnz(s_ASI_Central_on_solar_panel_v1(2,:))<2 && nnz(s_ASI_Central_on_solar_panel_v1(3,:))>1
    lgd=legend(['CST-ASI Central on solar panel (v1) (EMR = ',EMR_ASI_v1,' J/g)'],'CST-ASI Central on solar panel (v1) impactor','NASA SBM - SubCat v1','NASA SBM - Cat','NASA SBM - Cat ASI Central on solar panel (v1) modified');
else
    lgd=legend(['CST-ASI Central on solar panel (v1) (EMR = ',EMR_ASI_v1,' J/g)'],'NASA SBM - SubCat v1','NASA SBM - Cat','NASA SBM - Cat ASI Central on solar panel (v1) modified');
end
lgd.FontName='Helvetica';
lgd.FontSize=fontsizexy;
set(gca,'XLim',xx_lim)
set(gca,'YLim',yy_lim)

out_fig = ['ASI_Central_on_solar_panel_v1_',title_fig];
saveas(gcf,out_fig,'fig')
saveas(gcf,out_fig,'png')

%% Plot Lc ASI_Lateral_on_solar_panel_v1 modified CST
clear lgd
NASA_SBM_ASI_Lateral_on_solar_panel_v1_cat_mod= 0.1*(mass_imp_ASI+m_tot_ASI_Lateral_on_solar_panel_v1)^0.75*Lc_NASA.^(-1.71); 
figure('units','normalized','outerposition',[0 0 1 1],'Visible', 'on')
loglog(s_ASI_Lateral_on_solar_panel_v1(1,:),[N_ASI_Lateral_on_solar_panel_v1(1,1)+n_frags_bubbles_ASI_Lateral_on_solar_panel_v1(1),N_ASI_Lateral_on_solar_panel_v1(1,2:end)],'MarkerFaceColor',colorOrder(17,:),'Marker','d','LineStyle','none')
hold on
grid on
if nnz(s_ASI_Lateral_on_solar_panel_v1(2,:))>1
loglog(s_ASI_Lateral_on_solar_panel_v1(2,1:nnz(s_ASI_Lateral_on_solar_panel_v1(2,:))),[N_ASI_Lateral_on_solar_panel_v1(2,1)+n_frags_bubbles_ASI_Lateral_on_solar_panel_v1(2),N_ASI_Lateral_on_solar_panel_v1(2,2:1:nnz(s_ASI_Lateral_on_solar_panel_v1(2,:)))],'MarkerFaceColor',colorOrder(4,:),'Marker','^','LineStyle','none')
hold on
end
if nnz(s_ASI_Lateral_on_solar_panel_v1(3,:))>1
loglog(s_ASI_Lateral_on_solar_panel_v1(3,:),[N_ASI_Lateral_on_solar_panel_v1(3,1)+n_frags_bubbles_ASI_Lateral_on_solar_panel_v1(3),N_ASI_Lateral_on_solar_panel_v1(3,2:end)],'MarkerFaceColor',colorOrder(8,:),'Marker','*','LineStyle','none')
hold on
end
loglog(Lc_NASA,NASA_SBM_sub_v1,'--r','LineWidth',2)
loglog(Lc_NASA,NASA_SBM_cat,'-k','LineWidth',2)
loglog(Lc_NASA,NASA_SBM_ASI_Lateral_on_solar_panel_v1_cat_mod,'-b','LineWidth',2)
set(gca,'FontName','Helvetica','FontSize',fontsizexy,'FontWeight','bold')
title('CST results ASI Lateral on solar panel (v1) modified','FontName','Helvetica','FontSize',fontsizetitle,'FontWeight','bold')
xlabel('L_c [m]','FontName','Helvetica','FontSize',fontsizexy)
ylabel('Cumulative number CN','FontName','Helvetica','FontSize',fontsizexy)
if nnz(s_ASI_Lateral_on_solar_panel_v1(2,:))>1 && nnz(s_ASI_Lateral_on_solar_panel_v1(3,:))>1
    lgd=legend(['CST-ASI Lateral on solar panel (v1) (EMR = ',EMR_ASI_v1,' J/g)'],'CST-ASI Lateral on solar panel (v1) target','CST-ASI Lateral on solar panel (v1) impactor','NASA SBM - SubCat v1','NASA SBM - Cat','NASA SBM - Cat ASI Lateral on solar panel (v1) modified');
elseif nnz(s_ASI_Lateral_on_solar_panel_v1(2,:))>1 && nnz(s_ASI_Lateral_on_solar_panel_v1(3,:))<2    
    lgd=legend(['CST-ASI Lateral on solar panel (v1) (EMR = ',EMR_ASI_v1,' J/g)'],'CST-ASI Lateral on solar panel (v1) target','NASA SBM - SubCat v1','NASA SBM - Cat','NASA SBM - Cat ASI Lateral on solar panel (v1) modified');
elseif nnz(s_ASI_Lateral_on_solar_panel_v1(2,:))<2 && nnz(s_ASI_Lateral_on_solar_panel_v1(3,:))>1
    lgd=legend(['CST-ASI Lateral on solar panel (v1) (EMR = ',EMR_ASI_v1,' J/g)'],'CST-ASI Lateral on solar panel (v1) impactor','NASA SBM - SubCat v1','NASA SBM - Cat','NASA SBM - Cat ASI Lateral on solar panel (v1) modified');
else
    lgd=legend(['CST-ASI Lateral on solar panel (v1) (EMR = ',EMR_ASI_v1,' J/g)'],'NASA SBM - SubCat v1','NASA SBM - Cat','NASA SBM - Cat ASI Lateral on solar panel (v1) modified');
end
lgd.FontName='Helvetica';
lgd.FontSize=fontsizexy;
set(gca,'XLim',xx_lim)
set(gca,'YLim',yy_lim)

out_fig = ['ASI_Lateral_on_solar_panel_v1_',title_fig];
saveas(gcf,out_fig,'fig')
saveas(gcf,out_fig,'png')

%% Plot Lc ASI_Lateral_on_central_body_v2 modified CST
clear lgd
NASA_SBM_ASI_Lateral_on_central_body_v2_cat_mod= 0.1*(mass_imp_ASI+m_tot_ASI_Lateral_on_central_body_v2)^0.75*Lc_NASA.^(-1.71); 
figure('units','normalized','outerposition',[0 0 1 1],'Visible', 'on')
loglog(s_ASI_Lateral_on_central_body_v2(1,:),[N_ASI_Lateral_on_central_body_v2(1,1)+n_frags_bubbles_ASI_Lateral_on_central_body_v2(1),N_ASI_Lateral_on_central_body_v2(1,2:end)],'MarkerFaceColor',colorOrder(17,:),'Marker','d','LineStyle','none')
hold on
grid on
if nnz(s_ASI_Lateral_on_central_body_v2(2,:))>1
loglog(s_ASI_Lateral_on_central_body_v2(2,1:nnz(s_ASI_Lateral_on_central_body_v2(2,:))),[N_ASI_Lateral_on_central_body_v2(2,1)+n_frags_bubbles_ASI_Lateral_on_central_body_v2(2),N_ASI_Lateral_on_central_body_v2(2,2:1:nnz(s_ASI_Lateral_on_central_body_v2(2,:)))],'MarkerFaceColor',colorOrder(4,:),'Marker','^','LineStyle','none')
hold on
end
if nnz(s_ASI_Lateral_on_central_body_v2(3,:))>1
loglog(s_ASI_Lateral_on_central_body_v2(3,:),[N_ASI_Lateral_on_central_body_v2(3,1)+n_frags_bubbles_ASI_Lateral_on_central_body_v2(3),N_ASI_Lateral_on_central_body_v2(3,2:end)],'MarkerFaceColor',colorOrder(8,:),'Marker','*','LineStyle','none')
hold on
end
loglog(Lc_NASA,NASA_SBM_sub_v2,'--r','LineWidth',2)
loglog(Lc_NASA,NASA_SBM_cat,'-k','LineWidth',2)
loglog(Lc_NASA,NASA_SBM_ASI_Lateral_on_central_body_v2_cat_mod,'-b','LineWidth',2)
set(gca,'FontName','Helvetica','FontSize',fontsizexy,'FontWeight','bold')
title('CST results ASI Lateral on central body (v2) modified','FontName','Helvetica','FontSize',fontsizetitle,'FontWeight','bold')
xlabel('L_c [m]','FontName','Helvetica','FontSize',fontsizexy)
ylabel('Cumulative number CN','FontName','Helvetica','FontSize',fontsizexy)
if nnz(s_ASI_Lateral_on_central_body_v2(2,:))>1 && nnz(s_ASI_Lateral_on_central_body_v2(3,:))>1
    lgd=legend(['CST-ASI Lateral on central body (v2) (EMR = ',EMR_ASI_v2,' J/g)'],'CST-ASI Lateral on central body (v2) target','CST-ASI Lateral on central body (v2) impactor','NASA SBM - SubCat v2','NASA SBM - Cat','NASA SBM - Cat ASI Lateral on central body (v2) modified');
elseif nnz(s_ASI_Lateral_on_central_body_v2(2,:))>1 && nnz(s_ASI_Lateral_on_central_body_v2(3,:))<2    
    lgd=legend(['CST-ASI Lateral on central body (v2) (EMR = ',EMR_ASI_v2,' J/g)'],'CST-ASI Lateral on central body (v2) target','NASA SBM - SubCat v2','NASA SBM - Cat','NASA SBM - Cat ASI Lateral on central body (v2) modified');
elseif nnz(s_ASI_Lateral_on_central_body_v2(2,:))<2 && nnz(s_ASI_Lateral_on_central_body_v2(3,:))>1
    lgd=legend(['CST-ASI Lateral on central body (v2) (EMR = ',EMR_ASI_v2,' J/g)'],'CST-ASI Lateral on central body (v2) impactor','NASA SBM - SubCat v2','NASA SBM - Cat','NASA SBM - Cat ASI Lateral on central body (v2) modified');
else
    lgd=legend(['CST-ASI Lateral on central body (v2) (EMR = ',EMR_ASI_v2,' J/g)'],'NASA SBM - SubCat v2','NASA SBM - Cat','NASA SBM - Cat ASI Lateral on central body (v2) modified');
end
lgd.FontName='Helvetica';
lgd.FontSize=fontsizexy;
set(gca,'XLim',xx_lim)
set(gca,'YLim',yy_lim)

out_fig = ['ASI_Lateral_on_central_body_v2_',title_fig];
saveas(gcf,out_fig,'fig')
saveas(gcf,out_fig,'png')

%% Plot Lc ASI_Edge_on_central_body_v2 modified CST
clear lgd
NASA_SBM_ASI_Edge_on_central_body_v2_cat_mod= 0.1*(mass_imp_ASI+m_tot_ASI_Edge_on_central_body_v2)^0.75*Lc_NASA.^(-1.71); 
figure('units','normalized','outerposition',[0 0 1 1],'Visible', 'on')
loglog(s_ASI_Edge_on_central_body_v2(1,:),[N_ASI_Edge_on_central_body_v2(1,1)+n_frags_bubbles_ASI_Edge_on_central_body_v2(1),N_ASI_Edge_on_central_body_v2(1,2:end)],'MarkerFaceColor',colorOrder(17,:),'Marker','d','LineStyle','none')
hold on
grid on
if nnz(s_ASI_Edge_on_central_body_v2(2,:))>1
loglog(s_ASI_Edge_on_central_body_v2(2,1:nnz(s_ASI_Edge_on_central_body_v2(2,:))),[N_ASI_Edge_on_central_body_v2(2,1)+n_frags_bubbles_ASI_Edge_on_central_body_v2(2),N_ASI_Edge_on_central_body_v2(2,2:1:nnz(s_ASI_Edge_on_central_body_v2(2,:)))],'MarkerFaceColor',colorOrder(4,:),'Marker','^','LineStyle','none')
hold on
end
if nnz(s_ASI_Edge_on_central_body_v2(3,:))>1
loglog(s_ASI_Edge_on_central_body_v2(3,:),[N_ASI_Edge_on_central_body_v2(3,1)+n_frags_bubbles_ASI_Edge_on_central_body_v2(3),N_ASI_Edge_on_central_body_v2(3,2:end)],'MarkerFaceColor',colorOrder(8,:),'Marker','*','LineStyle','none')
hold on
end
loglog(Lc_NASA,NASA_SBM_sub_v2,'--r','LineWidth',2)
loglog(Lc_NASA,NASA_SBM_cat,'-k','LineWidth',2)
loglog(Lc_NASA,NASA_SBM_ASI_Edge_on_central_body_v2_cat_mod,'-b','LineWidth',2)
set(gca,'FontName','Helvetica','FontSize',fontsizexy,'FontWeight','bold')
title('CST results ASI Edge on central body (v2) modified','FontName','Helvetica','FontSize',fontsizetitle,'FontWeight','bold')
xlabel('L_c [m]','FontName','Helvetica','FontSize',fontsizexy)
ylabel('Cumulative number CN','FontName','Helvetica','FontSize',fontsizexy)
if nnz(s_ASI_Edge_on_central_body_v2(2,:))>1 && nnz(s_ASI_Edge_on_central_body_v2(3,:))>1
    lgd=legend(['CST-ASI Edge on central body (v2) (EMR = ',EMR_ASI_v2,' J/g)'],'CST-ASI Edge on central body (v2) target','CST-ASI Edge on central body (v2) impactor','NASA SBM - SubCat v2','NASA SBM - Cat','NASA SBM - Cat ASI Edge on central body (v2) modified');
elseif nnz(s_ASI_Edge_on_central_body_v2(2,:))>1 && nnz(s_ASI_Edge_on_central_body_v2(3,:))<2    
    lgd=legend(['CST-ASI Edge on central body (v2) (EMR = ',EMR_ASI_v2,' J/g)'],'CST-ASI Edge on central body (v2) target','NASA SBM - SubCat v2','NASA SBM - Cat','NASA SBM - Cat ASI Edge on central body (v2) modified');
elseif nnz(s_ASI_Edge_on_central_body_v2(2,:))<2 && nnz(s_ASI_Edge_on_central_body_v2(3,:))>1
    lgd=legend(['CST-ASI Edge on central body (v2) (EMR = ',EMR_ASI_v2,' J/g)'],'CST-ASI Edge on central body (v2) impactor','NASA SBM - SubCat v2','NASA SBM - Cat','NASA SBM - Cat ASI Edge on central body (v2) modified');
else
    lgd=legend(['CST-ASI Edge on central body (v2) (EMR = ',EMR_ASI_v2,' J/g)'],'NASA SBM - SubCat v2','NASA SBM - Cat','NASA SBM - Cat ASI Edge on central body (v2) modified');
end
lgd.FontName='Helvetica';
lgd.FontSize=fontsizexy;
set(gca,'XLim',xx_lim)
set(gca,'YLim',yy_lim)

out_fig = ['ASI_Edge_on_central_body_v2_',title_fig];
saveas(gcf,out_fig,'fig')
saveas(gcf,out_fig,'png')

%% Plot Lc ASI_Central_on_solar_panel_v2 modified CST
clear lgd
NASA_SBM_ASI_Central_on_solar_panel_v2_cat_mod= 0.1*(mass_imp_ASI+m_tot_ASI_Central_on_solar_panel_v2)^0.75*Lc_NASA.^(-1.71); 
figure('units','normalized','outerposition',[0 0 1 1],'Visible', 'on')
loglog(s_ASI_Central_on_solar_panel_v2(1,:),[N_ASI_Central_on_solar_panel_v2(1,1)+n_frags_bubbles_ASI_Central_on_solar_panel_v2(1),N_ASI_Central_on_solar_panel_v2(1,2:end)],'MarkerFaceColor',colorOrder(17,:),'Marker','d','LineStyle','none')
hold on
grid on
if nnz(s_ASI_Central_on_solar_panel_v2(2,:))>1
loglog(s_ASI_Central_on_solar_panel_v2(2,1:nnz(s_ASI_Central_on_solar_panel_v2(2,:))),[N_ASI_Central_on_solar_panel_v2(2,1)+n_frags_bubbles_ASI_Central_on_solar_panel_v2(2),N_ASI_Central_on_solar_panel_v2(2,2:1:nnz(s_ASI_Central_on_solar_panel_v2(2,:)))],'MarkerFaceColor',colorOrder(4,:),'Marker','^','LineStyle','none')
hold on
end
if nnz(s_ASI_Central_on_solar_panel_v2(3,:))>1
loglog(s_ASI_Central_on_solar_panel_v2(3,:),[N_ASI_Central_on_solar_panel_v2(3,1)+n_frags_bubbles_ASI_Central_on_solar_panel_v2(3),N_ASI_Central_on_solar_panel_v2(3,2:end)],'MarkerFaceColor',colorOrder(8,:),'Marker','*','LineStyle','none')
hold on
end
loglog(Lc_NASA,NASA_SBM_sub_v2,'--r','LineWidth',2)
loglog(Lc_NASA,NASA_SBM_cat,'-k','LineWidth',2)
loglog(Lc_NASA,NASA_SBM_ASI_Central_on_solar_panel_v2_cat_mod,'-b','LineWidth',2)
set(gca,'FontName','Helvetica','FontSize',fontsizexy,'FontWeight','bold')
title('CST results ASI Central on solar panel (v2) modified','FontName','Helvetica','FontSize',fontsizetitle,'FontWeight','bold')
xlabel('L_c [m]','FontName','Helvetica','FontSize',fontsizexy)
ylabel('Cumulative number CN','FontName','Helvetica','FontSize',fontsizexy)
if nnz(s_ASI_Central_on_solar_panel_v2(2,:))>1 && nnz(s_ASI_Central_on_solar_panel_v2(3,:))>1
    lgd=legend(['CST-ASI Central on solar panel (v2) (EMR = ',EMR_ASI_v2,' J/g)'],'CST-ASI Central on solar panel (v2) target','CST-ASI Central on solar panel (v2) impactor','NASA SBM - SubCat v2','NASA SBM - Cat','NASA SBM - Cat ASI Central on solar panel (v2) modified');
elseif nnz(s_ASI_Central_on_solar_panel_v2(2,:))>1 && nnz(s_ASI_Central_on_solar_panel_v2(3,:))<2    
    lgd=legend(['CST-ASI Central on solar panel (v2) (EMR = ',EMR_ASI_v2,' J/g)'],'CST-ASI Central on solar panel (v2) target','NASA SBM - SubCat v2','NASA SBM - Cat','NASA SBM - Cat ASI Central on solar panel (v2) modified');
elseif nnz(s_ASI_Central_on_solar_panel_v2(2,:))<2 && nnz(s_ASI_Central_on_solar_panel_v2(3,:))>1
    lgd=legend(['CST-ASI Central on solar panel (v2) (EMR = ',EMR_ASI_v2,' J/g)'],'CST-ASI Central on solar panel (v2) impactor','NASA SBM - SubCat v2','NASA SBM - Cat','NASA SBM - Cat ASI Central on solar panel (v2) modified');
else
    lgd=legend(['CST-ASI Central on solar panel (v2) (EMR = ',EMR_ASI_v2,' J/g)'],'NASA SBM - SubCat v2','NASA SBM - Cat','NASA SBM - Cat ASI Central on solar panel (v2) modified');
end
lgd.FontName='Helvetica';
lgd.FontSize=fontsizexy;
set(gca,'XLim',xx_lim)
set(gca,'YLim',yy_lim)

out_fig = ['ASI_Central_on_solar_panel_v2_',title_fig];
saveas(gcf,out_fig,'fig')
saveas(gcf,out_fig,'png')

%% Plot Lc ASI_Lateral_on_solar_panel_v2 modified CST
clear lgd
NASA_SBM_ASI_Lateral_on_solar_panel_v2_cat_mod= 0.1*(mass_imp_ASI+m_tot_ASI_Lateral_on_solar_panel_v2)^0.75*Lc_NASA.^(-1.71); 
figure('units','normalized','outerposition',[0 0 1 1],'Visible', 'on')
loglog(s_ASI_Lateral_on_solar_panel_v2(1,:),[N_ASI_Lateral_on_solar_panel_v2(1,1)+n_frags_bubbles_ASI_Lateral_on_solar_panel_v2(1),N_ASI_Lateral_on_solar_panel_v2(1,2:end)],'MarkerFaceColor',colorOrder(17,:),'Marker','d','LineStyle','none')
hold on
grid on
if nnz(s_ASI_Lateral_on_solar_panel_v2(2,:))>1
loglog(s_ASI_Lateral_on_solar_panel_v2(2,1:nnz(s_ASI_Lateral_on_solar_panel_v2(2,:))),[N_ASI_Lateral_on_solar_panel_v2(2,1)+n_frags_bubbles_ASI_Lateral_on_solar_panel_v2(2),N_ASI_Lateral_on_solar_panel_v2(2,2:1:nnz(s_ASI_Lateral_on_solar_panel_v2(2,:)))],'MarkerFaceColor',colorOrder(4,:),'Marker','^','LineStyle','none')
hold on
end
if nnz(s_ASI_Lateral_on_solar_panel_v2(3,:))>1
loglog(s_ASI_Lateral_on_solar_panel_v2(3,:),[N_ASI_Lateral_on_solar_panel_v2(3,1)+n_frags_bubbles_ASI_Lateral_on_solar_panel_v2(3),N_ASI_Lateral_on_solar_panel_v2(3,2:end)],'MarkerFaceColor',colorOrder(8,:),'Marker','*','LineStyle','none')
hold on
end
loglog(Lc_NASA,NASA_SBM_sub_v2,'--r','LineWidth',2)
loglog(Lc_NASA,NASA_SBM_cat,'-k','LineWidth',2)
loglog(Lc_NASA,NASA_SBM_ASI_Lateral_on_solar_panel_v2_cat_mod,'-b','LineWidth',2)
set(gca,'FontName','Helvetica','FontSize',fontsizexy,'FontWeight','bold')
title('CST results ASI Lateral on solar panel (v2) modified','FontName','Helvetica','FontSize',fontsizetitle,'FontWeight','bold')
xlabel('L_c [m]','FontName','Helvetica','FontSize',fontsizexy)
ylabel('Cumulative number CN','FontName','Helvetica','FontSize',fontsizexy)
if nnz(s_ASI_Lateral_on_solar_panel_v2(2,:))>1 && nnz(s_ASI_Lateral_on_solar_panel_v2(3,:))>1
    lgd=legend(['CST-ASI Lateral on solar panel (v2) (EMR = ',EMR_ASI_v2,' J/g)'],'CST-ASI Lateral on solar panel (v2) target','CST-ASI Lateral on solar panel (v2) impactor','NASA SBM - SubCat v2','NASA SBM - Cat','NASA SBM - Cat ASI Lateral on solar panel (v2) modified');
elseif nnz(s_ASI_Lateral_on_solar_panel_v2(2,:))>1 && nnz(s_ASI_Lateral_on_solar_panel_v2(3,:))<2    
    lgd=legend(['CST-ASI Lateral on solar panel (v2) (EMR = ',EMR_ASI_v2,' J/g)'],'CST-ASI Lateral on solar panel (v2) target','NASA SBM - SubCat v2','NASA SBM - Cat','NASA SBM - Cat ASI Lateral on solar panel (v2) modified');
elseif nnz(s_ASI_Lateral_on_solar_panel_v2(2,:))<2 && nnz(s_ASI_Lateral_on_solar_panel_v2(3,:))>1
    lgd=legend(['CST-ASI Lateral on solar panel (v2) (EMR = ',EMR_ASI_v2,' J/g)'],'CST-ASI Lateral on solar panel (v2) impactor','NASA SBM - SubCat v2','NASA SBM - Cat','NASA SBM - Cat ASI Lateral on solar panel (v2) modified');
else
    lgd=legend(['CST-ASI Lateral on solar panel (v2) (EMR = ',EMR_ASI_v2,' J/g)'],'NASA SBM - SubCat v2','NASA SBM - Cat','NASA SBM - Cat ASI Lateral on solar panel (v2) modified');
end
lgd.FontName='Helvetica';
lgd.FontSize=fontsizexy;
set(gca,'XLim',xx_lim)
set(gca,'YLim',yy_lim)

out_fig = ['ASI_Lateral_on_solar_panel_v2_',title_fig];
saveas(gcf,out_fig,'fig')
saveas(gcf,out_fig,'png')

%{
%% plot finale LOFT1 A su M
clear DAmSOC DAmSOC_matlab DAmSOC_NASA
lambda_c_min=log10(min([s_LOFT1 s_LOFT1_EMI_config s_LOFT1_all_continuum s_LOFT1_all_continuum s_LOFT1_no_links]));
lambda_c_max=log10(max([s_LOFT1 s_LOFT1_EMI_config s_LOFT1_all_continuum s_LOFT1_all_continuum s_LOFT1_no_links]));
lambda_c=lambda_c_min:(lambda_c_max-lambda_c_min)/15:lambda_c_max;
Am_LOFT1 =[Am_CST_LOFT1 Am_CST_LOFT1_EMI_config Am_CST_LOFT1_all_continuum Am_CST_LOFT1_all_continuum Am_CST_LOFT1_no_links];
csi_min=log10(min(Am_LOFT1(Am_LOFT1>0)));
csi_max=log10(max(Am_LOFT1));
n=length(lambda_c);
csi_NASA=csi_min:(csi_max-csi_min)/(n-1):csi_max;

for i=1:1:n
        if  lambda_c(i)<=-3.5
            muSOC=-0.3;
            sigmaSOC=0.2;
            DAmSOC(i,:)=(1/(sigmaSOC*(2*pi)^0.5))*exp(-((csi_NASA-muSOC).^2)/(2*sigmaSOC^2));
            pd = makedist('Normal','mu',muSOC,'sigma',sigmaSOC);
            DAmSOC_matlab(i,:) = pdf(pd,csi_NASA);
        elseif lambda_c(i)>-3.5 && lambda_c(i)<=-1.75
            muSOC=-0.3;
            sigmaSOC=0.2+0.1333*(lambda_c(i)+3.5);
            DAmSOC(i,:)=(1/(sigmaSOC*(2*pi)^0.5))*exp(-((csi_NASA-muSOC).^2)/(2*sigmaSOC^2));
            pd = makedist('Normal','mu',muSOC,'sigma',sigmaSOC);
            DAmSOC_matlab(i,:) = pdf(pd,csi_NASA);
        elseif lambda_c(i)>-1.75 && lambda_c(i)<-1.25
            muSOC=-0.3-1.4*(lambda_c(i)+1.75);
            sigmaSOC=0.2+0.1333*(lambda_c(i)+3.5);
            DAmSOC(i,:)=(1/(sigmaSOC*(2*pi)^0.5))*exp(-((csi_NASA-muSOC).^2)/(2*sigmaSOC^2));
            pd = makedist('Normal','mu',muSOC,'sigma',sigmaSOC);
            DAmSOC_matlab(i,:) = pdf(pd,csi_NASA);
        elseif lambda_c(i)>=-1.25
            muSOC=-1.0;
            sigmaSOC=0.2+0.1333*(lambda_c(i)+3.5);
            DAmSOC(i,:)=(1/(sigmaSOC*(2*pi)^0.5))*exp(-((csi_NASA-muSOC).^2)/(2*sigmaSOC^2));
            pd = makedist('Normal','mu',muSOC,'sigma',sigmaSOC);
            DAmSOC_matlab(i,:) = pdf(pd,csi_NASA);
        end
end

for k=1:1:size(DAmSOC,2)
    DAmSOC_NASA(k)=sum(DAmSOC(:,k));
    DAmSOC_matlab_NASA(k) = sum(DAmSOC_matlab(:,k));
end

figure('units','normalized','outerposition',[0 0 1 1],'Visible', 'on')
histogram(log10(Am_CST_LOFT1),'Normalization','probability','BinWidth',0.035)
grid on
hold on
histogram(log10(Am_CST_LOFT1_EMI_config),'Normalization','probability','BinWidth',0.035)
hold on
histogram(log10(Am_CST_LOFT1_all_continuum),'Normalization','probability','BinWidth',0.035)
hold on
histogram(log10(Am_CST_LOFT1_no_links),'Normalization','probability','BinWidth',0.035)
hold on
plot(csi_NASA,DAmSOC_NASA./sum(DAmSOC_NASA),'-b','LineWidth',2)
hold on
title('Comparison among PDFs')
set(gca,'FontSize',20,'FontWeight','bold')
xlabel('log_{10}(A/m) [m^{2}/kg]')
ylabel('Relative number N(A/m)')
lgd=legend('CST-LOFT1','CST-LOFT1 in EMI configuration','CST-LOFT1 all CONTINUUM links','CST-LOFT1 no links','NASA - SBM','Location','northwest')
set(gca,'XLim',xx_lim_Am)
set(gca,'YLim',yy_lim_Am)

out_fig = ['A_su_m_1',adj_title];
saveas(gcf,out_fig,'fig')
saveas(gcf,out_fig,'png')
%}

%% plot cpu time and simulation time
if exist(['Data_Simulation_ASI_',title_fig,'.txt'],'file')
    delete(['Data_Simulation_ASI_',title_fig,'.txt'])
end
diary(['Data_Simulation_ASI_',title_fig,'.txt'])
diary on
disp('-----------------------------------------------------------------------------------------------------------')
disp('SIMULATION                                  CPUTIME (min)       SIMULATION TIME (s)             FINAL STEP')
disp(['ASI Lateral on central body (v1)     ',cputime_ASI_Lateral_on_central_body_v1,tf_ASI_Lateral_on_central_body_v1,final_step_ASI_Lateral_on_central_body_v1])
disp(['ASI Edge on central body    (v1)     ',cputime_ASI_Edge_on_central_body_v1,tf_ASI_Edge_on_central_body_v1,final_step_ASI_Edge_on_central_body_v1])
disp(['ASI Central on solar panel  (v1)     ',cputime_ASI_Central_on_solar_panel_v1,tf_ASI_Central_on_solar_panel_v1,final_step_ASI_Central_on_solar_panel_v1])
disp(['ASI Lateral on solar panel  (v1)     ',cputime_ASI_Lateral_on_solar_panel_v1,tf_ASI_Lateral_on_solar_panel_v1,final_step_ASI_Lateral_on_solar_panel_v1])
disp(['ASI Lateral on central body (v2)     ',cputime_ASI_Lateral_on_central_body_v2,tf_ASI_Lateral_on_central_body_v2,final_step_ASI_Lateral_on_central_body_v2])
disp(['ASI Edge on central body    (v2)     ',cputime_ASI_Edge_on_central_body_v2,tf_ASI_Edge_on_central_body_v2,final_step_ASI_Edge_on_central_body_v2])
disp(['ASI Central on solar panel  (v2)     ',cputime_ASI_Central_on_solar_panel_v2,tf_ASI_Central_on_solar_panel_v2,final_step_ASI_Central_on_solar_panel_v2])
disp(['ASI Lateral on solar panel  (v2)     ',cputime_ASI_Lateral_on_solar_panel_v2,tf_ASI_Lateral_on_solar_panel_v2,final_step_ASI_Lateral_on_solar_panel_v2])
disp('-----------------------------------------------------------------------------------------------------------')
diary off