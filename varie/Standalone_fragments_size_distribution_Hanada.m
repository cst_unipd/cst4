% Standalone for fragments size distribution

clear all
close all

% path_initialization();

%% load Hanada experimental data (Hanada_Lc,Hanada_mass, Hanada_Am)
% All the data are stored as a two column matrix in the form (X',Y')
Data_Hanada_CN

%% load CST Data
% load('E:\AO_8507Catastrophic\CST2_ESTEC\CST3_Hanada_settaggi_Lan\Results\Hanada_Microsat_20180920T183632\data\step_451.mat') %% error
% load('E:\AO_8507Catastrophic\CST2_ESTEC\CST3_Hanada_settaggi_Lan\Results\Hanada_Microsat_20180920T211610\data\step_394.mat')
load('E:\AO_8507Catastrophic\CST2_ESTEC\CST3_Hanada_settaggi_Lan\Results\Hanada_Microsat_20180924T102323\data\step_31.mat') %% error
% load('E:\AO_8507Catastrophic\CST2_ESTEC\CST3_Hanada_settaggi_Lan2\Results\Hanada_Microsat_20180921T103513\data\step_57.mat')

%% DELETE FRAGMENTs & MEs WITH MASS == 0 AND DEFINE DIMENSIONS
n_size=length(FRAGMENTS);
FRAG_for_Plots = FRAGMENTS;
for i=n_size:-1:1
    if FRAG_for_Plots(i).GEOMETRY_DATA.mass==0
        FRAG_for_Plots(i)=[];
    else
        box = boundingBox3d(FRAG_for_Plots(i).GEOMETRY_DATA.c_hull);
        lx = abs(box(2)-box(1));
        ly = abs(box(4)-box(3));
        lz = abs(box(6)-box(5));
        FRAG_for_Plots(i).GEOMETRY_DATA.dimensions=[lx ly lz];
        x(i)=max(FRAG_for_Plots(i).GEOMETRY_DATA.dimensions);
    end
end
n_size=length(FRAG_for_Plots);

ME_for_Plots = ME;
n_size1=length(ME_for_Plots);
for i=n_size1:-1:1
    if ME_for_Plots(i).GEOMETRY_DATA.mass==0
        ME_for_Plots(i)=[];
    end
end
n_size1=length(ME_for_Plots);

if ~isempty(ME_for_Plots)
    FRAG_for_Plots(n_size+1:n_size+n_size1)=ME_for_Plots;
end

n_size=length(FRAG_for_Plots);

%% CREATE Lc, AM RATIO AND VELOCITY DISTRIBUTIONS FROM CST RESULTS
s1 =zeros(1,n_size-1);
k=1;
for i = 2:n_size
    Lc_CST(i-1)=mean(FRAG_for_Plots(i).GEOMETRY_DATA.dimensions);
    Am_CST(i-1)=((FRAG_for_Plots(i).GEOMETRY_DATA.dimensions(1)*FRAG_for_Plots(i).GEOMETRY_DATA.dimensions(2)+...
        FRAG_for_Plots(i).GEOMETRY_DATA.dimensions(1)*FRAG_for_Plots(i).GEOMETRY_DATA.dimensions(3)+...
        FRAG_for_Plots(i).GEOMETRY_DATA.dimensions(2)*FRAG_for_Plots(i).GEOMETRY_DATA.dimensions(3))/3/FRAG_for_Plots(i).GEOMETRY_DATA.mass);
    vel_CST_wp(:,i-1)=(FRAG_for_Plots(i).DYNAMICS_DATA.vel);
    mass_CST(i-1)=FRAG_for_Plots(i).GEOMETRY_DATA.mass;
end

%% data for plot Lc and standard deviation
s=sort(Lc_CST);
n_size=length(Lc_CST);
N=n_size:-1:1;

H_1=[s',N'];
H_ref = Hanada_Lc;

% windowing
global_min = min(H_ref(:,1));
global_max = max(H_ref(:,1));
H_1_new = H_1(and(H_1(:,1)>global_min,H_1(:,1)<global_max),:);

for j=1:length(H_ref)-1
    II1=[];
    II1=find(H_1_new(:,1)>= H_ref(j,1) & H_1_new(:,1)< H_ref(j+1,1));
    if ~isempty(II1)
        for i=1:length(II1)
            CN_1(II1(i),1)= H_1_new(II1(i),1);
            CN_1(II1(i),2)= H_ref(j,2)+(H_ref(j+1,2)-H_ref(j,2))/(H_ref(j+1,1)-H_ref(j,1))*(H_1_new(II1(i),1)-H_ref(j,1));
        end
    end
end
std_dev_matlab=std((H_1_new(:,2)-CN_1(:,2))./(CN_1(:,2)));
stn_dev1=sqrt(1/(length(CN_1(:,1))-1)*sum(((H_1_new(:,2)-CN_1(:,2)).^2)./((CN_1(:,2)).^2)));
disp(['The standard deviation computed with our formula is ',num2str(stn_dev1*100,4.2),'%']);
disp(['The standard deviation computed with Matlab built-in formula is ',num2str(std_dev_matlab*100,4.2),'%']);
%% data for plot delta V
q_cm=[0 0 0]';
for i=2:n_size
    q_cm=q_cm+mass_CST(i-1)*vel_CST_wp(:,i-1);
end
v_cm = q_cm/sum(mass_CST);
for i=2:n_size
    delta_v_CST(i-1)=norm(v_cm-vel_CST_wp(:,i-1));
end

%% data for plot NASA SBM A su M
lambda_c=-3:0.01:0;
n=length(lambda_c);
csi_NASA=-2:0.12:2;
for i=1:1:n
        if  lambda_c(i)<=-3.5
            muSOC=-0.3;
            sigmaSOC=0.2;
            DAmSOC(i,:)=(1/(sigmaSOC*(2*pi)^0.5))*exp(-((csi_NASA-muSOC).^2)/(2*sigmaSOC^2));
            pd = makedist('Normal','mu',muSOC,'sigma',sigmaSOC);
            DAmSOC_matlab(i,:) = pdf(pd,csi_NASA);
        elseif lambda_c(i)>-3.5 && lambda_c(i)<=-1.75
            muSOC=-0.3;
            sigmaSOC=0.2+0.1333*(lambda_c(i)+3.5);
            DAmSOC(i,:)=(1/(sigmaSOC*(2*pi)^0.5))*exp(-((csi_NASA-muSOC).^2)/(2*sigmaSOC^2));
            pd = makedist('Normal','mu',muSOC,'sigma',sigmaSOC);
            DAmSOC_matlab(i,:) = pdf(pd,csi_NASA);
        elseif lambda_c(i)>-1.75 && lambda_c(i)<-1.25
            muSOC=-0.3-1.4*(lambda_c(i)+1.75);
            sigmaSOC=0.2+0.1333*(lambda_c(i)+3.5);
            DAmSOC(i,:)=(1/(sigmaSOC*(2*pi)^0.5))*exp(-((csi_NASA-muSOC).^2)/(2*sigmaSOC^2));
            pd = makedist('Normal','mu',muSOC,'sigma',sigmaSOC);
            DAmSOC_matlab(i,:) = pdf(pd,csi_NASA);
        elseif lambda_c(i)>=-1.25
            muSOC=-1.0;
            sigmaSOC=0.2+0.1333*(lambda_c(i)+3.5);
            DAmSOC(i,:)=(1/(sigmaSOC*(2*pi)^0.5))*exp(-((csi_NASA-muSOC).^2)/(2*sigmaSOC^2));
            pd = makedist('Normal','mu',muSOC,'sigma',sigmaSOC);
            DAmSOC_matlab(i,:) = pdf(pd,csi_NASA);
        end
end

for k=1:1:size(DAmSOC,2)
    DAmSOC_NASA(k)=sum(DAmSOC(:,k));
    DAmSOC_matlab_NASA(k) = sum(DAmSOC_matlab(:,k));
end

%% data for plot NASA SBM of Delta_v

m=30;
csi_lim=[-2 2];
csi_NASA2=csi_lim(1):abs(csi_lim(1)-csi_lim(2))/m:csi_lim(2);
muEXP=0.9*csi_NASA2+2.9;
sigmaEXP=0.4;
delta_v_lim=[0 100];
delta_v_NASA=delta_v_lim(1):abs(delta_v_lim(1)-delta_v_lim(2))/m:delta_v_lim(2);
n=length(csi_NASA2);

for i=1:1:n
    DdvCOLL_NASA(:,i)=(1/(sigmaEXP*(2*pi)^0.5))*exp(-((delta_v_NASA-muEXP(i)).^2)/(2*sigmaEXP^2));
end

for k=1:1:size(DdvCOLL_NASA,1)
    DdvCOLL_NASA_1(k)=sum(DdvCOLL_NASA(:,k));
end

%% PLOT FRAGMENT DISTRIBUTIONS CST characteristic length

NASA_SBM = 0.1*(1.3)^0.75*Hanada_Lc(:,1).^(-1.71);

h = figure('Position', [653         464        1012         500],'Visible', 'on');
loglog(s,N,'+r','LineWidth',2)
hold on
grid on
% loglog(s*4.63,N*1.67,'dm','LineWidth',2) % for cfrp
% hold on
loglog(Hanada_Lc(:,1),Hanada_Lc(:,2),'*b','LineWidth',2)
hold on
loglog(Hanada_Lc(:,1),NASA_SBM,'--k','LineWidth',2)
hold on
title('Comparison among PDFs')
set(gca,'FontSize',20,'FontWeight','bold')
xlabel('Characteristic length L_c [m]')
ylabel('CN(L_c)')
legend(['CST Distribution at t = ',num2str(tf*1.e3,4.2),' ms'],'Hanada Experimental','Nasa SBM')


ylim([0.9 10000])
xlim([0.001 1])
%% PLOT FRAGMENT DISTRIBUTIONS CST mass
mm=sort(mass_CST);

h = figure('Position', [24          77        1010         500],'Visible', 'on');
loglog(mm,N,'+r','LineWidth',2)
grid on
hold on
loglog(Hanada_mass(:,1),Hanada_mass(:,2),'*k','LineWidth',2)
hold on
title('Comparison among PDFs')
set(gca,'FontSize',20,'FontWeight','bold')
xlabel('Mass [kg]')
ylabel('CN(mass)')
legend('CST Distribution','Hanada Experimental Distribution')


ylim([0.9 10000])
xlim([0.0000001 1])

%% PLOT Area su massa

h = figure('Position', [604          45        1010         500],'Visible', 'on');
histogram(log10(Am_CST*2.36),'Normalization','probability','BinWidth',0.03) %
grid on
hold on
b=bar(log10(Hanada_Am(:,1)),Hanada_Am(:,2)./sum(Hanada_Am(:,2)),1.6,'LineWidth',1.5);
alpha(b,.3)
hold on
plot(csi_NASA,DAmSOC_NASA./sum(DAmSOC_NASA),'b','LineWidth',2)
hold on
% plot(csi_NASA,DAmSOC_matlab_NASA./sum(DAmSOC_matlab_NASA),'b','LineWidth',2)
% hold on
title('Comparison among PDFs')
set(gca,'FontSize',20,'FontWeight','bold')
xlabel('log_{10}(A/m) [m^{2}/kg]')
ylabel('CN(A/m)')
legend('CST Distribution','Hanada Experimental Distribution','Nasa SBM','Location','southoutside')

xlim([-2 2])

%% PLOT delta V

figure
histogram(delta_v_CST,'Normalization','probability','BinWidth',1)
grid on
hold on
plot(delta_v_NASA,DdvCOLL_NASA_1./sum(DdvCOLL_NASA_1),'b','LineWidth',3)
title('Comparison among PDFs')
xlabel('\DeltaV [m/s] ')
ylabel('Relative Number of Fragments per \DeltaV bin')
legend('CST Fragments Velocities','NASA SBM')
xlim([0 100])