% Standalone for fragments size distribution
clear all

name_file_result_mixed='E:\AO_8507Catastrophic\CST2_ESTEC\CST3\Results\LOFT1_20181109T121617\data\step_2000.mat';
name_file_result_continuum='E:\AO_8507Catastrophic\CST2_ESTEC\CST3_LOFT1_all_continuum\Results\LOFT1_20181112T171433\data\step_2000.mat';
name_file_result_bolted='E:\AO_8507Catastrophic\CST2_ESTEC\CST3_LOFT1_all bolted\Results\LOFT1_20181112T165507\data\step_2000.mat';

load(name_file_result_mixed,'FRAGMENTS','ME')

case_loft = name_file_result_mixed(strfind(name_file_result_mixed,'Results\')+8:strfind(name_file_result_mixed,'Results\')+12);
switch case_loft
    case 'LOFT1'
        LOFT = load('22_PDFsam_2018CSID_MSchimmerohn_SCDisrupt_crop_LOFT1_LOG.txt');
    case 'LOFT2'
        LOFT = load('22_PDFsam_2018CSID_MSchimmerohn_SCDisrupt_crop_LOFT2_LOG.txt');
    case 'LOFT3'
        LOFT=load('22_PDFsam_2018CSID_MSchimmerohn_SCDisrupt_crop_LOFT3_LOG.txt');
    case 'LOFT4'
        LOFT=load('22_PDFsam_2018CSID_MSchimmerohn_SCDisrupt_crop_LOFT4_LOG.txt');
    case 'LOFT5'
        LOFT=load('22_PDFsam_2018CSID_MSchimmerohn_SCDisrupt_crop_LOFT5_LOG.txt');
    case 'LOFT6'
        LOFT=load('22_PDFsam_2018CSID_MSchimmerohn_SCDisrupt_crop_LOFT6_LOG.txt');
end

mass_imp=0;
for i = 52:length(ME)
    mass_imp = mass_imp + ME(i).GEOMETRY_DATA.mass0;
end
clear ME

%% processing mixed link scenario data

FRAGMENTS_mixed_link=FRAGMENTS;
clear FRAGMENTS
n_size=length(FRAGMENTS_mixed_link);
for i=n_size:-1:1
    if FRAGMENTS_mixed_link(i).GEOMETRY_DATA.mass==0
        FRAGMENTS_mixed_link(i)=[];
    end
end
n_size=length(FRAGMENTS_mixed_link);

for i = 2:n_size
    Lc_CST_mixed(i-1)=mean(FRAGMENTS_mixed_link(i).GEOMETRY_DATA.dimensions);
    Am_CST_mixed(i-1)=FRAGMENTS_mixed_link(i).GEOMETRY_DATA.A_M_ratio;
    vel_CST_mixed(i-1)=norm(FRAGMENTS_mixed_link(i).DYNAMICS_DATA.vel);
    mass_CST_mixed(i-1)=FRAGMENTS_mixed_link(i).GEOMETRY_DATA.mass;
end

s_mixed=sort(Lc_CST_mixed);

if n_size==1 %patch to avoid only one fragment case
    n_size=n_size+1;
end

N_mixed=n_size-1:-1:1;

clear n_size

%% processing continuum link scenario data

load(name_file_result_continuum,'FRAGMENTS')

FRAGMENTS_continuum_link=FRAGMENTS;
clear FRAGMENTS

n_size=length(FRAGMENTS_continuum_link);
for i=n_size:-1:1
    if FRAGMENTS_continuum_link(i).GEOMETRY_DATA.mass==0
        FRAGMENTS_continuum_link(i)=[];
    end
end
n_size=length(FRAGMENTS_continuum_link);

for i = 2:n_size
    Lc_CST_continuum(i-1)=mean(FRAGMENTS_continuum_link(i).GEOMETRY_DATA.dimensions);
    Am_CST_continuum(i-1)=FRAGMENTS_continuum_link(i).GEOMETRY_DATA.A_M_ratio;
    vel_CST_continuum(i-1)=norm(FRAGMENTS_continuum_link(i).DYNAMICS_DATA.vel);
    mass_CST_continuum(i-1)=FRAGMENTS_continuum_link(i).GEOMETRY_DATA.mass;
end

s_continuum=sort(Lc_CST_continuum);

if n_size==1 %patch to avoid only one fragment case
    n_size=n_size+1;
end

N_continuum=n_size-1:-1:1;

%% processing continuum link scenario data

load(name_file_result_bolted,'FRAGMENTS')

FRAGMENTS_bolted_link=FRAGMENTS;
clear FRAGMENTS

n_size=length(FRAGMENTS_bolted_link);
for i=n_size:-1:1
    if FRAGMENTS_bolted_link(i).GEOMETRY_DATA.mass==0
        FRAGMENTS_bolted_link(i)=[];
    end
end
n_size=length(FRAGMENTS_bolted_link);

for i = 2:n_size
    Lc_CST_bolted(i-1)=mean(FRAGMENTS_bolted_link(i).GEOMETRY_DATA.dimensions);
    Am_CST_bolted(i-1)=FRAGMENTS_bolted_link(i).GEOMETRY_DATA.A_M_ratio;
    vel_CST_bolted(i-1)=norm(FRAGMENTS_bolted_link(i).DYNAMICS_DATA.vel);
    mass_CST_bolted(i-1)=FRAGMENTS_bolted_link(i).GEOMETRY_DATA.mass;
end

% PLOT FRAGMENT DISTRIBUTIONS CST
s_bolted=sort(Lc_CST_bolted);

if n_size==1 %patch to avoid only one fragment case (LO)
    n_size=n_size+1;
end

N_bolted=n_size-1:-1:1;

%% plot finale Lc

NASA_SBM = 0.1*(mass_imp)^0.75*LOFT(:,1).^(-1.71);

h = figure('Position', [100, 50, 500, 500],'Visible', 'on');
loglog(s_mixed,N_mixed,'+g','LineWidth',2)
grid on
hold on
loglog(s_continuum,N_continuum,'<b','LineWidth',2)
hold on
loglog(s_bolted,N_bolted,'*k','LineWidth',2)
hold on
loglog(LOFT(:,1),LOFT(:,2),'*r','LineWidth',2)
hold on
loglog(LOFT(:,1),NASA_SBM,'--m','LineWidth',2)
hold on
title('Comparison EMI-CST results LOFT')
xlabel('L_c [m]')
ylabel('Cumulative number N')
legend('CST simulation','CST simulation (all continuum links)','CST simulation (all bolted links)','EMI','NASA - SBM')

%% plot finale A_su_M
%{
%CST results
alpha4=0.06;
sigma4=0.2;
mu4=-0.85;
P2=alpha4*(1/(sigma4*(2*pi)^0.5))*exp(-((csi-mu4).^2)/(2*sigma4^2));

Am=logspace(-1,1);
n=length(Am);
csi2=log10(Am);
for i=1:1:n
    lambda_c=-1.5;
    muSOC=-0.3-1.4*(lambda_c+1.75);
    sigmaSOC=0.2;
    DAmSOC(i)=0.06*(1/(sigmaSOC*(2*pi)^0.5))*exp(-((csi2(i)-muSOC)^2)/(2*sigmaSOC^2));
end
%}
%% plot finale vel
