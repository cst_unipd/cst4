% Standalone for fragments size distribution
clear all
close all
clc
%% Load Data CST
name_file_result_LOFT6_q_loss_m='N:\CST3_da_64GB_17122018_LOFT8_MD_2\Results\LOFT6_Q_loss_m\data\step_3507.mat';
name_file_result_LOFT6_q_loss_q='N:\CST3\Results\LOFT6_20190128T162130\data\step_1450.mat';

%% processing LOFT6_q_loss_m scenario data
name_mat=[name_file_result_LOFT6_q_loss_m(1:strfind(name_file_result_LOFT6_q_loss_m,'Results')-1),filesep,'set_up',filesep,'material_list.m'];
run(name_mat)

load(name_file_result_LOFT6_q_loss_m,'FRAGMENTS','ME','BUBBLE','PROPAGATION_THRESHOLD','link_data','tf','RunTime')
length_LOFT_ME=51;
mass_LOFT=0;
for i = 1:length_LOFT_ME
    mass_LOFT = mass_LOFT + ME(i).GEOMETRY_DATA.mass0;
end

mass_imp_3=0;
q_imp_3 = [0 0 0]';

for i = (length_LOFT_ME+1):length(ME)
    mass_imp_3 = mass_imp_3 + ME(i).GEOMETRY_DATA.mass0;
    q_imp_3 = q_imp_3 + ME(i).GEOMETRY_DATA.mass0*ME(i).DYNAMICS_INITIAL_DATA.vel0;
end
v_p_3 = 1/mass_imp_3*q_imp_3/1000;

[n_frags_bubbles_LOFT6_q_loss_m,s_LOFT6_q_loss_m,N_LOFT6_q_loss_m,Am_CST_LOFT6_q_loss_m,delta_v_CST_LOFT6_q_loss_m]=postprocess_data(FRAGMENTS,ME,BUBBLE,PROPAGATION_THRESHOLD,link_data,length_LOFT_ME);
load('Area_LOFT_proj.mat')
[m_tot_LOFT6_q_loss_m,volume_tot_LOFT6_q_loss_m]=LOFT_mass_intersection(length_LOFT_ME+1,ME,6);
Area_imp3=[0.2*0.2,0.2*0.3,0.2*0.3]';
m_star3=mass_LOFT*mean(Area_imp3)/mean(Area_LOFT);

tf_LOFT6_q_loss_m = sprintf('%25.4f',tf);
if exist('RunTime','var')
    cputime_LOFT6_q_loss_m = sprintf('%20.2f',RunTime/60);
else
    cputime_LOFT6_q_loss_m = '                   -';
end

final_step_LOFT6_q_loss_m = sprintf('%24d',str2double(name_file_result_LOFT6_q_loss_m(strfind(name_file_result_LOFT6_q_loss_m,'step_')+5:end-4)));
EMR_LOFT6_q_loss_m = sprintf('%5.2f',1/2*mass_imp_3*norm(v_p_3*1000)^2/(mass_imp_3+mass_LOFT)/1000);
clear FRAGMENTS ME BUBBLE PROPAGATION_THRESHOLD link_data tf RunTime

%% processing LOFT6_q_loss_q scenario data

load(name_file_result_LOFT6_q_loss_q,'FRAGMENTS','ME','BUBBLE','PROPAGATION_THRESHOLD','link_data','tf','RunTime')

[n_frags_bubbles_LOFT6_q_loss_q,s_LOFT6_q_loss_q,N_LOFT6_q_loss_q,Am_CST_LOFT6_q_loss_q,delta_v_CST_LOFT6_q_loss_q]=postprocess_data(FRAGMENTS,ME,BUBBLE,PROPAGATION_THRESHOLD,link_data,length_LOFT_ME);

mass_imp_5 = mass_imp_3;

[m_tot_LOFT6_q_loss_q,volume_tot_LOFT6_q_loss_q]=LOFT_mass_intersection(length_LOFT_ME+1,ME,6);
Area_imp5=Area_imp3;
m_star5=mass_LOFT*mean(Area_imp5)/mean(Area_LOFT);

tf_LOFT6_q_loss_q = sprintf('%25.4f',tf);
if exist('RunTime','var')
    cputime_LOFT6_q_loss_q = sprintf('%20.2f',RunTime/60);
else
    cputime_LOFT6_q_loss_q = '                   -';
end

m_tot=0;
for ii = 1:length(ME)
    m_tot=m_tot+ME(ii).GEOMETRY_DATA.mass0;
end

final_step_LOFT6_q_loss_q = sprintf('%24d',str2double(name_file_result_LOFT6_q_loss_q(strfind(name_file_result_LOFT6_q_loss_q,'step_')+5:end-4)));
EMR_LOFT6_q_loss_q = EMR_LOFT6_q_loss_m;
clear FRAGMENTS ME BUBBLE PROPAGATION_THRESHOLD link_data tf RunTime

%% plot finale Lc
%{d
colorOrder =  [ ...
        0            0            0       ;...% 6 BLACK
        0            0            0.7       ;...% 1 BLUE
        0            0.7            0       ;...% 3 GREEN (pale)
        0.7            0            0.7       ;...% 5 MAGENTA (pale)
        0            0.8            0.8       ;...% 4 CYAN
        0.6          0.5          0.4     ;  % 15 BROWN (dark)
        1            1            0       ;...% 7 YELLOW (pale)
        0            0.75         0.75    ;...% 8 TURQUOISE
        0            0.5          0       ;...% 9 GREEN (dark)
        0.75         0.75         0       ;...% 10 YELLOW (dark)
        1            0.50         0.25    ;...% 11 ORANGE
        0.75         0            0.75    ;...% 12 MAGENTA (dark)
        0.7          0.7          0.7     ;...% 13 GREY
        0.8          0.7          0.6     ;...% 14 BROWN (pale)
        0.4          0.7          0.6     ;...
        0.4          0          0.6     ;...
        0.4          0.6          0     ;...
        0.6          0.5          0.4 ];  % 18 BROWN (dark)
    
fontsizexy=18;
fontsizetitle=18;

%% Plot Lc LOFT tutti
LOFT_all=[s_LOFT6_q_loss_m(1,:),s_LOFT6_q_loss_q(1,:)];
Lc_NASA=[min(LOFT_all):(max(LOFT_all)-min(LOFT_all))/100:max(LOFT_all),1];
NASA_SBM_10_sub = 0.1*(mass_imp_3*norm(v_p_3))^0.75*Lc_NASA.^(-1.71);
NASA_SBM_10_cat = 0.1*(m_tot)^0.75*Lc_NASA.^(-1.71);

xx_lim = [min(Lc_NASA) max(Lc_NASA)];
yy_lim = [1e0,1e6];


%% Plot Lc LOFT cases CST

figure('units','normalized','outerposition',[0 0 1 1],'Visible', 'on')
loglog(s_LOFT6_q_loss_m(1,:),[N_LOFT6_q_loss_m(1,1)+n_frags_bubbles_LOFT6_q_loss_m(1),N_LOFT6_q_loss_m(1,2:end)],'MarkerEdgeColor',colorOrder(11,:),'MarkerFaceColor',colorOrder(11,:),'Marker','o','LineStyle','none')
grid on
hold on
loglog(s_LOFT6_q_loss_q(1,:),[N_LOFT6_q_loss_q(1,1)+n_frags_bubbles_LOFT6_q_loss_q(1),N_LOFT6_q_loss_q(1,2:end)],'MarkerEdgeColor',colorOrder(4,:),'MarkerFaceColor',colorOrder(4,:),'Marker','<','LineStyle','none')
hold on
loglog(Lc_NASA,NASA_SBM_10_sub,'-r','LineWidth',2)
loglog(Lc_NASA,NASA_SBM_10_cat,'-k','LineWidth',2)
set(gca,'FontName','Helvetica','FontSize',fontsizexy,'FontWeight','bold')
title('CST results LOFT 6','FontName','Helvetica','FontSize',fontsizetitle,'FontWeight','bold')
xlabel('L_c [m]','FontName','Helvetica','FontSize',fontsizexy)
ylabel('Cumulative number CN','FontName','Helvetica','FontSize',fontsizexy)
lgd=legend('CST-LOFT6 Q(m) ','CST-LOFT6 Q(Q)','NASA SBM - SubCat 10 kg','NASA SBM - Cat');
lgd.FontName='Helvetica';
lgd.FontSize=fontsizexy;
set(gca,'XLim',xx_lim)
set(gca,'YLim',yy_lim)

out_fig = 'Lc_CST_baseline_data_6_q_loss';
saveas(gcf,out_fig,'fig')
saveas(gcf,out_fig,'png')
