% Standalone for fragments size distribution
clear all
close all

name_file_result_baseline_LAN='E:\AO_8507Catastrophic\CST_integration\CST3\Results\Lan_Satellite_Finale\data\step_1000.mat'; %25
name_file_result_LAN_new_OLD_conf='E:\AO_8507Catastrophic\CST_integration\CST3_LAN\Results\Lan_Satellite_20181119T150805\data\step_1603.mat';
name_file_result_LAN_new_diff_links='E:\AO_8507Catastrophic\CST_integration\CST3_LAN2\Results\Lan_Satellite_20181119T150942\data\step_1296.mat';

%% processing baseline LAN scenario data

load(name_file_result_baseline_LAN,'FRAGMENTS','ME')

n_size=length(FRAGMENTS);
for i=n_size:-1:1
    if FRAGMENTS(i).GEOMETRY_DATA.mass==0
        FRAGMENTS(i)=[];
    else
        box = boundingBox3d(FRAGMENTS(i).GEOMETRY_DATA.c_hull);
        lx = abs(box(2)-box(1));
        ly = abs(box(4)-box(3));
        lz = abs(box(6)-box(5));
        FRAGMENTS(i).GEOMETRY_DATA.dimensions=[lx ly lz];
        x(i)=max(FRAGMENTS(i).GEOMETRY_DATA.dimensions);
    end
end
n_size=length(FRAGMENTS);
FRAGMENTS1=FRAGMENTS;

mass_imp=0;
for i = 66:length(ME)
    mass_imp = mass_imp + ME(i).GEOMETRY_DATA.mass0;
    q_imp = ME(i).GEOMETRY_DATA.mass0*ME(i).DYNAMICS_INITIAL_DATA.vel0;
end
v_p = 1/mass_imp*q_imp/1000;
% clear ME

n_size1=length(ME);
for i=n_size1:-1:1
    if ME(i).GEOMETRY_DATA.mass==0
        ME(i)=[];
    end
end
n_size1=length(ME);

if ~isempty(ME)
    FRAGMENTS1(n_size+1:n_size+n_size1)=ME;
end

n_size=length(FRAGMENTS1);

s1 =zeros(1,n_size-1);
k=1;
for i = 2:n_size
    Lc_CST_LAN(i-1)=mean(FRAGMENTS1(i).GEOMETRY_DATA.dimensions);
    Am_CST_LAN(i-1)=((FRAGMENTS1(i).GEOMETRY_DATA.dimensions(1)*FRAGMENTS1(i).GEOMETRY_DATA.dimensions(2)+...
        FRAGMENTS1(i).GEOMETRY_DATA.dimensions(1)*FRAGMENTS1(i).GEOMETRY_DATA.dimensions(3)+...
        FRAGMENTS1(i).GEOMETRY_DATA.dimensions(2)*FRAGMENTS1(i).GEOMETRY_DATA.dimensions(3))/3/FRAGMENTS1(i).GEOMETRY_DATA.mass);
    vel_CST_LAN(i-1)=norm(FRAGMENTS1(i).DYNAMICS_DATA.vel);
    mass_CST_LAN(i-1)=FRAGMENTS1(i).GEOMETRY_DATA.mass;
    if FRAGMENTS1(i).object_ID~=66
       vel_CST_np(k)=norm(FRAGMENTS1(i).DYNAMICS_DATA.vel);
       k=k+1;
    end
end

s_LAN=sort(Lc_CST_LAN);

if n_size==1 %patch to avoid only one fragment case
    n_size=n_size+1;
end

N_LAN=n_size-1:-1:1;

% data for plot delta V
q_cm=[0 0 0]';
for i=2:n_size
    q_cm=q_cm+mass_CST_LAN(i-1)*vel_CST_LAN(:,i-1);
end
v_cm = q_cm/sum(mass_CST_LAN);
for i=2:n_size
    delta_v_CST_LAN(i-1)=norm(v_cm-vel_CST_LAN(:,i-1));
end

clear v_cm q_cm n_size FRAGMENTS ME

%% processing LAN OLD configuration link scenario data

load(name_file_result_LAN_new_OLD_conf,'FRAGMENTS','ME')

n_size=length(FRAGMENTS);
for i=n_size:-1:1
    if FRAGMENTS(i).GEOMETRY_DATA.mass==0
        FRAGMENTS(i)=[];
    else
        box = boundingBox3d(FRAGMENTS(i).GEOMETRY_DATA.c_hull);
        lx = abs(box(2)-box(1));
        ly = abs(box(4)-box(3));
        lz = abs(box(6)-box(5));
        FRAGMENTS(i).GEOMETRY_DATA.dimensions=[lx ly lz];
        x(i)=max(FRAGMENTS(i).GEOMETRY_DATA.dimensions);
    end
end
n_size=length(FRAGMENTS);
FRAGMENTS_LAN_OLD_conf=FRAGMENTS;
clear FRAGMENTS

n_size=length(FRAGMENTS_LAN_OLD_conf);
for i=n_size:-1:1
    if FRAGMENTS_LAN_OLD_conf(i).GEOMETRY_DATA.mass==0
        FRAGMENTS_LAN_OLD_conf(i)=[];
    end
end
n_size=length(FRAGMENTS_LAN_OLD_conf);

n_size1=length(ME);
for i=n_size1:-1:1
    if ME(i).GEOMETRY_DATA.mass==0
        ME(i)=[];
    end
end
n_size1=length(ME);

if ~isempty(ME)
    FRAGMENTS_LAN_OLD_conf(n_size+1:n_size+n_size1)=ME;
end

for i = 2:n_size
    Lc_CST_LAN_OLD_conf(i-1)=mean(FRAGMENTS_LAN_OLD_conf(i).GEOMETRY_DATA.dimensions);
    Am_CST_LAN_OLD_conf(i-1)=FRAGMENTS_LAN_OLD_conf(i).GEOMETRY_DATA.A_M_ratio;
    vel_CST_LAN_OLD_conf(i-1)=norm(FRAGMENTS_LAN_OLD_conf(i).DYNAMICS_DATA.vel);
    mass_CST_LAN_OLD_conf(i-1)=FRAGMENTS_LAN_OLD_conf(i).GEOMETRY_DATA.mass;
end

s_LAN_OLD_conf=sort(Lc_CST_LAN_OLD_conf);

if n_size==1 %patch to avoid only one fragment case
    n_size=n_size+1;
end

N_LAN_OLD_conf=n_size-1:-1:1;

% data for plot delta V
q_cm=[0 0 0]';
for i=2:n_size
    q_cm=q_cm+mass_CST_LAN_OLD_conf(i-1)*vel_CST_LAN_OLD_conf(:,i-1);
end
v_cm = q_cm/sum(mass_CST_LAN_OLD_conf);
for i=2:n_size
    delta_v_CST_LAN_OLD_conf(i-1)=norm(v_cm-vel_CST_LAN_OLD_conf(:,i-1));
end

clear v_cm q_cm n_size FRAGMENTS ME

%% processing LAN different links configuration

load(name_file_result_LAN_new_diff_links,'FRAGMENTS','ME')

n_size=length(FRAGMENTS);
for i=n_size:-1:1
    if FRAGMENTS(i).GEOMETRY_DATA.mass==0
        FRAGMENTS(i)=[];
    else
        box = boundingBox3d(FRAGMENTS(i).GEOMETRY_DATA.c_hull);
        lx = abs(box(2)-box(1));
        ly = abs(box(4)-box(3));
        lz = abs(box(6)-box(5));
        FRAGMENTS(i).GEOMETRY_DATA.dimensions=[lx ly lz];
        x(i)=max(FRAGMENTS(i).GEOMETRY_DATA.dimensions);
    end
end
n_size=length(FRAGMENTS);
FRAGMENTS_LAN_new_diff_links=FRAGMENTS;
clear FRAGMENTS

n_size=length(FRAGMENTS_LAN_new_diff_links);
for i=n_size:-1:1
    if FRAGMENTS_LAN_new_diff_links(i).GEOMETRY_DATA.mass==0
        FRAGMENTS_LAN_new_diff_links(i)=[];
    end
end
n_size=length(FRAGMENTS_LAN_new_diff_links);

n_size1=length(ME);
for i=n_size1:-1:1
    if ME(i).GEOMETRY_DATA.mass==0
        ME(i)=[];
    end
end
n_size1=length(ME);

if ~isempty(ME)
    FRAGMENTS_LAN_new_diff_links(n_size+1:n_size+n_size1)=ME;
end

for i = 2:n_size
    Lc_CST_LAN_new_diff_links(i-1)=mean(FRAGMENTS_LAN_new_diff_links(i).GEOMETRY_DATA.dimensions);
    Am_CST_LAN_new_diff_links(i-1)=FRAGMENTS_LAN_new_diff_links(i).GEOMETRY_DATA.A_M_ratio;
    vel_CST_LAN_new_diff_links(i-1)=norm(FRAGMENTS_LAN_new_diff_links(i).DYNAMICS_DATA.vel);
    mass_CST_LAN_new_diff_links(i-1)=FRAGMENTS_LAN_new_diff_links(i).GEOMETRY_DATA.mass;
end

s_LAN_new_diff_links=sort(Lc_CST_LAN_new_diff_links);

if n_size==1 %patch to avoid only one fragment case
    n_size=n_size+1;
end

N_LAN_new_diff_links=n_size-1:-1:1;

% data for plot delta V
q_cm=[0 0 0]';
for i=2:n_size
    q_cm=q_cm+mass_CST_LAN_new_diff_links(i-1)*vel_CST_LAN_new_diff_links(:,i-1);
end
v_cm = q_cm/sum(mass_CST_LAN_new_diff_links);
for i=2:n_size
    delta_v_CST_LAN_new_diff_links(i-1)=norm(v_cm-vel_CST_LAN_new_diff_links(:,i-1));
end

clear v_cm q_cm n_size FRAGMENTS

%% plot finale Lc
%{s
Lc_NASA=1:-(1-5*10^-3)/100:5*10^-3;
NASA_SBM_sub = 0.1*(mass_imp*norm(v_p))^0.75*Lc_NASA.^(-1.71);
m_tot=0;
for ii = 1:length(ME)
    m_tot=m_tot+ME(ii).GEOMETRY_DATA.mass0;
end
NASA_SBM_cat = 0.1*(m_tot)^0.75*Lc_NASA.^(-1.71);

Data_Lan_CN

h = figure('Position', [45   129   906   553],'Visible', 'on');
loglog(s_LAN,N_LAN,'+g','LineWidth',2)
grid on
hold on
loglog(s_LAN_OLD_conf,N_LAN_OLD_conf,'+r','LineWidth',2)
hold on
loglog(s_LAN_new_diff_links,N_LAN_new_diff_links,'^b','LineWidth',2)
hold on
loglog(Lan_CN(:,1),Lan_CN(:,2),'-k','LineWidth',2)
hold on
loglog(Lc_NASA,NASA_SBM_sub,'--c','LineWidth',2)
hold on
loglog(Lc_NASA,NASA_SBM_cat,'-c','LineWidth',2)
hold on
set(gca,'FontSize',20,'FontWeight','bold')
title('Comparison CST results & LAN')
xlabel('L_c [m]')
ylabel('Cumulative number N')
legend('CST simulation LAN OLD','CST simulation LAN NEW with OLD links','CST simulation LAN NEW with NEW links','LAN experimental results','NASA - SubCat-SBM','NASA - Cat-SBM')
xlim([6*10^-4 1])
ylim([10^0 10^4])

%% plot finale A su M

clear DAmSOC DAmSOC_matlab DAmSOC_NASA
lambda_c_min=log10(min([s_LAN s_LAN_OLD_conf s_LAN_new_diff_links]));
lambda_c_max=log10(max([s_LAN s_LAN_OLD_conf s_LAN_new_diff_links]));
lambda_c=lambda_c_min:(lambda_c_max-lambda_c_min)/10:lambda_c_max;
csi_min=log10(min([Am_CST_LAN Am_CST_LAN_OLD_conf Am_CST_LAN_new_diff_links]));
csi_max=log10(max([Am_CST_LAN Am_CST_LAN_OLD_conf Am_CST_LAN_new_diff_links]));
n=length(lambda_c);
csi_NASA=csi_min:(csi_max-csi_min)/(n-1):csi_max;

for i=1:1:n
        if  lambda_c(i)<=-3.5
            muSOC=-0.3;
            sigmaSOC=0.2;
            DAmSOC(i,:)=(1/(sigmaSOC*(2*pi)^0.5))*exp(-((csi_NASA-muSOC).^2)/(2*sigmaSOC^2));
            pd = makedist('Normal','mu',muSOC,'sigma',sigmaSOC);
            DAmSOC_matlab(i,:) = pdf(pd,csi_NASA);
        elseif lambda_c(i)>-3.5 && lambda_c(i)<=-1.75
            muSOC=-0.3;
            sigmaSOC=0.2+0.1333*(lambda_c(i)+3.5);
            DAmSOC(i,:)=(1/(sigmaSOC*(2*pi)^0.5))*exp(-((csi_NASA-muSOC).^2)/(2*sigmaSOC^2));
            pd = makedist('Normal','mu',muSOC,'sigma',sigmaSOC);
            DAmSOC_matlab(i,:) = pdf(pd,csi_NASA);
        elseif lambda_c(i)>-1.75 && lambda_c(i)<-1.25
            muSOC=-0.3-1.4*(lambda_c(i)+1.75);
            sigmaSOC=0.2+0.1333*(lambda_c(i)+3.5);
            DAmSOC(i,:)=(1/(sigmaSOC*(2*pi)^0.5))*exp(-((csi_NASA-muSOC).^2)/(2*sigmaSOC^2));
            pd = makedist('Normal','mu',muSOC,'sigma',sigmaSOC);
            DAmSOC_matlab(i,:) = pdf(pd,csi_NASA);
        elseif lambda_c(i)>=-1.25
            muSOC=-1.0;
            sigmaSOC=0.2+0.1333*(lambda_c(i)+3.5);
            DAmSOC(i,:)=(1/(sigmaSOC*(2*pi)^0.5))*exp(-((csi_NASA-muSOC).^2)/(2*sigmaSOC^2));
            pd = makedist('Normal','mu',muSOC,'sigma',sigmaSOC);
            DAmSOC_matlab(i,:) = pdf(pd,csi_NASA);
        end
end

for k=1:1:size(DAmSOC,2)
    DAmSOC_NASA(k)=sum(DAmSOC(:,k));
    DAmSOC_matlab_NASA(k) = sum(DAmSOC_matlab(:,k));
end

%% LOAD LAN RESULTS (obtained from paper)
load('LAN_A_m_ratio.mat')
Am_LAN=10.^Y;
Lc_LAN=10.^X;

%LAN results
alpha1=0.0060;
sigma1=0.1270;
mu1=-1.180;
alpha2=0.0360;
sigma2=0.1050;
mu2=-0.695;
alpha3=0.0150;
sigma3=0.1450;
mu3=-0.295;
csi=log10(sort(Am_LAN));

P=alpha1*(1/(sigma1*(2*pi)^0.5))*exp(-((csi-mu1).^2)/(2*sigma1^2))+...
    alpha2*(1/(sigma2*(2*pi)^0.5))*exp(-((csi-mu2).^2)/(2*sigma2^2))+...
    alpha3*(1/(sigma3*(2*pi)^0.5))*exp(-((csi-mu3).^2)/(2*sigma3^2));

%{r
h = figure('Position', [604          45        1010         500],'Visible', 'on');
histogram(log10(Am_CST_LAN),'Normalization','probability','BinWidth',0.05) 
grid on
hold on
histogram(log10(Am_CST_LAN_OLD_conf),'Normalization','probability','BinWidth',0.05) 
hold on
histogram(log10(Am_CST_LAN_new_diff_links),'Normalization','probability','BinWidth',0.05) 
hold on
plot(csi_NASA,DAmSOC_NASA./sum(DAmSOC_NASA),'-b','LineWidth',2)
hold on
plot(csi,P,'-k','LineWidth',2)
hold on
title('Comparison among PDFs')
set(gca,'FontSize',20,'FontWeight','bold')
xlabel('log_{10}(A/m) [m^{2}/kg]')
ylabel('CN(A/m)')
legend('CST simulation LAN OLD','CST simulation LAN NEW with OLD links','CST simulation LAN NEW with NEW links','NASA - SBM','LAN PDF')

h = figure('Position', [604          45        1010         500],'Visible', 'on');
histogram(log10(Am_CST_LAN),'Normalization','probability','BinWidth',0.05) 
grid on
hold on
histogram(log10(Am_CST_LAN_OLD_conf),'Normalization','probability','BinWidth',0.05) 
hold on
plot(csi,P,'-k','LineWidth',2)
hold on
title('Comparison among PDFs')
set(gca,'FontSize',20,'FontWeight','bold')
xlabel('log_{10}(A/m) [m^{2}/kg]')
ylabel('CN(A/m)')
legend('CST simulation LAN OLD','CST simulation LAN NEW with OLD links','LAN PDF')

h = figure('Position', [604          45        1010         500],'Visible', 'on');
histogram(log10(Am_CST_LAN),'Normalization','probability','BinWidth',0.05) 
grid on
hold on
histogram(log10(Am_CST_LAN_new_diff_links),'Normalization','probability','BinWidth',0.05) 
hold on
plot(csi,P,'-k','LineWidth',2)
hold on
title('Comparison among PDFs')
set(gca,'FontSize',20,'FontWeight','bold')
xlabel('log_{10}(A/m) [m^{2}/kg]')
ylabel('CN(A/m)')
legend('CST simulation LAN OLD','CST simulation LAN NEW with NEW links','LAN PDF')

%}
%% plot finale of Delta_v

%{
m=length(csi_NASA);
muEXP=0.9*csi_NASA+2.9;
sigmaEXP=0.4;
vel=sort([vel_CST_LAN vel_CST_LAN_OLD_conf vel_CST_LAN_new_diff_links]);
vel(vel==0)=[];
delta_v_min=vel(1);
delta_v_max=max([vel_CST_LAN vel_CST_LAN_OLD_conf vel_CST_LAN_new_diff_links]);
delta_v_NASA=delta_v_min:abs(delta_v_min-delta_v_max)/(m-1):delta_v_max;

for i=1:1:m
    DdvCOLL_NASA(:,i)=(1/(sigmaEXP*(2*pi)^0.5))*exp(-((delta_v_NASA-muEXP(i)).^2)/(2*sigmaEXP^2));
end

for k=1:1:m
    DdvCOLL_NASA_1(k)=sum(DdvCOLL_NASA(:,k));
end

figure
histogram(delta_v_CST_LAN/1000,'Normalization','probability','BinWidth',0.03)
grid on
hold on
histogram(delta_v_CST_LAN_OLD_conf/1000,'Normalization','probability','BinWidth',0.03)
hold on
histogram(delta_v_CST_LAN_new_diff_links/1000,'Normalization','probability','BinWidth',0.03)
hold on
plot(delta_v_NASA/1000,DdvCOLL_NASA_1./sum(DdvCOLL_NASA_1),'b','LineWidth',2)
title('Comparison among PDFs')
xlabel('\DeltaV [km/s] ')
ylabel('Relative Number of Fragments per \DeltaV bin')
legend('CST simulation LAN OLD','CST simulation LAN NEW with OLD links','CST simulation LAN NEW with new links','NASA SBM')
%  xlim([0 6000])
%}
