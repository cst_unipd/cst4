% Standalone for fragments size distribution
clear all
close all
% clc

global MATERIAL_LIST

%% Load Data

name_file_result_baseline='E:\AO_8507Catastrophic\CST_integration\CST3_21_11_2018_risultati_RILEVANTI\Results\LOFT1_Baseline_BEFORE_correction_velocity_model\data\step_2000.mat';
name_file_result_continuum='E:\AO_8507Catastrophic\CST_integration\CST3_21_11_2018_risultati_RILEVANTI\Results\LOFT1_Baseline_BEFORE_correction_velocity_model_CONTINUUM_links\data\step_2000.mat';
name_file_result_bolted='E:\AO_8507Catastrophic\CST_integration\CST3_21_11_2018_risultati_RILEVANTI\Results\LOFT1_Baseline_BEFORE_correction_velocity_model_BOLTED_links\data\step_2000.mat';

%% Settings

colorOrder =  [ ...
    0            0            1       ;...% 1 BLUE   
    1            0            0       ;...% 2 RED
    0            1            0       ;...% 3 GREEN (pale)
    0            1            1       ;...% 4 CYAN
    1            0            1       ;...% 5 MAGENTA (pale)
    0.6          0.5          0.4     ;  % 6 BROWN (dark)
    0            0            0       ;...% 7 BLACK
    1            1            0       ;...% 8 YELLOW (pale)
    0            0.75         0.75    ;...% 9 TURQUOISE
    0            0.5          0       ;...% 10 GREEN (dark)
    0.75         0.75         0       ;...% 11 YELLOW (dark)
    1            0.50         0.25    ;...% 12 ORANGE
    0.75         0            0.75    ;...% 13 MAGENTA (dark)
    0.7          0.7          0.7     ;...% 14 GREY
    0.8          0.7          0.6     ;...% 15 BROWN (pale)
    0.4          0.7          0.6     ;...
    0.4          0          0.6     ;...
    0.4          0.6          0     ;...
    0.6          0.5          0.4 ];  % 15 BROWN (dark)

%% processing mixed link scenario data

load(name_file_result_baseline,'FRAGMENTS','ME','BUBBLE','PROPAGATION_THRESHOLD')
name_mat=[name_file_result_baseline(1:strfind(name_file_result_baseline,'\Results')),'set_up',filesep,'material_list.m'];
run(name_mat)

mass_imp=0;
for i = 52:length(ME)
    mass_imp = mass_imp + ME(i).GEOMETRY_DATA.mass0;
    q_imp = ME(i).GEOMETRY_DATA.mass0*ME(i).DYNAMICS_INITIAL_DATA.vel0;
end
v_p = 1/mass_imp*q_imp/1000;
% clear ME

n_size=length(FRAGMENTS);
for i=n_size:-1:1
    if FRAGMENTS(i).GEOMETRY_DATA.mass==0
        FRAGMENTS(i)=[];
    else
        box = boundingBox3d(FRAGMENTS(i).GEOMETRY_DATA.c_hull);
        lx = abs(box(2)-box(1));
        ly = abs(box(4)-box(3));
        lz = abs(box(6)-box(5));
        FRAGMENTS(i).GEOMETRY_DATA.dimensions=[lx ly lz];
        x(i)=max(FRAGMENTS(i).GEOMETRY_DATA.dimensions);
    end
end
n_size=length(FRAGMENTS);
FRAGMENTS_baseline_link=FRAGMENTS;
clear FRAGMENTS

n_size=length(FRAGMENTS_baseline_link);
for i=n_size:-1:1
    if FRAGMENTS_baseline_link(i).GEOMETRY_DATA.mass==0
        FRAGMENTS_baseline_link(i)=[];
    end
end
n_size=length(FRAGMENTS_baseline_link);

for i = 2:n_size
    Lc_CST_baseline(i-1)=mean(FRAGMENTS_baseline_link(i).GEOMETRY_DATA.dimensions);
    Am_CST_baseline(i-1)=FRAGMENTS_baseline_link(i).GEOMETRY_DATA.A_M_ratio;
    vel_CST_baseline(i-1)=norm(FRAGMENTS_baseline_link(i).DYNAMICS_DATA.vel);
    mass_CST_baseline(i-1)=FRAGMENTS_baseline_link(i).GEOMETRY_DATA.mass;
end

s_baseline=sort(Lc_CST_baseline);

if n_size==1 %patch to avoid only one fragment case
    n_size=n_size+1;
end

N_baseline=n_size-1:-1:1;

% data for plot delta V
q_cm=[0 0 0]';
for i=2:n_size
    q_cm=q_cm+mass_CST_baseline(i-1)*vel_CST_baseline(:,i-1);
end
v_cm = q_cm/sum(mass_CST_baseline);
for i=2:n_size
    delta_v_CST_baseline(i-1)=norm(v_cm-vel_CST_baseline(:,i-1));
end

clear v_cm q_cm n_size

%% processing continuum link scenario data

load(name_file_result_continuum,'FRAGMENTS')

n_size=length(FRAGMENTS);
for i=n_size:-1:1
    if FRAGMENTS(i).GEOMETRY_DATA.mass==0
        FRAGMENTS(i)=[];
    else
        box = boundingBox3d(FRAGMENTS(i).GEOMETRY_DATA.c_hull);
        lx = abs(box(2)-box(1));
        ly = abs(box(4)-box(3));
        lz = abs(box(6)-box(5));
        FRAGMENTS(i).GEOMETRY_DATA.dimensions=[lx ly lz];
        x(i)=max(FRAGMENTS(i).GEOMETRY_DATA.dimensions);
    end
end
n_size=length(FRAGMENTS);

FRAGMENTS_continuum_link=FRAGMENTS;
clear FRAGMENTS

n_size=length(FRAGMENTS_continuum_link);
for i=n_size:-1:1
    if FRAGMENTS_continuum_link(i).GEOMETRY_DATA.mass==0
        FRAGMENTS_continuum_link(i)=[];
    end
end
n_size=length(FRAGMENTS_continuum_link);

for i = 2:n_size
    Lc_CST_continuum(i-1)=mean(FRAGMENTS_continuum_link(i).GEOMETRY_DATA.dimensions);
    Am_CST_continuum(i-1)=FRAGMENTS_continuum_link(i).GEOMETRY_DATA.A_M_ratio;
    vel_CST_continuum(i-1)=norm(FRAGMENTS_continuum_link(i).DYNAMICS_DATA.vel);
    mass_CST_continuum(i-1)=FRAGMENTS_continuum_link(i).GEOMETRY_DATA.mass;
end

s_continuum=sort(Lc_CST_continuum);

if n_size==1 %patch to avoid only one fragment case
    n_size=n_size+1;
end

N_continuum=n_size-1:-1:1;

% data for plot delta V
q_cm=[0 0 0]';
for i=2:n_size
    q_cm=q_cm+mass_CST_continuum(i-1)*vel_CST_continuum(:,i-1);
end
v_cm = q_cm/sum(mass_CST_continuum);
for i=2:n_size
    delta_v_CST_continuum(i-1)=norm(v_cm-vel_CST_continuum(:,i-1));
end

clear v_cm q_cm n_size

%% processing bolted link scenario data

load(name_file_result_bolted,'FRAGMENTS')

n_size=length(FRAGMENTS);
for i=n_size:-1:1
    if FRAGMENTS(i).GEOMETRY_DATA.mass==0
        FRAGMENTS(i)=[];
    else
        box = boundingBox3d(FRAGMENTS(i).GEOMETRY_DATA.c_hull);
        lx = abs(box(2)-box(1));
        ly = abs(box(4)-box(3));
        lz = abs(box(6)-box(5));
        FRAGMENTS(i).GEOMETRY_DATA.dimensions=[lx ly lz];
        x(i)=max(FRAGMENTS(i).GEOMETRY_DATA.dimensions);
    end
end
n_size=length(FRAGMENTS);

FRAGMENTS_bolted_link=FRAGMENTS;
clear FRAGMENTS

n_size=length(FRAGMENTS_bolted_link);
for i=n_size:-1:1
    if FRAGMENTS_bolted_link(i).GEOMETRY_DATA.mass==0
        FRAGMENTS_bolted_link(i)=[];
    end
end
n_size=length(FRAGMENTS_bolted_link);

for i = 2:n_size
    Lc_CST_bolted(i-1)=mean(FRAGMENTS_bolted_link(i).GEOMETRY_DATA.dimensions);
    Am_CST_bolted(i-1)=FRAGMENTS_bolted_link(i).GEOMETRY_DATA.A_M_ratio;
    vel_CST_bolted(i-1)=norm(FRAGMENTS_bolted_link(i).DYNAMICS_DATA.vel);
    mass_CST_bolted(i-1)=FRAGMENTS_bolted_link(i).GEOMETRY_DATA.mass;
end

% PLOT FRAGMENT DISTRIBUTIONS CST
s_bolted=sort(Lc_CST_bolted);

if n_size==1 %patch to avoid only one fragment case (LO)
    n_size=n_size+1;
end

N_bolted=n_size-1:-1:1;

% data for plot delta V
q_cm=[0 0 0]';
for i=2:n_size
    q_cm=q_cm+mass_CST_bolted(i-1)*vel_CST_bolted(:,i-1);
end
v_cm = q_cm/sum(mass_CST_bolted);
for i=2:n_size
    delta_v_CST_bolted(i-1)=norm(v_cm-vel_CST_bolted(:,i-1));
end

clear v_cm q_cm n_size

%% plot finale Lc

%{s
Lc_NASA=min(Lc_CST_baseline):(max(Lc_CST_baseline)-min(Lc_CST_baseline))/100:max(Lc_CST_baseline);
NASA_SBM_sub = 0.1*(mass_imp*norm(v_p))^0.75*Lc_NASA.^(-1.71);
m_tot=0;
for ii = 1:length(ME)
    m_tot=m_tot+ME(ii).GEOMETRY_DATA.mass0;
end
NASA_SBM_cat = 0.1*(m_tot)^0.75*Lc_NASA.^(-1.71);

n_frags_bubbles=0;
for ii = 1:length(BUBBLE)
    n_frags_bubbles=n_frags_bubbles+BUBBLE(ii).GEOMETRY_DATA.mass/(4/3*PROPAGATION_THRESHOLD.bubble_radius_th^3*pi()*MATERIAL_LIST(BUBBLE(ii).material_ID).density);
end

h = figure('Position', [45   129   906   553],'Visible', 'on');
loglog(s_baseline,[N_baseline(1)+n_frags_bubbles,N_baseline(2:end)],'+g','LineWidth',2)
grid on
hold on
loglog(s_continuum,[N_continuum(1)+n_frags_bubbles,N_continuum(2:end)],'MarkerFaceColor',colorOrder(9,:),'Marker','*','LineStyle','none')
hold on
loglog(s_bolted,[N_bolted(1)+n_frags_bubbles,N_bolted(2:end)],'MarkerFaceColor',colorOrder(11,:),'Marker','o','LineStyle','none')
hold on
loglog(Lc_NASA,NASA_SBM_sub,'-r','LineWidth',2)
hold on
loglog(Lc_NASA,NASA_SBM_cat,'--r','LineWidth',2)
hold on
title('Comparison CST results LOFT1')
xlabel('L_c [m]')
ylabel('Cumulative number N')
legend('CST LOFT1','CST LOFT1 (all CONTINUUM links)','CST LOFT1 (all BOLTED links)','NASA - SubCat-SBM','NASA - Cat-SBM')
set(gca,'FontSize',14,'FontWeight','bold')
xlim([4*10^-3 1])
ylim([10^0 10^6])
%}

%% plot finale A su M

%{r
clear DAmSOC DAmSOC_matlab DAmSOC_NASA
lambda_c_min=log10(min([s_baseline s_continuum s_bolted]));
lambda_c_max=log10(max([s_baseline s_continuum s_bolted]));
lambda_c=lambda_c_min:(lambda_c_max-lambda_c_min)/15:lambda_c_max;
csi_min=log10(min([Am_CST_baseline Am_CST_continuum Am_CST_bolted]));
csi_max=log10(max([Am_CST_baseline Am_CST_continuum Am_CST_bolted]));
n=length(lambda_c);
csi_NASA=csi_min:(csi_max-csi_min)/(n-1):csi_max;

for i=1:1:n
        if  lambda_c(i)<=-3.5
            muSOC=-0.3;
            sigmaSOC=0.2;
            DAmSOC(i,:)=(1/(sigmaSOC*(2*pi)^0.5))*exp(-((csi_NASA-muSOC).^2)/(2*sigmaSOC^2));
            pd = makedist('Normal','mu',muSOC,'sigma',sigmaSOC);
            DAmSOC_matlab(i,:) = pdf(pd,csi_NASA);
        elseif lambda_c(i)>-3.5 && lambda_c(i)<=-1.75
            muSOC=-0.3;
            sigmaSOC=0.2+0.1333*(lambda_c(i)+3.5);
            DAmSOC(i,:)=(1/(sigmaSOC*(2*pi)^0.5))*exp(-((csi_NASA-muSOC).^2)/(2*sigmaSOC^2));
            pd = makedist('Normal','mu',muSOC,'sigma',sigmaSOC);
            DAmSOC_matlab(i,:) = pdf(pd,csi_NASA);
        elseif lambda_c(i)>-1.75 && lambda_c(i)<-1.25
            muSOC=-0.3-1.4*(lambda_c(i)+1.75);
            sigmaSOC=0.2+0.1333*(lambda_c(i)+3.5);
            DAmSOC(i,:)=(1/(sigmaSOC*(2*pi)^0.5))*exp(-((csi_NASA-muSOC).^2)/(2*sigmaSOC^2));
            pd = makedist('Normal','mu',muSOC,'sigma',sigmaSOC);
            DAmSOC_matlab(i,:) = pdf(pd,csi_NASA);
        elseif lambda_c(i)>=-1.25
            muSOC=-1.0;
            sigmaSOC=0.2+0.1333*(lambda_c(i)+3.5);
            DAmSOC(i,:)=(1/(sigmaSOC*(2*pi)^0.5))*exp(-((csi_NASA-muSOC).^2)/(2*sigmaSOC^2));
            pd = makedist('Normal','mu',muSOC,'sigma',sigmaSOC);
            DAmSOC_matlab(i,:) = pdf(pd,csi_NASA);
        end
end

for k=1:1:size(DAmSOC,2)
    DAmSOC_NASA(k)=sum(DAmSOC(:,k));
    DAmSOC_matlab_NASA(k) = sum(DAmSOC_matlab(:,k));
end

h = figure('Position', [604          45        1010         500],'Visible', 'on');
histogram(log10(Am_CST_baseline),'Normalization','probability','BinWidth',0.03) 
grid on
hold on
histogram(log10(Am_CST_continuum),'Normalization','probability','BinWidth',0.03) 
hold on
histogram(log10(Am_CST_bolted),'Normalization','probability','BinWidth',0.03) 
hold on
plot(csi_NASA,DAmSOC_NASA./sum(DAmSOC_NASA),'-b','LineWidth',2)
hold on
title('Comparison among PDFs')
set(gca,'FontSize',20,'FontWeight','bold')
xlabel('log_{10}(A/m) [m^{2}/kg]')
ylabel('CN(A/m)')
legend('CST LOFT1','CST LOFT1 (all CONTINUUM links)','CST LOFT1 (all BOLTED links)','NASA SBM','Location','northeast')
%}

%% plot finale of Delta_v

%{
m=length(csi_NASA);
muEXP=0.9*csi_NASA+2.9;
sigmaEXP=0.4;
vel=sort([vel_CST_baseline vel_CST_continuum vel_CST_bolted]);
vel(vel==0)=[];
delta_v_min=vel(1);
delta_v_max=max([vel_CST_baseline vel_CST_continuum vel_CST_bolted]);
delta_v_NASA=delta_v_min:abs(delta_v_min-delta_v_max)/(m-1):delta_v_max;

for i=1:1:n
    DdvCOLL_NASA(:,i)=(1/(sigmaEXP*(2*pi)^0.5))*exp(-((delta_v_NASA-muEXP(i)).^2)/(2*sigmaEXP^2));
end

for k=1:1:size(DdvCOLL_NASA,1)
    DdvCOLL_NASA_1(k)=sum(DdvCOLL_NASA(:,k));
end

figure
histogram(delta_v_CST_baseline,'Normalization','probability','BinWidth',5)
grid on
hold on
histogram(delta_v_CST_continuum,'Normalization','probability','BinWidth',5)
hold on
histogram(delta_v_CST_bolted,'Normalization','probability','BinWidth',5)
hold on
plot(delta_v_NASA,DdvCOLL_NASA_1./sum(DdvCOLL_NASA_1),'b','LineWidth',2)
set(gca,'FontSize',20,'FontWeight','bold')
title('Comparison among PDFs')
xlabel('\DeltaV [m/s] ')
ylabel('Relative Number of Fragments per \DeltaV bin')
legend('CST LOFT1','CST LOFT1 (all CONTINUUM links)','CST LOFT1 (all BOLTED links)','Nasa SBM','Location','northeast')
% xlim([0 2700])
%}