function create_user_link()

global sim_title
global link_data

if ~isempty(link_data)
	f_name=fullfile(pwd,'Simulation_cases',sim_title,'link_user.m');
	if exist(f_name)
		delete(f_name);
	end

	fid=fopen(f_name,'w');

    for i=1:length(link_data)
        fprintf(fid,'i=%d;\n',i);
        fprintf(fid,'link_data(%d).id=%d;\n',i,link_data(i).id);
        fprintf(fid,'link_data(%d).ME_connect=[%d,%d];\n',i,link_data(i).ME_connect(1),link_data(i).ME_connect(2));
        fprintf(fid,'link_data(%d).material_ID=%d;\n',i,link_data(i).material_ID);
%         fprintf(fid,'link_data(%d).geometry_ID=%d;\n',i,link_data(i).geometry_ID);
        fprintf(fid,'link_data(%d).type_ID=%d;\n',i,link_data(i).type_ID);
        fprintf(fid,'link_data(%d).failure_ID=%f;\n',i,link_data(i).failure_ID);
        AA=[];
        AA=link_data(i).k_mat;
        fprintf(fid,'link_data(%d).k_mat=[%f,%f,%f,%f,%f,%f;\n                    %f,%f,%f,%f,%f,%f;\n                     %f,%f,%f,%f,%f,%f;\n                     %f,%f,%f,%f,%f,%f;\n                     %f,%f,%f,%f,%f,%f;\n                     %f,%f,%f,%f,%f,%f];\n',i,...
            AA(1,1),AA(1,2),AA(1,3),AA(1,4),AA(1,5),AA(1,6),...
            AA(2,1),AA(2,2),AA(2,3),AA(2,4),AA(2,5),AA(2,6),...
            AA(3,1),AA(3,2),AA(3,3),AA(3,4),AA(3,5),AA(3,6),...
            AA(4,1),AA(4,2),AA(4,3),AA(4,4),AA(4,5),AA(4,6),...
            AA(5,1),AA(5,2),AA(5,3),AA(5,4),AA(5,5),AA(5,6),...
            AA(6,1),AA(6,2),AA(6,3),AA(6,4),AA(6,5),AA(6,6));
        AA=[];
        AA=link_data(i).c_mat;
        fprintf(fid,'link_data(%d).c_mat=[%f,%f,%f,%f,%f,%f;\n                    %f,%f,%f,%f,%f,%f;\n                     %f,%f,%f,%f,%f,%f;\n                     %f,%f,%f,%f,%f,%f;\n                     %f,%f,%f,%f,%f,%f;\n                     %f,%f,%f,%f,%f,%f];\n',i,...
            AA(1,1),AA(1,2),AA(1,3),AA(1,4),AA(1,5),AA(1,6),...
            AA(2,1),AA(2,2),AA(2,3),AA(2,4),AA(2,5),AA(2,6),...
            AA(3,1),AA(3,2),AA(3,3),AA(3,4),AA(3,5),AA(3,6),...
            AA(4,1),AA(4,2),AA(4,3),AA(4,4),AA(4,5),AA(4,6),...
            AA(5,1),AA(5,2),AA(5,3),AA(5,4),AA(5,5),AA(5,6),...
            AA(6,1),AA(6,2),AA(6,3),AA(6,4),AA(6,5),AA(6,6));
        fprintf(fid,'link_data(%d).rest=[%f;%f;%f;%f;%f;%f];\n',i,link_data(i).rest(1),link_data(i).rest(2),link_data(i).rest(3),link_data(i).rest(4),link_data(i).rest(5),link_data(i).rest(6));
        fprintf(fid,'link_data(%d).state=%d;\n',i,link_data(i).state);
    end
    fclose(fid);
end
