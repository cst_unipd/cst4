function create_user_ME()

global sim_title
global ME FRAGMENTS BUBBLE

f_name=fullfile(pwd,'Simulation_cases',sim_title,'user_ME.m');
if exist(f_name)
    delete(f_name);
end

default_FRAGMENTATION_DATA_failure_ID=1;
default_minumum_SEED_distribution_param_1=1;
default_minumum_SEED_distribution_param_2=5;
default_fragmentation_data_cELOSS=0.1;
default_fragmentation_data_cMLOSS=0.2;
fid=fopen(f_name,'w');

if ~isempty(ME)
    for i=1:length(ME)
        fprintf(fid,'i=%d;\n',i);
        fprintf(fid,'ME(%d).object_ID=%d;\n',i,ME(i).object_ID);
        fprintf(fid,'ME(%d).material_ID=%d;\n',i,ME(i).material_ID);
        fprintf(fid,'ME(%d).GEOMETRY_DATA.shape_ID=%d;\n',i,ME(i).GEOMETRY_DATA.shape_ID);
        fprintf(fid,'ME(%d).GEOMETRY_DATA.dimensions=[%f,%f,%f];\n',i,ME(i).GEOMETRY_DATA.dimensions(1),ME(i).GEOMETRY_DATA.dimensions(2),ME(i).GEOMETRY_DATA.dimensions(3));
        fprintf(fid,'ME(%d).GEOMETRY_DATA.thick=%f;\n',i,ME(i).GEOMETRY_DATA.thick);
        fprintf(fid,'ME(%d).GEOMETRY_DATA.mass0=%f;\n',i,ME(i).GEOMETRY_DATA.mass0);
        fprintf(fid,'ME(%d).GEOMETRY_DATA.mass=%f;\n',i,ME(i).GEOMETRY_DATA.mass);
        fprintf(fid,'ME(%d).DYNAMICS_INITIAL_DATA.cm_coord0=[%f;%f;%f];\n',i,ME(i).DYNAMICS_INITIAL_DATA.cm_coord0(1),ME(i).DYNAMICS_INITIAL_DATA.cm_coord0(2),ME(i).DYNAMICS_INITIAL_DATA.cm_coord0(3));
        fprintf(fid,'ME(%d).DYNAMICS_DATA.cm_coord=ME(%d).DYNAMICS_INITIAL_DATA.cm_coord0;\n',i,i);
        fprintf(fid,'ME(%d).DYNAMICS_INITIAL_DATA.quaternions0=[%f,%f,%f,%f];\n',i,ME(i).DYNAMICS_INITIAL_DATA.quaternions0(1),ME(i).DYNAMICS_INITIAL_DATA.quaternions0(2),ME(i).DYNAMICS_INITIAL_DATA.quaternions0(3),ME(i).DYNAMICS_INITIAL_DATA.quaternions0(4));
        fprintf(fid,'ME(%d).DYNAMICS_INITIAL_DATA.vel0=[%f;%f;%f];\n',i,ME(i).DYNAMICS_INITIAL_DATA.vel0(1),ME(i).DYNAMICS_INITIAL_DATA.vel0(2),ME(i).DYNAMICS_INITIAL_DATA.vel0(3));
        fprintf(fid,'ME(%d).DYNAMICS_DATA.vel=ME(%d).DYNAMICS_INITIAL_DATA.vel0;\n',i,i);
        fprintf(fid,'ME(%d).DYNAMICS_INITIAL_DATA.w0=[%f;%f;%f];\n',i,ME(i).DYNAMICS_INITIAL_DATA.w0(1),ME(i).DYNAMICS_INITIAL_DATA.w0(2),ME(i).DYNAMICS_INITIAL_DATA.w0(3));
        fprintf(fid,'ME(%d).DYNAMICS_DATA.quaternions=ME(%d).DYNAMICS_INITIAL_DATA.quaternions0;\n',i,i);
        fprintf(fid,'ME(%d).DYNAMICS_DATA.w=ME(%d).DYNAMICS_INITIAL_DATA.w0;\n',i,i);
        fprintf(fid,'ME(%d).DYNAMICS_DATA.virt_momentum=[%f;%f;%f];\n',i,ME(i).DYNAMICS_DATA.virt_momentum(1),ME(i).DYNAMICS_DATA.virt_momentum(2),ME(i).DYNAMICS_DATA.virt_momentum(3));
        fprintf(fid,'ME(%d).GEOMETRY_DATA.A_M_ratio=%f;\n',i,ME(i).GEOMETRY_DATA.A_M_ratio);
        fprintf(fid,'ME(%d).GEOMETRY_DATA.c_hull=[0,0,0];\n',i);
        ME(i).FRAGMENTATION_DATA.threshold=ME(i).FRAGMENTATION_DATA.threshold0;
        fprintf(fid,'ME(%d).FRAGMENTATION_DATA.threshold0=%f;\n',i,ME(i).FRAGMENTATION_DATA.threshold0);
        fprintf(fid,'ME(%d).FRAGMENTATION_DATA.threshold=%f;\n',i,ME(i).FRAGMENTATION_DATA.threshold0);
        ME(i).FRAGMENTATION_DATA.failure_ID=default_FRAGMENTATION_DATA_failure_ID;
        fprintf(fid,'ME(%d).FRAGMENTATION_DATA.failure_ID=%d;\n',i,ME(i).FRAGMENTATION_DATA.failure_ID);
        fprintf(fid,'ME(%d).FRAGMENTATION_DATA.breakup_flag=%d;\n',i,ME(i).FRAGMENTATION_DATA.breakup_flag);
        fprintf(fid,'ME(%d).FRAGMENTATION_DATA.ME_energy_transfer_coef=%f;\n',i,ME(i).FRAGMENTATION_DATA.ME_energy_transfer_coef);
        if(ME(i).FRAGMENTATION_DATA.cMLOSS == 0)
            ME(i).FRAGMENTATION_DATA.cMLOSS=default_fragmentation_data_cMLOSS;
        end
        fprintf(fid,'ME(%d).FRAGMENTATION_DATA.cMLOSS=%f;\n',i,ME(i).FRAGMENTATION_DATA.cMLOSS);
        if(ME(i).FRAGMENTATION_DATA.cELOSS == 0)
            ME(i).FRAGMENTATION_DATA.cELOSS=default_fragmentation_data_cELOSS;
        end
        fprintf(fid,'ME(%d).FRAGMENTATION_DATA.cELOSS=%f;\n',i,ME(i).FRAGMENTATION_DATA.cELOSS);
        fprintf(fid,'ME(%d).FRAGMENTATION_DATA.c_EXPL=%f;\n',i,ME(i).FRAGMENTATION_DATA.c_EXPL);
        fprintf(fid,'ME(%d).FRAGMENTATION_DATA.seeds_distribution_ID=%d;\n',i,ME(i).FRAGMENTATION_DATA.seeds_distribution_ID);
        if(ME(i).FRAGMENTATION_DATA.seeds_distribution_param1<default_minumum_SEED_distribution_param_1)
            ME(i).FRAGMENTATION_DATA.seeds_distribution_param1=default_minumum_SEED_distribution_param_1;
            disp('Setting a minimum seeds number');
        end
        fprintf(fid,'ME(%d).FRAGMENTATION_DATA.seeds_distribution_param1=%d;\n',i,ME(i).FRAGMENTATION_DATA.seeds_distribution_param1);
        if(ME(i).FRAGMENTATION_DATA.seeds_distribution_param2<default_minumum_SEED_distribution_param_2)
            ME(i).FRAGMENTATION_DATA.seeds_distribution_param2=default_minumum_SEED_distribution_param_2;
            disp('Setting a minimum number of iterations for Voronoi solver calculations');
        end
        fprintf(fid,'ME(%d).FRAGMENTATION_DATA.seeds_distribution_param2=%f;\n',i,ME(i).FRAGMENTATION_DATA.seeds_distribution_param2);
        fprintf(fid,'ME(%d).FRAGMENTATION_DATA.param_add1=%f;\n',i,ME(i).FRAGMENTATION_DATA.param_add1);
        fprintf(fid,'ME(%d).FRAGMENTATION_DATA.param_add2=%f;\n',i,ME(i).FRAGMENTATION_DATA.param_add2);
    end
end

if ~isempty(FRAGMENTS)
    for i=1:length(FRAGMENTS)
        fprintf(fid,'i=%d;\n',i);
        fprintf(fid,'FRAGMENTS(%d).object_ID=%d;\n',i,FRAGMENTS(i).object_ID);
        fprintf(fid,'FRAGMENTS(%d).material_ID=%d;\n',i,FRAGMENTS(i).material_ID);
        fprintf(fid,'FRAGMENTS(%d).GEOMETRY_DATA.shape_ID=%d;\n',i,FRAGMENTS(i).GEOMETRY_DATA.shape_ID);
        fprintf(fid,'FRAGMENTS(%d).GEOMETRY_DATA.dimensions=[%f;%f;%f];\n',i,FRAGMENTS(i).GEOMETRY_DATA.dimensions(1),FRAGMENTS(i).GEOMETRY_DATA.dimensions(2),FRAGMENTS(i).GEOMETRY_DATA.dimensions(3));
        fprintf(fid,'FRAGMENTS(%d).GEOMETRY_DATA.thick=%f;\n',i,FRAGMENTS(i).GEOMETRY_DATA.thick);
        fprintf(fid,'FRAGMENTS(%d).GEOMETRY_DATA.mass0=%f;\n',i,FRAGMENTS(i).GEOMETRY_DATA.mass0);
        fprintf(fid,'FRAGMENTS(%d).GEOMETRY_DATA.mass=%f;\n',i,FRAGMENTS(i).GEOMETRY_DATA.mass);
        fprintf(fid,'FRAGMENTS(%d).DYNAMICS_INITIAL_DATA.cm_coord0=[%f;%f;%f];\n',i,FRAGMENTS(i).DYNAMICS_INITIAL_DATA.cm_coord0(1),FRAGMENTS(i).DYNAMICS_INITIAL_DATA.cm_coord0(2),FRAGMENTS(i).DYNAMICS_INITIAL_DATA.cm_coord0(3));
        fprintf(fid,'FRAGMENTS(%d).DYNAMICS_DATA.cm_coord=FRAGMENTS(%d).DYNAMICS_INITIAL_DATA.cm_coord0;\n',i,i);
        fprintf(fid,'FRAGMENTS(%d).DYNAMICS_INITIAL_DATA.quaternions0=[%f;%f;%f;%f];\n',i,FRAGMENTS(i).DYNAMICS_INITIAL_DATA.quaternions0(1),FRAGMENTS(i).DYNAMICS_INITIAL_DATA.quaternions0(2),FRAGMENTS(i).DYNAMICS_INITIAL_DATA.quaternions0(3),FRAGMENTS(i).DYNAMICS_INITIAL_DATA.quaternions0(4));
        fprintf(fid,'FRAGMENTS(%d).DYNAMICS_INITIAL_DATA.vel0=[%f;%f;%f];\n',i,FRAGMENTS(i).DYNAMICS_INITIAL_DATA.vel0(1),FRAGMENTS(i).DYNAMICS_INITIAL_DATA.vel0(2),FRAGMENTS(i).DYNAMICS_INITIAL_DATA.vel0(3));
        fprintf(fid,'FRAGMENTS(%d).DYNAMICS_DATA.vel=FRAGMENTS(%d).DYNAMICS_INITIAL_DATA.vel0;\n',i,i);
        fprintf(fid,'FRAGMENTS(%d).DYNAMICS_INITIAL_DATA.w0=[%f;%f;%f];\n',i,FRAGMENTS(i).DYNAMICS_INITIAL_DATA.w0(1),FRAGMENTS(i).DYNAMICS_INITIAL_DATA.w0(2),FRAGMENTS(i).DYNAMICS_INITIAL_DATA.w0(3));
        fprintf(fid,'FRAGMENTS(%d).DYNAMICS_DATA.quaternions=FRAGMENTS(%d).DYNAMICS_INITIAL_DATA.quaternions0;\n',i,i);
        fprintf(fid,'FRAGMENTS(%d).DYNAMICS_DATA.w=FRAGMENTS(%d).DYNAMICS_INITIAL_DATA.w0;\n',i,i);
        fprintf(fid,'FRAGMENTS(%d).DYNAMICS_DATA.virt_momentum=[%f;%f;%f];\n',i,FRAGMENTS(i).DYNAMICS_DATA.virt_momentum(1),FRAGMENTS(i).DYNAMICS_DATA.virt_momentum(2),FRAGMENTS(i).DYNAMICS_DATA.virt_momentum(3));
        fprintf(fid,'FRAGMENTS(%d).GEOMETRY_DATA.A_M_ratio=%f;\n',i,FRAGMENTS(i).GEOMETRY_DATA.A_M_ratio);
        fprintf(fid,'FRAGMENTS(%d).GEOMETRY_DATA.c_hull=[0,0,0];\n',i);
        FRAGMENTS(i).FRAGMENTATION_DATA.threshold=FRAGMENTS(i).FRAGMENTATION_DATA.threshold0;
        fprintf(fid,'FRAGMENTS(%d).FRAGMENTATION_DATA.threshold0=%f;\n',i,FRAGMENTS(i).FRAGMENTATION_DATA.threshold0);
        fprintf(fid,'FRAGMENTS(%d).FRAGMENTATION_DATA.threshold=%f;\n',i,FRAGMENTS(i).FRAGMENTATION_DATA.threshold0);
        FRAGMENTS(i).FRAGMENTATION_DATA.failure_ID=default_FRAGMENTATION_DATA_failure_ID;
        fprintf(fid,'FRAGMENTS(%d).FRAGMENTATION_DATA.failure_ID=%d;\n',i,FRAGMENTS(i).FRAGMENTATION_DATA.failure_ID);
        fprintf(fid,'FRAGMENTS(%d).FRAGMENTATION_DATA.breakup_flag=%d;\n',i,FRAGMENTS(i).FRAGMENTATION_DATA.breakup_flag);
        fprintf(fid,'FRAGMENTS(%d).FRAGMENTATION_DATA.ME_energy_transfer_coef=%f;\n',i,FRAGMENTS(i).FRAGMENTATION_DATA.ME_energy_transfer_coef);
        if(FRAGMENTS(i).FRAGMENTATION_DATA.cMLOSS == 0)
            FRAGMENTS(i).FRAGMENTATION_DATA.cMLOSS=default_fragmentation_data_cMLOSS;
        end
        fprintf(fid,'FRAGMENTS(%d).FRAGMENTATION_DATA.cMLOSS=%f;\n',i,FRAGMENTS(i).FRAGMENTATION_DATA.cMLOSS);
        if(FRAGMENTS(i).FRAGMENTATION_DATA.cELOSS == 0)
            FRAGMENTS(i).FRAGMENTATION_DATA.cELOSS=default_fragmentation_data_cELOSS;
        end
        fprintf(fid,'FRAGMENTS(%d).FRAGMENTATION_DATA.cELOSS=%f;\n',i,FRAGMENTS(i).FRAGMENTATION_DATA.cELOSS);
        fprintf(fid,'FRAGMENTS(%d).FRAGMENTATION_DATA.c_EXPL=%f;\n',i,FRAGMENTS(i).FRAGMENTATION_DATA.c_EXPL);
        fprintf(fid,'FRAGMENTS(%d).FRAGMENTATION_DATA.seeds_distribution_ID=%d;\n',i,FRAGMENTS(i).FRAGMENTATION_DATA.seeds_distribution_ID);
        if FRAGMENTS(i).FRAGMENTATION_DATA.seeds_distribution_param1<default_minumum_SEED_distribution_param_1
            FRAGMENTS(i).FRAGMENTATION_DATA.seeds_distribution_param1=default_minumum_SEED_distribution_param_1;
            disp('Setting a minimum seeds number');
        end
        fprintf(fid,'FRAGMENTS(%d).FRAGMENTATION_DATA.seeds_distribution_param1=%d;\n',i,FRAGMENTS(i).FRAGMENTATION_DATA.seeds_distribution_param1);
        if FRAGMENTS(i).FRAGMENTATION_DATA.seeds_distribution_param2<default_minumum_SEED_distribution_param_2
            FRAGMENTS(i).FRAGMENTATION_DATA.seeds_distribution_param2=default_minumum_SEED_distribution_param_2;
            disp('Setting a minimum number of iterations for Voronoi solver calculations');
        end
        fprintf(fid,'FRAGMENTS(%d).FRAGMENTATION_DATA.seeds_distribution_param2=%f;\n',i,FRAGMENTS(i).FRAGMENTATION_DATA.seeds_distribution_param2);
        fprintf(fid,'FRAGMENTS(%d).FRAGMENTATION_DATA.param_add1=%f;\n',i,FRAGMENTS(i).FRAGMENTATION_DATA.param_add1);
        fprintf(fid,'FRAGMENTS(%d).FRAGMENTATION_DATA.param_add2=%f;\n',i,FRAGMENTS(i).FRAGMENTATION_DATA.param_add2);
    end
end

if ~isempty(BUBBLE)
    for i=1:length(BUBBLE)
        fprintf(fid,'i=%d;\n',i);
        fprintf(fid,'BUBBLE(%d).object_ID=%d;\n',i,BUBBLE(i).object_ID);
        fprintf(fid,'BUBBLE(%d).material_ID=%d;\n',i,BUBBLE(i).material_ID);
        fprintf(fid,'BUBBLE(%d).GEOMETRY_DATA.shape_ID=%d;\n',i,BUBBLE(i).GEOMETRY_DATA.shape_ID);
        fprintf(fid,'BUBBLE(%d).GEOMETRY_DATA.dimensions=[%f,%f,%f];\n',i,BUBBLE(i).GEOMETRY_DATA.dimensions(1),BUBBLE(i).GEOMETRY_DATA.dimensions(2),BUBBLE(i).GEOMETRY_DATA.dimensions(3));
        fprintf(fid,'BUBBLE(%d).GEOMETRY_DATA.thick=%f;\n',i,BUBBLE(i).GEOMETRY_DATA.thick);
        fprintf(fid,'BUBBLE(%d).GEOMETRY_DATA.mass0=%f;\n',i,BUBBLE(i).GEOMETRY_DATA.mass0);
        fprintf(fid,'BUBBLE(%d).GEOMETRY_DATA.mass=%f;\n',i,BUBBLE(i).GEOMETRY_DATA.mass);
        fprintf(fid,'BUBBLE(%d).DYNAMICS_INITIAL_DATA.cm_coord0=[%f;%f;%f];\n',i,BUBBLE(i).DYNAMICS_INITIAL_DATA.cm_coord0(1),BUBBLE(i).DYNAMICS_INITIAL_DATA.cm_coord0(2),BUBBLE(i).DYNAMICS_INITIAL_DATA.cm_coord0(3));
        fprintf(fid,'BUBBLE(%d).DYNAMICS_DATA.cm_coord=BUBBLE(%d).DYNAMICS_INITIAL_DATA.cm_coord0;\n',i,i);
        fprintf(fid,'BUBBLE(%d).DYNAMICS_INITIAL_DATA.quaternions0=[%f,%f,%f,%f];\n',i,BUBBLE(i).DYNAMICS_INITIAL_DATA.quaternions0(1),BUBBLE(i).DYNAMICS_INITIAL_DATA.quaternions0(2),BUBBLE(i).DYNAMICS_INITIAL_DATA.quaternions0(3),BUBBLE(i).DYNAMICS_INITIAL_DATA.quaternions0(4));
        fprintf(fid,'BUBBLE(%d).DYNAMICS_INITIAL_DATA.vel0=[%f;%f;%f];\n',i,BUBBLE(i).DYNAMICS_INITIAL_DATA.vel0(1),BUBBLE(i).DYNAMICS_INITIAL_DATA.vel0(2),BUBBLE(i).DYNAMICS_INITIAL_DATA.vel0(3));
        fprintf(fid,'BUBBLE(%d).DYNAMICS_DATA.vel=BUBBLE(%d).DYNAMICS_INITIAL_DATA.vel0;\n',i,i);
        fprintf(fid,'BUBBLE(%d).DYNAMICS_INITIAL_DATA.w0=[%f;%f;%f];\n',i,BUBBLE(i).DYNAMICS_INITIAL_DATA.w0(1),BUBBLE(i).DYNAMICS_INITIAL_DATA.w0(2),BUBBLE(i).DYNAMICS_INITIAL_DATA.w0(3));
        fprintf(fid,'BUBBLE(%d).DYNAMICS_DATA.quaternions=BUBBLE(%d).DYNAMICS_INITIAL_DATA.quaternions0;\n',i,i);
        fprintf(fid,'BUBBLE(%d).DYNAMICS_DATA.w=BUBBLE(%d).DYNAMICS_INITIAL_DATA.w0;\n',i,i);
        fprintf(fid,'BUBBLE(%d).DYNAMICS_DATA.virt_momentum=[%f;%f;%f];\n',i,BUBBLE(i).DYNAMICS_DATA.virt_momentum(1),BUBBLE(i).DYNAMICS_DATA.virt_momentum(2),BUBBLE(i).DYNAMICS_DATA.virt_momentum(3));
        fprintf(fid,'BUBBLE(%d).GEOMETRY_DATA.A_M_ratio=%f;\n',i,BUBBLE(i).GEOMETRY_DATA.A_M_ratio);
        fprintf(fid,'BUBBLE(%d).GEOMETRY_DATA.c_hull=[0,0,0];\n',i);
        fprintf(fid,'BUBBLE(%d).FRAGMENTATION_DATA.threshold0=%f;\n',i,BUBBLE(i).FRAGMENTATION_DATA.threshold0);
        fprintf(fid,'BUBBLE(%d).FRAGMENTATION_DATA.threshold=%f;\n',i,BUBBLE(i).FRAGMENTATION_DATA.threshold);
        %                 fprintf(fid,'BUBBLE(%d).FRAGMENTATION_DATA.failure_ID=%d;\n',i,BUBBLE(i).FRAGMENTATION_DATA.failure_ID);
        fprintf(fid,'BUBBLE(%d).FRAGMENTATION_DATA.failure_ID=%d;\n',i,default_FRAGMENTATION_DATA_failure_ID);
        BUBBLE(i).FRAGMENTATION_DATA.failure_ID=default_FRAGMENTATION_DATA_failure_ID;
        fprintf(fid,'BUBBLE(%d).FRAGMENTATION_DATA.breakup_flag=%d;\n',i,BUBBLE(i).FRAGMENTATION_DATA.breakup_flag);
        fprintf(fid,'BUBBLE(%d).FRAGMENTATION_DATA.ME_energy_transfer_coef=%f;\n',i,BUBBLE(i).FRAGMENTATION_DATA.ME_energy_transfer_coef);
        if(BUBBLE(i).FRAGMENTATION_DATA.cMLOSS == 0)
            BUBBLE(i).FRAGMENTATION_DATA.cMLOSS=default_fragmentation_data_cMLOSS;
        end
        fprintf(fid,'BUBBLE(%d).FRAGMENTATION_DATA.cMLOSS=%f;\n',i,BUBBLE(i).FRAGMENTATION_DATA.cMLOSS);
        if(BUBBLE(i).FRAGMENTATION_DATA.cELOSS == 0)
            BUBBLE(i).FRAGMENTATION_DATA.cELOSS=default_fragmentation_data_cELOSS;
        end
        fprintf(fid,'BUBBLE(%d).FRAGMENTATION_DATA.cELOSS=%f;\n',i,BUBBLE(i).FRAGMENTATION_DATA.cELOSS);
        fprintf(fid,'BUBBLE(%d).FRAGMENTATION_DATA.cELOSS=%f;\n',i,BUBBLE(i).FRAGMENTATION_DATA.cELOSS);
        fprintf(fid,'BUBBLE(%d).FRAGMENTATION_DATA.c_EXPL=%f;\n',i,BUBBLE(i).FRAGMENTATION_DATA.c_EXPL);
        fprintf(fid,'BUBBLE(%d).FRAGMENTATION_DATA.seeds_distribution_ID=%d;\n',i,BUBBLE(i).FRAGMENTATION_DATA.seeds_distribution_ID);
        if BUBBLE(i).FRAGMENTATION_DATA.seeds_distribution_param1<default_minumum_SEED_distribution_param_1
            BUBBLE(i).FRAGMENTATION_DATA.seeds_distribution_param1=default_minumum_SEED_distribution_param_1;
            disp('Setting a minimum seeds number');
        end
        fprintf(fid,'BUBBLE(%d).FRAGMENTATION_DATA.seeds_distribution_param1=%d;\n',i,BUBBLE(i).FRAGMENTATION_DATA.seeds_distribution_param1);
        if BUBBLE(i).FRAGMENTATION_DATA.seeds_distribution_param2<default_minumum_SEED_distribution_param_2
            BUBBLE(i).FRAGMENTATION_DATA.seeds_distribution_param2=default_minumum_SEED_distribution_param_2;
            disp('Setting a minimum seeds number');
        end
        fprintf(fid,'BUBBLE(%d).FRAGMENTATION_DATA.seeds_distribution_param2=%f;\n',i,BUBBLE(i).FRAGMENTATION_DATA.seeds_distribution_param2);
        fprintf(fid,'BUBBLE(%d).FRAGMENTATION_DATA.param_add1=%f;\n',i,BUBBLE(i).FRAGMENTATION_DATA.param_add1);
        fprintf(fid,'BUBBLE(%d).FRAGMENTATION_DATA.param_add2=%f;\n',i,BUBBLE(i).FRAGMENTATION_DATA.param_add2);
    end
end

fclose(fid);