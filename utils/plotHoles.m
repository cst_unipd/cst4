function plotHoles()

%%
global HOLES;
global ME;

nHoles = length(HOLES);
nME = length(ME);

MEe = ME;
for k=1:nME
    MEe(k).GEOMETRY_DATA.f_hull = minConvexHull(MEe(k).GEOMETRY_DATA.c_hull);
end

%%

figure()
set(gcf,'color','white');
hold on; axis equal;
view(30,40);

% plot Global RF
% pltassi(trasm(eye(3),[0 0 0]'),'r','G',1,0.3);

% plot ME
%{
for k=1:nME
    drawPoint3d(MEe(k).DYNAMICS_DATA.cm_coord','ok');
    drawMesh(MEe(k).GEOMETRY_DATA.c_hull,MEe(k).GEOMETRY_DATA.f_hull,...
             'FaceColor',[0 0 0],'FaceAlpha',0.05);
end
%}

%
% plot Holes
for k=1:nHoles
    % plot parent ME
    pidx = HOLES(k).object_ID;
    drawPoint3d(MEe(pidx).DYNAMICS_DATA.cm_coord','ok');
    drawMesh(((rquat(MEe(pidx).DYNAMICS_DATA.quaternions)*MEe(pidx).GEOMETRY_DATA.c_hull')+MEe(pidx).DYNAMICS_DATA.cm_coord)',...
             MEe(pidx).GEOMETRY_DATA.f_hull,...
             'FaceColor',[0 0 0],'FaceAlpha',0.01);
%     drawMesh(MEe(pidx).GEOMETRY_DATA.c_hull+MEe(pidx).DYNAMICS_DATA.cm_coord',...
%              MEe(pidx).GEOMETRY_DATA.f_hull,...
%              'FaceColor',[0 0 0],'FaceAlpha',0.01);
    
    drawPoint3d(HOLES(k).DYNAMICS_DATA.cm_coord','*r');
    HOLES(k).f_hull = minConvexHull(HOLES(k).GEOMETRY_DATA.c_hull);
    drawMesh(HOLES(k).GEOMETRY_DATA.c_hull+HOLES(k).DYNAMICS_DATA.cm_coord',...
             HOLES(k).f_hull,...
             'FaceColor',[1 0 0],'FaceAlpha',0.2);
end
%}


end