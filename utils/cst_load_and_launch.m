%> @file  cst_load_and_launch.m
%> @brief Collision Simulation Tool file loader and solver launch
%> Copyright (c) 2018 CISAS - UNIVERSITY OF PADOVA
%======================================================================
% Interface with etamax outputs and test cases
% Developed in Feb. 2018 - Dr. Lorenzo Olivieri - lorenzo.olivieri@unipd.it

function []= cst_load_and_launch(config)

% profile on -detail builtin
global ME;
global FRAGMENTS;
global BUBBLE;
global sim_title_dir;
global sim_title;
% global t;            % Current time within the simulation
global dt_SR;           % Current time step
global t_end;        % Simulation maximum duration
global ME_SR;   %ME updated by Structural Response
global ME_SR_tm1; %V02, copy of ME at the preavious step, used only in SR (FF)
global SR_delta; %ME displacement calculated by Structural Response

global MATERIAL_LIST;
global SHAPE_ID_LIST;
global SEEDS_DISTRIBUTION;
global PROPAGATION_THRESHOLD;
global cygwin64_folder;
global voropp_folder;
global neper_folder;% global folder paths
% global hostname; % host name
global voronoi_solver_ID;
global plot_flag; %flag for fragmentation plottings
global link_data;
global cst_log;
global COLLISION_DATA_tracking

%% Initialization of useful data
shape_id_list();
material_list();
seeds_distribution_list();

%% Create results folders
sim_title=config;
sim_title_dir=[sim_title,'_',datestr(now,30)];
sim_title_dir=fullfile('Results',sim_title_dir);
mkdir(sim_title_dir);
mkdir(fullfile(sim_title_dir,'figures'));
mkdir(fullfile(sim_title_dir,'data'));

% create structures
ME=population_empty_structure();
FRAGMENTS=population_empty_structure();
BUBBLE=population_empty_structure();
link_data=[];
link_data0=link_data;
COLLISION_DATA_tracking=struct('target',{},'target_shape',{},'impactor',{},'impactor_shape',{},'point',{});


%% SCENARIO DEFINITION
% FARE CHECK SE VI E' MATERIALE NELLA CARTELLA
% IN CASO CARICARE PARSER
% FARE EDIT

input_data_dir=fullfile('Simulation_cases',config);

file=dir(input_data_dir);

%% EDIT DI ME; FRAGMENTS; BUBBLE; SETTINGS
[t_end, t_step, t_start, dt_SR, n_max_main_loop, voronoi_solver_ID, PROPAGATION_THRESHOLD]=cst_settings_parser();

if length(file)<=2
    disp('No simulation input data found. Exit from simulation')
else
    % loading the ME; FRAGMENTS, BUBBLES, link_data
    for i=length(file):-1:1
        if file(i).isdir==1
            file(i)=[];
        end
    end
    
    restart;
    
    % See if there is at least a file called "user", and load it
    count_user=0;
    count_link_user=0;
    for i=1:length(file)
        [~,fname,~] = fileparts(file(i).name);
        if contains(fname,'user')
            file_user(count_user+1)=file(i);
            count_user=count_user+1;
        end
        if contains(fname,'link_user')
            file_link_user(count_link_user+1)=file(i);
            count_link_user=count_link_user+1;
        end
    end
    
    % See if there is at least a file called "generate", and load it
    count_generate=0;
    count_link_generate=0;
    for i=1:length(file)
        [~,fname,~] = fileparts(file(i).name);
        if contains(fname,'generate')
            file_generate(count_generate+1)=file(i);
            count_generate=count_generate+1;
        end
        
        if contains(fname,'link_generate')
            file_link_generate(count_link_generate+1)=file(i);
            count_link_generate=count_link_generate+1;
        end
    end
    
    % LOAD THE user FILES
    if count_user ~=0
        
        file=dir(input_data_dir);
        
        for i=length(file):-1:1
            if file(i).isdir==1
                file(i)=[];
            end
        end
        
        dlg = @questdlg;
        % Call the dialog
        button = dlg('Do you want to modify the m-files?','','YES','NO','NO');
        
        if isempty(button)==true
            button='NO';
        end
        switch button
            case 'YES'
                file1=file;
                for i=length(file1):-1:1
                    [~,~,exp] = fileparts(file1(i).name);
                    if contains(exp,'dump')
                        file1(i)=[];
                    elseif contains(exp,'data')
                        file1(i)=[];
                    elseif contains(exp,'mat')
                        file1(i)=[];
                    end
                end
                str = {file1.name};
                [index_config,exit_2] = listdlg('PromptString','Select the file. Note that you can select multiple files',...
                    'SelectionMode','multiple','ListString',str);
                if  exit_2==0
                    disp('No file selected, proceed with simulation');
                else
                    
                    for i_string=1:length(index_config)
                        
                        edit(fullfile(file1(index_config(i_string)).folder,file1(index_config(i_string)).name))
                        
                        if i_string==length(index_config)
                            string_dial='Click the OK button to prosecute simulation; SAVE THE FILE';
                        else
                            string_dial='Click the OK button to modify next file; SAVE THE FILE';
                        end
                        options.WindowStyle='normal';
                        options.Interpreter='tex';
                        options.Default = 'OK';
                        MFquestdlg([ 0.5 , 0.5 ],string_dial, '','OK','OK');
                    end
                end
        end
        
        
        for i=1:count_user
            run(fullfile(file_user(i).folder,file_user(i).name))
        end
        for i=1:count_link_user
            run(fullfile(file_link_user(i).folder,file_link_user(i).name))
        end
        
        
        % LOAD THE GENERATE FILES
    elseif count_generate~=0
        
        cd(input_data_dir)
        i1=length(ME)+1;
        i2=length(FRAGMENTS)+1;
        i3=length(BUBBLE)+1;
        for i=1:count_generate
            name=file_generate(i).name;
            L1=length(name);
            name(L1-1:L1)=[];
            fh = str2func(name);
            [ME,FRAGMENTS,BUBBLE]=fh(i1,i2,i3,ME,FRAGMENTS,BUBBLE);
            i1=length(ME)+1;
            i2=length(FRAGMENTS)+1;
            i3=length(BUBBLE)+1;
        end
        for i=1:count_link_generate
            name=file_link_generate(i).name;
            L1=length(name);
            name(L1-1:L1)=[];
            fh = str2func(name);
            [ME,FRAGMENTS,BUBBLE]=fh(i1,i2,i3,ME,FRAGMENTS,BUBBLE);
        end
        cd('..');
        cd('..');
        link_data0=link_data;
        create_user_ME()
        create_user_link()
        
        file=dir(input_data_dir);
        
        for i=length(file):-1:1
            if file(i).isdir==1
                file(i)=[];
            end
        end
        
        dlg = @questdlg;
        % Call the dialog
        button = dlg('Do you want to modify the m-files?','','YES','NO','NO');
        
        if isempty(button)==true
            button='NO';
        end
        switch button
            case 'YES'
                str = {file.name};
                [index_config,exit_2] = listdlg('PromptString','Select the file. Note that you can select multiple files',...
                    'SelectionMode','multiple','ListString',str);
                if  exit_2==0
                    disp('No file selected, proceed with simulation');
                else
                    
                    for i_string=1:length(index_config)
                        edit(fullfile(file(index_config(i_string)).folder,file(index_config(i_string)).name))
                        
                        if i_string==length(index_config)
                            string_dial='Click the OK button to prosecute simulation; SAVE THE FILE';
                        else
                            string_dial='Click the OK button to modify next file; SAVE THE FILE';
                        end
                        options.WindowStyle='normal';
                        options.Interpreter='tex';
                        options.Default = 'OK';
                        MFquestdlg([ 0.5 , 0.5 ],string_dial, '','OK','OK');
                    end
                end
        end
        
        % LOAD THE RESTART FILES
    elseif flag_restart==1
        
        create_user_ME()
        create_user_link()
        
        file=dir(input_data_dir);
        
        for i=length(file):-1:1
            if file(i).isdir==1
                file(i)=[];
            end
        end
        
        dlg = @questdlg;
        % Call the dialog
        button = dlg('Do you want to modify the m-files?','','YES','NO','NO');
        
        if isempty(button)==true
            button='NO';
        end
        switch button
            case 'YES'
                file1=file;
                for i=length(file1):-1:1
                    [~,~,exp] = fileparts(file1(i).name);
                    if contains(exp,'dump')
                        file1(i)=[];
                    elseif contains(exp,'data')
                        file1(i)=[];
                    elseif contains(exp,'mat')
                        file1(i)=[];
                    end
                end
                str = {file1.name};
                [index_config,exit_2] = listdlg('PromptString','Select the file. Note that you can select multiple files',...
                    'SelectionMode','multiple','ListString',str);
                if  exit_2==0
                    disp('No file selected, proceed with simulation');
                else
                    
                    for i_string=1:length(index_config)
                        edit(fullfile(file1(index_config(i_string)).folder,file1(index_config(i_string)).name))
                        
                        if i_string==length(index_config)
                            string_dial='Click the OK button to prosecute simulation; SAVE THE FILE';
                        else
                            string_dial='Click the OK button to modify next file; SAVE THE FILE';
                        end
                        options.WindowStyle='normal';
                        options.Interpreter='tex';
                        options.Default = 'OK';
                        MFquestdlg([ 0.5 , 0.5 ],string_dial, '','OK','OK');
                    end
                end
        end
        
        
    else
        
        %% SE ETAMAX
        xmlFilePath=fullfile('GUI2Solver_Interface','Interface Files','CstDatabase_v3.xml');
        ME_OLD=population_empty_structure();
        for i=1:length(file)
            [~,~,ext] = fileparts(file(i).name);
            
            if contains(ext,'dump') || contains(ext,'data')
                dumpFilePath=fullfile(file(i).folder,file(i).name);
                %                 ME_OLD=ME;
                ME=population_empty_structure(); %-> CG
                get_ME_Objs_From_Interface(xmlFilePath,dumpFilePath);
                L_ME_OLD=length(ME_OLD);
                L_ME=length(ME);
                if(L_ME_OLD>0) % -> CG
                    ME_OLD=[ME_OLD, ME];
                    for j=1:length(ME)
                        %                         ME_OLD(L_ME_OLD+j).object_ID=L_ME_OLD+j;
                    end
                else
                    ME_OLD=ME;
                end
                %                 ME_OLD(L_ME_OLD+1:L_ME_OLD+L_ME)=ME;
                %                 ME=ME_OLD;
            end
            
        end
        ME=ME_OLD;
        
        create_user_ME()
        create_user_link()
        
        file=dir(input_data_dir);
        
        for i=length(file):-1:1
            if file(i).isdir==1
                file(i)=[];
            end
        end
        
        dlg = @questdlg;
        % Call the dialog
        button = dlg('Do you want to modify the m-files?','','YES','NO','NO');
        
        if isempty(button)==true
            button='NO';
        end
        switch button
            case 'YES'
                file1=file;
                for i=length(file1):-1:1
                    [~,~,exp] = fileparts(file1(i).name);
                    if contains(exp,'dump')
                        file1(i)=[];
                    elseif contains(exp,'data')
                        file1(i)=[];
                    elseif contains(exp,'mat')
                        file1(i)=[];
                    end
                end
                str = {file1.name};
                [index_config,exit_2] = listdlg('PromptString','Select the file. Note that you can select multiple files',...
                    'SelectionMode','multiple','ListString',str);
                if  exit_2==0
                    disp('No file selected, proceed with simulation');
                else
                    
                    for i_string=1:length(index_config)
                        edit(fullfile(file1(index_config(i_string)).folder,file1(index_config(i_string)).name))
                        
                        if i_string==length(index_config)
                            string_dial='Click the OK button to prosecute simulation; SAVE THE FILE';
                        else
                            string_dial='Click the OK button to modify next file; SAVE THE FILE';
                        end
                        options.WindowStyle='normal';
                        options.Interpreter='tex';
                        options.Default = 'OK';
                        MFquestdlg([ 0.5 , 0.5 ],string_dial, '','OK','OK');
                    end
                end
        end
        
    end
    
    
    
    %% EDIT DI ME; FRAGMENTS; BUBBLE; SETTINGS
    [t_end, t_step, t_start1, dt_SR, n_max_main_loop, voronoi_solver_ID, PROPAGATION_THRESHOLD]=cst_settings_parser(fullfile('Simulation_cases',sim_title,'cst_tot_sim_settings.txt'));
    t_start=max([t_start t_start1]);
    %% Launch the simulation
    ME_SR=ME;
    
    pause(0.01)
%     cst_main_USER
    
    isCyclic = false;
    isLoadLaunch = true;
    cst_main_base;
    
end