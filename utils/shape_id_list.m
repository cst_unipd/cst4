%> @file   shape_id_list.m
%> @brief  Function that sets the global variable SHAPE_ID_LIST
%> @author Andrea Valmorbida (andrea.valmorbida@unipd.it)

%==========================================================================
%> @brief Function that sets the global variable SHAPE_ID_LIST
%>
%> Function that sets the global variable SHAPE_ID_LIST
%>
%==========================================================================
function shape_id_list()

global SHAPE_ID_LIST;

% shape_ID of the impacted object:

% 0 - FRAGMENT
SHAPE_ID_LIST.FRAGMENT = 0;

% 1 - SOLID BOX / PLATE
SHAPE_ID_LIST.SOLID_BOX = 1;

% 2 - SOLID SPHERE
SHAPE_ID_LIST.SOLID_SPHERE = 2;

% 3 - HOLLOW SPHERE
SHAPE_ID_LIST.HOLLOW_SPHERE = 3;

% 4 - SOLID CYLINDER
SHAPE_ID_LIST.SOLID_CYLINDER = 4;

% 5 - HOLLOW CYLINDER (ELLIPSOID)
SHAPE_ID_LIST.HOLLOW_CYLINDER = 5;
% SHAPE_ID_LIST.HOLLOW_ELLIPSOID = SHAPE_ID_LIST.HOLLOW_CYLINDER;

% 6 - CONVEX HULL
SHAPE_ID_LIST.CONVEX_HULL = 6;

% 7 - TBD 1
% SHAPE_ID_LIST.TBD1 = 7;
SHAPE_ID_LIST.HOLLOW_ELLIPSOID = 7;

% 8 - TBD 2
SHAPE_ID_LIST.TBD2 = 8;

% 9 - BUBBLE
SHAPE_ID_LIST.BUBBLE = 9;


end

