%> @file    CST_LOG.m
%> @brief   Class that manages log info for CST Matlab
%> @author  Andrea Valmorbida (andrea.valmorbida@unipd.it)
%> @date    17 January 2018

%> Copyright 2017 CISAS - UNIVERSITY OF PADOVA


classdef CST_LOG < handle
    % CST_LOG class
    %   Class that manages log info for CST Matlab.
    
    % ******************** PROPERTIES *************************************
    properties (Access = private)
        
        diary_file_ID = -1;
        diary_file_name = '';
        
        error_file_ID = -1;
        error_file_name = '';
%         error_file_status = -1;
        error_count = 0;
        
        warning_file_ID = -1;
        warning_file_name = '';
%         warning_file_status = -1;
        warning_count = 0;
        
        error_disp_flag = 0;
        warning_disp_flag = 0;
        
        commwin_disp_flag = 0;

    end
    % *********************************************************************
    
    
    % ******************** METHODS ****************************************
    methods
        
        % -----------------------------------------------------------------
        function obj = CST_LOG(diary_file_name,warning_file_name,error_file_name)
            obj.diary_file_name = diary_file_name;
            obj.warning_file_name = warning_file_name;
            obj.error_file_name = error_file_name;
            obj.openFiles();
        end
        
        % -----------------------------------------------------------------
        function count = getWarningCount(obj)
            count = obj.warning_count;
        end
        
        % -----------------------------------------------------------------
        function count = getErrorCount(obj)
            count = obj.error_count;
        end
        
        % -----------------------------------------------------------------
        function diaryOn(obj)
            if ~isempty(obj.diary_file_name)
                diary(obj.diary_file_name);
                obj.diary_file_ID = 1;
            end
        end
        
        % -----------------------------------------------------------------
        function diaryOff(obj)
            diary off;
        end
        
        % -----------------------------------------------------------------
        function openFiles(obj)
            if obj.warning_file_ID <= 0
                obj.warning_file_ID = fopen(obj.warning_file_name,'w');
            end
            if obj.error_file_ID <= 0
                obj.error_file_ID = fopen(obj.error_file_name,'w');
            end
        end
        
        % -----------------------------------------------------------------
        function closeFiles(obj)
            if obj.warning_file_ID > 0
                obj.warning_file_ID = fclose(obj.warning_file_ID);
            end
            if obj.error_file_ID > 0
                obj.error_file_ID = fclose(obj.error_file_ID);
            end
        end       

        % -----------------------------------------------------------------
        function setDisplayFlags(obj,warning_disp_flag,error_disp_flag,commwin_disp_flag)
            obj.error_disp_flag = error_disp_flag;
            obj.warning_disp_flag = warning_disp_flag;
            obj.commwin_disp_flag = commwin_disp_flag;
        end
        
        % -----------------------------------------------------------------
        function warningMsg(obj,msg)
            printMsg(obj,msg,obj.warning_disp_flag,obj.warning_file_ID);
            obj.warning_count = obj.warning_count + 1;
        end
        
        % -----------------------------------------------------------------
        function errorMsg(obj,msg)
            printMsg(obj,msg,obj.error_disp_flag,obj.error_file_ID);
            obj.error_count = obj.error_count + 1;
        end
        
        % -----------------------------------------------------------------
        function printMsg(obj,msg,disp_flag,file_ID)
            % display msg
            if disp_flag==1
                disp(msg);
            end
            % print msg in warning file
            if ~isempty(file_ID)
                fprintf(file_ID,[msg '\n']);
            end
        end
        
        % -----------------------------------------------------------------
        function dispMsg(obj,msg)
            if obj.commwin_disp_flag==1
                disp(msg);
            end
        end
        
    end
    
end