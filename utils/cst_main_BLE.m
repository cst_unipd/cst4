%> @file  cst_main_BLE.m
%> @brief Collision Simulation Tool main file
%> Copyright (c) 2017 CISAS - UNIVERSITY OF PADOVA

%======================================================================
%> @mainpage Matlab CST Documentation
%>
%> @section intro Introduction
%>
%> The objective of this Study is to @b develop @b a @b tool to characterize fragments
%> clouds resulting from hypervelocity collisions involving satellites
%> and impactors of different sizes (up to a complete spacecraft).
%> The tool aims to go beyond the current capabilities of state-of-the-art breakup models,
%> thanks to the ability to simulate shock wave propagation and impact energy
%> transmission along structures in consequence of collision.
%> This feature will provide a better insight into the transition between
%> local damage and catastrophic disruption, accounting for variables not considered before,
%> e.g. the detailed configuration of the colliding objects, their overlap
%> and relative orientation. This will eventually make possible to re-assess
%> the EMR critical value in function of the variety of possible impact scenarios.
%>

function cst_main_BLE()

clear all; 
close all;
tic

% These values are overwritten if caso.mat has been created and not deleted
% by BLE_tests function

caso=51; % set =0 or =[] for automatic case 
n_max_main_loop = 20;   % maximum number of the main while loop runs
question_flag_case=0;

if(exist('caso.mat'))
    caso_mat=load('caso.mat');
    caso=caso_mat.caso;
    n_max_main_loop=caso_mat.steps;
    question_flag_case=1;
    disp(['Loaded test no.', num2str(caso),' with ', num2str(n_max_main_loop), ' steps.'])
end
% suppress question at the end of input data check

if isnumeric(caso) 
    if caso>50
        question_flag_case=1;
    end
end

% profile on -detail builtin
global ME;
global FRAGMENTS;
global BUBBLE;
global sim_title_dir;
global sim_title;
% global t;            % Current time within the simulation
global dt_SR;           % Current time step 
global t_end;        % Simulation maximum duration 
global ME_SR;   %ME updated by Structural Response
global ME_SR_tm1; %V02, copy of ME at the preavious step, used only in SR (FF)
global SR_delta; %ME displacement calculated by Structural Response
global HOLES;

global MATERIAL_LIST;
global SHAPE_ID_LIST;
global SEEDS_DISTRIBUTION;
global PROPAGATION_THRESHOLD;
global cygwin64_folder;
global voropp_folder;
global neper_folder;% global folder paths
global voropp_interface_folder;
% global hostname; % host name
global VORONOI_SOLVER_LIST;
global voronoi_solver_ID;
global CRATER_SHAPE_LIST;
global crater_shape_ID;
global HOLES_ANALYSIS_FLAG;

global plot_flag; %flag for fragmentation plottings

global link_data;

global cst_log;


%cd('..');
path_initialization();

if(~isempty(dir(fullfile(neper_folder,'Frag_ME_shape*'))))
    delete(fullfile(neper_folder,'Frag_ME_shape*'));
end
if(exist('tracking_vertex.txt','file'))
    delete('tracking_vertex.txt');
end

shape_id_list();
voronoi_solver_list();

CRATER_SHAPE_LIST.SPHERE = 10;
CRATER_SHAPE_LIST.ELLIPSOID = 11;

HOLES_ANALYSIS_FLAG=1;
if(HOLES_ANALYSIS_FLAG<2)
    crater_shape_ID = CRATER_SHAPE_LIST.SPHERE;
else
    crater_shape_ID = CRATER_SHAPE_LIST.ELLIPSOID;
end

% voronoi_solver_ID:
% 1 => Neper with bash              => VORONOI_SOLVER_LIST.NEPER_BASH
% 2 => Voro++ with bash             => VORONOI_SOLVER_LIST.VOROPP_BASH
% 3 => Voro++ from system           => VORONOI_SOLVER_LIST.VOROPP_SYS
% 4 => Voro++ with MEX interface    => VORONOI_SOLVER_LIST.VOROPP_MEX
voronoi_solver_ID = VORONOI_SOLVER_LIST.VOROPP_MEX;



%% DEBUGGING
plot_flag = 0;
debug_flag = 0; %<<<=== Activate this flag to compile cst_tracking*

if debug_flag==1
    compile_flag = 1;
    plot_flag = 1;
else
    compile_flag = 0;
end

 compile_flag = 1;
tracking_plot=0;



%%

% ------------------------
% global variable related to general settings 
% Generate material list
material_list();

% Reference shape_list 
% shape_list={...
%     '0 = Fragment',...
%     '1 = Box', ...
%     '2 = Sphere',...
%     '3 = Hollow Sphere' ,...
%     '4 = Cylinder', ...
%     '5 = Hollow Cylinder',...
%     '6 = Convex Hull', ...
%     '7 = TBD',...
%     '8 = TBD',...
%     '9 = BUBBLE'}

% Generate seeds distributions types list
seeds_distribution_list();



% Initialization of the PROPAGATION_THRESHOLD structure
[PROPAGATION_THRESHOLD] = propagation_thr_struct;

%%
% ----------------------------------------------------------------
% SCENARIO DEFINITION
% Test_cases;
%showcase_PM3;
scenario_def;

HOLES=population_empty_structure();
% ----------------------------------------------------------------
% create cst_log object
diary_file_name = fullfile(sim_title_dir,'diary.txt');
warning_file_name = fullfile(sim_title_dir,'warning.log');
error_file_name = fullfile(sim_title_dir,'error.log');

cst_log = CST_LOG(diary_file_name,warning_file_name,error_file_name);
cst_log.diaryOn;
cst_log.setDisplayFlags(1,1,1);

% diary
% diary_filename=fullfile(sim_title_dir,'diary.txt');
%diary(diary_filename);

cst_log.dispMsg(['==>> SIMULATION TITLE: ' sim_title ' <<==']);
cst_log.dispMsg(' ');

% check input data
%ME(1).object_ID = -1;

input_data_check();


% display warning / error messages
if cst_log.getWarningCount>0
    cst_log.dispMsg(['-->> ' num2str(cst_log.getWarningCount) ' warnings! Check the warning.log file in the results folder.']);
end
if cst_log.getErrorCount>0
    cst_log.dispMsg(['-->> ' num2str(cst_log.getErrorCount) ' errors! Check the error.log file in the results folder.']);
end

% decide if the simulation has to be stopped
if cst_log.getErrorCount>0
    cst_log.dispMsg('');
    cst_log.dispMsg('=======> SOME ERRORS OCCURRED <=======');
    cst_log.diaryOff;
    cst_log.closeFiles();
    return;
else
    if cst_log.getWarningCount>0 && question_flag_case==0
        cst_log.dispMsg(' ');
        continue_flag = input('Whould you like to continue the simulation? y/n [y]: ','s');
        if strcmpi(continue_flag,'n')
            cst_log.dispMsg(' ');
            cst_log.dispMsg('Simulation concluded by the user.');
            cst_log.diaryOff;
            cst_log.closeFiles();
            return;
        end
    else
        cst_log.dispMsg('');
        cst_log.dispMsg('=======> NO ERRORS OR WARNINGS <=======');
        % debugging
        %{
        cst_log.diaryOff;
        cst_log.closeFiles();
        return;
        %}
    end
end


% % check seeds distribution ID
% for k=1:length(ME)
%     if isSolidShape(ME(k).object_ID)
%         if ME(k).FRAGMENTATION_DATA.seeds_distribution_ID ~= 0
%             ME(k).FRAGMENTATION_DATA.seeds_distribution_ID = 0;
%             cst_log.warningMsg(['ME(' num2str(k) ').FRAGMENTATION_DATA.seeds_distribution_ID was set to 0 (solid shape)']);
%         end
%     else
%         if ME(k).FRAGMENTATION_DATA.seeds_distribution_ID ~= 4
%             ME(k).FRAGMENTATION_DATA.seeds_distribution_ID = 4;
%             cst_log.warningMsg(['ME(' num2str(k) ').FRAGMENTATION_DATA.seeds_distribution_ID was set to 4 (hollow shape)']);
%         end
%     end
% end


%%
%------------------------------------------------------------------

% set Bullet Physics
%ME_new=ME_struct;
if(compile_flag==1)
    mex_path_init;
    fprintf('\n');
end

%%
breakup_flag = 0;
breakup_flag_SR = 0;

collision_flag = 0; % flag used to check if a collision occurred
                    % it is used to activate the Breakup Algorithm
SR_flag = 0;        % flag used to activate the Structural Responce Algorithm


% plot LOFT model
ME_SR = ME; % ME_SR is used in the drawLOFTmodel function
%  if plot_flag==1
% %     draw3D();
% %     drawLOFTmodel();
% %     hold on
% %     plot3([FRAGMENTS(1).DYNAMICS_DATA.cm_coord(1),FRAGMENTS(1).DYNAMICS_DATA.cm_coord(1)],[FRAGMENTS(1).DYNAMICS_DATA.cm_coord(2),FRAGMENTS(1).DYNAMICS_DATA.cm_coord(2)],[FRAGMENTS(1).DYNAMICS_DATA.cm_coord(3),FRAGMENTS(1).DYNAMICS_DATA.cm_coord(3)],'mx','LineWidth',2);
%  end


n_ME0=length(ME);
n_FRAGMENTS0=length(FRAGMENTS);
n_BUBBLE0=length(BUBBLE);
n_links0=length(link_data);
%%
%Initialization of simulation timing
t_start = 0;    % initial time of the entire simulation
t_end = 10;      % final time of the entire simulation
t_step = 0.1;   % simulation step
dt_SR = 1E-6;   % Structural response timestep

% n_max_main_loop = 20;   % maximum number of the main while loop runs


pause(0.001);
figHandles = findall(groot, 'Type', 'figure');
f_num=numel(figHandles);
for f = 1:f_num
  filefigname = fullfile(sim_title_dir,'figures',['Figure', num2str(f_num-(f-1)), '_step0']);
  %saveas(figHandles(f), filefigname,'fig');
  saveas(figHandles(f), filefigname,'png');
end
% close all;

disp('========> SIMULATION STARTED <========');
fprintf('\n');

%%

%Initialization of counters
n_collision_tot = 0;%Collisions counter
ti = t_start;   % initial time of the time step
                % tf: finel time of the time step
main_loop_count = 1;    % count for the main while loop

% main loop
while ((ti<t_end) && (main_loop_count <= n_max_main_loop))
    
    disp('***********************************************');
    disp(['   Step no. = ' num2str(main_loop_count)]);
    disp(['   ti = ' num2str(ti) ' s']);
    disp('***********************************************');
    fprintf('\n');
    
    
    % =====================================================================
    % take a copy of ME @ti for Structural Response Algorithm
    if SR_flag == 1
        ME_ti = ME;
        if main_loop_count==1
            ME_SR_tm1=ME;
        end
    end
    
    
    % =====================================================================
    % call the Tracking Algorithm: evolve from ti_plus to tf_min
    % tf = toi if a collision occurred, ti+t_step otherwise
    disp('++++++++++++++++++++++++++++++++++++++++++++++++++++')
    disp('... -> Performing Tracking and Collision Detection');

    % DA TOGLIERE PER TEST NORMALI!!!! CG 28-07-18
    % VINCOLO LE PIASTRE
    for i_ME=1:2
        ME(i_ME).DYNAMICS_DATA.vel=0*ME(i_ME).DYNAMICS_DATA.vel;
        for i_holes=1:length(HOLES)
            if (HOLES(i_holes).object_ID==ME(i_ME).object_ID_index)
                if(ME(i_ME).GEOMETRY_DATA.mass>0)
                    HOLES(i_holes).DYNAMICS_DATA.vel=ME(i_ME).DYNAMICS_DATA.vel;
                else
                    HOLES(i_holes).GEOMETRY_DATA.mass=0;
                end
            end
        end
    end
  
% ME(1).DYNAMICS_DATA.vel=0*ME(1).DYNAMICS_DATA.vel; %!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! DA TOGLIERE!! Prova per WS
% ME(2).DYNAMICS_DATA.vel=0*ME(2).DYNAMICS_DATA.vel; %!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! DA TOGLIERE!! Prova per WS
    [COLLISION_DATA_tracking]=reset_structure(COLLISION_DATA_tracking);  
%     ME(2).DYNAMICS_DATA.vel = 0*ME(2).DYNAMICS_DATA.vel;
%     [ME, FRAGMENTS, BUBBLE, collision_flag, COLLISION_DATA_tracking, n_collision,toi_time] = ...
%             cst_tracking_mex_interface(ME, FRAGMENTS, BUBBLE, collision_flag, t_step);

for ik=1:length(FRAGMENTS)
    if(isnan(FRAGMENTS(ik).DYNAMICS_DATA.cm_coord))
        disp(['PRE-TRACKING FRAGMENTS(',num2str(ik),').GEOMETRY_DATA.cm_coord is NAN']);
    end
    if(isnan(FRAGMENTS(ik).DYNAMICS_DATA.vel))
        disp(['PRE-TRACKING FRAGMENTS(',num2str(ik),').DYNAMICS_DATA.vel is NAN']);
    end
end
      [ME, FRAGMENTS, BUBBLE, HOLES, collision_flag, COLLISION_DATA_tracking, n_collision,toi_time] = ...
            cst_tracking_mex_interface(ME, FRAGMENTS, BUBBLE, HOLES, collision_flag, t_step);

    clear('cst_tracking_mex_interface.mexa64');
for ik=1:length(FRAGMENTS)
    if(isnan(FRAGMENTS(ik).DYNAMICS_DATA.cm_coord))
        disp(['POST-TRACKING FRAGMENTS(',num2str(ik),').GEOMETRY_DATA.cm_coord is NAN']);
    end
    if(isnan(FRAGMENTS(ik).DYNAMICS_DATA.vel))
        disp(['POST-TRACKING FRAGMENTS(',num2str(ik),').DYNAMICS_DATA.vel is NAN']);
    end
end

    
    if((main_loop_count==1) && (tracking_plot==1))
        tracking_vertex_plot('tracking_vertex.txt');
    end
        

    FRAGMENTS = FRAGMENTS';
    BUBBLE = BUBBLE';
    HOLES = HOLES';

     if(main_loop_count==1)
%         figure
%         for i=1:length(ME)
%             if(ME(i).GEOMETRY_DATA.mass0>0)
%                 drawPoint3d(ME(i).GEOMETRY_DATA.c_hull+ME(i).DYNAMICS_INITIAL_DATA.cm_coord0','Marker','o')
%             end
%             hold on
%         end
%         hold on
%         for i=1:length(FRAGMENTS)
%             if(FRAGMENTS(i).GEOMETRY_DATA.mass0>0)
%                 drawPoint3d(FRAGMENTS(i).GEOMETRY_DATA.c_hull+FRAGMENTS(i).DYNAMICS_INITIAL_DATA.cm_coord0','Marker','*')
%             end
%             hold on
%         end
%         pause(0.01);
%         filefigname = fullfile(sim_title_dir,'figures',['Figure0_step0']);
%         %saveas(gcf, filefigname,'fig');
%         saveas(gcf, filefigname,'png');
%         close all;
        out_file=fullfile(sim_title_dir,'data',['step_0']);
        save(out_file,'ME','FRAGMENTS','BUBBLE','HOLES','ti','link_data');
     end
     %collision_flag=0; %For debugging
     
     tf = ti + toi_time;
     if(tf-ti==0)
         SR_flag =0;
     end
     for iii=1:length(BUBBLE)
         BUBBLE(iii).GEOMETRY_DATA.dimensions(1)=BUBBLE(iii).DYNAMICS_DATA.v_exp*toi_time+BUBBLE(iii).GEOMETRY_DATA.dimensions(1);
     end
             %Updating HOLES position and velocity
    for i_ME=1:length(ME)
        for i_holes=1:length(HOLES)
            if (HOLES(i_holes).object_ID==ME(i_ME).object_ID_index)
                if(ME(i_ME).GEOMETRY_DATA.mass>0)
% %                     HOLES(i_holes).DYNAMICS_DATA.cm_coord=HOLES(i_holes).DYNAMICS_INITIAL_DATA.cm_coord0+(ME(i_ME).DYNAMICS_DATA.cm_coord-ME(i_ME).DYNAMICS_INITIAL_DATA.cm_coord0);
% %                     HOLES(i_holes).DYNAMICS_DATA.cm_coord=HOLES(i_holes).DYNAMICS_DATA.cm_coord+(ME(i_ME).DYNAMICS_DATA.cm_coord-ME_SR_tm1(i_ME).DYNAMICS_DATA.cm_coord);
%                     HOLES(i_holes).DYNAMICS_DATA.cm_coord=HOLES(i_holes).DYNAMICS_DATA.cm_coord+HOLES(i_holes).DYNAMICS_DATA.vel*(tf-ti);
                    HOLES(i_holes).DYNAMICS_DATA.vel=ME(i_ME).DYNAMICS_DATA.vel;
                else
                    HOLES(i_holes).GEOMETRY_DATA.mass=0;
                end
            end
        end
    end
     
     disp(['No. of detected collisions (@ toi=',num2str(toi_time),' s) = ', num2str(n_collision)]);
     disp('... End Tracking');
     fprintf('\n');
 
    SR_delta=zeros(3,length(ME));
    % =====================================================================
    % call Structual Response if enabled
    sum_ME_mass=0;
    for i_s=1:length(ME)
        sum_ME_mass=sum_ME_mass+ME(i_s).GEOMETRY_DATA.mass;
    end
    if(sum_ME_mass<=0||isempty(link_data)==true)
        SR_flag=0;
    end
    
    if SR_flag == 1
        disp('++++++++++++++++++++++++++++++++++++++++++++++++++++')
        disp('... -> Performing Structural Response');
        
        SR_flag_calc = Structural_Response_Algorithm_ODE(ME_ti,ti,tf-ti); % -> ME_SR = ME_SR(tf_min) %-> GC CORRETTO t_step
        
        disp('... End Structural Response');
        
        %Calculating ME displacement wrt tracking
        for i_ME=1:length(ME)
            SR_delta(:,i_ME)=ME_SR(i_ME).DYNAMICS_DATA.cm_coord-ME(i_ME).DYNAMICS_DATA.cm_coord;
            ME(i_ME).DYNAMICS_DATA.vel=ME_SR(i_ME).DYNAMICS_DATA.vel;  %V02, update of ME velocity according to SR (FF)
        end 
    end
     ME_SR_tm1=ME; %V02, keep track of ME proprieties before fragmentation (FF)
    
    % =====================================================================
    % call Breakup if a collision was detected
    if collision_flag == 1
        disp('++++++++++++++++++++++++++++++++++++++++++++++++++++')
        disp('... -> Performing Breakup');
%         COLLISION_DATA_tracking
        disp('Impactors')
        COLLISION_DATA_tracking.impactor
        disp('Impact points')
        COLLISION_DATA_tracking.point
        % creating COLLISION_DATA structure from COLLISION_DATA_tracking
        [COLLISION_DATA]=collision_data_interface(COLLISION_DATA_tracking);
        disp('... ... COLLISION_DATA structure updated');
%         COLLISION_DATA
        % call Breakup Algorithm
        [KILL_LIST,COLLISION_DATA] = CST_breakup_main_UPGRADE_CST2(COLLISION_DATA,main_loop_count,tf);
        if voronoi_solver_ID == VORONOI_SOLVER_LIST.VOROPP_MEX
            clear('cst_voropp_mex_interface.mexa64');
        end
        disp('... End Breakup');

    end
    disp('++++++++++++++++++++++++++++++++++++++++++++++++++++')
    if (SR_flag == 1)
%         ME=ME_SR; 
        disp('... Updating ME Population by Structural Response calculations');
         SR_flag = SR_flag_calc;
     end  
    if(collision_flag==1)
        collision_flag = 0;
        SR_flag = 1;
%         if(length(link_data)>0)
        if(isempty(link_data)==true)
            SR_flag = 0;
        end
    end
    
    %CONTROLLARE!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    % ME gi� updatati con ME_SR??????????????????????????
    %Updating ME & HOLES position and HOLES velocity and mass
    for i_ME=1:length(ME)
        ME(i_ME).DYNAMICS_DATA.cm_coord=ME(i_ME).DYNAMICS_DATA.cm_coord+SR_delta(:,i_ME);
        for i_holes=1:length(HOLES)
            if (HOLES(i_holes).object_ID==ME(i_ME).object_ID_index)
                if(ME(i_ME).GEOMETRY_DATA.mass>0)
                    HOLES(i_holes).DYNAMICS_DATA.cm_coord=HOLES(i_holes).DYNAMICS_DATA.cm_coord+SR_delta(:,i_ME);
                    HOLES(i_holes).DYNAMICS_DATA.vel=ME(i_ME).DYNAMICS_DATA.vel;
                else
                    HOLES(i_holes).GEOMETRY_DATA.mass=0;
                end
            end
        end
    end
    
    
%     %Killing HOLES belonging to dead MEs
%     for i_ME=1:length(ME)
%         for i_holes=1:length(HOLES)
%             if (HOLES(i_holes).object_ID==ME(i_ME).object_ID_index)
%                 if(ME(i_ME).GEOMETRY_DATA.mass<=0)
%                     HOLES(i_holes).GEOMETRY_DATA.mass=0;
%                 end
%             end
%         end
%     end
% %         ME_SR_tm1=ME; %V02, keep track of ME proprieties before fragmentation (FF)

    out_file=fullfile(sim_title_dir,'data',['step_', num2str(main_loop_count)]);
    save(out_file,'ME','FRAGMENTS','BUBBLE','HOLES','ti','link_data','tf');
    
    if(exist('tracking_vertex.txt','file'))
        copyfile(fullfile('tracking_vertex.txt'),fullfile(sim_title_dir,'data',['tracking_vertex_','step_', num2str(main_loop_count),'.txt']));
    end
    
    
    pause(0.01);
    figHandles = findall(groot, 'Type', 'figure');
    f_num=numel(figHandles);
    for f = 1:f_num
      filefigname = fullfile(neper_folder,['Figure', num2str(f_num-(f-1)), '_step', num2str(main_loop_count)]);
%       saveas(figHandles(f), filefigname,'fig');
      saveas(figHandles(f), filefigname,'png');
    end
    neper_figures=dir(fullfile(neper_folder,'Figure*'));
    neper_files=dir(fullfile(neper_folder,'Frag_ME*'));
    if(~isunix)
        system('tasklist | find /i "neper.exe" && taskkill /im neper.exe /F'); 
%        system('taskkill /F /IM neper.exe');
       pause(0.01);
    end

    if(isempty(neper_figures)==0)
        movefile(fullfile(neper_folder,'Figure*'), fullfile(sim_title_dir,['neper_files_step_', num2str(main_loop_count)]));
    end
    if(isempty(neper_files)==0)
        movefile(fullfile(neper_folder,'Frag_ME*'), fullfile(sim_title_dir,['neper_files_step_', num2str(main_loop_count)]));
    end
% close all;
    % =====================================================================
    % update times for the next timestep and counters
    ti = tf;
    main_loop_count = main_loop_count + 1;
    n_collision_tot = n_collision_tot + n_collision;
    
    conservations_checks
end
disp('');
disp(['*** ',num2str(n_collision_tot),' collisions detected ***']);
disp('');
disp('=======> SIMULATION CONCLUDED <=======');

% figure
% for i=1:length(ME)
%     if(ME(i).GEOMETRY_DATA.mass>0)
%         drawPoint3d(ME(i).GEOMETRY_DATA.c_hull+ME(i).DYNAMICS_DATA.cm_coord','Marker','o')
%     end
%     hold on
% end
% hold on
% for i=1:length(FRAGMENTS)
%     if(FRAGMENTS(i).GEOMETRY_DATA.mass>0)
%         drawPoint3d(FRAGMENTS(i).GEOMETRY_DATA.c_hull+FRAGMENTS(i).DYNAMICS_DATA.cm_coord','Marker','*')
%     end
%     hold on
% end
% hold on
% for i=1:length(BUBBLE)
%     if(BUBBLE(i).GEOMETRY_DATA.mass>0)
%         drawPoint3d(BUBBLE(i).GEOMETRY_DATA.c_hull+BUBBLE(i).DYNAMICS_DATA.cm_coord','Marker','^')
%     end
%     hold on
% end
pause(0.01);
filefigname = fullfile(sim_title_dir,'figures',['Figure0_end']);
% saveas(gcf, filefigname,'fig');
saveas(gcf, filefigname,'png');
% close all;

conservations_checks;% For internal testing

% disp('=======> PostProcessing ... ');
% Post_processing;

%SAVING PROCEDURE TBD
%POSTPROCESSING PROCEDURE TBD

% profile off
% % profile viewer
% profsave(profile('info'),'profile_results_CST_ver02')
% diary off;

plotHoles;
filefigname = fullfile(sim_title_dir,'figures','HOLES');
saveas(gcf, filefigname,'png');

fprintf('\n==>> Number of Holes: %d\n',length(HOLES));
disp('Holes object IDs:');
HOLES.object_ID


% close diary, warning and error log files
cst_log.diaryOff;
cst_log.closeFiles();
RunTime=toc
save(out_file,'RunTime','-append');
simu_rep_gen
tic

save('holes_temp.mat','HOLES');

% Post_processing2_UPGRADED
% PostProcessingTime=toc
% save(out_file,'PostProcessingTime','-append');
%}
% cd('utils');
% 
% cd('..');
end