global ME;

Parameters_Setup_file0=fullfile(pwd,'set_up','Parameters_Setup0.m');
if(exist(Parameters_Setup_file0,'file'))
    
    Vc_frag_max=0;
    for i=1:length(ME)
        Vc_frag_max=max([Vc_frag_max norm(ME(i).DYNAMICS_INITIAL_DATA.vel0)]);
    end
    Vc_frag_max=Vc_frag_max*1.2;
    file_init=fopen(fullfile(pwd,'set_up','Parameters_Setup0.m'),'r');
    fileID=fopen(fullfile(pwd,'set_up','Parameters_Setup.m'),'w');
    
    while ~feof(file_init)
        tline = fgets(file_init);
        if startsWith(tline,'Vc_frag_max')
            a=strfind(tline,'=');
            b=strfind(tline,';');
            new_vel=[tline(1:a),num2str(Vc_frag_max),tline(b:end)];
            fwrite(fileID, new_vel);
        else
            fwrite(fileID, tline);
        end
    end
    
    fclose(file_init);
    fclose(fileID);
    
else
    error('Missing Parameters_Setup.m')
end