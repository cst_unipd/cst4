//> @file  cst_tracking_cpp.h
//> @brief C++ code for tracking/collision detection using Bullet Continuous Collision Detection and Physics Library by Erwin Coumans  http://continuousphysics.com/Bullet/
//> @author Cinzia Giacomuzzo (cinzia.giacomuzzo@unipd.it)
// Copyright (c) 2017 CISAS - UNIVERSITY OF PADOVA
//======================================================================


#include <cstdio>
#include <cstdlib>
#include <map>
#include <vector>
#include <iostream>
#include <iterator>
#include <string>
#include <algorithm>
#include <cmath>
#ifdef MATLAB_MEX_FILE /* Is this file being compiled as a MEX-file? */
    #include "mex.h"
#endif
#if defined(_WIN64) || defined(WIN64) || defined(_WIN32) || defined(WIN32)
    #ifndef BT_USE_DOUBLE_PRECISION
    #define BT_USE_DOUBLE_PRECISION
    #endif
#endif




#include <btBulletDynamicsCommon.h>
#include "BulletCollision/CollisionShapes/btShapeHull.h"
#include "BulletCollision/CollisionShapes/btSphereShape.h"
#include "LinearMath/btConvexHull.h"
#include "LinearMath/btGeometryUtil.h"
#include "BulletCollision/NarrowPhaseCollision/btConvexCast.h"
#include "BulletCollision/NarrowPhaseCollision/btGjkConvexCast.h"
#include "BulletCollision/NarrowPhaseCollision/btGjkPairDetector.h"
#include "BulletCollision/NarrowPhaseCollision/btPointCollector.h"
#include "BulletCollision/NarrowPhaseCollision/btGjkEpaPenetrationDepthSolver.h"
#include "BulletCollision/CollisionDispatch/btConvexConvexAlgorithm.h"
#include "BulletCollision/NarrowPhaseCollision/btContinuousConvexCollision.h"
#include "BulletCollision/NarrowPhaseCollision/btMinkowskiPenetrationDepthSolver.h"






// #include <GL/glut.h>
#include "MyDebugDraw1.h"
// #include "vecmath_cpp.h"

#ifdef __unix__ 
    #ifndef BT_USE_DOUBLE_PRECISION
    #define BT_USE_DOUBLE_PRECISION
    #endif
#endif

#include "cst_tracking_settings.h"


using namespace std;


// int verbose_flag=0; //Flag to activate diagnostics text outputs [0=no text; 1=verbose; 2=verbose++] 
// int debug_draw_flag=1;
// //=============================================================================
// 
// /*Fixed values*/
// NOT TO BE MODIFIED!!!
const double MIN_BULLET_RANGE=0.05;
const double MAX_BULLET_RANGE=10;


std::map<const btCollisionObject*,std::vector<btManifoldPoint*> > objectsCollisions;

//==============================================================================

/*Structures definition*/
//Initialization of GEOMETRY_DATA population substructure
typedef struct {
	int shape_ID;
	double dimensions[3]={0, 0, 0};
	double thick;
	double mass0;
	double mass;
	double A_M_ratio;
    vector <double> c_hull[3];
} GEOMETRY_DATA_t;

//Initialization of DYNAMICS_INITIAL_DATA population substructure
typedef struct {
	double cm_coord0[3]={0, 0, 0};
	double vel0[3]={0, 0, 0};
	double quaternions0[4]={0, 0, 0, 1};
	double w0[3]={0, 0, 0};
    double v_exp0;
} DYNAMICS_INITIAL_DATA_t;

//Initialization of DYNAMICS_DATA population substructure
typedef struct {
	double cm_coord[3]={0, 0, 0};
	double vel[3]={0, 0, 0};
	double quaternions[4]={0, 0, 0, 0};
	double w[3]={0, 0, 0};
    double v_exp;
    double virt_momentum[3]={0, 0, 0};
} DYNAMICS_DATA_t ;

//Initialization of FRAGMENTATION_DATA population substructure
typedef struct {
    int failure_ID;
    double threshold0;
    double threshold;
    int breakup_flag;
    double ME_energy_transfer_coef;
    double cMLOSS;
    double cELOSS;
    double c_EXPL;
    int seeds_distribution_ID; 
    double seeds_distribution_param1;
    double seeds_distribution_param2;
    double param_add1;
    double param_add2;
} FRAGMENTATION_DATA_t ;

//Initialization of population structure (namely ME). For FRAGMENTS and BUBBLES the same structure is used
typedef struct {
	int object_ID;
    int object_ID_index;
	int material_ID;
	GEOMETRY_DATA_t GEOMETRY_DATA;
	DYNAMICS_INITIAL_DATA_t DYNAMICS_INITIAL_DATA;
	DYNAMICS_DATA_t DYNAMICS_DATA;
    FRAGMENTATION_DATA_t FRAGMENTATION_DATA;
} ME_t ;

//Initialization of COLLISION_DATA structure with data for each collision for each timestep
struct COLLISION_DATA_t {
	int target=0;
    int target_shape;
	vector <int> impactor;
    vector <int> impactor_shape;
	vector <double> point[3];
};

vector <double> toi_tot; //vector of toi detected during simulation: data saved during simulation from dispatcher nearcallback
vector <double> toi_tot0; //vector of toi detected at first iteration: data saved during simulation from dispatcher nearcallback 
double toi_def=1;


//Definition of contact structure that will be added to contact_array vector which stores contacts data saved at each timestep
typedef struct {
    double toi;
    double contact_p_at_toi[3];
    double distance_at_toi;
    ME_t *body_A;
    ME_t *body_B;
    btCollisionObject *col_A;
    btCollisionObject *col_B;
} contact_s;


vector <contact_s> contact_array;

//Definition of HOLES_ME_t that stores temporal HOLES data
struct HOLES_ME_t {
    int ME_object_ID_index;
    vector <ME_t> HOLES_vect;
};



//======================================================================
/*! \brief Checking for WORLD_SCALING_FACTOR 
 * 
 * If WORLD_SCALING_FACTOR==0 a check for objects dimensions fitting Bullet Physics range is performed, then  
 * WORLD_SCALING_FACTOR is adapted
 *
 * @param ME_t *ME pointer to ME structure array
 * @param ME_t *FRAGMENTS pointer to FRAGMENTS structure array
 * @param ME_t *BUBBLE pointer to BUBBLE structure array
 * @param ME_t *HOLES pointer to HOLES structure array
 * @param int n_ME number of ME elements
 * @param int n_FRAGMENTS number of FRAGMENTS elements
 * @param int n_BUBBLE number of BUBBLE elements
 * @param int n_HOLES number of HOLES elements
 *
 */
 void check_WORLD_SCALING_FACTOR(ME_t *ME, ME_t *FRAGMENTS, ME_t *BUBBLE, ME_t *HOLES, int n_ME, int n_FRAGMENTS, int n_BUBBLE, int n_HOLES)
 {
    vector <double> dimensions_range;
    int i,j;
    
    if(verbose_flag>=1)
     {
        printf("Checking WORLD SCALING FACTOR...\n");
        printf("Original WORLD_SCALING_FACTOR = %f\n", WORLD_SCALING_FACTOR);
     }

    if(WORLD_SCALING_FACTOR<=0)
    {
        //Check for smallest and largest body dimension
        for (i = 0; i<n_ME;i++) 
        {
            if(ME[i].GEOMETRY_DATA.dimensions[0]>0)
            {
                    dimensions_range.push_back(ME[i].GEOMETRY_DATA.shape_ID ==1 ? ME[i].GEOMETRY_DATA.dimensions[0] : 2*(ME[i].GEOMETRY_DATA.dimensions[0]));
            }
            if(ME[i].GEOMETRY_DATA.dimensions[1]>0)
                dimensions_range.push_back(ME[i].GEOMETRY_DATA.dimensions[1]);
            if(ME[i].GEOMETRY_DATA.dimensions[2]>0)
                dimensions_range.push_back(ME[i].GEOMETRY_DATA.dimensions[2]);
        };
        for (i = 0; i<n_FRAGMENTS;i++) 
        {
            dimensions_range.push_back(2*FRAGMENTS[i].GEOMETRY_DATA.dimensions[0]);
        };
        for (i = 0; i<n_BUBBLE;i++) 
        {
            if(bubble_assigned_radius==0)
                dimensions_range.push_back(2*BUBBLE[i].GEOMETRY_DATA.dimensions[0]);
            else
                dimensions_range.push_back(2*bubble_assigned_radius);
        };
        for (i = 0; i<n_HOLES;i++) 
        {
           if(HOLES[i].GEOMETRY_DATA.shape_ID==10)
                dimensions_range.push_back(2*HOLES[i].GEOMETRY_DATA.dimensions[0]);
           else if(HOLES[i].GEOMETRY_DATA.shape_ID==11)
               dimensions_range.push_back(2*(HOLES[i].GEOMETRY_DATA.dimensions[0]+HOLES[i].GEOMETRY_DATA.dimensions[1]+HOLES[i].GEOMETRY_DATA.dimensions[2])/3);
        };

        sort(dimensions_range.begin(),dimensions_range.end());
        
        //Scaling smallest and largest dimension to Bullet range
        if(dimensions_range[dimensions_range.size()-1]>MAX_BULLET_RANGE)
        {
            WORLD_SCALING_FACTOR = (MAX_BULLET_RANGE / dimensions_range[dimensions_range.size()-1]);
            if(verbose_flag>=1)
                printf("Resizing WORLD_SCALING_FACTOR exceeding MAX_BULLET_RANGE to %f\n", WORLD_SCALING_FACTOR);
        }
        if(dimensions_range[0]<MIN_BULLET_RANGE)
        {
            WORLD_SCALING_FACTOR = (MIN_BULLET_RANGE / dimensions_range[0]); 
            if(verbose_flag>=1)
                printf("Resizing WORLD_SCALING_FACTOR exceeding MIN_BULLET_RANGE to %f\n", WORLD_SCALING_FACTOR);
        } 
        if(WORLD_SCALING_FACTOR<=0)
        {
            WORLD_SCALING_FACTOR=1;
        }
        if(verbose_flag>=1)
            printf("New WORLD_SCALING_FACTOR = %f\n", WORLD_SCALING_FACTOR);
    }
   //General inputs redefinition
    if(WORLD_SCALING_FACTOR<1)
    {
        CCD_MOTION_THRESHOLD_FACTOR=WORLD_SCALING_FACTOR*CCD_MOTION_THRESHOLD_FACTOR;
        COLLISION_SHAPE_MARGIN=WORLD_SCALING_FACTOR*COLLISION_SHAPE_MARGIN;
        contactProcessingThreshold=WORLD_SCALING_FACTOR*contactProcessingThreshold;
        contactBreakingThreshold=WORLD_SCALING_FACTOR*contactBreakingThreshold;
        ME_threshold_distance=WORLD_SCALING_FACTOR*ME_threshold_distance;
        general_threshold_distance=WORLD_SCALING_FACTOR*general_threshold_distance;
        if(verbose_flag>1)
            printf("Contact/collision thresholds rescaled because WORLD_SCALING_FACTOR<1\n");
    }
 }

// void reshape(GLsizei width, GLsizei height) {  // GLsizei for non-negative integer
//    // Compute aspect ratio of the new window
//    if (height == 0) height = 1;                // To prevent divide by 0
//    GLfloat aspect = (GLfloat)width / (GLfloat)height;
// 
//    // Set the viewport to cover the new window
//    glViewport(0, 0, width, height);
// 
//    // Set the aspect ratio of the clipping volume to match the viewport
//    glMatrixMode(GL_PROJECTION);  // To operate on the Projection matrix
//    glLoadIdentity();             // Reset
//    // Enable perspective projection with fovy, aspect, zNear and zFar
//    gluPerspective(45.0f, aspect, 0.1f, 100.0f);
// }

//======================================================================
/*! \brief World objects vertices for debug drawer
 * 
 * Incomplete debug drawer to save world objects vertices; OpenGL is NOT activated.
 *Data are saved in an ASCII file as reported in MyDebugDraw1.h
 *
 * @param btDiscreteDynamicsWorld *dynamicsWorld pointer to the world
 *
 */ 
void tracking_debug_drawer(btDiscreteDynamicsWorld *dynamicsWorld)
{
//     int argc = 1;
//   char *argv[1] = {(char*)"Something"};
//    glutInit(&argc, argv);                 // Initialize GLUT
//    glutInitDisplayMode(GLUT_DOUBLE); // Enable double buffered mode
//    glutInitWindowSize(1000, 1000);   // Set the window's initial width & height
//    glutInitWindowPosition(50, 50); // Position the window's initial top-left cor
// 
//    glutCreateWindow("OpenGL Setup Test"); // Create a window with the given titl
//     glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
// //    glLoadIdentity();//load identity matrix
//    glMatrixMode(GL_MODELVIEW);     // To operate on model-view matrix


 MyDebugDraw *draw   =   new MyDebugDraw;
  dynamicsWorld->setDebugDrawer(draw);

  dynamicsWorld->getDebugDrawer()->setDebugMode( btIDebugDraw::DBG_DrawWireframe );

  dynamicsWorld->debugDrawWorld();
  
//   glutSwapBuffers();

//   glutReshapeFunc(reshape);       // Register callback handler for window re-size event
// glutMainLoop();

    
}

//======================================================================
/*! \brief Checking for body inclusion in ME holes  
 * 
 * Check for inclusion in HOLES belonging to the impacting objects.
 * The check is performed at the entrance OR the estimated exit point.
 *
 * @param ME_t HOLES_ME_body_i single hole belonging to the impacted object
 * @param ME_t *impactor_body impactor object
 * @param double toi_as_timestep_fraction time of impact as fraction of timestep
 *
 * @retval bool TRUE if the object is included in the hole
 */
bool check_for_holes_inclusion(ME_t HOLES_ME_body_i, ME_t impactor_body,double toi_as_timestep_fraction)
{
    btScalar distance_hole_impactor;
    int i_h, n_v, i_v;
    double q0, q1, q2, q3;
    double a11, a12, a13, a21, a22, a23, a31, a32, a33;
    double e00, e01, e02, e03, e11, e12, e13, e22, e23, e33;
    double X_el, Y_el, Z_el;
    
    btVector3 hole_cm(HOLES_ME_body_i.DYNAMICS_DATA.cm_coord[0],HOLES_ME_body_i.DYNAMICS_DATA.cm_coord[1],HOLES_ME_body_i.DYNAMICS_DATA.cm_coord[2]); //hole center
    btVector3 hole_vel(HOLES_ME_body_i.DYNAMICS_DATA.vel[0],HOLES_ME_body_i.DYNAMICS_DATA.vel[1],HOLES_ME_body_i.DYNAMICS_DATA.vel[2]); //hole velocity
    hole_cm=hole_cm+hole_vel*toi_as_timestep_fraction;
    n_v=(impactor_body.GEOMETRY_DATA.c_hull[0]).size();
    //Check for each convex hull vertex inclusion in the hole
    for(i_v=0; i_v<n_v; i_v++)
    {          
        btVector3 c_hull_vertex(impactor_body.GEOMETRY_DATA.c_hull[0][i_v],impactor_body.GEOMETRY_DATA.c_hull[1][i_v],impactor_body.GEOMETRY_DATA.c_hull[2][i_v]);
        btVector3 c_hull_cm(impactor_body.DYNAMICS_DATA.cm_coord[0],impactor_body.DYNAMICS_DATA.cm_coord[1],impactor_body.DYNAMICS_DATA.cm_coord[2]);
        btVector3 c_hull_vel(impactor_body.DYNAMICS_DATA.vel[0],impactor_body.DYNAMICS_DATA.vel[1],impactor_body.DYNAMICS_DATA.vel[2]);
        btQuaternion impactor_body_quat(-impactor_body.DYNAMICS_DATA.quaternions[1],-impactor_body.DYNAMICS_DATA.quaternions[2],-impactor_body.DYNAMICS_DATA.quaternions[3],impactor_body.DYNAMICS_DATA.quaternions[0]);
        c_hull_vertex=c_hull_vertex.rotate(impactor_body_quat.getAxis(), impactor_body_quat.getAngle());
        c_hull_cm=c_hull_cm+c_hull_vel*toi_as_timestep_fraction;
        c_hull_vertex=c_hull_vertex+c_hull_cm;
        
        //For spherical hole check on distance between vertex and hole center smaller that hole radius
        if((HOLES_ME_body_i.GEOMETRY_DATA.shape_ID==10 && HOLES_analysis_sphere_flag==1) || (HOLES_ME_body_i.GEOMETRY_DATA.shape_ID==11 && HOLES_analysis_sphere_flag==1 && HOLES_analysis_ellipsoid_flag==0))
        {
            distance_hole_impactor=c_hull_vertex.distance(hole_cm);//(c_hull_vertex-hole_cm).length();
            if(distance_hole_impactor>HOLES_ME_body_i.GEOMETRY_DATA.dimensions[0])
            {
                if(verbose_flag>=1)
                    printf("Shape%d Index%d Vertex%d: at least one vertex is outside hole with Index%d belonging to ME with ID%d\n",impactor_body.GEOMETRY_DATA.shape_ID,impactor_body.object_ID_index,i_v,HOLES_ME_body_i.object_ID_index,HOLES_ME_body_i.object_ID);
                 return false;
            }
            else
            {
                if(verbose_flag>=2)
                    printf("Shape%d Index%d Vertex%d: totally inside hole with Index%d belonging to ME with ID%d\n",impactor_body.GEOMETRY_DATA.shape_ID,impactor_body.object_ID_index,i_v,HOLES_ME_body_i.object_ID_index,HOLES_ME_body_i.object_ID);
            }
        }
        //For ellisoidal hole check if vertex satisfy ellispoid equation
        else if(HOLES_ME_body_i.GEOMETRY_DATA.shape_ID==11 && HOLES_analysis_ellipsoid_flag==1)
        {
            q0=HOLES_ME_body_i.DYNAMICS_DATA.quaternions[0];
            q1=HOLES_ME_body_i.DYNAMICS_DATA.quaternions[1];
            q2=HOLES_ME_body_i.DYNAMICS_DATA.quaternions[2];
            q3=HOLES_ME_body_i.DYNAMICS_DATA.quaternions[3];

            e00=q0*q0;
            e01=q0*q1;
            e02=q0*q2;
            e03=q0*q3;
            e11=q1*q1;
            e12=q1*q2;
            e13=q1*q3;
            e22=q2*q2;
            e23=q2*q3;
            e33=q3*q3;
            a11=e00+e11-e22-e33;
            a12=2*(e12-e03);
            a13=2*(e13+e02);
            a21=2*(e12+e03);
            a22=e00-e11+e22-e33;
            a23=2*(e23-e01);
            a31=2*(e13-e02);
            a32=2*(e23+e01);
            a33=e00-e11-e22+e33;

            c_hull_vertex=c_hull_vertex-hole_cm;
            X_el=a11*c_hull_vertex.getX()+a12*c_hull_vertex.getY()+a13*c_hull_vertex.getZ();//-hole_cm.getX();//+(hole_cm.getX()-c_hull_cm.getX());
            Y_el=a21*c_hull_vertex.getX()+a22*c_hull_vertex.getY()+a23*c_hull_vertex.getZ();//-hole_cm.getY();//+(hole_cm.getY()-c_hull_cm.getY());
            Z_el=a31*c_hull_vertex.getX()+a32*c_hull_vertex.getY()+a33*c_hull_vertex.getZ();//-hole_cm.getZ();//+(hole_cm.getZ()-c_hull_cm.getZ());
            if(((X_el*X_el)/(HOLES_ME_body_i.GEOMETRY_DATA.dimensions[0]*HOLES_ME_body_i.GEOMETRY_DATA.dimensions[0])+(Y_el*Y_el)/(HOLES_ME_body_i.GEOMETRY_DATA.dimensions[1]*HOLES_ME_body_i.GEOMETRY_DATA.dimensions[1])+(Z_el*Z_el)/(HOLES_ME_body_i.GEOMETRY_DATA.dimensions[2]*HOLES_ME_body_i.GEOMETRY_DATA.dimensions[2]))>1)
            {
                if(verbose_flag>=1)
                    printf("Shape%d Index%d: at least one vertex is outside hole with Index%d belonging to ME with ID%d\n",impactor_body.GEOMETRY_DATA.shape_ID,impactor_body.object_ID_index,HOLES_ME_body_i.object_ID_index,HOLES_ME_body_i.object_ID);
                return false;
            }
            else
            {
                if(verbose_flag>=2)
                    printf("Shape%d Index%d Vertex%d: inside hole with Index%d belonging to ME with ID%d\n",impactor_body.GEOMETRY_DATA.shape_ID,impactor_body.object_ID_index,i_v,HOLES_ME_body_i.object_ID_index,HOLES_ME_body_i.object_ID);
            }
        }
    }
    return true;
 }

//======================================================================
/*! \brief Checking for body inclusion in ME holes  
 * 
 * When an impact is dectected before saving impact data a check for inclusion in HOLES belonging to the impacting objects is performed.
 * The check is performed at the entrance and at the estimated exit point for ellipsoidal holes while for spherical holes is performed only at the entrance point.
 *
 * @param btCollisionObject* col0 object 0 of collision pair
 * @param vector <ME_t> HOLES_ME_body HOLES belonging to col0
 * @param ME_t *impactor_body impactor object
 * @param double toi_as_timestep_fraction time of impact as fraction of timestep
 *
 * @retval bool TRUE if the object is included in the hole both at entrance and exit point
 */
bool check_for_holes_inclusion_inout(btCollisionObject *col00, vector <ME_t> HOLES_ME_body0, ME_t *impactor_body0,double toi_as_timestep_fraction)
{
   bool hole_inclusion_in=false;
   bool hole_inclusion_out=false;

   btVector3 paj, pbj;
   double x1, y1,z1,x2,y2,z2,x3,y3,z3;
   double a_plane, b_plane, c_plane;                                          
   btScalar d_plane;
   btVector3 vert_array0;
   btVector3 vert_array1;
   btVector3 vert_array2;
   double prec_tol=1e-15;
   
   ME_t impactor_body;
   impactor_body=*impactor_body0;
   btCollisionObject col0;
   col0=*col00;
   vector <ME_t> HOLES_ME_body;
   HOLES_ME_body=HOLES_ME_body0;
   
    if(verbose_flag>=1)
        printf("ME with ID%d has %d holes\n",HOLES_ME_body[0].object_ID,HOLES_ME_body.size());
    for(int i_h=0;i_h<HOLES_ME_body.size(); i_h++)
    {
        if(HOLES_ME_body[i_h].GEOMETRY_DATA.mass>0)
        {
            if(verbose_flag>=1)
                printf("\n*** Check for inclusion of impactor with shapeID%d and index%d with HOLE with object_ID%d and object_ID_index%d\n", impactor_body.GEOMETRY_DATA.shape_ID,impactor_body.object_ID_index,HOLES_ME_body[i_h].object_ID,HOLES_ME_body[i_h].object_ID_index);   

        //Check for inclusion at entrance point
        hole_inclusion_in=check_for_holes_inclusion(HOLES_ME_body[i_h],impactor_body,toi_as_timestep_fraction);
        if(!hole_inclusion_in)
        {
            if(verbose_flag>=1)
                printf("... object NOT in hole at entrance\n");
//             return false;
        }
        else
        {
            if(verbose_flag>=1)
                printf("... object in hole at entrance\n");
//             if(HOLES_ME_body[i_h].GEOMETRY_DATA.shape_ID==10)
//                 return true;
        }
        hole_inclusion_out=hole_inclusion_in;


        //Check for inclusion at the estimated exit point for  holes
//         if(hole_inclusion_in && HOLES_ME_body[i_h].GEOMETRY_DATA.shape_ID==11)
        if(hole_inclusion_in)// && HOLES_ME_body[i_h].GEOMETRY_DATA.shape_ID==11)
        {
            //Predicting objects positions
            btVector3 origin_start_col1(impactor_body.DYNAMICS_DATA.cm_coord[0], impactor_body.DYNAMICS_DATA.cm_coord[1], impactor_body.DYNAMICS_DATA.cm_coord[2]);
            btQuaternion obj_quat_start_col1(impactor_body.DYNAMICS_DATA.quaternions[1],impactor_body.DYNAMICS_DATA.quaternions[2],impactor_body.DYNAMICS_DATA.quaternions[3],impactor_body.DYNAMICS_DATA.quaternions[0]);
//             origin_start_col1=origin_start_col1*WORLD_SCALING_FACTOR;
            btTransform startTransform_col1(obj_quat_start_col1,origin_start_col1);
            
            btTransform pred_trans_col1;
            btVector3 vel0_col1(impactor_body.DYNAMICS_DATA.vel[0],impactor_body.DYNAMICS_DATA.vel[1],impactor_body.DYNAMICS_DATA.vel[2]);
//             vel0_col1=vel0_col1*WORLD_SCALING_FACTOR;
            btVector3 ang0_col1(impactor_body.DYNAMICS_DATA.w[0],impactor_body.DYNAMICS_DATA.w[1],impactor_body.DYNAMICS_DATA.w[2]);
            btTransformUtil::integrateTransform(startTransform_col1,vel0_col1,ang0_col1,0,pred_trans_col1);

            btVector3 vel0_col0(((ME_t *) col0.getUserPointer())->DYNAMICS_DATA.vel[0],((ME_t *) col0.getUserPointer())->DYNAMICS_DATA.vel[1],((ME_t *) col0.getUserPointer())->DYNAMICS_DATA.vel[2]);
//             vel0_col0=vel0_col0*WORLD_SCALING_FACTOR;

            btTransform pred_trans_col1_t1;
            btVector3 vrel=vel0_col1-vel0_col0;
            btTransformUtil::integrateTransform(startTransform_col1,vrel,ang0_col1,1,pred_trans_col1_t1);

            // Getting convex hull edges for each facet; constructing the plane defined by two consecutive edges and find intersections between hole velocity vector and facet planes
            // Check for inclusion will be performed at the intersection point having the largest distance from impact point
             btVector3 o1t0=pred_trans_col1.getOrigin();
             btVector3 o1t1=pred_trans_col1_t1.getOrigin();
             
             btShapeHull* hull= new btShapeHull((const btConvexShape*)col0.getCollisionShape());
            hull->buildHull(((const btConvexShape*)col0.getCollisionShape())->getMargin());

            int numverts = hull->numVertices();
            const btVector3* vtx = hull->getVertexPointer();
            //create a new btConvexHull, using those points
//              btConvexHullShape* convexHullShape0 = new btConvexHullShape(vtx,numverts);
             btConvexHullShape* convexHullShape0 = new btConvexHullShape(*(hull->getVertexPointer()),numverts,sizeof(btVector3));

             double ME_q0, ME_q1, ME_q2, ME_q3;
             double ME_a11,ME_a12,ME_a13,ME_a21,ME_a22,ME_a23,ME_a31,ME_a32,ME_a33;
            ME_q0=((ME_t *) col0.getUserPointer())->DYNAMICS_DATA.quaternions[0];
            ME_q1=((ME_t *) col0.getUserPointer())->DYNAMICS_DATA.quaternions[1];
            ME_q2=((ME_t *) col0.getUserPointer())->DYNAMICS_DATA.quaternions[2];
            ME_q3=((ME_t *) col0.getUserPointer())->DYNAMICS_DATA.quaternions[3];

            ME_a11=ME_q0*ME_q0+ME_q1*ME_q1-ME_q2*ME_q2-ME_q3*ME_q3;
            ME_a12=2*(ME_q1*ME_q2-ME_q0*ME_q3);
            ME_a13=2*(ME_q1*ME_q3+ME_q0*ME_q2);
            ME_a21=2*(ME_q1*ME_q2+ME_q0*ME_q3);
            ME_a22=ME_q0*ME_q0-ME_q1*ME_q1+ME_q2*ME_q2-ME_q3*ME_q3;
            ME_a23=2*(ME_q2*ME_q3-ME_q0*ME_q1);
            ME_a31=2*(ME_q1*ME_q3-ME_q0*ME_q2);
            ME_a32=2*(ME_q2*ME_q3+ME_q0*ME_q1);
            ME_a33=ME_q0*ME_q0-ME_q1*ME_q1-ME_q2*ME_q2+ME_q3*ME_q3;


             btAlignedObjectArray<btPlane> planeEquations;
             btAlignedObjectArray<btVector3> plane_xyzmin;
             btAlignedObjectArray<btVector3> plane_xyzmax;
             planeEquations.clear();
             int ok_edge=0;
             for(int i_edge=0; i_edge<convexHullShape0->getNumEdges(); i_edge++)
             {
                 ok_edge=0;
                 convexHullShape0->getEdge(i_edge, vert_array0, vert_array1);
                
                 for(int j_edge=0; j_edge<convexHullShape0->getNumEdges(); j_edge++)
                 {
                   ok_edge=0;
                   if(j_edge!=i_edge)
                   {
                         convexHullShape0->getEdge(j_edge,paj,pbj);
                         if(abs(vert_array0.getX()-paj.getX())<=prec_tol && abs(vert_array0.getY()-paj.getY())<=prec_tol && abs(vert_array0.getZ()-paj.getZ())<=prec_tol)
                         {
                            ok_edge=1;
                            vert_array2=pbj;   
                         }
                         else if(abs(vert_array0.getX()-pbj.getX())<=prec_tol && abs(vert_array0.getY()-pbj.getY())<=prec_tol && abs(vert_array0.getZ()-pbj.getZ())<=prec_tol)
                         {
                            ok_edge=1;
                            vert_array2=paj;
                         }
                         else if(abs(vert_array1.getX()-paj.getX())<=prec_tol && abs(vert_array1.getY()-paj.getY())<=prec_tol && abs(vert_array1.getZ()-paj.getZ())<=prec_tol)
                         {
                            ok_edge=1;
                            vert_array2=pbj;  
                         }             
                         else if(abs(vert_array1.getX()-pbj.getX())<=prec_tol && abs(vert_array1.getY()-pbj.getY())<=prec_tol && abs(vert_array1.getZ()-pbj.getZ())<=prec_tol)
                         {
                            ok_edge=1;
                            vert_array2=paj;
                         }

                         if(ok_edge==1)
                         {
//                              x1=(vert_array0.getX()+col0.getWorldTransform().getOrigin().getX())/WORLD_SCALING_FACTOR;
//                              y1=(vert_array0.getY()+col0.getWorldTransform().getOrigin().getY())/WORLD_SCALING_FACTOR;
//                              z1=(vert_array0.getZ()+col0.getWorldTransform().getOrigin().getZ())/WORLD_SCALING_FACTOR;
//                              x2=(vert_array1.getX()+col0.getWorldTransform().getOrigin().getX())/WORLD_SCALING_FACTOR;
//                              y2=(vert_array1.getY()+col0.getWorldTransform().getOrigin().getY())/WORLD_SCALING_FACTOR;
//                              z2=(vert_array1.getZ()+col0.getWorldTransform().getOrigin().getZ())/WORLD_SCALING_FACTOR;
//                              x3=(vert_array2.getX()+col0.getWorldTransform().getOrigin().getX())/WORLD_SCALING_FACTOR;
//                              y3=(vert_array2.getY()+col0.getWorldTransform().getOrigin().getY())/WORLD_SCALING_FACTOR;
//                              z3=(vert_array2.getZ()+col0.getWorldTransform().getOrigin().getZ())/WORLD_SCALING_FACTOR;
                             
                                x1=ME_a11*vert_array0.getX()+ME_a12*vert_array0.getY()+ME_a13*vert_array0.getZ();
                                y1=ME_a21*vert_array0.getX()+ME_a22*vert_array0.getY()+ME_a23*vert_array0.getZ();
                                z1=ME_a31*vert_array0.getX()+ME_a32*vert_array0.getY()+ME_a33*vert_array0.getZ();
                                x2=ME_a11*vert_array1.getX()+ME_a12*vert_array1.getY()+ME_a13*vert_array1.getZ();
                                y2=ME_a21*vert_array1.getX()+ME_a22*vert_array1.getY()+ME_a23*vert_array1.getZ();
                                z2=ME_a31*vert_array1.getX()+ME_a32*vert_array1.getY()+ME_a33*vert_array1.getZ();
                                x3=ME_a11*vert_array2.getX()+ME_a12*vert_array2.getY()+ME_a13*vert_array2.getZ();
                                y3=ME_a21*vert_array2.getX()+ME_a22*vert_array2.getY()+ME_a23*vert_array2.getZ();
                                z3=ME_a31*vert_array2.getX()+ME_a32*vert_array2.getY()+ME_a33*vert_array2.getZ();

                                
                             x1=(x1+col0.getWorldTransform().getOrigin().getX())/WORLD_SCALING_FACTOR;
                             y1=(y1+col0.getWorldTransform().getOrigin().getY())/WORLD_SCALING_FACTOR;
                             z1=(z1+col0.getWorldTransform().getOrigin().getZ())/WORLD_SCALING_FACTOR;
                             x2=(x2+col0.getWorldTransform().getOrigin().getX())/WORLD_SCALING_FACTOR;
                             y2=(y2+col0.getWorldTransform().getOrigin().getY())/WORLD_SCALING_FACTOR;
                             z2=(z2+col0.getWorldTransform().getOrigin().getZ())/WORLD_SCALING_FACTOR;
                             x3=(x3+col0.getWorldTransform().getOrigin().getX())/WORLD_SCALING_FACTOR;
                             y3=(y3+col0.getWorldTransform().getOrigin().getY())/WORLD_SCALING_FACTOR;
                             z3=(z3+col0.getWorldTransform().getOrigin().getZ())/WORLD_SCALING_FACTOR;


                             a_plane=(y2-y1)*(z3-z1)-(y3-y1)*(z2-z1);
                             b_plane=-((x2-x1)*(z3-z1)-(x3-x1)*(z2-z1));
                             c_plane=(x2-x1)*(y3-y1)-(x3-x1)*(y2-y1);
                             d_plane=-(x1*a_plane+y1*b_plane+z1*c_plane);


                             plane_xyzmin.push_back(btVector3(min(x1,min(x2,x3)),min(y1,min(y2,y3)),min(z1,min(z2,z3))));
                             plane_xyzmax.push_back(btVector3(max(x1,max(x2,x3)),max(y1,max(y2,y3)),max(z1,max(z2,z3))));                 

                             btVector3 abc_plane(a_plane,b_plane,c_plane);
                             btPlane plane(abc_plane, d_plane);
                             if(verbose_flag>=2)
                                  printf("plane.normal=[%f,%f,%f], plane.dist=%f\n",plane.normal.getX(),plane.normal.getY(),plane.normal.getZ(),plane.dist);
                             planeEquations.push_back(plane);
                            }
                    }
                }
             }         
                       
             vector <btVector3> intersection_points;
             vector <btScalar> intersection_points_distance1;
             intersection_points.clear();
             intersection_points_distance1.clear();
             for(int i_plane=0;i_plane<planeEquations.size();i_plane++)
             {
                   btVector3 o1t0_o1t1_dif(o1t1-o1t0);
                   double dn= (planeEquations[i_plane].normal).dot(o1t0_o1t1_dif); 
                   double t = -(planeEquations[i_plane].dist+(planeEquations[i_plane].normal).dot(o1t0))/dn;
                    btVector3 intersec_p(o1t0+(o1t0_o1t1_dif*t));
//                     printf("plane [n,d]=[(%f,%f,%f),%f]\n",planeEquations[i_plane].normal.getX(),planeEquations[i_plane].normal.getY(),planeEquations[i_plane].normal.getZ(),planeEquations[i_plane].dist);
                   if(isfinite(intersec_p.getX()) && isfinite(intersec_p.getY()) && isfinite(intersec_p.getZ()))
                   {
                       if(intersec_p.getX()>=plane_xyzmin[i_plane].getX()-prec_tol && intersec_p.getX()<=plane_xyzmax[i_plane].getX()+prec_tol && intersec_p.getY()>=plane_xyzmin[i_plane].getY()-prec_tol && intersec_p.getY()<=plane_xyzmax[i_plane].getY()+prec_tol && intersec_p.getZ()>=plane_xyzmin[i_plane].getZ()-prec_tol && intersec_p.getZ()<=plane_xyzmax[i_plane].getZ()+prec_tol)
                       {
                           intersection_points.push_back(intersec_p);
                           intersection_points_distance1.push_back(o1t0.distance(intersec_p));
                       }
                   }
             }
             
             int maxo1t0_index;
             btVector3 def_intersection_p;
            if(intersection_points.size()>0)
            {
                maxo1t0_index = std::max_element(intersection_points_distance1.begin(), intersection_points_distance1.end()) - intersection_points_distance1.begin();
                def_intersection_p.setX((intersection_points[maxo1t0_index]).getX());
                def_intersection_p.setY((intersection_points[maxo1t0_index]).getY());
                def_intersection_p.setZ((intersection_points[maxo1t0_index]).getZ());
                if(verbose_flag>=2)
                    printf("def_intersection_p=[%f,%f,%f]\n",def_intersection_p.getX(),def_intersection_p.getY(),def_intersection_p.getZ());
                impactor_body.DYNAMICS_DATA.cm_coord[0]=def_intersection_p.getX();// /WORLD_SCALING_FACTOR;
                impactor_body.DYNAMICS_DATA.cm_coord[1]=def_intersection_p.getY();// /WORLD_SCALING_FACTOR;
                impactor_body.DYNAMICS_DATA.cm_coord[2]=def_intersection_p.getZ();// /WORLD_SCALING_FACTOR;

                hole_inclusion_out=check_for_holes_inclusion(HOLES_ME_body[i_h], impactor_body,0);
//                 if(hole_inclusion_out)
//                 {
//                     printf("... object in hole at exit\n");
//                 }
//                 else
//                 {
//                     printf("... object NOT in hole at exit\n");
//                 }
             }
            else
            {
                   hole_inclusion_out=true;
//                 hole_inclusion_out=false;
            }
            if(hole_inclusion_out)
            {
                if(verbose_flag>=1)
                    printf("... object in hole at exit\n");
                return hole_inclusion_out;
            }
            else
            {
                if(verbose_flag>=1)
                    printf("... object NOT in hole at exit\n");
            }

          delete hull;
          delete convexHullShape0;

         }
        }
    }
     return hole_inclusion_out;
}


//======================================================================
/*! \brief Checking for penetrating objects 
 * 
 * Each detected pair is checked for penetration at the biginning of the simulation; if true no collision is detected
 *
 * @param btCollisionObject* col0 object 0 of collision pair
 * @param btCollisionObject* col1 object 1 of collision pair
 *
 * @retval bool TRUE if penetration is detected
 */
bool check_penetration(btCollisionObject* col0,btCollisionObject* col1, double allowed_distance)
{
   btConvexShape* convex0 = static_cast<btConvexShape*>(col0->getCollisionShape());
   btConvexShape* convex1 = static_cast<btConvexShape*>(col1->getCollisionShape());

             
    btVector3 origin_start_col0(((ME_t *) col0->getUserPointer())->DYNAMICS_DATA.cm_coord[0], ((ME_t *) col0->getUserPointer())->DYNAMICS_DATA.cm_coord[1], ((ME_t *) col0->getUserPointer())->DYNAMICS_DATA.cm_coord[2]);
    btQuaternion obj_quat_start_col0(((ME_t *) col0->getUserPointer())->DYNAMICS_DATA.quaternions[1],((ME_t *) col0->getUserPointer())->DYNAMICS_DATA.quaternions[2],((ME_t *) col0->getUserPointer())->DYNAMICS_DATA.quaternions[3],((ME_t *) col0->getUserPointer())->DYNAMICS_DATA.quaternions[0]);
    origin_start_col0=origin_start_col0*WORLD_SCALING_FACTOR;
    btTransform startTransform_col0(obj_quat_start_col0,origin_start_col0);

    btTransform pred_trans_col0;
    btVector3 vel0_col0(((ME_t *) col0->getUserPointer())->DYNAMICS_DATA.vel[0],((ME_t *) col0->getUserPointer())->DYNAMICS_DATA.vel[1],((ME_t *) col0->getUserPointer())->DYNAMICS_DATA.vel[2]);
    vel0_col0=vel0_col0*WORLD_SCALING_FACTOR;
    btVector3 ang0_col0(((ME_t *) col0->getUserPointer())->DYNAMICS_DATA.w[0],((ME_t *) col0->getUserPointer())->DYNAMICS_DATA.w[1],((ME_t *) col0->getUserPointer())->DYNAMICS_DATA.w[2]);
    btTransformUtil::integrateTransform(startTransform_col0,vel0_col0,ang0_col0,0,pred_trans_col0);

    btVector3 origin_start_col1(((ME_t *) col1->getUserPointer())->DYNAMICS_DATA.cm_coord[0], ((ME_t *) col1->getUserPointer())->DYNAMICS_DATA.cm_coord[1], ((ME_t *) col1->getUserPointer())->DYNAMICS_DATA.cm_coord[2]);
    btQuaternion obj_quat_start_col1(((ME_t *) col1->getUserPointer())->DYNAMICS_DATA.quaternions[1],((ME_t *) col1->getUserPointer())->DYNAMICS_DATA.quaternions[2],((ME_t *) col1->getUserPointer())->DYNAMICS_DATA.quaternions[3],((ME_t *) col1->getUserPointer())->DYNAMICS_DATA.quaternions[0]);
    origin_start_col1=origin_start_col1*WORLD_SCALING_FACTOR;
    btTransform startTransform_col1(obj_quat_start_col1,origin_start_col1);

    btTransform pred_trans_col1;
    btVector3 vel0_col1(((ME_t *) col1->getUserPointer())->DYNAMICS_DATA.vel[0],((ME_t *) col1->getUserPointer())->DYNAMICS_DATA.vel[1],((ME_t *) col1->getUserPointer())->DYNAMICS_DATA.vel[2]);
    vel0_col1=vel0_col1*WORLD_SCALING_FACTOR;
    btVector3 ang0_col1(((ME_t *) col1->getUserPointer())->DYNAMICS_DATA.w[0],((ME_t *) col1->getUserPointer())->DYNAMICS_DATA.w[1],((ME_t *) col1->getUserPointer())->DYNAMICS_DATA.w[2]);
    btTransformUtil::integrateTransform(startTransform_col1,vel0_col1,ang0_col1,0,pred_trans_col1);

    btGjkEpaPenetrationDepthSolver epa;
    btVoronoiSimplexSolver voronoiSimplex;
    btGjkPairDetector	convexConvex(convex0 ,convex1,&voronoiSimplex,&epa);

    btPointCollector gjkOutput;
    btGjkPairDetector::ClosestPointInput input;
    input.m_transformA = pred_trans_col0;
    input.m_transformB = pred_trans_col1;
    convexConvex.getClosestPoints(input, gjkOutput,0);
    
    if(gjkOutput.m_distance<-allowed_distance)//CCD_ALLOWED_PENETRATION)
    {
         if(verbose_flag>=1)
            printf("Objects (shape%d, ID%d, index%d) and (shape%d, ID%d, index%d) are penetrating: no collision is detected\n",((ME_t *) col0->getUserPointer())->GEOMETRY_DATA.shape_ID, ((ME_t *) col0->getUserPointer())->object_ID, ((ME_t *) col0->getUserPointer())->object_ID_index,((ME_t *) col1->getUserPointer())->GEOMETRY_DATA.shape_ID, ((ME_t *) col1->getUserPointer())->object_ID, ((ME_t *) col1->getUserPointer())->object_ID_index);
         return true;
    }
    else
    {
         if(verbose_flag>=1)
            printf("Objects (shape%d, ID%d, index%d) and (shape%d, ID%d, index%d) are NOT penetrating: collision detection is performed\n",((ME_t *) col0->getUserPointer())->GEOMETRY_DATA.shape_ID, ((ME_t *) col0->getUserPointer())->object_ID, ((ME_t *) col0->getUserPointer())->object_ID_index,((ME_t *) col1->getUserPointer())->GEOMETRY_DATA.shape_ID, ((ME_t *) col1->getUserPointer())->object_ID, ((ME_t *) col1->getUserPointer())->object_ID_index);   
    }
    return false;
}





//======================================================================
/*! \brief Time of Impact calculation for not default collision algorithm
 * 
 * Calculation if TOI for collision algorithms with gjk_flag>0
 *
 * @param btCollisionObject* col0 pointer to the first collision pair object
 * @param btCollisionObject* col1 pointer to the second collision pair object
 *
 * @retval bool TRUE if a collision is detected
 */

bool toi_calculation_new(btCollisionObject* col0,btCollisionObject* col1, contact_s *contact_elem)
{
   contact_s contact_elem_new;

   btScalar resultFraction = btScalar(1.);
   btConvexShape* convex0 = static_cast<btConvexShape*>(col0->getCollisionShape());
   btConvexShape* convex1 = static_cast<btConvexShape*>(col1->getCollisionShape());
   btSphereShape   sphere0(col0->getCcdSweptSphereRadius());
   btSphereShape   sphere1(col1->getCcdSweptSphereRadius());
   bool discard_collision;
    btGjkEpaPenetrationDepthSolver epa;
    btVoronoiSimplexSolver voronoiSimplex;

   //Check for penetrating objects
   discard_collision=check_penetration(col0,col1,CCD_ALLOWED_PENETRATION);
    if(discard_collision)
    {
       return false;
    }


   btConvexCast::CastResult result;
//    btVoronoiSimplexSolver voronoiSimplex;
   bool toi_calc_res;
   bool toi_calc_res0;
   //SubsimplexConvexCast ccd0(&sphere,min0,&voronoiSimplex);
   if(gjk_flag==1)
   {
     btGjkConvexCast ccd0( convex0 ,&sphere1,&voronoiSimplex);
     toi_calc_res0=ccd0.calcTimeOfImpact(col0->getWorldTransform(),col0->getInterpolationWorldTransform(),
           col1->getWorldTransform(),col1->getInterpolationWorldTransform(),result);
       if (toi_calc_res0)
       {
           //store result.m_fraction in both bodies
           if (col0->getHitFraction()> result.m_fraction)
                   col0->setHitFraction( result.m_fraction );
           if (col1->getHitFraction() > result.m_fraction)
                   col1->setHitFraction( result.m_fraction);
           if (resultFraction > result.m_fraction)
                   resultFraction = result.m_fraction;
       }

     btGjkConvexCast ccd1( &sphere0, convex1,&voronoiSimplex);
     toi_calc_res=ccd1.calcTimeOfImpact(col0->getWorldTransform(),col0->getInterpolationWorldTransform(),
           col1->getWorldTransform(),col1->getInterpolationWorldTransform(),result);
   }
   else if(gjk_flag==2)
   {
     btGjkConvexCast ccd0( convex0 ,convex1,&voronoiSimplex);
     toi_calc_res0=ccd0.calcTimeOfImpact(col0->getWorldTransform(),col0->getInterpolationWorldTransform(),
           col1->getWorldTransform(),col1->getInterpolationWorldTransform(),result);
       if (toi_calc_res0)
       {
           //store result.m_fraction in both bodies
           if (col0->getHitFraction()> result.m_fraction)
                   col0->setHitFraction( result.m_fraction );
           if (col1->getHitFraction() > result.m_fraction)
                   col1->setHitFraction( result.m_fraction);
           if (resultFraction > result.m_fraction)
                   resultFraction = result.m_fraction;
       }


       btGjkConvexCast ccd1( convex0 ,convex1,&voronoiSimplex);
       toi_calc_res=ccd1.calcTimeOfImpact(col0->getWorldTransform(),col0->getInterpolationWorldTransform(),
           col1->getWorldTransform(),col1->getInterpolationWorldTransform(),result);
       
   }
   else if(gjk_flag==3)
   {
       btContinuousConvexCollision ccd1(convex0 ,convex1,&voronoiSimplex,&epa);
       toi_calc_res=ccd1.calcTimeOfImpact(col0->getWorldTransform(),col0->getInterpolationWorldTransform(),
           col1->getWorldTransform(),col1->getInterpolationWorldTransform(),result);
   }

   if (toi_calc_res)
   {
       //store result.m_fraction in both bodies

       if (col0->getHitFraction()> result.m_fraction)
               col0->setHitFraction( result.m_fraction );

       if (col1->getHitFraction() > result.m_fraction)
               col1->setHitFraction( result.m_fraction);

       if (resultFraction > result.m_fraction)
               resultFraction = result.m_fraction;
       
    if(verbose_flag>=1)
    printf("result.m_hitPoint c0=[%f,%f,%f]\n",result.m_hitPoint.getX(),result.m_hitPoint.getY(),result.m_hitPoint.getZ());
   contact_elem_new.toi=resultFraction;
   contact_elem_new.contact_p_at_toi[0]=result.m_hitPoint.getX();
   contact_elem_new.contact_p_at_toi[1]=result.m_hitPoint.getY();
   contact_elem_new.contact_p_at_toi[2]=result.m_hitPoint.getZ();
   contact_elem_new.body_A=((ME_t *) col0->getUserPointer());
   contact_elem_new.body_B=((ME_t *) col1->getUserPointer());
   contact_elem_new.col_A=col0;
   contact_elem_new.col_B=col1;

   contact_elem_new.distance_at_toi=0;//gjkOutput.m_distance;

   *contact_elem=contact_elem_new;
   
   return true;
   }
//    else
       return false;

}

//======================================================================
/*! \brief Defining a specific nearcallback for bullet physics dispatcher 
 * 
 * Default Dispatcher nearcallback will be override including query for DISCRETE or CONTINUOUS COLLISION DETECTION and TIME OF IMPACT calculation
 *
 * @param btBroadphasePair& collisionPair pair of colliding objects in broadphase
 * @param btCollisionDispatcher& dispatcher collision dispatcher
 * @param const btDispatcherInfo& dispatchInfo information from dispatcher
 */
void CST_NearCallback(btBroadphasePair& collisionPair, btCollisionDispatcher& dispatcher, const btDispatcherInfo& dispatchInfo)
{
        btScalar toi;
        toi=-1;
        int find_coll_algo=0;
        bool discard_collision_gjk0;
        /*Retrieving btCollisionObject data from input collision pair*/
         btCollisionObject* colObj0 = (btCollisionObject*)(collisionPair.m_pProxy0->m_clientObject);
         btCollisionObject* colObj1 = (btCollisionObject*)(collisionPair.m_pProxy1->m_clientObject);
         
        int shape0=((ME_t *) colObj0->getUserPointer())->GEOMETRY_DATA.shape_ID;
        int shape1=((ME_t *) colObj1->getUserPointer())->GEOMETRY_DATA.shape_ID;
        if((((shape0>0) && (shape0<9)) || ((shape1>0) && (shape1<9))) && (shape0!=10 && shape1!=10 && shape0!=11 && shape1!=11))
        {

             /*Checking for collision objects settings*/
            if (dispatcher.needsCollision(colObj0,colObj1))
            {
                /*Retrieving btCollisionObjectWrapper data from input collision pair*/
                btCollisionObjectWrapper colObj0_wrap(0,colObj0->getCollisionShape(),colObj0,colObj0->getWorldTransform(),0,0);
                btCollisionObjectWrapper colObj1_wrap(0,colObj1->getCollisionShape(),colObj1,colObj1->getWorldTransform(),0,0);



              /*Checking for collision algorithm assignment. If not defined, dispatcher automatically find 
               * an algorithm coherent with objects geometry and collision settings. 
               * Dispatcher will keep algorithms persistent in the collision pair*/


              if (!collisionPair.m_algorithm)
              {
                find_coll_algo=1;
                collisionPair.m_algorithm = dispatcher.findAlgorithm(&colObj0_wrap, &colObj1_wrap,0); //collision algorithm automatic assignment //OK!!!

              }
               if (collisionPair.m_algorithm)
               {
                 btManifoldResult contactPointResult = btManifoldResult(&colObj0_wrap,&colObj1_wrap);
//                  contact_life_time=contactPointResult.getPersistentManifold.getLifeTime

                  if (dispatchInfo.m_dispatchFunc == btDispatcherInfo::DISPATCH_DISCRETE)
                  {
                    //discrete collision detection query
                      if(verbose_flag>1)
                           printf("... discrete collision detection query ...\n");
                     collisionPair.m_algorithm->processCollision(&colObj0_wrap,&colObj1_wrap,dispatchInfo,&contactPointResult);
                     toi=1;
                 } 
                  else
                 {
                    //continuous collision detection query, time of impact (toi)

                      if(gjk_flag==0)
                      {
                        if(verbose_flag>1)
                              printf("... continuous collision detection query, time of impact (toi) ...\n");
                        discard_collision_gjk0=check_penetration(colObj0,colObj1,CCD_ALLOWED_PENETRATION);
                        if(!discard_collision_gjk0)
                        {
                            toi = collisionPair.m_algorithm->calculateTimeOfImpact(colObj0,colObj1,dispatchInfo,&contactPointResult);
                            if(verbose_flag>1)
                                   printf("toi_find=%f\n",toi);

                            if(toi==1 && find_coll_algo==1)
                            {
                                btPersistentManifold *mf= new btPersistentManifold(colObj0,colObj1,0,contactBreakingThreshold,contactProcessingThreshold);              
                                btVoronoiSimplexSolver* simplex = new btVoronoiSimplexSolver();
                                btMinkowskiPenetrationDepthSolver* pdSolver = new btMinkowskiPenetrationDepthSolver();
                                btCollisionAlgorithmConstructionInfo ci;
                                btConvexConvexAlgorithm convex_algo( mf, ci, &colObj0_wrap, &colObj1_wrap, simplex, pdSolver, numPerturbationIterations, minimumPointsPerturbationThreshold );

                                toi = convex_algo.calculateTimeOfImpact(colObj0,colObj1,dispatchInfo,&contactPointResult);
                                if(verbose_flag>1)
                                       printf("toi_convex_algo=%f\n",toi);

                                delete mf;
                                delete simplex;
                                delete pdSolver;
                            }
                        }

                      }
                      else if(gjk_flag>=1)
                      {
                        contact_s contact_gjk;
                        bool true_contact_gjk;
//                         contact_gjk=toi_calculation(colObj0,colObj1);
                        true_contact_gjk=toi_calculation_new(colObj0,colObj1,&contact_gjk);
                        if(true_contact_gjk)
                        {
                            contact_array.push_back(contact_gjk);
                            if(verbose_flag>1)
                                   printf("toi gjk=%f\n",contact_gjk.toi);    
                            toi=contact_gjk.toi;
                        }
                      }
                    if(toi>-1)
                    {
                        toi_tot.push_back(toi);
                        if(verbose_flag>1)
                        {
                            printf("toi_tot.push_back(%f)\n",toi);
                            printf("shape0=%d ID=%d, shape1=%d ID=%d\n",shape0,((ME_t *) colObj0->getUserPointer())->object_ID_index,shape1,((ME_t *) colObj1->getUserPointer())->object_ID_index);
                        }
                    

                        if (dispatchInfo.m_timeOfImpact > toi)
                           dispatchInfo.m_timeOfImpact = toi; //assigning effective toi to dispacher
                      }

    //                 if(find_coll_algo==1)
    //                 {
    //                     cout <<"free0" << endl;
    //                     dispatcher.freeCollisionAlgorithm(collisionPair.m_algorithm);
    //                     cout <<"free1" << endl;
    //                 }
                 }
             }
         }
    }
}//ending CST_NearCallback


//======================================================================
/*! \brief Populating dynamic world with objects defined in population (ME, FRAGMENTS, BUBBLE) structure 
 * 
 * @param population ME_t structure containing ME or FRAGMENTS or BUBBLE population objects
 * @param n_population Number of objects in population
 * @param dynamicsWorld pointer to btDiscreteDynamicsWorld 
 * @param collisionShapes pointer to the array of collision shapes
 */
void populate_dynamicWorld(ME_t population[], int n_population, btDiscreteDynamicsWorld *dynamicsWorld, btAlignedObjectArray<btCollisionShape*> collisionShapes)
{
// % Reference shape_list 
// % shape_list={...
// %     '0 = Fragment',...
// %     '1 = Box', ...
// %     '2 = Sphere',...
// %     '3 = Hollow Sphere' ,...
// %     '4 = Cylinder', ...
// %     '5 = Hollow Cylinder',...
// %     '6 = Convex Hull', ...
// %     '7 = TBD',...
// %     '8 = TBD',...
// %     '9 = BUBBLE',...
// %     '10 = SPHERICAL HOLES',...
// %     '11 = ELLIPSOID HOLES'}
    
 for (int i = 0; i < n_population; ++i) {  
     if(population[i].GEOMETRY_DATA.mass>0) //Check for not killed objects
     {
     	double ccd_radius; //CCD sphere radius for continuous collision detection process
        btCollisionShape *colShape; //collision shape representing each population object
        btShapeHull *Shape_Hull; //convex hull representing object envelope
                 
        /* Assigning Bullet type shape for each population object */
		switch (population[i].GEOMETRY_DATA.shape_ID){
            //Fragment (as convex hull or sphere)
            case 0: {  
                int n_v0=(population[i].GEOMETRY_DATA.c_hull[0]).size();
                if(n_v0>1)
                {
                    colShape = new btConvexHullShape();
                    vector <double> x_array;
                    vector <double> y_array;
                    vector <double> z_array;
                    for(int i_v=0; i_v<n_v0; i_v++)
                    {          
                     btVector3 c_hull_v(population[i].GEOMETRY_DATA.c_hull[0][i_v], population[i].GEOMETRY_DATA.c_hull[1][i_v], population[i].GEOMETRY_DATA.c_hull[2][i_v]);
                     ((btConvexHullShape*)colShape)->addPoint(c_hull_v);
                     x_array.push_back(population[i].GEOMETRY_DATA.c_hull[0][i_v]);
                     y_array.push_back(population[i].GEOMETRY_DATA.c_hull[1][i_v]);
                     z_array.push_back(population[i].GEOMETRY_DATA.c_hull[2][i_v]);
                    }

                    //calculation for ccd_radius

                    double max_diff_x=abs(-*min_element(x_array.begin(), x_array.end())+*max_element(x_array.begin(), x_array.end()));
                    double max_diff_y=abs(-*min_element(y_array.begin(), y_array.end())+*max_element(y_array.begin(), y_array.end()));
                    double max_diff_z=abs(-*min_element(z_array.begin(), z_array.end())+*max_element(z_array.begin(), z_array.end()));
                    ccd_radius=0.5*(max_diff_x+max_diff_y+max_diff_z)/3*WORLD_SCALING_FACTOR;
                    x_array.clear();
                    y_array.clear();
                    z_array.clear();
                }
                else
                {
                    btScalar sphere_radius(population[i].GEOMETRY_DATA.dimensions[0]);
                    colShape = new btSphereShape(sphere_radius);
                    ccd_radius=population[i].GEOMETRY_DATA.dimensions[0];
                }
                break;
			};
            //Box
			case 1: {
				btVector3 box_dim(0.5*population[i].GEOMETRY_DATA.dimensions[0], 0.5*population[i].GEOMETRY_DATA.dimensions[1], 0.5*population[i].GEOMETRY_DATA.dimensions[2]);
				colShape = new btBoxShape(box_dim);
				ccd_radius=((box_dim.getX()+box_dim.getY()+box_dim.getZ())/3);
                break;
			};
            //Sphere 
			case 2: {
				btScalar sphere_radius(population[i].GEOMETRY_DATA.dimensions[0]);
				colShape = new btSphereShape(sphere_radius);
				ccd_radius=population[i].GEOMETRY_DATA.dimensions[0];
                break;
			};
            //Hollow Sphere
            case 3: {
				btScalar sphere_radius(population[i].GEOMETRY_DATA.dimensions[0]);
				colShape = new btSphereShape(sphere_radius);
				ccd_radius=population[i].GEOMETRY_DATA.dimensions[0];
                break;
			};

            //Cylinder
			case 4: {
				btVector3 cyl_dim(population[i].GEOMETRY_DATA.dimensions[0], population[i].GEOMETRY_DATA.dimensions[0], population[i].GEOMETRY_DATA.dimensions[2]/2); //cylinder with axis along z; 0 and 1st data=radius, 2nd datum=height/2,We were here! FF+GS
				colShape = new btCylinderShapeZ(cyl_dim);
				ccd_radius=0.5*0.5*(population[i].GEOMETRY_DATA.dimensions[0]*2+population[i].GEOMETRY_DATA.dimensions[2]);
                break;
			};
            //Hollow Cylinder
			case 5: {
				btVector3 cyl_dim(population[i].GEOMETRY_DATA.dimensions[0], population[i].GEOMETRY_DATA.dimensions[0], population[i].GEOMETRY_DATA.dimensions[2]/2); //cylinder with axis along z; 0 and 1st data=radius, 2nd datum=height/2,We were here! FF+GS
				colShape = new btCylinderShapeZ(cyl_dim);
				ccd_radius=0.5*0.5*(population[i].GEOMETRY_DATA.dimensions[0]*2+population[i].GEOMETRY_DATA.dimensions[2]);
                break;
			};
            //ConvexHull
            case 6: {
				colShape = new btConvexHullShape();
                int n_v=sizeof(population[i].GEOMETRY_DATA.c_hull)/(sizeof(double)*3);
                vector <double> x_array6;
                vector <double> y_array6;
                vector <double> z_array6;
                for(int i_v=0; i_v<n_v; i_v++)
                {          
                 btVector3 c_hull_v(population[i].GEOMETRY_DATA.c_hull[0][i_v], population[i].GEOMETRY_DATA.c_hull[1][i_v], population[i].GEOMETRY_DATA.c_hull[2][i_v]);
                 ((btConvexHullShape*)colShape)->addPoint(c_hull_v);
                 x_array6.push_back(population[i].GEOMETRY_DATA.c_hull[0][i_v]);
                 y_array6.push_back(population[i].GEOMETRY_DATA.c_hull[1][i_v]);
                 z_array6.push_back(population[i].GEOMETRY_DATA.c_hull[2][i_v]);
                }
                double max_diff_x6=abs(-*min_element(x_array6.begin(), x_array6.end())+*max_element(x_array6.begin(), x_array6.end()));
                double max_diff_y6=abs(-*min_element(y_array6.begin(), y_array6.end())+*max_element(y_array6.begin(), y_array6.end()));
                double max_diff_z6=abs(-*min_element(z_array6.begin(), z_array6.end())+*max_element(z_array6.begin(), z_array6.end()));
                ccd_radius=0.5*(max_diff_x6+max_diff_y6+max_diff_z6)/3;  
                x_array6.clear();
                y_array6.clear();
                z_array6.clear();
                break;
            };

            case 7: {
                break;
            };
            case 8: {
                break;
            };

           //Bubble
           case 9: { //BUBBLE
               if(bubble_collision_flag==1) //Check for activation of Collision flag for BUBBLEs 
               {
                   btScalar sphere_radius;
                   if(bubble_assigned_radius==0)
                   {
                        sphere_radius=population[i].GEOMETRY_DATA.dimensions[0];
                   }
                   else
                   {
                        sphere_radius=bubble_assigned_radius;
                   }
                    colShape = new btSphereShape(sphere_radius);
                    ccd_radius=sphere_radius;
                    break;
               }
               else
               {
                   break;
               }
               
			};
            
            //SPHERICAL HOLES
            case 10: { //Sphere                
               btScalar hole_radius;
               hole_radius=population[i].GEOMETRY_DATA.dimensions[0];
               colShape = new btSphereShape(hole_radius);
               ccd_radius=hole_radius;
               break;
			};

            //ELLIPSOIDAL HOLES
            case 11: { //Ell.                
               btScalar hole_radius;
               btVector3 hole_pos0;
               hole_pos0.setX(0);
               hole_pos0.setY(0);
               hole_pos0.setZ(0);
               hole_radius=1;//population[i].GEOMETRY_DATA.dimensions[0];
               colShape = new btMultiSphereShape(&hole_pos0,&hole_radius,1);
               btVector3 ellipsoid_scaling_factor(population[i].GEOMETRY_DATA.dimensions[0],population[i].GEOMETRY_DATA.dimensions[1],population[i].GEOMETRY_DATA.dimensions[2]);
               colShape->setLocalScaling(ellipsoid_scaling_factor); //scaling collision shape
               ccd_radius=(population[i].GEOMETRY_DATA.dimensions[0]+population[i].GEOMETRY_DATA.dimensions[1]+population[i].GEOMETRY_DATA.dimensions[2])/3;
               break;
			};
            
            default: {
                break;
            };
	
		};
        
        /*Getting convex hull (array of vertices) representing object envelope and saving data in GEOMETRY_DATA.c_hull field*/
        
        int n_v1=(population[i].GEOMETRY_DATA.c_hull[0]).size();
        if(n_v1<2)
        {
            population[i].GEOMETRY_DATA.c_hull[0].clear();
            population[i].GEOMETRY_DATA.c_hull[1].clear();
            population[i].GEOMETRY_DATA.c_hull[2].clear();
            Shape_Hull= new btShapeHull((const btConvexShape*)colShape);
            Shape_Hull->buildHull(((const btConvexShape*)colShape)->getMargin());
            for(int i_shape_hull_n_v=0; i_shape_hull_n_v<Shape_Hull->numVertices(); i_shape_hull_n_v++)
            {
                const btVector3 * vertex(Shape_Hull->getVertexPointer()+i_shape_hull_n_v);
                population[i].GEOMETRY_DATA.c_hull[0].push_back(vertex->getX());
                population[i].GEOMETRY_DATA.c_hull[1].push_back(vertex->getY());
                population[i].GEOMETRY_DATA.c_hull[2].push_back(vertex->getZ());
            }
            delete Shape_Hull;
        }



        /*Settings for collision shapes*/
        colShape->setMargin(COLLISION_SHAPE_MARGIN);
        btVector3 scaling_factor(WORLD_SCALING_FACTOR,WORLD_SCALING_FACTOR,WORLD_SCALING_FACTOR);
        colShape->setLocalScaling(scaling_factor); //scaling collision shape
        collisionShapes.push_back(colShape);
        btScalar mass=population[i].GEOMETRY_DATA.mass;
        btVector3 localInertia(0.0f, 0.0f, 0.0f);
        colShape->calculateLocalInertia(mass, localInertia);
        
        
        /*Retrieving position and orientation of population object*/
        btTransform startTransform;
        startTransform.setIdentity();
		btVector3 origin(population[i].DYNAMICS_DATA.cm_coord[0], population[i].DYNAMICS_DATA.cm_coord[1], population[i].DYNAMICS_DATA.cm_coord[2]);
        origin=origin*WORLD_SCALING_FACTOR; //scaling position
        btQuaternion obj_quat(-population[i].DYNAMICS_DATA.quaternions[1],-population[i].DYNAMICS_DATA.quaternions[2],-population[i].DYNAMICS_DATA.quaternions[3],population[i].DYNAMICS_DATA.quaternions[0]);
        startTransform.setRotation(obj_quat);
        startTransform.setOrigin(origin);

		/*Creating a rigid body from collision object settings and assigning population object dynamics*/
        btDefaultMotionState *myMotionState = new btDefaultMotionState(startTransform);
        btRigidBody *body = new btRigidBody(btRigidBody::btRigidBodyConstructionInfo(mass, myMotionState, colShape, localInertia));
		btVector3 vel(population[i].DYNAMICS_DATA.vel[0], population[i].DYNAMICS_DATA.vel[1], population[i].DYNAMICS_DATA.vel[2]);
        vel=vel*WORLD_SCALING_FACTOR; //scaling velocity
		body->setLinearVelocity(vel);
        btVector3 ang_vel(population[i].DYNAMICS_DATA.w[0], population[i].DYNAMICS_DATA.w[1], population[i].DYNAMICS_DATA.w[2]);
		body->setAngularVelocity(ang_vel);
        
        /*Continuous collision detection settings*/
        ccd_radius=ccd_radius*WORLD_SCALING_FACTOR;
		body->setCcdMotionThreshold(CCD_MOTION_THRESHOLD_FACTOR*ccd_radius); 
		body->setCcdSweptSphereRadius(CCD_SWEPT_SPHERE_RADIUS_FACTOR*ccd_radius);
        
        /* Keeping user ID and pointers of ME_t original structure*/
		//body->setUserIndex(population[i].object_ID_index);
//        body->setUserIndex(i+1);
        body->setUserIndex(i);
        body->setUserPointer(&population[i]);
        
        /*Adding the rigid body to the bullet dynamics world*/
        dynamicsWorld->addRigidBody(body);
//         dynamicsWorld->addRigidBody(body,COL_OBJECT, OBJECT_CollidesWith);
     }
 }
         
} 



//======================================================================
/*! \brief Checking for collisions between dynamics world objects if standard collision detection algorithm is selected 
 * 
 * @param dynamicsWorld pointer to btDiscreteDynamicsWorld 
 * @param COLLISION_DATA_temp pointer to the COLLISION_DATA type structure containing collision info
 *
 * @retval n_contacts_found_tot Number of valid contacts found
 */

int checkCollisions(btDiscreteDynamicsWorld *m_dynamicsWorld, HOLES_ME_t HOLES_ME_temp[],COLLISION_DATA_t *COLLISION_DATA_temp, double time_pred)
{
    //map<btCollisionObject*, CollisionInfo> newContacts;

    if(verbose_flag>=1)
        printf("Performing classic checkCollision\n");
    
    int n_contacts_found_tot=0; //number of total contacts found
    bool in_HOLES=false;
    
    /* Browsing all possible collision manifolds */ 
    int numManifolds = m_dynamicsWorld->getDispatcher()->getNumManifolds();
    if(verbose_flag>=1)
          printf("Number of manifolds = %d\n",numManifolds);
   for (int ii=0; ii<numManifolds; ii++)
   {
       if(verbose_flag>1)
           printf("-------------\n");
     //Analysing manifolds persistents during overlapping
     btPersistentManifold* contactManifold = m_dynamicsWorld->getDispatcher()->getManifoldByIndexInternal(ii);
     

     //Retrieving collision objects
      const btCollisionObject *bodyA;
      const btCollisionObject *bodyB;
      const btCollisionObject *bodyA_0 =  contactManifold->getBody0();
	  const btCollisionObject *bodyB_0 =  contactManifold->getBody1();      

      //Retrieving original shape ID
      int shape0_0=((ME_t *) bodyA_0->getUserPointer())->GEOMETRY_DATA.shape_ID;
      int shape1_0=((ME_t *) bodyB_0->getUserPointer())->GEOMETRY_DATA.shape_ID;
      int shape0, shape1;
      
      //Checking for target object to be a ME
      if(shape0_0==0 || shape0_0==9)
      {
                bodyA =  bodyB_0;
                bodyB =  bodyA_0;
                shape0=shape1_0;
                shape1=shape0_0;
      }
      else
      {
                bodyA =  bodyA_0;
                bodyB =  bodyB_0;
                shape0=shape0_0;
                shape1=shape1_0;   
      }
          

      /*Setting collision thresholds*/
      contactManifold->setContactProcessingThreshold(contactProcessingThreshold);
      contactManifold->setContactBreakingThreshold(contactBreakingThreshold);
      btScalar contact_threshold_distance;
      contact_threshold_distance=general_threshold_distance;
      //Assigning a different threshold distance in case of impact between two MEs
      if(((shape0>0) && (shape0<9)) && ((shape1>0) && (shape1<9)))
      {
          contact_threshold_distance=ME_threshold_distance;  
      }

      /*Performing collision check on pairs including at least one of the body is a ME*/
      if(((shape0>0) && (shape0<9)) || ((shape1>0) && (shape1<9)))
      {
           //Expliciting type of object 
           char target_type_str[80];
           char impactor_type_str[80];
           
           strcpy(target_type_str,"ME");
           if((shape0>0) && (shape0<9)){
               strcpy(target_type_str,"ME");
           } else if (shape0==0){
               strcpy(target_type_str,"Fragment");
           } else if(shape0==9){
               strcpy(target_type_str,"Bubble");
           }
           
           if((shape1>0) && (shape1<9)){
               strcpy(impactor_type_str,"ME");
           } else if (shape1==0){
               strcpy(impactor_type_str,"Fragment");
           } else if(shape1==9){
               strcpy(impactor_type_str,"Bubble");
           } 

           
   
           
           //Refreshign contact points by updating positions
         if(refresh_contactpoints_flag==1){
             contactManifold ->refreshContactPoints(bodyA->getWorldTransform(), bodyB->getWorldTransform());
         }      

            btTransform startTransformA;
            startTransformA.setIdentity();
            btVector3 origin_startA(((ME_t *) bodyA->getUserPointer())->DYNAMICS_DATA.cm_coord[0], ((ME_t *) bodyA->getUserPointer())->DYNAMICS_DATA.cm_coord[1], ((ME_t *) bodyA->getUserPointer())->DYNAMICS_DATA.cm_coord[2]);
            btQuaternion obj_quat_startA(((ME_t *) bodyA->getUserPointer())->DYNAMICS_DATA.quaternions[1],((ME_t *) bodyA->getUserPointer())->DYNAMICS_DATA.quaternions[2],((ME_t *) bodyA->getUserPointer())->DYNAMICS_DATA.quaternions[3],((ME_t *) bodyA->getUserPointer())->DYNAMICS_DATA.quaternions[0]);
            startTransformA.setRotation(obj_quat_startA);
            origin_startA=origin_startA*WORLD_SCALING_FACTOR;
            startTransformA.setOrigin(origin_startA);
             btTransform pred_transA;
             btVector3 vel0A(((ME_t *) bodyA->getUserPointer())->DYNAMICS_DATA.vel[0],((ME_t *) bodyA->getUserPointer())->DYNAMICS_DATA.vel[1],((ME_t *) bodyA->getUserPointer())->DYNAMICS_DATA.vel[2]);
             vel0A=vel0A*WORLD_SCALING_FACTOR;
             btVector3 ang0A(((ME_t *) bodyA->getUserPointer())->DYNAMICS_DATA.w[0],((ME_t *) bodyA->getUserPointer())->DYNAMICS_DATA.w[1],((ME_t *) bodyA->getUserPointer())->DYNAMICS_DATA.w[2]);
             btTransformUtil::integrateTransform(startTransformA,vel0A,ang0A,time_pred,pred_transA);

            btTransform startTransformB;
            startTransformB.setIdentity();
            btVector3 origin_startB(((ME_t *) bodyB->getUserPointer())->DYNAMICS_DATA.cm_coord[0], ((ME_t *) bodyB->getUserPointer())->DYNAMICS_DATA.cm_coord[1], ((ME_t *) bodyB->getUserPointer())->DYNAMICS_DATA.cm_coord[2]);
            btQuaternion obj_quat_startB(((ME_t *) bodyB->getUserPointer())->DYNAMICS_DATA.quaternions[1],((ME_t *) bodyB->getUserPointer())->DYNAMICS_DATA.quaternions[2],((ME_t *) bodyB->getUserPointer())->DYNAMICS_DATA.quaternions[3],((ME_t *) bodyB->getUserPointer())->DYNAMICS_DATA.quaternions[0]);
            startTransformB.setRotation(obj_quat_startB);
            origin_startB=origin_startB*WORLD_SCALING_FACTOR;
            startTransformB.setOrigin(origin_startB);
             btTransform pred_transB;
             btVector3 vel0B(((ME_t *) bodyB->getUserPointer())->DYNAMICS_DATA.vel[0],((ME_t *) bodyB->getUserPointer())->DYNAMICS_DATA.vel[1],((ME_t *) bodyB->getUserPointer())->DYNAMICS_DATA.vel[2]);
             vel0B=vel0B*WORLD_SCALING_FACTOR;
             btVector3 ang0B(((ME_t *) bodyB->getUserPointer())->DYNAMICS_DATA.w[0],((ME_t *) bodyB->getUserPointer())->DYNAMICS_DATA.w[1],((ME_t *) bodyB->getUserPointer())->DYNAMICS_DATA.w[2]);
             btTransformUtil::integrateTransform(startTransformB,vel0B,ang0B,time_pred,pred_transB);

             
             if(verbose_flag>1) 
                    printf("time_pred=%f\n",time_pred);
// // //              contactManifold ->refreshContactPoints(pred_transA, pred_transB); //Replaced by lines from:  pt.m_positionWorldOnA = pred_transA( pt.m_localPointA ); 
             

             

// //           int index0=bodyA->getUserIndex();
// //           int index1=bodyB->getUserIndex();
          int index0=((ME_t *) bodyA->getUserPointer())->object_ID_index;
          int index1=((ME_t *) bodyB->getUserPointer())->object_ID_index;


          /* Check all contacts points */ 
          int numContacts = contactManifold->getNumContacts();
          if(verbose_flag>=1)
          {
              printf("Target: Type=%s, ID=%d; Impactor: Type=%s, ID=%d => ",target_type_str,index0,impactor_type_str,index1);
              printf("Num. of contacts=%d\n",numContacts);
          }
          int n_contacts_found=0;
          for (int jc=0;jc<numContacts;jc++)
          {
                
            btManifoldPoint& pt = contactManifold->getContactPoint(jc);
            //Updating contact points to toi
            pt.m_positionWorldOnA = pred_transA( pt.m_localPointA )/WORLD_SCALING_FACTOR;
            pt.m_positionWorldOnB = pred_transB( pt.m_localPointB*(pt.m_normalWorldOnB))/WORLD_SCALING_FACTOR;
            pt.m_distance1 = (pt.m_positionWorldOnA -  pt.m_positionWorldOnB).dot(pt.m_normalWorldOnB);

             const btVector3& impact_pointB = pt.m_positionWorldOnA-(bodyA->getCcdSweptSphereRadius()/WORLD_SCALING_FACTOR)*(pt.m_normalWorldOnB.absolute())*CCD_FLAG; //Impact point

             if(verbose_flag>=1)
             {
                if(verbose_flag==2)
                {
//                        cout << endl;
                       printf("bodyA->getCcdSweptSphereRadius()=%f\n",bodyA->getCcdSweptSphereRadius()/WORLD_SCALING_FACTOR);
                       printf("normalOnB=%f,%f,%f\n",pt.m_normalWorldOnB.getX(),pt.m_normalWorldOnB.getY(),pt.m_normalWorldOnB.getZ());
                       printf("ptA=%f,%f,%f\n",pt.m_positionWorldOnA.getX(),pt.m_positionWorldOnA.getY(),pt.m_positionWorldOnA.getZ());
                       printf("ptB=%f,%f,%f\n",pt.m_positionWorldOnB.getX(),pt.m_positionWorldOnB.getY(),pt.m_positionWorldOnB.getZ());
                       printf("pt.m_localPointA=%f,%f,%f\n",pt.m_localPointA.getX()/WORLD_SCALING_FACTOR,pt.m_localPointA.getY()/WORLD_SCALING_FACTOR,pt.m_localPointA.getZ()/WORLD_SCALING_FACTOR);
                       printf("pt.m_localPointB=%f,%f,%f\n",pt.m_localPointB.getX()/WORLD_SCALING_FACTOR,pt.m_localPointB.getY()/WORLD_SCALING_FACTOR,pt.m_localPointB.getZ()/WORLD_SCALING_FACTOR);
                }
                printf("Distance=%f",pt.m_distance1);
             }

             //Storing contact points satisfying contact threshold distance
              if (pt.m_distance1<=contact_threshold_distance)

             {
                 //Check for inclusion in perforating holes. If true collision is not saved
                if(HOLES_ME_temp[index0].HOLES_vect.size()>0)
                {
                    if((shape0>0) && (shape0<9) && (HOLES_analysis_ellipsoid_flag==1 || HOLES_analysis_sphere_flag==1))
                    {
                        in_HOLES=check_for_holes_inclusion_inout((btCollisionObject *) bodyA, HOLES_ME_temp[index0].HOLES_vect, ((ME_t *) bodyB->getUserPointer()),time_pred);
                    }
                }
                 else
                {
                    if(verbose_flag>=1)
                        printf("No HOLES in ME%d\n",index0);
                }

                if(HOLES_ME_temp[index1].HOLES_vect.size()>0)
                {
                    if((shape1>0) && (shape1<9) && (HOLES_analysis_ellipsoid_flag==1 || HOLES_analysis_sphere_flag==1))
                    {
                        in_HOLES=check_for_holes_inclusion_inout((btCollisionObject *) bodyB, HOLES_ME_temp[index1].HOLES_vect, ((ME_t *) bodyA->getUserPointer()),time_pred);
                    }   
                }
                else
                {
                    if(verbose_flag>=1)
                        printf("No HOLES in ME%d\n",index1);
                }


                if(in_HOLES)
                {
                    n_contacts_found =n_contacts_found;
                    n_contacts_found_tot=n_contacts_found_tot;
                    if(verbose_flag>=1)
                         printf("  ------->>> Object in hole, Contact discarded\n");
                }
                else
                {

                      if(verbose_flag>=1)
                        printf("  ------->>> Contact found\n");

                    //updating counters
                    n_contacts_found = n_contacts_found+1;
                    n_contacts_found_tot=n_contacts_found_tot+1;

                    //contact point coordinates



                    //Saving results in COLLISION_DATA
                    COLLISION_DATA_temp[index0].target=index0;
                    COLLISION_DATA_temp[index0].target_shape=shape0;
                    COLLISION_DATA_temp[index0].impactor.push_back(index1);
                    COLLISION_DATA_temp[index0].impactor_shape.push_back(shape1);

                    printf("Target=%d; Impactor=%d\n",COLLISION_DATA_temp[index0].target,COLLISION_DATA_temp[index0].impactor[n_contacts_found-1]);
                    const btVector3& ptA = pt.getPositionWorldOnA();
                    const btVector3& ptB = pt.getPositionWorldOnB();
                    const btVector3& normalOnB = pt.m_normalWorldOnB;
                    const btVector3& impact_pointB = pt.getPositionWorldOnB()-bodyA->getCcdSweptSphereRadius()*pt.m_normalWorldOnB;

                    COLLISION_DATA_temp[index0].point[0].push_back(impact_pointB.getX());
                    COLLISION_DATA_temp[index0].point[1].push_back(impact_pointB.getY());
                    COLLISION_DATA_temp[index0].point[2].push_back(impact_pointB.getZ());
                    if(verbose_flag>=1)
                    {
                           printf("\nimpact_point=%f,%f,%f\n",impact_pointB.getX(),impact_pointB.getY(),impact_pointB.getZ());
                           printf("  ------->>> Contact found\n");
                           if(verbose_flag==2)
                           {
                               printf("COLLISION_DATA[%d] Target=%d; COLLISION_DATA Impactor=%d\n",index0,COLLISION_DATA_temp[index0].target,COLLISION_DATA_temp[index0].impactor[COLLISION_DATA_temp[index0].impactor.size()-1]);
                               printf("COLLISION_DATA[%d] Point=%f,%f,%f\n",index0,COLLISION_DATA_temp[index0].point[0][COLLISION_DATA_temp[index0].impactor.size()-1],COLLISION_DATA_temp[index0].point[1][COLLISION_DATA_temp[index0].impactor.size()-1],COLLISION_DATA_temp[index0].point[2][COLLISION_DATA_temp[index0].impactor.size()-1]);
                           }
                    }
                }
             }
             else
             {
                if(verbose_flag>=1)
                         printf("  ------->>> Contact discarded\n");
             }
          }
      } 

   }
   return n_contacts_found_tot;
}

//======================================================================
/*! \brief Checking for collisions between dynamics world objects if a collision detection algorithm based on gjk is selected
 * 
 * @param dynamicsWorld pointer to btDiscreteDynamicsWorld 
 * @param COLLISION_DATA_temp pointer to the COLLISION_DATA type structure containing collision info
 *
 * @retval n_contacts_found_tot Number of valid contacts found
 */

int checkCollisions_gjk(contact_s contact_array_i, HOLES_ME_t HOLES_ME_temp[], COLLISION_DATA_t *COLLISION_DATA_temp, double tstep)
{

     if(verbose_flag>=1)
           printf("Performing checkCollision_gjk\n");
    
    int n_contacts_found_tot=0; //number of total contacts found
    bool in_HOLES=false;
                
     //Retrieving collision objects      
      ME_t *bodyA;
      ME_t *bodyB;
      btCollisionObject *col0;
      btCollisionObject *col1;
      //Retrieving original shape ID
      int shape0_0=contact_array_i.body_A->GEOMETRY_DATA.shape_ID;
      int shape1_0=contact_array_i.body_B->GEOMETRY_DATA.shape_ID;
      int shape0, shape1;
      
      //Checking for target object to be a ME
      if(shape0_0==0 || shape0_0==9)
      {
        bodyA = contact_array_i.body_B;
        bodyB = contact_array_i.body_A;
        col0 = contact_array_i.col_B;
        col1 = contact_array_i.col_A;
        shape0=shape1_0;
        shape1=shape0_0;
      }
      else
      {
        bodyA =  contact_array_i.body_A;
        bodyB =  contact_array_i.body_B;
        col0 =  contact_array_i.col_A;
        col1 =  contact_array_i.col_B;
        shape0=shape0_0;
        shape1=shape1_0;   
      }
        //Expliciting type of object 
       char target_type_str[80];
       char impactor_type_str[80];

       strcpy(target_type_str,"ME");
       if((shape0>0) && (shape0<9)){
           strcpy(target_type_str,"ME");
       } else if (shape0==0){
           strcpy(target_type_str,"Fragment");
       } else if(shape0==9){
           strcpy(target_type_str,"Bubble");
       }

       if((shape1>0) && (shape1<9)){
           strcpy(impactor_type_str,"ME");
       } else if (shape1==0){
           strcpy(impactor_type_str,"Fragment");
       } else if(shape1==9){
           strcpy(impactor_type_str,"Bubble");
       } 


      int index0=bodyA->object_ID_index;
      int index1=bodyB->object_ID_index;
       
      if(verbose_flag>=1)
             printf("Target: Type=%s, ID=%d; Impactor: Type=%s, ID=%d => ",target_type_str, index0,impactor_type_str,index1);

//       /*Setting collision thresholds*/
      btScalar contact_threshold_distance;
      contact_threshold_distance=general_threshold_distance;
      //Assigning a different threshold distance in case of impact between two MEs
      if(((shape0>0) && (shape0<9)) && ((shape1>0) && (shape1<9)))
      {
          contact_threshold_distance=ME_threshold_distance;  
      }

      /*Performing collision check on pairs including at least one of the body is a ME*/
      if(((shape0>0) && (shape0<9)) || ((shape1>0) && (shape1<9)))
      { 
        int n_contacts_found;
        //Check for inclusion in perforating holes. If true collision is not saved
        if(HOLES_ME_temp[index0].HOLES_vect.size()>0)
        {
            if((shape0>0) && (shape0<9) && (HOLES_analysis_ellipsoid_flag==1 || HOLES_analysis_sphere_flag==1))
            {
                in_HOLES=check_for_holes_inclusion_inout(col0, HOLES_ME_temp[index0].HOLES_vect, bodyB,contact_array_i.toi*tstep);
            }
        }
        else
        {
            if(verbose_flag>=1)
                printf("No HOLES in ME%d\n",index0);
        }

        if(HOLES_ME_temp[index1].HOLES_vect.size()>0)
        {
            if((shape1>0) && (shape1<9) && (HOLES_analysis_ellipsoid_flag==1 || HOLES_analysis_sphere_flag==1))
            {
                in_HOLES=check_for_holes_inclusion_inout(col1, HOLES_ME_temp[index1].HOLES_vect, bodyA,contact_array_i.toi*tstep);
            }               
        }
        else
        {
            if(verbose_flag>=1)
                printf("No HOLES in ME%d\n",index1);
        }
        if(in_HOLES)
        {
            n_contacts_found =0;
            n_contacts_found_tot=n_contacts_found_tot;
             if(verbose_flag>=1)
                 printf("  ------->>> Object in hole, Contact discarded\n");
        }
        else
        {
          n_contacts_found =1;
          /* Check all contacts points */ 
          printf(" @TOI=%f s Target: Type=%s, ID=%d; Impactor: Type=%s, ID=%d => ",contact_array_i.toi,target_type_str,index0,impactor_type_str,index1);
              
              //Storing contact points satisfying contact threshold distance
             if (contact_array_i.distance_at_toi<=contact_threshold_distance)
             {
                if(verbose_flag>=1)
                    printf("  ------->>> Contact found\n");
 
                //updating counters
//                 n_contacts_found = n_contacts_found+1;
                n_contacts_found_tot=n_contacts_found_tot+1;
                if(verbose_flag>=1)
                   {
                       printf("Num. of contacts=%d\n",  n_contacts_found);
                       printf("impact point=%f,%f,%f\n",contact_array_i.contact_p_at_toi[0]/WORLD_SCALING_FACTOR,contact_array_i.contact_p_at_toi[1]/WORLD_SCALING_FACTOR,contact_array_i.contact_p_at_toi[2]/WORLD_SCALING_FACTOR);
                       printf("  ------->>> Contact found\n");
                   }

                //Saving results in COLLISION_DATA
                COLLISION_DATA_temp[index0].target=index0;
                COLLISION_DATA_temp[index0].target_shape=shape0;
                COLLISION_DATA_temp[index0].impactor.push_back(index1);
                COLLISION_DATA_temp[index0].impactor_shape.push_back(shape1);

//                 cout <<"Target="<<COLLISION_DATA_temp[index0].target<<"; Impactor="<<COLLISION_DATA_temp[index0].impactor[n_contacts_found-1]<<endl;
                COLLISION_DATA_temp[index0].point[0].push_back(contact_array_i.contact_p_at_toi[0]/WORLD_SCALING_FACTOR);
                COLLISION_DATA_temp[index0].point[1].push_back(contact_array_i.contact_p_at_toi[1]/WORLD_SCALING_FACTOR);
                COLLISION_DATA_temp[index0].point[2].push_back(contact_array_i.contact_p_at_toi[2]/WORLD_SCALING_FACTOR);
                if(verbose_flag>1)
                {
                           printf("COLLISION_DATA[%d] Target=%d; COLLISION_DATA Impactor=%d",index0,COLLISION_DATA_temp[index0].target,COLLISION_DATA_temp[index0].impactor[COLLISION_DATA_temp[index0].impactor.size()-1]);
                           printf("COLLISION_DATA[%d] Point=%f,%f,%f\n",index0,COLLISION_DATA_temp[index0].point[0][COLLISION_DATA_temp[index0].impactor.size()-1],COLLISION_DATA_temp[index0].point[1][COLLISION_DATA_temp[index0].impactor.size()-1],COLLISION_DATA_temp[index0].point[2][COLLISION_DATA_temp[index0].impactor.size()-1]);
                }
             }
             else
             {
                 if(verbose_flag>=1)
                 {                   
                     printf("Num. of contacts=%d\n",  n_contacts_found);
                     printf("distance %f is larger than contact_threshold_distance (%f)  ------->>> Contact discarded\n",contact_array_i.distance_at_toi,contact_threshold_distance);
                 }
             }
          }
      }
      else
      {
          if(verbose_flag>=1)
              printf("  ------->>> Contact between Fragments/Bubbles => discarded\n");
      }
//     } 

   //}
   return n_contacts_found_tot;
} 

//======================================================================
/*! \brief Tracking simulation function 
 *
* @param ME pointer to MacroElements population
* @param FRAGMENTS pointer to FRAGMENTS population
* @param BUBBLE pointer to BUBBLE population
* @param HOLES pointer to HOLES population
* @param n_ME number of objects in MacroElements population
* @param n_FRAGMENTS number of objects in FRAGMENTS population
* @param n_BUBBLE number of objects in BUBBLE population
* @param n_HOLES number of objects in HOLES population
* @param timeStep Timestep for tracking simulation
* @param collision_flag Activation flag for breakup
* @param COLLISION_DATA Collisions output data
* @param N_contacts_found_tot Number of detected collisions
* @param toi_timeStep time of the first impact counted from the beginning of the timeStep
 */
void tracking(ME_t *ME, ME_t *FRAGMENTS, ME_t *BUBBLE, ME_t *HOLES, int n_ME, int n_FRAGMENTS, int n_BUBBLE, int n_HOLES, double *timeStep, int *collision_flag, COLLISION_DATA_t *COLLISION_DATA, int *N_contacts_found_tot, double *toi_timeStep) 
{
  
    int nObjects=n_ME+n_FRAGMENTS+n_BUBBLE+n_HOLES; //Number of total objects
    double toi_1st0;//first time of impact
    double toi_1st;//first time of impact+tolerance
    contact_s contact_at_toi;
    vector <int> ius;
    int i_cc;
    check_WORLD_SCALING_FACTOR(ME, FRAGMENTS, BUBBLE, HOLES, n_ME, n_FRAGMENTS, n_BUBBLE, n_HOLES);


    /*-------------------------------------------------------*/
    /*Settings for Dispatcher, Solvers and DynamicsWorld*/
    btDefaultCollisionConfiguration *collisionConfiguration = new btDefaultCollisionConfiguration();
	collisionConfiguration->setConvexConvexMultipointIterations(numPerturbationIterations, minimumPointsPerturbationThreshold);
 	collisionConfiguration->setPlaneConvexMultipointIterations(numPerturbationIterations, minimumPointsPerturbationThreshold); 	
    btCollisionDispatcher *dispatcher = new btCollisionDispatcher(collisionConfiguration);
    btBroadphaseInterface *overlappingPairCache = new btDbvtBroadphase();
    btSequentialImpulseConstraintSolver* solver = new btSequentialImpulseConstraintSolver;
    btDiscreteDynamicsWorld *dynamicsWorld = new btDiscreteDynamicsWorld(dispatcher, overlappingPairCache, solver, collisionConfiguration);
    dynamicsWorld->getDispatchInfo().m_dispatchFunc = btDispatcherInfo::DISPATCH_DISCRETE;
    btScalar ccd_allowed_pen(CCD_ALLOWED_PENETRATION);
//      dynamicsWorld->getDispatchInfo().m_allowedCcdPenetration=btScalar(0.1);

    
            MyDebugDraw *draw   =   new MyDebugDraw;
          dynamicsWorld->setDebugDrawer(draw);
          dynamicsWorld->getDebugDrawer()->setDebugMode( btIDebugDraw::DBG_DrawWireframe );

    
    //Activating Continuous collision detection
    dynamicsWorld->getDispatchInfo().m_useContinuous = true;
    
    if(CCD_FLAG==1)
    {
        dynamicsWorld->getDispatchInfo().m_dispatchFunc = btDispatcherInfo::DISPATCH_CONTINUOUS;

    }

    dispatcher->setNearCallback((btNearCallback)CST_NearCallback);
    
    btVector3 gravity0(0.0, 0.0, 0.0); //gravity 
    dynamicsWorld->setGravity(gravity0);

    
    /*----------------------------------------------------*/
    /*Call to populate_dynamicWorld function: world will be populated with MEs, FRAGMENTS and BUBBLE*/
    btAlignedObjectArray<btCollisionShape*> collisionShapes;
     
    //add_objects to dynamicsWorld
     populate_dynamicWorld(ME, n_ME, dynamicsWorld, collisionShapes);

    if(n_FRAGMENTS>0)
    {
         populate_dynamicWorld(FRAGMENTS, n_FRAGMENTS, dynamicsWorld, collisionShapes);
    }
    if(n_BUBBLE>0)
    {
         populate_dynamicWorld(BUBBLE, n_BUBBLE, dynamicsWorld, collisionShapes);
    }
    if(n_HOLES>0)
    {
         populate_dynamicWorld(HOLES, n_HOLES, dynamicsWorld, collisionShapes);
    }

    HOLES_ME_t HOLES_ME[n_ME+1];
    for(int i_h_me=0;i_h_me<n_HOLES; i_h_me++)
    {
       HOLES_ME[HOLES[i_h_me].object_ID].ME_object_ID_index=HOLES[i_h_me].object_ID;
       HOLES_ME[HOLES[i_h_me].object_ID].HOLES_vect.push_back(HOLES[i_h_me]);
    }
//      check_WORLD_SCALING_FACTOR(dynamicsWorld, WORLD_SCALING_FACTOR);
 
    /*----------------------------------------------------*/
    /* Simulation loop */
    int max_loop=1;// maximum number of loops to be performed. CST tracking sets it to 1 because a more general loop is performed in CST main in Matlab code
	
       if(verbose_flag>=1)
           printf("Num. of collision objects: %d\n",dynamicsWorld->getNumCollisionObjects());
   
    for (int iit = 0; iit < max_loop; ++iit) 
    {
        contact_array.clear();
        toi_tot0.clear();
        toi_tot.clear();
        ius.clear();
//         for(int i_cc=0; i_cc<sizeof(COLLISION_DATA)/sizeof(COLLISION_DATA[0]) ; i_cc++){
        i_cc=0;
        while(COLLISION_DATA[i_cc].impactor.size()>0)
        {
            COLLISION_DATA[i_cc].impactor.clear();
            COLLISION_DATA[i_cc].impactor_shape.clear();
            COLLISION_DATA[i_cc].point[0].clear();
            COLLISION_DATA[i_cc].point[1].clear();
            COLLISION_DATA[i_cc].point[2].clear();
            i_cc=i_cc+1;
        }

//         cout <<"i_cc="<<i_cc<<endl;
        //Performing a first simulation to find the first time of impact
        //cout << "*** First iteration ***"<< endl;
        
        remove("tracking_vertex.txt");
        if(debug_draw_flag==1)
            tracking_debug_drawer(dynamicsWorld);

        if (dynamicsWorld->getDispatchInfo().m_dispatchFunc == btDispatcherInfo::DISPATCH_DISCRETE)
        {
              printf("... discrete collision detection query ...\n");
              dynamicsWorld->stepSimulation((*timeStep) ,substeps,*timeStep/substeps);
        }
        else
        {
              printf("... continuous collision detection query ...\n");

              dynamicsWorld->stepSimulation((*timeStep) ,substeps,*timeStep/substeps);
              
            if(verbose_flag>=1)
                printf("Collisions detected at toi= [ ");
             for (int i_toi=0;i_toi<toi_tot.size(); i_toi++)
             {
                 toi_tot0.push_back(toi_tot.at(i_toi));
                if(verbose_flag>=1)
                    printf("%f ",toi_tot[i_toi]);
             }
            if(verbose_flag>=1)
                printf("]\n");

             sort(toi_tot.begin(), toi_tot.end());
             ius.clear();
             

             for(int i_sorted=0; i_sorted<toi_tot.size(); i_sorted++)
             {
                if((i_sorted>=1) && (toi_tot[i_sorted]==toi_tot[i_sorted-1]))
                    continue;

                for(int i_unsorted=0; i_unsorted<toi_tot0.size(); i_unsorted++)
                  {
                       if((toi_tot[i_sorted]==toi_tot0[i_unsorted])) 
                      {
                       ius.push_back(i_unsorted);
                      }
                  }

             }                   
        }
        int i_toi=-1;
        if(gjk_flag>=1)
        {
            double toi0;

            for(int i_c_a=0;i_c_a<ius.size();i_c_a++)
            {   
              int contact_1=0;
              toi0=contact_array[ius[i_c_a >0 ? i_c_a-1 : 0]].toi;
              if((*N_contacts_found_tot>0) && ((contact_array[ius[i_c_a]].toi>toi0) || (i_c_a==ius.size()-1)))                  
              {
                toi_def=toi0;
                break;
              }
              if((*N_contacts_found_tot==0) || (contact_array[ius[i_c_a]].toi==toi0))
              {
                  contact_1=checkCollisions_gjk(contact_array[ius[i_c_a]], HOLES_ME, COLLISION_DATA, *timeStep);
                  toi_def= contact_1>0 ? contact_array[ius[i_c_a]].toi : 1;
                  *N_contacts_found_tot = *N_contacts_found_tot + contact_1;// + checkCollisions_gjk(contact_array[ius[i_c_a]], HOLES_ME, COLLISION_DATA, *timeStep);
              }
            }
        }
        else
        {
            while(*N_contacts_found_tot<1 && i_toi+1<toi_tot.size())
            {
                i_toi=i_toi+1;
                toi_1st=toi_tot[i_toi];

            //Performing checks for collisions
                
            *N_contacts_found_tot = checkCollisions(dynamicsWorld, HOLES_ME, COLLISION_DATA, *timeStep*toi_tot[i_toi]);
             if(verbose_flag>1)
                printf("toi_tot[%d]=%f\n",i_toi,toi_tot[i_toi]);
            }
            toi_def= *N_contacts_found_tot>0 ? toi_tot[i_toi] : 1;
        }
 
        if(*N_contacts_found_tot==0)
        {
         toi_def=1;   
        }
        
        *toi_timeStep=(*timeStep) * toi_def;
       if(verbose_flag>1)
           printf("--> toi_def=%f\n",toi_def);
        

        //Retrieving updated info for all the objects        
        for (int j = dynamicsWorld->getNumCollisionObjects() - 1; j >= 0; --j) 
        {
            btCollisionObject *obj = dynamicsWorld->getCollisionObjectArray()[j];		
            btRigidBody *body = btRigidBody::upcast(obj);
            
            btTransform trans;

            if (body && body->getMotionState()) {
                body->getMotionState()->getWorldTransform(trans);
                
            } else {
                trans = obj->getWorldTransform();
            }     
            
//             trans = body->getWorldTransform();
            btVector3 origin = trans.getOrigin();
            
            btVector3 body_cm=body->getCenterOfMassPosition()/WORLD_SCALING_FACTOR;
            
//             if(*N_contacts_found_tot<1)
//             {
//                 //Updating population objects properties
//                 ((ME_t *) body->getUserPointer())->DYNAMICS_DATA.cm_coord[0]=body_cm.getX();
//                 ((ME_t *) body->getUserPointer())->DYNAMICS_DATA.cm_coord[1]=body_cm.getY();
//                 ((ME_t *) body->getUserPointer())->DYNAMICS_DATA.cm_coord[2]=body_cm.getZ(); 
//                 if(verbose_flag>1)
//                 {
//                     cout << "body" <<j<<" position=["<< body_cm.getX()<<", " << body_cm.getY() << ", " << body_cm.getZ()<<"]" << endl; 
// //                     cout << "origin" <<j<<" position=["<< origin.getX()<<", " << origin.getY() << ", " << origin.getZ()<<"]" << endl; 
//                 }
//             }
//             else
//             {
            
                btTransform startTransform;
                startTransform.setIdentity();
                btVector3 origin_start(((ME_t *) body->getUserPointer())->DYNAMICS_DATA.cm_coord[0], ((ME_t *) body->getUserPointer())->DYNAMICS_DATA.cm_coord[1], ((ME_t *) body->getUserPointer())->DYNAMICS_DATA.cm_coord[2]);
                btQuaternion obj_quat_start(((ME_t *) body->getUserPointer())->DYNAMICS_DATA.quaternions[1],((ME_t *) body->getUserPointer())->DYNAMICS_DATA.quaternions[2],((ME_t *) body->getUserPointer())->DYNAMICS_DATA.quaternions[3],((ME_t *) body->getUserPointer())->DYNAMICS_DATA.quaternions[0]);
                startTransform.setRotation(obj_quat_start);
                origin_start=origin_start*WORLD_SCALING_FACTOR;
                startTransform.setOrigin(origin_start);

                btTransform pred_trans;
                btVector3 vel0(((ME_t *) body->getUserPointer())->DYNAMICS_DATA.vel[0],((ME_t *) body->getUserPointer())->DYNAMICS_DATA.vel[1],((ME_t *) body->getUserPointer())->DYNAMICS_DATA.vel[2]);
                vel0=vel0*WORLD_SCALING_FACTOR;
                btVector3 ang0(((ME_t *) body->getUserPointer())->DYNAMICS_DATA.w[0],((ME_t *) body->getUserPointer())->DYNAMICS_DATA.w[1],((ME_t *) body->getUserPointer())->DYNAMICS_DATA.w[2]);
                btTransformUtil::integrateTransform(startTransform,vel0,ang0,(*timeStep)*toi_def,pred_trans);
                if(verbose_flag>=1)
                {
                   printf("toi_def=%f\n",toi_def);
                   printf("(*timeStep)*toi_def=%f\n",(*timeStep)*toi_def);
                }
                btVector3 pred_origin=pred_trans.getOrigin();
                btVector3 body_cm1=pred_origin/WORLD_SCALING_FACTOR;


                //Updating population objects properties
                ((ME_t *) body->getUserPointer())->DYNAMICS_DATA.cm_coord[0]=body_cm1.getX();
                ((ME_t *) body->getUserPointer())->DYNAMICS_DATA.cm_coord[1]=body_cm1.getY();
                ((ME_t *) body->getUserPointer())->DYNAMICS_DATA.cm_coord[2]=body_cm1.getZ();
                if(verbose_flag>=1)
                {
                    printf("TOI (%f) body%d,shape%d  position=[%f,%f,%f]\n",toi_def,((ME_t *) body->getUserPointer())->object_ID_index,((ME_t *) body->getUserPointer())->GEOMETRY_DATA.shape_ID,body_cm1.getX(), body_cm1.getY(),body_cm1.getZ()); 
                    printf("N_contacts_found_tot=%d\n",*N_contacts_found_tot);
                }
//                 *collision_flag=1;
//             }
                if(*N_contacts_found_tot>=1)
                {
                   *collision_flag=1;                    
                }
		};   
	};
                printf("N_contacts_found_tot=%d\n",*N_contacts_found_tot);

   
    // Cleanup.
    int i_clean;
    for (i_clean = dynamicsWorld->getNumCollisionObjects() - 1; i_clean >= 0; --i_clean) 
    {
        btCollisionObject* obj = dynamicsWorld->getCollisionObjectArray()[i_clean];
        btRigidBody* body = btRigidBody::upcast(obj);
        if (body && body->getMotionState()) 
        {
            delete body->getMotionState();
        }
        dynamicsWorld->removeCollisionObject(obj);
        delete obj;
    }
    for (i_clean = 0; i_clean < collisionShapes.size(); ++i_clean) 
    {
        delete collisionShapes[i_clean];
    }
//     for(int i_cP=0;i_cP<collisionPairs_tot.size();i_cP++)
//    {
//     dispatcher->freeCollisionAlgorithm(collisionPairs_tot[i_cP].m_algorithm);
//     }
    
//                 if(find_coll_algo==1)
//                 {
//                     cout <<"free0" << endl;
//                     dispatcher.freeCollisionAlgorithm(collisionPair.m_algorithm);
//                     cout <<"free1" << endl;
//                 }


    delete dynamicsWorld;
    delete solver;
    delete overlappingPairCache;
    delete dispatcher;
    
    delete collisionConfiguration;
    collisionShapes.clear();

}
