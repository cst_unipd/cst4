%> @file  collision_data_interface.m
%> @brief Populating COLLISION_DATA structure for Breakup algorithm
%> @authors Lorenzo Olivieri (lorenzo.olivieri@unipd.it) and Cinzia Giacomuzzo (cinzia.giacomuzzo@unipd.it)
%======================================================================
%> @brief Function used to populate COLLISION_DATA structure by copying COLLISION_DATA_small data from Tracking collision detection and adding useful fields as input for Breakup algorithm  
%> @param C_DATA_tracking_in % Informations on collisions from tracking
%> 
%> @retval C_DATA            % Updated informations on collision
%======================================================================
function [COLLISION_DATA]=collision_data_interface(COLLISION_DATA_tracking_in)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% ME FRAGMENTATION ALGORITHM v03 - dr. L. Olivieri
% Full ME Fragmentation Algorithm
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Updating the COLLISION_DATA set in order to simplify the access to the
% most important information:
% COLLISION_DATA(i):
%     .TARGET
%             .ID
%             .type
%             .mass
%             .vel
%             .Q
%             .Ek
%     .IMPACTOR(j)
%             .ID
%             .type
%             .mass
%             .POINT
%             .vel
%             .Q
%             .Ek
%             .Ek_rel     % wrt TARGET
%             .v_rel      % wrt TARGET
%             .E          % [E_TOT E_PROJ], from Breakup Threshold
%             .c_hull_impactor
%             .c_hull_target
%             .V_imp_tar  % intersection ratios
%             .V_tar      % Target intersection volume on target volume
%             .V_imp      % Impactor intersection volume on impactor volume
%             .t_b        % equivalent thichness wrt to crater depth
%             .t_EQ       % Target equivalent thichness
%             .dp_EQ      % equivalent impactor diameter
%             .A_inter    % Target_impactor area intersection
%     .V_CM               % system CM absolute velocity
%     .Q_TOT              % Total momentum
%     .m_TOT              % Total mass
%     .Ek_TOT             % Total kinetic energy
%     .F_L                % Fragmentation Level, from Breakup Threshold

global ME FRAGMENTS BUBBLE
%% Variables initialization
DIM0=0;
DIM=0;
DIM2=0;

clear COLLISION_DATA

TARGET_s=struct('ID',{}, 'type',{}, 'v_loss',{}, 'mass', {},'vel',{},'Q',{},'Ek',{}, 'failure_ID', {}, 'cMLOSS', {}, 'cELOSS',{}, 'c_EXPL',{},'Threshold_update',{});
IMPACTOR_s=struct('ID',{},'type',{},'mass',{},'POINT',{},'vel',{},...
    'Q',{},'Ek',{},'Ek_rel',{},'v_rel',{},'E',{},'c_hull_impactor',{},'c_hull_target',{},'V_imp_tar',{},'V_tar',{},'V_imp',{},'t_b',{},'t_EQ',{},'dp_EQ',{},'A_inter',{});
COLLISION_DATA=struct('TARGET', TARGET_s, 'IMPACTOR', IMPACTOR_s, 'V_CM',{},'Q_TOT',{},'m_TOT',{},'Ek_TOT',{},'F_L',{});

%% 1 - CONVERTING COLLISION DATA TRACKING OUTPUT
DIM0=length(COLLISION_DATA_tracking_in);
for i=1:DIM0
    COLLISION_DATA(i).TARGET.ID=COLLISION_DATA_tracking_in(i).target;
    if(COLLISION_DATA_tracking_in(i).target_shape==0)
        COLLISION_DATA(i).TARGET.type=0; %FRAGMENT
    elseif(COLLISION_DATA_tracking_in(i).target_shape==9)
        COLLISION_DATA(i).TARGET.type=2; %BUBBLE
    else
        COLLISION_DATA(i).TARGET.type=1; %ME
    end
    
    for j=1:length([COLLISION_DATA_tracking_in(i).impactor])
        COLLISION_DATA(i).IMPACTOR(j).ID=COLLISION_DATA_tracking_in(i).impactor(j);
        if(COLLISION_DATA_tracking_in(i).impactor_shape(j)==0)
            COLLISION_DATA(i).IMPACTOR(j).type=0; %FRAGMENT
        elseif(COLLISION_DATA_tracking_in(i).impactor_shape(j)==9) 
            COLLISION_DATA(i).IMPACTOR(j).type=2; %BUBBLE
        else
            COLLISION_DATA(i).IMPACTOR(j).type=1; %ME
        end
        COLLISION_DATA(i).IMPACTOR(j).POINT(1)=COLLISION_DATA_tracking_in(i).point(1,j);
        COLLISION_DATA(i).IMPACTOR(j).POINT(2)=COLLISION_DATA_tracking_in(i).point(2,j);
        COLLISION_DATA(i).IMPACTOR(j).POINT(3)=COLLISION_DATA_tracking_in(i).point(3,j);
    end
end
% Define the momentum conservation and the energy loss in v_loss
COLLISION_DATA=velocity_model_data_interface(COLLISION_DATA);

% Add the impactors as target
L1=length(COLLISION_DATA);
count=1;
for iC1=1:L1 % each collision of COLLISION DATA
    L2=length(COLLISION_DATA(iC1).IMPACTOR);
    for i=1:L2 % each impactor of COLLISION DATA
        COLLISION_DATA(L1+count).TARGET.ID=COLLISION_DATA(iC1).IMPACTOR(i).ID;
        COLLISION_DATA(L1+count).TARGET.type=COLLISION_DATA(iC1).IMPACTOR(i).type;
        COLLISION_DATA(L1+count).TARGET.v_loss=COLLISION_DATA(iC1).IMPACTOR(i).v_loss;
        dbstop at 102 if any(isnan(COLLISION_DATA(L1+count).TARGET.v_loss));
        COLLISION_DATA(L1+count).IMPACTOR(1).ID=COLLISION_DATA(iC1).TARGET.ID;
        COLLISION_DATA(L1+count).IMPACTOR(1).type=COLLISION_DATA(iC1).TARGET.type;
        COLLISION_DATA(L1+count).IMPACTOR(1).v_loss=COLLISION_DATA(iC1).TARGET.v_loss;
        dbstop at 106 if any(isnan(COLLISION_DATA(L1+count).IMPACTOR(1).v_loss));
        COLLISION_DATA(L1+count).IMPACTOR(1).POINT=COLLISION_DATA(iC1).IMPACTOR(i).POINT;
        
        count=count+1;
    end
end
%  merge impacts on same target
L1=length(COLLISION_DATA);
for iC1=L1:-1:1
    for i=1:1:(iC1-1)
        if COLLISION_DATA(iC1).TARGET.ID==COLLISION_DATA(i).TARGET.ID && COLLISION_DATA(iC1).TARGET.type==COLLISION_DATA(i).TARGET.type
            COLLISION_DATA(i).TARGET.v_loss=COLLISION_DATA(i).TARGET.v_loss+COLLISION_DATA(iC1).TARGET.v_loss;
            COLLISION_DATA(i).IMPACTOR=[COLLISION_DATA(i).IMPACTOR COLLISION_DATA(iC1).IMPACTOR];
            COLLISION_DATA(iC1)=[];
            i=iC1;
            break;
        end      
    end
end

% Delete impacts with bubble as target
L1=length(COLLISION_DATA);
for iC1=L1:-1:1
    if COLLISION_DATA(iC1).TARGET.type==2
        COLLISION_DATA(iC1)=[];
    end
end

%% 2 - RETRIEVING FIELDS FROM POPULATION STRUCTURE
DIM=length(COLLISION_DATA);
for i=1:DIM
    % Here the TARGET data
    switch COLLISION_DATA(i).TARGET.type
        case 0
            COLLISION_DATA(i).TARGET.mass= FRAGMENTS(COLLISION_DATA(i).TARGET.ID).GEOMETRY_DATA.mass;
            COLLISION_DATA(i).TARGET.vel = FRAGMENTS(COLLISION_DATA(i).TARGET.ID).DYNAMICS_DATA.vel;
            COLLISION_DATA(i).TARGET.failure_ID = FRAGMENTS(COLLISION_DATA(i).TARGET.ID).FRAGMENTATION_DATA.failure_ID;
            COLLISION_DATA(i).TARGET.cMLOSS = FRAGMENTS(COLLISION_DATA(i).TARGET.ID).FRAGMENTATION_DATA.cMLOSS;
            COLLISION_DATA(i).TARGET.cELOSS = FRAGMENTS(COLLISION_DATA(i).TARGET.ID).FRAGMENTATION_DATA.cELOSS;
            COLLISION_DATA(i).TARGET.c_EXPL = FRAGMENTS(COLLISION_DATA(i).TARGET.ID).FRAGMENTATION_DATA.c_EXPL;
        case 1
            COLLISION_DATA(i).TARGET.mass= ME(COLLISION_DATA(i).TARGET.ID).GEOMETRY_DATA.mass;
            COLLISION_DATA(i).TARGET.vel = ME(COLLISION_DATA(i).TARGET.ID).DYNAMICS_DATA.vel;
            COLLISION_DATA(i).TARGET.failure_ID = ME(COLLISION_DATA(i).TARGET.ID).FRAGMENTATION_DATA.failure_ID;
            COLLISION_DATA(i).TARGET.cMLOSS = ME(COLLISION_DATA(i).TARGET.ID).FRAGMENTATION_DATA.cMLOSS;
            COLLISION_DATA(i).TARGET.cELOSS = ME(COLLISION_DATA(i).TARGET.ID).FRAGMENTATION_DATA.cELOSS;
            COLLISION_DATA(i).TARGET.c_EXPL = ME(COLLISION_DATA(i).TARGET.ID).FRAGMENTATION_DATA.c_EXPL;
        case 2
            COLLISION_DATA(i).TARGET.mass= BUBBLE(COLLISION_DATA(i).TARGET.ID).GEOMETRY_DATA.mass;
            COLLISION_DATA(i).TARGET.vel = BUBBLE(COLLISION_DATA(i).TARGET.ID).DYNAMICS_DATA.vel;
            COLLISION_DATA(i).TARGET.failure_ID = BUBBLE(COLLISION_DATA(i).TARGET.ID).FRAGMENTATION_DATA.failure_ID;
            COLLISION_DATA(i).TARGET.cMLOSS = BUBBLE(COLLISION_DATA(i).TARGET.ID).FRAGMENTATION_DATA.cMLOSS;
            COLLISION_DATA(i).TARGET.cELOSS = BUBBLE(COLLISION_DATA(i).TARGET.ID).FRAGMENTATION_DATA.cELOSS;
            COLLISION_DATA(i).TARGET.c_EXPL = BUBBLE(COLLISION_DATA(i).TARGET.ID).FRAGMENTATION_DATA.c_EXPL;
            disp('error: BUBBLE shall not be TARGET, TARGET type shall be 0 (debris/fragment) or 1 (ME)')
        otherwise
            disp('error: TARGET type shall be 0 (debris/fragment) or 1 (ME)')
    end
    COLLISION_DATA(i).TARGET.Q  =     COLLISION_DATA(i).TARGET.vel *      COLLISION_DATA(i).TARGET.mass;
    COLLISION_DATA(i).TARGET.Ek = 0.5*COLLISION_DATA(i).TARGET.mass*(norm(COLLISION_DATA(i).TARGET.vel)^2);
    COLLISION_DATA(i).TARGET.Threshold_update = 0;
    
    % Here the IMPACTOR(S) data
    DIM2=length([COLLISION_DATA(i).IMPACTOR]); % number of impactors on target i
    for j=1:DIM2


        switch COLLISION_DATA(i).IMPACTOR(j).type
            case 0

                COLLISION_DATA(i).IMPACTOR(j).mass= FRAGMENTS(COLLISION_DATA(i).IMPACTOR(j).ID).GEOMETRY_DATA.mass;
                COLLISION_DATA(i).IMPACTOR(j).vel = FRAGMENTS(COLLISION_DATA(i).IMPACTOR(j).ID).DYNAMICS_DATA.vel;
            case 1
                COLLISION_DATA(i).IMPACTOR(j).mass= ME(COLLISION_DATA(i).IMPACTOR(j).ID).GEOMETRY_DATA.mass;
                COLLISION_DATA(i).IMPACTOR(j).vel = ME(COLLISION_DATA(i).IMPACTOR(j).ID).DYNAMICS_DATA.vel;
            case 2
                COLLISION_DATA(i).IMPACTOR(j).mass= BUBBLE(COLLISION_DATA(i).IMPACTOR(j).ID).GEOMETRY_DATA.mass;
                COLLISION_DATA(i).IMPACTOR(j).vel = BUBBLE(COLLISION_DATA(i).IMPACTOR(j).ID).DYNAMICS_DATA.vel;            
            otherwise
                disp('error: IMPACTOR type shall be 0 (debris/fragment) or 1 (ME)')
        end
        COLLISION_DATA(i).IMPACTOR(j).v_rel =     COLLISION_DATA(i).IMPACTOR(j).vel -      COLLISION_DATA(i).TARGET.vel;
        COLLISION_DATA(i).IMPACTOR(j).Q     =     COLLISION_DATA(i).IMPACTOR(j).vel *      COLLISION_DATA(i).IMPACTOR(j).mass;
        COLLISION_DATA(i).IMPACTOR(j).Ek    = 0.5*COLLISION_DATA(i).IMPACTOR(j).mass*(norm(COLLISION_DATA(i).IMPACTOR(j).vel)^2);
        COLLISION_DATA(i).IMPACTOR(j).Ek_rel= 0.5*COLLISION_DATA(i).IMPACTOR(j).mass*(norm(COLLISION_DATA(i).IMPACTOR(j).v_rel)^2);

 
    end
    
    % Here the common data
    COLLISION_DATA(i).Q_TOT = sum([COLLISION_DATA(i).IMPACTOR(:).Q],2)  + COLLISION_DATA(i).TARGET.Q    ;   % Total momentum
    COLLISION_DATA(i).m_TOT = sum([COLLISION_DATA(i).IMPACTOR(:).mass]) + COLLISION_DATA(i).TARGET.mass ;   % Total mass
    COLLISION_DATA(i).Ek_TOT= sum([COLLISION_DATA(i).IMPACTOR(:).Ek])   + COLLISION_DATA(i).TARGET.Ek   ;   % Total kinetic energy
    COLLISION_DATA(i).V_CM  =      COLLISION_DATA(i).Q_TOT              / COLLISION_DATA(i).m_TOT;          % system CM absolute velocity
end


%% 3 - CALCULATING PROJECTIONS
for i=1:DIM
    DIM2=length([COLLISION_DATA(i).IMPACTOR]);
    switch COLLISION_DATA(i).TARGET.type
        case 0
            c_hT=FRAGMENTS(COLLISION_DATA(i).TARGET.ID).GEOMETRY_DATA.c_hull'+FRAGMENTS(COLLISION_DATA(i).TARGET.ID).DYNAMICS_DATA.cm_coord ;
        case 1
            R=RotationMatrix(ME(COLLISION_DATA(i).TARGET.ID).DYNAMICS_DATA.quaternions);
            c_hT= (R'*ME(COLLISION_DATA(i).TARGET.ID).GEOMETRY_DATA.c_hull')+ME(COLLISION_DATA(i).TARGET.ID).DYNAMICS_DATA.cm_coord;
        case 2
            c_hT= BUBBLE(COLLISION_DATA(i).TARGET.ID).GEOMETRY_DATA.c_hull'+BUBBLE(COLLISION_DATA(i).TARGET.ID).DYNAMICS_DATA.cm_coord;
    end
    
    for j=1:DIM2
        switch COLLISION_DATA(i).IMPACTOR(j).type
            case 0
                c_hI= FRAGMENTS(COLLISION_DATA(i).IMPACTOR(j).ID).GEOMETRY_DATA.c_hull'+FRAGMENTS(COLLISION_DATA(i).IMPACTOR(j).ID).DYNAMICS_DATA.cm_coord;
            case 1
                R=RotationMatrix(ME(COLLISION_DATA(i).IMPACTOR(j).ID).DYNAMICS_DATA.quaternions);
                c_hI= (R'*ME(COLLISION_DATA(i).IMPACTOR(j).ID).GEOMETRY_DATA.c_hull')+ME(COLLISION_DATA(i).IMPACTOR(j).ID).DYNAMICS_DATA.cm_coord;
            case 2
                c_hI= BUBBLE(COLLISION_DATA(i).IMPACTOR(j).ID).GEOMETRY_DATA.c_hull'+BUBBLE(COLLISION_DATA(i).IMPACTOR(j).ID).DYNAMICS_DATA.cm_coord;
        end

        P=COLLISION_DATA(i).IMPACTOR(j).POINT';
        V=COLLISION_DATA(i).IMPACTOR(j).v_rel;
        if norm(V)~=0
            V=V/norm(V);
            dbstop at 231 if (any(isnan(V)) || any(any(isnan(c_hI))) || any(any(isnan(c_hT))) || any(isnan(P)));
            COLLISION_DATA(i).IMPACTOR(j).c_hull_impactor=proj2D(c_hI,V,P);
            COLLISION_DATA(i).IMPACTOR(j).c_hull_target=proj2D(c_hT,V,P);
        else
            V=COLLISION_DATA(i).IMPACTOR(j).vel; % Relative velocities patch
            V=V/norm(V);
            dbstop at 237 if (any(isnan(V)) || any(any(isnan(c_hI))) || any(any(isnan(c_hT))) || any(isnan(P)));
            COLLISION_DATA(i).IMPACTOR(j).c_hull_impactor=proj2D(c_hI,V,P);
            COLLISION_DATA(i).IMPACTOR(j).c_hull_target=proj2D(c_hT,V,P);
        end
        COLLISION_DATA(i).IMPACTOR(j).V_imp=0;
        COLLISION_DATA(i).IMPACTOR(j).V_tar=0;
        COLLISION_DATA(i).IMPACTOR(j).V_imp_tar=0;
        
        [COLLISION_DATA(i).IMPACTOR(j).V_imp_tar, COLLISION_DATA(i).IMPACTOR(j).V_imp, COLLISION_DATA(i).IMPACTOR(j).V_tar,...
            COLLISION_DATA(i).IMPACTOR(j).t_b, COLLISION_DATA(i).IMPACTOR(j).t_EQ, COLLISION_DATA(i).IMPACTOR(j).dp_EQ, COLLISION_DATA(i).IMPACTOR(j).A_inter]=...
            intersection_volume(COLLISION_DATA,i,j);
        
       if COLLISION_DATA(i).TARGET.type == 1  
            if isHollowShape(ME(COLLISION_DATA(i).TARGET.ID).GEOMETRY_DATA.shape_ID)
                COLLISION_DATA(i).IMPACTOR(j).V_imp=0;
                COLLISION_DATA(i).IMPACTOR(j).V_tar=0;
                COLLISION_DATA(i).IMPACTOR(j).V_imp_tar=0;
            end
        end
                
    end
end
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
