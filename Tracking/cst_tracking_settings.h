//=============================================================================


double WORLD_SCALING_FACTOR=0; //If 0 it is automatically calculated as the largest/smallest object dimension in the world scaled to fit MAX_BULLET_RANGE and then MIN_BULLET_RANGE

int verbose_flag=1; //Flag to activate diagnostics text outputs [0=no text; 1=verbose; 2=verbose++] 
int debug_draw_flag=0; //Flag to activate diagnostics plot outputs
int HOLES_analysis_sphere_flag=1; //Flag to activate spherical holes analysis
int HOLES_analysis_ellipsoid_flag=1; //Flag to activate ellipsoid holes analysis

/*Tuning variables*/

//Population Settings: same values assigned to all objects
double COLLISION_SHAPE_MARGIN=0.0001; //margin for collision ->  should be > 0 for stability 
double CCD_ALLOWED_PENETRATION=0.0001;


//Settings for collision cheking
const int gjk_flag=2; //Flag to activate solvers for collision detection [0=Bullet default; 1=GJK convex-sphere; 2=GJK convex-convex; 3=continuous convex] -> prefer 2
const int bubble_collision_flag=1; // Flag to activate collision detection for the impactor BUBBLE (1=enabled, 0=disabled)
const double bubble_assigned_radius=0.001; // Bubble geometry radius involved in collision: if 0 the original input radius is considered, 
                                       //otherwise this specified radius is taken into account. The original value in BUBBLE structure will NOT be overwritten
//Settings for Continuous collision detection; ccd_radius is the radius of CCD sphere
const int CCD_FLAG=1; //enabling continuous collision detection for toi calculation (1=enabled)
double CCD_MOTION_THRESHOLD_FACTOR=0.00001; //Continuous collision detection factor for CcdMotionThreshold=ccd_radius*CCD_MOTION_THRESHOLD_FACTOR; if 0 CCD is disabled
const double CCD_SWEPT_SPHERE_RADIUS_FACTOR=1;//0.98; //Continuous collision detection factor for CcdSweptSphereRadius=ccd_radius*CCD_SWEPT_SPHERE_RADIUS_FACTOR

double contactProcessingThreshold=0.00001;
double contactBreakingThreshold=-0.001;//1.0f;
double ME_threshold_distance=0.001+COLLISION_SHAPE_MARGIN;
double general_threshold_distance=0.00;//+COLLISION_SHAPE_MARGIN;
const int refresh_contactpoints_flag=0;

//Settings for ConvexMultiIteration
const int numPerturbationIterations=3;
const int minimumPointsPerturbationThreshold = 3;

//Settings for simulation
const int substeps=1; //number of simulationsubsteps; fixed at 1 for CCD?

