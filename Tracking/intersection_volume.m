%> @file  intersection_volumes.m
%> @brief Volumes involved in impacts for COLLISION_DATA structure 
%> @author Lorenzo Olivieri (lorenzo.olivieri@unipd.it) 
%======================================================================
%> @brief Function used to calculate the Volumes involved in impacts in 
%> COLLISION_DATA 
%> @param C_DATA       % Informations on the collision
%> @param i            % counter
%> @param j            % counter
%> 
%> @retval  V_imp_tar  % intersection ratios
%> @retval  V_tar      % Target intersection volume on target volume
%> @retval  V_imp      % Impactor intersection volume on impactor volume
%> @retval  t_b        % equivalent thichness wrt to crater depth
%> @retval  t_EQ       % Target equivalent thichness
%>
function [V_IMP_TAR, V_IMP, V_TAR, t_b, t, dp, A_IMP_inter] = intersection_volume(C_DATA,i,j)

global ME FRAGMENTS BUBBLE
global MATERIAL_LIST

c2DT= C_DATA(i).IMPACTOR(j).c_hull_impactor; % target carrot based on impactor area
c2DI= C_DATA(i).IMPACTOR(j).c_hull_target; % impactor carrot based on target area

xT=(C_DATA(i).IMPACTOR(j).c_hull_target(:,1));
yT=(C_DATA(i).IMPACTOR(j).c_hull_target(:,2));
xI=(C_DATA(i).IMPACTOR(j).c_hull_impactor(:,1));
yI=(C_DATA(i).IMPACTOR(j).c_hull_impactor(:,2));

% Convert polygon contour to clockwise vertex ordering
if ~exist(fullfile(matlabroot,'toolbox','map','map','poly2cw.m'),'file')
    Point_T=[xT yT];
    Point_T=angleSort(Point_T);
    Point_T = Point_T(end:-1:1,:);
    xT=Point_T(:,1);
    yT=Point_T(:,2);
    
    Point_I=[xI yI];
    Point_I=angleSort(Point_I);
    Point_I = Point_I(end:-1:1,:);
    xI=Point_I(:,1);
    yI=Point_I(:,2);
else
    [xI, yI] = poly2cw(xI, yI);
    [xT, yT] = poly2cw(xT, yT);
end
% Find intersection
if ~exist(fullfile(matlabroot,'toolbox','map','map','polybool.m'),'file')
    points_I=[xI,yI];
    points_T=[xT,yT];
    [pnts3, ~] = getBoundariesIntersection(points_I,points_T);
    x_PR=pnts3(:,1);
    y_PR=pnts3(:,2);
else
    [x_PR, y_PR] = polybool('intersection', xI,yI, xT,yT); %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
end

[A_IMP_inter,~,~] = polycenter(x_PR,y_PR); % Intersection area
if A_IMP_inter==0
    toll=1e-3;
    steps=10;
    for alpha=0:2*pi/steps:2*pi
        xT1=xT+toll*cos(alpha);
        yT1=yT+toll*cos(alpha);
        if ~exist(fullfile(matlabroot,'toolbox','map','map','polybool.m'),'file')
            points_I=[xI,yI];
            points_T=[xT1,yT1];
            [pnts3, ~] = getBoundariesIntersection(points_I,points_T);
            x_PR=pnts3(:,1);
            y_PR=pnts3(:,2);
        else
            [x_PR, y_PR] = polybool('intersection', xI,yI, xT1,yT1); %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        end
        [A_IMP_inter1,~,~] = polycenter(x_PR,y_PR);
        A_IMP_inter=max([A_IMP_inter1 A_IMP_inter]);
    end
end

switch C_DATA(i).TARGET.type
    case 0
        aT=FRAGMENTS(C_DATA(i).TARGET.ID).GEOMETRY_DATA.dimensions;
        c3DT=FRAGMENTS(C_DATA(i).TARGET.ID).GEOMETRY_DATA.c_hull;
    case 1
        aT=ME(C_DATA(i).TARGET.ID).GEOMETRY_DATA.dimensions;
        c3DT=ME(C_DATA(i).TARGET.ID).GEOMETRY_DATA.c_hull;
    case 2
        aT=BUBBLE(C_DATA(i).TARGET.ID).GEOMETRY_DATA.dimensions;
        c3DT=BUBBLE(C_DATA(i).TARGET.ID).GEOMETRY_DATA.c_hull;
end

switch C_DATA(i).IMPACTOR(j).type
    case 0
        aI=FRAGMENTS(C_DATA(i).IMPACTOR(j).ID).GEOMETRY_DATA.dimensions;
        c3DI=FRAGMENTS(C_DATA(i).IMPACTOR(j).ID).GEOMETRY_DATA.c_hull;
    case 1
        aI=ME(C_DATA(i).IMPACTOR(j).ID).GEOMETRY_DATA.dimensions;
        c3DI=ME(C_DATA(i).IMPACTOR(j).ID).GEOMETRY_DATA.c_hull;
    case 2
        aI=BUBBLE(C_DATA(i).IMPACTOR(j).ID).GEOMETRY_DATA.dimensions;
        c3DI=BUBBLE(C_DATA(i).IMPACTOR(j).ID).GEOMETRY_DATA.c_hull;
end

v=C_DATA(i).IMPACTOR(j).v_rel/norm(C_DATA(i).IMPACTOR(j).v_rel);


[~,vol_target] = polyhedronCentroidVolume(c3DT);
[~,vol_impactor] = polyhedronCentroidVolume(c3DI);

if vol_target>1E-8 
    int_target_v=carrot_hull(c2DT,c3DT,aT,v);
    [~,vol_int_target] = polyhedronCentroidVolume(int_target_v);
else
    vol_int_target=vol_target;
end

if vol_impactor>1E-8    
    int_impactor_v=carrot_hull(c2DI,c3DI,aI,v);
    [~,vol_int_impactor] = polyhedronCentroidVolume(int_impactor_v);
else
    vol_int_impactor=vol_impactor;
end

V_TAR = vol_int_target/vol_target ;
V_IMP   = vol_int_impactor/vol_impactor;

if V_TAR>1.5
    disp('### Target volume intersection below tolerance: volume recalculation');
    V_TAR=1;
end
if V_IMP>1.5
    disp('### Impactor volume intersection below tolerance: volume recalculation');
    V_IMP=1;
end

if C_DATA(i).TARGET.type==1
    MAT_ID_T=ME(C_DATA(i).TARGET.ID).material_ID;
elseif C_DATA(i).TARGET.type==0
    MAT_ID_T=FRAGMENTS(C_DATA(i).TARGET.ID).material_ID;
end

if C_DATA(i).IMPACTOR(j).type==1
    MAT_ID_I=ME(C_DATA(i).IMPACTOR(j).ID).material_ID;
elseif C_DATA(i).IMPACTOR(j).type==0
    MAT_ID_I=FRAGMENTS(C_DATA(i).IMPACTOR(j).ID).material_ID;
elseif C_DATA(i).IMPACTOR(j).type==2
    MAT_ID_I=BUBBLE(C_DATA(i).IMPACTOR(j).ID).material_ID;
end

k=2/k_frag(MAT_ID_T,MAT_ID_T);
Ek=C_DATA(i).IMPACTOR(j).Ek;

    

% x_PROJ=A_IMP_inter^(2/3)/vol_int_impactor;
% b=(k*Ek/((4/3)*pi*x_PROJ^2))^(1/3);

% x_TAR=A_IMP_inter^(2/3)/vol_int_target;
% t=(k*Ek/((4/3)*pi*x_TAR^2))^(1/3);

if A_IMP_inter~=0
    
    rhoT=MATERIAL_LIST(MAT_ID_T).density;
    rhoI=MATERIAL_LIST(MAT_ID_I).density;
    x_PROJ=(A_IMP_inter^(2/3)/vol_int_impactor) *(rhoT/rhoI) ;
    b=(k*Ek/((4/3)*pi*x_PROJ^2))^(1/3);
    t=vol_int_target/A_IMP_inter; %A_IMP_inter^(2/3)/vol_int_target;
    dp=2*sqrt(A_IMP_inter/pi);
    if t/b < 1e-5 % minimum value of t/b
        V_IMP_TAR=1e-5;
        t_b=1e-5;
    elseif t/b > 2/3
        V_IMP_TAR=1;
        t_b=1;
    else
        V_IMP_TAR = vol_int_target/vol_int_impactor ;
        t_b=t/b;
    end
else
    V_IMP_TAR =1e-5;
    t=max(aT);
    t_b=1e-5;
    dp=max(aI);
    
end
