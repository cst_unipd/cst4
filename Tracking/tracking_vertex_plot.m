%> @file  tracking_vertex_plot.m
%> @brief plot vertex from tracking
%> @author Cinzia Giacomuzzo (cinzia.giacomuzzo@unipd.it)
%======================================================================
%> @brief Function used to plot vertex from tracking
%>
%======================================================================
function tracking_vertex_plot(inputfile)
figure
if(exist(inputfile,'file'))
    a=load(inputfile);

    for i=1:2:length(a)
        line([a(i,1) a(i+1,1)], [a(i,2) a(i+1,2)],[a(i,3) a(i+1,3)]);
%         plot3(a(i,1),a(i,2),a(i,3),'rx');
%         hold on
%         plot3(a(i+1,1),a(i+1,2),a(i+1,3),'rx');
%         hold on
    end
    
%     mesh(a(:,1), a(:,2), a(:,3), gradient(a(:,3)))
title('Vertex Plot')
xlabel(' X axis [m]')
ylabel(' Y axis [m]')
end

% Vertex1=zeros(12,3);
% Vertex2=zeros(12,3);
% Vertex1(1,:)=[-1,48.9972,-1];
% Vertex2(1,:)=[1,48.9972,-1];
% Vertex1(2,:)=[1,48.9972,-1];
% Vertex2(2,:)=[1,50.9972,-1];
% Vertex1(3,:)=[1,50.9972,-1];
% Vertex2(3,:)=[-1,50.9972,-1];
% Vertex1(4,:)=[-1,50.9972,-1];
% Vertex2(4,:)=[-1,48.9972,-1];
% Vertex1(5,:)=[-1,48.9972,-1];
% Vertex2(5,:)=[-1,48.9972,1];
% Vertex1(6,:)=[1,48.9972,-1];
% Vertex2(6,:)=[1,48.9972,1];
% Vertex1(7,:)=[1,50.9972,-1];
% Vertex2(7,:)=[1,50.9972,1];
% Vertex1(8,:)=[-1,50.9972,-1];
% Vertex2(8,:)=[-1,50.9972,1];
% Vertex1(9,:)=[-1,48.9972,1];
% Vertex2(9,:)=[1,48.9972,1];
% Vertex1(10,:)=[1,48.9972,1];
% Vertex2(10,:)=[1,50.9972,1];
% Vertex1(11,:)=[1,50.9972,1];
% Vertex2(11,:)=[-1,50.9972,1];
% Vertex1(12,:)=[-1,50.9972,1];
% Vertex2(12,:)=[-1,48.9972,1];

% Vertex1(1,:)=[0.159096,-0.197235,0.25];
% Vertex2(1,:)=[0.159096,-0.197235,-0.25];
% Vertex1(2,:)=[0.159096,-0.197235,-0.25];
% Vertex2(2,:)=[-0.194452,0.156324,-0.25];
% Vertex1(3,:)=[-0.194452,0.156324,-0.25];
% Vertex2(3,:)=[-0.194452,0.156324,0.25];
% Vertex1(4,:)=[-0.194452,0.156324,0.25];
% Vertex2(4,:)=[0.159096,-0.197235,0.25];
% Vertex1(5,:)=[0.159096,-0.197235,0.25];
% Vertex2(5,:)=[0.194452,-0.16188,0.25];
% Vertex1(6,:)=[0.159096,-0.197235,-0.25];
% Vertex2(6,:)=[0.194452,-0.16188,-0.25];
% Vertex1(7,:)=[-0.194452,0.156324,-0.25];
% Vertex2(7,:)=[-0.159096,0.191679,-0.25];
% Vertex1(8,:)=[-0.194452,0.156324,0.25];
% Vertex2(8,:)=[-0.159096,0.191679,0.25];
% Vertex1(9,:)=[0.194452,-0.16188,0.25];
% Vertex2(9,:)=[0.194452,-0.16188,-0.25];
% Vertex1(10,:)=[0.194452,-0.16188,-0.25];
% Vertex2(10,:)=[-0.159096,0.191679,-0.25];
% Vertex1(11,:)=[-0.159096,0.191679,-0.25];
% Vertex2(11,:)=[-0.159096,0.191679,0.25];
% Vertex1(12,:)=[-0.159096,0.191679,0.25];
% Vertex2(12,:)=[0.194452,-0.16188,0.25];



% 
% for i=1:12
% %     plot3(Vertex1(i,1),Vertex1(i,2),Vertex1(i,3),'bx');
% %     hold on
% %     plot3(Vertex2(i,1),Vertex2(i,2),Vertex2(i,3),'rx');
% %     hold on
%     line([Vertex1(i,1) Vertex2(i,1)], [Vertex1(i,2) Vertex2(i,2)],[Vertex1(i,3) Vertex2(i,3)]);
%     hold on
%     line([VVertex1(i,1) VVertex2(i,1)], [VVertex1(i,2) VVertex2(i,2)],[VVertex1(i,3) VVertex2(i,3)]);
% 
% end
end