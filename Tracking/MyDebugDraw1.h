//MyDebugDraww.h
#include "LinearMath/btIDebugDraw.h"
#include <iostream>

extern double WORLD_SCALING_FACTOR;

class MyDebugDraw: public btIDebugDraw{
    int m_debugMode;

public:

    virtual void drawLine(const btVector3& from,const btVector3& to
              ,const btVector3& color);

/*    virtual void drawBox(const btVector3& bbMin, const btVector3& bbMax, const btVector3& color);
*/
    virtual void   drawContactPoint(const btVector3& PointOnB
             ,const btVector3& normalOnB,btScalar distance
              ,int lifeTime,const btVector3& color);

    virtual void   reportErrorWarning(const char* warningString);

    virtual void   draw3dText(const btVector3& location
                             ,const char* textString);

    virtual void   setDebugMode(int debugMode);

    virtual int      getDebugMode() const;
};

void MyDebugDraw::drawLine(const btVector3& from,const btVector3& to
                              ,const btVector3& color){
                              
// //   glLoadIdentity();                  // Reset the model-view matrix
//     
// //    glColor3f(0.0f,0.0f,1.0f); //blue color                              
// // glPushMatrix();
//     glBegin(GL_LINES);
// //    glColor4f(color.getX(), color.getY(), color.getZ(), 1.0);
//     glColor3f(1.0f,0.0f,0.0f);
// 
//     const GLfloat line[] = {
//         from.getX()*1, from.getY()*1, from.getZ()*1, //point A
//         to.getX()*1, to.getY()*1,to.getZ()*1 //point B
//     };
  
   FILE *fd;
  fd=fopen("tracking_vertex.txt", "a");
  fprintf(fd,"%f\t%f\t%f\n", from.getX()/WORLD_SCALING_FACTOR,from.getY()/WORLD_SCALING_FACTOR,from.getZ()/WORLD_SCALING_FACTOR);
  fclose(fd);
     
 //   std::cout << "Drawing a Shape" << std::endl;
//    std::cout << "Vertex1(,:)=["<< from.getX()<<","<<from.getY()<<","<<from.getZ()<<"];"<< std::endl;
//    std::cout << "Vertex2(,:)=["<< to.getX()<<","<<to.getY()<<","<<to.getZ()<<"];"<<std::endl;

//     glVertex3f(from.getX()*1, from.getY()*1, from.getZ()*1);
//     glVertex3f(to.getX()*1, to.getY()*1,to.getZ()*1);
//     
//     glEnd();
//    glPopMatrix();
//glutSwapBuffers();
//    glFlush();
    
}
/*
void MyDebugDraw::drawBox(const btVector3& bbMin, const btVector3& bbMax, const btVector3& color){
std::cout<<"start box"<<std::endl;
                std::cout << "bbMin=["<< bbMin[0]<<","<< bbMin[1] << "," <<bbMin[2]<<std::endl;
                std::cout << "bbMax=["<< bbMax[0]<<","<< bbMax[1] << "," <<bbMax[2]<<std::endl;

                drawLine(btVector3(bbMin[0], bbMin[1], bbMin[2]), btVector3(bbMax[0], bbMin[1], bbMin[2]), color);
                drawLine(btVector3(bbMax[0], bbMin[1], bbMin[2]), btVector3(bbMax[0], bbMax[1], bbMin[2]), color);
                drawLine(btVector3(bbMax[0], bbMax[1], bbMin[2]), btVector3(bbMin[0], bbMax[1], bbMin[2]), color);
                drawLine(btVector3(bbMin[0], bbMax[1], bbMin[2]), btVector3(bbMin[0], bbMin[1], bbMin[2]), color);
                drawLine(btVector3(bbMin[0], bbMin[1], bbMin[2]), btVector3(bbMin[0], bbMin[1], bbMax[2]), color);
                drawLine(btVector3(bbMax[0], bbMin[1], bbMin[2]), btVector3(bbMax[0], bbMin[1], bbMax[2]), color);
                drawLine(btVector3(bbMax[0], bbMax[1], bbMin[2]), btVector3(bbMax[0], bbMax[1], bbMax[2]), color);
                drawLine(btVector3(bbMin[0], bbMax[1], bbMin[2]), btVector3(bbMin[0], bbMax[1], bbMax[2]), color);
                drawLine(btVector3(bbMin[0], bbMin[1], bbMax[2]), btVector3(bbMax[0], bbMin[1], bbMax[2]), color);
                drawLine(btVector3(bbMax[0], bbMin[1], bbMax[2]), btVector3(bbMax[0], bbMax[1], bbMax[2]), color);
                drawLine(btVector3(bbMax[0], bbMax[1], bbMax[2]), btVector3(bbMin[0], bbMax[1], bbMax[2]), color);
                drawLine(btVector3(bbMin[0], bbMax[1], bbMax[2]), btVector3(bbMin[0], bbMin[1], bbMax[2]), color);

std::cout <<"end box"<<std::endl;
}
*/
void MyDebugDraw::drawContactPoint(const btVector3 &PointOnB 
         ,const btVector3 &normalOnB, btScalar distance
         ,int lifeTime, const btVector3 &color){      
         

}

void MyDebugDraw::reportErrorWarning(const char *warningString){
}

void MyDebugDraw::draw3dText(const btVector3 &location
                                , const char *textString){
}

void MyDebugDraw::setDebugMode(int debugMode){
}

int MyDebugDraw::getDebugMode() const{
    return DBG_DrawWireframe;
}