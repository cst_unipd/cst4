------------------------------------------------------------------------------
SIMULATION                     CPUTIME (min)    SIM. TIME (s)     FINAL STEP
REDSHIFT baseline                    206.45          10.0073            1997
REDSHIFT baseline no links           106.66          10.0039            1464
REDSHIFT weak struct                 311.02          10.0041            2229
REDSHIFT weak struct no links        219.97          10.0036            1841
------------------------------------------------------------------------------
