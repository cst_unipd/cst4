%> @file  check_links_status.m
%> @brief Chech the status of the links
%> @author Dr. M. Duzzi (matteo.duzzi@phd.unipd.it) & Dr. G. Sarego (giulia.sarego@unipd.it)
%> Copyright (c) 2018 CISAS - UNIVERSITY OF PADOVA
%>======================================================================
%> @brief If all links are broken, there is no point in further iterations, 
%> as the inertail motion is handeled by the tracking algorithm; 
%> 
%> @retval number of link not broken
%======================================================================
%CHECK_ALL_LINKS:
function  active_link_count = check_links_status()

global link_data;

active_link_count = 0;

for i=1:1:size(link_data,2)
    active_link_count = active_link_count + link_data(i).state;
end

if active_link_count==0
    disp('All links are broken');
end


end