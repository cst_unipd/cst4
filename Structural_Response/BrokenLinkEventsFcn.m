function [stop] = BrokenLinkEventsFcn(t,y,flag)
global N_iter

N_iter_max=1000;
stop=0;
if N_iter > N_iter_max
    stop = 1;  % Halt integration 
end