%> @file derivatives.m
%> @brief Provide the vector of derivative for the ME net
%> @author Dr. M. Duzzi (matteo.duzzi@phd.unipd.it) & Dr. G. Sarego (giulia.sarego@unipd.it)
%> Copyright (c) 2018 CISAS - UNIVERSITY OF PADOVA
%>======================================================================
function [ derivative ] = derivatives(t, X )
%DERIVATIVES Provide the vector of derivative for the ME net
%The parameter "t" is formally needed for matlab, however it is not used .

global ME_SR;        % As with "ME", contains info about  position and velocity of the MEs
global link_data;    % Information about the links between ME (material,type, etc..)
global N_iter

n_ME=length(ME_SR);
n_links=length(link_data);
N_iter=N_iter+1;

%Properly loads the initial conditions (X) onto the ME_SR structure, which is what is
%used by the other subroutines 
j=1;
jj=length(X)/2+1;
for i=1:1:n_ME
    if ME_SR(i).GEOMETRY_DATA.mass>0 
        ME_SR(i).DYNAMICS_DATA.vel=[X(j);X(j+1);X(j+2)];   %   velocity in global coordinates
        ME_SR(i).DYNAMICS_DATA.cm_coord=[X(jj);X(jj+1);X(jj+2)];         %   position in global coordinates
        j=j+3;
        jj=jj+3;
    end
end


%Computes forces exchanges
F=zeros(6,n_ME);      % Zeros the matrix of forces (and torques) exchanged between all MEs @t
for link_idx=1:1:n_links
    if link_data(link_idx).state==1 % if link 's' is intact
        ME_1=find([ME_SR.object_ID]==link_data(link_idx).ME_connect(1)); % ME1 is the ME1 index
        ME_2=find([ME_SR.object_ID]==link_data(link_idx).ME_connect(2)); % ME2 is the ME2 index
        if ~isempty(ME_1) && ~isempty(ME_2) && ME_SR(ME_1).GEOMETRY_DATA.mass>0 && ME_SR(ME_2).GEOMETRY_DATA.mass>0
            F_exchanged= force_from_j2i(link_idx); %Force exchanged through the link "link_idx"
            F(:,ME_1)=F(:,ME_1)+F_exchanged; %the 2 MEs at the end of each link are subjected to opposite forces
            F(:,ME_2)=F(:,ME_2)-F_exchanged;
        else
            link_data(link_idx).state=0;
        end
    end
end
%Re formats the output in the shape needed by the ODE
j=1;
for i=1:1:n_ME
    if ME_SR(i).GEOMETRY_DATA.mass>0 
        m_ME=ME_SR(i).GEOMETRY_DATA.mass;
        D_ME_vel(j:j+2)= F(1:3,i)/m_ME; % x acc
        D_ME_pos(j:j+2)= X(j:j+2); % given velocity
        j=j+3;
    end
end
derivative=[D_ME_vel';D_ME_pos'];
dbstop at 58 if (any(isnan(derivative)))
end

