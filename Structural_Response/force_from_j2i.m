%> @file  force_from_j2i.m
%> @brief Computes forces across the link "link_idx"
%> @author Dr. M. Duzzi (matteo.duzzi@phd.unipd.it) & Dr. G. Sarego (giulia.sarego@unipd.it)
%> Copyright (c) 2018 CISAS - UNIVERSITY OF PADOVA
%>======================================================================
%> @brief From the current ME configuration (both position and speed),
%> it computes the deformation compared to the initial/rest configuration.
%> It outputs the force in the global frame of reference.
%> @param link_idx Which link to consider
%> @retval F_j2i Force [N], with sign from the j ME to the i ME
%>
%>======================================================================
function [ F_j2i ] = force_from_j2i(link_idx)
%FORCE_FROM_J2I Calculates the forces transferred through the link "link_idx",
% from the ME "j" on the ME "i"

global link_data;
global ME_SR;

% Everything is done from the point of view of link_idx(1) ME, if we want the opposite force,
% we apply a minus in front of the global response. This assures all internal
% forces add up to zero.

% The link opposes "deformation"; more significant deformation will produce
% more intense forces/torques. A natural way to express deformation is to
% measure change of link shape (length and direction).
% However, since the configuration in the global frame of reference changes, deformation is measured in local coordinates.

%Link in GLOBAL coordinates; renaming for a more "intuitive"
%representation
idx_ME1=find([ME_SR.object_ID]==link_data(link_idx).ME_connect(1)); % idx_ME1 is the ME1 index
idx_ME2=find([ME_SR.object_ID]==link_data(link_idx).ME_connect(2)); % idx_ME2 is the ME2 index

% LOCAL Description
[X,d_X,Rot_l2g]=link_local_description(idx_ME1,idx_ME2); % LOCAL coordinates

rest(1:3,1)=link_data(link_idx).rest(1:3); %"Rest" (=equilibrium configuration) length of link in GLOBAL coordinates
rest(4:6,1)=link_data(link_idx).rest(4:6); %"Rest" direction of link GLOBAL coordinates

if link_data(link_idx).type_ID ==1 % bolted

    Stiffness_joint_bolt(link_idx,[Rot_l2g, zeros(3); zeros(3),Rot_l2g]*X); % in GLOBAL COORDINATES
    Stiffness=link_data(link_idx).k_mat;
    Viscousness=link_data(link_idx).c_mat;
    Elastic_response=Stiffness*([Rot_l2g, zeros(3); zeros(3),Rot_l2g]*X-rest);
    Viscous_response=Viscousness*([Rot_l2g, zeros(3); zeros(3),Rot_l2g]*(d_X));
    global_response= Elastic_response+Viscous_response;
    
elseif link_data(link_idx).type_ID ==2 % welded
    
    Stiffness_joint_weld(link_idx,[Rot_l2g, zeros(3); zeros(3),Rot_l2g]*X); % in GLOBAL COORDINATES
    Stiffness=link_data(link_idx).k_mat;
    Viscousness=link_data(link_idx).c_mat;
    Elastic_response=Stiffness*([Rot_l2g, zeros(3); zeros(3),Rot_l2g]*X-rest);
    Viscous_response=Viscousness*([Rot_l2g, zeros(3); zeros(3),Rot_l2g]*(d_X));
    global_response= Elastic_response+Viscous_response;
    
elseif link_data(link_idx).type_ID ==3 % glued
    
    Stiffness_joint_adhesive(link_idx,[Rot_l2g, zeros(3); zeros(3),Rot_l2g]*X); % in GLOBAL COORDINATES
    Stiffness=link_data(link_idx).k_mat;
    Viscousness=link_data(link_idx).c_mat;
    Elastic_response=Stiffness*([Rot_l2g, zeros(3); zeros(3),Rot_l2g]*X-rest);
    Viscous_response=Viscousness*([Rot_l2g, zeros(3); zeros(3),Rot_l2g]*(d_X));
    global_response= Elastic_response+Viscous_response;
    
elseif link_data(link_idx).type_ID ==4 % continuum
    
    Stiffness_joint_continuum(link_idx,[Rot_l2g, zeros(3); zeros(3),Rot_l2g]*X); % in GLOBAL COORDINATES
    Stiffness=link_data(link_idx).k_mat;
    Viscousness=link_data(link_idx).c_mat;
    Elastic_response=Stiffness*([Rot_l2g, zeros(3); zeros(3),Rot_l2g]*X-rest);
    Viscous_response=Viscousness*([Rot_l2g, zeros(3); zeros(3),Rot_l2g]*(d_X));
    global_response= Elastic_response+Viscous_response;
    
end

%Output in the Global frame of reference
F_j2i(1:3,1)=global_response(1:3); % Forces in global coordiantes
F_j2i(4:6,1)=global_response(4:6); % Torque in global coordinates

