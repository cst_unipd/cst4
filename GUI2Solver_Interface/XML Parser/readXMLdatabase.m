% function for reading the Data Structure XML file, which is provided by
% the GUI. 
 
function [MATERIAL_LIST, SEEDS_DISTRIBUTION, FRAGMENTATION_FAILURE_LIST]=readXMLdatabase(xmlFilePath)


global MATERIAL_LIST
global SEEDS_DISTRIBUTION
global FRAGMENTATION_FAILURE_LIST


% parse xml file
xmlStruc=xml2struct(xmlFilePath);

%-----------------<Read and Define Material Database>------------------------
[~,nrMatItems]=size(xmlStruc.CSTu_dashu_Database.standardu_dashu_materials.material);

% preallocate cells
mat_id = cell(nrMatItems,1);
mat_name = cell(nrMatItems,1);
rho = cell(nrMatItems,1);
E = cell(nrMatItems,1);
max_e = cell(nrMatItems,1);

i=1;
while i<=nrMatItems
%get ID
idTemp=xmlStruc.CSTu_dashu_Database.standardu_dashu_materials.material{1, i}.id.Text;
idTemp=str2num(idTemp);
mat_id{i}=idTemp;

%get name
nameTemp=xmlStruc.CSTu_dashu_Database.standardu_dashu_materials.material{1, i}.name.Text;
mat_name{i}=nameTemp;

%get density
densityTemp=xmlStruc.CSTu_dashu_Database.standardu_dashu_materials.material{1, i}.density.Text;
densityTemp=str2double(densityTemp);
rho{i}=densityTemp;

%get young
youngTemp=xmlStruc.CSTu_dashu_Database.standardu_dashu_materials.material{1, i}.young.Text;
youngTemp=str2double(youngTemp);
E{i}=youngTemp;

%get max stain
strainTemp=xmlStruc.CSTu_dashu_Database.standardu_dashu_materials.material{1, i}.maxu_strain.Text;
strainTemp=str2double(strainTemp);
max_e{i}=strainTemp;

i=i+1;
end

%define mat structure
field_mat_id='mat_id';
field_name='name';
field_den='density';
field_E='young';
field_m_strain='max_strain';

MATERIAL_LIST=struct(field_mat_id,mat_id,field_name,mat_name,field_den,rho,field_E,E,field_m_strain,max_e);

%-----------------<Read and Define Seed Distribution Database>------------------------
[~,nrSeedItems]=size(xmlStruc.CSTu_dashu_Database.standardu_dashu_seedu_dashu_distributions.seedu_dashu_distribution);

% preallocate cells
seed_id = cell(nrMatItems,1);
seed_name = cell(nrMatItems,1);
param1 = cell(nrMatItems,1);
param2 = cell(nrMatItems,1);

i=1;
while i<=nrSeedItems
%get ID
idTemp=xmlStruc.CSTu_dashu_Database.standardu_dashu_seedu_dashu_distributions.seedu_dashu_distribution{1, i}.id.Text;
idTemp=str2num(idTemp);
seed_id{i}=idTemp;

%get name
nameTemp=xmlStruc.CSTu_dashu_Database.standardu_dashu_seedu_dashu_distributions.seedu_dashu_distribution{1, i}.name.Text;
seed_name{i}=nameTemp;

%get param1
param1Temp=xmlStruc.CSTu_dashu_Database.standardu_dashu_seedu_dashu_distributions.seedu_dashu_distribution{1, i}.param1.Text;
param1Temp=str2double(param1Temp);
param1{i}=param1Temp;

%get param2
param2Temp=xmlStruc.CSTu_dashu_Database.standardu_dashu_seedu_dashu_distributions.seedu_dashu_distribution{1, i}.param2.Text;
param2Temp=str2double(param2Temp);
param2{i}=param2Temp;

i=i+1;
end

%define seed distribution structure
field_id = 'id';
field_name = 'name';
field_param1 = 'param1';
field_param2 = 'param2';

SEEDS_DISTRIBUTION = struct(field_id, seed_id, ...
                            field_name, seed_name, ...
                            field_param1, param1, ...
                            field_param2, param2);

%-----------------<Read and Define Fragmentation Failure Database>------------------------
[~,nrFragFailItems]=size(xmlStruc.CSTu_dashu_Database.standardu_dashu_fragmentationu_dashu_failures.fragmentationu_dashu_failure);

% preallocate cells
FragFail_id = cell(nrMatItems,1);
FragFail_name = cell(nrMatItems,1);

i=1;
while i<=nrFragFailItems
%get ID
idTemp=xmlStruc.CSTu_dashu_Database.standardu_dashu_fragmentationu_dashu_failures.fragmentationu_dashu_failure.id.Text;
idTemp=str2num(idTemp);
FragFail_id{i}=idTemp;

%get name
nameTemp=xmlStruc.CSTu_dashu_Database.standardu_dashu_fragmentationu_dashu_failures.fragmentationu_dashu_failure.name.Text;
FragFail_name{i}=nameTemp;

i=i+1;
end

%define seed distribution structure
field_id = 'id';
field_name = 'name';

FRAGMENTATION_FAILURE_LIST=struct(field_id,FragFail_id,field_name,FragFail_name);