% Example file for testing/implementing the xml parser

% Note: The Data Structures regarding the link geometry and properties are
% neglected at this point since, on the one hand, they are not provided by
% the GUI for SW1 and, on the other hand, etamax currently does not have any databank
% regarding the link geometries and their properties. Thus for any other
% database beside Material and Seed Distribution, the standard values of
% the CST solver should be used.

clc 
clear all

%read xml file
xmlFilePath='CstDatabase_v3.xml'; % Path to the xml file 

[MATERIAL_LIST, SEEDS_DISTRIBUTION, FRAGMENTATION_FAILURE_LIST]=readXMLdatabase(xmlFilePath);

MATERIAL_LIST(1).mat_id
MATERIAL_LIST(1).name
MATERIAL_LIST(1).young

SEEDS_DISTRIBUTION(2).id
SEEDS_DISTRIBUTION(2).name
SEEDS_DISTRIBUTION(2).param1

