% Main function to (1) extract and provide the MATERIAL LIST from the XML
% file as well as to (2) read the Dump Files and to (3) construct the ME 
% Objects (based on Dump File data) as structure type variables for the CST Solver.
function get_ME_Objs_From_Interface(xmlFilePath,dumpFilePath)

% start the parser forthe xml database and provide the resulting material database as
% a global variable.
disp('Initialise Database ...');
readXMLdatabase(xmlFilePath);
disp('... Database Initialisation Completed');

% start the parser for the dump file and collect the items in the dump file
% in internal lists.
disp('Read ME properties ...');
readCSTdumpFile(dumpFilePath);
disp('... ME Properties Aquired');

% construct the ME objects based on the lists collected from dump file. ME
% objects are provided as global variables.
disp('Initialise ME''s ...');
constructMEs();
disp('... ME Initialisation Completed');

% clear workspace
clear xmlFilePath dumpFilePath ME_LIST
