% Construct the ME's as a structure type variable as it is used by the
% Solver

function constructMEs_OLD()

% material database
global MATERIAL_LIST

% general list parameters
global  ME_GENERAL_PARAMETERS
global  GEOMETRY_GENERAL_PARAMETERS      
global  DYNAMICS_INT_GENERAL_PARAMETERS  
global  DYNAMICS_GENERAL_PARAMETERS       
global  FRAGMENTATION_GENERAL_PARAMETERS

% general list values
global ME_LIST
global GEOMETRY_LIST
global DYNAMIC_INT_LIST
global DYNAMIC_LIST
global FRAGMENTATION_LIST

% ME Objects
global ME

ME_Counter=1;
nrOfME_Objects=ME_GENERAL_PARAMETERS.nrOfItems;

% start to allocate the ME properties to the respective structure fields

% loop over ME Objects
while ME_Counter<=nrOfME_Objects
    
    % get the ID of the corresponding fields for this ME   
    corresponding_geom_id=      ME_LIST(ME_Counter).geom_data_id; 
    corresponding_mat_id=       ME_LIST(ME_Counter).material_id;    
    corresponding_dyn_int_id=   ME_LIST(ME_Counter).dyn_init_data_id;    
    corresponding_dyn_id=       ME_LIST(ME_Counter).dyn_data_id;    
    corresponding_frag_id=      ME_LIST(ME_Counter).frag_data_id;
    
    % find the related Items according to the ID's in the Data Lists
    geom_list_item      =find([GEOMETRY_LIST.geom_data_id]==corresponding_geom_id);
    mat_list_item       =find([MATERIAL_LIST.mat_id]==corresponding_mat_id);
    dyn_init_list_item  =find([DYNAMIC_INT_LIST.dyn_ini_data_id]==corresponding_dyn_int_id);
    dyn_list_item       =find([DYNAMIC_LIST.dyn_data_id]==corresponding_dyn_id);
    frag_list_item      =find([FRAGMENTATION_LIST.frag_data_id]==corresponding_frag_id);
    
    %-------------------------------------------------------
    % initialise ME Structure according to CST solver example
    %-------------------------------------------------------
    
    ME(ME_Counter).object_ID                =ME_Counter;
    ME(ME_Counter).material_ID              =mat_list_item;
    
    ME(ME_Counter).GEOMETRY_DATA.shape_ID   =getShapeIdFromName(GEOMETRY_LIST(geom_list_item).shape_name);
    ME(ME_Counter).GEOMETRY_DATA.dimensions =GEOMETRY_LIST(geom_list_item).dimensions /1000;
    ME(ME_Counter).GEOMETRY_DATA.thick      =GEOMETRY_LIST(geom_list_item).thick /1000;
    ME(ME_Counter).GEOMETRY_DATA.mass0      =GEOMETRY_LIST(geom_list_item).mass0;
    ME(ME_Counter).GEOMETRY_DATA.mass       =GEOMETRY_LIST(geom_list_item).mass;
    ME(ME_Counter).GEOMETRY_DATA.A_M_ratio  =GEOMETRY_LIST(geom_list_item).a_m_ratio;
    ME(ME_Counter).GEOMETRY_DATA.c_hull     =GEOMETRY_LIST(geom_list_item).c_hull;
    
    ME(ME_Counter).DYNAMICS_INITIAL_DATA.cm_coord0      =DYNAMIC_INT_LIST(dyn_init_list_item).cm_coord0 /1000;
    ME(ME_Counter).DYNAMICS_INITIAL_DATA.quaternions0   =DYNAMIC_INT_LIST(dyn_init_list_item).quaternions0; 
    ME(ME_Counter).DYNAMICS_INITIAL_DATA.vel0           =DYNAMIC_INT_LIST(dyn_init_list_item).vel0;           
    ME(ME_Counter).DYNAMICS_INITIAL_DATA.w0             =DYNAMIC_INT_LIST(dyn_init_list_item).w0;
    
    ME(ME_Counter).DYNAMICS_DATA.cm_coord               =DYNAMIC_LIST(dyn_list_item).cm_coord /1000;
    ME(ME_Counter).DYNAMICS_DATA.quaternions            =DYNAMIC_LIST(dyn_list_item).quaternions;
    ME(ME_Counter).DYNAMICS_DATA.vel                    =DYNAMIC_LIST(dyn_list_item).vel;
    ME(ME_Counter).DYNAMICS_DATA.w                      =DYNAMIC_LIST(dyn_list_item).w;
    ME(ME_Counter).DYNAMICS_DATA.virt_momentum          =DYNAMIC_LIST(dyn_list_item).virt_momentum;
    
    ME(ME_Counter).FRAGMENTATION_DATA.failure_ID                =FRAGMENTATION_LIST(frag_list_item).frag_fai_id;
    ME(ME_Counter).FRAGMENTATION_DATA.threshold0                =FRAGMENTATION_LIST(frag_list_item).threshold0;
    ME(ME_Counter).FRAGMENTATION_DATA.threshold                 =FRAGMENTATION_LIST(frag_list_item).threshold;
    ME(ME_Counter).FRAGMENTATION_DATA.breakup_flag              =FRAGMENTATION_LIST(frag_list_item).breakup_flag;
    ME(ME_Counter).FRAGMENTATION_DATA.ME_energy_transfer_coef   =FRAGMENTATION_LIST(frag_list_item).me_energy_tran_coef;
    ME(ME_Counter).FRAGMENTATION_DATA.cMLOSS                    =FRAGMENTATION_LIST(frag_list_item).cmloss;
    ME(ME_Counter).FRAGMENTATION_DATA.cELOSS                    =FRAGMENTATION_LIST(frag_list_item).celoss;
    ME(ME_Counter).FRAGMENTATION_DATA.c_EXPL                    =FRAGMENTATION_LIST(frag_list_item).C_expl;
    ME(ME_Counter).FRAGMENTATION_DATA.seeds_distribution_ID     =FRAGMENTATION_LIST(frag_list_item).seeds_dist_id;
    ME(ME_Counter).FRAGMENTATION_DATA.seeds_distribution_param1 =FRAGMENTATION_LIST(frag_list_item).seeds_dist_param1;
    ME(ME_Counter).FRAGMENTATION_DATA.seeds_distribution_param2 =FRAGMENTATION_LIST(frag_list_item).seeds_dist_param2;
    ME(ME_Counter).FRAGMENTATION_DATA.param_add1                =FRAGMENTATION_LIST(frag_list_item).param_add1;
    ME(ME_Counter).FRAGMENTATION_DATA.param_add2                =FRAGMENTATION_LIST(frag_list_item).param_add2;    
    
    
ME_Counter=ME_Counter+1;
end

% clear all unnecessary global variables 
global_var_general_parameters={'ME_GENERAL_PARAMETERS', 'GEOMETRY_GENERAL_PARAMETERS', ...
    'DYNAMICS_INT_GENERAL_PARAMETERS', 'DYNAMICS_GENERAL_PARAMETERS','FRAGMENTATION_GENERAL_PARAMETERS' };
global_var_lists={'ME_LIST','GEOMETRY_LIST','DYNAMIC_INT_LIST','DYNAMIC_LIST','FRAGMENTATION_LIST'};
clear (global_var_general_parameters{:})
clear (global_var_lists{:})
