% read array as string and extract the values into an real array
function array=readArrayValue(arrayString)

% remove bracers from array stirng
tempString=erase(arrayString,'(');
tempString=erase(tempString,')');

% find position of comma in string
commaPosition=strfind(tempString,',');

% get number of ',' in the string -> if 2 than 3D vector, if 3 than 4D
% vector
quantityComma=length(commaPosition);
array = zeros(1,quantityComma+1);

% adjust comma position array for a better handling in the following loop
commaTempArray=[0,commaPosition,length(tempString)+1];

m=1;
while m<=quantityComma+1
    
% % allocate the values inbetween the comma respectively
% array(m)=str2double(tempString(1:commaPosition(1)-1));
% array(m)=str2double(tempString(commaPosition(1)+1:commaPosition(2)-1));
% array(m)=str2double(tempString(commaPosition(2)+1:end));

% allocate the values inbetween the comma respectively
array(m)=str2double(tempString(commaTempArray(m)+1:commaTempArray(m+1)-1));

m=m+1;
end