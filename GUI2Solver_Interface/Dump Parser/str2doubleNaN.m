% converts a string to a double. In case no string exists, it will catch
% the resulting 'NaN' and give '0' instead
% this is needed, in case a Tag has no content
function valueDouble=str2doubleNaN(stringConvert)

valueDouble = str2double(stringConvert);

if isnan(valueDouble)
    valueDouble=0;
end