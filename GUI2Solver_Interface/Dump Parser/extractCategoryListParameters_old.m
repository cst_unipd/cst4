% get the general parameter information of the catagories

function extractCategoryListParameters_old(dumpCell,cellLength)
     
global  ME_GENERAL_PARAMETERS
global  GEOMETRY_GENERAL_PARAMETERS      
global  DYNAMICS_INT_GENERAL_PARAMETERS  
global  DYNAMICS_GENERAL_PARAMETERS       
global  FRAGMENTATION_GENERAL_PARAMETERS



nr_ME_lines=0;
nr_Geometry_lines=0;
nr_Dynamic_init_lines=0;
nr_Dynamic_lines=0;
nr_Frag_lines=0;  
    
% Loop over OCC Tags -> Get general information about the categories

i=2; % ignore first line since it describes the root tag
while i<=cellLength

    % get the OCC Tag String and Tag length from the dumpCell storing the Tag 
    [tagString, tagLength]=getTagString(dumpCell{1, 1}{i, 1});
    
    %category IDs are always positioned from 5th to 6th digit of the TAG
    %string
    categoryID=tagString(5:6);
    
    % counting the number of allocated lines for each category in the dump
    % file

    switch categoryID
        
        case '10'
          nr_ME_lines=nr_ME_lines+1;            
        case '11'
          nr_Geometry_lines=nr_Geometry_lines+1;            
        case '12'
          nr_Dynamic_init_lines=nr_Dynamic_init_lines+1;            
        case '13'
          nr_Dynamic_lines=nr_Dynamic_lines+1;           
        case '14'
          nr_Frag_lines=nr_Frag_lines+1;  
    end % end switch case for counting lines of each category      
    
    % the tag length (including ':') of the categories is always 6 characters
    if tagLength<=6
        
        % --- < extract general information of the categories> ----
        switch categoryID
            
            case '10' % ME General parameters
                
                startingLine=i;
                nrOfItems=str2double(dumpCell{1, 2}{i, 1});
                nrOfAttributes=6;
                
                ME_GENERAL_PARAMETERS.nrOfItems=nrOfItems;
                ME_GENERAL_PARAMETERS.startingLine=startingLine;
                ME_GENERAL_PARAMETERS.nrOfAttributes=nrOfAttributes;
                
            case '11' % Geometry data list
                
                startingLine=i;
                nrOfItems=str2double(dumpCell{1, 2}{i, 1});
                nrOfAttributes=8;
                
                GEOMETRY_GENERAL_PARAMETERS.nrOfItems=nrOfItems;
                GEOMETRY_GENERAL_PARAMETERS.startingLine=startingLine;
                GEOMETRY_GENERAL_PARAMETERS.nrOfAttributes=nrOfAttributes;
                
            case '12' % dynamics initial data list

                startingLine=i;
                nrOfItems=str2double(dumpCell{1, 2}{i, 1});
                nrOfAttributes=5;
                
                DYNAMICS_INT_GENERAL_PARAMETERS.nrOfItems=nrOfItems;
                DYNAMICS_INT_GENERAL_PARAMETERS.startingLine=startingLine;
                DYNAMICS_INT_GENERAL_PARAMETERS.nrOfAttributes=nrOfAttributes;
                
            case '13' % dynamics data 
                
                startingLine=i;
                nrOfItems=str2double(dumpCell{1, 2}{i, 1});
                nrOfAttributes=6;
                
                DYNAMICS_GENERAL_PARAMETERS.nrOfItems=nrOfItems;
                DYNAMICS_GENERAL_PARAMETERS.startingLine=startingLine;
                DYNAMICS_GENERAL_PARAMETERS.nrOfAttributes=nrOfAttributes;
                
            case '14' % fragmentation data
                
                startingLine=i;
                nrOfItems=str2double(dumpCell{1, 2}{i, 1});
                nrOfAttributes=12;   
                
                FRAGMENTATION_GENERAL_PARAMETERS.nrOfItems=nrOfItems;
                FRAGMENTATION_GENERAL_PARAMETERS.startingLine=startingLine;
                FRAGMENTATION_GENERAL_PARAMETERS.nrOfAttributes=nrOfAttributes;
                
        end % end of switch case 'categoryID'
        
    end % end of if case 'tagLength<=6'
    
i=i+1;    
end % end of OCC tag loop

ME_GENERAL_PARAMETERS.endingLine=ME_GENERAL_PARAMETERS.startingLine+nr_ME_lines-1;
GEOMETRY_GENERAL_PARAMETERS.endingLine=GEOMETRY_GENERAL_PARAMETERS.startingLine+nr_Geometry_lines-1;
DYNAMICS_INT_GENERAL_PARAMETERS.endingLine=DYNAMICS_INT_GENERAL_PARAMETERS.startingLine+nr_Dynamic_init_lines-1;
DYNAMICS_GENERAL_PARAMETERS.endingLine=DYNAMICS_GENERAL_PARAMETERS.startingLine+nr_Dynamic_lines-1;
FRAGMENTATION_GENERAL_PARAMETERS.endingLine=FRAGMENTATION_GENERAL_PARAMETERS.startingLine+nr_Frag_lines-1;

    

