% function for reading the CST Dumpe file, which is provided by
% the GUI, to extract the ME's 

function readCSTdumpFile(dumpFilePath)

fileID = fopen(dumpFilePath);

% parse dump file in Matlab. Use '=' as delimeter
dumpCell = textscan(fileID, '%s%s', 'Delimiter','=');

% get nr of elements in cell
cellLength=length(dumpCell{1, 1});

% get the general parameter information of the catagories
extractCategoryListParameters(dumpCell,cellLength);

% get the values from 'ME Category'
extractCategoryListValues('ME_Category', dumpCell)

% get the values from 'Geometry Category'
extractCategoryListValues('Geometry_Category', dumpCell) 

% get the values from 'Dynamics initial Category'
extractCategoryListValues('Dynamics_Int_Category', dumpCell)

% get the values from 'Dynamics Category'
extractCategoryListValues('Dynamics_Category', dumpCell)

% get the values from 'FragmentationData Category'
extractCategoryListValues('Fragmentation_Category', dumpCell)

fclose(fileID);


