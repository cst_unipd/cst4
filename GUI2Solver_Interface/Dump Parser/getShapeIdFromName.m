% get shape ID based on given shape name
function shapeID=getShapeIdFromName(shapeName)

switch shapeName
    
    case 'Box'
        
        shapeID=1;
        
    case 'Cylinder'
        
        shapeID=4;
        
    case 'Sphere'
    
        shapeID=2;
        
    case 'Hollow_Sphere'
    
        shapeID=3;  
        
    case 'Hollow_Cylinder'
    
        shapeID=5;    
end