%> @file  CST_START_USER.m
%> @brief Launcher for CST simulations and post processing
%> Copyright (c) 2018 CISAS - UNIVERSITY OF PADOVA
clear all
close all

path_initialization();
exit_message='No selection was performed - exiting from CST';

%% FIRST GUI: deciding which utility to launch
dlg = @questdlg;
% Call the dialog
button = dlg('CISAS Collision Simulation Tool: please select the desired output','','CST Simulation','Post Processing','run CST MAIN','run CST MAIN');

if isempty(button)==true
    button='Exit';
end

switch button
    %% Launching CST
    case 'CST Simulation' 
        button2 = dlg('Do you want to start last simulation or access other simulations?','','Last simulation','Load folder','Exit','Exit');
        if isempty(button2)==true
            button2='Exit';
        end
        switch button2
            case 'Last simulation'
                files = dir('Simulation_cases');
                
                if length(files)>2  % Delete '.' and '..' "folders"
                    for i1=2:-1:1
                        if files(i1).name(1)=='.'
                            files(i1)=[];
                        end
                    end
                    [~,I] = max([files(:).datenum]);
                    config=files(I).name
                    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
                    disp('Launching last simulation configuration:')
                    disp(config)
                    cst_load_and_launch(config);
                    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
                    
                else
                    disp(exit_message);
                end
            case 'Load folder'
                files = dir('Simulation_cases');
                for i=length(files):-1:1
                    if files(i).isdir ~=1
                        files(i)=[];
                    end
                end
                str = {files.name};
                [index_config,exit_2] = listdlg('PromptString','Select a simulation configuration:',...
                    'SelectionMode','single',...
                    'ListString',str);
                if  exit_2==0
                    disp(exit_message);
                else
                    config=files(index_config).name;
                    if config(1)=='.'
                        disp(exit_message);
                    else
                        disp('Launching the selected configuration:')
                        disp(config)
                        cst_load_and_launch(config);
                    end
                end
            
            case'Exit'
                disp(exit_message)
            otherwise
                disp(exit_message)
        end
    
    case 'Post Processing'
    %% Launching CST
        Post_processing2_UPGRADED
    case 'run CST MAIN'
    %% cst main with two options: standard cst main or cyclic
        button2 = dlg('Do you want to run single or cyclic CST MAIN?','','SINGLE','CYCLIC','Exit','Exit');
        if isempty(button2)==true
            button2='Exit';
        end
        switch button2
            case 'SINGLE'
                disp('SINGLE cst main');
                caso = input('Input test number to run:','s');
                caso = str2double(caso);
                if isempty(caso) || isnan(caso)
                    error('===>>> invalid test number');
                else
                    if caso < 1 || caso > 32
                        error('===>>> invalid test number: input a test number between 1 and 32');
                    end
                end
                cst_main_base_function(false,caso);
            case 'CYCLIC'
                disp('CYCLIC cst main');
                caso1 = input('Input initial test number to run:','s');
                caso1 = str2double(caso1);
                if isempty(caso1) || isnan(caso1)
                    error('===>>> invalid initial test number');
                else
                    if caso1 < 1 || caso1 > 32
                        error('===>>> invalid initial test number: input a test number between 1 and 32');
                    end
                end
                caso2 = input('Input final test number to run:','s');
                caso2 = str2double(caso2);
                if isempty(caso2) || isnan(caso2)
                    error('===>>> invalid final test number');
                else
                    if caso2 < 1 || caso2 > 32
                        error('===>>> invalid final test number: input a test number between 1 and 32');
                    end
                end
                if caso2<caso1
                    error('final test number has to be greater than initial test number');
                end
                caso = caso1:1:caso2;
                for i = 1:length(caso)
                    cst_main_base_function(true,caso(i));
                end
            case'Exit'
                disp(exit_message)
            otherwise
                disp(exit_message)
        end
    case 'Exit'
    %% Launching EXIT
        disp(exit_message)
    otherwise
        disp(exit_message)
end
