function  [ME,FRAGMENTS,BUBBLE]=link_generate_cubesat(ii,jj,kk,ME,FRAGMENTS,BUBBLE)
%GENERATE_LOFT_LINK; in this file only address which element are connected and
% with which material
global link_data;
global link_data0;

%-------------------------------------------------------------------------
%LINK INIATIALIZATION, 

f_id = 'id';
f_con = 'ME_connect';   % The pair of ME connected by the link
f_mat = 'material_ID';  % 1=Al, 2=steel, 
f_geom= 'geometry';
f_type= 'type_ID';      %1=beam, 2=surface...
f_fail= 'failure_ID';  % Type of faiulre (1=max elongation, 2= max strain...)
f_k   = 'k_mat';       % 6x6 matrix for stiffness response
f_c   = 'c_mat';       % 6x6 matrix for viscous response 
f_rest = 'rest';     % Equilibrium configuration, when no forces are exchanged
f_state = 'state';   % 1= intact, 0=broken

ID = {1,2,3,4,5,6,7,8,9,10,11,12};      %Id of the link
con = {[1,2],[2,3],[3,4],[1,4],[4,5],[3,5],[2,5],[1,5],[1,6],[2,6],[3,6],[4,6]}; %connection with the MEs
mat = {1,1,1,1,1,1,1,1,1,1,1,1};
geom={1,1,1,1,1,1,1,1,1,1,1,1};
type={1,1,1,1,1,1,1,1,1,1,1,1};
val_fail = {1,1,1,1,1,1,1,1,1,1,1,1};
val_k = {eye(6),eye(6),eye(6),eye(6),eye(6),eye(6),eye(6),eye(6),eye(6),eye(6),eye(6),eye(6)}; %Dummy, identity matrix. Filled out below
val_c = {eye(6),eye(6),eye(6),eye(6),eye(6),eye(6),eye(6),eye(6),eye(6),eye(6),eye(6),eye(6)}; %Dummy
val_rest = {ones(1,6),ones(1,6),ones(1,6),ones(1,6),ones(1,6),ones(1,6),ones(1,6),ones(1,6),ones(1,6),ones(1,6),ones(1,6),ones(1,6)}; %Dummy
val_state= {1,1,1,1,1,1,1,1,1,1,1,1}; % 1 is intact 0 is broken. 

link_data = struct(f_id,ID,f_con,con,f_mat,mat,f_geom,geom,f_type,type,f_fail,val_fail,f_state,val_state,f_k,val_k,f_c,val_c,f_rest,val_rest);

%Completes the link array with the geometries
set_link_properties();
link_data0=link_data;

end

