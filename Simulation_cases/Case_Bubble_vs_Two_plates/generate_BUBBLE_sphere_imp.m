function [ME,FRAGMENTS,BUBBLE]=generate_BUBBLE_sphere_imp(kk,jj,i,ME,FRAGMENTS,BUBBLE)

% INITIAL CONDITION TARGET
name=i;
% mass= 10;       %[kg]
SHAPE_ID = 9;   %box=1, sphere=2, hollow_sphere=3, cylinder=4, hollow cylinder =5;
dimension_abc=[0.006,0,0]; % IF box/plate (SHAPE_ID=1) [L1,L2,0], thickness=t;
% IF sphere    (SHAPE_ID=2) [R,0 ,0 ] thickness=0
% IF hollow sphere (SHAPE_ID=3) [R,0 ,0 ]
% thickness=t
% IF cylinder (SHAPE_ID=4) [R,0, H] thickness=0;
% IF hollow cylinder (SHAPE_ID=5) [R,0, H] thickness=t;
thickness = 0; %[m]
position_cm=[0;0;-1]; %[m]
Initial_vel=[0;0;6000]; %[m/s]velocity of every ME
rot_axis=[0;1;0]; % rotates the ME around this axis of rotation alpha[rad]
alpha=0; %[rad]
quaternions=[cos(alpha/2),sin(alpha/2)*rot_axis(1),sin(alpha/2)*rot_axis(2),sin(alpha/2)*rot_axis(3)];
%Default values
material_ID=1; % 1= Al, 2=TBD
mass= 0.1;       %[kg]
FAIL_TYPE=1; % 1=strian 2= TBD...
EMR= 0; % Energy to mass ratio
w=[0;0;0];
DEFAULT_seeds_distribution_ID = 0;%<<<<=== 0 random, 3 McKnight
DEFAULT_seeds_distribution_param1 = 100;
DEFAULT_seeds_distribution_param2 = 5;
%ME definition
BUBBLE(i).object_ID=name;
BUBBLE(i).material_ID=material_ID;                   % 1=Aluminium, only one available
BUBBLE(i).GEOMETRY_DATA.shape_ID=SHAPE_ID; % 1=plate, 2=sphere, 4=cylinder
BUBBLE(i).GEOMETRY_DATA.dimensions=dimension_abc;
BUBBLE(i).GEOMETRY_DATA.thick=thickness;
BUBBLE(i).GEOMETRY_DATA.mass0=mass;
BUBBLE(i).GEOMETRY_DATA.mass=mass;
BUBBLE(i).DYNAMICS_INITIAL_DATA.cm_coord0=position_cm;
BUBBLE(i).DYNAMICS_DATA.cm_coord=position_cm;
BUBBLE(i).DYNAMICS_INITIAL_DATA.quaternions0=quaternions;
BUBBLE(i).DYNAMICS_INITIAL_DATA.vel0=Initial_vel;
BUBBLE(i).DYNAMICS_INITIAL_DATA.v_exp0=500;
BUBBLE(i).DYNAMICS_DATA.vel=Initial_vel;
BUBBLE(i).DYNAMICS_INITIAL_DATA.w0=w;
BUBBLE(i).DYNAMICS_DATA.quaternions=quaternions;
BUBBLE(i).DYNAMICS_DATA.w=w;
BUBBLE(i).DYNAMICS_DATA.virt_momentum=[0,0,0]';
BUBBLE(i).DYNAMICS_DATA.v_exp=BUBBLE(i).DYNAMICS_INITIAL_DATA.v_exp0;
BUBBLE(i).GEOMETRY_DATA.A_M_ratio=0;
BUBBLE(i).GEOMETRY_DATA.c_hull=[0, 0, 0];
BUBBLE(i).FRAGMENTATION_DATA.threshold0=EMR;
BUBBLE(i).FRAGMENTATION_DATA.threshold=EMR;
BUBBLE(i).FRAGMENTATION_DATA.failure_ID=FAIL_TYPE;
BUBBLE(i).FRAGMENTATION_DATA.breakup_flag=1;
BUBBLE(i).FRAGMENTATION_DATA.ME_energy_transfer_coef=0.001;
BUBBLE(i).FRAGMENTATION_DATA.cMLOSS=0.2;
BUBBLE(i).FRAGMENTATION_DATA.cELOSS=0.1;
BUBBLE(i).FRAGMENTATION_DATA.c_EXPL=0;
BUBBLE(i).FRAGMENTATION_DATA.seeds_distribution_ID = DEFAULT_seeds_distribution_ID;
BUBBLE(i).FRAGMENTATION_DATA.seeds_distribution_param1 = DEFAULT_seeds_distribution_param1;
BUBBLE(i).FRAGMENTATION_DATA.seeds_distribution_param2 = DEFAULT_seeds_distribution_param2;
BUBBLE(i).FRAGMENTATION_DATA.param_add1=0;
BUBBLE(i).FRAGMENTATION_DATA.param_add2=0;
