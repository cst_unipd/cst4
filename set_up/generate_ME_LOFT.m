function [ME,FRAGMENTS,BUBBLE]=generate_ME_LOFT_2_MD_GS(i_ME,j_FR,k_BU,ME,FRAGMENTS,BUBBLE)

global MATERIAL_LIST

% INITIAL CONDITION AT WILL
vel=[0;0;0]; %velocity of every ME
EMR= 0; % Energy to mass ratio

DEFAULT_seeds_distribution_ID = 1;%<<<<=== 0 random cartesian, 4 random hollow
DEFAULT_seeds_distribution_param1 = 0;
DEFAULT_seeds_distribution_param2 = 5;
z_plane=2.685/2; %m
z_lower=-z_plane;
long_side=3.475;
short_side=1.003;
body_diameter=1.930; %[m]

for i=i_ME:i_ME+5
    %they are 6 panel rotated of 60 degrees each
    j=i-1; % start from zero, just because
    r=(long_side/2+body_diameter/2);
    alpha=pi/6*j;
    pos=[r*cos(j*pi/6+alpha);r*sin(j*pi/6+alpha);z_plane];
    quaternions=[cos(j*pi/6+alpha),0,0,sin(j*pi/6+alpha)];
    w=[0,0,0]';
    ME(i).object_ID=i;
    ME(i).material_ID=1;
    ME(i).GEOMETRY_DATA.shape_ID=1; %plate
    ME(i).GEOMETRY_DATA.dimensions=[long_side,short_side,(2*MATERIAL_LIST(1).density+3*2330)/(1000*MATERIAL_LIST(ME(i).material_ID).density)];
    ME(i).GEOMETRY_DATA.thick=0; %kg
    ME(i).GEOMETRY_DATA.mass0 = ME(i).GEOMETRY_DATA.dimensions(1)*ME(i).GEOMETRY_DATA.dimensions(2)*ME(i).GEOMETRY_DATA.dimensions(3)*MATERIAL_LIST(ME(i).material_ID).density; %kg
    ME(i).GEOMETRY_DATA.mass=ME(i).GEOMETRY_DATA.mass0; %kg
    ME(i).GEOMETRY_DATA.A_M_ratio=0;
    ME(i).GEOMETRY_DATA.c_hull=[0, 0, 0];
    ME(i).DYNAMICS_INITIAL_DATA.cm_coord0=pos;
    ME(i).DYNAMICS_INITIAL_DATA.quaternions0=quaternions;
    ME(i).DYNAMICS_INITIAL_DATA.vel0=vel;
    ME(i).DYNAMICS_INITIAL_DATA.w0=w;
    ME(i).DYNAMICS_DATA.cm_coord=ME(i).DYNAMICS_INITIAL_DATA.cm_coord0;
    ME(i).DYNAMICS_DATA.quaternions=ME(i).DYNAMICS_INITIAL_DATA.quaternions0;
    ME(i).DYNAMICS_DATA.vel=ME(i).DYNAMICS_INITIAL_DATA.vel0;
    ME(i).DYNAMICS_DATA.w=ME(i).DYNAMICS_INITIAL_DATA.w0;
    ME(i).DYNAMICS_DATA.virt_momentum=[0,0,0]';
    ME(i).FRAGMENTATION_DATA.failure_ID=3;
    ME(i).FRAGMENTATION_DATA.threshold0=0;
    ME(i).FRAGMENTATION_DATA.threshold=0;
    ME(i).FRAGMENTATION_DATA.breakup_flag=1;
    ME(i).FRAGMENTATION_DATA.ME_energy_transfer_coef=0.001;
    ME(i).FRAGMENTATION_DATA.cMLOSS=0.2;
    ME(i).FRAGMENTATION_DATA.cELOSS=0.1;
    ME(i).FRAGMENTATION_DATA.c_EXPL=0;
    ME(i).FRAGMENTATION_DATA.seeds_distribution_ID = DEFAULT_seeds_distribution_ID;
    ME(i).FRAGMENTATION_DATA.seeds_distribution_param1 = DEFAULT_seeds_distribution_param1;
    ME(i).FRAGMENTATION_DATA.seeds_distribution_param2 = DEFAULT_seeds_distribution_param2;
    ME(i).FRAGMENTATION_DATA.param_add1=0;
    ME(i).FRAGMENTATION_DATA.param_add2=0;
end

%center element of the Hat
i=i_ME+6;
alpha1=-60*pi/180;
rot_axis1 = [0 0 1];
quaternions=[cos(alpha1/2), sin(alpha1/2)*rot_axis1(1), sin(alpha1/2)*rot_axis1(2), sin(alpha1/2)*rot_axis1(3)];
ME(i).object_ID=i;
ME(i).material_ID=1;
ME(i).GEOMETRY_DATA.shape_ID=1; %A box
ME(i).GEOMETRY_DATA.dimensions=[0.3*body_diameter,0.6*body_diameter,0.25]; %#####
ME(i).GEOMETRY_DATA.thick=0;
ME(i).GEOMETRY_DATA.mass0 = ME(i).GEOMETRY_DATA.dimensions(1)*ME(i).GEOMETRY_DATA.dimensions(2)*ME(i).GEOMETRY_DATA.dimensions(3)*MATERIAL_LIST(ME(i).material_ID).density; %kg
ME(i).GEOMETRY_DATA.mass=ME(i).GEOMETRY_DATA.mass0; %kg
ME(i).GEOMETRY_DATA.A_M_ratio=0;
ME(i).GEOMETRY_DATA.c_hull=[0, 0, 0];
ME(i).DYNAMICS_INITIAL_DATA.cm_coord0=[0;0;z_plane+0.25];
ME(i).DYNAMICS_INITIAL_DATA.quaternions0=quaternions;
ME(i).DYNAMICS_INITIAL_DATA.vel0=vel;
ME(i).DYNAMICS_INITIAL_DATA.w0=w;
ME(i).DYNAMICS_DATA.cm_coord=ME(i).DYNAMICS_INITIAL_DATA.cm_coord0;
ME(i).DYNAMICS_DATA.quaternions=ME(i).DYNAMICS_INITIAL_DATA.quaternions0;
ME(i).DYNAMICS_DATA.vel=ME(i).DYNAMICS_INITIAL_DATA.vel0;
ME(i).DYNAMICS_DATA.w=ME(i).DYNAMICS_INITIAL_DATA.w0;
ME(i).DYNAMICS_DATA.virt_momentum=[0;0;0];
ME(i).FRAGMENTATION_DATA.failure_ID=3;
ME(i).FRAGMENTATION_DATA.threshold0=EMR;
ME(i).FRAGMENTATION_DATA.threshold=EMR;
ME(i).FRAGMENTATION_DATA.breakup_flag=1;
ME(i).FRAGMENTATION_DATA.ME_energy_transfer_coef=0.001;
ME(i).FRAGMENTATION_DATA.cMLOSS=0.2;
ME(i).FRAGMENTATION_DATA.cELOSS=0.1;
ME(i).FRAGMENTATION_DATA.c_EXPL=0;
ME(i).FRAGMENTATION_DATA.seeds_distribution_ID = DEFAULT_seeds_distribution_ID;
ME(i).FRAGMENTATION_DATA.seeds_distribution_param1 = DEFAULT_seeds_distribution_param1;
ME(i).FRAGMENTATION_DATA.seeds_distribution_param2 = DEFAULT_seeds_distribution_param2;
ME(i).FRAGMENTATION_DATA.param_add1=0;
ME(i).FRAGMENTATION_DATA.param_add2=0;

%body: Main body
long_side=2.685;
short_side=0.930*0.5;

for i=i_ME+7:i_ME+12
    %they are 6 panel rotated of 60 degrees each
    j=i-1; %start from zero, just because
    r=short_side;
    pos=[r*cos(j*pi/3);r*sin(j*pi/3);z_plane-long_side/2-0.15];
    quat2=[cos(pi/4),0,sin(pi/4),0];
    quat1=[cos(j*pi/6),0,0,-sin(j*pi/6)];
    if exist(fullfile(matlabroot,'toolbox','aero','aero','quatmultiply.m'),'file')
        quaternions=quatmultiply(quat2,quat1);
    else
        quaternions=quatxquat(quat2,quat1);
    end
    w=[0,0,0]';
    ME(i).object_ID=i;
    ME(i).material_ID=1;
    ME(i).GEOMETRY_DATA.shape_ID=1; %plate
    ME(i).GEOMETRY_DATA.dimensions=[long_side,short_side,9.26/MATERIAL_LIST(ME(i).material_ID).density];
    ME(i).GEOMETRY_DATA.thick=0; %kg
    ME(i).GEOMETRY_DATA.mass0 = ME(i).GEOMETRY_DATA.dimensions(1)*ME(i).GEOMETRY_DATA.dimensions(2)*ME(i).GEOMETRY_DATA.dimensions(3)*MATERIAL_LIST(ME(i).material_ID).density; %kg
    ME(i).GEOMETRY_DATA.mass=ME(i).GEOMETRY_DATA.mass0; %kg
    ME(i).GEOMETRY_DATA.A_M_ratio=0;
    ME(i).GEOMETRY_DATA.c_hull=[0, 0, 0];
    ME(i).DYNAMICS_INITIAL_DATA.cm_coord0=pos;
    ME(i).DYNAMICS_INITIAL_DATA.quaternions0=quaternions;
    ME(i).DYNAMICS_INITIAL_DATA.vel0=vel;
    ME(i).DYNAMICS_INITIAL_DATA.w0=w;
    ME(i).DYNAMICS_DATA.cm_coord=ME(i).DYNAMICS_INITIAL_DATA.cm_coord0;
    ME(i).DYNAMICS_DATA.quaternions=ME(i).DYNAMICS_INITIAL_DATA.quaternions0;
    ME(i).DYNAMICS_DATA.vel=ME(i).DYNAMICS_INITIAL_DATA.vel0;
    ME(i).DYNAMICS_DATA.w=ME(i).DYNAMICS_INITIAL_DATA.w0;
    ME(i).DYNAMICS_DATA.virt_momentum=[0,0,0]';
    ME(i).FRAGMENTATION_DATA.failure_ID=3;
    ME(i).FRAGMENTATION_DATA.threshold0=0;
    ME(i).FRAGMENTATION_DATA.threshold=0;
    ME(i).FRAGMENTATION_DATA.breakup_flag=1;
    ME(i).FRAGMENTATION_DATA.ME_energy_transfer_coef=0.001;
    ME(i).FRAGMENTATION_DATA.cMLOSS=0.2;
    ME(i).FRAGMENTATION_DATA.cELOSS=0.1;
    ME(i).FRAGMENTATION_DATA.c_EXPL=0;
    ME(i).FRAGMENTATION_DATA.seeds_distribution_ID = DEFAULT_seeds_distribution_ID;
    ME(i).FRAGMENTATION_DATA.seeds_distribution_param1 = DEFAULT_seeds_distribution_param1;
    ME(i).FRAGMENTATION_DATA.seeds_distribution_param2 = DEFAULT_seeds_distribution_param2;
    ME(i).FRAGMENTATION_DATA.param_add1=0;
    ME(i).FRAGMENTATION_DATA.param_add2=0;
end

%Tank (sphere)
shift_z=0.2;
i=i_ME+13;
ME(i).object_ID=i;
ME(i).material_ID=3;
ME(i).GEOMETRY_DATA.shape_ID=2; % a sphere
ME(i).GEOMETRY_DATA.dimensions=[0.64/2,0,0]; % ### body_diameter/4
ME(i).GEOMETRY_DATA.thick=0.02; %kg
ME(i).GEOMETRY_DATA.mass0 = 4/3*pi*ME(i).GEOMETRY_DATA.dimensions(1)^3*MATERIAL_LIST(ME(i).material_ID).density;%kg
ME(i).GEOMETRY_DATA.mass=ME(i).GEOMETRY_DATA.mass0; %kg
ME(i).GEOMETRY_DATA.A_M_ratio=0;
ME(i).GEOMETRY_DATA.c_hull=[0, 0, 0];
ME(i).DYNAMICS_INITIAL_DATA.cm_coord0=[0;0;z_lower-0.992*0.8/2-shift_z*1.75];
ME(i).DYNAMICS_INITIAL_DATA.quaternions0=[1,0,0,0];
ME(i).DYNAMICS_INITIAL_DATA.vel0=vel;
ME(i).DYNAMICS_INITIAL_DATA.w0=w;
ME(i).DYNAMICS_DATA.cm_coord=ME(i).DYNAMICS_INITIAL_DATA.cm_coord0;
ME(i).DYNAMICS_DATA.quaternions=ME(i).DYNAMICS_INITIAL_DATA.quaternions0;
ME(i).DYNAMICS_DATA.vel=ME(i).DYNAMICS_INITIAL_DATA.vel0;
ME(i).DYNAMICS_DATA.w=ME(i).DYNAMICS_INITIAL_DATA.w0;
ME(i).DYNAMICS_DATA.virt_momentum=[0;0;0];
ME(i).FRAGMENTATION_DATA.failure_ID=3;
ME(i).FRAGMENTATION_DATA.threshold0=EMR;
ME(i).FRAGMENTATION_DATA.threshold=EMR;
ME(i).FRAGMENTATION_DATA.breakup_flag=1;
ME(i).FRAGMENTATION_DATA.ME_energy_transfer_coef=0.001;
ME(i).FRAGMENTATION_DATA.cMLOSS=0.2;
ME(i).FRAGMENTATION_DATA.cELOSS=0.1;
ME(i).FRAGMENTATION_DATA.c_EXPL=0;
ME(i).FRAGMENTATION_DATA.seeds_distribution_ID = DEFAULT_seeds_distribution_ID;
ME(i).FRAGMENTATION_DATA.seeds_distribution_param1 = DEFAULT_seeds_distribution_param1;
ME(i).FRAGMENTATION_DATA.seeds_distribution_param2 = DEFAULT_seeds_distribution_param2;
ME(i).FRAGMENTATION_DATA.param_add1=0;
ME(i).FRAGMENTATION_DATA.param_add2=0;

%Bottom hexagonal prism
hex_diameter=1.930;
long_side=0.992; % ### 0.992
short_side=0.900; % ### 0.900

for i=i_ME+14:i_ME+19
    %they are 6 panel rotated of 60 degrees each
    j=i-1; %start from zero, just because
    r=1.05*hex_diameter/2;
    pos=[r*cos(j*pi/3);r*sin(j*pi/3);z_lower-long_side/2-shift_z*1.75];
    quaternion_1=[cos(pi/4),0,sin(pi/4),0];
    quaternion_2=[cos(j*pi/6),0,0,-sin(j*pi/6)];
    if exist(fullfile(matlabroot,'toolbox','aero','aero','quatmultiply.m'),'file')
        quaternions=quatmultiply(quaternion_1,quaternion_2);
    else
        quaternions=quatxquat(quaternion_1,quaternion_2);
    end
    w=[0;0;0];
    ME(i).object_ID=i;
    ME(i).material_ID=1;
    ME(i).GEOMETRY_DATA.shape_ID=1; %plate
    ME(i).GEOMETRY_DATA.dimensions=[long_side,short_side,3.16/MATERIAL_LIST(ME(i).material_ID).density];
    ME(i).GEOMETRY_DATA.thick=0; %kg
    ME(i).GEOMETRY_DATA.mass0 = ME(i).GEOMETRY_DATA.dimensions(1)*ME(i).GEOMETRY_DATA.dimensions(2)*ME(i).GEOMETRY_DATA.dimensions(3)*MATERIAL_LIST(ME(i).material_ID).density; %kg
    ME(i).GEOMETRY_DATA.mass=ME(i).GEOMETRY_DATA.mass0; %kg
    ME(i).GEOMETRY_DATA.A_M_ratio=0;
    ME(i).GEOMETRY_DATA.c_hull=[0, 0, 0];
    ME(i).DYNAMICS_INITIAL_DATA.cm_coord0=pos;
    ME(i).DYNAMICS_INITIAL_DATA.quaternions0=quaternions;
    ME(i).DYNAMICS_INITIAL_DATA.vel0=vel;
    ME(i).DYNAMICS_INITIAL_DATA.w0=w;
    ME(i).DYNAMICS_DATA.cm_coord=ME(i).DYNAMICS_INITIAL_DATA.cm_coord0;
    ME(i).DYNAMICS_DATA.quaternions=ME(i).DYNAMICS_INITIAL_DATA.quaternions0;
    ME(i).DYNAMICS_DATA.vel=ME(i).DYNAMICS_INITIAL_DATA.vel0;
    ME(i).DYNAMICS_DATA.w=ME(i).DYNAMICS_INITIAL_DATA.w0;
    ME(i).DYNAMICS_DATA.virt_momentum=[0;0;0];
    ME(i).FRAGMENTATION_DATA.failure_ID=3;
    ME(i).FRAGMENTATION_DATA.threshold0=EMR;
    ME(i).FRAGMENTATION_DATA.threshold=EMR;
    ME(i).FRAGMENTATION_DATA.breakup_flag=1;
    ME(i).FRAGMENTATION_DATA.ME_energy_transfer_coef=0.001;
    ME(i).FRAGMENTATION_DATA.cMLOSS=0.2;
    ME(i).FRAGMENTATION_DATA.cELOSS=0.1;
    ME(i).FRAGMENTATION_DATA.c_EXPL=0;
    ME(i).FRAGMENTATION_DATA.seeds_distribution_ID = DEFAULT_seeds_distribution_ID;
    ME(i).FRAGMENTATION_DATA.seeds_distribution_param1 = DEFAULT_seeds_distribution_param1;
    ME(i).FRAGMENTATION_DATA.seeds_distribution_param2 = DEFAULT_seeds_distribution_param2;
    ME(i).FRAGMENTATION_DATA.param_add1=0;
    ME(i).FRAGMENTATION_DATA.param_add2=0;
    
end

%Top box near the hat
i=i_ME+20;
alpha1=pi/6;
rot_axis1 = [0 0 1];
quaternions=[cos(alpha1/2), sin(alpha1/2)*rot_axis1(1), sin(alpha1/2)*rot_axis1(2), sin(alpha1/2)*rot_axis1(3)];

ME(i).object_ID=i;
ME(i).material_ID=1;
ME(i).GEOMETRY_DATA.shape_ID=1; %A box
ME(i).GEOMETRY_DATA.dimensions=[0.3875,0.194,0.504];
ME(i).GEOMETRY_DATA.thick=0;
ME(i).GEOMETRY_DATA.mass0 = ME(i).GEOMETRY_DATA.dimensions(1)*ME(i).GEOMETRY_DATA.dimensions(2)*ME(i).GEOMETRY_DATA.dimensions(3)*MATERIAL_LIST(ME(i).material_ID).density; %kg
ME(i).GEOMETRY_DATA.mass=ME(i).GEOMETRY_DATA.mass0; %kg
ME(i).GEOMETRY_DATA.A_M_ratio=0;
ME(i).GEOMETRY_DATA.c_hull=[0, 0, 0];
ME(i).DYNAMICS_INITIAL_DATA.cm_coord0=[-0.4;-0.7;z_plane+0.35];
ME(i).DYNAMICS_INITIAL_DATA.quaternions0=quaternions;
ME(i).DYNAMICS_INITIAL_DATA.vel0=vel;
ME(i).DYNAMICS_INITIAL_DATA.w0=w;
ME(i).DYNAMICS_DATA.cm_coord=ME(i).DYNAMICS_INITIAL_DATA.cm_coord0;
ME(i).DYNAMICS_DATA.quaternions=ME(i).DYNAMICS_INITIAL_DATA.quaternions0;
ME(i).DYNAMICS_DATA.vel=ME(i).DYNAMICS_INITIAL_DATA.vel0;
ME(i).DYNAMICS_DATA.w=ME(i).DYNAMICS_INITIAL_DATA.w0;
ME(i).DYNAMICS_DATA.virt_momentum=[0;0;0];
ME(i).FRAGMENTATION_DATA.failure_ID=3;
ME(i).FRAGMENTATION_DATA.threshold0=EMR;
ME(i).FRAGMENTATION_DATA.threshold=EMR;
ME(i).FRAGMENTATION_DATA.breakup_flag=1;
ME(i).FRAGMENTATION_DATA.ME_energy_transfer_coef=0.001;
ME(i).FRAGMENTATION_DATA.cMLOSS=0.2;
ME(i).FRAGMENTATION_DATA.cELOSS=0.1;
ME(i).FRAGMENTATION_DATA.c_EXPL=0;
ME(i).FRAGMENTATION_DATA.seeds_distribution_ID = DEFAULT_seeds_distribution_ID;
ME(i).FRAGMENTATION_DATA.seeds_distribution_param1 = DEFAULT_seeds_distribution_param1;
ME(i).FRAGMENTATION_DATA.seeds_distribution_param2 = DEFAULT_seeds_distribution_param2;
ME(i).FRAGMENTATION_DATA.param_add1=0;
ME(i).FRAGMENTATION_DATA.param_add2=0;

%Solar panel
i=i_ME+21;
pos=[r*cos(0*pi/3);r*sin(0*pi/3);z_lower-shift_z];
quaternion_1=[cos(pi/4),0,sin(pi/4),0];
ME(i).object_ID=i;
ME(i).material_ID=1;
ME(i).GEOMETRY_DATA.shape_ID=1; %plate
ME(i).GEOMETRY_DATA.dimensions=[2.7,7.213,0.0013];
ME(i).GEOMETRY_DATA.thick=0; %kg
ME(i).GEOMETRY_DATA.mass0 = ME(i).GEOMETRY_DATA.dimensions(1)*ME(i).GEOMETRY_DATA.dimensions(2)*ME(i).GEOMETRY_DATA.dimensions(3)*MATERIAL_LIST(ME(i).material_ID).density; %kg
ME(i).GEOMETRY_DATA.mass=ME(i).GEOMETRY_DATA.mass0; %kg
ME(i).GEOMETRY_DATA.A_M_ratio=0;
ME(i).GEOMETRY_DATA.c_hull=[0, 0, 0];
ME(i).DYNAMICS_INITIAL_DATA.cm_coord0=pos-[-0.1;0;1.7]; % ### 1.5
ME(i).DYNAMICS_INITIAL_DATA.quaternions0=quaternion_1;
ME(i).DYNAMICS_INITIAL_DATA.vel0=vel;
ME(i).DYNAMICS_INITIAL_DATA.w0=w;
ME(i).DYNAMICS_DATA.cm_coord=ME(i).DYNAMICS_INITIAL_DATA.cm_coord0;
ME(i).DYNAMICS_DATA.quaternions=ME(i).DYNAMICS_INITIAL_DATA.quaternions0;
ME(i).DYNAMICS_DATA.vel=ME(i).DYNAMICS_INITIAL_DATA.vel0;
ME(i).DYNAMICS_DATA.w=ME(i).DYNAMICS_INITIAL_DATA.w0;
ME(i).DYNAMICS_DATA.virt_momentum=[0,0,0]';
ME(i).FRAGMENTATION_DATA.failure_ID=3;
ME(i).FRAGMENTATION_DATA.threshold0=EMR;
ME(i).FRAGMENTATION_DATA.threshold=EMR;
ME(i).FRAGMENTATION_DATA.breakup_flag=1;
ME(i).FRAGMENTATION_DATA.ME_energy_transfer_coef=0.001;
ME(i).FRAGMENTATION_DATA.cMLOSS=0.2;
ME(i).FRAGMENTATION_DATA.cELOSS=0.1;
ME(i).FRAGMENTATION_DATA.c_EXPL=0;
ME(i).FRAGMENTATION_DATA.seeds_distribution_ID = DEFAULT_seeds_distribution_ID;
ME(i).FRAGMENTATION_DATA.seeds_distribution_param1 = DEFAULT_seeds_distribution_param1;
ME(i).FRAGMENTATION_DATA.seeds_distribution_param2 = DEFAULT_seeds_distribution_param2;
ME(i).FRAGMENTATION_DATA.param_add1=0;
ME(i).FRAGMENTATION_DATA.param_add2=0;

%Top hexagonal wings
hex_diameter=1.430;
long_side=1.350; %
short_side=0.475; %
j=0;
for i=i_ME+22:i_ME+27
    %they are 6 panel rotated of 60 degrees each
    %     j=i-1; %start from zero, just because
    r=hex_diameter/2;
    alpha=30*pi/180;
    pos=[r*cos(j*pi/3+alpha);r*sin(j*pi/3+alpha);z_plane-long_side/2-0.15];
    quaternion_1=[cos(pi/4),0,sin(pi/4),0];
    rot_axis2 = [0 0 1];
    if j==0 || j==3
        alpha2=60*pi/180;
    elseif j==1 || j==4
        alpha2=0;
    elseif j==2 || j==5
        alpha2=-60*pi/180;
    end
    quaternion_2=[cos(alpha2/2), sin(alpha2/2)*rot_axis2(1), sin(alpha2/2)*rot_axis2(2), sin(alpha2/2)*rot_axis2(3)];
    if exist(fullfile(matlabroot,'toolbox','aero','aero','quatmultiply.m'),'file')
        quaternions=quatmultiply(quaternion_1,quaternion_2);
    else
        quaternions=quatxquat(quaternion_1,quaternion_2);
    end
    j=j+1;
    w=[0;0;0];
    ME(i).object_ID=i;
    ME(i).material_ID=1;
    ME(i).GEOMETRY_DATA.shape_ID=1; %plate
    ME(i).GEOMETRY_DATA.dimensions=[long_side,short_side,3.56/MATERIAL_LIST(ME(i).material_ID).density];
    ME(i).GEOMETRY_DATA.thick=0; %kg
    ME(i).GEOMETRY_DATA.mass0 = ME(i).GEOMETRY_DATA.dimensions(1)*ME(i).GEOMETRY_DATA.dimensions(2)*ME(i).GEOMETRY_DATA.dimensions(3)*MATERIAL_LIST(ME(i).material_ID).density; %kg
    ME(i).GEOMETRY_DATA.mass=ME(i).GEOMETRY_DATA.mass0; %kg
    ME(i).GEOMETRY_DATA.A_M_ratio=0;
    ME(i).GEOMETRY_DATA.c_hull=[0, 0, 0];
    ME(i).DYNAMICS_INITIAL_DATA.cm_coord0=pos;
    ME(i).DYNAMICS_INITIAL_DATA.quaternions0=quaternions;
    ME(i).DYNAMICS_INITIAL_DATA.vel0=vel;
    ME(i).DYNAMICS_INITIAL_DATA.w0=w;
    ME(i).DYNAMICS_DATA.cm_coord=ME(i).DYNAMICS_INITIAL_DATA.cm_coord0;
    ME(i).DYNAMICS_DATA.quaternions=ME(i).DYNAMICS_INITIAL_DATA.quaternions0;
    ME(i).DYNAMICS_DATA.vel=ME(i).DYNAMICS_INITIAL_DATA.vel0;
    ME(i).DYNAMICS_DATA.w=ME(i).DYNAMICS_INITIAL_DATA.w0;
    ME(i).DYNAMICS_DATA.virt_momentum=[0;0;0];
    ME(i).FRAGMENTATION_DATA.failure_ID=3;
    ME(i).FRAGMENTATION_DATA.threshold0=EMR;
    ME(i).FRAGMENTATION_DATA.threshold=EMR;
    ME(i).FRAGMENTATION_DATA.breakup_flag=1;
    ME(i).FRAGMENTATION_DATA.ME_energy_transfer_coef=0.001;
    ME(i).FRAGMENTATION_DATA.cMLOSS=0.2;
    ME(i).FRAGMENTATION_DATA.cELOSS=0.1;
    ME(i).FRAGMENTATION_DATA.c_EXPL=0;
    ME(i).FRAGMENTATION_DATA.seeds_distribution_ID = DEFAULT_seeds_distribution_ID;
    ME(i).FRAGMENTATION_DATA.seeds_distribution_param1 = DEFAULT_seeds_distribution_param1;
    ME(i).FRAGMENTATION_DATA.seeds_distribution_param2 = DEFAULT_seeds_distribution_param2;
    ME(i).FRAGMENTATION_DATA.param_add1=0;
    ME(i).FRAGMENTATION_DATA.param_add2=0;
end

long_side=0.992;
%2 Boxes inside the bottom hexagonal prism
j=0;
for i=i_ME+28:i_ME+29
    r2=1.05-0.12;
    pos=[r2*cos(2*pi/3+8/180*pi*j);r2*sin(2*pi/3+8/180*pi*j);z_lower-long_side+0.6-shift_z];
    quaternion_2=[cos(2*pi/6),0,0,-sin(2*pi/6)];
    ME(i).object_ID=i;
    ME(i).material_ID=1;
    ME(i).GEOMETRY_DATA.shape_ID=1; %box
    ME(i).GEOMETRY_DATA.dimensions=[0.07,0.11,0.13];
    ME(i).GEOMETRY_DATA.thick=0; %kg
    ME(i).GEOMETRY_DATA.mass0 = ME(i).GEOMETRY_DATA.dimensions(1)*ME(i).GEOMETRY_DATA.dimensions(2)*ME(i).GEOMETRY_DATA.dimensions(3)*MATERIAL_LIST(ME(i).material_ID).density; %kg
    ME(i).GEOMETRY_DATA.mass=ME(i).GEOMETRY_DATA.mass0; %kg
    ME(i).GEOMETRY_DATA.A_M_ratio=0;
    ME(i).GEOMETRY_DATA.c_hull=[0, 0, 0];
    ME(i).DYNAMICS_INITIAL_DATA.cm_coord0=pos;
    ME(i).DYNAMICS_INITIAL_DATA.quaternions0=quaternion_2;
    ME(i).DYNAMICS_INITIAL_DATA.vel0=vel;
    ME(i).DYNAMICS_INITIAL_DATA.w0=w;
    ME(i).DYNAMICS_DATA.cm_coord=ME(i).DYNAMICS_INITIAL_DATA.cm_coord0;
    ME(i).DYNAMICS_DATA.quaternions=ME(i).DYNAMICS_INITIAL_DATA.quaternions0;
    ME(i).DYNAMICS_DATA.vel=ME(i).DYNAMICS_INITIAL_DATA.vel0;
    ME(i).DYNAMICS_DATA.w=ME(i).DYNAMICS_INITIAL_DATA.w0;
    ME(i).DYNAMICS_DATA.virt_momentum=[0;0;0];
    ME(i).FRAGMENTATION_DATA.failure_ID=3;
    ME(i).FRAGMENTATION_DATA.threshold0=EMR;
    ME(i).FRAGMENTATION_DATA.threshold=EMR;
    ME(i).FRAGMENTATION_DATA.breakup_flag=1;
    ME(i).FRAGMENTATION_DATA.ME_energy_transfer_coef=0.001;
    ME(i).FRAGMENTATION_DATA.cMLOSS=0.2;
    ME(i).FRAGMENTATION_DATA.cELOSS=0.1;
    ME(i).FRAGMENTATION_DATA.c_EXPL=0;
    ME(i).FRAGMENTATION_DATA.seeds_distribution_ID = DEFAULT_seeds_distribution_ID;
    ME(i).FRAGMENTATION_DATA.seeds_distribution_param1 = DEFAULT_seeds_distribution_param1;
    ME(i).FRAGMENTATION_DATA.seeds_distribution_param2 = DEFAULT_seeds_distribution_param2;
    ME(i).FRAGMENTATION_DATA.param_add1=0;
    ME(i).FRAGMENTATION_DATA.param_add2=0;
    j=j+1;
end

long_side=0.992;
%4 Cylinders inside the bottom hexagonal prism
j=0;
for i=i_ME+30:i_ME+33
    r2=0.8*1.05-0.12;
    if j==0
        pos=[r2*cos(pi)-short_side/12;r2*sin(pi)+short_side/3;z_lower-long_side/2+0.05-shift_z];
        alpha2=-pi/4;
        rot_axis2 = [0 1 0];
        quaternions=[cos(alpha2/2), sin(alpha2/2)*rot_axis2(1), sin(alpha2/2)*rot_axis2(2), sin(alpha2/2)*rot_axis2(3)];
    elseif j==1
        pos=[r2*cos(pi)-short_side/12;r2*sin(pi)-short_side/3;z_lower-long_side/2+0.05-shift_z];
        alpha1=pi/2;
        rot_axis1 = [1 0 0];
        quaternion_1=[cos(alpha1/2), sin(alpha1/2)*rot_axis1(1), sin(alpha1/2)*rot_axis1(2), sin(alpha1/2)*rot_axis1(3)];
        alpha2=-pi/6;
        rot_axis2 = [0 0 1];
        quaternion_2=[cos(alpha2/2), sin(alpha2/2)*rot_axis2(1), sin(alpha2/2)*rot_axis2(2), sin(alpha2/2)*rot_axis2(3)];
        if exist(fullfile(matlabroot,'toolbox','aero','aero','quatmultiply.m'),'file')
            quaternions=quatmultiply(quaternion_1,quaternion_2);
        else
            quaternions=quatxquat(quaternion_1,quaternion_2);
        end
    elseif j==2
        pos=[r2*cos(pi)-short_side/12;r2*sin(pi)+short_side/3;z_lower-long_side+0.05-shift_z];
        alpha2=pi/4;
        rot_axis2 = [0 1 0];
        quaternions=[cos(alpha2/2), sin(alpha2/2)*rot_axis2(1), sin(alpha2/2)*rot_axis2(2), sin(alpha2/2)*rot_axis2(3)];
    elseif j==3
        pos=[r2*cos(pi)-short_side/12;r2*sin(pi)-short_side/3;z_lower-long_side+0.05-shift_z];
        alpha1=pi/2;
        rot_axis1 = [1 0 0];
        quaternion_1=[cos(alpha1/2), sin(alpha1/2)*rot_axis1(1), sin(alpha1/2)*rot_axis1(2), sin(alpha1/2)*rot_axis1(3)];
        alpha2=-pi/6;
        rot_axis2 = [0 0 1];
        quaternion_2=[cos(alpha2/2), sin(alpha2/2)*rot_axis2(1), sin(alpha2/2)*rot_axis2(2), sin(alpha2/2)*rot_axis2(3)];
        if exist(fullfile(matlabroot,'toolbox','aero','aero','quatmultiply.m'),'file')
            quaternions=quatmultiply(quaternion_1,quaternion_2);
        else
            quaternions=quatxquat(quaternion_1,quaternion_2);
        end
    end
    ME(i).object_ID=i;
    ME(i).material_ID=1;                   % 1=Aluminium, only one available
    ME(i).GEOMETRY_DATA.shape_ID=4; % 1=plate, 2=sphere, 4=cylinder
    ME(i).GEOMETRY_DATA.dimensions=[0.1635,0,0.125];
    ME(i).GEOMETRY_DATA.thick=0;
    ME(i).GEOMETRY_DATA.mass0 = pi*ME(i).GEOMETRY_DATA.dimensions(1)^2*ME(i).GEOMETRY_DATA.dimensions(3)*MATERIAL_LIST(ME(i).material_ID).density; %kg
    ME(i).GEOMETRY_DATA.mass=ME(i).GEOMETRY_DATA.mass0; %kg
    ME(i).DYNAMICS_INITIAL_DATA.cm_coord0=pos;
    ME(i).DYNAMICS_INITIAL_DATA.quaternions0=quaternions;
    ME(i).DYNAMICS_INITIAL_DATA.vel0=vel;
    ME(i).DYNAMICS_INITIAL_DATA.w0=w;
    ME(i).DYNAMICS_DATA.cm_coord=ME(i).DYNAMICS_INITIAL_DATA.cm_coord0;
    ME(i).DYNAMICS_DATA.quaternions=ME(i).DYNAMICS_INITIAL_DATA.quaternions0;
    ME(i).DYNAMICS_DATA.vel=ME(i).DYNAMICS_INITIAL_DATA.vel0;
    ME(i).DYNAMICS_DATA.w=ME(i).DYNAMICS_INITIAL_DATA.w0;
    ME(i).DYNAMICS_DATA.virt_momentum=[0,0,0];
    ME(i).FRAGMENTATION_DATA.failure_ID=3;
    ME(i).FRAGMENTATION_DATA.threshold0=EMR;
    ME(i).FRAGMENTATION_DATA.threshold=EMR;
    ME(i).FRAGMENTATION_DATA.breakup_flag=1;
    ME(i).FRAGMENTATION_DATA.ME_energy_transfer_coef=0.001;
    ME(i).FRAGMENTATION_DATA.cMLOSS=0.2;
    ME(i).FRAGMENTATION_DATA.cELOSS=0.1;
    ME(i).FRAGMENTATION_DATA.c_EXPL=0;
    ME(i).FRAGMENTATION_DATA.seeds_distribution_ID = DEFAULT_seeds_distribution_ID;
    ME(i).FRAGMENTATION_DATA.seeds_distribution_param1 = DEFAULT_seeds_distribution_param1;
    ME(i).FRAGMENTATION_DATA.seeds_distribution_param2 = DEFAULT_seeds_distribution_param2;
    ME(i).FRAGMENTATION_DATA.param_add1=0;
    ME(i).FRAGMENTATION_DATA.param_add2=0;
    ME(i).GEOMETRY_DATA.A_M_ratio=0;
    ME(i).GEOMETRY_DATA.c_hull=[0, 0, 0];
    j=j+1;
end

long_side=0.992;
%3 Boxes inside the bottom hexagonal prism
j=0;
for i=i_ME+34:i_ME+36
    r2=1.05-0.2;
    pos=[r2*cos(4*pi/3);r2*sin(4*pi/3);z_lower-long_side+0.35*j-shift_z];
    alpha1=-60*pi/180;
    rot_axis1 = [0 0 1];
    quaternions=[cos(alpha1/2), sin(alpha1/2)*rot_axis1(1), sin(alpha1/2)*rot_axis1(2), sin(alpha1/2)*rot_axis1(3)];
    ME(i).object_ID=i;
    ME(i).material_ID=1;
    ME(i).GEOMETRY_DATA.shape_ID=1; %box
    ME(i).GEOMETRY_DATA.dimensions=[0.19,0.419,0.238];
    if j==1
        ME(i).GEOMETRY_DATA.dimensions=[0.265,0.43,0.336];
    end
    ME(i).GEOMETRY_DATA.thick=0; %kg
    ME(i).GEOMETRY_DATA.mass0 = ME(i).GEOMETRY_DATA.dimensions(1)*ME(i).GEOMETRY_DATA.dimensions(2)*ME(i).GEOMETRY_DATA.dimensions(3)*MATERIAL_LIST(ME(i).material_ID).density; %kg
    ME(i).GEOMETRY_DATA.mass=ME(i).GEOMETRY_DATA.mass0; %kg
    ME(i).GEOMETRY_DATA.A_M_ratio=0;
    ME(i).GEOMETRY_DATA.c_hull=[0, 0, 0];
    ME(i).DYNAMICS_INITIAL_DATA.cm_coord0=pos;
    ME(i).DYNAMICS_INITIAL_DATA.quaternions0=quaternions;
    ME(i).DYNAMICS_INITIAL_DATA.vel0=vel;
    ME(i).DYNAMICS_INITIAL_DATA.w0=w;
    ME(i).DYNAMICS_DATA.cm_coord=ME(i).DYNAMICS_INITIAL_DATA.cm_coord0;
    ME(i).DYNAMICS_DATA.quaternions=ME(i).DYNAMICS_INITIAL_DATA.quaternions0;
    ME(i).DYNAMICS_DATA.vel=ME(i).DYNAMICS_INITIAL_DATA.vel0;
    ME(i).DYNAMICS_DATA.w=ME(i).DYNAMICS_INITIAL_DATA.w0;
    ME(i).DYNAMICS_DATA.virt_momentum=[0;0;0];
    ME(i).FRAGMENTATION_DATA.failure_ID=3;
    ME(i).FRAGMENTATION_DATA.threshold0=EMR;
    ME(i).FRAGMENTATION_DATA.threshold=EMR;
    ME(i).FRAGMENTATION_DATA.breakup_flag=1;
    ME(i).FRAGMENTATION_DATA.ME_energy_transfer_coef=0.001;
    ME(i).FRAGMENTATION_DATA.cMLOSS=0.2;
    ME(i).FRAGMENTATION_DATA.cELOSS=0.1;
    ME(i).FRAGMENTATION_DATA.c_EXPL=0;
    ME(i).FRAGMENTATION_DATA.seeds_distribution_ID = DEFAULT_seeds_distribution_ID;
    ME(i).FRAGMENTATION_DATA.seeds_distribution_param1 = DEFAULT_seeds_distribution_param1;
    ME(i).FRAGMENTATION_DATA.seeds_distribution_param2 = DEFAULT_seeds_distribution_param2;
    ME(i).FRAGMENTATION_DATA.param_add1=0;
    ME(i).FRAGMENTATION_DATA.param_add2=0;
    j=j+1;
end

long_side=0.992;
short_side=0.475; %
%5 Boxes inside the bottom hexagonal prism
j=0;
for i=i_ME+37:i_ME+41
    r2=1.05-0.2;
    alpha1=60*pi/180;
    rot_axis1 = [0 0 1];
    quaternions=[cos(alpha1/2), sin(alpha1/2)*rot_axis1(1), sin(alpha1/2)*rot_axis1(2), sin(alpha1/2)*rot_axis1(3)];
    ME(i).object_ID=i;
    ME(i).material_ID=1;
    ME(i).GEOMETRY_DATA.shape_ID=1; %box
    if j==0
        pos=[r2*cos(5*pi/3);r2*sin(5*pi/3);z_lower-long_side-shift_z];
        ME(i).GEOMETRY_DATA.dimensions=[0.278,0.35,0.26];
    elseif j==1
        pos=[r2*cos(5*pi/3)-short_side/4;r2*sin(5*pi/3)-short_side/4;z_lower-long_side+0.35-shift_z];
        ME(i).GEOMETRY_DATA.dimensions=[0.1,0.3,0.2];
    elseif j==2
        pos=[r2*cos(5*pi/3+15*pi/180)+short_side/10;r2*sin(5*pi/3+15*pi/180)-short_side/8;z_lower-long_side+0.35-shift_z];
        ME(i).GEOMETRY_DATA.dimensions=[0.1,0.3,0.2];
    elseif j==3
        pos=[r2*cos(5*pi/3)-short_side/4;r2*sin(5*pi/3)-short_side/4;z_lower-long_side+0.6-shift_z];
        ME(i).GEOMETRY_DATA.dimensions=[0.1,0.2,0.2];
    elseif j==4
        pos=[r2*cos(5*pi/3+15*pi/180)+short_side/10;r2*sin(5*pi/3+15*pi/180)-short_side/8;z_lower-long_side+0.6-shift_z];
        ME(i).GEOMETRY_DATA.dimensions=[0.1,0.2,0.2];
    end
    ME(i).GEOMETRY_DATA.thick=0; %kg
    ME(i).GEOMETRY_DATA.mass0 = ME(i).GEOMETRY_DATA.dimensions(1)*ME(i).GEOMETRY_DATA.dimensions(2)*ME(i).GEOMETRY_DATA.dimensions(3)*MATERIAL_LIST(ME(i).material_ID).density; %kg
    ME(i).GEOMETRY_DATA.mass=ME(i).GEOMETRY_DATA.mass0; %kg
    ME(i).GEOMETRY_DATA.A_M_ratio=0;
    ME(i).GEOMETRY_DATA.c_hull=[0, 0, 0];
    ME(i).DYNAMICS_INITIAL_DATA.cm_coord0=pos;
    ME(i).DYNAMICS_INITIAL_DATA.quaternions0=quaternions;
    ME(i).DYNAMICS_INITIAL_DATA.vel0=vel;
    ME(i).DYNAMICS_INITIAL_DATA.w0=w;
    ME(i).DYNAMICS_DATA.cm_coord=ME(i).DYNAMICS_INITIAL_DATA.cm_coord0;
    ME(i).DYNAMICS_DATA.quaternions=ME(i).DYNAMICS_INITIAL_DATA.quaternions0;
    ME(i).DYNAMICS_DATA.vel=ME(i).DYNAMICS_INITIAL_DATA.vel0;
    ME(i).DYNAMICS_DATA.w=ME(i).DYNAMICS_INITIAL_DATA.w0;
    ME(i).DYNAMICS_DATA.virt_momentum=[0;0;0];
    ME(i).FRAGMENTATION_DATA.failure_ID=3;
    ME(i).FRAGMENTATION_DATA.threshold0=EMR;
    ME(i).FRAGMENTATION_DATA.threshold=EMR;
    ME(i).FRAGMENTATION_DATA.breakup_flag=1;
    ME(i).FRAGMENTATION_DATA.ME_energy_transfer_coef=0.001;
    ME(i).FRAGMENTATION_DATA.cMLOSS=0.2;
    ME(i).FRAGMENTATION_DATA.cELOSS=0.1;
    ME(i).FRAGMENTATION_DATA.c_EXPL=0;
    ME(i).FRAGMENTATION_DATA.seeds_distribution_ID = DEFAULT_seeds_distribution_ID;
    ME(i).FRAGMENTATION_DATA.seeds_distribution_param1 = DEFAULT_seeds_distribution_param1;
    ME(i).FRAGMENTATION_DATA.seeds_distribution_param2 = DEFAULT_seeds_distribution_param2;
    ME(i).FRAGMENTATION_DATA.param_add1=0;
    ME(i).FRAGMENTATION_DATA.param_add2=0;
    j=j+1;
end

%3 Cylinders along the main body
z_plane=2.685/2; %m
long_side=2.685;
body_diameter=1.930*0.98; %[m]

j=0;
for i=i_ME+42:i_ME+44
    if j==0
        pos=[0;0;z_plane];
        quaternions=[1,0,0,0];
    elseif j==1
        pos=[0;0;z_plane-long_side-shift_z*1.5];
        quaternions=[1,0,0,0];
    elseif j==2
        pos=[0;0;-long_side-shift_z/2];
        quaternions=[1,0,0,0];
    end
    ME(i).object_ID=i;
    ME(i).material_ID=1;                   % 1=Aluminium, only one available
    ME(i).GEOMETRY_DATA.shape_ID=4; % 1=plate, 2=sphere, 4=cylinder
    ME(i).GEOMETRY_DATA.dimensions=[body_diameter/2,0,4.4/MATERIAL_LIST(ME(i).material_ID).density];
    ME(i).GEOMETRY_DATA.thick=0;
    ME(i).GEOMETRY_DATA.mass0 = pi*ME(i).GEOMETRY_DATA.dimensions(1)^2*ME(i).GEOMETRY_DATA.dimensions(3)*MATERIAL_LIST(ME(i).material_ID).density; %kg
    ME(i).GEOMETRY_DATA.mass=ME(i).GEOMETRY_DATA.mass0; %kg
    ME(i).DYNAMICS_INITIAL_DATA.cm_coord0=pos;
    ME(i).DYNAMICS_INITIAL_DATA.quaternions0=quaternions;
    ME(i).DYNAMICS_INITIAL_DATA.vel0=vel;
    ME(i).DYNAMICS_INITIAL_DATA.w0=w;
    ME(i).DYNAMICS_DATA.cm_coord=ME(i).DYNAMICS_INITIAL_DATA.cm_coord0;
    ME(i).DYNAMICS_DATA.quaternions=ME(i).DYNAMICS_INITIAL_DATA.quaternions0;
    ME(i).DYNAMICS_DATA.vel=ME(i).DYNAMICS_INITIAL_DATA.vel0;
    ME(i).DYNAMICS_DATA.w=ME(i).DYNAMICS_INITIAL_DATA.w0;
    ME(i).DYNAMICS_DATA.virt_momentum=[0,0,0];
    ME(i).FRAGMENTATION_DATA.failure_ID=3;
    ME(i).FRAGMENTATION_DATA.threshold0=EMR;
    ME(i).FRAGMENTATION_DATA.threshold=EMR;
    ME(i).FRAGMENTATION_DATA.breakup_flag=1;
    ME(i).FRAGMENTATION_DATA.ME_energy_transfer_coef=0.001;
    ME(i).FRAGMENTATION_DATA.cMLOSS=0.2;
    ME(i).FRAGMENTATION_DATA.cELOSS=0.1;
    ME(i).FRAGMENTATION_DATA.c_EXPL=0;
    ME(i).FRAGMENTATION_DATA.seeds_distribution_ID = DEFAULT_seeds_distribution_ID;
    ME(i).FRAGMENTATION_DATA.seeds_distribution_param1 = DEFAULT_seeds_distribution_param1;
    ME(i).FRAGMENTATION_DATA.seeds_distribution_param2 = DEFAULT_seeds_distribution_param2;
    ME(i).FRAGMENTATION_DATA.param_add1=0;
    ME(i).FRAGMENTATION_DATA.param_add2=0;
    ME(i).GEOMETRY_DATA.A_M_ratio=0;
    ME(i).GEOMETRY_DATA.c_hull=[0, 0, 0];
    j=j+1;
end

%Bottom hexagonal prism inside
long_side=0.992; % ### 0.992
short_side=0.930*0.5; % ### 0.900
thickness=0.01;

for i=i_ME+45:i_ME+50
    %they are 6 panel rotated of 60 degrees each
    j=i-1; %start from zero, just because
    r=short_side;
    pos=[r*cos(j*pi/3);r*sin(j*pi/3);z_lower-long_side/2-shift_z*1.75];
    quaternion_1=[cos(pi/4),0,sin(pi/4),0];
    quaternion_2=[cos(j*pi/6),0,0,-sin(j*pi/6)];
    if exist(fullfile(matlabroot,'toolbox','aero','aero','quatmultiply.m'),'file')
        quaternions=quatmultiply(quaternion_1,quaternion_2);
    else
        quaternions=quatxquat(quaternion_1,quaternion_2);
    end
    w=[0;0;0];
    ME(i).object_ID=i;
    ME(i).material_ID=1;
    ME(i).GEOMETRY_DATA.shape_ID=1; %plate
    ME(i).GEOMETRY_DATA.dimensions=[long_side,short_side,thickness];
    ME(i).GEOMETRY_DATA.thick=0; %kg
    ME(i).GEOMETRY_DATA.mass0 = ME(i).GEOMETRY_DATA.dimensions(1)*ME(i).GEOMETRY_DATA.dimensions(2)*ME(i).GEOMETRY_DATA.dimensions(3)*MATERIAL_LIST(ME(i).material_ID).density; %kg
    ME(i).GEOMETRY_DATA.mass=ME(i).GEOMETRY_DATA.mass0; %kg
    ME(i).GEOMETRY_DATA.A_M_ratio=0;
    ME(i).GEOMETRY_DATA.c_hull=[0, 0, 0];
    ME(i).DYNAMICS_INITIAL_DATA.cm_coord0=pos;
    ME(i).DYNAMICS_INITIAL_DATA.quaternions0=quaternions;
    ME(i).DYNAMICS_INITIAL_DATA.vel0=vel;
    ME(i).DYNAMICS_INITIAL_DATA.w0=w;
    ME(i).DYNAMICS_DATA.cm_coord=ME(i).DYNAMICS_INITIAL_DATA.cm_coord0;
    ME(i).DYNAMICS_DATA.quaternions=ME(i).DYNAMICS_INITIAL_DATA.quaternions0;
    ME(i).DYNAMICS_DATA.vel=ME(i).DYNAMICS_INITIAL_DATA.vel0;
    ME(i).DYNAMICS_DATA.w=ME(i).DYNAMICS_INITIAL_DATA.w0;
    ME(i).DYNAMICS_DATA.virt_momentum=[0;0;0];
    ME(i).FRAGMENTATION_DATA.failure_ID=3;
    ME(i).FRAGMENTATION_DATA.threshold0=EMR;
    ME(i).FRAGMENTATION_DATA.threshold=EMR;
    ME(i).FRAGMENTATION_DATA.breakup_flag=1;
    ME(i).FRAGMENTATION_DATA.ME_energy_transfer_coef=0.001;
    ME(i).FRAGMENTATION_DATA.cMLOSS=0.2;
    ME(i).FRAGMENTATION_DATA.cELOSS=0.1;
    ME(i).FRAGMENTATION_DATA.c_EXPL=0;
    ME(i).FRAGMENTATION_DATA.seeds_distribution_ID = DEFAULT_seeds_distribution_ID;
    ME(i).FRAGMENTATION_DATA.seeds_distribution_param1 = DEFAULT_seeds_distribution_param1;
    ME(i).FRAGMENTATION_DATA.seeds_distribution_param2 = DEFAULT_seeds_distribution_param2;
    ME(i).FRAGMENTATION_DATA.param_add1=0;
    ME(i).FRAGMENTATION_DATA.param_add2=0;
    
end

end
