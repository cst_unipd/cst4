function [ME,FRAGMENTS,BUBBLE]=generate_ReDSHIFT2(i_ME,jj,kk,ME,FRAGMENTS,BUBBLE)

global MATERIAL_LIST

% INITIAL CONDITION AT WILL
Initial_vel=[0;0;0]; %velocity of every ME
EMR= 0; % Energy to mass ratio
SHAPE_ID=1;
FAIL_TYPE=3;

DEFAULT_seeds_distribution_ID = 1;%<<<<=== 0 random cartesian, 4 random hollow
DEFAULT_seeds_distribution_param1 = 0;
DEFAULT_seeds_distribution_param2 = 5;

box_l=0.227; %[m]
z_plane=box_l/2; %m
z_lower=-z_plane;
long_side=2*z_plane;
short_side=2*z_plane;
thickness=0.0018; % computed to match the mass
dimension_abc=[long_side,short_side,thickness];
material_ID=12; % AlSi10Mg
mass=long_side*short_side*thickness*MATERIAL_LIST(material_ID).density; %[kg]

% 4 Lateral plates
for i=i_ME:i_ME+3
    jj=i-1;
    r=long_side/2+thickness;
    position_cm=[r*cos(jj*pi/2);r*sin(jj*pi/2);0];
    quat2=[cos(pi/4),0,sin(pi/4),0];
    quat1=[cos(jj*pi/4),0,0,-sin(jj*pi/4)];
    if exist(fullfile(matlabroot,'toolbox','aero','aero','quatmultiply.m'),'file')
        quaternions=quatmultiply(quat2,quat1);
    else
        quaternions=quatxquat(quat2,quat1);
    end
    w=[0,0,0]';
    ME(i).object_ID=i;
    ME(i).material_ID=material_ID;                   % 1=Aluminium, only one available
    ME(i).GEOMETRY_DATA.shape_ID=SHAPE_ID; % 1=plate, 2=sphere, 4=cylinder
    ME(i).GEOMETRY_DATA.dimensions=dimension_abc;
    ME(i).GEOMETRY_DATA.thick=thickness;
    ME(i).GEOMETRY_DATA.mass0=mass;
    ME(i).GEOMETRY_DATA.mass=mass;
    ME(i).DYNAMICS_INITIAL_DATA.cm_coord0=position_cm;
    ME(i).DYNAMICS_DATA.cm_coord=position_cm;
    ME(i).DYNAMICS_INITIAL_DATA.quaternions0=quaternions;
    ME(i).DYNAMICS_INITIAL_DATA.vel0=Initial_vel;
    ME(i).DYNAMICS_DATA.vel=Initial_vel;
    ME(i).DYNAMICS_INITIAL_DATA.w0=w;
    ME(i).DYNAMICS_DATA.quaternions=quaternions;
    ME(i).DYNAMICS_DATA.w=w;
    ME(i).DYNAMICS_DATA.virt_momentum=[0,0,0]';
    ME(i).GEOMETRY_DATA.A_M_ratio=0;
    ME(i).GEOMETRY_DATA.c_hull=[0, 0, 0];
    ME(i).FRAGMENTATION_DATA.threshold0=EMR;
    ME(i).FRAGMENTATION_DATA.threshold=EMR;
    ME(i).FRAGMENTATION_DATA.failure_ID=FAIL_TYPE;
    ME(i).FRAGMENTATION_DATA.breakup_flag=1;
    ME(i).FRAGMENTATION_DATA.ME_energy_transfer_coef=0.001;
    ME(i).FRAGMENTATION_DATA.cMLOSS=0.2;
    ME(i).FRAGMENTATION_DATA.cELOSS=0.1;
    ME(i).FRAGMENTATION_DATA.c_EXPL=0;
    ME(i).FRAGMENTATION_DATA.seeds_distribution_ID = DEFAULT_seeds_distribution_ID;
    ME(i).FRAGMENTATION_DATA.seeds_distribution_param1 = DEFAULT_seeds_distribution_param1;
    ME(i).FRAGMENTATION_DATA.seeds_distribution_param2 = DEFAULT_seeds_distribution_param2;
    ME(i).FRAGMENTATION_DATA.param_add1=0;
    ME(i).FRAGMENTATION_DATA.param_add2=0;
end

% Upper hat
i=i_ME+4;
position_cm=[0;0;z_plane+thickness];
quaternions=[1,0,0,0];
w=[0,0,0]';
ME(i).object_ID=i;
ME(i).material_ID=material_ID;                   % 1=Aluminium, only one available
ME(i).GEOMETRY_DATA.shape_ID=SHAPE_ID; % 1=plate, 2=sphere, 4=cylinder
ME(i).GEOMETRY_DATA.dimensions=dimension_abc;
ME(i).GEOMETRY_DATA.thick=thickness;
ME(i).GEOMETRY_DATA.mass0=mass;
ME(i).GEOMETRY_DATA.mass=mass;
ME(i).DYNAMICS_INITIAL_DATA.cm_coord0=position_cm;
ME(i).DYNAMICS_DATA.cm_coord=position_cm;
ME(i).DYNAMICS_INITIAL_DATA.quaternions0=quaternions;
ME(i).DYNAMICS_INITIAL_DATA.vel0=Initial_vel;
ME(i).DYNAMICS_DATA.vel=Initial_vel;
ME(i).DYNAMICS_INITIAL_DATA.w0=w;
ME(i).DYNAMICS_DATA.quaternions=quaternions;
ME(i).DYNAMICS_DATA.w=w;
ME(i).DYNAMICS_DATA.virt_momentum=[0,0,0]';
ME(i).GEOMETRY_DATA.A_M_ratio=0;
ME(i).GEOMETRY_DATA.c_hull=[0, 0, 0];
ME(i).FRAGMENTATION_DATA.threshold0=EMR;
ME(i).FRAGMENTATION_DATA.threshold=EMR;
ME(i).FRAGMENTATION_DATA.failure_ID=FAIL_TYPE;
ME(i).FRAGMENTATION_DATA.breakup_flag=1;
ME(i).FRAGMENTATION_DATA.ME_energy_transfer_coef=0.001;
ME(i).FRAGMENTATION_DATA.cMLOSS=0.2;
ME(i).FRAGMENTATION_DATA.cELOSS=0.1;
ME(i).FRAGMENTATION_DATA.c_EXPL=0;
ME(i).FRAGMENTATION_DATA.seeds_distribution_ID = DEFAULT_seeds_distribution_ID;
ME(i).FRAGMENTATION_DATA.seeds_distribution_param1 = DEFAULT_seeds_distribution_param1;
ME(i).FRAGMENTATION_DATA.seeds_distribution_param2 = DEFAULT_seeds_distribution_param2;
ME(i).FRAGMENTATION_DATA.param_add1=0;
ME(i).FRAGMENTATION_DATA.param_add2=0;

% Lower hat
i=i_ME+5;
position_cm=[0;0;z_lower-thickness];
quaternions=[1,0,0,0];
w=[0,0,0]';
ME(i).object_ID=i;
ME(i).material_ID=material_ID;                   % 1=Aluminium, only one available
ME(i).GEOMETRY_DATA.shape_ID=SHAPE_ID; % 1=plate, 2=sphere, 4=cylinder
ME(i).GEOMETRY_DATA.dimensions=dimension_abc;
ME(i).GEOMETRY_DATA.thick=thickness;
ME(i).GEOMETRY_DATA.mass0=mass;
ME(i).GEOMETRY_DATA.mass=mass;
ME(i).DYNAMICS_INITIAL_DATA.cm_coord0=position_cm;
ME(i).DYNAMICS_DATA.cm_coord=position_cm;
ME(i).DYNAMICS_INITIAL_DATA.quaternions0=quaternions;
ME(i).DYNAMICS_INITIAL_DATA.vel0=Initial_vel;
ME(i).DYNAMICS_DATA.vel=Initial_vel;
ME(i).DYNAMICS_INITIAL_DATA.w0=w;
ME(i).DYNAMICS_DATA.quaternions=quaternions;
ME(i).DYNAMICS_DATA.w=w;
ME(i).DYNAMICS_DATA.virt_momentum=[0,0,0]';
ME(i).GEOMETRY_DATA.A_M_ratio=0;
ME(i).GEOMETRY_DATA.c_hull=[0, 0, 0];
ME(i).FRAGMENTATION_DATA.threshold0=EMR;
ME(i).FRAGMENTATION_DATA.threshold=EMR;
ME(i).FRAGMENTATION_DATA.failure_ID=FAIL_TYPE;
ME(i).FRAGMENTATION_DATA.breakup_flag=1;
ME(i).FRAGMENTATION_DATA.ME_energy_transfer_coef=0.001;
ME(i).FRAGMENTATION_DATA.cMLOSS=0.2;
ME(i).FRAGMENTATION_DATA.cELOSS=0.1;
ME(i).FRAGMENTATION_DATA.c_EXPL=0;
ME(i).FRAGMENTATION_DATA.seeds_distribution_ID = DEFAULT_seeds_distribution_ID;
ME(i).FRAGMENTATION_DATA.seeds_distribution_param1 = DEFAULT_seeds_distribution_param1;
ME(i).FRAGMENTATION_DATA.seeds_distribution_param2 = DEFAULT_seeds_distribution_param2;
ME(i).FRAGMENTATION_DATA.param_add1=0;
ME(i).FRAGMENTATION_DATA.param_add2=0;

% Central plate - equipment tray
material_ID=9; % carbon fiber
i=i_ME+6;
pos=[0;0;0];
ME(i).object_ID=i;
ME(i).material_ID=material_ID;
ME(i).GEOMETRY_DATA.shape_ID=1; %plate
ME(i).GEOMETRY_DATA.dimensions=[0.2013,0.2013,0.0088];
ME(i).GEOMETRY_DATA.thick=0.0088; %kg
ME(i).GEOMETRY_DATA.mass0 = ME(i).GEOMETRY_DATA.dimensions(1)*ME(i).GEOMETRY_DATA.dimensions(2)*ME(i).GEOMETRY_DATA.dimensions(3)*631; % MATERIAL_LIST(ME(i).material_ID).density; %kg
ME(i).GEOMETRY_DATA.mass=ME(i).GEOMETRY_DATA.mass0; %kg
ME(i).GEOMETRY_DATA.A_M_ratio=0;
ME(i).GEOMETRY_DATA.c_hull=[0, 0, 0];
ME(i).DYNAMICS_INITIAL_DATA.cm_coord0=pos; % ### 1.5
ME(i).DYNAMICS_INITIAL_DATA.quaternions0=quaternions;
ME(i).DYNAMICS_INITIAL_DATA.vel0=Initial_vel;
ME(i).DYNAMICS_INITIAL_DATA.w0=w;
ME(i).DYNAMICS_DATA.cm_coord=ME(i).DYNAMICS_INITIAL_DATA.cm_coord0;
ME(i).DYNAMICS_DATA.quaternions=ME(i).DYNAMICS_INITIAL_DATA.quaternions0;
ME(i).DYNAMICS_DATA.vel=ME(i).DYNAMICS_INITIAL_DATA.vel0;
ME(i).DYNAMICS_DATA.w=ME(i).DYNAMICS_INITIAL_DATA.w0;
ME(i).DYNAMICS_DATA.virt_momentum=[0,0,0]';
ME(i).FRAGMENTATION_DATA.failure_ID=FAIL_TYPE;
ME(i).FRAGMENTATION_DATA.threshold0=EMR;
ME(i).FRAGMENTATION_DATA.threshold=EMR;
ME(i).FRAGMENTATION_DATA.breakup_flag=1;
ME(i).FRAGMENTATION_DATA.ME_energy_transfer_coef=0.001;
ME(i).FRAGMENTATION_DATA.cMLOSS=0.2;
ME(i).FRAGMENTATION_DATA.cELOSS=0.1;
ME(i).FRAGMENTATION_DATA.c_EXPL=0;
ME(i).FRAGMENTATION_DATA.seeds_distribution_ID = DEFAULT_seeds_distribution_ID;
ME(i).FRAGMENTATION_DATA.seeds_distribution_param1 = DEFAULT_seeds_distribution_param1;
ME(i).FRAGMENTATION_DATA.seeds_distribution_param2 = DEFAULT_seeds_distribution_param2;
ME(i).FRAGMENTATION_DATA.param_add1=0;
ME(i).FRAGMENTATION_DATA.param_add2=0;

% 5 Boxes on the central plate (reaction wheels,XTU,IMU)
material_ID=1; %Al
quaternions=[1,0,0,0];
j=0;
for i=i_ME+7:i_ME+11
    ME(i).object_ID=i;
    ME(i).material_ID=material_ID; % Al
    ME(i).GEOMETRY_DATA.shape_ID=1; %box
    if j==0 % reaction wheels
        pos=[-box_l/2+2*thickness+0.01+0.046/2; -0.01; 0.046/2+thickness*1.5]; 
        ME(i).GEOMETRY_DATA.dimensions=[0.046,0.040,0.046];
        ME(i).GEOMETRY_DATA.mass0 = ME(i).GEOMETRY_DATA.dimensions(1)*ME(i).GEOMETRY_DATA.dimensions(2)*ME(i).GEOMETRY_DATA.dimensions(3)*2186; % MATERIAL_LIST(ME(i).material_ID).density; %kg
    elseif j==1 % reaction wheels
        pos=[-box_l/2+2*thickness+0.01+0.046/2; -0.01-0.05; 0.046/2+thickness*1.5];
        ME(i).GEOMETRY_DATA.dimensions=[0.046,0.046,0.040];
        ME(i).GEOMETRY_DATA.mass0 = ME(i).GEOMETRY_DATA.dimensions(1)*ME(i).GEOMETRY_DATA.dimensions(2)*ME(i).GEOMETRY_DATA.dimensions(3)*2186; % MATERIAL_LIST(ME(i).material_ID).density; %kg
    elseif j==2 % reaction wheels
        pos=[-box_l/2+2*thickness+0.01+0.046/2+0.05; -0.01-0.05; 0.046/2+thickness*1.5];
        ME(i).GEOMETRY_DATA.dimensions=[0.040,0.046,0.046];
        ME(i).GEOMETRY_DATA.mass0 = ME(i).GEOMETRY_DATA.dimensions(1)*ME(i).GEOMETRY_DATA.dimensions(2)*ME(i).GEOMETRY_DATA.dimensions(3)*2186; % MATERIAL_LIST(ME(i).material_ID).density; %kg
    elseif j==3 % XTU
        pos=[-box_l/2+2*thickness+0.01+0.046/2+0.05+0.075; -0.01-0.05-0.008; 0.086/2+thickness*1.5];
        ME(i).GEOMETRY_DATA.dimensions=[0.094,0.024,0.086];
        ME(i).GEOMETRY_DATA.mass0 = ME(i).GEOMETRY_DATA.dimensions(1)*ME(i).GEOMETRY_DATA.dimensions(2)*ME(i).GEOMETRY_DATA.dimensions(3)*2320; % MATERIAL_LIST(ME(i).material_ID).density; %kg
    elseif j==4 % IMU
        pos=[-box_l/2+2*thickness+0.155; -box_l/2+2*thickness+0.163; 0.047+thickness*1.5];
        ME(i).GEOMETRY_DATA.dimensions=[0.0205,0.038,0.032];
        ME(i).GEOMETRY_DATA.mass0 = ME(i).GEOMETRY_DATA.dimensions(1)*ME(i).GEOMETRY_DATA.dimensions(2)*ME(i).GEOMETRY_DATA.dimensions(3)*2085; % MATERIAL_LIST(ME(i).material_ID).density; %kg
    end
    ME(i).GEOMETRY_DATA.thick=0; %kg
    ME(i).GEOMETRY_DATA.mass=ME(i).GEOMETRY_DATA.mass0; %kg
    ME(i).GEOMETRY_DATA.A_M_ratio=0;
    ME(i).GEOMETRY_DATA.c_hull=[0, 0, 0];
    ME(i).DYNAMICS_INITIAL_DATA.cm_coord0=pos;
    ME(i).DYNAMICS_INITIAL_DATA.quaternions0=quaternions;
    ME(i).DYNAMICS_INITIAL_DATA.vel0=Initial_vel;
    ME(i).DYNAMICS_INITIAL_DATA.w0=w;
    ME(i).DYNAMICS_DATA.cm_coord=ME(i).DYNAMICS_INITIAL_DATA.cm_coord0;
    ME(i).DYNAMICS_DATA.quaternions=ME(i).DYNAMICS_INITIAL_DATA.quaternions0;
    ME(i).DYNAMICS_DATA.vel=ME(i).DYNAMICS_INITIAL_DATA.vel0;
    ME(i).DYNAMICS_DATA.w=ME(i).DYNAMICS_INITIAL_DATA.w0;
    ME(i).DYNAMICS_DATA.virt_momentum=[0,0,0]';
    ME(i).FRAGMENTATION_DATA.failure_ID=FAIL_TYPE;
    ME(i).FRAGMENTATION_DATA.threshold0=EMR;
    ME(i).FRAGMENTATION_DATA.threshold=EMR;
    ME(i).FRAGMENTATION_DATA.breakup_flag=1;
    ME(i).FRAGMENTATION_DATA.ME_energy_transfer_coef=0.001;
    ME(i).FRAGMENTATION_DATA.cMLOSS=0.2;
    ME(i).FRAGMENTATION_DATA.cELOSS=0.1;
    ME(i).FRAGMENTATION_DATA.c_EXPL=0;
    ME(i).FRAGMENTATION_DATA.seeds_distribution_ID = DEFAULT_seeds_distribution_ID;
    ME(i).FRAGMENTATION_DATA.seeds_distribution_param1 = DEFAULT_seeds_distribution_param1;
    ME(i).FRAGMENTATION_DATA.seeds_distribution_param2 = DEFAULT_seeds_distribution_param2;
    ME(i).FRAGMENTATION_DATA.param_add1=0;
    ME(i).FRAGMENTATION_DATA.param_add2=0;
    j=j+1;
end

% 4 Plates (inside) bracket
material_ID=12; % AlSi10Mg
for i=i_ME+12:i_ME+15
    jj=i-1;
    r=0.06/2+3/2*thickness;
    position_cm=[r*cos(jj*pi/2);r*sin(jj*pi/2);0.071/2+thickness];
    quat2=[cos(pi/4),0,sin(pi/4),0];
    quat1=[cos(jj*pi/4),0,0,-sin(jj*pi/4)];
    if exist(fullfile(matlabroot,'toolbox','aero','aero','quatmultiply.m'),'file')
        quaternions=quatmultiply(quat2,quat1);
    else
        quaternions=quatxquat(quat2,quat1);
    end
    w=[0,0,0]';
    ME(i).object_ID=i;
    ME(i).material_ID=material_ID;                   % 1=Aluminium, only one available
    ME(i).GEOMETRY_DATA.shape_ID=SHAPE_ID; % 1=plate, 2=sphere, 4=cylinder
    ME(i).GEOMETRY_DATA.dimensions=[0.065,0.063,thickness];
    ME(i).GEOMETRY_DATA.thick=thickness;
    ME(i).GEOMETRY_DATA.mass0 = ME(i).GEOMETRY_DATA.dimensions(1)*ME(i).GEOMETRY_DATA.dimensions(2)*ME(i).GEOMETRY_DATA.dimensions(3)*MATERIAL_LIST(ME(i).material_ID).density*1.3; %kg
    ME(i).GEOMETRY_DATA.mass=ME(i).GEOMETRY_DATA.mass0; %kg
    ME(i).DYNAMICS_INITIAL_DATA.cm_coord0=position_cm;
    ME(i).DYNAMICS_DATA.cm_coord=position_cm;
    ME(i).DYNAMICS_INITIAL_DATA.quaternions0=quaternions;
    ME(i).DYNAMICS_INITIAL_DATA.vel0=Initial_vel;
    ME(i).DYNAMICS_DATA.vel=Initial_vel;
    ME(i).DYNAMICS_INITIAL_DATA.w0=w;
    ME(i).DYNAMICS_DATA.quaternions=quaternions;
    ME(i).DYNAMICS_DATA.w=w;
    ME(i).DYNAMICS_DATA.virt_momentum=[0,0,0]';
    ME(i).GEOMETRY_DATA.A_M_ratio=0;
    ME(i).GEOMETRY_DATA.c_hull=[0, 0, 0];
    ME(i).FRAGMENTATION_DATA.threshold0=EMR;
    ME(i).FRAGMENTATION_DATA.threshold=EMR;
    ME(i).FRAGMENTATION_DATA.failure_ID=FAIL_TYPE;
    ME(i).FRAGMENTATION_DATA.breakup_flag=1;
    ME(i).FRAGMENTATION_DATA.ME_energy_transfer_coef=0.001;
    ME(i).FRAGMENTATION_DATA.cMLOSS=0.2;
    ME(i).FRAGMENTATION_DATA.cELOSS=0.1;
    ME(i).FRAGMENTATION_DATA.c_EXPL=0;
    ME(i).FRAGMENTATION_DATA.seeds_distribution_ID = DEFAULT_seeds_distribution_ID;
    ME(i).FRAGMENTATION_DATA.seeds_distribution_param1 = DEFAULT_seeds_distribution_param1;
    ME(i).FRAGMENTATION_DATA.seeds_distribution_param2 = DEFAULT_seeds_distribution_param2;
    ME(i).FRAGMENTATION_DATA.param_add1=0;
    ME(i).FRAGMENTATION_DATA.param_add2=0;
end

% 2 Cylinders on central plate (star tracker)
material_ID=1; % Al
long_side=0.209;
for i=i_ME+16:i_ME+17
    if i==i_ME+16
        pos=[long_side/2-0.055/2;0;0.0475];
        alpha2=-pi/2;
        rot_axis2 = [0 1 0];
        quaternions=[cos(alpha2/2), sin(alpha2/2)*rot_axis2(1), sin(alpha2/2)*rot_axis2(2), sin(alpha2/2)*rot_axis2(3)];
    elseif i==i_ME+17
        pos=[0;long_side/2-0.055/2;0.0475];
        alpha2=-pi/2;
        rot_axis2 = [1 0 0];
        quaternions=[cos(alpha2/2), sin(alpha2/2)*rot_axis2(1), sin(alpha2/2)*rot_axis2(2), sin(alpha2/2)*rot_axis2(3)];
    end
    ME(i).object_ID=i;
    ME(i).material_ID=material_ID;
    ME(i).GEOMETRY_DATA.shape_ID=4; % 1=plate, 2=sphere, 4=cylinder
    ME(i).GEOMETRY_DATA.dimensions=[0.062/2,0,0.055];
    ME(i).GEOMETRY_DATA.thick=0;
    ME(i).GEOMETRY_DATA.mass0 = pi*ME(i).GEOMETRY_DATA.dimensions(1)^2*ME(i).GEOMETRY_DATA.dimensions(3)*1690;% MATERIAL_LIST(ME(i).material_ID).density; %kg
    ME(i).GEOMETRY_DATA.mass=ME(i).GEOMETRY_DATA.mass0; %kg
    ME(i).DYNAMICS_INITIAL_DATA.cm_coord0=pos;
    ME(i).DYNAMICS_INITIAL_DATA.quaternions0=quaternions;
    ME(i).DYNAMICS_INITIAL_DATA.vel0=Initial_vel;
    ME(i).DYNAMICS_INITIAL_DATA.w0=w;
    ME(i).DYNAMICS_DATA.cm_coord=ME(i).DYNAMICS_INITIAL_DATA.cm_coord0;
    ME(i).DYNAMICS_DATA.quaternions=ME(i).DYNAMICS_INITIAL_DATA.quaternions0;
    ME(i).DYNAMICS_DATA.vel=ME(i).DYNAMICS_INITIAL_DATA.vel0;
    ME(i).DYNAMICS_DATA.w=ME(i).DYNAMICS_INITIAL_DATA.w0;
    ME(i).DYNAMICS_DATA.virt_momentum=[0,0,0]';
    ME(i).GEOMETRY_DATA.A_M_ratio=0;
    ME(i).GEOMETRY_DATA.c_hull=[0, 0, 0];
    ME(i).FRAGMENTATION_DATA.threshold0=EMR;
    ME(i).FRAGMENTATION_DATA.threshold=EMR;
    ME(i).FRAGMENTATION_DATA.failure_ID=FAIL_TYPE;
    ME(i).FRAGMENTATION_DATA.breakup_flag=1;
    ME(i).FRAGMENTATION_DATA.ME_energy_transfer_coef=0.001;
    ME(i).FRAGMENTATION_DATA.cMLOSS=0.2;
    ME(i).FRAGMENTATION_DATA.cELOSS=0.1;
    ME(i).FRAGMENTATION_DATA.c_EXPL=0;
    ME(i).FRAGMENTATION_DATA.seeds_distribution_ID = DEFAULT_seeds_distribution_ID;
    ME(i).FRAGMENTATION_DATA.seeds_distribution_param1 = DEFAULT_seeds_distribution_param1;
    ME(i).FRAGMENTATION_DATA.seeds_distribution_param2 = DEFAULT_seeds_distribution_param2;
    ME(i).FRAGMENTATION_DATA.param_add1=0;
    ME(i).FRAGMENTATION_DATA.param_add2=0;
end

% Plate (base of propulsion system)
i=i_ME+18;
pos=[-box_l/4+0.005;box_l/4;-0.01];
ME(i).object_ID=i;
ME(i).material_ID=material_ID;
ME(i).GEOMETRY_DATA.shape_ID=1; %plate
ME(i).GEOMETRY_DATA.dimensions=[0.082,0.086,0.005];
ME(i).GEOMETRY_DATA.thick=0.005; %kg
ME(i).GEOMETRY_DATA.mass0 = ME(i).GEOMETRY_DATA.dimensions(1)*ME(i).GEOMETRY_DATA.dimensions(2)*ME(i).GEOMETRY_DATA.dimensions(3)*MATERIAL_LIST(ME(i).material_ID).density; %kg
ME(i).GEOMETRY_DATA.mass=ME(i).GEOMETRY_DATA.mass0; %kg
ME(i).GEOMETRY_DATA.A_M_ratio=0;
ME(i).GEOMETRY_DATA.c_hull=[0, 0, 0];
ME(i).DYNAMICS_INITIAL_DATA.cm_coord0=pos; % ### 1.5
ME(i).DYNAMICS_INITIAL_DATA.quaternions0=[1,0,0,0];
ME(i).DYNAMICS_INITIAL_DATA.vel0=Initial_vel;
ME(i).DYNAMICS_INITIAL_DATA.w0=w;
ME(i).DYNAMICS_DATA.cm_coord=ME(i).DYNAMICS_INITIAL_DATA.cm_coord0;
ME(i).DYNAMICS_DATA.quaternions=ME(i).DYNAMICS_INITIAL_DATA.quaternions0;
ME(i).DYNAMICS_DATA.vel=ME(i).DYNAMICS_INITIAL_DATA.vel0;
ME(i).DYNAMICS_DATA.w=ME(i).DYNAMICS_INITIAL_DATA.w0;
ME(i).DYNAMICS_DATA.virt_momentum=[0,0,0]';
ME(i).FRAGMENTATION_DATA.failure_ID=FAIL_TYPE;
ME(i).FRAGMENTATION_DATA.threshold0=EMR;
ME(i).FRAGMENTATION_DATA.threshold=EMR;
ME(i).FRAGMENTATION_DATA.breakup_flag=1;
ME(i).FRAGMENTATION_DATA.ME_energy_transfer_coef=0.001;
ME(i).FRAGMENTATION_DATA.cMLOSS=0.2;
ME(i).FRAGMENTATION_DATA.cELOSS=0.1;
ME(i).FRAGMENTATION_DATA.c_EXPL=0;
ME(i).FRAGMENTATION_DATA.seeds_distribution_ID = DEFAULT_seeds_distribution_ID;
ME(i).FRAGMENTATION_DATA.seeds_distribution_param1 = DEFAULT_seeds_distribution_param1;
ME(i).FRAGMENTATION_DATA.seeds_distribution_param2 = DEFAULT_seeds_distribution_param2;
ME(i).FRAGMENTATION_DATA.param_add1=0;
ME(i).FRAGMENTATION_DATA.param_add2=0;

% Plate (base of optical payload)
i=i_ME+19;
pos=[box_l/4-0.005;box_l/4;-0.01];
ME(i).object_ID=i;
ME(i).material_ID=material_ID;
ME(i).GEOMETRY_DATA.shape_ID=1; %plate
ME(i).GEOMETRY_DATA.dimensions=[0.094,0.086,0.008];
ME(i).GEOMETRY_DATA.thick=0.008; %kg
ME(i).GEOMETRY_DATA.mass0 = ME(i).GEOMETRY_DATA.dimensions(1)*ME(i).GEOMETRY_DATA.dimensions(2)*ME(i).GEOMETRY_DATA.dimensions(3)*MATERIAL_LIST(ME(i).material_ID).density; %kg
ME(i).GEOMETRY_DATA.mass=ME(i).GEOMETRY_DATA.mass0; %kg
ME(i).GEOMETRY_DATA.A_M_ratio=0;
ME(i).GEOMETRY_DATA.c_hull=[0, 0, 0];
ME(i).DYNAMICS_INITIAL_DATA.cm_coord0=pos; % ### 1.5
ME(i).DYNAMICS_INITIAL_DATA.quaternions0=[1,0,0,0];
ME(i).DYNAMICS_INITIAL_DATA.vel0=Initial_vel;
ME(i).DYNAMICS_INITIAL_DATA.w0=w;
ME(i).DYNAMICS_DATA.cm_coord=ME(i).DYNAMICS_INITIAL_DATA.cm_coord0;
ME(i).DYNAMICS_DATA.quaternions=ME(i).DYNAMICS_INITIAL_DATA.quaternions0;
ME(i).DYNAMICS_DATA.vel=ME(i).DYNAMICS_INITIAL_DATA.vel0;
ME(i).DYNAMICS_DATA.w=ME(i).DYNAMICS_INITIAL_DATA.w0;
ME(i).DYNAMICS_DATA.virt_momentum=[0,0,0]';
ME(i).FRAGMENTATION_DATA.failure_ID=FAIL_TYPE;
ME(i).FRAGMENTATION_DATA.threshold0=EMR;
ME(i).FRAGMENTATION_DATA.threshold=EMR;
ME(i).FRAGMENTATION_DATA.breakup_flag=1;
ME(i).FRAGMENTATION_DATA.ME_energy_transfer_coef=0.001;
ME(i).FRAGMENTATION_DATA.cMLOSS=0.2;
ME(i).FRAGMENTATION_DATA.cELOSS=0.1;
ME(i).FRAGMENTATION_DATA.c_EXPL=0;
ME(i).FRAGMENTATION_DATA.seeds_distribution_ID = DEFAULT_seeds_distribution_ID;
ME(i).FRAGMENTATION_DATA.seeds_distribution_param1 = DEFAULT_seeds_distribution_param1;
ME(i).FRAGMENTATION_DATA.seeds_distribution_param2 = DEFAULT_seeds_distribution_param2;
ME(i).FRAGMENTATION_DATA.param_add1=0;
ME(i).FRAGMENTATION_DATA.param_add2=0;

% Plate (base of data and power module)
i=i_ME+20;
pos=[0;-box_l/4+0.005;-0.015];
ME(i).object_ID=i;
ME(i).material_ID=material_ID;
ME(i).GEOMETRY_DATA.shape_ID=1; %plate
ME(i).GEOMETRY_DATA.dimensions=[0.185,0.092,0.011];
ME(i).GEOMETRY_DATA.thick=0.008; %kg
ME(i).GEOMETRY_DATA.mass0 = ME(i).GEOMETRY_DATA.dimensions(1)*ME(i).GEOMETRY_DATA.dimensions(2)*ME(i).GEOMETRY_DATA.dimensions(3)*MATERIAL_LIST(ME(i).material_ID).density; %kg
ME(i).GEOMETRY_DATA.mass=ME(i).GEOMETRY_DATA.mass0; %kg
ME(i).GEOMETRY_DATA.A_M_ratio=0;
ME(i).GEOMETRY_DATA.c_hull=[0, 0, 0];
ME(i).DYNAMICS_INITIAL_DATA.cm_coord0=pos; % ### 1.5
ME(i).DYNAMICS_INITIAL_DATA.quaternions0=[1,0,0,0];
ME(i).DYNAMICS_INITIAL_DATA.vel0=Initial_vel;
ME(i).DYNAMICS_INITIAL_DATA.w0=w;
ME(i).DYNAMICS_DATA.cm_coord=ME(i).DYNAMICS_INITIAL_DATA.cm_coord0;
ME(i).DYNAMICS_DATA.quaternions=ME(i).DYNAMICS_INITIAL_DATA.quaternions0;
ME(i).DYNAMICS_DATA.vel=ME(i).DYNAMICS_INITIAL_DATA.vel0;
ME(i).DYNAMICS_DATA.w=ME(i).DYNAMICS_INITIAL_DATA.w0;
ME(i).DYNAMICS_DATA.virt_momentum=[0,0,0]';
ME(i).FRAGMENTATION_DATA.failure_ID=FAIL_TYPE;
ME(i).FRAGMENTATION_DATA.threshold0=EMR;
ME(i).FRAGMENTATION_DATA.threshold=EMR;
ME(i).FRAGMENTATION_DATA.breakup_flag=1;
ME(i).FRAGMENTATION_DATA.ME_energy_transfer_coef=0.001;
ME(i).FRAGMENTATION_DATA.cMLOSS=0.2;
ME(i).FRAGMENTATION_DATA.cELOSS=0.1;
ME(i).FRAGMENTATION_DATA.c_EXPL=0;
ME(i).FRAGMENTATION_DATA.seeds_distribution_ID = DEFAULT_seeds_distribution_ID;
ME(i).FRAGMENTATION_DATA.seeds_distribution_param1 = DEFAULT_seeds_distribution_param1;
ME(i).FRAGMENTATION_DATA.seeds_distribution_param2 = DEFAULT_seeds_distribution_param2;
ME(i).FRAGMENTATION_DATA.param_add1=0;
ME(i).FRAGMENTATION_DATA.param_add2=0;

% 2 Boxes on plates (propulsion + data and power module)
for i=i_ME+21:i_ME+22
    ME(i).object_ID=i;
    ME(i).material_ID=material_ID;
    ME(i).GEOMETRY_DATA.shape_ID=1; %box
    if i==i_ME+21 % propulsion
        pos=[-box_l/4+0.005;box_l/4;-0.015-0.066/2];
        ME(i).GEOMETRY_DATA.dimensions=[0.0674,0.0674,0.066];
        ME(i).GEOMETRY_DATA.mass0 = ME(i).GEOMETRY_DATA.dimensions(1)*ME(i).GEOMETRY_DATA.dimensions(2)*ME(i).GEOMETRY_DATA.dimensions(3)*5207; % MATERIAL_LIST(ME(i).material_ID).density; %kg
    elseif i==i_ME+22 % data and power module
        pos=[0;-box_l/4+0.005;-0.025-0.075/2];
        ME(i).GEOMETRY_DATA.dimensions=[0.141,0.076,0.075];
        ME(i).GEOMETRY_DATA.mass0 = ME(i).GEOMETRY_DATA.dimensions(1)*ME(i).GEOMETRY_DATA.dimensions(2)*ME(i).GEOMETRY_DATA.dimensions(3)*1590; % MATERIAL_LIST(ME(i).material_ID).density; %kg
    end
    ME(i).GEOMETRY_DATA.thick=0; %kg
    ME(i).GEOMETRY_DATA.mass=ME(i).GEOMETRY_DATA.mass0; %kg
    ME(i).GEOMETRY_DATA.A_M_ratio=0;
    ME(i).GEOMETRY_DATA.c_hull=[0, 0, 0];
    ME(i).DYNAMICS_INITIAL_DATA.cm_coord0=pos;
    ME(i).DYNAMICS_INITIAL_DATA.quaternions0=[1,0,0,0];
    ME(i).DYNAMICS_INITIAL_DATA.vel0=Initial_vel;
    ME(i).DYNAMICS_INITIAL_DATA.w0=w;
    ME(i).DYNAMICS_DATA.cm_coord=ME(i).DYNAMICS_INITIAL_DATA.cm_coord0;
    ME(i).DYNAMICS_DATA.quaternions=ME(i).DYNAMICS_INITIAL_DATA.quaternions0;
    ME(i).DYNAMICS_DATA.vel=ME(i).DYNAMICS_INITIAL_DATA.vel0;
    ME(i).DYNAMICS_DATA.w=ME(i).DYNAMICS_INITIAL_DATA.w0;
    ME(i).DYNAMICS_DATA.virt_momentum=[0,0,0]';
    ME(i).FRAGMENTATION_DATA.failure_ID=FAIL_TYPE;
    ME(i).FRAGMENTATION_DATA.threshold0=EMR;
    ME(i).FRAGMENTATION_DATA.threshold=EMR;
    ME(i).FRAGMENTATION_DATA.breakup_flag=1;
    ME(i).FRAGMENTATION_DATA.ME_energy_transfer_coef=0.001;
    ME(i).FRAGMENTATION_DATA.cMLOSS=0.2;
    ME(i).FRAGMENTATION_DATA.cELOSS=0.1;
    ME(i).FRAGMENTATION_DATA.c_EXPL=0;
    ME(i).FRAGMENTATION_DATA.seeds_distribution_ID = DEFAULT_seeds_distribution_ID;
    ME(i).FRAGMENTATION_DATA.seeds_distribution_param1 = DEFAULT_seeds_distribution_param1;
    ME(i).FRAGMENTATION_DATA.seeds_distribution_param2 = DEFAULT_seeds_distribution_param2;
    ME(i).FRAGMENTATION_DATA.param_add1=0;
    ME(i).FRAGMENTATION_DATA.param_add2=0;
end

% 1 Cylinder on plate (optical payload)
i=i_ME+23;
pos=[box_l/4-0.005;box_l/4;-0.015-0.044/2];
alpha2=-pi/2;
rot_axis2 = [0 1 0];
quaternions=[cos(alpha2/2), sin(alpha2/2)*rot_axis2(1), sin(alpha2/2)*rot_axis2(2), sin(alpha2/2)*rot_axis2(3)];
ME(i).object_ID=i;
ME(i).material_ID=material_ID;                   % 1=Aluminium, only one available
ME(i).GEOMETRY_DATA.shape_ID=4; % 1=plate, 2=sphere, 4=cylinder
ME(i).GEOMETRY_DATA.dimensions=[0.044/2,0,0.097];
ME(i).GEOMETRY_DATA.thick=0;
ME(i).GEOMETRY_DATA.mass0 = pi*ME(i).GEOMETRY_DATA.dimensions(1)^2*ME(i).GEOMETRY_DATA.dimensions(3)*2030; % MATERIAL_LIST(ME(i).material_ID).density; %kg
ME(i).GEOMETRY_DATA.mass=ME(i).GEOMETRY_DATA.mass0; %kg
ME(i).DYNAMICS_INITIAL_DATA.cm_coord0=pos;
ME(i).DYNAMICS_INITIAL_DATA.quaternions0=quaternions;
ME(i).DYNAMICS_INITIAL_DATA.vel0=Initial_vel;
ME(i).DYNAMICS_INITIAL_DATA.w0=w;
ME(i).DYNAMICS_DATA.cm_coord=ME(i).DYNAMICS_INITIAL_DATA.cm_coord0;
ME(i).DYNAMICS_DATA.quaternions=ME(i).DYNAMICS_INITIAL_DATA.quaternions0;
ME(i).DYNAMICS_DATA.vel=ME(i).DYNAMICS_INITIAL_DATA.vel0;
ME(i).DYNAMICS_DATA.w=ME(i).DYNAMICS_INITIAL_DATA.w0;
ME(i).DYNAMICS_DATA.virt_momentum=[0,0,0]';
ME(i).GEOMETRY_DATA.A_M_ratio=0;
ME(i).GEOMETRY_DATA.c_hull=[0, 0, 0];
ME(i).FRAGMENTATION_DATA.threshold0=EMR;
ME(i).FRAGMENTATION_DATA.threshold=EMR;
ME(i).FRAGMENTATION_DATA.failure_ID=FAIL_TYPE;
ME(i).FRAGMENTATION_DATA.breakup_flag=1;
ME(i).FRAGMENTATION_DATA.ME_energy_transfer_coef=0.001;
ME(i).FRAGMENTATION_DATA.cMLOSS=0.2;
ME(i).FRAGMENTATION_DATA.cELOSS=0.1;
ME(i).FRAGMENTATION_DATA.c_EXPL=0;
ME(i).FRAGMENTATION_DATA.seeds_distribution_ID = DEFAULT_seeds_distribution_ID;
ME(i).FRAGMENTATION_DATA.seeds_distribution_param1 = DEFAULT_seeds_distribution_param1;
ME(i).FRAGMENTATION_DATA.seeds_distribution_param2 = DEFAULT_seeds_distribution_param2;
ME(i).FRAGMENTATION_DATA.param_add1=0;
ME(i).FRAGMENTATION_DATA.param_add2=0;

% Antenna 
i=i_ME+24;
pos=ME(i_ME+21).DYNAMICS_INITIAL_DATA.cm_coord0-1/2*[1;-1;1].*ME(i_ME+21).GEOMETRY_DATA.dimensions' + [0.002;0.0045; -0.01870];
ME(i).object_ID=i;
ME(i).material_ID=material_ID;
ME(i).GEOMETRY_DATA.shape_ID=1; %plate
ME(i).GEOMETRY_DATA.dimensions=[0.042,0.042,0.0045];
ME(i).GEOMETRY_DATA.thick=0; %kg
ME(i).GEOMETRY_DATA.mass0 = ME(i).GEOMETRY_DATA.dimensions(1)*ME(i).GEOMETRY_DATA.dimensions(2)*ME(i).GEOMETRY_DATA.dimensions(3)*5350; % MATERIAL_LIST(ME(i).material_ID).density; %kg
ME(i).GEOMETRY_DATA.mass=ME(i).GEOMETRY_DATA.mass0; %kg
ME(i).GEOMETRY_DATA.A_M_ratio=0;
ME(i).GEOMETRY_DATA.c_hull=[0, 0, 0];
ME(i).DYNAMICS_INITIAL_DATA.cm_coord0=pos; % ### 1.5
ME(i).DYNAMICS_INITIAL_DATA.quaternions0=[1,0,0,0];
ME(i).DYNAMICS_INITIAL_DATA.vel0=Initial_vel;
ME(i).DYNAMICS_INITIAL_DATA.w0=w;
ME(i).DYNAMICS_DATA.cm_coord=ME(i).DYNAMICS_INITIAL_DATA.cm_coord0;
ME(i).DYNAMICS_DATA.quaternions=ME(i).DYNAMICS_INITIAL_DATA.quaternions0;
ME(i).DYNAMICS_DATA.vel=ME(i).DYNAMICS_INITIAL_DATA.vel0;
ME(i).DYNAMICS_DATA.w=ME(i).DYNAMICS_INITIAL_DATA.w0;
ME(i).DYNAMICS_DATA.virt_momentum=[0,0,0]';
ME(i).FRAGMENTATION_DATA.failure_ID=FAIL_TYPE;
ME(i).FRAGMENTATION_DATA.threshold0=EMR;
ME(i).FRAGMENTATION_DATA.threshold=EMR;
ME(i).FRAGMENTATION_DATA.breakup_flag=1;
ME(i).FRAGMENTATION_DATA.ME_energy_transfer_coef=0.001;
ME(i).FRAGMENTATION_DATA.cMLOSS=0.2;
ME(i).FRAGMENTATION_DATA.cELOSS=0.1;
ME(i).FRAGMENTATION_DATA.c_EXPL=0;
ME(i).FRAGMENTATION_DATA.seeds_distribution_ID = DEFAULT_seeds_distribution_ID;
ME(i).FRAGMENTATION_DATA.seeds_distribution_param1 = DEFAULT_seeds_distribution_param1;
ME(i).FRAGMENTATION_DATA.seeds_distribution_param2 = DEFAULT_seeds_distribution_param2;
ME(i).FRAGMENTATION_DATA.param_add1=0;
ME(i).FRAGMENTATION_DATA.param_add2=0;

end