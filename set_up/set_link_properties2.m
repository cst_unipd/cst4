%> @file set_link_properties.m
%> @brief associates properties to each link
%> @author Dr. M. Duzzi (matteo.duzzi@phd.unipd.it) & Dr. G. Sarego (giulia.sarego@unipd.it)
%> Copyright (c) 2018 CISAS - UNIVERSITY OF PADOVA
%>======================================================================
function set_link_properties2()

% function used to associate properties to each link
%
% Syntax: set_link_properties2() 
%
% Inputs: none
%
% Outputs: none (link_data structure is updated)
%    
% Other m-files required: link_type, link_type_repeated
%                         
% Subfunctions: size, disp, input 
% 
% MAT-files required: none
%
% See also: link_type, link_type_repeated
%
% Authors:   
%            Matteo Duzzi, PhD
%            Center of Studies and Activities for Space "Giuseppe Colombo" (CISAS)
%            University of Padova
%
%            Giulia Sarego, PhD
%            Department of Industrial Engineering (DII)
%            University of Padova
%
% Email address: matteo.duzzi@phd.unipd.it, giulia.sarego@unipd.it
% Date: 2018/07/23
% Revision: 1.0
% Copyright: 2018 CISAS - UNIVERSITY OF PADOVA
%
% HISTORY
% 2018/07/23 : first version by MD, GS

%#codegen

global link_data0
global link_data
global ME

% number of links
n_links0=size(link_data0,2);
n_links=size(link_data,2);

% loop to associate properties to each link
for idx=n_links0+1:1:n_links % i is the link index, not the ID
    i_1=find([ME.object_ID]==link_data(idx).ME_connect(1)); % i_1 is the ME1 index
    i_2=find([ME.object_ID]==link_data(idx).ME_connect(2)); % i_1 is the ME2 index
    disp(' ')
    disp(['Link connecting ME ',num2str(link_data(idx).ME_connect(1)),...
        ' and ME ',num2str(link_data(idx).ME_connect(2)),'.'])
    disp(' ')
    if idx==1
        link_type(idx,i_1,i_2); 
    else
        ReapeatedLink = input('Do you want this link to have the same characteristics of the previous one? y/n ','s');
        if ReapeatedLink~='y'
            ReapeatedLink = 'n';
        end
        switch ReapeatedLink
            case 'y'
                link_type_repeated(idx,i_1,i_2);
            case 'n'
                link_type(idx,i_1,i_2);
        end
    end
end

end