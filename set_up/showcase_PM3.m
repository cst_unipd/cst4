%USE this to test the set up, it produces a picture at the end
% clear all
% close all

global ME;
global FRAGMENTS;
global BUBBLE;
% global sim_title_dir;
global sim_title;
% global caso

ME=population_empty_structure();
FRAGMENTS=population_empty_structure();
BUBBLE=population_empty_structure();

COLLISION_DATA_tracking=struct('target',{},'target_shape',{},'impactor',{},'impactor_shape',{},'point',{});
material_list();

switch caso
    case 1 
        sim_title='Normal_Impact_on_Simple_Plate';
        [ME,FRAGMENTS,BUBBLE] = generate_ME_target_simple_plate(1,0,0,ME,FRAGMENTS,BUBBLE);
        [ME,FRAGMENTS,BUBBLE] = generate_ME_Al_sphere_4mm(2,0,0,ME,FRAGMENTS,BUBBLE);
    case 2 
        sim_title='Sphere_vs_Whipple_Shield_basic';
        [ME,FRAGMENTS,BUBBLE] = generate_ME_target_whipple_shield(1,0,0,ME,FRAGMENTS,BUBBLE);
        [ME,FRAGMENTS,BUBBLE] = generate_ME_Al_sphere_whipple(3,0,0,ME,FRAGMENTS,BUBBLE);
    case 3 
        sim_title='Sphere_vs_Whipple_Shield_bigger';
        [ME,FRAGMENTS,BUBBLE] = generate_ME_target_whipple_shield(1,0,0,ME,FRAGMENTS,BUBBLE);
        [ME,FRAGMENTS,BUBBLE] = generate_ME_Al_sphere_whipple2(3,0,0,ME,FRAGMENTS,BUBBLE);
    case 4 
        sim_title='Sphere_vs_Whipple_Shield_faster';
        [ME,FRAGMENTS,BUBBLE] = generate_ME_target_whipple_shield(1,0,0,ME,FRAGMENTS,BUBBLE);
        [ME,FRAGMENTS,BUBBLE] = generate_ME_Al_sphere_whipple3(3,0,0,ME,FRAGMENTS,BUBBLE);
    case 5 
        sim_title='Sphere_vs_Hollow_Cylinder';
        [ME,FRAGMENTS,BUBBLE] = generate_ME_Hollow_cylinder(1,0,0,ME,FRAGMENTS,BUBBLE);
        [ME,FRAGMENTS,BUBBLE] = generate_ME_Al_sphere_cylinder(2,0,0,ME,FRAGMENTS,BUBBLE);
    case 6 
        sim_title='Sphere_vs_Explosive_Hollow_cylinder';
        [ME,FRAGMENTS,BUBBLE] = generate_ME_Hollow_Explosive_Cylinder(1,0,0,ME,FRAGMENTS,BUBBLE);
        [ME,FRAGMENTS,BUBBLE] = generate_ME_Al_sphere_cylinder(2,0,0,ME,FRAGMENTS,BUBBLE);
    case 7 
        sim_title='Sphere_vs_linked_structure';
        [ME,FRAGMENTS,BUBBLE] = generate_ME_Four_Linked_Plates(1,0,0,ME,FRAGMENTS,BUBBLE);
        [ME,FRAGMENTS,BUBBLE] = link_generate_4linked_structure2(0,0,0,ME,FRAGMENTS,BUBBLE);
        [ME,FRAGMENTS,BUBBLE] = generate_ME_Al_sphere_structure(5,0,0,ME,FRAGMENTS,BUBBLE);
    case 8 
        sim_title='Sphere_vs_Loft';
        [ME,FRAGMENTS,BUBBLE] = generate_ME_LOFT_2(1,0,0,ME,FRAGMENTS,BUBBLE);
        [ME,FRAGMENTS,BUBBLE] = link_generate_LOFT_2(0,0,0,ME,FRAGMENTS,BUBBLE);
        [ME,FRAGMENTS,BUBBLE] = generate_ME_Al_sphere_LOFT(length(ME)+1,0,0,ME,FRAGMENTS,BUBBLE);
    case 9
        sim_title='Sphere_vs_Cubesat';
        [ME,FRAGMENTS,BUBBLE] = generate_ME_cubesat(1,0,0,ME,FRAGMENTS,BUBBLE);
        [ME,FRAGMENTS,BUBBLE] = link_generate_cubesat(0,0,0,ME,FRAGMENTS,BUBBLE);
        [ME,FRAGMENTS,BUBBLE] = generate_ME_sphere_imp_cubesat(length(ME)+1,0,0,ME,FRAGMENTS,BUBBLE);
end


sim_title_dir=[sim_title,'_',datestr(now,30)];
sim_title_dir=fullfile('Results',sim_title_dir);
mkdir(sim_title_dir);
mkdir(fullfile(sim_title_dir,'figures'));
mkdir(fullfile(sim_title_dir,'data'));
global ME_SR;
ME_SR=ME;
draw3D();
pause(0.01)
%close all
