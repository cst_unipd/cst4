function  [ME,FRAGMENTS,BUBBLE]=link_generate_ReDSHIFT_cmat(ii,jj,kk,ME,FRAGMENTS,BUBBLE)
%GENERATE_LOFT_LINK; in this file only address which element are connected and
% with which material
global link_data;
global link_data0;
%{
%-------------------------------------------------------------------------
%LINK INIATIALIZATION,

f_id = 'id';
f_con = 'ME_connect';   % The pair of ME connected by the link
f_mat = 'material_ID';  % 1=Al, 2=steel,
f_geom= 'geometry';
f_type= 'type_ID';      %1=beam, 2=surface...
f_fail= 'failure_ID';  % Type of faiulre (1=max elongation, 2= max strain...)
f_k   = 'k_mat';       % 6x6 matrix for stiffness response
f_c   = 'c_mat';       % 6x6 matrix for viscous response
f_rest = 'rest';     % Equilibrium configuration, when no forces are exchanged
f_state = 'state';   % 1= intact, 0=broken

% MEs ID
% ID = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16... % 16 Lateral plates
%       17,18,19,20,... % 4 Upper hats
%       21,22,23,24,... % 4 Lower hats
%       25,26,27,28,... % 4 Central plates - equipment tray
%       29,30,31,32,33,... % 5 Boxes on the central plate (reaction wheels,XTU,IMU)
%       34,35,36,37,... % 4 Plates (inside) bracket
%       38,39,... % 2 Cylinders on central plate (star tracker)
%       40,... % Plate (base of propulsion system)
%       41,... % Plate (base of optical payload)
%       42,... % Plate (base of data and power module)
%       43,44,... % 2 Boxes on plates (propulsion + data and power module)
%       45,... % 1 Cylinder on plate (optical payload)
%       46}; % Antenna


con = {[1,6],[2,7],[3,8],[4,5],[1,5],[2,6],[3,7],[4,8],...%
       [9,14],[10,15],[11,16],[12,13],[9,13],[10,14],[11,15],[12,16],...%
       [17,18],[18,19],[19,20],[17,20],...%
       [1,17],[6,17],[2,18],[7,18],[3,19],[8,19],[4,20],[5,20],...%
       [21,22],[22,23],[23,24],[21,24],...%
       [9,21],[14,21],[10,22],[15,22],[11,23],[16,23],[12,24],[13,24],...%
       [1,9],[2,10],[3,11],[4,12],[5,13],[6,14],[7,15],[8,16],...%
       [25,26],[26,27],[27,28],[25,28],...%
       [1,25],[6,25],[9,25],[14,25],[2,26],[7,26],[10,26],[15,26],...%
       [3,27],[8,27],[11,27],[16,27],[4,28],[5,28],[12,28],[13,28],...%
       [27,29],[27,30],[27,31],[28,32],[25,33],...%
       [25,34],[26,34],[26,35],[27,35],[27,36],[28,36],[28,37],[25,37],...%
       [25,39],[26,39],[25,38],[28,38],...%
       [26,40],[25,41],[27,42],[28,42],...%
       [40,43],[42,44],[41,45],[43,46]}; %connection with the MEs


for i=1:length(con)
    
    ID{i} = i;                          % Identification number of the link
    mat{i} = 1;                         % Material type for each link: 1 = Al, 2 = steel,
    geom{i} = 1;                        % Geometrical type of each link: 1 = dot, 2 = line, 3 = surface
    type{i} = 1;                        % Element type of each link: 1 = bolted, 2 = welded,
    val_fail{i} = 1;                    % Type of faiulre of the link: 1 = max elongation, 2 = max strain,
    val_k{i} = eye(6);                  % Inizialization of the matrix for stiffness response
    val_c{i} = eye(6);                  % Inizialization of the matrix for viscous response
    val_rest{i} = ones(1,6);            % Inizialization of the equilibrium configuration
    val_state{i} = 1;                   % Inizialization of the state of the link: 1 = intact, 0 = broken
    
end
link_data = struct(f_id,ID,f_con,con,f_mat,mat,f_geom,geom,f_type,type,f_fail,val_fail,f_state,val_state,f_k,val_k,f_c,val_c,f_rest,val_rest);

%Completes the link array with the geometries
set_link_properties();
save('Link_ReDSHIFT_weak_2.mat','link_data')
%}
load('Link_ReDSHIFT_weak.mat','link_data')
link_data0=link_data;
end

