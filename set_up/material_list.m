%> @file material_list.m
%> @brief provides material proprieties
%> @author Dr. M. Duzzi (matteo.duzzi@phd.unipd.it) & Dr. G. Sarego (giulia.sarego@unipd.it)
%> Copyright (c) 2018 CISAS - UNIVERSITY OF PADOVA
%>======================================================================
function material_list()
% Material_list provides material proprieties

global MATERIAL_LIST;

field_mat_id='mat_id';
field_name='name';
field_den='density';
field_E='young';
field_m_strain='max_strain';
field_ni='ni';
field_HB='brinell';
field_m_stress='max_stress';
field_yield_stress='yield_stress';
field_G='shear_modulus';
field_m_shear_stress='max_shear';

mat_id={1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12};
Names={'Alluminum', 'Steel', 'Titanium Alloy Ti-6Al-4V','Metlbond 329','Araldite AV138','Araldite 2015','SikaForce 7888','GFRP','CFRP','Nylon_6','ULTEM9085','AlSi10Mg'}; %4/5 from https://doi.org/10.1016/j.engfracmech.2015.02.010
rho={2800, 7800, 4430, 1600, 1600, 1700, 1400, 1800, 1563, 1140, 1340, 2680}; %[kg/m^3]
E={69e9, 200e9, 114e9, 6.86e9, 4.2e9, 1.85e9, 1.89e9, 15.14e9, 34.95e9, NaN, 2.27e9, 68e9}; %[Pa] % data from pdf
max_e={0.17, 0.08, 0.1, 0.015, 0.008, 0.0477, 0.43, 0.017, 0.00155, NaN, 0.022, 0.055};% maximum elongation (not in %)
ni={0.33, 0.28, 0.33, 0, 0.35, 0.33, 0.33, 0.258, 0.446, NaN, 0.258, 0.338};
max_stress={70e6, 1000e6, 900e6, 61.3e6, 43e6, 21e6, 28e6, 431.8e6, 353.23e6, NaN, 71.6e6, 270e6}; %[Pa]
yield_stress={0, 900e6, 0, 0, 0, 0, 0, 0, 0, NaN, 0, 170e6}; %[Pa]
G={69e9/(2*(1+0.33)), 200e9/(2*(1+0.28)), 44e9, 6.86e9/(2*(1+0)), 1.56e9, 0.56e9, 0.71e9, 1.79e9, 16.59e9, NaN, 2.27e9/(2*(1+0.258)), 68e9/(2*(1+0.338))};  % attenta che non sia zero!!!
m_shear_stress={0, 215e6, 0, 11e6, 18.4e6, 18e6, 20e6, 80e6, 122.33e6, NaN, 122.33e6, 0}; 

HB={95, 150, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
MATERIAL_LIST=struct(field_mat_id,mat_id,field_name,Names,field_den,rho,...
    field_E,E,field_ni,ni,field_m_strain,max_e,field_HB,HB,...
    field_m_stress,max_stress,field_yield_stress,yield_stress,field_G,G,...
    field_m_shear_stress,m_shear_stress);