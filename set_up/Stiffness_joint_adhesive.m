%> @file Stiffness_joint_adhesive.m
%> @brief creates the stiffness and viscousness matrices for the ADHESIVE joint
%> @author Dr. M. Duzzi (matteo.duzzi@phd.unipd.it) & Dr. G. Sarego (giulia.sarego@unipd.it)
%> Copyright (c) 2018 CISAS - UNIVERSITY OF PADOVA
%>======================================================================
function Stiffness_joint_adhesive(link_idx,X)

% function which creates the stiffness and viscousness matrices in GLOBAL
% COORDINATES for the ADHESIVE joint and update them in the link_data
% structure
%
% Syntax: Stiffness_joint_adhesive(link_idx,X)
%
% Inputs 
%           link_idx:   index of the considered link
%           X:          relative cuttent position between the two mass
%                       centers of the MEs connected by the link
%
% Outputs: none (link_data structure is updated)
%    
% Other m-files required: rquat
%                         
% Subfunctions: dot, norm, acos, eye, sign, abs, cross, find
% 
% MAT-files required: none
%
% See also:
%
% Authors:   
%            Matteo Duzzi, PhD
%            Center of Studies and Activities for Space "Giuseppe Colombo" (CISAS)
%            University of Padova
%
%            Giulia Sarego, PhD
%            Department of Industrial Engineering (DII)
%            University of Padova
%
% Email address: matteo.duzzi@phd.unipd.it, giulia.sarego@unipd.it
% Date: 2018/08/02
% Revision: 1.0
% Copyright: 2018 CISAS - UNIVERSITY OF PADOVA
%
% HISTORY
% 2018/08/02 : first version by MD, GS

%#codegen

global ME ME_SR
global link_data
global MATERIAL_LIST

if isempty(ME_SR)
    ME_SR = ME;
end

%assignment of the link characteristics
mat_idx_joint = find([MATERIAL_LIST.mat_id]==link_data(link_idx).material_ID); % index of link material in MATERIAL_LIST

l_adh_x = link_data(link_idx).geometry{1}(1);
l_adh_y = link_data(link_idx).geometry{1}(2);
t_adh = link_data(link_idx).geometry{1}(3);
t1 = link_data(link_idx).geometry{1}(4);
t2 = link_data(link_idx).geometry{1}(5);
X_dir = link_data(link_idx).geometry{2}(:,1);
Y_dir = link_data(link_idx).geometry{2}(:,2);
G = MATERIAL_LIST(mat_idx_joint).shear_modulus;

rest=link_data(link_idx).rest(1:3); %"Rest" (=equilibrium configuration) length of link in GLOBAL coordinates

%rotation of the joint in GLOBAL coordinates and error checks
cos_alpha = dot(X(1:3),rest)/(norm(X(1:3))*norm(rest));

if cos_alpha > 1 %avoid angle errors
    alpha = acos(1);
elseif cos_alpha < -1 %avoid angle errors
    alpha = acos(-1);
elseif isnan(cos_alpha) %avoid angle errors
    alpha = 0;
else
    alpha = acos(cos_alpha); %actual rotation angle
end

if alpha == 0 %parallel
    R_link2glob=eye(3); % no rotation
else
    vect_alpha = cross(X(1:3),rest);
    if norm(vect_alpha)==0
        R_link2glob=eye(3); % no rotation
    else
        vect_alpha=vect_alpha/norm(vect_alpha);
        qq=[cos(alpha/2),vect_alpha(1)*sin(alpha/2),vect_alpha(2)*sin(alpha/2),vect_alpha(3)*sin(alpha/2)];
        R_link2glob = rquat(qq); %Compute the rotation matrix from the quaternion (Euler's parameters)
    end
end

%direction of the joint in GLOBAL coordinates
X_dir = X_dir - (dot(X_dir,Y_dir)*norm(X_dir))*Y_dir;
X_dir = X_dir/norm(X_dir);
Z_dir=cross(X_dir,Y_dir)/(norm(X_dir)*norm(Y_dir));
ROT_joint2glob =R_link2glob*[X_dir,Y_dir,Z_dir];

gamma_glob = (X(1:3)-rest);
if abs(gamma_glob(1))<eps
    gamma_glob(1)=0;
end
if abs(gamma_glob(2))<eps
    gamma_glob(2)=0;
end
if abs(gamma_glob(3))<eps
    gamma_glob(3)=0;
end

gamma_loc1 = ROT_joint2glob'*(gamma_glob./t_adh); %dispalcement in LOCAL coordinates


link_data(link_idx).geometry(3) = {ROT_joint2glob};

%indexes of the MEs
i_1=find([ME.object_ID]==link_data(link_idx).ME_connect(1)); % i_1 is the ME1 index
i_2=find([ME.object_ID]==link_data(link_idx).ME_connect(2)); % i_2 is the ME2 index
%indexes of the ME's Materials
mat_1=find([MATERIAL_LIST.mat_id]==ME(i_1).material_ID); % mat_1 is the ME1 material index
mat_2=find([MATERIAL_LIST.mat_id]==ME(i_2).material_ID); % mat_2 is the ME2 material index

%assignment of the material characteristics (for the i_th link) and calculations of the
%parameters for the shear stresses
N1x = MATERIAL_LIST(mat_1).shear_modulus*gamma_loc1(1)*l_adh_y;
N1y = MATERIAL_LIST(mat_1).shear_modulus*gamma_loc1(2)*l_adh_x;
ni1 = MATERIAL_LIST(mat_1).ni;
E1 = MATERIAL_LIST(mat_1).young;
%parameters for the shear stress calculation
lambda1 = ni1^2;
if N1x == 0
    tau1x = 0;
else
    alpha1x = 1/(1+2*sqrt(2)*tanh(l_adh_x/t1*sqrt(3*(1-lambda1)*abs(N1x)/(2*E1*t1))));
    W1x = sqrt((2*(1-lambda1)*G)/(E1*t1*t_adh));
    K1x = 1/4*(W1x*l_adh_x*(1+3*alpha1x)*coth(W1x*l_adh_x)+3*(1-alpha1x));
    tau1x = K1x*N1x/l_adh_y; %shear stress along the local x direction (i_th ME) of the link
end

if N1y == 0
    tau1y = 0;
else
    alpha1y = 1/(1+2*sqrt(2)*tanh(l_adh_y/t1*sqrt(3*(1-lambda1)*abs(N1y)/(2*E1*t1))));
    W1y = sqrt((2*(1-lambda1)*G)/(E1*t1*t_adh));
    K1y = 1/4*(W1y*l_adh_y*(1+3*alpha1y)*coth(W1y*l_adh_y)+3*(1-alpha1y));
    tau1y = K1y*N1y/l_adh_x; %shear stress along the local y direction (i_th ME) of the link
end

%assignment of the material characteristics (for the j_th link) and calculations of the
%parameters for the shear stresses
N2x = MATERIAL_LIST(mat_2).shear_modulus*gamma_loc1(1)*l_adh_y;
N2y = MATERIAL_LIST(mat_2).shear_modulus*gamma_loc1(2)*l_adh_x;
ni2 = MATERIAL_LIST(mat_2).ni;
E2 = MATERIAL_LIST(mat_2).young;
%parameters for the shear stress calculation
lambda2 = ni2^2;
if N2x == 0
    tau2x = 0;
else
    alpha2x = 1/(1+2*sqrt(2)*tanh(l_adh_x/t2*sqrt(3*(1-lambda2)*abs(N2x)/(2*E2*t2))));
    W2x = sqrt((2*(1-lambda2)*G)/(E2*t2*t_adh));
    K2x = 1/4*(W2x*l_adh_x*(1+3*alpha2x)*coth(W2x*l_adh_x)+3*(1-alpha2x));
    tau2x = K2x*N2x/l_adh_y; %shear stress along the local x direction (j_th) of the link
end

if N2y == 0
    tau2y = 0;
else
    alpha2y = 1/(1+2*sqrt(2)*tanh(l_adh_y/t2*sqrt(3*(1-lambda2)*abs(N2y)/(2*E2*t2))));
    W2y = sqrt((2*(1-lambda2)*G)/(E2*t2*t_adh));
    K2y = 1/4*(W2y*l_adh_y*(1+3*alpha2y)*coth(W2y*l_adh_y)+3*(1-alpha2y));
    tau2y = K2y*N2y/l_adh_x; %shear stress along the local y direction (j_th) of the link
end

%selection of the maximum shear stress
ttx = [tau1x,tau2x];
[tau_max_x,t_x] = max(abs(ttx));
tty = [tau1y,tau2y];
[tau_max_y,t_y] = max(abs(tty));

%creation of the shear stress vector
tau_loc = [sign(ttx(t_x))*tau_max_x; sign(tty(t_y))*tau_max_y; 0];
Tau = ROT_joint2glob*tau_loc;

%creation of the glued stiffness matrix
K_matrix1=diag(Tau)*(l_adh_x*l_adh_y)./((X(1:3)-rest)); % in GLOBAL coordinates
K_matrix1(~isfinite(K_matrix1))=0;
K_matrix2=diag(Tau)*(l_adh_x*l_adh_y)./((X(1:3)-rest)); % in GLOBAL coordinates
K_matrix2(~isfinite(K_matrix2))=0;

m_bar=(ME_SR(i_1).GEOMETRY_DATA.mass+ME_SR(i_2).GEOMETRY_DATA.mass)/(ME_SR(i_1).GEOMETRY_DATA.mass*ME_SR(i_2).GEOMETRY_DATA.mass);

K_matrix1(abs(K_matrix1)<eps)=0;
K_matrix2(abs(K_matrix2)<eps)=0;

csi=0.01;
C_matrix1=2*csi*sqrt(abs(K_matrix1)).*sqrt(m_bar).*sign(K_matrix1);  % in GLOBAL coordinates
C_matrix1(~isfinite(C_matrix1))=0;
C_matrix2=2*csi*sqrt(abs(K_matrix2)).*sqrt(m_bar).*sign(K_matrix2); % in GLOBAL coordinates
C_matrix2(~isfinite(C_matrix2))=0;

K_matrix6=[K_matrix1, zeros(3); K_matrix2, zeros(3)];
C_matrix6=[C_matrix1, zeros(3); C_matrix2, zeros(3)];

K_matrix_local = (ROT_joint2glob')*K_matrix1;

link_data(link_idx).geometry(4) = {K_matrix_local}; % local matrix

link_data(link_idx).k_mat = K_matrix6; % GLOBAL matrix
link_data(link_idx).c_mat = C_matrix6; % GLOBAL matrix

end