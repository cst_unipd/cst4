function  [ME,FRAGMENTS,BUBBLE]=link_generate_Hanada(ii,jj,kk,ME,FRAGMENTS,BUBBLE)
% link_generate_user: links connecting two MEs have to be set here

global link_data;
global link_data0;

%% LINK INIATIALIZATION
% 
% con = {[1 6],[1 7],[1 8],[1 9],[1 10],[1 11],[1 12],[1 13],[2 6],...
%     [2 7],[2 8],[2 9],[2 10],[2 11],[2 12],[2 13],[2 14],[2 15],...
%     [2 16],[2 17],[3 6],[3 7],[3 8],[3 9],[3 14],[3 15],[3 16],...
%     [3 17],[3 18],[3 19],[3 20],[3 21],[4 6],[4 7],[4 8],[4 9],...
%     [4 18],[4 19],[4 20],[4 21],[4 22],[4 23],[4 24],[4 25],[5 6],...
%     [5 7],[5 8],[5 9],[5 22],[5 23],[5 24],[5 25],[6 7],[6 9],...
%     [7 8],[8 9],[8 26],[2 27],[3 28],[4 29]};        % Pairs of MEs connected by each link
% 
% for i=1:length(con)
%     
%     ID{i} = i;                          % Identification number of the link 
%     mat{i} = 1;                         % Material type for each link: 1 = Al, 2 = steel,
%     geom{i} = 1;                        % Geometrical type of each link: 1 = dot, 2 = line, 3 = surface
%     type{i} = 1;                        % Element type of each link: 1 = bolted, 2 = welded,
%     val_fail{i} = 1;                    % Type of faiulre of the link: 1 = max elongation, 2 = max strain,
%     val_k{i} = eye(6);                  % Inizialization of the matrix for stiffness response
%     val_c{i} = eye(6);                  % Inizialization of the matrix for viscous response
%     val_rest{i} = ones(1,6);            % Inizialization of the equilibrium configuration
%     val_state{i} = 1;                   % Inizialization of the state of the link: 1 = intact, 0 = broken
%     
% end
% 
% % Field Names
% 
% f_id    = 'id';
% f_con   = 'ME_connect';   % Field of the pair of ME connected by the link
% f_mat   = 'material_ID';  % 1=Al, 2=steel, 
% f_geom  = 'geometry';
% f_type  = 'type_ID';      %1 = beam, 2 = surface...
% f_fail  = 'failure_ID';  % Type of faiulre (1=max elongation, 2= max strain...)
% f_k     = 'k_mat';       % 6x6 matrix for stiffness response
% f_c     = 'c_mat';       % 6x6 matrix for viscous response 
% f_rest  = 'rest';     % Equilibrium configuration, when no forces are exchanged
% f_state = 'state';   % 1= intact, 0=broken
% 
% link_data = struct(f_id,ID,f_con,con,f_mat,mat,f_geom,geom,f_type,type,f_fail,val_fail,f_state,val_state,f_k,val_k,f_c,val_c,f_rest,val_rest);

%Completes the link array with the geometries
% set_link_properties();
% save('link_Hanada.mat','link_data')
load('link_Hanada.mat','link_data')
link_data0=link_data;
end

