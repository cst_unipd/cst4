%> @file  Sphere_Vs_Whipple_Shield.m
%> @brief generate a Whipple Shield and an impacting spherical projectile
%> @author dr. L. Olivieri (lorenzo.olivieri@unipd.it)
%> Copyright (c) 2018 CISAS - UNIVERSITY OF PADOVA
%======================================================================
global ME;
global FRAGMENTS;
global BUBBLE;
global sim_title_dir;
global sim_title;

%% Simulation Name
sim_title = ['WhippleShield_' num2str(01)];

%% Whipple Shield parameters
bumper_dimensions = [0.2,0.2,0.0012]; %[x,y,thickness] [m]
wall_dimensions   = [0.2,0.2,0.0012]; %[x,y,thickness] [m]
standoff = 0.1;            %[m]
bumper_position=[0;0;0]; %[m]
wall_position=bumper_position+[0;0;standoff]; %[m]
projectile_position = [0,0,-0.01]; % [m]

projectile_diameter = 1.5e-3;      % [m]
projectile_velocity = 1e3*[0 0 5]; % [m/s]

%% Create structures and populations
ME=population_empty_structure();
FRAGMENTS=population_empty_structure();
BUBBLE=population_empty_structure();
COLLISION_DATA_tracking=struct('target',{},'target_shape',{},'impactor',{},'impactor_shape',{},'point',{});

[ME,FRAGMENTS,BUBBLE] = generate_ME_simple_plate_WhippleShield(1,0,0,ME,FRAGMENTS,BUBBLE,bumper_dimensions,bumper_position);
[ME,FRAGMENTS,BUBBLE] = generate_ME_simple_plate_WhippleShield(2,0,0,ME,FRAGMENTS,BUBBLE,wall_dimensions,wall_position);
[ME,FRAGMENTS,BUBBLE] = generate_ME_sphere_imp_WhippleShield(3,0,0,ME,FRAGMENTS,BUBBLE,projectile_diameter,projectile_position,projectile_velocity);

%% Create simulation parameters
sim_title_dir=[sim_title,'_',datestr(now,30)];
sim_title_dir=fullfile('Results',sim_title_dir);
mkdir(sim_title_dir);
mkdir(fullfile(sim_title_dir,'data'));
mkdir(fullfile(sim_title_dir,'figures'));