%> @file  parameters_setup.m
%> @brief Initialization of CST general parameters
%> @author Lorenzo Olivieri (lorenzo.olivieri@unipd.it)
%======================================================================
function [total_failure_coeff, F_L_MIN, a_Ni, k_Ni, BUBBLE_radius_th, BUBBLE_k_bubble, FRAGMENT_radius_th, n_seeds_max, Vc_frag_max, param1] = Parameters_Setup()

total_failure_coeff= 0.7;           % IN input_data_check    

%% F_L_MIN
F_L_MIN= 1E-3;                      % IN fragmentation_mode_case3

%% BUBBLE
BUBBLE_radius_th=1.e-3; % diam frag x BUBBLE       % IN propagation_thr_struct
BUBBLE_k_bubble=0.002; % energy damage coeff          % IN bubble_damage

%% FRAGMENTS MIN SIZE
FRAGMENT_radius_th=5.e-4;                            % IN propagation_thr_struct  && SeedsDistrib_GaussianCartesian_Solid

%% RANDOM GAUSS WITH NISHIDA
a_Ni=1e-4;%%%Nisihida a paramaeter (min size of Nishida fragment)   % IN A6_compute_Seeds_Distrib_Frag_Volume     
k_Ni= 1;  %%%Coeff for Nishida distribution                         % IN A6_compute_Seeds_Distrib_Frag_Volume

%% MAX RANDOM POINTS
n_seeds_max=1.5E4; %%% coefficent for max rnd points                  % IN A6_compute_Seeds_Distrib_Frag_Volume

%% MAX VELOCITY MCKNIGHT
Vc_frag_max=4332; %%% max velocity for McKnight distribution       % A10_Velocity_Distribution

%% FREE TO USE
param1=0;

end