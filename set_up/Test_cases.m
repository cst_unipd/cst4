
%USE this to test the set up, it produces a picture at the end
% clear all
% close all

global ME;
global FRAGMENTS;
global BUBBLE;
global link_data;
% global sim_title_dir;
global sim_title;
% global caso

ME=population_empty_structure();
FRAGMENTS=population_empty_structure();
BUBBLE=population_empty_structure();
link_data=[];

COLLISION_DATA_tracking=struct('target',{},'target_shape',{},'impactor',{},'impactor_shape',{},'point',{});

material_list();


if ~exist('caso','var')
    caso=40;
end

% for caso = 20:26 % number of cases
switch caso
    case 1 % sphere on sphere
        sim_title='Sphere_vs_Sphere';
        [ME,FRAGMENTS,BUBBLE] = generate_ME_sphere(1,0,0,ME,FRAGMENTS,BUBBLE);
        [ME,FRAGMENTS,BUBBLE] = generate_ME_sphere_imp(2,0,0,ME,FRAGMENTS,BUBBLE);
    case 2 % sphere impacting on simple plate
        sim_title='Sphere_vs_Simple_plate';
        [ME,FRAGMENTS,BUBBLE] = generate_ME_simple_plate1(1,0,0,ME,FRAGMENTS,BUBBLE);
        [ME,FRAGMENTS,BUBBLE] = generate_ME_sphere_imp(2,0,0,ME,FRAGMENTS,BUBBLE);
    case 3 % sphere impacting on a edge of simple plate
        sim_title='Sphere_vs_Thickness_simple_plate';
        [ME,FRAGMENTS,BUBBLE] = generate_ME_simple_plate2(1,0,0,ME,FRAGMENTS,BUBBLE);
        [ME,FRAGMENTS,BUBBLE] = generate_ME_sphere_imp(2,0,0,ME,FRAGMENTS,BUBBLE);
    case 4 % sphere impacting on a corner of a simple plate
        sim_title='Sphere_vs_Edge_simple_plate';
        [ME,FRAGMENTS,BUBBLE] = generate_ME_simple_plate3(1,0,0,ME,FRAGMENTS,BUBBLE);
        [ME,FRAGMENTS,BUBBLE] = generate_ME_sphere_imp(2,0,0,ME,FRAGMENTS,BUBBLE);
    case 5 % sphere impacting on side cylinder
        sim_title='Sphere_vs_Base_cylinder';
        [ME,FRAGMENTS,BUBBLE] = generate_ME_cylinder2(1,0,0,ME,FRAGMENTS,BUBBLE);
        [ME,FRAGMENTS,BUBBLE] = generate_ME_sphere_imp(2,0,0,ME,FRAGMENTS,BUBBLE);
    case 6 % sphere impacting on side cylinder
        sim_title='Sphere_vs_Side_cylinder';
        [ME,FRAGMENTS,BUBBLE] = generate_ME_cylinder1(1,0,0,ME,FRAGMENTS,BUBBLE);
        [ME,FRAGMENTS,BUBBLE] = generate_ME_sphere_imp(2,0,0,ME,FRAGMENTS,BUBBLE);
    case 7 % simple plate impacting on simple plate
        sim_title='Simple_plate_vs_Simple_plate';
        [ME,FRAGMENTS,BUBBLE] = generate_ME_simple_plate1(1,0,0,ME,FRAGMENTS,BUBBLE);
        [ME,FRAGMENTS,BUBBLE] = generate_ME_simple_plate_imp1(2,0,0,ME,FRAGMENTS,BUBBLE);
    case 8 % simple plate impacting on edge of a simple plate
        sim_title='Simple_plate_vs_Thickness_simple_plate';
        [ME,FRAGMENTS,BUBBLE] = generate_ME_simple_plate1(1,0,0,ME,FRAGMENTS,BUBBLE);
        [ME,FRAGMENTS,BUBBLE] = generate_ME_simple_plate_imp2(2,0,0,ME,FRAGMENTS,BUBBLE);
    case 9 % simple plate impacting on corner of a simple plate
        sim_title='Simple_plate_vs_Edge_simple_plate';
        [ME,FRAGMENTS,BUBBLE] = generate_ME_simple_plate1(1,0,0,ME,FRAGMENTS,BUBBLE);
        [ME,FRAGMENTS,BUBBLE] = generate_ME_simple_plate_imp3(2,0,0,ME,FRAGMENTS,BUBBLE);
    case 10 % cylinder impacting on simple plate
        sim_title='Base_cylinder_vs_Simple_plate';
        [ME,FRAGMENTS,BUBBLE] = generate_ME_simple_plate1(1,0,0,ME,FRAGMENTS,BUBBLE);
        [ME,FRAGMENTS,BUBBLE] = generate_ME_cylinder_imp1(2,0,0,ME,FRAGMENTS,BUBBLE);
    case 11 % edge cylinder impacting on simple plate
        sim_title='Edge_cylinder_vs_Simple_plate';
        [ME,FRAGMENTS,BUBBLE] = generate_ME_simple_plate1(1,0,0,ME,FRAGMENTS,BUBBLE);
        [ME,FRAGMENTS,BUBBLE] = generate_ME_cylinder_imp3(2,0,0,ME,FRAGMENTS,BUBBLE);
    case 12 % side cylinder impacting on simple plate
        sim_title='Side_cylinder_vs_Simple_plate';
        [ME,FRAGMENTS,BUBBLE] = generate_ME_simple_plate1(1,0,0,ME,FRAGMENTS,BUBBLE);
        [ME,FRAGMENTS,BUBBLE] = generate_ME_cylinder_imp2(2,0,0,ME,FRAGMENTS,BUBBLE);
    case 13 % sphere impacting on two consecutive plates
        sim_title='Sphere_vs_Two_plates';
        [ME,FRAGMENTS,BUBBLE] = generate_ME_two_plates(1,0,0,ME,FRAGMENTS,BUBBLE);
        [ME,FRAGMENTS,BUBBLE] = generate_ME_sphere_imp(3,0,0,ME,FRAGMENTS,BUBBLE);
        
        
    case 14 % sphere impacting on hollow sphere
        sim_title='Sphere_vs_Hollow_sphere';
        [ME,FRAGMENTS,BUBBLE] = generate_ME_hollow_sphere(1,0,0,ME,FRAGMENTS,BUBBLE);
        [ME,FRAGMENTS,BUBBLE] = generate_ME_sphere_imp(2,0,0,ME,FRAGMENTS,BUBBLE);
    case 15 % sphere impacting on base hollow cylinder
        sim_title='Sphere_vs_Base_hollow_cylinder';
        [ME,FRAGMENTS,BUBBLE] = generate_ME_hollow_cylinder2(1,0,0,ME,FRAGMENTS,BUBBLE);
        [ME,FRAGMENTS,BUBBLE] = generate_ME_sphere_imp(2,0,0,ME,FRAGMENTS,BUBBLE);
    case 16 % sphere impacting on side hollow cylinder
        sim_title='Sphere_vs_side_hollow_cylinder';
        [ME,FRAGMENTS,BUBBLE] = generate_ME_hollow_cylinder1(1,0,0,ME,FRAGMENTS,BUBBLE);
        [ME,FRAGMENTS,BUBBLE] = generate_ME_sphere_imp(2,0,0,ME,FRAGMENTS,BUBBLE);
        
    case 17 % sphere impacting on EXPLOSIVE hollow cylinder
        sim_title='Sphere_vs_explosive_side_hollow_cylinder';
        [ME,FRAGMENTS,BUBBLE] = generate_ME_explosive_hollow_cylinder(1,0,0,ME,FRAGMENTS,BUBBLE);
        [ME,FRAGMENTS,BUBBLE] = generate_ME_sphere_imp(2,0,0,ME,FRAGMENTS,BUBBLE);
    case 18 % linked structure - small ball
        sim_title='Sphere_vs_linked_structure';
        [ME,FRAGMENTS,BUBBLE]=generate_ME_linked_structure(1,0,0,ME,FRAGMENTS,BUBBLE);
        [ME,FRAGMENTS,BUBBLE]=link_generate_4linked_structure(0,0,0,ME,FRAGMENTS,BUBBLE);
        [ME,FRAGMENTS,BUBBLE] = generate_ME_sphere_imp(5,0,0,ME,FRAGMENTS,BUBBLE);
    case 19 % linked structure2 - bigger sphere
        sim_title='Bigger_Sphere_vs_linked_sturcture';
        [ME,FRAGMENTS,BUBBLE]=generate_ME_linked_structure(1,0,0,ME,FRAGMENTS,BUBBLE);
        [ME,FRAGMENTS,BUBBLE]=link_generate_4linked_structure(0,0,0,ME,FRAGMENTS,BUBBLE);
        [ME,FRAGMENTS,BUBBLE]=generate_ME_sphere_imp_40cm(length(ME)+1,0,0,ME,FRAGMENTS,BUBBLE);
    case 20 % sphere impacting on long simple plate, to verify cut
        sim_title='Sphere_vs_Slab';
        [ME,FRAGMENTS,BUBBLE] = generate_ME_long_simple_plate(1,0,0,ME,FRAGMENTS,BUBBLE);
        [ME,FRAGMENTS,BUBBLE] = generate_ME_sphere_imp(2,0,0,ME,FRAGMENTS,BUBBLE);
    case 21 % Bubble impacting on two consecutive plates
        sim_title='Bubble_vs_Two_plates';
        [ME,FRAGMENTS,BUBBLE] = generate_ME_two_plates(1,0,0,ME,FRAGMENTS,BUBBLE);
        [ME,FRAGMENTS,BUBBLE] = generate_BUBBLE_sphere_imp(0,0,1,ME,FRAGMENTS,BUBBLE);
    case 22 % sphere impacting on 4 linked simple plate
        sim_title='Sphere_vs_FourLinkedPlates';
        [ME,FRAGMENTS,BUBBLE] = generate_ME_Four_Linked_Plates(1,0,0,ME,FRAGMENTS,BUBBLE);
        [ME,FRAGMENTS,BUBBLE] = link_generate_4linked_structure2(0,0,0,ME,FRAGMENTS,BUBBLE);
        [ME,FRAGMENTS,BUBBLE] = generate_ME_sphere4linkedstruct(5,0,0,ME,FRAGMENTS,BUBBLE);
    case 23 %Check if fragments go back
        sim_title='Ejecta_test';
        [ME,FRAGMENTS,BUBBLE] = generate_ME_Solid_Wall(1,0,0,ME,FRAGMENTS,BUBBLE);
        [ME,FRAGMENTS,BUBBLE] = generate_ME_6mmsphere(2,0,0,ME,FRAGMENTS,BUBBLE);
    case 24 % sphere impacting on explosive hollow sphere
        sim_title='Sphere_vs_Explosive_Hollow_sphere';
        [ME,FRAGMENTS,BUBBLE] = generate_ME_explosive_hollow_sphere(1,0,0,ME,FRAGMENTS,BUBBLE);
        [ME,FRAGMENTS,BUBBLE] = generate_ME_sphere_imp(2,0,0,ME,FRAGMENTS,BUBBLE);
    case 25 % sphere impacting on long simple plate, to verify cut
        sim_title='Double_spheres_vs_Slab';
        [ME,FRAGMENTS,BUBBLE] = generate_ME_long_simple_plate(1,0,0,ME,FRAGMENTS,BUBBLE);
        [ME,FRAGMENTS,BUBBLE] = generate_ME_double_sphere_imp(2,0,0,ME,FRAGMENTS,BUBBLE);
    case 26 % sphere impacting on side cylinder
        sim_title='Cylinder_outline';
        [ME,FRAGMENTS,BUBBLE] = generate_ME_cylinder2(1,0,0,ME,FRAGMENTS,BUBBLE);
        [ME,FRAGMENTS,BUBBLE] = generate_ME_6spheres(2,0,0,ME,FRAGMENTS,BUBBLE);
    case 27 % sphere impacting on side cylinder
        sim_title='Fragments_Vs_Cylinder';
        [ME,FRAGMENTS,BUBBLE] = generate_ME_cylinder2(1,0,0,ME,FRAGMENTS,BUBBLE);
        [ME,FRAGMENTS,BUBBLE]=generate_FRAGMENTS_6(0,1,0,ME,FRAGMENTS,BUBBLE);
    case 28 %testing more complex links
        sim_title='More_complex_links';
        [ME,FRAGMENTS,BUBBLE] = generate_ME_two_Linked_Plates(1,0,0,ME,FRAGMENTS,BUBBLE);
        [ME,FRAGMENTS,BUBBLE] = generate_link2linked_structure2(0,0,0,ME,FRAGMENTS,BUBBLE);
        [ME,FRAGMENTS,BUBBLE] = generate_ME_sphere_imp_2plate(3,0,0,ME,FRAGMENTS,BUBBLE);
    case 29 %LOFT model
        sim_title='Loft_model_vs_sphere';
        [ME,FRAGMENTS,BUBBLE]=generate_ME_LOFT(0,0,0,ME,FRAGMENTS,BUBBLE);
        [ME,FRAGMENTS,BUBBLE]=link_generate_LOFT(0,0,0,ME,FRAGMENTS,BUBBLE);
        [ME,FRAGMENTS,BUBBLE]=generate_ME_sphere_imp4_LOFT(length(ME)+1,0,0,ME,FRAGMENTS,BUBBLE);
    case 30 % sphere vs simple plate and explosive hollow cylinder
        sim_title='Sphere_vs_Simple_Plate_and_Explosive_Hollow_Cylinder';
        [ME,FRAGMENTS,BUBBLE] = generate_ME_simple_plate1(1,0,0,ME,FRAGMENTS,BUBBLE);
        [ME,FRAGMENTS,BUBBLE] = generate_ME_explosive_hollow_cylinder2(2,0,0,ME,FRAGMENTS,BUBBLE);
        [ME,FRAGMENTS,BUBBLE] = generate_ME_sphere_imp2(3,0,0,ME,FRAGMENTS,BUBBLE);
    case 31 % LOFT model with plates as central body
        sim_title='Panel_loft_model_vs_sphere';
        [ME,FRAGMENTS,BUBBLE]=generate_ME_LOFT_2(1,0,0,ME,FRAGMENTS,BUBBLE);
        [ME,FRAGMENTS,BUBBLE]=link_generate_LOFT_2(0,0,0,ME,FRAGMENTS,BUBBLE);
        [ME,FRAGMENTS,BUBBLE]=generate_ME_Al_sphere_LOFT(length(ME)+1,0,0,ME,FRAGMENTS,BUBBLE);
    case 32 % Cubesat with plates vs sphere
        sim_title='Cubesat_vs_sphere';
        [ME,FRAGMENTS,BUBBLE]=generate_ME_cubesat(1,0,0,ME,FRAGMENTS,BUBBLE);
        [ME,FRAGMENTS,BUBBLE]=link_generate_cubesat(0,0,0,ME,FRAGMENTS,BUBBLE);
        [ME,FRAGMENTS,BUBBLE]=generate_ME_sphere_imp_cubesat(length(ME)+1,0,0,ME,FRAGMENTS,BUBBLE);
        %     case 32 % array of linked plates
        %         n_x = 5;
        %         n_y = 2;
        %         sim_title='Sphere_vs_MatrixLinkedPlates';
        %         [ME,FRAGMENTS,BUBBLE] = generate_ME_matrix_Linked_Plates(1,n_x,n_y);
        %         [ME,FRAGMENTS,BUBBLE] = generate_link_matrix_linked_structure(n_x,n_y);
        %         [ME,FRAGMENTS,BUBBLE] = generate_ME_double_sphere_imp2(n_x*n_y+1,ME);
    case 40
        sim_title='Nishida_first_complete_test';
        [ME,FRAGMENTS,BUBBLE] = generate_ME_simple_plate_Nishida1(1,0,0,ME,FRAGMENTS,BUBBLE);
        [ME,FRAGMENTS,BUBBLE] = generate_ME_sphere_imp_Nishida1(2,0,0,ME,FRAGMENTS,BUBBLE);
end

sim_title_dir=[sim_title,'_',datestr(now,30)];
sim_title_dir=fullfile('Results',sim_title_dir);
mkdir(sim_title_dir);
mkdir(fullfile(sim_title_dir,'figures'));
mkdir(fullfile(sim_title_dir,'data'));
global ME_SR;
ME_SR=ME;
draw3D();
pause(0.01)

%close all
