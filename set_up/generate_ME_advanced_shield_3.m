function [ME,FRAGMENTS,BUBBLE] =generate_ME_advanced_shield_3(i_ME,j,k,ME,FRAGMENTS,BUBBLE)

global MATERIAL_LIST

% INITIAL CONDITION TARGET
% IF sphere    (SHAPE_ID=2) [R,0 ,0 ] thickness=0
% IF hollow sphere (SHAPE_ID=3) [R,0 ,0 ]
% thickness=t
% IF cylinder (SHAPE_ID=4) [R,0, H] thickness=0;
% IF hollow cylinder (SHAPE_ID=5) [R,0, H] thickness=t;

Initial_vel=[0;0;0]; %velocity of every ME
EMR= 0; % Energy to mass ratio
SHAPE_ID=1;
FAIL_TYPE=3;
DEFAULT_seeds_distribution_ID = 1;%<<<<=== 0 random cartesian, 4 random hollow
DEFAULT_seeds_distribution_param1 = 0;
DEFAULT_seeds_distribution_param2 = 15;

rho=[0.343, 0.007]*10; %[kg/m2]
thickness=rho/2700; %[0.001, 0.001]; %[m]
long_side=ones([1,length(rho)])*0.1; %[m]
short_side=ones([1,length(rho)])*0.1; %[m]

for i=1:1:length(rho)
    density(i)=rho(i)/thickness(i); %[kg/m3]
    mass(i)=long_side(i)*short_side(i)*thickness(i)*density(i);
end

material_ID=1;
pos=[0;0;-0.002];

%MEs outer wall
j=0;
k=1;
for i = i_ME:i_ME+1
    position_cm=[0;0;0]+pos*j;
    quaternions=[1,0,0,0];
    w=[0,0,0]';
    ME(i).object_ID=i;
    ME(i).material_ID=material_ID;                   % 1=Aluminium, only one available
    ME(i).GEOMETRY_DATA.shape_ID=SHAPE_ID; % 1=plate, 2=sphere, 4=cylinder
    ME(i).GEOMETRY_DATA.dimensions=[long_side(k),short_side(k),thickness(k)];
    ME(i).GEOMETRY_DATA.thick=thickness(k);
    ME(i).GEOMETRY_DATA.mass0=mass(k);
    ME(i).GEOMETRY_DATA.mass=mass(k);
    ME(i).DYNAMICS_INITIAL_DATA.cm_coord0=position_cm;
    ME(i).DYNAMICS_DATA.cm_coord=position_cm;
    ME(i).DYNAMICS_INITIAL_DATA.quaternions0=quaternions;
    ME(i).DYNAMICS_INITIAL_DATA.vel0=Initial_vel;
    ME(i).DYNAMICS_DATA.vel=Initial_vel;
    ME(i).DYNAMICS_INITIAL_DATA.w0=w;
    ME(i).DYNAMICS_DATA.quaternions=quaternions;
    ME(i).DYNAMICS_DATA.w=w;
    ME(i).DYNAMICS_DATA.virt_momentum=[0,0,0]';
    ME(i).GEOMETRY_DATA.A_M_ratio=0;
    ME(i).GEOMETRY_DATA.c_hull=[0, 0, 0];
    ME(i).FRAGMENTATION_DATA.threshold0=EMR;
    ME(i).FRAGMENTATION_DATA.threshold=EMR;
    ME(i).FRAGMENTATION_DATA.failure_ID=FAIL_TYPE;
    ME(i).FRAGMENTATION_DATA.breakup_flag=1;
    ME(i).FRAGMENTATION_DATA.ME_energy_transfer_coef=0.001;
    ME(i).FRAGMENTATION_DATA.cMLOSS=0.2;
    ME(i).FRAGMENTATION_DATA.cELOSS=0.1;
    ME(i).FRAGMENTATION_DATA.c_EXPL=0;
    ME(i).FRAGMENTATION_DATA.seeds_distribution_ID = DEFAULT_seeds_distribution_ID;
    ME(i).FRAGMENTATION_DATA.seeds_distribution_param1 = DEFAULT_seeds_distribution_param1;
    ME(i).FRAGMENTATION_DATA.seeds_distribution_param2 = DEFAULT_seeds_distribution_param2;
    ME(i).FRAGMENTATION_DATA.param_add1=0;
    ME(i).FRAGMENTATION_DATA.param_add2=0;
    j=j+1;
    k=k+1;
end

rho_Al=2710; %[kg/m3]
thickness_Al=0.001; %[m]
long_side_Al=0.1; %[m]
short_side_Al=0.1; %[m]
mass_Al=long_side_Al*short_side_Al*thickness_Al*rho_Al;

%ME rear wall
i = i_ME+2;
position_cm=ME(i_ME+1).DYNAMICS_INITIAL_DATA.cm_coord0-[0;0;0.01];
quaternions=[1,0,0,0];
w=[0,0,0]';
ME(i).object_ID=i;
ME(i).material_ID=material_ID;                   % 1=Aluminium, only one available
ME(i).GEOMETRY_DATA.shape_ID=SHAPE_ID; % 1=plate, 2=sphere, 4=cylinder
ME(i).GEOMETRY_DATA.dimensions=[long_side_Al,short_side_Al,thickness_Al];
ME(i).GEOMETRY_DATA.thick=thickness_Al;
ME(i).GEOMETRY_DATA.mass0=mass_Al;
ME(i).GEOMETRY_DATA.mass=mass_Al;
ME(i).DYNAMICS_INITIAL_DATA.cm_coord0=position_cm;
ME(i).DYNAMICS_DATA.cm_coord=position_cm;
ME(i).DYNAMICS_INITIAL_DATA.quaternions0=quaternions;
ME(i).DYNAMICS_INITIAL_DATA.vel0=Initial_vel;
ME(i).DYNAMICS_DATA.vel=Initial_vel;
ME(i).DYNAMICS_INITIAL_DATA.w0=w;
ME(i).DYNAMICS_DATA.quaternions=quaternions;
ME(i).DYNAMICS_DATA.w=w;
ME(i).DYNAMICS_DATA.virt_momentum=[0,0,0]';
ME(i).GEOMETRY_DATA.A_M_ratio=0;
ME(i).GEOMETRY_DATA.c_hull=[0, 0, 0];
ME(i).FRAGMENTATION_DATA.threshold0=EMR;
ME(i).FRAGMENTATION_DATA.threshold=EMR;
ME(i).FRAGMENTATION_DATA.failure_ID=FAIL_TYPE;
ME(i).FRAGMENTATION_DATA.breakup_flag=1;
ME(i).FRAGMENTATION_DATA.ME_energy_transfer_coef=0.001;
ME(i).FRAGMENTATION_DATA.cMLOSS=0.2;
ME(i).FRAGMENTATION_DATA.cELOSS=0.1;
ME(i).FRAGMENTATION_DATA.c_EXPL=0;
ME(i).FRAGMENTATION_DATA.seeds_distribution_ID = DEFAULT_seeds_distribution_ID;
ME(i).FRAGMENTATION_DATA.seeds_distribution_param1 = DEFAULT_seeds_distribution_param1;
ME(i).FRAGMENTATION_DATA.seeds_distribution_param2 = DEFAULT_seeds_distribution_param2;
ME(i).FRAGMENTATION_DATA.param_add1=0;
ME(i).FRAGMENTATION_DATA.param_add2=0;
k=k+1;

rho_Ti=4428.784752; %[kg/m3]
thickness_Ti=0.001; %[m]
long_side_Ti=0.1; %[m]
short_side_Ti=0.1; %[m]
mass_Ti=long_side_Ti*short_side_Ti*thickness_Ti*rho_Ti;

%ME rear
i = i_ME+3;
position_cm=ME(i_ME+2).DYNAMICS_INITIAL_DATA.cm_coord0-[0;0;0.115];
quaternions=[1,0,0,0];
w=[0,0,0]';
ME(i).object_ID=i;
ME(i).material_ID=material_ID;                   % 1=Aluminium, only one available
ME(i).GEOMETRY_DATA.shape_ID=SHAPE_ID; % 1=plate, 2=sphere, 4=cylinder
ME(i).GEOMETRY_DATA.dimensions=[long_side_Ti,short_side_Ti,thickness_Ti];
ME(i).GEOMETRY_DATA.thick=thickness_Ti;
ME(i).GEOMETRY_DATA.mass0=mass_Ti;
ME(i).GEOMETRY_DATA.mass=mass_Ti;
ME(i).DYNAMICS_INITIAL_DATA.cm_coord0=position_cm;
ME(i).DYNAMICS_DATA.cm_coord=position_cm;
ME(i).DYNAMICS_INITIAL_DATA.quaternions0=quaternions;
ME(i).DYNAMICS_INITIAL_DATA.vel0=Initial_vel;
ME(i).DYNAMICS_DATA.vel=Initial_vel;
ME(i).DYNAMICS_INITIAL_DATA.w0=w;
ME(i).DYNAMICS_DATA.quaternions=quaternions;
ME(i).DYNAMICS_DATA.w=w;
ME(i).DYNAMICS_DATA.virt_momentum=[0,0,0]';
ME(i).GEOMETRY_DATA.A_M_ratio=0;
ME(i).GEOMETRY_DATA.c_hull=[0, 0, 0];
ME(i).FRAGMENTATION_DATA.threshold0=EMR;
ME(i).FRAGMENTATION_DATA.threshold=EMR;
ME(i).FRAGMENTATION_DATA.failure_ID=FAIL_TYPE;
ME(i).FRAGMENTATION_DATA.breakup_flag=1;
ME(i).FRAGMENTATION_DATA.ME_energy_transfer_coef=0.001;
ME(i).FRAGMENTATION_DATA.cMLOSS=0.2;
ME(i).FRAGMENTATION_DATA.cELOSS=0.1;
ME(i).FRAGMENTATION_DATA.c_EXPL=0;
ME(i).FRAGMENTATION_DATA.seeds_distribution_ID = DEFAULT_seeds_distribution_ID;
ME(i).FRAGMENTATION_DATA.seeds_distribution_param1 = DEFAULT_seeds_distribution_param1;
ME(i).FRAGMENTATION_DATA.seeds_distribution_param2 = DEFAULT_seeds_distribution_param2;
ME(i).FRAGMENTATION_DATA.param_add1=0;
ME(i).FRAGMENTATION_DATA.param_add2=0;
end
