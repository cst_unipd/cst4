function [ME,FRAGMENTS,BUBBLE]=generate_ME(i,jj,kk,ME,FRAGMENTS,BUBBLE)

global MATERIAL_LIST

%% DATA (to modify)

ME(i).object_ID = i;

SHAPE_ID = 1;   % 1 = box/plate, 
                % 2 = sphere,
                % 3 = hollow_sphere
                % 4 = cylinder
                % 5 = hollow cylinder
                
dimension_abc=[0.2, 0.2, 0.001];  % if box/plate       (SHAPE_ID = 1) [L1, L2, L3]  [m]
                                  % if sphere          (SHAPE_ID = 2) [R, 0, 0]     [m]
                                  % if hollow sphere   (SHAPE_ID = 3) [R_ext, 0, 0] [m]
                                  % if cylinder        (SHAPE_ID = 4) [R, 0, H]     [m]
                                  % if hollow cylinder (SHAPE_ID = 5) [R_ext, 0, H] [m]
                                  
thickness = 0;   % if solid     (SHAPE_ID = 1 or 2 or 4)  thickness = 0
                 % if hollow    (SHAPE_ID = 3 or 5)       thickness = t [m]
                                
position_cm = [0; 0; 0]; % position of the center of mass of the object [m]

rot_axis = [0; 1; 0]; % rotates the ME around an axis of rotation alpha [rad] 
alpha = pi/2; % [rad] angle of rotation

initial_vel=[0; 0; 0]; % [m/s] initial velocity 
material_ID = 1; %  1 = Al
                 %  2 = Steel

isBreakable = 1; % 1 = true
                 % 0 = false

FAIL_TYPE = 1; % 1 = strain 2 = TBD...
                 
EMR = 0; % Energy to mass ratio to trigger complete fragmentation [J/kg]

Type_of_seeds_distribution = 1;   % 0 = random
                                  % 1 = gaussian
                                  % 4 = distribution for hollow shapes

number_of_fragments = 3000;


%% DATA (not to modify)
switch (SHAPE_ID)
    case 1
        mass = dimension_abc(1)*dimension_abc(2)*dimension_abc(3)*MATERIAL_LIST(material_ID).density; %[kg]
    case 2
        mass= 4/3*pi*dimension_abc(1)^3*MATERIAL_LIST(material_ID).density; %[kg]
    case 3
        mass= 4/3*pi*(dimension_abc(1)^3-(dimension_abc(1)-thickness)^3)*MATERIAL_LIST(material_ID).density; %[kg]
    case 4
        mass = pi*dimension_abc(1)^2*dimension_abc(3)*MATERIAL_LIST(material_ID).density; %[kg]
    case 5
        mass = (pi*dimension_abc(1)^2*dimension_abc(3)-(pi*(dimension_abc(1)-thickness)^2*(dimension_abc(3)-thickness)))*MATERIAL_LIST(material_ID).density; %[kg]
end

quaternions=[cos(alpha/2), sin(alpha/2)*rot_axis(1), sin(alpha/2)*rot_axis(2), sin(alpha/2)*rot_axis(3)];

ME(i).material_ID = material_ID;                   % 1=Aluminium, only one available
ME(i).GEOMETRY_DATA.shape_ID = SHAPE_ID; % 1=plate, 2=sphere, 4=cylinder
ME(i).GEOMETRY_DATA.dimensions = dimension_abc;
ME(i).GEOMETRY_DATA.thick = thickness;
ME(i).GEOMETRY_DATA.mass0 = mass;
ME(i).GEOMETRY_DATA.mass = mass;
ME(i).DYNAMICS_INITIAL_DATA.cm_coord0 = position_cm;
ME(i).DYNAMICS_DATA.cm_coord = position_cm;
ME(i).DYNAMICS_INITIAL_DATA.quaternions0 = quaternions;
ME(i).DYNAMICS_INITIAL_DATA.vel0 = initial_vel;
ME(i).DYNAMICS_DATA.vel = initial_vel;
ME(i).DYNAMICS_INITIAL_DATA.w0 = w;
ME(i).DYNAMICS_DATA.quaternions = quaternions;
ME(i).DYNAMICS_DATA.w = w;
ME(i).DYNAMICS_DATA.virt_momentum = [0,0,0]';
ME(i).GEOMETRY_DATA.A_M_ratio = 0;
ME(i).GEOMETRY_DATA.c_hull = [0, 0, 0];
ME(i).FRAGMENTATION_DATA.threshold0 = EMR;
ME(i).FRAGMENTATION_DATA.threshold = EMR;
ME(i).FRAGMENTATION_DATA.failure_ID = FAIL_TYPE;
ME(i).FRAGMENTATION_DATA.breakup_flag = isBreakable;
ME(i).FRAGMENTATION_DATA.ME_energy_transfer_coef = 0.001; % default value
ME(i).FRAGMENTATION_DATA.cMLOSS = 0.2;
ME(i).FRAGMENTATION_DATA.cELOSS = 0.1;
ME(i).FRAGMENTATION_DATA.c_EXPL = 0;
ME(i).FRAGMENTATION_DATA.seeds_distribution_ID = Type_of_seeds_distribution;
ME(i).FRAGMENTATION_DATA.seeds_distribution_param1 = number_of_fragments;
ME(i).FRAGMENTATION_DATA.seeds_distribution_param2 = 10;
ME(i).FRAGMENTATION_DATA.param_add1 = 0;
ME(i).FRAGMENTATION_DATA.param_add2 = 0;

