function  generate_link_matrix_linked_structure(n_x,n_y)
%GENERATE_LOFT_LINK; in this file only address which element are connected and
% with which material
global link_data;
global link_data0;

%-------------------------------------------------------------------------
%LINK INIATIALIZATION, 

f_id = 'id';
f_con = 'ME_connect';   % The pair of ME connected by the link
f_mat = 'material_ID';  % 1=Al, 2=steel, 
f_geom= 'geometry';
f_type= 'type_ID';      %1=beam, 2=surface...
f_fail= 'failure_ID';  % Type of faiulre (1=max elongation, 2= max strain...)
f_k   = 'k_mat';       % 6x6 matrix for stiffness response
f_c   = 'c_mat';       % 6x6 matrix for viscous response 
f_rest = 'rest';     % Equilibrium configuration, when no forces are exchanged
f_state = 'state';   % 1= intact, 0=broken

number_links = 2*n_x*n_y - (n_x + n_y);

for ss = 1:number_links
    ID(ss) = {ss};      %ID of the link
    mat(ss)  = {1};
    geom(ss) ={1};
    type(ss) ={1};
    val_fail(ss) = {1};
    val_k(ss) = {eye(6)}; %Dummy, identity matrix. Filled out below
    val_c(ss) = {eye(6)}; %Dummy
    val_rest(ss) = {ones(1,6)}; %Dummy
    val_state(ss) = {1}; % 1 is intact 0 is broken.
end

kk = 1;

for jj = 1:n_x
    
    if jj == (n_x)
        
        for ii = 1:(n_y-1)
            con(kk)={[(jj-1)*n_y+ii (jj-1)*n_y+(ii+1)]}; % connection with the MEs
            kk = kk + 1;       
        end
    else
        
        for ii = 1:(n_y-1)
            
            con(kk)={[(jj-1)*n_y+ii (jj-1)*n_y+(ii+1)]};
            con(kk+1)={[(jj-1)*n_y+ii jj*n_y+ii]};
            kk = kk + 2;
            
            if ii == (n_y-1)
                con(kk)={[(jj-1)*n_y+(ii+1) jj*n_y+(ii+1)]};
                kk = kk + 1;
            end
            
        end
        
    end
    
end


link_data = struct(f_id,ID,f_con,con,f_mat,mat,f_geom,geom,f_type,type,f_fail,val_fail,f_state,val_state,f_k,val_k,f_c,val_c,f_rest,val_rest);

%Completes the link array with the geometries
set_link_properties();
link_data0=link_data;

end

