function ME = generate_cyl_Hanada(name,dimension_abc,position_cm,quaternions,ME)

global MATERIAL_LIST

ii=length(ME)+1;
ME(ii).object_ID_index = name;                                     
ME(ii).material_ID = 1;                  
ME(ii).GEOMETRY_DATA.shape_ID = 4;   
ME(ii).GEOMETRY_DATA.dimensions = dimension_abc;
ME(ii).GEOMETRY_DATA.thick = 0;
ME(ii).GEOMETRY_DATA.mass0 = pi*dimension_abc(1)^2*dimension_abc(3)*MATERIAL_LIST(ME(ii).material_ID).density;
ME(ii).GEOMETRY_DATA.mass = pi*dimension_abc(1)^2*dimension_abc(3)*MATERIAL_LIST(ME(ii).material_ID).density;
ME(ii).GEOMETRY_DATA.A_M_ratio = 0;
ME(ii).GEOMETRY_DATA.c_hull = [0, 0, 0];
ME(ii).DYNAMICS_INITIAL_DATA.cm_coord0 = reshape(position_cm,3,1);
ME(ii).DYNAMICS_DATA.cm_coord = reshape(position_cm,3,1);
ME(ii).DYNAMICS_INITIAL_DATA.quaternions0 = quaternions;
ME(ii).DYNAMICS_INITIAL_DATA.vel0 = [0; 0; 0];
ME(ii).DYNAMICS_DATA.vel = [0; 0; 0];
ME(ii).DYNAMICS_INITIAL_DATA.w0 = [0; 0; 0];
ME(ii).DYNAMICS_DATA.quaternions = quaternions;
ME(ii).DYNAMICS_DATA.w = [0; 0; 0];
ME(ii).DYNAMICS_DATA.virt_momentum = [0,0,0]';
ME(ii).FRAGMENTATION_DATA.threshold0 = 0;
ME(ii).FRAGMENTATION_DATA.threshold = 0;
ME(ii).FRAGMENTATION_DATA.failure_ID = 3;
ME(ii).FRAGMENTATION_DATA.breakup_flag = 1;
ME(ii).FRAGMENTATION_DATA.ME_energy_transfer_coef = 0; % default value
ME(ii).FRAGMENTATION_DATA.cMLOSS = 0.2;
ME(ii).FRAGMENTATION_DATA.cELOSS = 0.1;
ME(ii).FRAGMENTATION_DATA.c_EXPL = 0;
ME(ii).FRAGMENTATION_DATA.seeds_distribution_ID = 1;
ME(ii).FRAGMENTATION_DATA.seeds_distribution_param1 = 0;
ME(ii).FRAGMENTATION_DATA.seeds_distribution_param2 = 10;
ME(ii).FRAGMENTATION_DATA.param_add1 = 0;
ME(ii).FRAGMENTATION_DATA.param_add2 = 0;

