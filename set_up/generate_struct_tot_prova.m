%> @file  generate_struct_tot_prova.m
%> @brief Populating ME structure
%> @author Cinzia Giacomuzzo (cinzia.giacomuzzo@unipd.it)
%======================================================================
%> @brief Function used to populate MacroElements structure and to initialise COLLISION_DATA structures. It can be used to populate FRAGMENTS and BUBBLE for a not empty fragments initial environment  
%>
%> @retval ME MacroElements initial population
%> @retval FRAGMENTS FRAGMENTS initial population. By default it will be
%populated only after a collision
%> @retval BUBBLE BUBBLE initial population. By default it will be
%populated after applying bubble threshold criterion to every new FRAGMENTS
%population
%> @retval COLLISION_DATA COLLISION_DATA structure initialization. It will
%be populated by copying output collision data from Tracking 
%> @retval COLLISION_DATA_small collision data from Tracking
%======================================================================
function [ME, FRAGMENTS, BUBBLE, COLLISION_DATA_tracking]=generate_struct_tot_prova()

    ME=population_empty_structure();
    FRAGMENTS=population_empty_structure();
    BUBBLE=population_empty_structure();


    COLLISION_DATA_tracking=struct('target',{},'target_shape',{},'impactor',{},'impactor_shape',{},'point',{});


    % INITIAL CONDITION AT WILL
    Initial_vel=[0;0;-0.0001]; %velocity of every ME
    EMR= 0; % Energy to mass ratio 
DEFAULT_seeds_distribution_ID = 0;%<<<<=== 0 random, 3 McKnight
DEFAULT_seeds_distribution_param1 = 20;
DEFAULT_seeds_distribution_param2 = 5;
DEFAULT_seeds_distribution_ID_hollow=4;


% % %         i=1;
% % %         quaternions=[1,0,0,0];
% % %         %quaternions=[sqrt(2)/2,-sqrt(2)/2,0,0];
% % %         vel=[0;0;0];
% % %         w=[0;0;0];
% % %         ME(i).object_ID=i;
% % %         ME(i).material_ID=1;    
% % %         ME(i).GEOMETRY_DATA.shape_ID=1; %box
% % %         ME(i).GEOMETRY_DATA.dimensions=[15,0.05,10];
% % %         ME(i).GEOMETRY_DATA.thick=0; %kg
% % %         ME(i).GEOMETRY_DATA.mass0=1*i; %kg
% % %         ME(i).GEOMETRY_DATA.mass=1*i; %kg
% % %         ME(i).GEOMETRY_DATA.A_M_ratio=0;
% % %         ME(i).GEOMETRY_DATA.c_hull=[0, 0, 0];% 1, 1, 1; 2, 2, 2; 3, 3, 3];
% % %         ME(i).DYNAMICS_INITIAL_DATA.cm_coord0=[11;10;5];
% % %         ME(i).DYNAMICS_INITIAL_DATA.quaternions0=quaternions;
% % %         ME(i).DYNAMICS_INITIAL_DATA.vel0=[0;0;0];
% % %         ME(i).DYNAMICS_INITIAL_DATA.w0=w;
% % %         ME(i).DYNAMICS_DATA.cm_coord=ME(i).DYNAMICS_INITIAL_DATA.cm_coord0;
% % %         ME(i).DYNAMICS_DATA.quaternions=ME(i).DYNAMICS_INITIAL_DATA.quaternions0;
% % %         ME(i).DYNAMICS_DATA.vel=ME(i).DYNAMICS_INITIAL_DATA.vel0;
% % %         ME(i).DYNAMICS_DATA.w=ME(i).DYNAMICS_INITIAL_DATA.w0;
% % %         ME(i).DYNAMICS_DATA.virt_momentum=[0,0,0];
% % %         ME(i).FRAGMENTATION_DATA.failure_ID=1;
% % %         ME(i).FRAGMENTATION_DATA.threshold0=500000;
% % %         ME(i).FRAGMENTATION_DATA.threshold=500000;
% % %         ME(i).FRAGMENTATION_DATA.breakup_flag=1;
% % %         ME(i).FRAGMENTATION_DATA.ME_energy_transfer_coef=0.001;
% % %         ME(i).FRAGMENTATION_DATA.cMLOSS=0.2;
% % %         ME(i).FRAGMENTATION_DATA.cELOSS=0.1;
% % %         ME(i).FRAGMENTATION_DATA.c_EXPL=0;
% % %         ME(i).FRAGMENTATION_DATA.seeds_distribution_ID = DEFAULT_seeds_distribution_ID; 
% % %         ME(i).FRAGMENTATION_DATA.seeds_distribution_param1 = DEFAULT_seeds_distribution_param1;
% % %         ME(i).FRAGMENTATION_DATA.seeds_distribution_param2 = DEFAULT_seeds_distribution_param2;
% % %         ME(i).FRAGMENTATION_DATA.param_add1=0;
% % %         ME(i).FRAGMENTATION_DATA.param_add2=0;



    i=1;
        quaternions=[1,0,0,0];
        %quaternions=[sqrt(2)/2,-sqrt(2)/2,0,0];
        vel=[0;0;0];
        w=[0;0;0];
        ME(i).object_ID=i;
        ME(i).material_ID=1;    
        ME(i).GEOMETRY_DATA.shape_ID=1; %box
        ME(i).GEOMETRY_DATA.dimensions=[25,0.05,25];
        ME(i).GEOMETRY_DATA.thick=0; %kg
        ME(i).GEOMETRY_DATA.mass0=1; %kg
        ME(i).GEOMETRY_DATA.mass=1; %kg
        ME(i).GEOMETRY_DATA.A_M_ratio=0;
        ME(i).GEOMETRY_DATA.c_hull=[0, 0, 0];% 1, 1, 1; 2, 2, 2; 3, 3, 3];
        ME(i).DYNAMICS_INITIAL_DATA.cm_coord0=[11;10;5];
        ME(i).DYNAMICS_INITIAL_DATA.quaternions0=quaternions;
        ME(i).DYNAMICS_INITIAL_DATA.vel0=[0;0;0];
        ME(i).DYNAMICS_INITIAL_DATA.w0=w;
        ME(i).DYNAMICS_DATA.cm_coord=ME(i).DYNAMICS_INITIAL_DATA.cm_coord0;
        ME(i).DYNAMICS_DATA.quaternions=ME(i).DYNAMICS_INITIAL_DATA.quaternions0;
        ME(i).DYNAMICS_DATA.vel=ME(i).DYNAMICS_INITIAL_DATA.vel0;
        ME(i).DYNAMICS_DATA.w=ME(i).DYNAMICS_INITIAL_DATA.w0;
        ME(i).DYNAMICS_DATA.virt_momentum=[0,0,0];
        ME(i).FRAGMENTATION_DATA.failure_ID=1;
        ME(i).FRAGMENTATION_DATA.threshold0=0;
        ME(i).FRAGMENTATION_DATA.threshold=0;
        ME(i).FRAGMENTATION_DATA.breakup_flag=1;
        ME(i).FRAGMENTATION_DATA.ME_energy_transfer_coef=0.001;
        ME(i).FRAGMENTATION_DATA.cMLOSS=0.2;
        ME(i).FRAGMENTATION_DATA.cELOSS=0.1;
        ME(i).FRAGMENTATION_DATA.c_EXPL=0;
        ME(i).FRAGMENTATION_DATA.seeds_distribution_ID = DEFAULT_seeds_distribution_ID; 
        ME(i).FRAGMENTATION_DATA.seeds_distribution_param1 = DEFAULT_seeds_distribution_param1;
        ME(i).FRAGMENTATION_DATA.seeds_distribution_param2 = DEFAULT_seeds_distribution_param2;
        ME(i).FRAGMENTATION_DATA.param_add1=0;
        ME(i).FRAGMENTATION_DATA.param_add2=0;


    
 i=1;
        quaternions=[1,0,0,0];
        vel=[0;0;0];
        w=[0;0;0];
        FRAGMENTS(i).object_ID=i;
        FRAGMENTS(i).material_ID=1;    
        FRAGMENTS(i).GEOMETRY_DATA.shape_ID=0; %sphere
        FRAGMENTS(i).GEOMETRY_DATA.dimensions=[0.05,0,0];
        FRAGMENTS(i).GEOMETRY_DATA.thick=0; %kg
        FRAGMENTS(i).GEOMETRY_DATA.mass0=0.01; %kg
        FRAGMENTS(i).GEOMETRY_DATA.mass=0.01; %kg
        FRAGMENTS(i).GEOMETRY_DATA.A_M_ratio=0;
        FRAGMENTS(i).GEOMETRY_DATA.c_hull=[0, 0, 0];% 1, 1, 1; 2, 2, 2];
        FRAGMENTS(i).DYNAMICS_INITIAL_DATA.cm_coord0=[11;15;5];
        FRAGMENTS(i).DYNAMICS_INITIAL_DATA.quaternions0=quaternions;
        FRAGMENTS(i).DYNAMICS_INITIAL_DATA.vel0=[0; -3000; 0];
        FRAGMENTS(i).DYNAMICS_INITIAL_DATA.w0=w;
        FRAGMENTS(i).DYNAMICS_DATA.cm_coord=FRAGMENTS(i).DYNAMICS_INITIAL_DATA.cm_coord0;
        FRAGMENTS(i).DYNAMICS_DATA.quaternions=FRAGMENTS(i).DYNAMICS_INITIAL_DATA.quaternions0;
        FRAGMENTS(i).DYNAMICS_DATA.vel=FRAGMENTS(i).DYNAMICS_INITIAL_DATA.vel0;
        FRAGMENTS(i).DYNAMICS_DATA.w=FRAGMENTS(i).DYNAMICS_INITIAL_DATA.w0;
        FRAGMENTS(i).DYNAMICS_DATA.virt_momentum=[0,0,0];
        FRAGMENTS(i).FRAGMENTATION_DATA.failure_ID=1;
        FRAGMENTS(i).FRAGMENTATION_DATA.threshold0=EMR;
        FRAGMENTS(i).FRAGMENTATION_DATA.threshold=EMR;
        FRAGMENTS(i).FRAGMENTATION_DATA.breakup_flag=0;
        FRAGMENTS(i).FRAGMENTATION_DATA.ME_energy_transfer_coef=0.001;
        FRAGMENTS(i).FRAGMENTATION_DATA.cMLOSS=0.2;
        FRAGMENTS(i).FRAGMENTATION_DATA.cELOSS=0.1;
        FRAGMENTS(i).FRAGMENTATION_DATA.c_EXPL=0;
        FRAGMENTS(i).FRAGMENTATION_DATA.seeds_distribution_ID = DEFAULT_seeds_distribution_ID; 
        FRAGMENTS(i).FRAGMENTATION_DATA.seeds_distribution_param1 = DEFAULT_seeds_distribution_param1;
        FRAGMENTS(i).FRAGMENTATION_DATA.seeds_distribution_param2 = DEFAULT_seeds_distribution_param2;
        FRAGMENTS(i).FRAGMENTATION_DATA.param_add1=0;
        FRAGMENTS(i).FRAGMENTATION_DATA.param_add2=0;
        
i=2;
        quaternions=[1,0,0,0];
        vel=[0;0;0];
        w=[0;0;0];
        FRAGMENTS(i).object_ID=i-1;
        FRAGMENTS(i).material_ID=1;    
        FRAGMENTS(i).GEOMETRY_DATA.shape_ID=0; %sphere
        FRAGMENTS(i).GEOMETRY_DATA.dimensions=[0.01,0,0];
        FRAGMENTS(i).GEOMETRY_DATA.thick=0; %kg
        FRAGMENTS(i).GEOMETRY_DATA.mass0=i*0.002; %kg
        FRAGMENTS(i).GEOMETRY_DATA.mass=i*0.002; %kg
        FRAGMENTS(i).GEOMETRY_DATA.A_M_ratio=0;
        FRAGMENTS(i).GEOMETRY_DATA.c_hull=[0, 0, 0];% 1, 1, 1; 2, 2, 2];
        FRAGMENTS(i).DYNAMICS_INITIAL_DATA.cm_coord0=[11;20;5];
        FRAGMENTS(i).DYNAMICS_INITIAL_DATA.quaternions0=quaternions;
        FRAGMENTS(i).DYNAMICS_INITIAL_DATA.vel0=[0; -1000; 0];
        FRAGMENTS(i).DYNAMICS_INITIAL_DATA.w0=w;
        FRAGMENTS(i).DYNAMICS_DATA.cm_coord=FRAGMENTS(i).DYNAMICS_INITIAL_DATA.cm_coord0;
        FRAGMENTS(i).DYNAMICS_DATA.quaternions=FRAGMENTS(i).DYNAMICS_INITIAL_DATA.quaternions0;
        FRAGMENTS(i).DYNAMICS_DATA.vel=FRAGMENTS(i).DYNAMICS_INITIAL_DATA.vel0;
        FRAGMENTS(i).DYNAMICS_DATA.w=FRAGMENTS(i).DYNAMICS_INITIAL_DATA.w0;
        FRAGMENTS(i).DYNAMICS_DATA.virt_momentum=[0,0,0];
        FRAGMENTS(i).FRAGMENTATION_DATA.failure_ID=0;
        FRAGMENTS(i).FRAGMENTATION_DATA.threshold0=EMR;
        FRAGMENTS(i).FRAGMENTATION_DATA.threshold=EMR;
        FRAGMENTS(i).FRAGMENTATION_DATA.breakup_flag=0;
        FRAGMENTS(i).FRAGMENTATION_DATA.ME_energy_transfer_coef=0.001;
        FRAGMENTS(i).FRAGMENTATION_DATA.cMLOSS=0.2;
        FRAGMENTS(i).FRAGMENTATION_DATA.cELOSS=0.1;
        FRAGMENTS(i).FRAGMENTATION_DATA.c_EXPL=0;
        FRAGMENTS(i).FRAGMENTATION_DATA.seeds_distribution_ID = DEFAULT_seeds_distribution_ID; 
        FRAGMENTS(i).FRAGMENTATION_DATA.seeds_distribution_param1 = DEFAULT_seeds_distribution_param1;
        FRAGMENTS(i).FRAGMENTATION_DATA.seeds_distribution_param2 = DEFAULT_seeds_distribution_param2;
        FRAGMENTS(i).FRAGMENTATION_DATA.param_add1=0;
        FRAGMENTS(i).FRAGMENTATION_DATA.param_add2=0;
% 
% % %          i=1;
% % %         quaternions=[1,0,0,0];
% % %         vel=[0;0;0];
% % %         w=[0;0;0];
% % %         FRAGMENTS(i).object_ID=i;
% % %         FRAGMENTS(i).material_ID=1;    
% % %         FRAGMENTS(i).GEOMETRY_DATA.shape_ID=0; %fragment=sphere
% % %         FRAGMENTS(i).GEOMETRY_DATA.dimensions=[0.05,0,0];
% % %         FRAGMENTS(i).GEOMETRY_DATA.thick=0; %kg
% % %         FRAGMENTS(i).GEOMETRY_DATA.mass0=1*0.05; %kg
% % %         FRAGMENTS(i).GEOMETRY_DATA.mass=1*0.05; %kg
% % %         FRAGMENTS(i).GEOMETRY_DATA.A_M_ratio=0;
% % %         FRAGMENTS(i).GEOMETRY_DATA.c_hull=[0, 0, 0];% 1, 1, 1; 2, 2, 2];
% % %         FRAGMENTS(i).DYNAMICS_INITIAL_DATA.cm_coord0=[11;6;5];
% % %         FRAGMENTS(i).DYNAMICS_INITIAL_DATA.quaternions0=quaternions;
% % %         FRAGMENTS(i).DYNAMICS_INITIAL_DATA.vel0=[0; 1000; 0];
% % %         FRAGMENTS(i).DYNAMICS_INITIAL_DATA.w0=w;
% % %         FRAGMENTS(i).DYNAMICS_DATA.cm_coord=FRAGMENTS(i).DYNAMICS_INITIAL_DATA.cm_coord0;
% % %         FRAGMENTS(i).DYNAMICS_DATA.quaternions=FRAGMENTS(i).DYNAMICS_INITIAL_DATA.quaternions0;
% % %         FRAGMENTS(i).DYNAMICS_DATA.vel=FRAGMENTS(i).DYNAMICS_INITIAL_DATA.vel0;
% % %         FRAGMENTS(i).DYNAMICS_DATA.w=FRAGMENTS(i).DYNAMICS_INITIAL_DATA.w0;
% % %         FRAGMENTS(i).DYNAMICS_DATA.virt_momentum=[0,0,0];
% % %         FRAGMENTS(i).FRAGMENTATION_DATA.failure_ID=1;
% % %         FRAGMENTS(i).FRAGMENTATION_DATA.threshold0=EMR;
% % %         FRAGMENTS(i).FRAGMENTATION_DATA.threshold=EMR;
% % %         FRAGMENTS(i).FRAGMENTATION_DATA.breakup_flag=0;
% % %         FRAGMENTS(i).FRAGMENTATION_DATA.ME_energy_transfer_coef=0.001;
% % %         FRAGMENTS(i).FRAGMENTATION_DATA.cMLOSS=0.2;
% % %         FRAGMENTS(i).FRAGMENTATION_DATA.cELOSS=0.1;
% % %         FRAGMENTS(i).FRAGMENTATION_DATA.c_EXPL=0;
% % %         FRAGMENTS(i).FRAGMENTATION_DATA.seeds_distribution_ID = DEFAULT_seeds_distribution_ID; 
% % %         FRAGMENTS(i).FRAGMENTATION_DATA.seeds_distribution_param1 = DEFAULT_seeds_distribution_param1;
% % %         FRAGMENTS(i).FRAGMENTATION_DATA.seeds_distribution_param2 = DEFAULT_seeds_distribution_param2;
% % %         FRAGMENTS(i).FRAGMENTATION_DATA.param_add1=0;
% % %         FRAGMENTS(i).FRAGMENTATION_DATA.param_add2=0;
% % %         
% % %         i=2;
% % %         quaternions=[1,0,0,0];
% % %         vel=[0;0;0];
% % %         w=[0;0;0];
% % %         FRAGMENTS(i).object_ID=i;
% % %         FRAGMENTS(i).material_ID=1;    
% % %         FRAGMENTS(i).GEOMETRY_DATA.shape_ID=0; %fragment=sphere
% % %         FRAGMENTS(i).GEOMETRY_DATA.dimensions=[0.01,0,0];
% % %         FRAGMENTS(i).GEOMETRY_DATA.thick=0; %kg
% % %         FRAGMENTS(i).GEOMETRY_DATA.mass0=1*0.001; %kg
% % %         FRAGMENTS(i).GEOMETRY_DATA.mass=1*0.001; %kg
% % %         FRAGMENTS(i).GEOMETRY_DATA.A_M_ratio=0;
% % %         FRAGMENTS(i).GEOMETRY_DATA.c_hull=[0, 0, 0];% 1, 1, 1; 2, 2, 2];
% % %         FRAGMENTS(i).DYNAMICS_INITIAL_DATA.cm_coord0=[10;0;5];
% % %         FRAGMENTS(i).DYNAMICS_INITIAL_DATA.quaternions0=quaternions;
% % %         FRAGMENTS(i).DYNAMICS_INITIAL_DATA.vel0=[0; 1000; 0];
% % %         FRAGMENTS(i).DYNAMICS_INITIAL_DATA.w0=w;
% % %         FRAGMENTS(i).DYNAMICS_DATA.cm_coord=FRAGMENTS(i).DYNAMICS_INITIAL_DATA.cm_coord0;
% % %         FRAGMENTS(i).DYNAMICS_DATA.quaternions=FRAGMENTS(i).DYNAMICS_INITIAL_DATA.quaternions0;
% % %         FRAGMENTS(i).DYNAMICS_DATA.vel=FRAGMENTS(i).DYNAMICS_INITIAL_DATA.vel0;
% % %         FRAGMENTS(i).DYNAMICS_DATA.w=FRAGMENTS(i).DYNAMICS_INITIAL_DATA.w0;
% % %         FRAGMENTS(i).DYNAMICS_DATA.virt_momentum=[0,0,0];
% % %         FRAGMENTS(i).FRAGMENTATION_DATA.failure_ID=1;
% % %         FRAGMENTS(i).FRAGMENTATION_DATA.threshold0=EMR;
% % %         FRAGMENTS(i).FRAGMENTATION_DATA.threshold=EMR;
% % %         FRAGMENTS(i).FRAGMENTATION_DATA.breakup_flag=0;
% % %         FRAGMENTS(i).FRAGMENTATION_DATA.ME_energy_transfer_coef=0.001;
% % %         FRAGMENTS(i).FRAGMENTATION_DATA.cMLOSS=0.2;
% % %         FRAGMENTS(i).FRAGMENTATION_DATA.cELOSS=0.1;
% % %         FRAGMENTS(i).FRAGMENTATION_DATA.c_EXPL=0;
% % %         FRAGMENTS(i).FRAGMENTATION_DATA.seeds_distribution_ID = DEFAULT_seeds_distribution_ID; 
% % %         FRAGMENTS(i).FRAGMENTATION_DATA.seeds_distribution_param1 = DEFAULT_seeds_distribution_param1;
% % %         FRAGMENTS(i).FRAGMENTATION_DATA.seeds_distribution_param2 = DEFAULT_seeds_distribution_param2;
% % %         FRAGMENTS(i).FRAGMENTATION_DATA.param_add1=0;
% % %         FRAGMENTS(i).FRAGMENTATION_DATA.param_add2=0;
        
    %center element of the Hat
    

% end