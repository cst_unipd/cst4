function  [ME,FRAGMENTS,BUBBLE]=link_generate_LOFT_2(ii,jj,kk,ME,FRAGMENTS,BUBBLE)
%GENERATE_LOFT_LINK; in this file only address which element are connected and
% with which material
global link_data;
global link_data0;
%{
%-------------------------------------------------------------------------
%LINK INIATIALIZATION,

f_id = 'id';
f_con = 'ME_connect';   % The pair of ME connected by the link
f_mat = 'material_ID';  % 1=Al, 2=steel,
f_geom= 'geometry';
f_type= 'type_ID';      %1=beam, 2=surface...
f_fail= 'failure_ID';  % Type of faiulre (1=max elongation, 2= max strain...)
f_k   = 'k_mat';       % 6x6 matrix for stiffness response
f_c   = 'c_mat';       % 6x6 matrix for viscous response
f_rest = 'rest';     % Equilibrium configuration, when no forces are exchanged
f_state = 'state';   % 1= intact, 0=broken

% MEs ID
% ID = {1,2,3,4,5,6,... %6 horizontal panels
%       7,... %top center element
%       8,9,10,11,12,13,... %main body, 6 vertical panels
%       14,... %tank
%       15,16,17,18,19,20,... %bottom hexagonal panels (outside)
%       21,... %top box near the hat
%       22,... %solar panel
%       23,24,25,26,27,28,... %top hexagonal wings
%       29,30,... %two boxes inside bottom hexagonal prism
%       31,32,33,34,... %4 cylinders inside bottom hexagonal prism
%       35,36,37,... %three boxes inside bottom hexagonal prism
%       38,39,40,41,42,... %five boxes inside bottom hexagonal prism
%       43,44,45,... %three flat cylinders along the body
%       46,47,48,49,50,51}; %bottom hexagonal panels (inside)

con = {[1,43],[2,43],[3,43],[4,43],[5,43],[6,43],...
    [7,43],...
    [8,43],[9,43],[10,43],[11,43],[12,43],[13,43],...
    [8,44],[9,44],[10,44],[11,44],[12,44],[13,44],...
    [8,9],[9,10],[10,11],[11,12],[12,13],[8,13],...
    [14,44],[14,45],...
    [15,44],[16,44],[17,44],[18,44],[19,44],[20,44],...
    [15,45],[16,45],[17,45],[18,45],[19,45],[20,45],...
    [21,43],...
    [19,22],...
    [8,23],[8,24],[9,24],[9,25],[10,25],[10,26],[11,26],[11,27],[12,27],[12,28],[13,28],[13,23],...
    [23,43],[24,43],[25,43],[26,43],[27,43],[28,43],...
    [15,29],[15,30],...
    [16,31],[16,32],[16,33],[16,34],...
    [17,35],[17,36],[17,37],...
    [18,38],[18,39],[18,40],[18,41],[18,42],...
    [44,46],[44,47],[44,48],[44,49],[44,50],[44,51],[45,46],[45,47],[45,48],[45,49],[45,50],[45,51]}; %connection with the MEs

for i=1:length(con)
    
    ID{i} = i;                          % Identification number of the link
    mat{i} = 1;                         % Material type for each link: 1 = Al, 2 = steel,
    geom{i} = 1;                        % Geometrical type of each link: 1 = dot, 2 = line, 3 = surface
    type{i} = 1;                        % Element type of each link: 1 = bolted, 2 = welded,
    val_fail{i} = 1;                    % Type of faiulre of the link: 1 = max elongation, 2 = max strain,
    val_k{i} = eye(6);                  % Inizialization of the matrix for stiffness response
    val_c{i} = eye(6);                  % Inizialization of the matrix for viscous response
    val_rest{i} = ones(1,6);            % Inizialization of the equilibrium configuration
    val_state{i} = 1;                   % Inizialization of the state of the link: 1 = intact, 0 = broken
    
end
link_data = struct(f_id,ID,f_con,con,f_mat,mat,f_geom,geom,f_type,type,f_fail,val_fail,f_state,val_state,f_k,val_k,f_c,val_c,f_rest,val_rest);

%Completes the link array with the geometries
set_link_properties();
save('Link_LOFT_all_continuum_new.mat','link_data')
%}
load('Link_LOFT_baseline.mat','link_data')
% load('Link_LOFT_all_continuum_new.mat','link_data')
link_data0=link_data;
end

