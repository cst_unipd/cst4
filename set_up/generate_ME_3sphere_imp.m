function [ME,FRAGMENTS,BUBBLE]=generate_ME_3sphere_imp(i,j,k,ME,FRAGMENTS,BUBBLE)

global MATERIAL_LIST

% INITIAL CONDITION TARGET
r=0.008;
for name = i:1:i+2
    % mass= 10;       %[kg]
    SHAPE_ID = 2;   %box=1, sphere=2, hollow_sphere=3, cylinder=4, hollow cylinder =5;
    dimension_abc=[0.002,0,0]; % IF box/plate (SHAPE_ID=1) [L1,L2,0], thickness=t;
    % IF sphere    (SHAPE_ID=2) [R,0 ,0 ] thickness=0
    % IF hollow sphere (SHAPE_ID=3) [R,0 ,0 ]
    % thickness=t
    % IF cylinder (SHAPE_ID=4) [R,0, H] thickness=0;
    % IF hollow cylinder (SHAPE_ID=5) [R,0, H] thickness=t;
    thickness = 0;%.001;%.001; %[m]
    position_cm=[0+r*mod(name,2)*(name-i)/2;0+r*(name-i)/2;-0.5]; %[m]
    Initial_vel=[0;0;6000]; %[m/s]velocity of every ME
    rot_axis=[0;1;0]; % rotates the ME around this axis of rotation alpha[rad]
    alpha=0; %[rad]
    quaternions=[cos(alpha/2),sin(alpha/2)*rot_axis(1),sin(alpha/2)*rot_axis(2),sin(alpha/2)*rot_axis(3)];
    %Default values
    material_ID=1; % 1= Al, 2=TBD
    mass= 4/3*pi*dimension_abc(1)^3*MATERIAL_LIST(material_ID).density;       %[kg]
    FAIL_TYPE=3; % 1=strian 2= TBD...
    EMR= 0; % Energy to mass ratio
    w=[0;0;0];
    DEFAULT_seeds_distribution_ID = 1;%<<<<=== 0 random, 3 McKnight
    DEFAULT_seeds_distribution_param1 = 192;%100;
    DEFAULT_seeds_distribution_param2 = 10;
    %ME definition
    ME(name).object_ID=name;
    ME(name).material_ID=material_ID;                   % 1=Aluminium, only one available
    ME(name).GEOMETRY_DATA.shape_ID=SHAPE_ID; % 1=plate, 2=sphere, 4=cylinder
    ME(name).GEOMETRY_DATA.dimensions=dimension_abc;
    ME(name).GEOMETRY_DATA.thick=thickness;
    ME(name).GEOMETRY_DATA.mass0=mass;
    ME(name).GEOMETRY_DATA.mass=mass;
    ME(name).DYNAMICS_INITIAL_DATA.cm_coord0=position_cm;
    ME(name).DYNAMICS_DATA.cm_coord=position_cm;
    ME(name).DYNAMICS_INITIAL_DATA.quaternions0=quaternions;
    ME(name).DYNAMICS_INITIAL_DATA.vel0=Initial_vel;
    ME(name).DYNAMICS_DATA.vel=Initial_vel;
    ME(name).DYNAMICS_INITIAL_DATA.w0=w;
    ME(name).DYNAMICS_DATA.quaternions=quaternions;
    ME(name).DYNAMICS_DATA.w=w;
    ME(name).DYNAMICS_DATA.virt_momentum=[0,0,0]';
    ME(name).GEOMETRY_DATA.A_M_ratio=0;
    ME(name).GEOMETRY_DATA.c_hull=[0, 0, 0];
    ME(name).FRAGMENTATION_DATA.threshold0=EMR;
    ME(name).FRAGMENTATION_DATA.threshold=EMR;
    ME(name).FRAGMENTATION_DATA.failure_ID=FAIL_TYPE;
    ME(name).FRAGMENTATION_DATA.breakup_flag=1;
    ME(name).FRAGMENTATION_DATA.ME_energy_transfer_coef=0.001;
    ME(name).FRAGMENTATION_DATA.cMLOSS=0.2;
    ME(name).FRAGMENTATION_DATA.cELOSS=0.1;
    ME(name).FRAGMENTATION_DATA.c_EXPL=0;
    ME(name).FRAGMENTATION_DATA.seeds_distribution_ID = DEFAULT_seeds_distribution_ID;
    ME(name).FRAGMENTATION_DATA.seeds_distribution_param1 = DEFAULT_seeds_distribution_param1;
    ME(name).FRAGMENTATION_DATA.seeds_distribution_param2 = DEFAULT_seeds_distribution_param2;
    ME(name).FRAGMENTATION_DATA.param_add1=0;
    ME(name).FRAGMENTATION_DATA.param_add2=0;
end
