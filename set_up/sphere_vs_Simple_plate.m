
global ME;
global FRAGMENTS;
global BUBBLE;
global sim_title_dir;
global sim_title;


sim_title='sphere_vs_Simple_plate';

% Generate populations and collision data
%[ME, FRAGMENTS, BUBBLE, COLLISION_DATA_tracking] = generate_LOFT_structures;
[ME, FRAGMENTS, BUBBLE, COLLISION_DATA_tracking] = generate_ME_simple_plate;


% Generate Links
 %generate_LOFT_link();

%generate_fragment_impactor(); % used to simulate a first collision
generate_fragment_sphere(); % used to simulate a first collision

sim_title_dir=fullfile('Results',filesep,[sim_title,'_',datestr(now,30)],filesep);
mkdir(sim_title_dir);