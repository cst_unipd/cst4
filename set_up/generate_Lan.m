function [ME,FRAGMENTS,BUBBLE]=generate_Lan(ii,jj,kk,ME,FRAGMENTS,BUBBLE)

Data_Lan % matrix with cm, dimensions and orientation of boxes
bb=1;
box=1:length(data_lan);
for name = ii : ii + length(box)-1
    box_index = box(bb);
    position_cm = data_lan(box_index,1:3)*1.02;
    dimension_abc = data_lan(box_index,4:6)*0.96;
    rot_axis1 = data_lan(box_index,7:9);
    alpha1 = data_lan(box_index,10);
    quat1=[cos(alpha1/2), sin(alpha1/2)*rot_axis1(1), sin(alpha1/2)*rot_axis1(2), sin(alpha1/2)*rot_axis1(3)];
    rot_axis2 = data_lan(box_index,11:13);
    alpha2 = data_lan(box_index,14);
    quat2=[cos(alpha2/2), sin(alpha2/2)*rot_axis2(1), sin(alpha2/2)*rot_axis2(2), sin(alpha2/2)*rot_axis2(3)];
    if exist(fullfile(matlabroot,'toolbox','aero','aero','quatmultiply.m'),'file')
        quaternions=quatmultiply(quat1,quat2);
    else
        quaternions=quatxquat(quat1,quat2);
    end
    ME = generate_box_Lan(name,dimension_abc,position_cm,quaternions,ME);
    ME(name).object_ID = box_index;
    bb = bb+1;
    clear position_cm dimension_abc rot_axis alpha quaternions
end
