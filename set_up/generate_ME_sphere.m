function [ME,FRAGMENTS,BUBBLE]=generate_ME_sphere(i,j,k,ME,FRAGMENTS,BUBBLE)

global MATERIAL_LIST

% INITIAL CONDITION TARGET
name=i;
% mass= 10;       %[kg]
SHAPE_ID = 2;   %box=1, sphere=2, hollow_sphere=3, cylinder=4, hollow cylinder =5;
dimension_abc=[0.05,0,0]; % IF box/plate (SHAPE_ID=1) [L1,L2,0], thickness=t;
% IF sphere    (SHAPE_ID=2) [R,0 ,0 ] thickness=0
% IF hollow sphere (SHAPE_ID=3) [R,0 ,0 ]
% thickness=t
% IF cylinder (SHAPE_ID=4) [R,0, H] thickness=0;
% IF hollow cylinder (SHAPE_ID=5) [R,0, H] thickness=t;
thickness = 0; %[m]
position_cm=[0;0;0]; %[m]
Initial_vel=[0;0;0.00]; %[m/s]velocity of every ME
rot_axis=[0;1;0]; % rotates the ME around this axis of rotation alpha[rad]
alpha=pi/2; %[rad]
% quaternions=[cos(alpha/2),sin(alpha/2)*rot_axis(1),sin(alpha/2)*rot_axis(2),sin(alpha/2)*rot_axis(3)];
quaternions=[0.70710678118654746, 0, -0.70710678118654746,0];
%Default values
material_ID=1; % 1= Al, 2=TBD
mass= 4/3*pi*dimension_abc(1)^3*MATERIAL_LIST(material_ID).density;       %[kg]
FAIL_TYPE=3; % 1=strian 2= TBD...
EMR= 0; % Energy to mass ratio
w=[0;0;0];
DEFAULT_seeds_distribution_ID = 1;%<<<<=== 0 random, 3 McKnight
DEFAULT_seeds_distribution_param1 = 0;
DEFAULT_seeds_distribution_param2 = 5;
%ME definition
ME(i).object_ID=name;
ME(i).material_ID=material_ID;                   % 1=Aluminium, only one available
ME(i).GEOMETRY_DATA.shape_ID=SHAPE_ID; % 1=plate, 2=sphere, 4=cylinder
ME(i).GEOMETRY_DATA.dimensions=dimension_abc;
ME(i).GEOMETRY_DATA.thick=thickness;
ME(i).GEOMETRY_DATA.mass0=mass;
ME(i).GEOMETRY_DATA.mass=mass;
ME(i).DYNAMICS_INITIAL_DATA.cm_coord0=position_cm;
ME(i).DYNAMICS_DATA.cm_coord=position_cm;
ME(i).DYNAMICS_INITIAL_DATA.quaternions0=quaternions;
ME(i).DYNAMICS_INITIAL_DATA.vel0=Initial_vel;
ME(i).DYNAMICS_DATA.vel=Initial_vel;
ME(i).DYNAMICS_INITIAL_DATA.w0=w;
ME(i).DYNAMICS_DATA.quaternions=quaternions;
ME(i).DYNAMICS_DATA.w=w;
ME(i).DYNAMICS_DATA.virt_momentum=[0,0,0]';
ME(i).GEOMETRY_DATA.A_M_ratio=0;
ME(i).GEOMETRY_DATA.c_hull=[0, 0, 0];
ME(i).FRAGMENTATION_DATA.threshold0=EMR;
ME(i).FRAGMENTATION_DATA.threshold=EMR;
ME(i).FRAGMENTATION_DATA.failure_ID=FAIL_TYPE;
ME(i).FRAGMENTATION_DATA.breakup_flag=1;
ME(i).FRAGMENTATION_DATA.ME_energy_transfer_coef=0;
ME(i).FRAGMENTATION_DATA.cMLOSS=0;
ME(i).FRAGMENTATION_DATA.cELOSS=0.15;
ME(i).FRAGMENTATION_DATA.c_EXPL=0;
ME(i).FRAGMENTATION_DATA.seeds_distribution_ID = DEFAULT_seeds_distribution_ID;
ME(i).FRAGMENTATION_DATA.seeds_distribution_param1 = DEFAULT_seeds_distribution_param1;
ME(i).FRAGMENTATION_DATA.seeds_distribution_param2 = DEFAULT_seeds_distribution_param2;
ME(i).FRAGMENTATION_DATA.param_add1=0;
ME(i).FRAGMENTATION_DATA.param_add2=0;
