function  [ME,FRAGMENTS,BUBBLE]=link_generate_user(ii,jj,kk,ME,FRAGMENTS,BUBBLE)
% link_generate_user: links connecting two MEs have to be set here

global link_data;
global link_data0;

%% LINK INIATIALIZATION

ID = {1,2,3};                     % Identification number of the link 
con = {[1,2],[2,3],[1,3]};        % Pairs of MEs connected by each link
mat = {1,1,1};                    % Material type for each link: 1 = Al, 2 = steel,
geom = {1,1,1};                   % Geometrical type of each link: 1 = dot, 2 = line, 3 = surface
type = {1,1,1};                   % Element type of each link: 1 = bolted, 2 = welded,
val_fail = {1,1,1};               % Type of faiulre of the link: 1 = max elongation, 2 = max strain,

%% Data (not to be modified)

% Inizialization of material matrices
val_k = {eye(6),eye(6),eye(6)}; % Inizialization of the matrix for stiffness response
val_c = {eye(6),eye(6),eye(6)}; % Inizialization of the matrix for viscous response
val_rest = {ones(1,6),ones(1,6),ones(1,6)}; % Inizialization of the equilibrium configuration
val_state= {1,1,1}; % Inizialization of the state of the link: 1 = intact, 0 = broken

% Field Names

f_id    = 'id';
f_con   = 'ME_connect';   % Field of the pair of ME connected by the link
f_mat   = 'material_ID';  % 1=Al, 2=steel, 
f_geom  = 'geometry';
f_type  = 'type_ID';      %1 = beam, 2 = surface...
f_fail  = 'failure_ID';  % Type of faiulre (1=max elongation, 2= max strain...)
f_k     = 'k_mat';       % 6x6 matrix for stiffness response
f_c     = 'c_mat';       % 6x6 matrix for viscous response 
f_rest  = 'rest';     % Equilibrium configuration, when no forces are exchanged
f_state = 'state';   % 1= intact, 0=broken

link_data = struct(f_id,ID,f_con,con,f_mat,mat,f_geom,geom,f_type,type,f_fail,val_fail,f_state,val_state,f_k,val_k,f_c,val_c,f_rest,val_rest);

%Completes the link array with the geometries
set_link_properties();
link_data0=link_data;

end

