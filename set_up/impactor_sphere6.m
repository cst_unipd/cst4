function [ME]=impactor_sphere6(i,ME)

%Quick set up 
dimension_abc=[0.002,0,0]; % For sphere (SHAPE_ID=2) [R,0,0]
Velocity=9000;%m/s
Impact_angle=45; %[deg] from the normal to the plate

position_cm=0.5*[0;-sind(Impact_angle);-cosd(Impact_angle)]; %[m]
Initial_vel=Velocity*[0;sind(Impact_angle);cosd(Impact_angle)]; %[m/s]

global MATERIAL_LIST
Al_material_ID=1; % 1= Al
mass= 4/3*pi*dimension_abc(1)^3*MATERIAL_LIST(Al_material_ID).density;       %[kg]

isBreakable=0;
EMR= 0; % Energy to mass ratio to trigger complete fragmentation
Type_of_seeds_distribution = 1;%<<<<=== 0 random, 4 for hollow shapes
number_of_fragements = 200;
Sphere_shape_ID = 2;   %box=1, Sphere=2, hollow_sphere=3, cylinder=4, hollow cylinder =5;


ME(i)=default_obj(); %Fills all fields with default values
%Refresh the ME
ME(i).object_ID=i;
ME(i).material_ID=Al_material_ID;                
ME(i).GEOMETRY_DATA.shape_ID=Sphere_shape_ID; 
ME(i).GEOMETRY_DATA.dimensions=dimension_abc;
ME(i).GEOMETRY_DATA.mass0=mass;
ME(i).GEOMETRY_DATA.mass=ME(i).GEOMETRY_DATA.mass0;
ME(i).DYNAMICS_INITIAL_DATA.cm_coord0=position_cm;
ME(i).DYNAMICS_DATA.cm_coord=ME(i).DYNAMICS_INITIAL_DATA.cm_coord0;
ME(i).DYNAMICS_INITIAL_DATA.vel0=Initial_vel;
ME(i).DYNAMICS_DATA.vel=ME(i).DYNAMICS_INITIAL_DATA.vel0;
ME(i).FRAGMENTATION_DATA.threshold0=EMR;
ME(i).FRAGMENTATION_DATA.threshold=EMR;
ME(i).FRAGMENTATION_DATA.breakup_flag=isBreakable;
ME(i).FRAGMENTATION_DATA.seeds_distribution_ID = Type_of_seeds_distribution;
ME(i).FRAGMENTATION_DATA.seeds_distribution_param1 = number_of_fragements;

