function [ME,FRAGMENTS,BUBBLE] = generate_ME_target_whipple_shield(i,jj,kk,ME,FRAGMENTS,BUBBLE)

%Quick set up 
bumper_dimensions = [0.2,0.2,0.0012]; %[x,y,thickness] [m]
wall_dimensions   = [0.2,0.2,0.0032]; %[x,y,thickness] [m]
standoff = 0.1;            %[m]

%In depth settings
bumper_position=[0;0;0]; %[m]
wall_position=bumper_position+[0;0;standoff]; %[m]

Al_material_ID=1; % 1= Al, 2=TBD
global MATERIAL_LIST
bumper_mass= bumper_dimensions(1)*bumper_dimensions(2)*bumper_dimensions(3)*MATERIAL_LIST(Al_material_ID).density;       %[kg]
wall_mass= wall_dimensions(1)*wall_dimensions(2)*wall_dimensions(3)*MATERIAL_LIST(Al_material_ID).density;       %[kg]

isBreakable=1;
EMR= 0; % Energy to mass ratio to trigger complete fragmentation
Type_of_seeds_distribution = 1;%<<<<=== 0 random, 4 for Hollows
number_of_fragments = 5000;
Box_SHAPE_ID = 1; 
ME(i)=default_obj(); %Fills all fields with default values
ME(i+1)=default_obj();
%Bumper
ME(i).object_ID=i;
             
ME(i).GEOMETRY_DATA.shape_ID=Box_SHAPE_ID; 
ME(i).GEOMETRY_DATA.dimensions=bumper_dimensions;
ME(i).GEOMETRY_DATA.mass0=bumper_mass;
ME(i).GEOMETRY_DATA.mass=ME(i).GEOMETRY_DATA.mass0;
ME(i).DYNAMICS_INITIAL_DATA.cm_coord0=bumper_position;
ME(i).DYNAMICS_DATA.cm_coord=ME(i).DYNAMICS_INITIAL_DATA.cm_coord0;
ME(i).FRAGMENTATION_DATA.threshold0=EMR;
ME(i).FRAGMENTATION_DATA.threshold=EMR;
ME(i).FRAGMENTATION_DATA.breakup_flag=isBreakable;
ME(i).FRAGMENTATION_DATA.seeds_distribution_ID = Type_of_seeds_distribution;
ME(i).FRAGMENTATION_DATA.seeds_distribution_param1 = number_of_fragments;
ME(i).material_ID=Al_material_ID;   

i=i+1;
%Wall
ME(i).object_ID=i;      
ME(i).GEOMETRY_DATA.shape_ID=Box_SHAPE_ID; 
ME(i).GEOMETRY_DATA.dimensions=wall_dimensions;
ME(i).GEOMETRY_DATA.mass0=wall_mass;
ME(i).GEOMETRY_DATA.mass=ME(i).GEOMETRY_DATA.mass0;
ME(i).DYNAMICS_INITIAL_DATA.cm_coord0=wall_position;
ME(i).DYNAMICS_DATA.cm_coord=ME(i).DYNAMICS_INITIAL_DATA.cm_coord0;
ME(i).FRAGMENTATION_DATA.threshold0=0;
ME(i).FRAGMENTATION_DATA.threshold=ME(i).FRAGMENTATION_DATA.threshold0;
ME(i).FRAGMENTATION_DATA.breakup_flag=isBreakable;
ME(i).FRAGMENTATION_DATA.seeds_distribution_ID = Type_of_seeds_distribution;
ME(i).FRAGMENTATION_DATA.seeds_distribution_param1 = number_of_fragments;
ME(i).material_ID=Al_material_ID;  
