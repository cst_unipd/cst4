function [ME]=impactor_sphere(i,ME)

global MATERIAL_LIST
ME(i)=default_obj(); %Fills all fields with default values

% INITIAL CONDITION TARGET
SHAPE_ID = 2;   %box=1, sphere=2, hollow_sphere=3, cylinder=4, hollow cylinder =5;
dimension_abc=[0.01,0,0]; 
thickness = 0; %[m]
% For Fragment
% For box/plate (SHAPE_ID=1) [L1,L2,L3], thickness must be 0;
% For sphere    (SHAPE_ID=2) [R,0,0],  thickness must be 0
% For hollow sphere (SHAPE_ID=3) [R,0,0], thickness must be >0 
% For cylinder (SHAPE_ID=4) [R,0, H] thickness=0;
% IF hollow cylinder (SHAPE_ID=5) [R,0, H] thickness must be >0 
position_cm=[0;0;-0.5]; %[m]
Initial_vel=[0;0;7000]; %[m/s]velocity of every ME
material_ID=1; % 1= Al, 2=TBD
mass= 4/3*pi*dimension_abc(1)^3*MATERIAL_LIST(material_ID).density;       %[kg]

%Default values
rot_axis=[0;1;0]; % rotates the obj around this axis of rotation alpha[rad]
alpha=0; %[rad]

isBreakable=true;
EMR= 0; % Energy to mass ratio to trigger complete fragmentation
Type_of_seeds_distribution = 0;%<<<<=== 0 random, 4 for Hollows
number_of_fragements = 100;
quaternions=[cos(alpha/2),sin(alpha/2)*rot_axis(1),sin(alpha/2)*rot_axis(2),sin(alpha/2)*rot_axis(3)];

%Refresh the ME
ME(i).object_ID=i;
ME(i).material_ID=material_ID;                
ME(i).GEOMETRY_DATA.shape_ID=SHAPE_ID; 
ME(i).GEOMETRY_DATA.dimensions=dimension_abc;
ME(i).GEOMETRY_DATA.thick=thickness;
ME(i).GEOMETRY_DATA.mass0=mass;
ME(i).GEOMETRY_DATA.mass=ME(i).GEOMETRY_DATA.mass0;
ME(i).DYNAMICS_INITIAL_DATA.cm_coord0=position_cm;
ME(i).DYNAMICS_DATA.cm_coord=ME(i).DYNAMICS_INITIAL_DATA.cm_coord0;
ME(i).DYNAMICS_INITIAL_DATA.quaternions0=quaternions;
ME(i).DYNAMICS_DATA.quaternions=quaternions;
ME(i).DYNAMICS_INITIAL_DATA.vel0=Initial_vel;
ME(i).DYNAMICS_DATA.vel=ME(i).DYNAMICS_INITIAL_DATA.vel0;
ME(i).FRAGMENTATION_DATA.threshold0=EMR;
ME(i).FRAGMENTATION_DATA.threshold=EMR;
ME(i).FRAGMENTATION_DATA.breakup_flag=isBreakable;
ME(i).FRAGMENTATION_DATA.seeds_distribution_ID = Type_of_seeds_distribution;
ME(i).FRAGMENTATION_DATA.seeds_distribution_param1 = number_of_fragements;

