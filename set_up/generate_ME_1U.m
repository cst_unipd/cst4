function [ME,FRAGMENTS,BUBBLE]=generate_ME_1U(i_ME,j_FR,k_BU,ME,FRAGMENTS,BUBBLE)

global MATERIAL_LIST

% INITIAL CONDITION AT WILL
Initial_vel=[-11000;0;0]; %velocity of every ME
EMR= 0; % Energy to mass ratio
SHAPE_ID=1;
FAIL_TYPE=3;

DEFAULT_seeds_distribution_ID = 1;%<<<<=== 0 random cartesian, 4 random hollow
DEFAULT_seeds_distribution_param1 = 0;
DEFAULT_seeds_distribution_param2 = 5;
z_plane=0.05; %m
z_lower=-z_plane;
long_side=2*z_plane;
short_side=2*z_plane;
thickness=0.00325;
dimension_abc=[long_side,short_side,thickness];
material_ID=1; % 1= Al, 2=TBD
mass=long_side*short_side*thickness*MATERIAL_LIST(material_ID).density; %[kg]

for i=i_ME:i_ME+3
    jj=i-1;
    r=long_side/2+thickness;
    position_cm=[r*cos(jj*pi/2)+2;r*sin(jj*pi/2);0];
    quat2=[cos(pi/4),0,sin(pi/4),0];
    quat1=[cos(jj*pi/4),0,0,-sin(jj*pi/4)];
    if exist(fullfile(matlabroot,'toolbox','aero','aero','quatmultiply.m'),'file')
        quaternions=quatmultiply(quat2,quat1);
    else
        quaternions=quatxquat(quat2,quat1);
    end
    w=[0,0,0]';
    ME(i).object_ID=i;
    ME(i).material_ID=material_ID;                   % 1=Aluminium, only one available
    ME(i).GEOMETRY_DATA.shape_ID=SHAPE_ID; % 1=plate, 2=sphere, 4=cylinder
    ME(i).GEOMETRY_DATA.dimensions=dimension_abc;
    ME(i).GEOMETRY_DATA.thick=thickness;
    ME(i).GEOMETRY_DATA.mass0=mass;
    ME(i).GEOMETRY_DATA.mass=mass;
    ME(i).DYNAMICS_INITIAL_DATA.cm_coord0=position_cm;
    ME(i).DYNAMICS_DATA.cm_coord=position_cm;
    ME(i).DYNAMICS_INITIAL_DATA.quaternions0=quaternions;
    ME(i).DYNAMICS_INITIAL_DATA.vel0=Initial_vel;
    ME(i).DYNAMICS_DATA.vel=Initial_vel;
    ME(i).DYNAMICS_INITIAL_DATA.w0=w;
    ME(i).DYNAMICS_DATA.quaternions=quaternions;
    ME(i).DYNAMICS_DATA.w=w;
    ME(i).DYNAMICS_DATA.virt_momentum=[0,0,0]';
    ME(i).GEOMETRY_DATA.A_M_ratio=0;
    ME(i).GEOMETRY_DATA.c_hull=[0, 0, 0];
    ME(i).FRAGMENTATION_DATA.threshold0=EMR;
    ME(i).FRAGMENTATION_DATA.threshold=EMR;
    ME(i).FRAGMENTATION_DATA.failure_ID=FAIL_TYPE;
    ME(i).FRAGMENTATION_DATA.breakup_flag=1;
    ME(i).FRAGMENTATION_DATA.ME_energy_transfer_coef=0.001;
    ME(i).FRAGMENTATION_DATA.cMLOSS=0.2;
    ME(i).FRAGMENTATION_DATA.cELOSS=0.1;
    ME(i).FRAGMENTATION_DATA.c_EXPL=0;
    ME(i).FRAGMENTATION_DATA.seeds_distribution_ID = DEFAULT_seeds_distribution_ID;
    ME(i).FRAGMENTATION_DATA.seeds_distribution_param1 = DEFAULT_seeds_distribution_param1;
    ME(i).FRAGMENTATION_DATA.seeds_distribution_param2 = DEFAULT_seeds_distribution_param2;
    ME(i).FRAGMENTATION_DATA.param_add1=0;
    ME(i).FRAGMENTATION_DATA.param_add2=0;
end

for i = i_ME+4:i_ME+10
    position_cm=[2;0;(z_plane+thickness)-((z_plane+thickness)-(z_lower-thickness))/6*(i-(i_ME+4))];
    quaternions=[1,0,0,0];
    w=[0,0,0]';
    ME(i).object_ID=i;
    ME(i).material_ID=material_ID;                   % 1=Aluminium, only one available
    ME(i).GEOMETRY_DATA.shape_ID=SHAPE_ID; % 1=plate, 2=sphere, 4=cylinder
    ME(i).GEOMETRY_DATA.dimensions=dimension_abc;
    ME(i).GEOMETRY_DATA.thick=thickness;
    ME(i).GEOMETRY_DATA.mass0=mass;
    ME(i).GEOMETRY_DATA.mass=mass;
    ME(i).DYNAMICS_INITIAL_DATA.cm_coord0=position_cm;
    ME(i).DYNAMICS_DATA.cm_coord=position_cm;
    ME(i).DYNAMICS_INITIAL_DATA.quaternions0=quaternions;
    ME(i).DYNAMICS_INITIAL_DATA.vel0=Initial_vel;
    ME(i).DYNAMICS_DATA.vel=Initial_vel;
    ME(i).DYNAMICS_INITIAL_DATA.w0=w;
    ME(i).DYNAMICS_DATA.quaternions=quaternions;
    ME(i).DYNAMICS_DATA.w=w;
    ME(i).DYNAMICS_DATA.virt_momentum=[0,0,0]';
    ME(i).GEOMETRY_DATA.A_M_ratio=0;
    ME(i).GEOMETRY_DATA.c_hull=[0, 0, 0];
    ME(i).FRAGMENTATION_DATA.threshold0=EMR;
    ME(i).FRAGMENTATION_DATA.threshold=EMR;
    ME(i).FRAGMENTATION_DATA.failure_ID=FAIL_TYPE;
    ME(i).FRAGMENTATION_DATA.breakup_flag=1;
    ME(i).FRAGMENTATION_DATA.ME_energy_transfer_coef=0.001;
    ME(i).FRAGMENTATION_DATA.cMLOSS=0.2;
    ME(i).FRAGMENTATION_DATA.cELOSS=0.1;
    ME(i).FRAGMENTATION_DATA.c_EXPL=0;
    ME(i).FRAGMENTATION_DATA.seeds_distribution_ID = DEFAULT_seeds_distribution_ID;
    ME(i).FRAGMENTATION_DATA.seeds_distribution_param1 = DEFAULT_seeds_distribution_param1;
    ME(i).FRAGMENTATION_DATA.seeds_distribution_param2 = DEFAULT_seeds_distribution_param2;
    ME(i).FRAGMENTATION_DATA.param_add1=0;
    ME(i).FRAGMENTATION_DATA.param_add2=0;
end

end