function  [ME,FRAGMENTS,BUBBLE]=link_generate_1U(ii,jj,kk,ME,FRAGMENTS,BUBBLE)
%GENERATE_LOFT_LINK; in this file only address which element are connected and
% with which material

global link_data;
global link_data0;

%{

%-------------------------------------------------------------------------
%LINK INIATIALIZATION
n=ii;

con = {[n,n+1],[n+1,n+2],[n+2,n+3],[n,n+3],[n+3,n+4],[n+2,n+4],[n+1,n+4],...
       [n,n+4],[n,n+10],[n+1,n+10],[n+2,n+10],[n+3,n+10],...
       [n,n+5],[n+1,n+5],[n+2,n+5],[n+3,n+5],[n+4,n+5],[n+5,n+6],...
       [n,n+6],[n+1,n+6],[n+2,n+6],[n+3,n+6],[n+6,n+7],...
       [n,n+7],[n+1,n+7],[n+2,n+7],[n+3,n+7],[n+7,n+8],...
       [n,n+8],[n+1,n+8],[n+2,n+8],[n+3,n+8],[n+8,n+9],...
       [n,n+9],[n+1,n+9],[n+2,n+9],[n+3,n+9],[n+9,n+10]}; %connection with the MEs

for i = size(link_data0,2)+1:(size(link_data0,2)+length(con))
    link_data(i).id = i;      %Id of the link
    link_data(i).material_ID = 1;
    link_data(i).geometry = 1;
    link_data(i).type_ID = 1;
    link_data(i).failure_ID = 1;
    link_data(i).k_mat = eye(6); %Dummy, identity matrix. Filled out below
    link_data(i).c_mat = eye(6); %Dummy
    link_data(i).rest = ones(1,6); %Dummy
    link_data(i).state = 1; % 1 is intact 0 is broken.
    link_data(i).ME_connect = con{i-size(link_data0,2)};
end

%Completes the link array with the geometries
set_link_properties2();
save('CubeSat1U_for_LOFT.mat','link_data')
pause
%}
load('CubeSat1U_for_LOFT.mat','link_data')
link_data0=link_data;
end

