function seeds_distribution_list()

% Available seeds distribution types
% Other seeds distribution types will be added in the future

global SEEDS_DISTRIBUTION;

field_id = 'id';
field_name = 'name';
field_param1 = 'param1';
field_param2 = 'param2';

id = {0,1,2,3,4};
name = {'RANDOM','RANDOM GAUSS','','LOG-SPIRAL','HOLLOW'};
param1 = {0,0,0,0,0};
param2 = {0,0,0,0,0};

SEEDS_DISTRIBUTION = struct(field_id, id, ...
                            field_name, name, ...
                            field_param1, param1, ...
                            field_param2, param2);

end
