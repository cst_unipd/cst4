function  [ME,FRAGMENTS,BUBBLE]=link_generate_ReDSHIFT(ii,jj,kk,ME,FRAGMENTS,BUBBLE)
%GENERATE_LOFT_LINK; in this file only address which element are connected and
% with which material
global link_data;
global link_data0;
%{
%-------------------------------------------------------------------------
%LINK INIATIALIZATION,

f_id = 'id';
f_con = 'ME_connect';   % The pair of ME connected by the link
f_mat = 'material_ID';  % 1=Al, 2=steel,
f_geom= 'geometry';
f_type= 'type_ID';      %1=beam, 2=surface...
f_fail= 'failure_ID';  % Type of faiulre (1=max elongation, 2= max strain...)
f_k   = 'k_mat';       % 6x6 matrix for stiffness response
f_c   = 'c_mat';       % 6x6 matrix for viscous response
f_rest = 'rest';     % Equilibrium configuration, when no forces are exchanged
f_state = 'state';   % 1= intact, 0=broken

% MEs ID
% ID = {1,2,3,4,... % 4 Lateral plates
%       5,6,... % % Upper hat & Lower hat
%       7,... % Central plate - equipment tray
%       8,9,10,11,12,... % 5 Boxes on the central plate (reaction wheels,XTU,IMU)
%       13,14,15,16,... % 4 Plates (inside) bracket
%       17,18,... % 2 Cylinders on central plate (star tracker)
%       19,... % Plate (base of propulsion system)
%       20,... % Plate (base of optical payload)
%       21,... % Plate (base of data and power module)
%       22,23,... % 2 Boxes on plates (propulsion + data and power module)
%       24,... % 1 Cylinder on plate (optical payload)
%       25}; % Antenna 

con = {[1,2],[1,4],[2,3],[3,4],...
       [1,5],[1,6],[2,5],[2,6],[3,5],[3,6],[4,5],[4,6]...
       [7,1],[7,2],[7,3],[7,4],...
       [7,8],[7,9],[7,10],[7,11],[7,12],...
       [7,13],[7,14],[7,15],[7,16],...
       [7,17],[7,18],[7,19],[7,20],[7,21],...
       [13,14],[13,16],[14,15],[15,16],...
       [19,22],[20,24],[21,23],[22,25]}; %connection with the MEs

for i=1:length(con)
    
    ID{i} = i;                          % Identification number of the link
    mat{i} = 1;                         % Material type for each link: 1 = Al, 2 = steel,
    geom{i} = 1;                        % Geometrical type of each link: 1 = dot, 2 = line, 3 = surface
    type{i} = 1;                        % Element type of each link: 1 = bolted, 2 = welded,
    val_fail{i} = 1;                    % Type of faiulre of the link: 1 = max elongation, 2 = max strain,
    val_k{i} = eye(6);                  % Inizialization of the matrix for stiffness response
    val_c{i} = eye(6);                  % Inizialization of the matrix for viscous response
    val_rest{i} = ones(1,6);            % Inizialization of the equilibrium configuration
    val_state{i} = 1;                   % Inizialization of the state of the link: 1 = intact, 0 = broken
    
end
link_data = struct(f_id,ID,f_con,con,f_mat,mat,f_geom,geom,f_type,type,f_fail,val_fail,f_state,val_state,f_k,val_k,f_c,val_c,f_rest,val_rest);

%Completes the link array with the geometries
set_link_properties();
save('Link_ReDSHIFT.mat','link_data')
%}
load('Link_ReDSHIFT.mat','link_data')
% for i=1:length(link_data)
%     link_data(i).state=0;
% end
link_data0=link_data;
end

