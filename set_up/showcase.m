%USE this to test the set up, it produces a picture at the end
% clear all
% close all

global ME;
global FRAGMENTS;
global BUBBLE;
% global sim_title_dir;
global sim_title;
% global caso

ME=population_empty_structure();
FRAGMENTS=population_empty_structure();
BUBBLE=population_empty_structure();

COLLISION_DATA_tracking=struct('target',{},'target_shape',{},'impactor',{},'impactor_shape',{},'point',{});

material_list();


switch caso
    case 1 
        sim_title='Sphere_vs_Simple_Plate';
        [ME] = target_simple_plate(1);
        [ME] = impactor_sphere1(2,ME);
    case 2 
        sim_title='Sphere_vs_Whipple_Shield';
        [ME] = target_whipple_shield(1);
        [ME] = impactor_sphere2(3,ME);
    case 3 
        sim_title='Sphere_vs_Multilayer_Target';
        [ME] = target_multi_layer(1);
        [ME] = impactor_sphere3(3,ME);
    case 4 
        sim_title='Oblique_Sphere_vs_Simple_Plate';
        [ME] = target_simple_plate(1);
        [ME] = impactor_sphere4(2,ME);
    case 5 
        sim_title='Oblique_Sphere_vs_Whipple_Shield';
        [ME] = target_whipple_shield(1);
        [ME] = impactor_sphere5(3,ME);
    case 6 
        sim_title='Oblique_Sphere_vs_Multilayer_Target';
        [ME] = target_multi_layer(1);
        [ME] = impactor_sphere6(4,ME);
        
end


sim_title_dir=[sim_title,'_',datestr(now,30)];
sim_title_dir=fullfile('Results',sim_title_dir);
mkdir(sim_title_dir);
mkdir(fullfile(sim_title_dir,'figures'));
mkdir(fullfile(sim_title_dir,'data'));
global ME_SR;
ME_SR=ME;
draw3D();
pause(0.01)
%close all
