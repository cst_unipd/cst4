
global ME;
global FRAGMENTS;
global BUBBLE;
global sim_title_dir;
global sim_title;


%--------------------
% % %LOFT
% sim_title='LOFT';
% % Generate populations and collision data
% [ME, FRAGMENTS, BUBBLE, COLLISION_DATA_tracking] = generate_LOFT_structures;
% % Generate Links
%  generate_LOFT_link();

%generate_fragment_impactor(); % used to simulate a first collision
%--------------------
% %sphere vs simple plate
% sim_title='sphere_vs_Simple_plate';
% [ME, FRAGMENTS, BUBBLE, COLLISION_DATA_tracking] = generate_ME_simple_plate;
%  gene rate_fragment_sphere(); % used to simulate a first collision

%--------------------
% % %Prova per debugging
% sim_title='struct_tot_prova';
% [ME, FRAGMENTS, BUBBLE, COLLISION_DATA_tracking] =generate_struct_tot_prova;


%--------------
% casi da Test_case

%caso=28;
if ~isnumeric(caso) || caso<1
    caso=55;
end

ME=population_empty_structure();
FRAGMENTS=population_empty_structure();
BUBBLE=population_empty_structure();


COLLISION_DATA_tracking=struct('target',{},'target_shape',{},'impactor',{},'impactor_shape',{},'point',{});

switch caso
    case 1 % sphere on sphere
        sim_title='Sphere_vs_Sphere';
        [ME,FRAGMENTS,BUBBLE] = generate_ME_sphere(1,0,0,ME,FRAGMENTS,BUBBLE);
        [ME,FRAGMENTS,BUBBLE] = generate_ME_sphere_imp(2,0,0,ME,FRAGMENTS,BUBBLE);
    case 2 % sphere impacting on simple plate
        sim_title='Sphere_vs_Simple_plate';
        [ME,FRAGMENTS,BUBBLE] = generate_ME_simple_plate1(1,0,0,ME,FRAGMENTS,BUBBLE);
        [ME,FRAGMENTS,BUBBLE] = generate_ME_sphere_imp(2,0,0,ME,FRAGMENTS,BUBBLE);
    case 3 % sphere impacting on a edge of simple plate
        sim_title='Sphere_vs_Thickness_simple_plate';
        [ME,FRAGMENTS,BUBBLE] = generate_ME_simple_plate2(1,0,0,ME,FRAGMENTS,BUBBLE);
        [ME,FRAGMENTS,BUBBLE] = generate_ME_sphere_imp(2,0,0,ME,FRAGMENTS,BUBBLE);
    case 4 % sphere impacting on a corner of a simple plate
        sim_title='Sphere_vs_Edge_simple_plate';
        [ME,FRAGMENTS,BUBBLE] = generate_ME_simple_plate3(1,0,0,ME,FRAGMENTS,BUBBLE);
        [ME,FRAGMENTS,BUBBLE] = generate_ME_sphere_imp(2,0,0,ME,FRAGMENTS,BUBBLE);
    case 5 % sphere impacting on side cylinder
        sim_title='Sphere_vs_Base_cylinder';
        [ME,FRAGMENTS,BUBBLE] = generate_ME_cylinder2(1,0,0,ME,FRAGMENTS,BUBBLE);
        [ME,FRAGMENTS,BUBBLE] = generate_ME_sphere_imp(2,0,0,ME,FRAGMENTS,BUBBLE);
    case 6 % sphere impacting on side cylinder
        sim_title='Sphere_vs_Side_cylinder';
        [ME,FRAGMENTS,BUBBLE] = generate_ME_cylinder1(1,0,0,ME,FRAGMENTS,BUBBLE);
        [ME,FRAGMENTS,BUBBLE] = generate_ME_sphere_imp(2,0,0,ME,FRAGMENTS,BUBBLE);
    case 7 % simple plate impacting on simple plate
        sim_title='Simple_plate_vs_Simple_plate';
        [ME,FRAGMENTS,BUBBLE] = generate_ME_simple_plate1(1,0,0,ME,FRAGMENTS,BUBBLE);
        [ME,FRAGMENTS,BUBBLE] = generate_ME_simple_plate_imp1(2,0,0,ME,FRAGMENTS,BUBBLE);
    case 8 % simple plate impacting on edge of a simple plate
        sim_title='Simple_plate_vs_Thickness_simple_plate';
        [ME,FRAGMENTS,BUBBLE] = generate_ME_simple_plate1(1,0,0,ME,FRAGMENTS,BUBBLE);
        [ME,FRAGMENTS,BUBBLE] = generate_ME_simple_plate_imp2(2,0,0,ME,FRAGMENTS,BUBBLE);
    case 9 % simple plate impacting on corner of a simple plate
        sim_title='Simple_plate_vs_Edge_simple_plate';
        [ME,FRAGMENTS,BUBBLE] = generate_ME_simple_plate1(1,0,0,ME,FRAGMENTS,BUBBLE);
        [ME,FRAGMENTS,BUBBLE] = generate_ME_simple_plate_imp3(2,0,0,ME,FRAGMENTS,BUBBLE);
    case 10 % cylinder impacting on simple plate
        sim_title='Base_cylinder_vs_Simple_plate';
        [ME,FRAGMENTS,BUBBLE] = generate_ME_simple_plate1(1,0,0,ME,FRAGMENTS,BUBBLE);
        [ME,FRAGMENTS,BUBBLE] = generate_ME_cylinder_imp1(2,0,0,ME,FRAGMENTS,BUBBLE);
    case 11 % edge cylinder impacting on simple plate
        sim_title='Edge_cylinder_vs_Simple_plate';
        [ME,FRAGMENTS,BUBBLE] = generate_ME_simple_plate1(1,0,0,ME,FRAGMENTS,BUBBLE);
        [ME,FRAGMENTS,BUBBLE] = generate_ME_cylinder_imp3(2,0,0,ME,FRAGMENTS,BUBBLE);
    case 12 % side cylinder impacting on simple plate
        sim_title='Side_cylinder_vs_Simple_plate';
        [ME,FRAGMENTS,BUBBLE] = generate_ME_simple_plate1(1,0,0,ME,FRAGMENTS,BUBBLE);
        [ME,FRAGMENTS,BUBBLE] = generate_ME_cylinder_imp2(2,0,0,ME,FRAGMENTS,BUBBLE);
    case 13 % sphere impacting on two consecutive plates
        sim_title='Sphere_vs_Two_plates';
        [ME,FRAGMENTS,BUBBLE] = generate_ME_two_plates(1,0,0,ME,FRAGMENTS,BUBBLE);
        [ME,FRAGMENTS,BUBBLE] = generate_ME_sphere_imp(3,0,0,ME,FRAGMENTS,BUBBLE);
    case 14 % sphere impacting on hollow sphere
        sim_title='Sphere_vs_Hollow_sphere';
        [ME,FRAGMENTS,BUBBLE] = generate_ME_hollow_sphere(1,0,0,ME,FRAGMENTS,BUBBLE);
        [ME,FRAGMENTS,BUBBLE] = generate_ME_sphere_imp(2,0,0,ME,FRAGMENTS,BUBBLE);
    case 15 % sphere impacting on base hollow cylinder
        sim_title='Sphere_vs_Base_hollow_cylinder';
        [ME,FRAGMENTS,BUBBLE] = generate_ME_hollow_cylinder2(1,0,0,ME,FRAGMENTS,BUBBLE);
        [ME,FRAGMENTS,BUBBLE] = generate_ME_sphere_imp(2,0,0,ME,FRAGMENTS,BUBBLE);
    case 16 % sphere impacting on side hollow cylinder
        sim_title='Sphere_vs_side_hollow_cylinder';
        [ME,FRAGMENTS,BUBBLE] = generate_ME_hollow_cylinder1(1,0,0,ME,FRAGMENTS,BUBBLE);
        [ME,FRAGMENTS,BUBBLE] = generate_ME_sphere_imp(2,0,0,ME,FRAGMENTS,BUBBLE);
    case 17 % sphere impacting on EXPLOSIVE hollow cylinder
        sim_title='Sphere_vs_explosive_side_hollow_cylinder';
        [ME,FRAGMENTS,BUBBLE] = generate_ME_explosive_hollow_cylinder(1,0,0,ME,FRAGMENTS,BUBBLE);
        [ME,FRAGMENTS,BUBBLE] = generate_ME_sphere_imp(2,0,0,ME,FRAGMENTS,BUBBLE);
    case 18 % linked structure - small ball
        sim_title='Sphere_vs_linked_structure';
        [ME,FRAGMENTS,BUBBLE]=generate_ME_linked_structure(1,0,0,ME,FRAGMENTS,BUBBLE);
        [ME,FRAGMENTS,BUBBLE]=link_generate_4linked_structure(0,0,0,ME,FRAGMENTS,BUBBLE);
        [ME,FRAGMENTS,BUBBLE] = generate_ME_sphere_imp(5,0,0,ME,FRAGMENTS,BUBBLE);
    case 19 % linked structure2 - bigger sphere
        sim_title='Bigger_Sphere_vs_linked_sturcture';
        [ME,FRAGMENTS,BUBBLE]=generate_ME_linked_structure(1,0,0,ME,FRAGMENTS,BUBBLE);
        [ME,FRAGMENTS,BUBBLE]=link_generate_4linked_structure(0,0,0,ME,FRAGMENTS,BUBBLE);
        [ME,FRAGMENTS,BUBBLE]=generate_ME_sphere_imp_40cm(length(ME)+1,0,0,ME,FRAGMENTS,BUBBLE);
    case 20 % sphere impacting on long simple plate, to verify cut
        sim_title='Sphere_vs_Slab';
        [ME,FRAGMENTS,BUBBLE] = generate_ME_long_simple_plate(1,0,0,ME,FRAGMENTS,BUBBLE);
        [ME,FRAGMENTS,BUBBLE] = generate_ME_sphere_imp(2,0,0,ME,FRAGMENTS,BUBBLE);
    case 21 % Bubble impacting on two consecutive plates
        sim_title='Bubble_vs_Two_plates';
        [ME,FRAGMENTS,BUBBLE] = generate_ME_two_plates(1,0,0,ME,FRAGMENTS,BUBBLE);
        [ME,FRAGMENTS,BUBBLE] = generate_BUBBLE_sphere_imp(0,0,1,ME,FRAGMENTS,BUBBLE);
    case 22 % sphere impacting on 4 linked simple plate
        sim_title='Sphere_vs_FourLinkedPlates';
        [ME,FRAGMENTS,BUBBLE] = generate_ME_Four_Linked_Plates(1,0,0,ME,FRAGMENTS,BUBBLE);
        [ME,FRAGMENTS,BUBBLE] = link_generate_4linked_structure2(0,0,0,ME,FRAGMENTS,BUBBLE);
        [ME,FRAGMENTS,BUBBLE] = generate_ME_sphere4linkedstruct(5,0,0,ME,FRAGMENTS,BUBBLE);
    case 23 %Check if fragments go back
        sim_title='Ejecta_test';
        [ME,FRAGMENTS,BUBBLE] = generate_ME_Solid_Wall(1,0,0,ME,FRAGMENTS,BUBBLE);
        [ME,FRAGMENTS,BUBBLE] = generate_ME_6mmsphere(2,0,0,ME,FRAGMENTS,BUBBLE);
    case 24 % sphere impacting on explosive hollow sphere
        sim_title='Sphere_vs_Explosive_Hollow_sphere';
        [ME,FRAGMENTS,BUBBLE] = generate_ME_explosive_hollow_sphere(1,0,0,ME,FRAGMENTS,BUBBLE);
        [ME,FRAGMENTS,BUBBLE] = generate_ME_sphere_imp(2,0,0,ME,FRAGMENTS,BUBBLE);
    case 25 % sphere impacting on long simple plate, to verify cut
        sim_title='Double_spheres_vs_Slab';
        [ME,FRAGMENTS,BUBBLE] = generate_ME_long_simple_plate(1,0,0,ME,FRAGMENTS,BUBBLE);
        [ME,FRAGMENTS,BUBBLE] = generate_ME_double_sphere_imp(2,0,0,ME,FRAGMENTS,BUBBLE);
    case 26 % sphere impacting on side cylinder
        sim_title='Cylinder_outline';
        [ME,FRAGMENTS,BUBBLE] = generate_ME_cylinder2(1,0,0,ME,FRAGMENTS,BUBBLE);
        [ME,FRAGMENTS,BUBBLE] = generate_ME_6spheres(2,0,0,ME,FRAGMENTS,BUBBLE);
    case 27 % sphere impacting on side cylinder
        sim_title='Fragments_Vs_Cylinder';
        [ME,FRAGMENTS,BUBBLE] = generate_ME_cylinder2(1,0,0,ME,FRAGMENTS,BUBBLE);
        [ME,FRAGMENTS,BUBBLE]=generate_FRAGMENTS_6(0,1,0,ME,FRAGMENTS,BUBBLE);
    case 28 %testing more complex links
        sim_title='More_complex_links';
        [ME,FRAGMENTS,BUBBLE] = generate_ME_two_Linked_Plates(1,0,0,ME,FRAGMENTS,BUBBLE);
        [ME,FRAGMENTS,BUBBLE] = generate_link2linked_structure2(0,0,0,ME,FRAGMENTS,BUBBLE);
        [ME,FRAGMENTS,BUBBLE] = generate_ME_sphere_imp_2plate(3,0,0,ME,FRAGMENTS,BUBBLE);
    case 29 %LOFT model
        sim_title='Loft_model_vs_sphere';
        [ME,FRAGMENTS,BUBBLE]=generate_ME_LOFT(0,0,0,ME,FRAGMENTS,BUBBLE);
        [ME,FRAGMENTS,BUBBLE]=link_generate_LOFT(0,0,0,ME,FRAGMENTS,BUBBLE);
        [ME,FRAGMENTS,BUBBLE]=generate_ME_sphere_imp4_LOFT(length(ME)+1,0,0,ME,FRAGMENTS,BUBBLE);
    case 30 % sphere vs simple plate and explosive hollow cylinder
        sim_title='Sphere_vs_Simple_Plate_and_Explosive_Hollow_Cylinder';
        [ME,FRAGMENTS,BUBBLE] = generate_ME_simple_plate1(1,0,0,ME,FRAGMENTS,BUBBLE);
        [ME,FRAGMENTS,BUBBLE] = generate_ME_explosive_hollow_cylinder2(2,0,0,ME,FRAGMENTS,BUBBLE);
        [ME,FRAGMENTS,BUBBLE] = generate_ME_sphere_imp2(3,0,0,ME,FRAGMENTS,BUBBLE);
    case 31 % LOFT model with plates as central body
        sim_title='Panel_loft_model_vs_sphere';
        [ME,FRAGMENTS,BUBBLE]=generate_ME_LOFT_2(1,0,0,ME,FRAGMENTS,BUBBLE);
        [ME,FRAGMENTS,BUBBLE]=link_generate_LOFT_2(0,0,0,ME,FRAGMENTS,BUBBLE);
        [ME,FRAGMENTS,BUBBLE]=generate_ME_Al_sphere_LOFT(length(ME)+1,0,0,ME,FRAGMENTS,BUBBLE);
    case 32 % Cubesat with plates vs sphere
        sim_title='Cubesat_vs_sphere';
        [ME,FRAGMENTS,BUBBLE]=generate_ME_cubesat(1,0,0,ME,FRAGMENTS,BUBBLE);
        [ME,FRAGMENTS,BUBBLE]=link_generate_cubesat(0,0,0,ME,FRAGMENTS,BUBBLE);
        [ME,FRAGMENTS,BUBBLE]=generate_ME_sphere_imp_cubesat(length(ME)+1,0,0,ME,FRAGMENTS,BUBBLE);
    case 33 % Lan Satellite
        sim_title='Lan_Satellite';
        [ME,FRAGMENTS,BUBBLE] = generate_Lan(1,0,0,ME,FRAGMENTS,BUBBLE);
        [ME,FRAGMENTS,BUBBLE] = link_generate_Lan(0,0,0,ME,FRAGMENTS,BUBBLE);
        [ME,FRAGMENTS,BUBBLE] = generate_ME_cylinder_Lan(66,0,0,ME,FRAGMENTS,BUBBLE);
    case 34 % Hanada Microsat
        sim_title='Hanada_Microsat';
        [ME,FRAGMENTS,BUBBLE] = generate_Hanada(1,0,0,ME,FRAGMENTS,BUBBLE);
        [ME,FRAGMENTS,BUBBLE] = link_generate_Hanada(0,0,0,ME,FRAGMENTS,BUBBLE);
        [ME,FRAGMENTS,BUBBLE] = generate_ME_sphere_Hanada(30,0,0,ME,FRAGMENTS,BUBBLE);
    case 35 % LOFT cases D7
        [ME,FRAGMENTS,BUBBLE]=generate_ME_LOFT(1,0,0,ME,FRAGMENTS,BUBBLE);
%         [ME,FRAGMENTS,BUBBLE]=link_generate_LOFT_2(0,0,0,ME,FRAGMENTS,BUBBLE);
        length_MEloft=length(ME)+1;
        switch type
            case 'LOFT1' %impact with simple plate
                sim_title='LOFT1';
                [ME,FRAGMENTS,BUBBLE] = generate_ME_simple_plate_LOFT(length_MEloft,0,0,ME,FRAGMENTS,BUBBLE);
                [ME,FRAGMENTS,BUBBLE]=link_generate_LOFT(0,0,0,ME,FRAGMENTS,BUBBLE);
            case 'LOFT2' %impact with 1U CubeSat
                sim_title='LOFT2';
                [ME,FRAGMENTS,BUBBLE] = generate_ME_1U(length_MEloft,0,0,ME,FRAGMENTS,BUBBLE);
                [ME,FRAGMENTS,BUBBLE] = link_generate_1U(length_MEloft,0,0,ME,FRAGMENTS,BUBBLE);
            case 'LOFT3' %impact with 12U CubeSat
                sim_title='LOFT3';
                [ME,FRAGMENTS,BUBBLE] = generate_ME_12U(length_MEloft,0,0,ME,FRAGMENTS,BUBBLE);
                [ME,FRAGMENTS,BUBBLE] = link_generate_12U(length_MEloft,0,0,ME,FRAGMENTS,BUBBLE);
            case 'LOFT4' %impact with 12U CubeSat
                sim_title='LOFT4';
                [ME,FRAGMENTS,BUBBLE] = generate_ME_12U_LOFT4(length_MEloft,0,0,ME,FRAGMENTS,BUBBLE);
                [ME,FRAGMENTS,BUBBLE] = link_generate_12U(length_MEloft,0,0,ME,FRAGMENTS,BUBBLE);
            case 'LOFT5' %impact with 12U CubeSat 45� outside
                sim_title='LOFT5';
                [ME,FRAGMENTS,BUBBLE] = generate_ME_12U_LOFT5(length_MEloft,0,0,ME,FRAGMENTS,BUBBLE);
                [ME,FRAGMENTS,BUBBLE] = link_generate_12U(length_MEloft,0,0,ME,FRAGMENTS,BUBBLE);
            case 'LOFT6' %impact with 12U CubeSat 45� inside
                sim_title='LOFT6';
                [ME,FRAGMENTS,BUBBLE] = generate_ME_12U_LOFT6(length_MEloft,0,0,ME,FRAGMENTS,BUBBLE);
                [ME,FRAGMENTS,BUBBLE] = link_generate_12U(length_MEloft,0,0,ME,FRAGMENTS,BUBBLE);
            case 'LOFT7_AF' %impact with 48U CubeSat 45� inside
                sim_title='LOFT7_AF';
                [ME,FRAGMENTS,BUBBLE] = generate_ME_48U_LOFT7_AF(length_MEloft,0,0,ME,FRAGMENTS,BUBBLE);
                [ME,FRAGMENTS,BUBBLE] = link_generate_48U(length_MEloft,0,0,ME,FRAGMENTS,BUBBLE);
            case 'LOFT8_AF' %impact with 48U CubeSat 45� inside
                sim_title='LOFT8_AF';
                [ME,FRAGMENTS,BUBBLE] = generate_ME_48U_LOFT8_AF(length_MEloft,0,0,ME,FRAGMENTS,BUBBLE);
                [ME,FRAGMENTS,BUBBLE] = link_generate_48U(length_MEloft,0,0,ME,FRAGMENTS,BUBBLE);
            case 'LOFT8_MD' %impact with 48U CubeSat 45� inside
                sim_title='LOFT8_MD';
                [ME,FRAGMENTS,BUBBLE] = generate_ME_48U_LOFT8_MD(length_MEloft,0,0,ME,FRAGMENTS,BUBBLE);
                [ME,FRAGMENTS,BUBBLE] = link_generate_48U(length_MEloft,0,0,ME,FRAGMENTS,BUBBLE);
            otherwise
                error('invalid LOFT test case!');
        end
    
    case 36 % 3 spheres impacting on simple plate
        sim_title='Three_Spheres_vs_Simple_plate';
        [ME,FRAGMENTS,BUBBLE] = generate_ME_simple_plate_three_sphere(1,0,0,ME,FRAGMENTS,BUBBLE);
        [ME,FRAGMENTS,BUBBLE] = generate_ME_3sphere_imp(2,0,0,ME,FRAGMENTS,BUBBLE);
        
    case 37 % Advances Shields
        sim_title=['Advanced_Shields_' num2str(type_3) '_' num2str(type_1) '_' num2str(type_2)];
        switch type_1
            case 'Case1_a'
                project_diameter=0.002; %projectile diameter, [m]
                impact_angle=0; %impactor angle, [rad]
            case 'Case1_b'
                project_diameter=0.003; %projectile diameter, [m]
                impact_angle=0; %impactor angle, [rad]
            case 'Case1_c'
                project_diameter=0.004; %projectile diameter, [m]
                impact_angle=0; %impactor angle, [rad]
            case 'Case1_d'
                project_diameter=0.005; %projectile diameter, [m]
                impact_angle=0; %impactor angle, [rad]
                
            case 'Case2_a'
                project_diameter=0.002; %projectile diameter, [m]
                impact_angle=pi/4; %impactor angle, [rad]
            case 'Case2_b'
                project_diameter=0.003; %projectile diameter, [m]
                impact_angle=pi/4; %impactor angle, [rad]
            case 'Case2_c'
                project_diameter=0.004; %projectile diameter, [m]
                impact_angle=pi/4; %impactor angle, [rad]
            case 'Case2_d'
                project_diameter=0.005; %projectile diameter, [m]
                impact_angle=pi/4; %impactor angle, [rad]    
        end
        
        switch type_2
            case 'Al'
                mat_ID=1;
            case 'Nylon_6'
                mat_ID=10;
        end
        
        switch type_3
            case 'CASE1'
                [ME,FRAGMENTS,BUBBLE] = generate_ME_sphere_imp_adv_shield(1,0,0,ME,FRAGMENTS,BUBBLE,project_diameter,impact_angle,mat_ID);
                [ME,FRAGMENTS,BUBBLE] = generate_ME_advanced_shield_1(2,0,0,ME,FRAGMENTS,BUBBLE);
            case 'CASE2'
                [ME,FRAGMENTS,BUBBLE] = generate_ME_sphere_imp_adv_shield(1,0,0,ME,FRAGMENTS,BUBBLE,project_diameter,impact_angle,mat_ID);
                [ME,FRAGMENTS,BUBBLE] = generate_ME_advanced_shield_2(2,0,0,ME,FRAGMENTS,BUBBLE);
            case 'CASE3'
                [ME,FRAGMENTS,BUBBLE] = generate_ME_sphere_imp_adv_shield(1,0,0,ME,FRAGMENTS,BUBBLE,project_diameter,impact_angle,mat_ID);
                [ME,FRAGMENTS,BUBBLE] = generate_ME_advanced_shield_3(2,0,0,ME,FRAGMENTS,BUBBLE);
        end
        
    case 38 % RedSHIFT Satellite
        sim_title=['RedSHIFT_Satellite_' num2str(type)];
        switch type
            case 'Baseline'
                [ME,FRAGMENTS,BUBBLE] = generate_ReDSHIFT(1,0,0,ME,FRAGMENTS,BUBBLE);
                [ME,FRAGMENTS,BUBBLE] = link_generate_ReDSHIFT(0,0,0,ME,FRAGMENTS,BUBBLE);
                length_ME_RedSHIFT=length(ME)+1;
                [ME,FRAGMENTS,BUBBLE] = generate_ME_sphere_ReDSHIFT(length_ME_RedSHIFT,0,0,ME,FRAGMENTS,BUBBLE);
            case 'Weak_struct'
                [ME,FRAGMENTS,BUBBLE] = generate_ReDSHIFT_weak(1,0,0,ME,FRAGMENTS,BUBBLE);
                [ME,FRAGMENTS,BUBBLE] = link_generate_ReDSHIFT_weak(0,0,0,ME,FRAGMENTS,BUBBLE);
                length_ME_RedSHIFT=length(ME)+1;
                [ME,FRAGMENTS,BUBBLE] = generate_ME_sphere_ReDSHIFT(length_ME_RedSHIFT,0,0,ME,FRAGMENTS,BUBBLE);
            case 'c_mat'
                [ME,FRAGMENTS,BUBBLE] = generate_ReDSHIFT_weak(1,0,0,ME,FRAGMENTS,BUBBLE);
                [ME,FRAGMENTS,BUBBLE] = link_generate_ReDSHIFT_cmat(0,0,0,ME,FRAGMENTS,BUBBLE);
                length_ME_RedSHIFT=length(ME)+1;
                [ME,FRAGMENTS,BUBBLE] = generate_ME_sphere_ReDSHIFT(length_ME_RedSHIFT,0,0,ME,FRAGMENTS,BUBBLE);
        end
        
    case 40
        %{SW
        global nishida_test_case;
        global projectile_diameter;
        global impact_velocity;
        global spiral_param_vector;
        
        nishida_test_case = 7;
        sim_title = ['Nishida test case N. ' num2str(nishida_test_case)];
        
        switch nishida_test_case
            %             case 1
            %                 projectile_diameter = 7.0e-3;
            %                 impact_velocity = 2.01e3;
            case 2
                projectile_diameter = 3.2e-3;
                impact_velocity = 1.91e3;
            case 3
                projectile_diameter = 1.6e-3;
                impact_velocity = 1.85e3;
                %             case 4
                %                 projectile_diameter = 7.0e-3;
                %                 impact_velocity = 2.92e3;
            case 5
                projectile_diameter = 3.2e-3;
                impact_velocity = 3.01e3;
            case 6
                projectile_diameter = 1.6e-3;
                impact_velocity = 3.09e3;
            case 7
                projectile_diameter = 3.2e-3;
                impact_velocity = 4.14e3;
            case 8
                projectile_diameter = 1.6e-3;
                impact_velocity = 4.06e3;
                %             case 9
                %                 projectile_diameter = 3.2e-3;
                %                 impact_velocity = 6.62e3;
                %             case 10
                %                 projectile_diameter = 1.6e-3;
                %                 impact_velocity = 6.75e3;
            case 11
                projectile_diameter = 1.6e-3;
                impact_velocity = 2.50e3;
            case 12
                projectile_diameter = 1.6e-3;
                impact_velocity = 3.50e3;
            case 13
                projectile_diameter = 2.4e-3;
                impact_velocity = 2.00e3;
            case 14
                projectile_diameter = 2.4e-3;
                impact_velocity = 2.50e3;
            case 15
                projectile_diameter = 2.4e-3;
                impact_velocity = 3.00e3;
            case 16
                projectile_diameter = 2.4e-3;
                impact_velocity = 3.50e3;
            case 17
                projectile_diameter = 2.4e-3;
                impact_velocity = 4.00e3;
            case 18
                projectile_diameter = 3.2e-3;
                impact_velocity = 2.50e3;
            case 19
                projectile_diameter = 3.2e-3;
                impact_velocity = 3.50e3;
            case 20
                projectile_diameter = 1.8e-3;
                impact_velocity = 3.20e3;
            case 21
                projectile_diameter = 1.6e-3;
                impact_velocity = 3.20e3;
            otherwise
                error('invalid Nishida test case!');
        end
        
        GA_RESULTS_200 = [  2  3.2 1.91 1.141838 1.552032 1.136991 0.838965; ...
            3  1.6 1.85 1.755484 1.558192 1.272294 0.827922; ...
            5  3.2 3.01 1.136746 1.448108 1.029720 0.739014; ...
            6  1.6 3.09 1.499650 1.056789 1.325117 0.814322; ...
            7  3.2 4.14 1.455030 1.320553 1.027727 0.649057; ...
            8  1.6 4.06 1.334137 1.291440 1.253838 0.757464; ...
            11 1.6 2.50 1.035417 1.211411 1.224158 0.790349; ...
            12 1.6 3.50 1.080683 1.462822 1.183471 0.801643; ...
            13 2.4 2.00 1.122807 1.466516 1.282118 0.837401; ...
            14 2.4 2.50 1.334955 1.495162 1.264006 0.843588; ...
            15 2.4 3.00 1.032349 1.518564 1.041850 0.800538; ...
            16 2.4 3.50 1.071921 1.397354 1.028603 0.780449; ...
            17 2.4 4.00 1.071255 1.483894 1.033837 0.683386; ...
            18 3.2 2.50 1.000816 1.397753 1.018520 0.811151; ...
            19 3.2 3.50 1.000000 1.559633 1.019044 0.837899; ...
            20 1.8 3.20 1.338546 1.509621 1.310013 0.760632; ...
            21 1.6 3.20 1.043470 1.473874 1.173945 0.824401];
        
        for i=1:size(GA_RESULTS_200,1)
            if nishida_test_case == GA_RESULTS_200(i,1)
                spiral_param_vector = GA_RESULTS_200(i,4:7);
            end
        end
        
        [ME,FRAGMENTS,BUBBLE] = generate_ME_Nishida_target(1,0,0,ME,FRAGMENTS,BUBBLE);
        [ME,FRAGMENTS,BUBBLE] = generate_ME_Nishida_impactor(2,0,0,ME,FRAGMENTS,BUBBLE);
        %}
        
    case 41
        sim_title='Nishida_first_complete_test';
        [ME,FRAGMENTS,BUBBLE] = generate_ME_simple_plate_Nishida1(1,0,0,ME,FRAGMENTS,BUBBLE);
        [ME,FRAGMENTS,BUBBLE] = generate_ME_sphere_imp_Nishida1(2,0,0,ME,FRAGMENTS,BUBBLE);
    case 42
        sim_title='Nishida_second_complete_test';
        [ME,FRAGMENTS,BUBBLE] = generate_ME_simple_plate_Nishida1(1,0,0,ME,FRAGMENTS,BUBBLE);
        [ME,FRAGMENTS,BUBBLE] = generate_ME_simple_plate2_Nishida1(2,0,0,ME,FRAGMENTS,BUBBLE);
        [ME,FRAGMENTS,BUBBLE] = generate_ME_sphere_imp_Nishida1(3,0,0,ME,FRAGMENTS,BUBBLE);
        
    case 50 % whipple shield
        sim_title='WhippleShield1';
        %Quick set up
        bumper_dimensions = [0.2,0.2,0.0012]; %[x,y,thickness] [m]
        wall_dimensions   = [0.2,0.2,0.0012]; %[x,y,thickness] [m]
        standoff = 0.1;            %[m]
        projectile_diameter = 6.2e-3; % [m]
        
        %In depth settings
        bumper_position=[0;0;0]; %[m]
        wall_position=bumper_position+[0;0;standoff]; %[m]
        projectile_position = [0,0,-0.01];
        projectile_velocity = 1e3*[0 0 4];
        
        [ME,FRAGMENTS,BUBBLE] = generate_ME_simple_plate_WhippleShield(1,0,0,ME,FRAGMENTS,BUBBLE,bumper_dimensions,bumper_position);
        [ME,FRAGMENTS,BUBBLE] = generate_ME_simple_plate_WhippleShield(2,0,0,ME,FRAGMENTS,BUBBLE,wall_dimensions,wall_position);
        [ME,FRAGMENTS,BUBBLE] = generate_ME_sphere_imp_WhippleShield(3,0,0,ME,FRAGMENTS,BUBBLE,projectile_diameter,projectile_position,projectile_velocity);
        
end

if caso >=51 && caso<100
    sim_title = ['WhippleShield_' num2str(caso)];
    bumper_dimensions = [0.2,0.2,0.0012]; %[x,y,thickness] [m]
    wall_dimensions   = [0.2,0.2,0.0012]; %[x,y,thickness] [m]
    standoff = 0.1;            %[m]
    bumper_position=[0;0;0]; %[m]
    wall_position=bumper_position+[0;0;standoff]; %[m]
    projectile_position = [0,0,-0.01];
    switch caso
        case 51
            projectile_diameter = 1.367e-3; % [m]
            projectile_velocity = 1e3*[0 0 2.2];
        case 52
            projectile_diameter = 0.99e-3; % [m]
            projectile_velocity = 1e3*[0 0 3];
        case 53
            projectile_diameter = 1.406e-3; % [m]
            projectile_velocity = 1e3*[0 0 4];
        case 54
            projectile_diameter = 1.738e-3; % [m]
            projectile_velocity = 1e3*[0 0 4.8];
        case 55
            projectile_diameter = 2.867e-3; % [m]
            projectile_velocity = 1e3*[0 0 2.2];
        case 56
            projectile_diameter = 2.49e-3; % [m]
            projectile_velocity = 1e3*[0 0 3];
        case 57
            projectile_diameter = 2.906e-3; % [m]
            projectile_velocity = 1e3*[0 0 4];
        case 58
            projectile_diameter = 3.238e-3; % [m]
            projectile_velocity = 1e3*[0 0 4.8];
        case 59
            projectile_diameter = 4.0e-3; % [m]
            projectile_velocity = 1e3*[0 0 6.5];
        case 60
            projectile_diameter = 3.5e-3; % [m]
            projectile_velocity = 1e3*[0 0 8];
        case 61
            projectile_diameter = 3.238e-3; % [m]
            projectile_velocity = 1e3*[0 0 9];
        case 62
            projectile_diameter = 3.0e-3; % [m]
            projectile_velocity = 1e3*[0 0 10];
        case 63
            projectile_diameter = 2e-3; % [m]
            projectile_velocity = 1e3*[0 0 6.5];
        case 64
            projectile_diameter = 2.0e-3; % [m]
            projectile_velocity = 1e3*[0 0 8];
        case 65
            projectile_diameter = 1.9e-3; % [m]
            projectile_velocity = 1e3*[0 0 9];
        case 66
            projectile_diameter = 1.5e-3; % [m]
            projectile_velocity = 1e3*[0 0 10];
        case 67
            projectile_diameter = 1.5e-3; % [m]
            projectile_velocity = 1e3*[0 0 6.5];
        case 68
            projectile_diameter = 1.5e-3; % [m]
            projectile_velocity = 1e3*[0 0 8];
        case 69
            projectile_diameter = 1.6e-3; % [m]
            projectile_velocity = 1e3*[0 0 9];
        case 70
            projectile_diameter = 3e-3; % [m]
            projectile_velocity = 1e3*[0 0 2.2];
        case 71
            projectile_diameter = 3e-3; % [m]
            projectile_velocity = 1e3*[0 0 3];
        case 72
            projectile_diameter = 0.8e-3; % [m]
            projectile_velocity = 1e3*[0 0 2.2];
        case 73
            projectile_diameter = 0.5e-3; % [m]
            projectile_velocity = 1e3*[0 0 3];
        case 74
            projectile_diameter = 0.8e-3; % [m]
            projectile_velocity = 1e3*[0 0 4];
        case 75
            projectile_diameter = 1.0e-3; % [m]
            projectile_velocity = 1e3*[0 0 4.8];
        case 76
            projectile_diameter = 1.0e-3; % [m]
            projectile_velocity = 1e3*[0 0 10];
        case 77
            projectile_diameter = 1.1e-3; % [m]
            projectile_velocity = 1e3*[0 0 9];
        case 78
            projectile_diameter = 2e-3; % [m]
            projectile_velocity = 1e3*[0 0 2.2];
        case 79
            projectile_diameter = 1.5e-3; % [m]
            projectile_velocity = 1e3*[0 0 3];
        case 80
            projectile_diameter = 2e-3; % [m]
            projectile_velocity = 1e3*[0 0 4];
        case 81
            projectile_diameter = 2e-3; % [m]
            projectile_velocity = 1e3*[0 0 10];
        case 82
            projectile_diameter = 0.5e-3; % [m]
            projectile_velocity = 1e3*[0 0 6];
        case 83
            projectile_diameter = 0.5e-3; % [m]
            projectile_velocity = 1e3*[0 0 8];
        case 84
            projectile_diameter = 0.5e-3; % [m]
            projectile_velocity = 1e3*[0 0 10];
        case 85
            projectile_diameter = 1e-3; % [m]
            projectile_velocity = 1e3*[0 0 6];
        case 86
            projectile_diameter = 0.25e-3; % [m]
            projectile_velocity = 1e3*[0 0 8];
        case 87
            projectile_diameter = 0.5e-3; % [m]
            projectile_velocity = 1e3*[0 0 5];
        case 88
            projectile_diameter = 1e-3; % [m]
            projectile_velocity = 1e3*[0 0 5];
        case 89
            projectile_diameter = 0.5e-3; % [m]
            projectile_velocity = 1e3*[0 0 7];
        case 90
            projectile_diameter = 1e-3; % [m]
            projectile_velocity = 1e3*[0 0 7];
        case 91
            projectile_diameter = 1.5e-3; % [m]
            projectile_velocity = 1e3*[0 0 5];
        case 92
            projectile_diameter = 1.5e-3; % [m]
            projectile_velocity = 1e3*[0 0 3];
        case 93
            projectile_diameter = 2e-3; % [m]
            projectile_velocity = 1e3*[0 0 3];
        case 94
            projectile_diameter = 2e-3; % [m]
            projectile_velocity = 1e3*[0 0 2];
        case 95
            projectile_diameter = 1.8e-3; % [m]
            projectile_velocity = 1e3*[0 0 4];
        case 96
            projectile_diameter = 1.25e-3; % [m]
            projectile_velocity = 1e3*[0 0 5];
        case 97
            projectile_diameter = 1.5e-3; % [m]
            projectile_velocity = 1e3*[0 0 9];
    end
    [ME,FRAGMENTS,BUBBLE] = generate_ME_simple_plate_WhippleShield(1,0,0,ME,FRAGMENTS,BUBBLE,bumper_dimensions,bumper_position);
    [ME,FRAGMENTS,BUBBLE] = generate_ME_simple_plate_WhippleShield(2,0,0,ME,FRAGMENTS,BUBBLE,wall_dimensions,wall_position);
    [ME,FRAGMENTS,BUBBLE] = generate_ME_sphere_imp_WhippleShield(3,0,0,ME,FRAGMENTS,BUBBLE,projectile_diameter,projectile_position,projectile_velocity);
    %             %prova
    %             ME(2).FRAGMENTATION_DATA.threshold0=15000;
    %             ME(2).FRAGMENTATION_DATA.threshold=ME(2).FRAGMENTATION_DATA.threshold0;
    %             %
    
end

if caso==100
    Sphere_Vs_Whipple_Shield
end
%{
% caso=7;
%
%
% ME=population_empty_structure();
% FRAGMENTS=population_empty_structure();
% BUBBLE=population_empty_structure();
%
%
% COLLISION_DATA_tracking=struct('target',{},'target_shape',{},'impactor',{},'impactor_shape',{},'point',{});
%     switch caso
%         case 1 % sphere impacting on simple plate
%             sim_title='Sphere_vs_Simple_plate';
%             [ME] = generate_ME_simple_plate1(1);
%             [ME] = generate_ME_sphere_imp(2,ME);
%         case 2 % cylinder impacting on simple plate
%             sim_title='Base_cylinder_vs_Simple_plate';
%             [ME] = generate_ME_simple_plate1(1);
%             [ME] = generate_ME_cylinder_imp1(2,ME);
%         case 3 % simple plate impacting on simple plate
%             sim_title='Simple_plate_vs_Simple_plate';
%             [ME] = generate_ME_simple_plate1(1);
%             [ME] = generate_ME_simple_plate_imp1(2,ME);
%         case 4 % sphere impacting on hollow sphere
%             sim_title='Sphere_vs_Hollow_sphere';
%             [ME] = generate_ME_hollow_sphere(1);
%             [ME] = generate_ME_sphere_imp(2,ME);
%         case 5 % sphere impacting on side cylinder
%             sim_title='Sphere_vs_Side_cylinder';
%             [ME] = generate_ME_cylinder1(1);
%             [ME] = generate_ME_sphere_imp(2,ME);
%         case 6 % sphere impacting on simple plate
%             sim_title='Sphere_vs_Sphere';
%             [ME] = generate_ME_sphere(1);
%             [ME] = generate_ME_sphere_imp(2,ME);
%         case 7 % sphere impacting on side hollow cylinder
%             sim_title='Sphere_vs_side_hollow_cylinder';
%             [ME] = generate_ME_hollow_cylinder1(1);
%             [ME] = generate_ME_sphere_imp(2,ME);
%         case 8 % sphere impacting on two consecutive plates
%             sim_title='Sphere_vs_Two_plates';
%             [ME] = generate_ME_two_plates(1);
%             [ME] = generate_ME_sphere_imp(3,ME);
%         case 9 % sphere impacting on a thickness face of a simple plate
%             sim_title='Sphere_vs_Thickness_simple_plate';
%             [ME] = generate_ME_simple_plate2(1);
%             [ME] = generate_ME_sphere_imp(2,ME);
%         case 10 % sphere impacting on a edge of a simple plate
%             sim_title='Sphere_vs_Edge_simple_plate';
%             [ME] = generate_ME_simple_plate3(1);
%             [ME] = generate_ME_sphere_imp(2,ME);
%         case 11 % side cylinder impacting on simple plate
%             sim_title='Side_cylinder_vs_Simple_plate';
%             [ME] = generate_ME_simple_plate1(1);
%             [ME] = generate_ME_cylinder_imp2(2,ME);
%         case 12 % simple plate impacting on thickness face of a simple plate
%             sim_title='Simple_plate_vs_Thickness_simple_plate';
%             [ME] = generate_ME_simple_plate1(1);
%             [ME] = generate_ME_simple_plate_imp2(2,ME);
%         case 13 % simple plate impacting on edge of a simple plate
%             sim_title='Simple_plate_vs_Edge_simple_plate';
%             [ME] = generate_ME_simple_plate1(1);
%             [ME] = generate_ME_simple_plate_imp3(2,ME);
%         case 14 % edge cylinder impacting on simple plate
%             sim_title='Edge_cylinder_vs_Simple_plate';
%             [ME] = generate_ME_simple_plate1(1);
%             [ME] = generate_ME_cylinder_imp3(2,ME);
%         case 15 % sphere impacting on side sylinder
%             sim_title='Sphere_vs_Side_cylinder';
%             [ME] = generate_ME_cylinder2(1);
%             [ME] = generate_ME_sphere_imp(2,ME);
%         case 16 % sphere impacting on base hollow cylinder
%             sim_title='Sphere_vs_Base_hollow_cylinder';
%             [ME] = generate_ME_hollow_cylinder2(1);
%             [ME] = generate_ME_sphere_imp(2,ME);
%     end
%
%}
% 

sim_title_dir=[sim_title,'_',datestr(now,30)];
sim_title_dir=fullfile('Results',sim_title_dir);
mkdir(sim_title_dir);
mkdir(fullfile(sim_title_dir,'data'));
mkdir(fullfile(sim_title_dir,'figures'));

% global ME_SR;
% ME_SR=ME;
% draw3D();
% pause

% mm=0;
% for i=1:1:length(ME)
%     mm=mm+ME(i).GEOMETRY_DATA.mass0;
% end
% m_tot=mm
% EMR=((ME(length(ME)).GEOMETRY_DATA.mass0*0.5*5000^2)/mm)/1000
% pause