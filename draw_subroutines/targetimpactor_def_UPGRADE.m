%> @file  targetimpactor_def_UPGRADE.m
%> @brief definition of initial target and impactor(s) masses
%> Copyright (c) 2018 CISAS - UNIVERSITY OF PADOVA - Dr L. Olivieri
function [mp,vp,mt,vt,FL]=targetimpactor_def_UPGRADE(input_folder)

d=fullfile(input_folder,'step_*.mat');
n = length(dir(d))-1; % number of CST_DATA files
FL=1;

name=fullfile(input_folder,'F_DATA_');
in_file=[name num2str(0)];

i1=0;
while exist([in_file '.mat'],'file')==0 && i1<=(n+2) % stop at a step more than the max number of F_DATA
    i1=i1+1;
    in_file=[name num2str(i1)];
    % Check if F_DATA_i is empty, i.e. if the structure F_DATA is loaded
    if exist([in_file '.mat'],'file')==2
        load(in_file);
        if exist('C_DATA','var')==0
            in_file=[name num2str(0) '.mat'];
        else
            break
        end
    end
end

if exist('C_DATA','var')==0
    % No data on fragmentation
    mp=0;
    vp=0;
    mt=0;
    vt=0;
    FL=0;
elseif isempty(C_DATA)==true
    mp=0;
    vp=0;
    mt=0;
    vt=0;
    FL=0;
else
    n1=length(C_DATA);
    mv=zeros(n1,4);
    for i1=1:n1
        % MASS IMPACTOR
        mv(i1,1)=C_DATA(i1).m_TOT - C_DATA(i1).TARGET.mass;
        % IMPACT VELOCITY
        if mv(i1,1)~=0
            mv(i1,2)=norm((C_DATA(i1).Q_TOT - C_DATA(i1).TARGET.Q))./mv(i1,1);
        else
            mv(i1,2)=0;
        end
        % TARGET mass
        mv(i1,3)=C_DATA(i1).TARGET.mass;
        % TARGET velocity
        mv(i1,4)=norm(C_DATA(i1).TARGET.vel);
        % FRAGMENTATION LEVEL
        FL=min([FL C_DATA(i1).F_L]);
    end
    % delete conditions not acceptable
    for i2=n1:-1:1
        if mv(i2,1)==0 || mv(i2,2)==0 
            mv(i2,:)=[];
        end
    end
    [mp,ip]=min(mv(:,1)); % min mass
    vp=mv(ip,2);
    mt=mv(ip,3);
    vt=mv(ip,4);
    if isempty(mp)==true || isempty(mt)==true % patch del perch� sono nulli
        mp = 0;
        mt = 0;
    end
end
