%> @file  PLOT1_geometry_parameters.m
%> @brief calculation of plots parameters for post processing of CST data
%> Copyright (c) 2017 CISAS - UNIVERSITY OF PADOVA - Dr L. Olivieri

function [C,dim,az,el,V_scale] = PLOT1_geometry_parameters_MD(ME,FRAGMENTS,ME_surv)

% center of figure
C=[0;0;0]; % CENTER
Q=[0;0;0]; % MOMENTUM
dim=0.5;  % size of figure
k=0;

C=C+ME(ME_surv).DYNAMICS_DATA.cm_coord;
Q=Q+ME(ME_surv).DYNAMICS_DATA.vel*ME(ME_surv).GEOMETRY_DATA.mass;

for j=1:length(FRAGMENTS)
    if FRAGMENTS(j).object_ID==ME_surv
        if FRAGMENTS(j).GEOMETRY_DATA.mass>0
            C=C+FRAGMENTS(j).DYNAMICS_DATA.cm_coord;
            k=k+1;
            Q=Q+FRAGMENTS(j).DYNAMICS_DATA.vel*FRAGMENTS(j).GEOMETRY_DATA.mass;
        end
    end
end
if k~=0
    C=C/k;
end

if ME(ME_surv).GEOMETRY_DATA.mass>0
    dim=max([dim  max(ME(ME_surv).GEOMETRY_DATA.dimensions)  max(C-ME(ME_surv).DYNAMICS_DATA.cm_coord)    ]);
end

for j=1:length(FRAGMENTS)
    if FRAGMENTS(j).object_ID==ME_surv
        if FRAGMENTS(j).GEOMETRY_DATA.mass>0
            dim=max([dim  max(FRAGMENTS(j).GEOMETRY_DATA.dimensions)  max(C-FRAGMENTS(j).DYNAMICS_DATA.cm_coord)    ]);
        end
    end
end
dim=0.8*ceil(dim);%3*ceil(dim)
if norm(Q)==0 || norm(Q(1:2))==0
    az=20;
    el=20;
else
    Q=Q/norm(Q);
    az=45+(pi/180)*atan2(Q(2),Q(1));
    el=20+(pi/180)*atan2(Q(3),sqrt(Q(1)^2+Q(3)^2));
end
%   el = 20; %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%   az = 20; %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%   dim = 1; %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
V_scale=5.e3;