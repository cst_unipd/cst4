
function draw_cylinderV0( i,m_color,ME )
%DRAW_PLATE Draws the box (with orientation). 

% radius=ME(i).GEOMETRY_DATA.dimensions(1)/2;
% height=ME(i).GEOMETRY_DATA.dimensions(2);
% X_cg=ME(i).DYNAMICS_DATA.cm_coord(1);
% Y_cg=ME(i).DYNAMICS_DATA.cm_coord(2);
% Z_cg=ME(i).DYNAMICS_DATA.cm_coord(3);
% [X,Y,Z] = cylinder(radius);
% h=surf(X+X_cg,Y+Y_cg,Z*height-height/2+Z_cg);
% set(h,'FaceColor',[.6 .8*(m_color) .9*(m_color)],'FaceAlpha',1,'EdgeColor',[0 0 0]);

radius=ME(i).GEOMETRY_DATA.dimensions(1);
height=ME(i).GEOMETRY_DATA.dimensions(3);
X_cg=ME(i).DYNAMICS_DATA.cm_coord(1);
Y_cg=ME(i).DYNAMICS_DATA.cm_coord(2);
Z_cg=ME(i).DYNAMICS_DATA.cm_coord(3);

[Xci,Yci,Zci] = cylinder([radius radius radius]);
Zci=Zci*height;
[Xca,Yca,Zca]=cylinder([radius 0.001*radius 0.00001]); % Costruzione dei punti coperchio basso
Zca=Zca*0.00001*radius-height/2;
Zcb=Zca+height; % Costruzione dei punti del coperchio alto

% Aggiunge i coperhi al cilindro
X=[Xci Xca Xca];
Y=[Yci Yca Yca];
Z=[Zci-height/2 Zca Zcb];

q0=ME(i).DYNAMICS_DATA.quaternions(1);
q1=ME(i).DYNAMICS_DATA.quaternions(2);
q2=ME(i).DYNAMICS_DATA.quaternions(3);
q3=ME(i).DYNAMICS_DATA.quaternions(4);
%The orientation of the cylinder is given by the quaternions
Rot_l2g=[q0^2+q1^2-q2^2-q3^2, 2*(q1*q2-q0*q3), 2*(q0*q2+q1*q3);
    2*(q1*q2+q0*q3)    , q0^2-q1^2+q2^2-q3^2, 2*(q2*q3-q0*q1);
    2*(q1*q3-q0*q2)    , 2*(q0*q1+q2*q3),     q0^2-q1^2-q2^2+q3^2];

R_q=Rot_l2g'; %transpose the matrix
for jj = 1: numel(X)
    X_n(jj) = R_q(1,:)*[X(jj) Y(jj) Z(jj)]';
    Y_n(jj) = R_q(2,:)*[X(jj) Y(jj) Z(jj)]';
    Z_n(jj) = R_q(3,:)*[X(jj) Y(jj) Z(jj)]';
end
X_n = reshape(X_n,size(X,1),size(X,2));
Y_n = reshape(Y_n,size(Y,1),size(Y,2));
Z_n = reshape(Z_n,size(Z,1),size(Z,2));
h=surf(X_n+X_cg,Y_n+Y_cg,Z_n+Z_cg);
if ME(i).GEOMETRY_DATA.shape_ID==4
    set(h,'FaceColor',[.6 .8*(m_color) .9*(m_color)],'FaceAlpha',0.7,'EdgeColor',[.4 .4 .4]);
elseif ME(i).GEOMETRY_DATA.shape_ID==5
    set(h,'FaceColor',[.6 .8*(m_color) .9*(m_color)],'FaceAlpha',0.35,'EdgeColor',[.4 .4 .4]);
end
end


