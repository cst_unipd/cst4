function plot_fragmentation_LO_UPGRADED(i,F_DATA,C_DATA,main_loop_count,tf,ME,FRAGMENTS,j_fig)
global output_folder
output_folder_fig=fullfile(output_folder,'figures');

if isempty(F_DATA{i})
    return;
end

axis_len = 0;
axis_len = max([axis_len F_DATA{i}.Frag_ME.lx F_DATA{i}.Frag_ME.ly F_DATA{i}.Frag_ME.lz]);
axis_len_b = max([F_DATA{i}.Frag_ME.lx F_DATA{i}.Frag_ME.ly F_DATA{i}.Frag_ME.lz]);

titlestring1 = ['object type = ' num2str(C_DATA(i).TARGET.type) ' - object ID = ' num2str(C_DATA(i).TARGET.ID)];
titlestring2 = ['step N. ' num2str(main_loop_count) ' - time = ' num2str(tf) ' s'];

%%
[~,~,az,el,V_SCALE] = PLOT1_geometry_parameters(ME,FRAGMENTS);

h = figure('Position',[30 30 830 630],'Visible', 'off');
set(gcf,'color','white');
hold on; axis equal; axis off;
view(az,el);

title({titlestring1;titlestring2});

% --------------
% plot Global RF
%{
pltassi(trasm(eye(3),[0 0 0]'),'r','',1,axis_len);
%}

% ------------------------------
% plot the Impacted ME/FRAGMENTS
%
% pltassi(F_DATA{i}.Frag_ME.TMG,'b','',1,0.7*axis_len_b);
if isSolidShape(F_DATA{i}.Frag_ME.shape_ID)
    v = trasfg_vectors(F_DATA{i}.Frag_ME.v',F_DATA{i}.Frag_ME.TFG)';
    drawMesh(v, F_DATA{i}.Frag_ME.f, 'FaceColor',[0 0 0],'FaceAlpha',0.05);
else
    vi_cart = trasfg_vectors(F_DATA{i}.Frag_ME.vi_cart',F_DATA{i}.Frag_ME.TFG)';
    ve_cart = trasfg_vectors(F_DATA{i}.Frag_ME.ve_cart',F_DATA{i}.Frag_ME.TFG)';
    drawMesh(vi_cart,F_DATA{i}.Frag_ME.fi_cart, 'FaceColor',[0 0 0],'FaceAlpha',0.2,'EdgeAlpha',0.1);
    drawMesh(ve_cart,F_DATA{i}.Frag_ME.fe_cart, 'FaceColor',[0 0 1],'FaceAlpha',0.2,'EdgeAlpha',0.1);
end
% plot object_ID
text(F_DATA{i}.Frag_ME.TMG(1,4)+0.4*axis_len_b,...
     F_DATA{i}.Frag_ME.TMG(2,4)+0.4*axis_len_b,...
     F_DATA{i}.Frag_ME.TMG(3,4)+0.4*axis_len_b,...
     num2str(F_DATA{i}.Frag_ME.object_ID_index),...
     'FontSize',16);
%}

% ------------------
% plot impact points
%
%drawPoint3d(F_DATA{i}.Impact_Data.I_POS_G,'pm','MarkerSize',10);
mh = drawPoint3d(F_DATA{i}.Impact_Data.I_POS_G,'.r');
set(mh,'MarkerSize',30,'LineWidth',2);
%}

% --------------------
% plot impact velocity
%
vel_scale_factor = axis_len_b/norm(F_DATA{i}.Impact_Data.I_VEL_F)/(0.1*V_SCALE);
I_VEL_G = (F_DATA{i}.Frag_ME.RFG*(F_DATA{i}.Impact_Data.I_VEL_F'))';
for ii=1:F_DATA{i}.Impact_Data.N_impact
%     I_vel_g = 0.5*axis_len_b*I_VEL_G(ii,:)/norm(I_VEL_G(ii,:));
    I_vel_g = vel_scale_factor*I_VEL_G(ii,:);
    drawVector3d(F_DATA{i}.Impact_Data.I_POS_G(ii,:)-1.01*I_vel_g, ...
                 I_vel_g, ...
                 'Color','r','LineWidth',2);
end
%}

% % ------------------------------
% % plot fragments CoM
% 
% for k = 1:F_DATA{i}.Impact_Data.N_frag_vol
%     CoM_G = trasfg_vectors(F_DATA{i}.frags_in_volume{k}.CoM',F_DATA{i}.Frag_ME.TFG)';
%     drawPoint3d(CoM_G,'.k');
% end


% -------------------------------
% plot the edges of the fragments
% this may take time
plot_frag_vol_flag = 0;
for k=1:length(F_DATA{i}.fragments)
    if isfield(F_DATA{i}.fragments{k},'edges')
        for j=1:length(F_DATA{i}.fragments{k}.edges)
            edges_g = trasfg_vectors(F_DATA{i}.fragments{k}.edges{j}',F_DATA{i}.Frag_ME.TFG)';
            h_edges = drawEdge3d([edges_g(1,:),edges_g(2,:)]);
            set(h_edges,'Color','b','LineStyle','--');
        end
    else
        plot_frag_vol_flag = 1;
    end
end

if plot_frag_vol_flag == 1
    for k=1:F_DATA{i}.Impact_Data.N_frag_vol
        if isSolidShape(F_DATA{i}.Frag_ME.shape_ID)
            %%%% ORIGINAL
            %drawMesh(F_DATA{i}.Frag_Volume{k}.v, F_DATA{i}.Frag_Volume{k}.f, ...
            %         'FaceColor', 'r','FaceAlpha',0.1);
            %%%% LO CORRECTION
            drawMesh(F_DATA{i}.Frag_Volume{k}.v-F_DATA{i}.Frag_Data(1:3)+F_DATA{i}.Impact_Data.I_POS_G(k,:), F_DATA{i}.Frag_Volume{k}.f, ...
                     'FaceColor', 'r','FaceAlpha',0.1);
        else
            drawPoint3d(F_DATA{i}.Frag_Volume{k}.v_cart,'*r');
        end
    end
end

% ---
% plot velocity
%
v_CM = C_DATA(i).V_CM';
for k = 1:F_DATA{i}.Impact_Data.N_frag_vol
    CoM_G = trasfg_vectors(F_DATA{i}.frags_in_volume{k}.CoM',F_DATA{i}.Frag_ME.TFG)';
    vel_G = (F_DATA{i}.Frag_ME.RFG*(F_DATA{i}.frags_in_volume{k}.vel'))' + v_CM;
    drawVector3d(CoM_G(1:end,:), ...
                 90*vel_scale_factor*vel_G(1:end,:), ...
                 'Color','r','LineWidth',1);
end

%}

tmpl=fullfile(output_folder_fig,'Crater_plot_');
fnam=sprintf('%s_%d',tmpl,j_fig);
saveas(h, fnam,'png')
saveas(h, fnam,'fig')

end