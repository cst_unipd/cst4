
%> @file  Post_processing2.m
%> @brief post processing launcher of CST data, plots and figures

%======================================================================
disp('Post processing of CST simulation')
delay=10;
flag_exit=0;
button=Post_processing_start(delay);

%% Check and in case exit
if exist('sim_title_dir')==0 && button(1)~='E'
    flag_alone=1;
else
    flag_alone=0;
end

%% Make the standalone version work with just user input
if flag_alone==1
    clear ME
    clear FRAGMENTS
    clear RIS
    disp('Stand-alone post processing')
    %path_initialization
    button2=Post_processing_insert_box(delay);
    
    switch button2(1)
        case 'Y' % Insert custom name
            files = dir('Results');
            [index_ls,exit_2]=(Stand_alone_start_UPGRADED(1000*delay,files));
            if exit_2==0
                last_simulation='No valid folder was chosen, exiting post_processing';
                flag_exit=1;
                disp(last_simulation);
            else 
                last_simulation=files(index_ls).name;
                if last_simulation(1)=='.'
                    last_simulation='No valid folder was chosen, exiting post_processing';
                    flag_exit=1;
                    disp(last_simulation);
                end
            end
        case 'C'
            disp('Loading last simulation in the Results folder')
            last_simulation=[];
        otherwise
            flag_exit=1;
            last_simulation=[];
    end
      
    % FIND LAST RESULTS FOLDER
    if isempty(last_simulation)==true
        % Get a list of all files and folders in this folder.
        files = dir('Results');
        % Get a logical vector that tells which is a directory.
        dirFlags = [files.isdir];
        % Extract only those that are directories.
        subFolders = files(dirFlags);
        if length(subFolders)>2  % Delete '.' and '..' "folders"
            for i1=2:-1:1
                if subFolders(i1).name(1)=='.'
                    subFolders(i1)=[];
                end
                [~,I] = max([subFolders(:).datenum]);
                last_simulation=subFolders(I).name;
            end
        else
            flag_exit=1;
            last_simulation='does_not_exist';
        end
    end
    sim_title_dir=fullfile('Results',last_simulation);
    if exist(fullfile(sim_title_dir))==7 && exist(fullfile('Results',last_simulation,'data'))==7 && exist(fullfile('Results',last_simulation,'figures'))==7
        disp(['Start post processing of simulation ' last_simulation]);
    else
        flag_exit=1;
    end
end

%% Start real post-processing
if flag_exit==1
    disp('Wrong or non-existent simulation dataset. Exiting from CST post processing')
else
    switch button(1)
        case 'P'
            disp('Post processing of CST simulation: standard post-processing had been chosen')
            Post_processing_standard_UPGRADED
        case 'O'
            disp('Post processing of CST simulation: non-standard post-processing had been chosen: wait for dialog box')
            Post_processing_user_choice_UPGRADED_2
        case 'E'
            disp('Exit from post processing. Thank you for choosing CST')
        otherwise
            disp('Exit from post processing. Thank you for choosing CST')
    end
end

%% Clear variables for next post processing procedures
clear sim_title_dir
clear last_simulation
clear fnam
clear fnam2
clear fnam3
clear tmpl 
clear tmp2
clear tmp3
clear tmpVID
clear tmpVID_bubble

%% End
disp('---> POST PROCESSING COMPLETE. Thank you for choosing CST')




