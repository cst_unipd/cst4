function draw3D()
%"DRAW" outputs all the "graphics", just to help intuition and de-bugging.
%NB! It works with the local copy of ME, ME_SR. Therefore it plots the SR
%version of the actual configuration

global ME_SR;
global link_data;
global t;
global FRAGME_SRNT;
global sim_title;

V_scale=8000; %[1km/s will be a vector of lenght 1]
disp_local_coordinates=false;
Color=[0.7 0.7 0.7];
n_me= length(ME_SR);
%n_me=length(ME_SR);
for i=1:1:n_me % for each ME_SR
    if i>51
        Color='b';
    end
    %Draw the ME_SR centers of mass
    if ME_SR(i).GEOMETRY_DATA.mass~=0
        plot3(ME_SR(i).DYNAMICS_DATA.cm_coord(1),ME_SR(i).DYNAMICS_DATA.cm_coord(2),ME_SR(i).DYNAMICS_DATA.cm_coord(3),'or','markers',ME_SR(i).GEOMETRY_DATA.mass/100); %ME_SR_i
        hold on
        %Draws ME_SR shapes
        if ME_SR(i).GEOMETRY_DATA.shape_ID == 1 %it is a plate/box
            draw_box(i,Color);
        elseif ME_SR(i).GEOMETRY_DATA.shape_ID==2 || ME_SR(i).GEOMETRY_DATA.shape_ID==3 % sphere or hollow sphere
            draw_sphere(i,Color);
        elseif ME_SR(i).GEOMETRY_DATA.shape_ID==4 || ME_SR(i).GEOMETRY_DATA.shape_ID==5 % cylinder or hollow cylinder
            draw_cylinder(i,Color);
        end
        
        %Other things
        if disp_local_coordinates==true
            local_fr_X=Rot_l2g*[0.5;0;0];
            local_fr_Y=Rot_l2g*[0;0.5;0];
            local_fr_Z=Rot_l2g*[0;0;0.5];
            quiver3(ME_SR(i).DYNAMICS_DATA.cm_coord(1),ME_SR(i).DYNAMICS_DATA.cm_coord(2),ME_SR(i).DYNAMICS_DATA.cm_coord(3),local_fr_X(1),local_fr_X(2),local_fr_X(3),'r');
            quiver3(ME_SR(i).DYNAMICS_DATA.cm_coord(1),ME_SR(i).DYNAMICS_DATA.cm_coord(2),ME_SR(i).DYNAMICS_DATA.cm_coord(3),local_fr_Y(1),local_fr_Y(2),local_fr_Y(3),'b');
            quiver3(ME_SR(i).DYNAMICS_DATA.cm_coord(1),ME_SR(i).DYNAMICS_DATA.cm_coord(2),ME_SR(i).DYNAMICS_DATA.cm_coord(3),local_fr_Z(1),local_fr_Z(2),local_fr_Z(3),'g');
        end
        
        %Draws ME_SR velocity vector (scaled)
        quiver3(ME_SR(i).DYNAMICS_DATA.cm_coord(1),ME_SR(i).DYNAMICS_DATA.cm_coord(2),ME_SR(i).DYNAMICS_DATA.cm_coord(3),ME_SR(i).DYNAMICS_DATA.vel(1)/V_scale,ME_SR(i).DYNAMICS_DATA.vel(2)/V_scale,ME_SR(i).DYNAMICS_DATA.vel(3)/V_scale,'r','LineWidth',3); % V_ME_SR_i
    end
    hold on
    i
    pause
end

%Draws links
n_links=size(link_data,2);
for s=1:1:n_links
%     disp(link_data(s).ME_connect)
%     A=link_data(s).ME_connect; % Gets the Id of the ME_SR that are connected by the link
    i=find([ME_SR.object_ID]==link_data(s).ME_connect(1)); % i_1 is the ME1 index
    j=find([ME_SR.object_ID]==link_data(s).ME_connect(2)); % i_1 is the ME2 index
    if ~isempty(i) && ~isempty(j) && ME_SR(i).GEOMETRY_DATA.mass && ME_SR(j).GEOMETRY_DATA.mass
        if link_data(s).state~=0 && all(ismember([i,j],[ME_SR.object_ID]))
            %connect ME_SR i and ME_SR j
            plot3([ME_SR(i).DYNAMICS_DATA.cm_coord(1),ME_SR(j).DYNAMICS_DATA.cm_coord(1)],[ME_SR(i).DYNAMICS_DATA.cm_coord(2),ME_SR(j).DYNAMICS_DATA.cm_coord(2)],[ME_SR(i).DYNAMICS_DATA.cm_coord(3),ME_SR(j).DYNAMICS_DATA.cm_coord(3)],'k','LineWidth',2);
        end
    end
%     pause
end
%codice frammenti

n_size=size(FRAGME_SRNT,2);
hold on


% CM plot
hold on
for i = 2:n_size
    patch(FRAGME_SRNT(i).GEOMETRY_DATA.c_hull(:,1)+0.95,FRAGME_SRNT(i).GEOMETRY_DATA.c_hull(:,2)-ME_SR(1).GEOMETRY_DATA.dimensions(2)/2,FRAGME_SRNT(i).GEOMETRY_DATA.c_hull(:,1).*0+ME_SR(1).DYNAMICS_DATA.cm_coord(3),'w')
end
for i = 2:n_size
    hold on
    if FRAGME_SRNT(i).GEOMETRY_DATA.mass~=0
        plot3(FRAGME_SRNT(i).DYNAMICS_DATA.cm_coord(1)+FRAGME_SRNT(i).DYNAMICS_DATA.vel(1).*t,FRAGME_SRNT(i).DYNAMICS_DATA.cm_coord(2)+FRAGME_SRNT(i).DYNAMICS_DATA.vel(2).*t,FRAGME_SRNT(i).DYNAMICS_DATA.cm_coord(3)+FRAGME_SRNT(i).DYNAMICS_DATA.vel(3).*t,'r.');
        hold on
        patch(FRAGME_SRNT(i).GEOMETRY_DATA.c_hull(:,1)+0.95+FRAGME_SRNT(i).DYNAMICS_DATA.vel(1).*t,FRAGME_SRNT(i).GEOMETRY_DATA.c_hull(:,2)-ME_SR(1).GEOMETRY_DATA.dimensions(2)/2+FRAGME_SRNT(i).DYNAMICS_DATA.vel(2).*t,FRAGME_SRNT(i).GEOMETRY_DATA.c_hull(:,1).*0+ME_SR(1).DYNAMICS_DATA.cm_coord(3)+FRAGME_SRNT(i).DYNAMICS_DATA.vel(3).*t,'r')
        hold on
    end
    
end

% axis([-6 6 -6 6 -6 6]);
axis equal
xlabel('x [m]')
ylabel('y [m]')
zlabel('z [m]')
t_title=round(t,5);
% title_str=['Simulation at time ',num2str(t_title), 's'];
title_str = strrep(sim_title,'_',' ');
title(title_str);
set(gca,'fontsize',14)
grid on

end