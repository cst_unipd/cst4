function draw_sphereV0( i ,m_color,ME)
%DRAW_PLATE Draws the box (with orientation).

radius=ME(i).GEOMETRY_DATA.dimensions(1);
X_cg=ME(i).DYNAMICS_DATA.cm_coord(1);
Y_cg=ME(i).DYNAMICS_DATA.cm_coord(2);
Z_cg=ME(i).DYNAMICS_DATA.cm_coord(3);
[X,Y,Z] = sphere;
h=surf(X*radius+X_cg,Y*radius+Y_cg,Z*radius+Z_cg);
if ME(i).GEOMETRY_DATA.shape_ID==2
    set(h,'FaceColor',[.6 .8*(m_color) .9*(m_color)],'FaceAlpha',0.7,'EdgeColor',[.4 .4 .4]);
elseif ME(i).GEOMETRY_DATA.shape_ID==3
    set(h,'FaceColor',[.6 .8*(m_color) .9*(m_color)],'FaceAlpha',0.35,'EdgeColor',[.4 .4 .4]);
end
end

