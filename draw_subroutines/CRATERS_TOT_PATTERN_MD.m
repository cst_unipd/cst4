% load the data and launch the plots of the first crater

global output_folder
global n
global SHAPE_ID_LIST;

simu_name='Advanced_Shields_CASE1_Case1_a_Al_20181214T134417';
load('N:\CST3_ADV_SHIELDS_CASE1aAl\Results\Advanced_Shields_CASE1_Case1_a_Al_20181214T134417\data\step_2433.mat','HOLES','FRAGMENTS')

ME_last_hole=max([HOLES.object_ID]);
for i=ME_last_hole+1:1:length(ME)
    if ME(i).GEOMETRY_DATA.mass>0
        ME_surv=32;
        break
    end
end

output_folder=fullfile(simu_name);
sim_title_dir=fullfile(simu_name);

shape_id_list();
input_folder = fullfile(sim_title_dir,'data');
output_folder_fig=fullfile(output_folder,'figures');


cd(input_folder);
step_tot=dir('step*');
n=length(step_tot);

cd('..')
cd('..')
load(fullfile(input_folder,'step_0'));

%%


tmpl = fullfile(output_folder_fig, 'TIZ_crater');

name=fullfile(input_folder,'F_DATA_');


[~,~,az,el,V_scale] = PLOT1_geometry_parameters_MD(ME,FRAGMENTS,ME_surv);
h = figure('Position',[30 30 830 630],'Visible', 'on');
set(gcf,'color','white');
hold on; axis equal; axis off;
view(az,el);
% draw3Dv0_MD(link_data,ME,0,V_scale,ME_surv); % draw all MEs
pause(0.1)
for ii=3:n
    empty_falg=0;
    
    in_file=[name num2str(ii)];
    % Check if F_DATA_i is empty, i.e. if the structure F_DATA is loaded
    if exist([in_file '.mat'],'file')==2
        load(in_file);
        if exist('F_DATA','var')==0
            empty_falg=1;
        end
    else
        empty_falg=1;
    end
    
    if empty_falg==1
        disp('No available data in fragmentation files was found. Pleae check the simulation diary')
    else
        load(in_file);
        
        n_C = length(F_DATA);
        
        j=1;
        for i=1:n_C
            if isfield(F_DATA{i},'Frag_ME')==true
                %                 if ~isempty(F_DATA{i}.Frag_ME.object_ID_index)
                if F_DATA{i}.Frag_ME.object_ID_index==ME_surv %&& F_DATA{i}.Frag_ME.target_type==1
                    CRATER_plot_fragmentation_LO_UPGRADED_MD(i,F_DATA,C_DATA,main_loop_count,tf,ME,FRAGMENTS,j,h,ii,ME_surv);
                    j=j+1;
                    %                         pause
                    hold on
                    % j indicates the number of the figure, they are consecutive also
                    % with data unplottable (i.e. being in the other half of the if
                    % case)
                end
                %                 end
                %             else
                %                 disp(['No fragmentation was found for target ' num2str(i) ' of ' num2str(n_C) '.'])
            end
        end
    end
    disp(num2str(ii));
    clear F_DATA;
end

% if exist('Data_Simulation.txt','file')
%     delete('Data_Simulation.txt')
% end
% diary Data_Simulation_Advanced_Shield.txt
% diary on
% disp('----------------------------------------------------------------------------------------------------------')
% disp('SIMULATION                            CPUTIME (min)       SIMULATION TIME (s)       FINAL STEP')
% disp(['ADVANCED shield type 1 (Al, d=0.2)',cputime_ADV_1_1a,tf_ADV_1_1a,final_step_ADV_1_1a])
% disp(['ADVANCED shield type 1 (Al, d=0.5)',cputime_ADV_1_1d,tf_ADV_1_1d,final_step_ADV_1_1d])
% disp('----------------------------------------------------------------------------------------------------------')
% disp(['ADVANCED shield type 1 (Nylon, d=0.2)',cputime_ADV_1_1aN,tf_ADV_1_1aN,final_step_ADV_1_1aN])
% disp(['ADVANCED shield type 1 (Nylon, d=0.5)',cputime_ADV_1_1dN,tf_ADV_1_1dN,final_step_ADV_1_1dN])
% disp('----------------------------------------------------------------------------------------------------------')
% disp(['ADVANCED shield type 2 (Al, d=0.2)',cputime_ADV_2_1a,tf_ADV_2_1a,final_step_ADV_2_1a])
% disp(['ADVANCED shield type 2 (Al, d=0.5)',cputime_ADV_2_1d,tf_ADV_2_1d,final_step_ADV_2_1d])
% disp('----------------------------------------------------------------------------------------------------------')
% disp(['ADVANCED shield type 2 (Nylon, d=0.2)',cputime_ADV_2_1aN,tf_ADV_2_1aN,final_step_ADV_2_1aN])
% disp(['ADVANCED shield type 2 (Nylon, d=0.5)',cputime_ADV_2_1dN,tf_ADV_2_1dN,final_step_ADV_2_1dN])
% disp('----------------------------------------------------------------------------------------------------------')
% diary off