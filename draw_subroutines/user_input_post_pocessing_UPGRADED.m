function [C1,dim1,az1,el1,V_scale1,flag_velocities1,flag_bubbles1,plot_flag1,general_plot_flag1,videos_flag1,plot_fragmentation_flag1,pdf_flag1,step_size1,plot_crater1,limit_step1,const_delta_t_flag1,initial_step1]=...
    user_input_post_pocessing_UPGRADED(C,dim,az,el,V_scale,flag_velocities,flag_bubbles,plot_flag,general_plot_flag,videos_flag,plot_fragmentation_flag,pdf_flag,step_size,plot_crater,limit_step,const_delta_t_flag,initial_step)

% Function to give the user the possibility to choose the best desired
% post processing options

global n % number of dataset

disp('---> POST PROCESSING: USER INPUT CHOICHES START')

prompt = {'Save figures in .fig, 1 to execute, 0 to remove'...
          'Simulation Plot, 1 to execute, 0 to remove (mandatory for videos)',...
          '    Bubble plot, 1 to execute, 0 to remove (mandatory for bubble videos)',...
          '    Velocity plot, 1 to execute, 0 to remove (mandatory for velocity videos)',...
          '    Videos, 1 to execute, 0 to remove',...
          '    Plot center, default value visible in figure',...
          '    Plot dimension, default value visible in figure',...
          '    Azimuth, default value visible in figure',...
          '    Elevation, default value visible in figure',...
          '    Scaling of velocity vectors, default value visible in figure',...
          ['    Step size on a total of ' num2str(n) ' steps of simulation'],...
          '    Initial step of simulation',...
          '    Max step of simulation',...
          '    Constant time step, 1 = Yes, 0 = No',...
          'Fragments Plot, 1 to execute, 0 to remove',...
          'First crater plots, 1 to execute, 0 to remove',...
          'Output in the pdf file, 1 to execute, 0 to remove'};
dlg_title = 'Input';
num_lines = 1;
defaultans = {num2str(plot_flag),num2str(general_plot_flag),num2str(flag_bubbles),num2str(flag_velocities),num2str(videos_flag),...
    num2str(C'),num2str(dim),num2str(az),num2str(el),num2str(V_scale),num2str(step_size),num2str(initial_step),num2str(limit_step),num2str(const_delta_t_flag),num2str(plot_fragmentation_flag),num2str(plot_crater),num2str(pdf_flag)};
answer = inputdlg(prompt,dlg_title,num_lines,defaultans);

wrngs=0; % number of warnings
if ~isempty(answer)
    plot_flag1=str2num(answer{1});
    general_plot_flag1=str2num(answer{2});
    flag_bubbles1=str2num(answer{3});
    flag_velocities1=str2num(answer{4});
    videos_flag1=str2num(answer{5});
    C1=str2num(answer{6});
    C1=C1';
    dim1=str2num(answer{7});
    az1=str2num(answer{8});
    el1=str2num(answer{9});
    V_scale1=str2num(answer{10});
    step_size1=str2num(answer{11});
    initial_step1=str2num(answer{12});
    limit_step1=str2num(answer{13});
    const_delta_t_flag1=str2num(answer{14});
    plot_fragmentation_flag1=str2num(answer{15});
    plot_crater1=str2num(answer{16});
    pdf_flag1=str2num(answer{17});
else
    wrngs=17; % number of warnings
    disp('exit from interface, data set to default')
    plot_flag1=plot_flag;
    general_plot_flag1=general_plot_flag;
    flag_bubbles1=flag_bubbles;
    flag_velocities1=flag_velocities;
    videos_flag1=videos_flag;
    C1=C;
    dim1=dim;
    az1=az;
    el1=el;
    V_scale1=V_scale;
    initial_step1=initial_step;
    step_size1=step_size;
    limit_step1=limit_step;
    const_delta_t_flag1=const_delta_t_flag;
    plot_fragmentation_flag1=plot_fragmentation_flag;
    plot_crater1=plot_crater;
    pdf_flag1=pdf_flag;
end

if isnumeric(C1)==false
    C1=C;
    disp('error in the plot center definition')
    disp('plot dimesnion factor is forced to the default value')
    wrngs=wrngs+1;
elseif length(C1)~=length(C)
    C1=C;
    disp('error in the plot center definition')
    disp('plot dimesnion factor is forced to the default value')
    wrngs=wrngs+1;
end

if isnumeric(dim1)==false
    dim1=dim;
    disp('error in the plot dimesnion factor definition')
    disp(['plot dimesnion factor is forced == ' num2str(dim) ', the default value'])
    wrngs=wrngs+1;
elseif length(dim1)~=1
    dim1=dim;
    disp('error in the plot dimesnion factor definition')
    disp(['plot dimesnion factor is forced == ' num2str(dim) ', the default value'])
    wrngs=wrngs+1;
end

if isnumeric(az1)==false
    az1=az;
    disp('error in the azimuth definition')
    disp(['azimuth is forced == ' num2str(az) ', the default value'])
    wrngs=wrngs+1;
elseif length(az1)~=1
    az1=az;
    disp('error in the azimuth definition')
    disp(['azimuth is forced == ' num2str(az) ', the default value'])
    wrngs=wrngs+1;
end

if isnumeric(el1)==false
    el1=el;
    disp('error in the elevation definition')
    disp(['elevation is forced == ' num2str(el) ', the default value'])
    wrngs=wrngs+1;
elseif length(el1)~=1
    el1=el;
    disp('error in the elevation definition')
    disp(['elevation is forced == ' num2str(el) ', the default value'])
    wrngs=wrngs+1;
end

if isnumeric(V_scale1)==false
    V_scale1=V_scale;
    disp('error in the scaling of velocity vectors factor definition')
    disp(['scaling of velocity vectors is forced == ' num2str(V_scale) ', the default value'])
    wrngs=wrngs+1;
elseif length(V_scale1)~=1
    V_scale1=V_scale;
    disp('error in the scaling of velocity vectors factor definition')
    disp(['scaling of velocity vectors is forced == ' num2str(V_scale) ', the default value'])
    wrngs=wrngs+1;
end

if isnumeric(initial_step1)==false || isempty(initial_step1)
    initial_step1=initial_step;
    disp('error in initial step definition')
    disp(['Initial step is forced == ' num2str(initial_step) ', the default value'])
    wrngs=wrngs+1;
elseif initial_step1<1
    initial_step1=initial_step;
    disp('Initial step cannot be < 1')
    disp(['Initial step is forced == ' num2str(initial_step) ', the default value'])
    wrngs=wrngs+1;
end

if isnumeric(step_size1)==false || isempty(step_size1)
    step_size1=step_size;
    disp('error in step size definition')
    disp(['step size is forced == ' num2str(step_size) ', the default value'])
    wrngs=wrngs+1;
elseif step_size1==0
    step_size1=step_size;
    disp('step size cannot be == 0')
    disp(['step size is forced == ' num2str(step_size) ', the default value'])
    wrngs=wrngs+1;
end

if isnumeric(limit_step1)==false || isempty(limit_step1)
    limit_step1=limit_step;
    disp('error in max step definition')
    disp(['max step is forced == ' num2str(limit_step) ', the default value'])
    wrngs=wrngs+1;
elseif limit_step1==0
    limit_step1=limit_step;
    disp('max step cannot be == 0')
    disp(['max step is forced == ' num2str(limit_step) ', the default value'])
    wrngs=wrngs+1;
end

if isnumeric(flag_velocities1)==false
    flag_velocities1=flag_velocities;
    disp('The "velocities plot" flag can be equal only to 0 or 1')
    disp(['The flag is forced == ' num2str(flag_velocities) ', the default value'])
    wrngs=wrngs+1;
elseif length(flag_velocities1)~=1
    flag_velocities1=flag_velocities;
    disp('The "velocities plot" flag can be equal only to 0 or 1')
    disp(['The flag is forced == ' num2str(flag_velocities) ', the default value']) 
    wrngs=wrngs+1;
elseif flag_velocities1~=0 && flag_velocities1~=1
    flag_velocities1=flag_velocities;
    disp('The "velocities plot" flag can be equal only to 0 or 1')
    disp(['The flag is forced == ' num2str(flag_velocities) ', the default value'])
    wrngs=wrngs+1;
end

if isnumeric(flag_bubbles1)==false
    flag_bubbles1=flag_bubbles;
    disp('The "bubbles plot" flag can be equal only to 0 or 1')
    disp(['The flag is forced == ' num2str(flag_bubbles) ', the default value'])
    wrngs=wrngs+1;
elseif length(flag_bubbles1)~=1
    flag_bubbles1=flag_bubbles;
    disp('The "bubbles plot" flag can be equal only to 0 or 1')
    disp(['The flag is forced == ' num2str(flag_bubbles) ', the default value'])
    wrngs=wrngs+1;
elseif flag_bubbles1~=0 && flag_bubbles1~=1
    flag_bubbles1=flag_bubbles;
    disp('The "bubbles plot" flag can be equal only to 0 or 1')
    disp(['The flag is forced == ' num2str(flag_bubbles) ', the default value'])
    wrngs=wrngs+1;
end

if isnumeric(plot_flag1)==false
    plot_flag1=plot_flag;
    disp('The ".fig saving" flag can be equal only to 0 or 1')
    disp(['The flag is forced == ' num2str(plot_flag) ', the default value'])
    wrngs=wrngs+1;
elseif length(plot_flag1)~=1
    plot_flag1=plot_flag;
    disp('The ".fig saving" flag can be equal only to 0 or 1')
    disp(['The flag is forced == ' num2str(plot_flag) ', the default value'])
    wrngs=wrngs+1;
elseif plot_flag1~=0 && plot_flag1~=1
    plot_flag1=plot_flag;
    disp('The ".fig saving" flag can be equal only to 0 or 1')
    disp(['The flag is forced == ' num2str(plot_flag) ', the default value'])
    wrngs=wrngs+1;
end

if isnumeric(general_plot_flag1)==false
    general_plot_flag1=general_plot_flag;
    disp('The "general plot" flag can be equal only to 0 or 1')
    disp(['The flag is forced == ' num2str(general_plot_flag) ', the default value'])
    wrngs=wrngs+1;
elseif length(general_plot_flag1)~=1
    general_plot_flag1=general_plot_flag;
    disp('The "general plot" flag can be equal only to 0 or 1')
    disp(['The flag is forced == ' num2str(general_plot_flag) ', the default value'])
    wrngs=wrngs+1;
elseif general_plot_flag1~=0 && general_plot_flag1~=1
    general_plot_flag1=general_plot_flag;
    disp('The "general plot" flag can be equal only to 0 or 1')
    disp(['The flag is forced == ' num2str(general_plot_flag) ', the default value'])
    wrngs=wrngs+1;
end

if isnumeric(const_delta_t_flag)==false
    const_delta_t_flag1=const_delta_t_flag;
    disp('The "constant delta t" flag can be equal only to 0 or 1')
    disp(['The flag is forced == ' num2str(const_delta_t_flag) ', the default value'])
    wrngs=wrngs+1;
elseif length(const_delta_t_flag1)~=1
    const_delta_t_flag1=const_delta_t_flag;
    disp('The "constant delta t" flag can be equal only to 0 or 1')
    disp(['The flag is forced == ' num2str(const_delta_t_flag) ', the default value'])
    wrngs=wrngs+1;
elseif const_delta_t_flag1~=0 && const_delta_t_flag1~=1
    const_delta_t_flag1=const_delta_t_flag;
    disp('The "constant delta t" flag can be equal only to 0 or 1')
    disp(['The flag is forced == ' num2str(const_delta_t_flag) ', the default value'])
    wrngs=wrngs+1;
end

if isnumeric(videos_flag1)==false
    videos_flag1=videos_flag;
    disp('The "videos" flag can be equal only to 0 or 1')
    disp(['The flag is forced == ' num2str(videos_flag) ', the default value'])
    wrngs=wrngs+1;
elseif length(videos_flag1)~=1
    videos_flag1=videos_flag;
    disp('The "videos" flag can be equal only to 0 or 1')
    disp(['The flag is forced == ' num2str(videos_flag) ', the default value'])   
    wrngs=wrngs+1;
elseif videos_flag1~=0 && videos_flag1~=1
    videos_flag1=videos_flag;
    disp('The "videos" flag can be equal only to 0 or 1')
    disp(['The flag is forced == ' num2str(videos_flag) ', the default value'])
    wrngs=wrngs+1;
end

if isnumeric(plot_fragmentation_flag1)==false
    plot_fragmentation_flag1=plot_fragmentation_flag;
    disp('The "fragments Plot" flag can be equal only to 0 or 1')
    disp(['The flag is forced == ' num2str(plot_fragmentation_flag) ', the default value'])
    wrngs=wrngs+1;
elseif length(plot_fragmentation_flag1)~=1
    plot_fragmentation_flag1=plot_fragmentation_flag;
    disp('The "fragments Plot" flag can be equal only to 0 or 1')
    disp(['The flag is forced == ' num2str(plot_fragmentation_flag) ', the default value'])
    wrngs=wrngs+1;
elseif plot_fragmentation_flag1~=0 && plot_fragmentation_flag1~=1
    plot_fragmentation_flag1=plot_fragmentation_flag;
    disp('The "fragments Plot" flag can be equal only to 0 or 1')
    disp(['The flag is forced == ' num2str(plot_fragmentation_flag) ', the default value'])
    wrngs=wrngs+1;
end

if isnumeric(plot_crater1)==false
    plot_crater1=plot_crater;
    disp('The "first crater plots" flag can be equal only to 0 or 1')
    disp(['The flag is forced == ' num2str(plot_crater) ', the default value'])
    wrngs=wrngs+1;
elseif length(plot_crater1)~=1
    plot_crater1=plot_crater;
    disp('The "first crater plots" flag can be equal only to 0 or 1')
    disp(['The flag is forced == ' num2str(plot_crater) ', the default value'])
    wrngs=wrngs+1;
elseif plot_crater1~=0 && plot_crater1~=1
    plot_crater1=plot_crater;
    disp('The "first crater plots" flag can be equal only to 0 or 1')
    disp(['The flag is forced == ' num2str(plot_crater) ', the default value'])
    wrngs=wrngs+1;
end

if isnumeric(pdf_flag1)==false
    pdf_flag1=pdf_flag;
    disp('The "output in pdf" flag can be equal only to 0 or 1')
    disp(['The flag is forced == ' num2str(pdf_flag) ', the default value'])
    wrngs=wrngs+1;
elseif length(pdf_flag1)~=1
    pdf_flag1=pdf_flag;
    disp('The "output in pdf" flag can be equal only to 0 or 1')
    disp(['The flag is forced == ' num2str(pdf_flag) ', the default value'])
    wrngs=wrngs+1;
elseif pdf_flag1~=0 && pdf_flag1~=1
    pdf_flag1=pdf_flag;
    disp('The "output in pdf" flag can be equal only to 0 or 1')
    disp(['The flag is forced == ' num2str(pdf_flag) ', the default value'])
    wrngs=wrngs+1;
end

%% Displaying number of warnings and user choices
disp(' ')
disp(['--> A total of ' num2str(wrngs) ' warnings has been found and corrected']);
disp(' ')
disp('--> List of the user inputs:')
disp(' ')
disp(['Flag to save figures in .fig (1 to execute, 0 to remove) = ' num2str(plot_flag1)])
disp(['Simulation Plot, 1 to execute, 0 to remove (mandatory for videos) = ' num2str(general_plot_flag1)])
disp(['    Bubble plot, 1 to execute, 0 to remove (mandatory for bubble videos) = ' num2str(flag_bubbles1)])
disp(['    Velocity plot, 1 to execute, 0 to remove (mandatory for velocity videos) = ' num2str(flag_velocities1)])
disp(['    Videos, 1 to execute, 0 to remove = ' num2str(videos_flag1)])
disp(['    Plot center = ' num2str(C1')])
disp(['    Plot dimension = ' num2str(dim1)])
disp(['    Azimuth = ' num2str(az1)])
disp([ '    Elevation = ' num2str(el1)])
disp(['    Scaling of velocity vectors = ' num2str(V_scale1)])
disp(['    Step size on a total of ' num2str(n) ' steps of simulation = ' num2str(step_size1)])
disp(['    Max step of simulation = ' num2str(limit_step1)])
disp(['Fragments Plot, 1 to execute, 0 to remove = ' num2str(plot_fragmentation_flag1)])
disp(['First crater plots, 1 to execute, 0 to remove = ' num2str(plot_crater1)])
disp(['Output in the pdf file, 1 to execute, 0 to remove = ' num2str(pdf_flag1)])
disp(' ')
disp('---> POST PROCESSING: USER INPUT CHOICHES END')
disp(' ')
