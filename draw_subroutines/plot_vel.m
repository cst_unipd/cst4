
function plot_vel(RIS)
figure
    n_me= length(RIS.FRAGMENTS);
    V_scale=1000;
    for i=1:1:n_me % for each ME
        %Draw the ME centers of mass
        if RIS.FRAGMENTS(i).GEOMETRY_DATA.mass~=0
            plot3(RIS.FRAGMENTS(i).DYNAMICS_DATA.cm_coord(1),RIS.FRAGMENTS(i).DYNAMICS_DATA.cm_coord(2),RIS.FRAGMENTS(i).DYNAMICS_DATA.cm_coord(3),'or','markers',RIS.FRAGMENTS(i).GEOMETRY_DATA.mass/100); %ME_i
            hold on

            %Draws ME shapes
            if RIS.FRAGMENTS(i).GEOMETRY_DATA.shape_ID==1 %it is a plate or a box
                if RIS.FRAGMENTS(i).GEOMETRY_DATA.dimensions(3)/mean(RIS.FRAGMENTS(i).GEOMETRY_DATA.dimensions(1:2))<0.1
                    draw_plateV0(i);
                else
                    draw_boxV0(i);
                end
            elseif RIS.FRAGMENTS(i).GEOMETRY_DATA.shape_ID==2 || RIS.FRAGMENTS(i).GEOMETRY_DATA.shape_ID==3 % Sphere or Hollow Sphere 
                draw_sphereV0(i);
            elseif RIS.FRAGMENTS(i).GEOMETRY_DATA.shape_ID==4 || RIS.FRAGMENTS(i).GEOMETRY_DATA.shape_ID==5 % Cylinder or Hollow Cylinder
                draw_cylinderV0(i);
            end

            %Other things
%             if disp_local_coordinates==true
%                 local_fr_X=Rot_l2g*[0.5;0;0];
%                 local_fr_Y=Rot_l2g*[0;0.5;0];
%                 local_fr_Z=Rot_l2g*[0;0;0.5];
%                 quiver3(RIS.FRAGMENTS(i).DYNAMICS_DATA.cm_coord(1),RIS.FRAGMENTS(i).DYNAMICS_DATA.cm_coord(2),RIS.FRAGMENTS(i).DYNAMICS_DATA.cm_coord(3),local_fr_X(1),local_fr_X(2),local_fr_X(3),'r');
%                 quiver3(RIS.FRAGMENTS(i).DYNAMICS_DATA.cm_coord(1),RIS.FRAGMENTS(i).DYNAMICS_DATA.cm_coord(2),RIS.FRAGMENTS(i).DYNAMICS_DATA.cm_coord(3),local_fr_Y(1),local_fr_Y(2),local_fr_Y(3),'b');
%                 quiver3(RIS.FRAGMENTS(i).DYNAMICS_DATA.cm_coord(1),RIS.FRAGMENTS(i).DYNAMICS_DATA.cm_coord(2),RIS.FRAGMENTS(i).DYNAMICS_DATA.cm_coord(3),local_fr_Z(1),local_fr_Z(2),local_fr_Z(3),'g');
%             end

            %Draws ME velocity vector (scaled)
            if(i==1)
                quiver3(RIS.FRAGMENTS(i).DYNAMICS_DATA.cm_coord(1),RIS.FRAGMENTS(i).DYNAMICS_DATA.cm_coord(2),RIS.FRAGMENTS(i).DYNAMICS_DATA.cm_coord(3),RIS.FRAGMENTS(i).DYNAMICS_DATA.vel(1)/V_scale,RIS.FRAGMENTS(i).DYNAMICS_DATA.vel(2)/V_scale,RIS.FRAGMENTS(i).DYNAMICS_DATA.vel(3)/V_scale,'b'); % V_ME_i
            elseif(i<1001)
                quiver3(RIS.FRAGMENTS(i).DYNAMICS_DATA.cm_coord(1),RIS.FRAGMENTS(i).DYNAMICS_DATA.cm_coord(2),RIS.FRAGMENTS(i).DYNAMICS_DATA.cm_coord(3),RIS.FRAGMENTS(i).DYNAMICS_DATA.vel(1)/V_scale,RIS.FRAGMENTS(i).DYNAMICS_DATA.vel(2)/V_scale,RIS.FRAGMENTS(i).DYNAMICS_DATA.vel(3)/V_scale,'r'); % V_ME_i
            else
                quiver3(RIS.FRAGMENTS(i).DYNAMICS_DATA.cm_coord(1),RIS.FRAGMENTS(i).DYNAMICS_DATA.cm_coord(2),RIS.FRAGMENTS(i).DYNAMICS_DATA.cm_coord(3),RIS.FRAGMENTS(i).DYNAMICS_DATA.vel(1)/V_scale,RIS.FRAGMENTS(i).DYNAMICS_DATA.vel(2)/V_scale,RIS.FRAGMENTS(i).DYNAMICS_DATA.vel(3)/V_scale,'g'); % V_ME_i
            end
            pause(0.01)
        end
    end
end