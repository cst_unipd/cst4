function draw3Dv0_MD(ME,ME_surv)
%"DRAW" outputs all the "graphics", just to help intuition and de-bugging. 

%global geometry_data;
%global dynamic_data;
% global ME
% global link_data
% global t

%hold off;
% V_scale=500; %[1km/s will be a vector of lenght 1]
disp_local_coordinates=false;
if t == 0
	V_scale = V_scale/10;
end
% for i=1:1:length(ME)
i=ME_surv; % for each ME
    %Draw the ME centers of mass
    if ME(i).GEOMETRY_DATA.mass>0
        plot3(ME(ME_surv).DYNAMICS_DATA.cm_coord(1),ME(ME_surv).DYNAMICS_DATA.cm_coord(2),ME(ME_surv).DYNAMICS_DATA.cm_coord(3),'or','markers',ME(ME_surv).GEOMETRY_DATA.mass/100); %ME_i
        m_color=ME(i).FRAGMENTATION_DATA.threshold/ME(i).FRAGMENTATION_DATA.threshold0;
        hold on
        
        %Draws ME shapes
        if ME(i).GEOMETRY_DATA.shape_ID==1 %it is a plate or a box
%             if ME(i).GEOMETRY_DATA.dimensions(3)/mean(ME(i).GEOMETRY_DATA.dimensions(1:2))<0.1
%                 draw_plateV0(i,m_color,ME);
%             else
%                 draw_boxV0(i,m_color,ME);
%             end
            draw_boxV0(i,m_color,ME);
        elseif ME(i).GEOMETRY_DATA.shape_ID==2 || ME(i).GEOMETRY_DATA.shape_ID==3 % Sphere or Hollow Sphere 
            draw_sphereV0(i,m_color,ME);
        elseif ME(i).GEOMETRY_DATA.shape_ID==4 || ME(i).GEOMETRY_DATA.shape_ID==5 % Cylinder or Hollow Cylinder
            draw_cylinderV0(i,m_color,ME);
        end
        
        %Other things
        if disp_local_coordinates==true
            local_fr_X=Rot_l2g*[0.5;0;0];
            local_fr_Y=Rot_l2g*[0;0.5;0];
            local_fr_Z=Rot_l2g*[0;0;0.5];
            quiver3(ME(i).DYNAMICS_DATA.cm_coord(1),ME(i).DYNAMICS_DATA.cm_coord(2),ME(i).DYNAMICS_DATA.cm_coord(3),local_fr_X(1),local_fr_X(2),local_fr_X(3),'r');
            quiver3(ME(i).DYNAMICS_DATA.cm_coord(1),ME(i).DYNAMICS_DATA.cm_coord(2),ME(i).DYNAMICS_DATA.cm_coord(3),local_fr_Y(1),local_fr_Y(2),local_fr_Y(3),'b');
            quiver3(ME(i).DYNAMICS_DATA.cm_coord(1),ME(i).DYNAMICS_DATA.cm_coord(2),ME(i).DYNAMICS_DATA.cm_coord(3),local_fr_Z(1),local_fr_Z(2),local_fr_Z(3),'g');
        end
        
        %Draws ME velocity vector (scaled)
%         quiver3(ME(i).DYNAMICS_DATA.cm_coord(1),ME(i).DYNAMICS_DATA.cm_coord(2),ME(i).DYNAMICS_DATA.cm_coord(3),ME(i).DYNAMICS_DATA.vel(1)/V_scale,ME(i).DYNAMICS_DATA.vel(2)/V_scale,ME(i).DYNAMICS_DATA.vel(3)/V_scale,'r'); % V_ME_i
    end
% end
%Draws links
% n_links=size(link_data,2);
% for s=1:1:n_links
% %     A=link_data(s).ME_connect; % Gets the Id of the ME_SR that are connected by the link
%     i=find([ME.object_ID]==link_data(s).ME_connect(1)); % i_1 is the ME1 index
%     j=find([ME.object_ID]==link_data(s).ME_connect(2)); % i_1 is the ME2 index
%     if ~isempty(i) && ~isempty(j)
%         if link_data(s).state~=0 && all(ismember([i,j],[ME.object_ID]))
%             %connect ME_SR i and ME_SR j
%             plot3([ME(i).DYNAMICS_DATA.cm_coord(1),ME(j).DYNAMICS_DATA.cm_coord(1)],[ME(i).DYNAMICS_DATA.cm_coord(2),ME(j).DYNAMICS_DATA.cm_coord(2)],[ME(i).DYNAMICS_DATA.cm_coord(3),ME(j).DYNAMICS_DATA.cm_coord(3)],'k','LineWidth',2);
%         end
%     end
% end
%axis([-5 dynamic_data(1).lenght(1)*4 -1 dynamic_data(1).lenght(2)*4 -1 dynamic_data(1).lenght(2)*4 ])
% axis([-6 6 -6 6 -6 6]);
% xlabel('X')
% ylabel('Y')
% zlabel('Z')
% title_str=['Simulation at time ',num2str(t), ' s'];
%title(title_str);
% set(gca,'fontsize',18)
grid on
% view([45 15])
end


