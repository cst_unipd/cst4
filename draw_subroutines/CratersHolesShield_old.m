function [ME_last_hole_out,ME_surv_out,dm_tot] = CratersHolesShield(name_simu,ME,FRAGMENTS)

load(name_simu,'HOLES')
ME_last_hole=max([0 HOLES.object_ID]);
ME_last_hole_out=sprintf('%15d',ME_last_hole);
if ME_last_hole==length(ME)
    ME_surv=-1; % negative for the check
    ME_surv_out=sprintf('%10s','none');
    ME_last_hole_out=sprintf('%15d',ME_last_hole-1);
else
    for i=ME_last_hole+1:1:length(ME)
        if ME(i).GEOMETRY_DATA.mass>0
            ME(i).GEOMETRY_DATA.mass;
            ME_surv=i;
            ME_surv_out=sprintf('%10d',ME_surv);
            break;
        end
    end
end
mm=pwd;
input_folder=name_simu(1:strfind(name_simu,'step_')-1);
cd(input_folder)
step_tot=dir('step*');
n=length(step_tot)-1;
name=fullfile(input_folder,'F_DATA_');
centers2=[];
centers=[];
radii=[];
for ii=1:n
    in_file=[name num2str(ii)];
    if exist([in_file '.mat'],'file')==2
        % load all the collisions on the survivor layer
        load(in_file,'C_DATA');
        for jj=1:length(C_DATA)
            if  C_DATA(jj).TARGET.ID==ME_surv && C_DATA(jj).TARGET.type==1
                centers2=[centers2; reshape([C_DATA(jj).IMPACTOR.POINT],3,[])'];
                kk_c=[ii jj];
            end
        end
        % load all the collisions on the survivor layer
        load(in_file,'F_DATA');
        for jj=1:length(F_DATA)
            if ~(cellfun('isempty',F_DATA(jj))==1)
                if  F_DATA{jj}.Frag_ME.object_ID_index==ME_surv && F_DATA{jj}.Frag_ME.target_type==1
                    centers=[centers; F_DATA{jj}.Impact_Data.I_POS_F];
                    radii=[radii;F_DATA{jj}.Frag_Data(:,4)];
                    kk_f=[ii jj];
                end
            end
        end
    end
    disp(num2str(ii));
    clear C_DATA F_DATA;
end
CoM_G = unique(centers2,'rows');
clear centers2;
if exist('kk_f','var') || exist('kk_c','var')
    [~,~,az,el,~] = PLOT1_geometry_parameters_MD(ME,FRAGMENTS,ME_surv);
    set(gcf,'color','white');
    hold on;
    view(az,el);
    plot3(ME(ME_surv).DYNAMICS_DATA.cm_coord(1),ME(ME_surv).DYNAMICS_DATA.cm_coord(2),ME(ME_surv).DYNAMICS_DATA.cm_coord(3),'or','markers',ME(ME_surv).GEOMETRY_DATA.mass/100); %ME_i
    m_color=ME(ME_surv).FRAGMENTATION_DATA.threshold/ME(ME_surv).FRAGMENTATION_DATA.threshold0;
    hold on
    %Draws ME shapes
    if ME(ME_surv).GEOMETRY_DATA.shape_ID==1 %it is a plate or a box
        draw_boxV0(ME_surv,m_color,ME);
    elseif ME(ME_surv).GEOMETRY_DATA.shape_ID==2 || ME(ME_surv).GEOMETRY_DATA.shape_ID==3 % Sphere or Hollow Sphere
        draw_sphereV0(ME_surv,m_color,ME);
    elseif ME(ME_surv).GEOMETRY_DATA.shape_ID==4 || ME(ME_surv).GEOMETRY_DATA.shape_ID==5 % Cylinder or Hollow Cylinder
        draw_cylinderV0(ME_surv,m_color,ME);
    end
    grid on
    if ~isempty(CoM_G)
        for i=1:size(CoM_G,1)
            hold on
            plot3(CoM_G(i,1), CoM_G(i,2), CoM_G(i,3),'*r'); %ME_i
        end
    end
    if ~isempty(centers)
        for i=1:size(centers,1)
            hold on
            drawCircle3d(centers(i,1), centers(i,2), centers(i,3), radii(i), 0, 360,'LineWidth', 2, 'Color', 'k')
            hold on
            plot3(centers(i,1), centers(i,2), centers(i,3),'*b'); %ME_i
            axis equal
        end
    end
    grid on
    axis equal
    out_fig=['N:\CST3_da_64GB_17122018\Plot_Advanced_shield\',name_simu(strfind(name_simu,'Results')+8:strfind(name_simu,'\data')-1)];
    saveas(gcf,out_fig,'fig')
    saveas(gcf,out_fig,'png')
    close all
    cc=[centers;CoM_G];
    cm_dm = [mean(cc(:,1)),mean(cc(:,2)),mean(cc(:,3))];
    radius_dm = mean(vectorNorm3d(cc-repmat(cm_dm,size(cc,1),1)));
    dm1=num2str(pi*(radius_dm*1000)^2,'%10.2f');
    dm2=num2str((ME(ME_surv).GEOMETRY_DATA.dimensions(1)*ME(ME_surv).GEOMETRY_DATA.dimensions(2))*1000^2,'%10.2f');
else
    disp('No crater pattern to be displayed')
    dm1='0.00';
    if ME_surv>0
        dm2=num2str((ME(ME_surv).GEOMETRY_DATA.dimensions(1)*ME(ME_surv).GEOMETRY_DATA.dimensions(2))*1000^2,'%10.2f');
    else
        dm2='0.00';
    end
end
dm=[dm1,'/',dm2];
dm_tot=sprintf('%35s',dm);
cd(mm)