%> @file  PLOT3_statistics_on_IMPACT.m
%> @brief calculation of parameters and plots for fragments distributions
%> Copyright (c) 2017 CISAS - UNIVERSITY OF PADOVA - Dr L. Olivieri
function[]=PLOT3_statistics_on_IMPACT_UPGRADED(RIS,output_folder_fig,plot_flag)

global output_folder
input_folder=fullfile(output_folder,'data');
n_size=length(RIS.FRAGMENTS);

%% DELETE FRAGMENTS WITH MASS == 0
for i=n_size:-1:1
    if RIS.FRAGMENTS(i).GEOMETRY_DATA.mass==0
        RIS.FRAGMENTS(i)=[];
    else
        box = boundingBox3d(RIS.FRAGMENTS(i).GEOMETRY_DATA.c_hull);
        lx = abs(box(2)-box(1));
        ly = abs(box(4)-box(3));
        lz = abs(box(6)-box(5));
        RIS.FRAGMENTS(i).GEOMETRY_DATA.dimensions=[lx ly lz];
    end
end
n_size=length(RIS.FRAGMENTS);

%% DEFINE THE SYSTEM PARAMETES

% % IMPATOR(S)
% mp=0;
% qp=[0 0 0]';
% for j=1:length(RIS.FRAGMENTS)
%     mp=mp+RIS.FRAGMENTS(j).GEOMETRY_DATA.mass0;
%     qp=qp+RIS.FRAGMENTS(j).DYNAMICS_INITIAL_DATA.vel0*RIS.FRAGMENTS(1).GEOMETRY_DATA.mass0;
% end
% vp=qp/mp;
% % TARGET(S)
% mt=0;
% qt=[0 0 0]';
% Vol_t=0; % VOLUME
% for j=1:length(RIS.ME)
%     % if the ME mkass is changed, the object was the first target
%     mt=mt+RIS.ME(j).GEOMETRY_DATA.mass0;
%     qt=qt+RIS.ME(j).DYNAMICS_INITIAL_DATA.vel0*RIS.ME(1).GEOMETRY_DATA.mass0;
%     [K,V] = convhulln(RIS.ME(j).GEOMETRY_DATA.c_hull);
%     Vol_t=Vol_t+V;
%     
% end
% vt=qt/mt;

[mp,vp,mt,vt,FL]=targetimpactor_def_UPGRADE(input_folder); %
Vol_t=0; % VOLUME
for j=1:length(RIS.ME)
    [~,V] = convhulln(RIS.ME(j).GEOMETRY_DATA.c_hull);
    Vol_t=Vol_t+V;
end

if mp==0 && mt==0
    disp('Fragmentation data not available: target(s) and/or impactor(s) mass equal to 0')
    disp('Impact statistics not available')
else

    
    % WHOLE "SPACECRAFT"
    m_TOTAL=0;
    for i=1:length(RIS.ME)
        m_TOTAL=m_TOTAL+ RIS.ME(i).GEOMETRY_DATA.mass0;
    end
    
    %% CALCULATE AND REORDER THE MAIN PARAMETERS FOR DISTRIBUTIONS
    
    VCM=(vp.*mp+vt.*mt)./(mp+mt);
    VpR=vp-VCM;
    VtR=vt-VCM;
    rho=mt/Vol_t; %%%% mean density of fragments
    
    m1=zeros(1,n_size-1); % mass distributions
    v =zeros(1,n_size-1); % velocities distributions
    s1 =zeros(1,n_size-1);
    for i = 2:n_size
        m1(i-1)=RIS.FRAGMENTS(i).GEOMETRY_DATA.mass;
        v(i-1)=norm(RIS.FRAGMENTS(i).DYNAMICS_DATA.vel);
        s1(i-1)=mean(RIS.FRAGMENTS(i).GEOMETRY_DATA.dimensions);
    end
    v_min=min(v);
    v_max=max(v);
    
    m=sort(m1);
    s=sort(s1);
    
    if n_size==1 %patch to avoid only one fragment case (LO)
        n_size=n_size+1;
    end
    
    m_distr= logspace(log10(min(m)),3,n_size-1); % array for mass distributions
    s_distr= logspace(log10(min(s)),2,n_size-1); % array for size distribution
    
    rho_eq=1000; % LOFT equivalent density
    m_s= rho_eq.*s_distr.^3;  % NASA SBM equialent mass distribution
    
    % Cumulative numbers calculations
    % CST: m, N
    N=n_size-1:-1:1;
    
    %% PLOT DISTRIBUTIONS
    if plot_flag == 1
        h = figure('Position', [100, 50, 1050, 450],'Visible', 'on');
    else
        h = figure('Position', [100, 50, 1050, 450],'Visible', 'off');
    end
    subplot(1,2,1)
    set(gca,'fontsize',14)
    m_bess=m_distr;
    N3=7.32.*(m_bess).^(-0.8); % BESS
    a=find(N3<1);
    N3(a)=[];
    m_bess(a)=[];
    loglog(m,N,'r',m_bess,N3,'--g','LineWidth',2)
    legend('CST Breakup Algorithm','Bess')
    grid on
    set(gca,'fontsize',14)
    % xlim([0.008 100])
    % ylim([1 1e4])
    xlabel('fragment mass [kg]')
    ylabel('Cumulative number')
    title('Fragments mass distribution')
    
    subplot(1,2,2)
    set(gca,'fontsize',14)
    E1=0.5*(mp)*(norm(vp-vt))^2;
    EMR1=E1/m_TOTAL; % TOTAL mass
    
    % FASTT P=0.01
    P1=0.01;
    B=0.60+0.18*P1*(EMR1-40000)/EMR1;
    A=1.6290-1.6636*B;
    m_FAST1=m_distr;
    N1=A*(m_FAST1/m_TOTAL).^(-B); % FASTT
    a=find(N1<1);
    m_FAST1(a)=[];
    N1(a)=[];
    
    % FASTT P=1
    P2=1;
    B2=0.60+0.18*P2*(EMR1-40000)/EMR1;
    A2=1.6290-1.6636*B2;
    m_FAST001=m_distr;
    N12=A2*(m_FAST001/m_TOTAL).^(-B2); % FASTT P=1
    m_FAST001(a)=[];
    N12(a)=[];
    
    % NASA SBM
    
    N2=0.1*(((mp+mt)*vp/1000)^0.75).*(s_distr.^(-1.71)); % NASA SBM, V in km/s
    a=find(N2<1);
    N2(a)=[];
    s_distr1=s_distr;
    s_distr(a)=[];
    m_s(a)=[];
    
    if FL<1 || ((mp + mt)<m_TOTAL)
       % plot of subcatatstrophic 
       N222=0.1*((mp.*vp/1000)^0.75).*(s_distr1.^(-1.71)); % NASA SBM, V in km/s
       a=find(N222<1);
       N222(a)=[];
       s_distr1(a)=[];
    end
    
    
    loglog(m,N,'r',m_FAST1,N1,'--k',m_FAST001,N12,':b','LineWidth',2)
    legend('CST Breakup Algorithm','FAST with coupling P=0.01','FAST with coupling P=1')
    
    grid on
    set(gca,'fontsize',14)
    %xlim([0.008 100])
    ylim([1 1e4])
    xlabel('fragment mass [kg]')
    ylabel('Cumulative number')
    title('Fragments mass distribution (spacecraft)')
    
    tmpl = fullfile(output_folder_fig, 'Distributions_comparison');
    fnam=sprintf('%s',tmpl);
    saveas(h, fnam,'png')
    saveas(h, fnam,'fig')
    
    if plot_flag == 1
        h = figure('Position', [100, 50, 500, 500],'Visible', 'on');
    else
        h = figure('Position', [100, 50, 500, 500],'Visible', 'off');
    end
    if FL<1 || ((mp + mt)<m_TOTAL)
        loglog(s,N,'b',s_distr,N2,'r',s_distr1,N222,'--k','LineWidth',2)
    else
        loglog(s,N,'b',s_distr,N2,'r','LineWidth',2)
    end
    grid on
    set(gca,'fontsize',14)
    % xlim([0.008 5])
    % ylim([1 1e5])
    xlabel('fragment characteristic length [m]')
    ylabel('Cumulative number')
    title('Fragments distribution (spacecraft)')
    if FL<1 || ((mp + mt)<m_TOTAL)
        legend('CST Breakup Algorithm','NASA SBM 1998','NASA Subcat. 2001')
    else
        legend('CST Breakup Algorithm','NASA SBM 1998')
    end
    tmpl = fullfile(output_folder_fig, 'Size_distributions_comparison');
    fnam=sprintf('%s',tmpl);
    saveas(h, fnam,'png')
    saveas(h, fnam,'fig')
    
    %% total masses calculation
    
    mbess=0;
    mFAST1=0;
    mFAST001=0;
    mSBM=0;
    for i=1:length(m_bess)-1
        mbess=mbess+m_bess(i)*(N3(i)-N3(i+1));
    end
    for i=1:length(m_FAST1)-1
        mFAST1=mFAST1+m_FAST1(i)*(N12(i)-N12(i+1));
    end
    for i=1:length(m_FAST001)-1
        mFAST001=mFAST001+m_FAST001(i)*(N1(i)-N1(i+1));
    end
    for i=1:length(m_s)-1
        mSBM=mSBM+m_s(i)*(N2(i)-N2(i+1));
    end
    
    m_tot_cst=sum(m);
    m_tot_bess=mbess;
    m_tot_fast1=mFAST1;
    m_tot_fast001=mFAST001;
    m_tot_sbm=mSBM;

end