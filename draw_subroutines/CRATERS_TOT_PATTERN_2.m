% load the data and launch the plots of the first crater
clear all
close all

global output_folder
global n
global SHAPE_ID_LIST;


%% MODIFICARE
% simu_name='Sphere_vs_Whipple_Shield_random_dis';  %%% NAME OF SIMULATION
% n=121; %%%%%%%%%%%%%%%%%%%%%%%% max number of F_DATA
simu_name='Advanced_Shields_CASE1_Case1_d_Nylon_6_20181214T140700';
load('N:\CST3_da_64GB_17122018\Results\Advanced_Shields_CASE1_Case1_d_Nylon_6_20181214T140700\data\step_1944.mat','HOLES','FRAGMENTS','ME')
     
ME_last_hole=max([HOLES.object_ID]);
for i=ME_last_hole+1:1:length(ME)
    if ME(i).GEOMETRY_DATA.mass>0
        ME_surv=i;
        disp(i)
        break;
    end
end

output_folder=fullfile(simu_name);
sim_title_dir=fullfile(simu_name);

shape_id_list()
input_folder = fullfile(sim_title_dir,'data');
output_folder_fig=fullfile(output_folder,'figures');


cd(input_folder)
step_tot=dir('step*');
n=length(step_tot)-1;

cd('..')
cd('..')
load(fullfile(input_folder,'step_0'));

%%

tmpl = fullfile(output_folder_fig, 'TIZ_crater');

name=fullfile(input_folder,'F_DATA_');


centers=[];
radii=[];
if ~isempty(ME_last_hole)
    for ii=1:n
        in_file=[name num2str(ii)];
        % Check if F_DATA_i is empty, i.e. if the structure F_DATA is loaded
        if exist([in_file '.mat'],'file')==2
            load(in_file,'F_DATA');
            if ~(exist('F_DATA','var')==0)
                for jj=1:length(F_DATA)
                    if ~(cellfun('isempty',F_DATA(jj))==1)
                        if  F_DATA{jj}.Frag_ME.object_ID_index==ME_last_hole && F_DATA{jj}.Frag_ME.target_type==1
                            load(in_file,'C_DATA','main_loop_count','tf','ME','FRAGMENTS');
                            centers=[centers; F_DATA{jj}.Impact_Data.I_POS_F];
                            radii=[radii;F_DATA{jj}.Frag_Data(:,4)];
                            kk=[ii jj];
                            %                         pause
                        end
                    end
                end
            end
        end
        disp(num2str(ii));
        clear F_DATA;
    end
    if exist('kk','var')
        load([name num2str(kk(1))]);
        CoM_G = trasfg_vectors(centers',F_DATA{kk(2)}.Frag_ME.TFG)';
        [~,~,az,el,V_scale] = PLOT1_geometry_parameters_MD(ME,FRAGMENTS,ME_last_hole);
        h = figure('Position',[30 30 830 630],'Visible', 'on');
        set(gcf,'color','white');
        hold on;
        view(az,el);
        plot3(ME(ME_last_hole).DYNAMICS_DATA.cm_coord(1),ME(ME_last_hole).DYNAMICS_DATA.cm_coord(2),ME(ME_last_hole).DYNAMICS_DATA.cm_coord(3),'ob'); %ME_i
        m_color=ME(ME_last_hole).FRAGMENTATION_DATA.threshold/ME(ME_last_hole).FRAGMENTATION_DATA.threshold0;
        hold on
        %Draws ME shapes
        if ME(ME_last_hole).GEOMETRY_DATA.shape_ID==1 %it is a plate or a box
            draw_boxV0(ME_last_hole,m_color,ME);
        elseif ME(ME_last_hole).GEOMETRY_DATA.shape_ID==2 || ME(ME_last_hole).GEOMETRY_DATA.shape_ID==3 % Sphere or Hollow Sphere
            draw_sphereV0(ME_last_hole,m_color,ME);
        elseif ME(ME_last_hole).GEOMETRY_DATA.shape_ID==4 || ME(ME_last_hole).GEOMETRY_DATA.shape_ID==5 % Cylinder or Hollow Cylinder
            draw_cylinderV0(ME_last_hole,m_color,ME);
        end
        grid on
        for i=1:size(CoM_G,1)
            drawCircle3d(CoM_G(i,1), CoM_G(i,2), CoM_G(i,3), radii(i), 0, 360,'LineWidth', 2, 'Color', 'k')
            hold on
            plot3(CoM_G(i,1), CoM_G(i,2), CoM_G(i,3),'+k'); %ME_i
        end
    else
        disp('No crater pattern to be displayed')
    end
    
    %%
    
    centers2=[];
    radii=[];
    for ii=1:n
        in_file=[name num2str(ii)];
        % Check if F_DATA_i is empty, i.e. if the structure F_DATA is loaded
        if exist([in_file '.mat'],'file')==2
            load(in_file,'C_DATA');
            for jj=1:length(C_DATA)
                if  C_DATA(jj).TARGET.ID==ME_surv && C_DATA(jj).TARGET.type==1
                    centers2=[centers2; reshape([C_DATA(jj).IMPACTOR.POINT],3,[])'];
                    kk=[ii jj];
                    %                 disp(C_DATA(jj).TARGET.ID)
                    %                 centers2
                    %                 pause
                end
            end
        end
        disp(num2str(ii));
        clear C_DATA;
    end
    CoM_G = unique(centers2,'rows');
    clear centers2;
    if exist('kk','var')
        %     load([name num2str(kk(1))]);
        [~,~,az,el,V_scale] = PLOT1_geometry_parameters_MD(ME,FRAGMENTS,ME_surv);
        %     h = figure('Position',[30 30 830 630],'Visible', 'on');
        set(gcf,'color','white');
        hold on;
        view(az,el);
        plot3(ME(ME_surv).DYNAMICS_DATA.cm_coord(1),ME(ME_surv).DYNAMICS_DATA.cm_coord(2),ME(ME_surv).DYNAMICS_DATA.cm_coord(3),'or','markers',ME(ME_surv).GEOMETRY_DATA.mass/100); %ME_i
        m_color=ME(ME_surv).FRAGMENTATION_DATA.threshold/ME(ME_surv).FRAGMENTATION_DATA.threshold0;
        hold on
        %Draws ME shapes
        if ME(ME_surv).GEOMETRY_DATA.shape_ID==1 %it is a plate or a box
            draw_boxV0(ME_surv,m_color,ME);
        elseif ME(ME_surv).GEOMETRY_DATA.shape_ID==2 || ME(ME_surv).GEOMETRY_DATA.shape_ID==3 % Sphere or Hollow Sphere
            draw_sphereV0(ME_surv,m_color,ME);
        elseif ME(ME_surv).GEOMETRY_DATA.shape_ID==4 || ME(ME_surv).GEOMETRY_DATA.shape_ID==5 % Cylinder or Hollow Cylinder
            draw_cylinderV0(ME_surv,m_color,ME);
        end
        grid on
        for i=1:size(CoM_G,1)
            %         drawCircle3d(CoM_G(i,1), CoM_G(i,2), CoM_G(i,3), radii(i), 0, 360,'LineWidth', 2, 'Color', 'k')
            hold on
            plot3(CoM_G(i,1), CoM_G(i,2), CoM_G(i,3),'*r'); %ME_i
            axis equal
            %         pause
        end
        grid on
    else
        disp('No crater pattern to be displayed')
    end
else
    disp('The shield has been completely perforated')
end