function [button,v] = Stand_alone_start_UPGRADED(delay,files)
% questdlg function with timeout property
%
% Based on timeoutDlg by MathWorks Support Team
% https://uk.mathworks.com/matlabcentral/answers/96229-how-can-i-have-a-dialog-box-or-user-prompt-with-a-time-out-period
%
%
% Originally written by Kouichi C. Nakamura Ph.D.
% MRC Brain Network Dynamics Unit
% University of Oxford
% 08-Mar-2017 16:06:58
% Modified by Lorenzo Olivieri, Ph.D.
% in the framework of CST software development

f1 = findall(0, 'Type', 'figures');
ttt = timer('TimerFcn', {@closeit f1}, 'StartDelay', delay);
start(ttt);
dlg = @listdlg;
% Call the dialog
% button = dlg('Insert the name of the desired simulation, (wait time of 10.000 s)');

str = {files.name};
[button,v] = dlg('PromptString','Select a simulation folder:',...
                'SelectionMode','single',...
                'ListString',str);
            
% Delete the timer
if strcmp(ttt.Running, 'on')
  stop(ttt);
end
delete(ttt);

function closeit(src, event, f1)
disp('Time out, start standard Post-processing');
f2 = findall(0, 'Type', 'figure');
fnew = setdiff(f2, f1);
if ishandle(fnew)
  close(fnew);
end