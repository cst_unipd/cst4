function [button] = Post_processing_insert_box(delay)
% questdlg function with timeout property
%
% Based on timeoutDlg by MathWorks Support Team
% https://uk.mathworks.com/matlabcentral/answers/96229-how-can-i-have-a-dialog-box-or-user-prompt-with-a-time-out-period
%
%
% Originally written by Kouichi C. Nakamura Ph.D.
% MRC Brain Network Dynamics Unit
% University of Oxford
% 08-Mar-2017 16:06:58
% Modified by Lorenzo Olivieri, Ph.D.
% in the framework of CST software development

f1 = findall(0, 'Type', 'figures');
ttt = timer('TimerFcn', {@closeit f1}, 'StartDelay', delay);
start(ttt);
dlg = @questdlg;
% Call the dialog
button = dlg('Do you want to insert a folder?','Folder Insert','YES','CONTINUE WITH LAST SIMULATION','CONTINUE WITH LAST SIMULATION');
% Delete the timer
if strcmp(ttt.Running, 'on')
  stop(ttt);
end
delete(ttt);
if isempty(button)==true
    button='CONTINUE WITH LAST SIMULATION';
end


function closeit(src, event, f1)
disp('Time out, start standard Post-processing');
f2 = findall(0, 'Type', 'figure');
fnew = setdiff(f2, f1);
if ishandle(fnew)
  close(fnew);
end