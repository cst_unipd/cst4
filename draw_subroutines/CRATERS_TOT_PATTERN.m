% load the data and launch the plots of the first crater

global output_folder
global n
global SHAPE_ID_LIST;


%% MODIFICARE
% simu_name='Sphere_vs_Whipple_Shield_random_dis';  %%% NAME OF SIMULATION
% n=121; %%%%%%%%%%%%%%%%%%%%%%%% max number of F_DATA
simu_name='Advanced_Shields_CASE1_Case1_a_Al_20181214T134417';

output_folder=fullfile(simu_name);
sim_title_dir=fullfile(simu_name);

shape_id_list()
input_folder = fullfile(sim_title_dir,'data');
output_folder_fig=fullfile(output_folder,'figures');


cd(input_folder)
step_tot=dir('step*')
n=length(step_tot);

cd('..')
cd('..')
load(fullfile(input_folder,'step_0'));
% ME(3)=[];
% ME(1)=[];
% FRAGMENTS=[];
%% FINE MODIFICHE
%%


tmpl = fullfile(output_folder_fig, 'TIZ_crater');

name=fullfile(input_folder,'F_DATA_');



[~,~,az,el,V_scale] = PLOT1_geometry_parameters(ME,FRAGMENTS);
h = figure('Position',[30 30 830 630],'Visible', 'on');
set(gcf,'color','white');
hold on; axis equal; axis off;
view(az,el);
draw3Dv0(link_data,ME,0,V_scale); % draw all MEs
pause(0.01)
for ii=3:n
empty_falg=0;

in_file=[name num2str(ii)];

% Check if F_DATA_i is empty, i.e. if the structure F_DATA is loaded
if exist([in_file '.mat'],'file')==2
    load(in_file);
    if exist('F_DATA','var')==0
        empty_falg=1;
    end
else
    empty_falg=1;
end

if empty_falg==1
    disp('No available data in fragmentation files was found. Pleae check the simulation diary')
else
    load(in_file);
    n_C = length(F_DATA);
    j=1;
    for i=1:n_C
        if isfield(F_DATA{i},'Frag_ME')==true
            CRATER_plot_fragmentation_LO_UPGRADED(i,F_DATA,C_DATA,main_loop_count,tf,ME,FRAGMENTS,j,h,ii);
            j=j+1;
            hold on
            % j indicates the number of the figure, they are consecutive also
            % with data unplottable (i.e. being in the other half of the if
            % case)
        else
            disp(['No fragmentation was found for target ' num2str(i) ' of ' num2str(n_C) '.'])
        end
    end
end
disp(num2str(ii));
clear F_DATA;
end