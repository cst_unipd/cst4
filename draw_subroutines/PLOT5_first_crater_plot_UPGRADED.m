% load the data and launch the plots of the first crater

global output_folder
global n
global SHAPE_ID_LIST;


shape_id_list()
input_folder = fullfile(sim_title_dir,'data');
output_folder_fig=fullfile(output_folder,'figures');
tmpl = fullfile(output_folder_fig, 'crater');

name=fullfile(input_folder,'F_DATA_');



in_file=[name num2str(0)];
i1=0;
empty_falg=0;
while exist([in_file '.mat'],'file')==0 && i1<=(n+2) % stop at a step more than the max number of F_DATA
    i1=i1+1;
    in_file=[name num2str(i1)];
    % Check if F_DATA_i is empty, i.e. if the structure F_DATA is loaded
    if exist([in_file '.mat'],'file')==2
        load(in_file);
        if exist('F_DATA','var')==0
            in_file=[name num2str(0) '.mat'];
            empty_falg=1;
        end
    end
end

if i1>=n+2
    if empty_falg==1
        disp('No available data in fragmentation files was found. Pleae check the simulation diary')
    end
    disp('No fragmentation was found. Exit from craters plot')
else
    load(in_file);
    n_C = length(F_DATA);
    j=1;
    for i=1:n_C
        if isfield(F_DATA{i},'Frag_ME')==true
            plot_fragmentation_LO_UPGRADED(i,F_DATA,C_DATA,main_loop_count,tf,ME,FRAGMENTS,j);
            j=j+1; 
            % j indicates the number of the figure, they are consecutive also
            % with data unplottable (i.e. being in the other half of the if
            % case)
        else
            disp(['No fragmentation was found for target ' num2str(i) ' of ' num2str(n_C) '.'])
        end
    end   
end