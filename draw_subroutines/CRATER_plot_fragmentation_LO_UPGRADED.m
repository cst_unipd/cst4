function CRATER_plot_fragmentation_LO_UPGRADED(i,F_DATA,C_DATA,main_loop_count,tf,ME,FRAGMENTS,j_fig,h,iii)
global output_folder
% output_folder_fig=fullfile(output_folder,'figures');

if isempty(F_DATA{i})
    return;
end

axis_len = 0;
axis_len = max([axis_len F_DATA{i}.Frag_ME.lx F_DATA{i}.Frag_ME.ly F_DATA{i}.Frag_ME.lz]);
axis_len_b = max([F_DATA{i}.Frag_ME.lx F_DATA{i}.Frag_ME.ly F_DATA{i}.Frag_ME.lz]);


[~,~,~,~,V_SCALE] = PLOT1_geometry_parameters(ME,FRAGMENTS);


% ------------------
% plot impact points
%
%drawPoint3d(F_DATA{i}.Impact_Data.I_POS_G,'pm','MarkerSize',10);
mh = drawPoint3d(F_DATA{i}.Impact_Data.I_POS_G,'.r');
set(mh,'MarkerSize',30,'LineWidth',2);


% --------------------
% plot impact velocity
%
vel_scale_factor = axis_len_b/norm(F_DATA{i}.Impact_Data.I_VEL_F)/(0.1*V_SCALE);
I_VEL_G = (F_DATA{i}.Frag_ME.RFG*(F_DATA{i}.Impact_Data.I_VEL_F'))';
for ii=1:F_DATA{i}.Impact_Data.N_impact
%     I_vel_g = 0.5*axis_len_b*I_VEL_G(ii,:)/norm(I_VEL_G(ii,:));
    I_vel_g = vel_scale_factor*I_VEL_G(ii,:);
    drawVector3d(F_DATA{i}.Impact_Data.I_POS_G(ii,:)-1.01*I_vel_g, ...
                 I_vel_g, ...
                 'Color','r','LineWidth',2);
end
%}

% % ------------------------------
% % plot fragments CoM
%
% for k = 1:F_DATA{i}.Impact_Data.N_frag_vol
%     CoM_G = trasfg_vectors(F_DATA{i}.frags_in_volume{k}.CoM',F_DATA{i}.Frag_ME.TFG)';
%     drawPoint3d(CoM_G,'.k');
% end


% -------------------------------
% plot the edges of the fragments
% this may take time
plot_frag_vol_flag = 0;
for k=1:length(F_DATA{i}.fragments)
    if isfield(F_DATA{i}.fragments{k},'edges')
        for j=1:length(F_DATA{i}.fragments{k}.edges)
            edges_g = trasfg_vectors(F_DATA{i}.fragments{k}.edges{j}',F_DATA{i}.Frag_ME.TFG)';
            h_edges = drawEdge3d([edges_g(1,:),edges_g(2,:)]);
            set(h_edges,'Color','b','LineStyle','--');
        end
    else
        plot_frag_vol_flag = 1;
    end
end

if plot_frag_vol_flag == 1
    for k=1:F_DATA{i}.Impact_Data.N_frag_vol
        if isSolidShape(F_DATA{i}.Frag_ME.shape_ID)
            %%%% ORIGINAL
            %drawMesh(F_DATA{i}.Frag_Volume{k}.v, F_DATA{i}.Frag_Volume{k}.f, ...
            %         'FaceColor', 'r','FaceAlpha',0.1);
            %%%% LO CORRECTION
            drawMesh(F_DATA{i}.Frag_Volume{k}.v-F_DATA{i}.Frag_Data(1:3)+F_DATA{i}.Impact_Data.I_POS_G(k,:), F_DATA{i}.Frag_Volume{k}.f, ...
                     'FaceColor', 'r','FaceAlpha',0.1);
        else
            vG = trasfg_vectors(F_DATA{i}.Frag_Volume{k}.v_cart,F_DATA{i}.Frag_ME.TFG);
            drawPoint3d(vG,'*r');
        end
    end
end

% ---
% plot velocity
%
v_CM = C_DATA(i).V_CM';
for k = 1:F_DATA{i}.Impact_Data.N_frag_vol
    CoM_G = trasfg_vectors(F_DATA{i}.frags_in_volume{k}.CoM',F_DATA{i}.Frag_ME.TFG)';
    vel_G = (F_DATA{i}.Frag_ME.RFG*(F_DATA{i}.frags_in_volume{k}.vel'))' + v_CM;
    drawVector3d(CoM_G(1:end,:), ...
                 90*vel_scale_factor*vel_G(1:end,:), ...
                 'Color','r','LineWidth',1);
end


pause(0.01)
hold on
end