global output_folder
global n
output_folder_fig=fullfile(output_folder,'figures');

text = fileread('first_page.txt.m');
disp(text)

disp(['FINAL REPORT - CST Simulation ' date])
disp('Copyright by CISAS - University of Padova')
disp('Developed in the framework of contract ESA - AO1-714012')

%% 1 - SIMULATION REPORT
tmpl = fullfile(output_folder, 'simu_report.txt');
fnam=sprintf('%s',tmpl);
if exist(fnam)~=0
%     text = fileread('simu_report.txt');
    text = fileread(tmpl);
    disp(text)
else
    disp('No report on simulation is found')
end

%% 2 - MAIN FIGURES

%% 2.1 Simulation steps figures
%Plot of initial conditions
tmpl = fullfile(output_folder_fig, 'user_fig');
fnam0=sprintf('%s_%d.png',tmpl,0);
if exist(fnam0)~=0
    %disp('Plot of initial conditions:')
    img = imread(fnam0);
    img=imresize(img, 0.5);
    imshow(img);
    pause(0.001)
    image(img);
    pause(0.001)
    axis off;
end

%Plot of first step:
fnam1=sprintf('%s_%d.png',tmpl,0);
if exist(fnam1)~=0
    %disp('Plot of first step:')
    img = imread(fnam1);
    img=imresize(img, 0.5);
    imshow(img);
    pause(0.001)
    image(img);
    pause(0.001)
    axis off;
end

%%
%Plot of last step
fnamF=sprintf('%s_%d.png',tmpl,n);
%disp(['Plot of last simulated step at time ' num2str(t),])
if exist(fnamF)~=0
    img = imread(fnamF);
    img=imresize(img, 0.5);
    imshow(img);
    pause(0.001)
    image(img);
    pause(0.001)
    axis off;
end

%% 2.2 Distributions figures
% Plot of mass distributions
tmpl = fullfile(output_folder_fig, 'Distributions_comparison.png');
fnamA=sprintf('%s',tmpl);
if exist(fnamA)~=0
    %disp(['Plot of mass distribution:' num2str(t),])
    img = imread(fnamA);
    img=imresize(img, 0.7);
    imshow(img);
    pause(0.001)
    image(img);
    pause(0.001)
    axis off;
end

%%
% Plot of size distributions
tmpl = fullfile(output_folder_fig, 'Size_distributions_comparison.png');
fnam=sprintf('%s',tmpl);
if exist(fnam)~=0
    %disp(['Plot of mass distribution:' num2str(t),])
    img = imread(fnam);
    img=imresize(img, 0.7);
    imshow(img);
    pause(0.001)
    image(img);
    pause(0.001)
    axis off;
end

%%
% Plot of area to mass distribution
tmpl = fullfile(output_folder_fig, 'A_m_ratio_distr.png');
fnam=sprintf('%s',tmpl);
if exist(fnam)~=0
    img = imread(fnam);
    img=imresize(img, 0.7);
    imshow(img);
    pause(0.001)
    image(img);
    pause(0.001)
    axis off;
end

%%
% Plot of velocity distribution
tmpl = fullfile(output_folder_fig, 'Velocity_distr.png');
fnam=sprintf('%s',tmpl);
if exist(fnam)~=0
    img = imread(fnam);
    img=imresize(img, 0.7);
    imshow(img);
    pause(0.001)
    image(img);
    pause(0.001)
    axis off;
end

%% 2.3 Craters plot
% Plot of first impact crater
i1=1;
tmpl=fullfile(output_folder_fig,'Crater_plot_');
fnam1=sprintf('%s_%d.png',tmpl,i1);
while exist(fnam1)~=0
    img = imread(fnam1);
    img=imresize(img, 0.7);
    figure
    imshow(img);
    pause(0.001)
    axis off;
    i1=i1+1;
    tmpl=fullfile(output_folder_fig,'Crater_plot_');
    fnam1=sprintf('%s_%d.png',tmpl,i1);
end
pause(0.1);