% Function needed for the pre-set of the output

i=5


sim_title_dir=fullfile('Results','PM3test3_TEST_20180227T122724');

input_folder = fullfile(sim_title_dir,'data');
output_folder = sim_title_dir;
output_folder_fig=fullfile(output_folder,'figures');
% name=sprintf('%s/step_',input_folder);
name=fullfile(input_folder,'step_');

in_file=[name num2str(i)];
RIS=load(in_file);
ME=RIS.ME;
FRAGMENTS=RIS.FRAGMENTS;
[C,dim,az,el,V_scale]=PLOT1_geometry_parameters(ME,FRAGMENTS);

% C=[-2,2,-2];
dim=20;
V_scale=600;
plot_flag=1;
flag_velocities=1;
flag_NO_c_hull=0;

clear RIS
in_file=[name num2str(i)];
RIS=load(in_file);
ME=RIS.ME;
FRAGMENTS=RIS.FRAGMENTS;
link_data=RIS.link_data;
t=RIS.tf;

h=PLOT2_basic_plot(link_data,ME,FRAGMENTS,t,C,dim,az,el,V_scale,plot_flag,flag_velocities,flag_NO_c_hull);
