%> @file  PLOT3_statistics_on_IMPACT.m
%> @brief calculation of parameters and plots for fragments distributions
%> Copyright (c) 2017 CISAS - UNIVERSITY OF PADOVA - Dr L. Olivieri
function[]=PLOT3_statistics_on_IMPACT(FRAGMENTS,output_folder,plot_flag)

n_size=length(FRAGMENTS);

%% DELETE FRAGMENTS WITH MASS == 0 (should be already ok)
for i=n_size:-1:1
    if FRAGMENTS(i).GEOMETRY_DATA.mass==0
        FRAGMENTS(i)=[];
    end
end
n_size=length(FRAGMENTS);

%% SIMPLIFY THE VARIABLES
FRAG_AM=zeros(n_size,2);
FRAG_VEL=zeros(n_size,2);

for i=1:n_size
    FRAG_AM(i,1)=mean(FRAGMENTS(i).GEOMETRY_DATA.dimensions);
    FRAG_AM(i,2)=FRAGMENTS(i).GEOMETRY_DATA.A_M_ratio;
    FRAG_VEL(i,1)=mean(FRAGMENTS(i).GEOMETRY_DATA.dimensions);
    FRAG_VEL(i,2)=norm(FRAGMENTS(i).DYNAMICS_DATA.vel);
end

%% Sort matrices
[~,idx] = sort(FRAG_AM(:,1)); % sort just the second column
FRAG_AM_S = FRAG_AM(idx,:);
[~,idx] = sort(FRAG_VEL(:,1)); % sort just the second column
FRAG_VEL_S = FRAG_VEL(idx,:);

%% PLOT A_m and vel distributions
if plot_flag == 1
    h = figure('Position', [100, 50, 1050, 450],'Visible', 'on');
else
    h = figure('Position', [100, 50, 1050, 450],'Visible', 'off');
end
subplot(1,2,1)
set(gca,'fontsize',14)
semilogx(FRAG_AM_S(:,1),FRAG_AM_S(:,2),'r.','LineWidth',2)
grid on
set(gca,'fontsize',14)
xlabel('fragment size [m]')
ylabel('Area/mass [m^2/kg]')
title('Fragments area to mass ')

subplot(1,2,2)
histogram(FRAG_AM_S(:,2),100)
xlabel('Area/mass [m^2 / kg]')
ylabel('number of fragments')
title('Fragments area to mass distribution')
grid on
set(gca,'fontsize',14)
%

if plot_flag == 1
    h1 = figure('Position', [100, 50, 1050, 450],'Visible', 'off');
else
    h1 = figure('Position', [100, 50, 1050, 450],'Visible', 'off');
end
subplot(1,2,1)
set(gca,'fontsize',14)
semilogx(FRAG_VEL_S(:,1),FRAG_VEL_S(:,2),'r.','LineWidth',2)
grid on
set(gca,'fontsize',14)
xlabel('fragment size [m]')
ylabel('fragment velocity [m/s]')
title('Fragments velocity')

subplot(1,2,2)
histogram(FRAG_VEL_S(:,2),100)
grid on
set(gca,'fontsize',14)
xlabel('fragment velocity [m/s]')
ylabel('number of fragments')
title('Fragments velocity distribution')

%% Save images
tmpl = fullfile(output_folder, 'A_m_ratio_distr');
fnam=sprintf('%s',tmpl);
saveas(h, fnam,'png')
saveas(h, fnam,'fig')

tmpl = fullfile(output_folder, 'Velocity_distr');
fnam=sprintf('%s',tmpl);
saveas(h1, fnam,'png')
saveas(h1, fnam,'fig')