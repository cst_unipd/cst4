function [ME_last_hole_out,ME_surv_out,dm_tot,imp_n] = CratersHolesShield(name_simu,ME,FRAGMENTS)

load(name_simu,'HOLES')
ME_last_hole=max([0 HOLES.object_ID]);
ME_last_hole_out=sprintf('%15d',ME_last_hole);
if ME_last_hole==length(ME)
    ME_surv=-1; % negative for the check
    ME_surv_out=sprintf('%10s','none');
    ME_last_hole_out=sprintf('%15d',ME_last_hole-1);
else
    for i=ME_last_hole+1:1:length(ME)
        if ME(i).GEOMETRY_DATA.mass>0
            ME(i).GEOMETRY_DATA.mass;
            ME_surv=i;
            ME_surv_out=sprintf('%10d',ME_surv);
            break;
        end
    end
end
mm=pwd;
input_folder=name_simu(1:strfind(name_simu,'step_')-1);
cd(input_folder)
step_tot=dir('step*');
n=length(step_tot)-1;
name=fullfile(input_folder,'F_DATA_');
centers2_last_hole=[];
centers_last_hole=[];
radii_last_hole=[];
centers2_surv=[];
centers_surv=[];
radii_surv=[];
for ii=1:n
    in_file=[name num2str(ii)];
    if exist([in_file '.mat'],'file')==2
        % load all the collisions on the survivor layer
        load(in_file,'C_DATA');
        for jj=1:length(C_DATA)
            if  C_DATA(jj).TARGET.ID==ME_surv && C_DATA(jj).TARGET.type==1
                centers2_surv=[centers2_surv; reshape([C_DATA(jj).IMPACTOR.POINT],3,[])'];
            end
            if  C_DATA(jj).TARGET.ID==ME_last_hole && C_DATA(jj).TARGET.type==1
                centers2_last_hole=[centers2_last_hole; reshape([C_DATA(jj).IMPACTOR.POINT],3,[])'];
                kk_c_last_hole=true;
            end
        end
        % load all the collisions on the survivor layer
        load(in_file,'F_DATA');
        for jj=1:length(F_DATA)
            if ~(cellfun('isempty',F_DATA(jj))==1)
                if  F_DATA{jj}.Frag_ME.object_ID_index==ME_surv && F_DATA{jj}.Frag_ME.target_type==1
                    centers_surv=[centers_surv; F_DATA{jj}.Impact_Data.I_POS_G];
                    radii_surv=[radii_surv;F_DATA{jj}.Frag_Data(:,4)];
                end
                if  F_DATA{jj}.Frag_ME.object_ID_index==ME_last_hole && F_DATA{jj}.Frag_ME.target_type==1
                    centers_last_hole=[centers_last_hole; F_DATA{jj}.Impact_Data.I_POS_G];
                    radii_last_hole=[radii_last_hole;F_DATA{jj}.Frag_Data(:,4)];
                    kk_f_last_hole=true;
                end
            end
        end
    end
    disp(num2str(ii));
    clear C_DATA F_DATA;
end


if ~isempty(centers2_surv) || ~isempty(centers_surv)
    CoM_G_surv = unique(centers2_surv,'rows');
    %     [~,~,az,el,~] = PLOT1_geometry_parameters_MD(ME,FRAGMENTS,ME_surv);
    az = 0;
    el = 90;
    set(gcf,'color','white');
    hold on;
    view(az,el);
    plot3(ME(ME_surv).DYNAMICS_DATA.cm_coord(1),ME(ME_surv).DYNAMICS_DATA.cm_coord(2),ME(ME_surv).DYNAMICS_DATA.cm_coord(3),'or','markers',ME(ME_surv).GEOMETRY_DATA.mass/100); %ME_i
    m_color=ME(ME_surv).FRAGMENTATION_DATA.threshold/ME(ME_surv).FRAGMENTATION_DATA.threshold0;
    hold on
    %Draws ME shapes
    if ME(ME_surv).GEOMETRY_DATA.shape_ID==1 %it is a plate or a box
        draw_boxV0(ME_surv,m_color,ME);
    elseif ME(ME_surv).GEOMETRY_DATA.shape_ID==2 || ME(ME_surv).GEOMETRY_DATA.shape_ID==3 % Sphere or Hollow Sphere
        draw_sphereV0(ME_surv,m_color,ME);
    elseif ME(ME_surv).GEOMETRY_DATA.shape_ID==4 || ME(ME_surv).GEOMETRY_DATA.shape_ID==5 % Cylinder or Hollow Cylinder
        draw_cylinderV0(ME_surv,m_color,ME);
    end
    grid on
    if ~isempty(CoM_G_surv)
        for i=1:size(CoM_G_surv,1)
            hold on
            plot3(CoM_G_surv(i,1), CoM_G_surv(i,2), CoM_G_surv(i,3),'*r'); %ME_i
        end
    end
    if ~isempty(centers_surv)
        for i=1:size(centers_surv,1)
            hold on
            drawCircle3d(centers_surv(i,1), centers_surv(i,2), centers_surv(i,3), radii_surv(i), 0, 360,'LineWidth', 1, 'Color', 'b')
            hold on
            plot3(centers_surv(i,1), centers_surv(i,2), centers_surv(i,3),'*b'); %ME_i
            axis equal
        end
    end
    grid on
    axis equal
    out_fig=['N:\CST3_da_64GB_17122018\Plot_Advanced_shield\',name_simu(strfind(name_simu,'Results')+8:strfind(name_simu,'\data')-1),'_surv'];
    title('1^{st} Not-Perforated Layer')
    cc_surv=[centers_surv;CoM_G_surv];
    cm_dm = [mean(cc_surv(:,1)),mean(cc_surv(:,2)),mean(cc_surv(:,3))];
    radius_dm = max(vectorNorm3d(cc_surv-repmat(cm_dm,size(cc_surv,1),1)));
    hold on
    drawCircle3d(cm_dm(1), cm_dm(2), cm_dm(3), radius_dm, 0, 360,'LineWidth', 1, 'Color', 'b')
    saveas(gcf,out_fig,'fig')
    saveas(gcf,out_fig,'png')
    imp_n=sprintf('%10d',size(cc_surv,1));
    Area_dm=sum(pi*(1000*radii_surv).^2);
    dm1=num2str(Area_dm,'%10.2f');
    dm2=num2str((ME(ME_surv).GEOMETRY_DATA.dimensions(1)*ME(ME_surv).GEOMETRY_DATA.dimensions(2))*1000^2,'%10.2f');
else
    disp('No crater pattern to be displayed')
    dm1='0.00';
    if ME_surv>0
        dm2=num2str((ME(ME_surv).GEOMETRY_DATA.dimensions(1)*ME(ME_surv).GEOMETRY_DATA.dimensions(2))*1000^2,'%10.2f');
        imp_n=sprintf('%10s','0');
    else
        dm2='0.00';
        imp_n=sprintf('%10s','0');
    end
end
figure
%%
if ~isempty(centers2_last_hole) || ~isempty(centers_last_hole)
    CoM_G_last_hole = unique(centers2_last_hole,'rows');
    %     [~,~,az,el,~] = PLOT1_geometry_parameters_MD(ME,FRAGMENTS,ME_surv);
    az = 0;
    el = 90;
    set(gcf,'color','white');
    hold on;
    view(az,el);
    plot3(ME(ME_last_hole).DYNAMICS_DATA.cm_coord(1),ME(ME_last_hole).DYNAMICS_DATA.cm_coord(2),ME(ME_last_hole).DYNAMICS_DATA.cm_coord(3),'or','markers',3); %ME_i
    m_color=1;
    hold on
    %Draws ME shapes
    if ME(ME_last_hole).GEOMETRY_DATA.shape_ID==1 %it is a plate or a box
        draw_boxV0(ME_last_hole,m_color,ME);
    elseif ME(ME_last_hole).GEOMETRY_DATA.shape_ID==2 || ME(ME_last_hole).GEOMETRY_DATA.shape_ID==3 % Sphere or Hollow Sphere
        draw_sphereV0(ME_last_hole,m_color,ME);
    elseif ME(ME_last_hole).GEOMETRY_DATA.shape_ID==4 || ME(ME_last_hole).GEOMETRY_DATA.shape_ID==5 % Cylinder or Hollow Cylinder
        draw_cylinderV0(ME_last_hole,m_color,ME);
    end
    grid on
    if ~isempty(CoM_G_last_hole)
        for i=1:size(CoM_G_last_hole,1)
            hold on
            plot3(CoM_G_last_hole(i,1), CoM_G_last_hole(i,2), CoM_G_last_hole(i,3),'*r'); %ME_i
        end
    end
    if ~isempty(centers_last_hole)
        for i=1:size(centers_last_hole,1)
            hold on
            drawCircle3d(centers_last_hole(i,1), centers_last_hole(i,2), centers_last_hole(i,3), radii_last_hole(i), 0, 360,'LineWidth', 1, 'Color', 'k')
            hold on
            plot3(centers_last_hole(i,1), centers_last_hole(i,2), centers_last_hole(i,3),'*b'); %ME_i
            axis equal
        end
        cm_centers_last_hole = mean(centers_last_hole,1);
        vers_centers_last_hole = normalizeVector3d(centers_last_hole-repmat(cm_centers_last_hole,size(centers_last_hole,1),1));
        radii_centers_last_hole = max(vectorNorm3d(cm_centers_last_hole-vers_centers_last_hole.*repmat(radii_last_hole,1,size(vers_centers_last_hole,2))));
        hold on
        drawCircle3d(cm_centers_last_hole(1), cm_centers_last_hole(2), cm_centers_last_hole(3), radii_centers_last_hole, 0, 360,'LineWidth', 1, 'Color', 'm')
    end
    grid on
    axis equal
    cc_last_hole=[CoM_G_last_hole];
    cm_dm_last_hole = [mean(cc_last_hole(:,1)),mean(cc_last_hole(:,2)),mean(cc_last_hole(:,3))];
    radius_dm_last_hole = max(vectorNorm3d(cc_last_hole-repmat(cm_dm_last_hole,size(cc_last_hole,1),1)));
    hold on
    drawCircle3d(cm_dm_last_hole(1), cm_dm_last_hole(2), cm_dm_last_hole(3), radius_dm_last_hole, 0, 360,'LineWidth', 1, 'Color', 'b')
    title('Last Perforated Layer')
    out_fig=['N:\CST3_da_64GB_17122018\Plot_Advanced_shield\',name_simu(strfind(name_simu,'Results')+8:strfind(name_simu,'\data')-1),'_last_hole'];
    saveas(gcf,out_fig,'fig')
    saveas(gcf,out_fig,'png')
    imp_n_last_hole=sprintf('%10d',size(cc_last_hole,1));
    Area_dm_last_hole=sum(pi*(1000*radius_dm_last_hole).^2);
    dm1_last_hole=num2str(Area_dm,'%10.2f');
    dm2_last_hole=num2str((ME(ME_last_hole).GEOMETRY_DATA.dimensions(1)*ME(ME_last_hole).GEOMETRY_DATA.dimensions(2))*1000^2,'%10.2f');
else
    disp('No crater pattern to be displayed')
    dm1='0.00';
    if ME_last_hole>0
        dm2_last_hole=num2str((ME(ME_last_hole).GEOMETRY_DATA.dimensions(1)*ME(ME_last_hole).GEOMETRY_DATA.dimensions(2))*1000^2,'%10.2f');
        imp_n_last_hole=sprintf('%10s','0');
    else
        dm2_last_hole='0.00';
        imp_n_last_hole=sprintf('%10s','0');
    end
end
%%
dm=[dm1,'/',dm2];
dm_tot=sprintf('%35s',dm);
cd(mm)