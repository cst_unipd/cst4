function resulting_data=data_current(A_data,B_data,current_t)
if current_t == A_data.tf
    resulting_data = A_data;
elseif current_t == B_data.tf
    resulting_data = B_data;
else % tempo intermedio, propagazione lineare
   
    scale=(current_t-A_data.tf)/(B_data.tf-A_data.tf);
    ME=A_data.ME;
    if ~isempty(ME)
        for i=1:length(ME)
            ME(i).GEOMETRY_DATA.dimensions=(1-scale)*A_data.ME(i).GEOMETRY_DATA.dimensions+...
                scale*B_data.ME(i).GEOMETRY_DATA.dimensions;
            ME(i).GEOMETRY_DATA.thick=(1-scale)*A_data.ME(i).GEOMETRY_DATA.thick+...
                scale*B_data.ME(i).GEOMETRY_DATA.thick;
            ME(i).GEOMETRY_DATA.mass=(1-scale)*A_data.ME(i).GEOMETRY_DATA.mass+...
                scale*B_data.ME(i).GEOMETRY_DATA.mass;
            
            ME(i).DYNAMICS_DATA.cm_coord=(1-scale)*A_data.ME(i).DYNAMICS_DATA.cm_coord+...
                scale*B_data.ME(i).DYNAMICS_DATA.cm_coord;
            ME(i).DYNAMICS_DATA.vel=(1-scale)*A_data.ME(i).DYNAMICS_DATA.vel+...
                scale*B_data.ME(i).DYNAMICS_DATA.vel;
            ME(i).DYNAMICS_DATA.quaternions=(1-scale)*A_data.ME(i).DYNAMICS_DATA.quaternions+...
                scale*B_data.ME(i).DYNAMICS_DATA.quaternions;
            ME(i).DYNAMICS_DATA.w=(1-scale)*A_data.ME(i).DYNAMICS_DATA.w+...
                scale*B_data.ME(i).DYNAMICS_DATA.w;
            ME(i).DYNAMICS_DATA.v_exp=(1-scale)*A_data.ME(i).DYNAMICS_DATA.v_exp+...
                scale*B_data.ME(i).DYNAMICS_DATA.v_exp;
            ME(i).DYNAMICS_DATA.virt_momentum=(1-scale)*A_data.ME(i).DYNAMICS_DATA.virt_momentum+...
                scale*B_data.ME(i).DYNAMICS_DATA.virt_momentum;
            
            ME(i).FRAGMENTATION_DATA.threshold=(1-scale)*A_data.ME(i).FRAGMENTATION_DATA.threshold+...
                scale*B_data.ME(i).FRAGMENTATION_DATA.threshold;
        end
    end
    resulting_data.ME=ME;
    
    FRAGMENTS=A_data.FRAGMENTS;
    if ~isempty(FRAGMENTS)
        for i=1:length(FRAGMENTS)
            FRAGMENTS(i).GEOMETRY_DATA.dimensions=(1-scale)*A_data.FRAGMENTS(i).GEOMETRY_DATA.dimensions+...
                scale*B_data.FRAGMENTS(i).GEOMETRY_DATA.dimensions;
            FRAGMENTS(i).GEOMETRY_DATA.thick=(1-scale)*A_data.FRAGMENTS(i).GEOMETRY_DATA.thick+...
                scale*B_data.FRAGMENTS(i).GEOMETRY_DATA.thick;
            FRAGMENTS(i).GEOMETRY_DATA.mass=(1-scale)*A_data.FRAGMENTS(i).GEOMETRY_DATA.mass+...
                scale*B_data.FRAGMENTS(i).GEOMETRY_DATA.mass;
            
            FRAGMENTS(i).DYNAMICS_DATA.cm_coord=(1-scale)*A_data.FRAGMENTS(i).DYNAMICS_DATA.cm_coord+...
                scale*B_data.FRAGMENTS(i).DYNAMICS_DATA.cm_coord;
            FRAGMENTS(i).DYNAMICS_DATA.vel=(1-scale)*A_data.FRAGMENTS(i).DYNAMICS_DATA.vel+...
                scale*B_data.FRAGMENTS(i).DYNAMICS_DATA.vel;
            FRAGMENTS(i).DYNAMICS_DATA.quaternions=(1-scale)*A_data.FRAGMENTS(i).DYNAMICS_DATA.quaternions+...
                scale*B_data.FRAGMENTS(i).DYNAMICS_DATA.quaternions;
            FRAGMENTS(i).DYNAMICS_DATA.w=(1-scale)*A_data.FRAGMENTS(i).DYNAMICS_DATA.w+...
                scale*B_data.FRAGMENTS(i).DYNAMICS_DATA.w;
            FRAGMENTS(i).DYNAMICS_DATA.v_exp=(1-scale)*A_data.FRAGMENTS(i).DYNAMICS_DATA.v_exp+...
                scale*B_data.FRAGMENTS(i).DYNAMICS_DATA.v_exp;
            FRAGMENTS(i).DYNAMICS_DATA.virt_momentum=(1-scale)*A_data.FRAGMENTS(i).DYNAMICS_DATA.virt_momentum+...
                scale*B_data.FRAGMENTS(i).DYNAMICS_DATA.virt_momentum;
            
            FRAGMENTS(i).FRAGMENTATION_DATA.threshold=(1-scale)*A_data.FRAGMENTS(i).FRAGMENTATION_DATA.threshold+...
                scale*B_data.FRAGMENTS(i).FRAGMENTATION_DATA.threshold;
        end
    end
    
    resulting_data.FRAGMENTS=FRAGMENTS;
    
    BUBBLE=A_data.BUBBLE;
    if ~isempty(BUBBLE)
        for i=1:length(BUBBLE)
            BUBBLE(i).GEOMETRY_DATA.dimensions=(1-scale)*A_data.BUBBLE(i).GEOMETRY_DATA.dimensions+...
                scale*B_data.BUBBLE(i).GEOMETRY_DATA.dimensions;
            BUBBLE(i).GEOMETRY_DATA.thick=(1-scale)*A_data.BUBBLE(i).GEOMETRY_DATA.thick+...
                scale*B_data.BUBBLE(i).GEOMETRY_DATA.thick;
            BUBBLE(i).GEOMETRY_DATA.mass=(1-scale)*A_data.BUBBLE(i).GEOMETRY_DATA.mass+...
                scale*B_data.BUBBLE(i).GEOMETRY_DATA.mass;
            
            BUBBLE(i).DYNAMICS_DATA.cm_coord=(1-scale)*A_data.BUBBLE(i).DYNAMICS_DATA.cm_coord+...
                scale*B_data.BUBBLE(i).DYNAMICS_DATA.cm_coord;
            BUBBLE(i).DYNAMICS_DATA.vel=(1-scale)*A_data.BUBBLE(i).DYNAMICS_DATA.vel+...
                scale*B_data.BUBBLE(i).DYNAMICS_DATA.vel;
            BUBBLE(i).DYNAMICS_DATA.quaternions=(1-scale)*A_data.BUBBLE(i).DYNAMICS_DATA.quaternions+...
                scale*B_data.BUBBLE(i).DYNAMICS_DATA.quaternions;
            BUBBLE(i).DYNAMICS_DATA.w=(1-scale)*A_data.BUBBLE(i).DYNAMICS_DATA.w+...
                scale*B_data.BUBBLE(i).DYNAMICS_DATA.w;
            BUBBLE(i).DYNAMICS_DATA.v_exp=(1-scale)*A_data.BUBBLE(i).DYNAMICS_DATA.v_exp+...
                scale*B_data.BUBBLE(i).DYNAMICS_DATA.v_exp;
            BUBBLE(i).DYNAMICS_DATA.virt_momentum=(1-scale)*A_data.BUBBLE(i).DYNAMICS_DATA.virt_momentum+...
                scale*B_data.BUBBLE(i).DYNAMICS_DATA.virt_momentum;
            
            BUBBLE(i).FRAGMENTATION_DATA.threshold=(1-scale)*A_data.BUBBLE(i).FRAGMENTATION_DATA.threshold+...
                scale*B_data.BUBBLE(i).FRAGMENTATION_DATA.threshold;
        end
    end
    resulting_data.BUBBLE=BUBBLE;
    
    resulting_data.HOLES=A_data.HOLES;
    resulting_data.PROPAGATION_THRESHOLD=A_data.PROPAGATION_THRESHOLD;
    resulting_data.link_data=A_data.link_data;
    resulting_data.ti=current_t;
    resulting_data.tf=current_t;
    
end