%> @file  unbroken_targets.m
%> @brief ME FRAGMENTATION ALGORITHM v04 - main
%> @author dr. L. Olivieri (lorenzo.olivieri@unipd.it)
%======================================================================
%> @brief Function used to manage non fragmenting impacts
%>
%> This algorithm recieves the COLLISION_DATA from the CST breakup main 
%> and investigate the non fragmenting collisions, managing bubbles and 
%> bouncing objects.
%>
%> @param C_DATA: info on collisions detected from tracking
%> @param C_DATA1: info on non fragmenting collisions
%> @param KILL_LIST: all killed elements
%>
%> @retval KILL_LIST: all killed elements
%> @retval ME, FRAGMENTS, BUBBLE, HOLES update
%>
%======================================================================
function [KILL_LIST]=unbroken_targets(C_DATA,C_DATA1,KILL_LIST)

global BUBBLE
global ME_FR FRAGMENTS_FR
global HOLES

%% Studying the bubbles
n_C=length(C_DATA1);
for i=n_C:-1:1
    n_C1=length(C_DATA1(i).IMPACTOR);
    for j=n_C1:-1:1
        if C_DATA1(i).IMPACTOR(j).type==2 
            HOLES_creation_flag=bubble_damage(C_DATA1(i),j,BUBBLE(C_DATA1(i).IMPACTOR(j).ID),ME_FR(C_DATA1(i).TARGET.ID));
            if(HOLES_creation_flag)
                disp(['POTENTIAL FAILURE!: HOLES(',num2str(length(HOLES)),') created on ME(',num2str(C_DATA1(i).TARGET.ID),') due to BUBBLE impact'])
            end
            [v_NEW_B,~]=velocity_model_breakup(C_DATA1,i);
            BUBBLE_v_LOSS=C_DATA1(i).IMPACTOR(j).v_loss;
            BUBBLE_v_NEW=C_DATA1(i).IMPACTOR(j).vel-BUBBLE_v_LOSS;
            v_NEW_B2 = (ME_FR(C_DATA1(i).TARGET.ID).GEOMETRY_DATA.mass*v_NEW_B+...
                BUBBLE(C_DATA1(i).IMPACTOR(j).ID).GEOMETRY_DATA.mass*BUBBLE_v_NEW)/...
                (ME_FR(C_DATA1(i).TARGET.ID).GEOMETRY_DATA.mass+BUBBLE(C_DATA1(i).IMPACTOR(j).ID).GEOMETRY_DATA.mass);
            C_DATA1(i).TARGET.v_loss = C_DATA1(i).TARGET.v_loss + (v_NEW_B - v_NEW_B2);
            dbstop at 42 if any(isnan(C_DATA1(i).TARGET.v_loss));
            ME_FR(C_DATA1(i).TARGET.ID).GEOMETRY_DATA.mass=...
                ME_FR(C_DATA1(i).TARGET.ID).GEOMETRY_DATA.mass+BUBBLE(C_DATA1(i).IMPACTOR(j).ID).GEOMETRY_DATA.mass;
            BUBBLE(C_DATA1(i).IMPACTOR(j).ID).GEOMETRY_DATA.mass=0;
            
            C_DATA1(i).IMPACTOR(j)=[];
            if isempty(C_DATA1(i).IMPACTOR)
                [v_NEW,~]=velocity_model_breakup(C_DATA1,i);
                ME_FR(C_DATA1(i).TARGET.ID).DYNAMICS_DATA.vel=v_NEW;
                 C_DATA1(i).TARGET=[];
             end
        end
    end
end

%% IMPACTORS have already fragmented
for i=n_C:-1:1
    if(~isempty(C_DATA1(i).TARGET))
        n_C0=length(C_DATA1(i).IMPACTOR);
        flag_del=0;
        for i1=1:n_C0
            n_C1=length(C_DATA);
            for j=1:n_C1
                if ( ~isempty(C_DATA1(i).TARGET) && ~isempty(C_DATA1(i).IMPACTOR)  && C_DATA1(i).IMPACTOR(i1).type==C_DATA(j).TARGET.type &&  C_DATA1(i).IMPACTOR(i1).ID==C_DATA(j).TARGET.ID )
                    if C_DATA(j).F_L ~= 0
                        flag_del=1;
                    end
                end
            end
        end
        if flag_del==1
            [v_NEW,~]=velocity_model_breakup(C_DATA1,i);
            if C_DATA1(i).TARGET.type==1
                ME_FR(C_DATA1(i).TARGET.ID).DYNAMICS_DATA.vel=v_NEW;
            elseif C_DATA1(i).TARGET.type==0
                FRAGMENTS_FR(C_DATA1(i).TARGET.ID).DYNAMICS_DATA.vel=v_NEW;
            end
            C_DATA1(i).TARGET=[];
        end
    end
end

%% BOUNCING of single impacts
for i=n_C:-1:2                   
    if ~isempty(C_DATA1(i).TARGET) 
        if length(C_DATA1(i).IMPACTOR)==1
            for j=1:i-1
                if ( ~isempty(C_DATA1(i).TARGET) && ~isempty(C_DATA1(j).TARGET) && ~isempty(C_DATA1(i).IMPACTOR) && C_DATA1(i).IMPACTOR.type==C_DATA1(j).TARGET.type &&  C_DATA1(i).IMPACTOR.ID==C_DATA1(j).TARGET.ID && length(C_DATA1(j).IMPACTOR)==1)
                    m1= C_DATA1(i).TARGET.mass;
                    m2= C_DATA1(j).TARGET.mass;
                    [v1,~]=velocity_model_breakup(C_DATA1,i);
                    [v2,~]=velocity_model_breakup(C_DATA1,j);
                    E1=m1*(norm(C_DATA1(i).TARGET.vel-C_DATA1(i).V_CM))^2;
                    E2=m2*(norm(C_DATA1(j).TARGET.vel-C_DATA1(j).V_CM))^2;
                    [E,i_E]=max([C_DATA1(i).TARGET.type*E2/E1;C_DATA1(j).TARGET.type*E1/E2]);
                    
                    cond1= C_DATA1(i).TARGET.type==1 && m2/m1<0.001 && C_DATA1(i).IMPACTOR.V_imp>0.9;
                    cond2= C_DATA1(j).TARGET.type==1 && m1/m2<0.001 && C_DATA1(j).IMPACTOR.V_imp>0.9;
                    if ( (C_DATA1(i).TARGET.type+C_DATA1(j).TARGET.type)==1 && (E<0.1 || cond1 || cond2 ) ) 
                        ii=i;
                        jj=j;
                        if i_E==2 || cond2
                            ii=j;
                            jj=i;
                        end
                        
                        [v_NEWii,~]=velocity_model_breakup(C_DATA1,ii);
                        [v_NEWjj,~]=velocity_model_breakup(C_DATA1,jj);
                        m_TOT=C_DATA1(ii).TARGET.mass+C_DATA1(jj).TARGET.mass;
                        Q1=C_DATA1(ii).TARGET.mass*v_NEWii;
                        Q2=C_DATA1(jj).TARGET.mass*v_NEWjj;
                        vel=(Q1+Q2)/m_TOT;
                        ME_FR(C_DATA1(ii).TARGET.ID).DYNAMICS_DATA.vel=vel;
                        ME_FR(C_DATA1(ii).TARGET.ID).GEOMETRY_DATA.mass=ME_FR(C_DATA1(ii).TARGET.ID).GEOMETRY_DATA.mass+FRAGMENTS_FR(C_DATA1(jj).TARGET.ID).GEOMETRY_DATA.mass;
                        
                        KILL_LIST.FRAGMENTS=[KILL_LIST.FRAGMENTS; C_DATA1(jj).TARGET.ID];
                        FRAGMENTS_FR(C_DATA1(jj).TARGET.ID).GEOMETRY_DATA.mass=0;
                    else
                        v1N = ( (m1-m2)*v1 + 2*m2*v2 )/(m1+m2);
                        v2N = ( 2*m1*v1 + (m2-m1)*v2 )/(m1+m2);
                        
                        if C_DATA1(i).TARGET.type==1
                            ME_FR(C_DATA1(i).TARGET.ID).DYNAMICS_DATA.vel=v1N;
                        elseif C_DATA1(i).TARGET.type==0
                            FRAGMENTS_FR(C_DATA1(i).TARGET.ID).DYNAMICS_DATA.vel=v1N;
                        end
                        
                        if C_DATA1(j).TARGET.type==1
                            ME_FR(C_DATA1(j).TARGET.ID).DYNAMICS_DATA.vel=v2N;
                        elseif C_DATA1(j).TARGET.type==0
                            FRAGMENTS_FR(C_DATA1(j).TARGET.ID).DYNAMICS_DATA.vel=v2N;
                        end
                    end
                    
                    C_DATA1(i).TARGET=[];
                    C_DATA1(j).TARGET=[];
                    
                end
            end
        end
    end
end


%% BOUNCING of multiple impacts
for i=n_C:-1:1
    if (~isempty(C_DATA1(i).TARGET))
        for i1=1:length(C_DATA1(i).IMPACTOR)
            for j=1:i-1
                if i1<=length(C_DATA1(i).IMPACTOR) %%%%%
                    if (~isempty(C_DATA1(j).TARGET) && ~isempty(C_DATA1(i).TARGET) && ~isempty(C_DATA1(i).IMPACTOR(i1))&& C_DATA1(i).IMPACTOR(i1).type==C_DATA1(j).TARGET.type &&  C_DATA1(i).IMPACTOR(i1).ID==C_DATA1(j).TARGET.ID)
                        for j1=length(C_DATA1(j).IMPACTOR):-1:1
                            if ( ~isempty(C_DATA1(i).TARGET) && ~isempty(C_DATA1(j).IMPACTOR(j1)) && C_DATA1(j).IMPACTOR(j1).type==C_DATA1(i).TARGET.type &&  C_DATA1(j).IMPACTOR(j1).ID==C_DATA1(i).TARGET.ID)
                                m1= C_DATA1(i).TARGET.mass;
                                m2= C_DATA1(j).TARGET.mass;
                                [v1,~]=velocity_model_breakup(C_DATA1,i);
                                [v2,~]=velocity_model_breakup(C_DATA1,j);
                                Q = m1*v1 + m2*v2;
                                v_cm = Q/(m1+m2);
                                v1r = v1 - v_cm;
                                v2r = v2 - v_cm;
                                if norm(v1r)~=0
                                    ver_v1 = v1r/norm(v1r);
                                elseif norm(v2r)~=0
                                    ver_v1 = v2r/norm(v2r);
                                else
                                    dbstop at 168
                                end
                                v1=norm(v1r);
                                v2= norm(v2r);
                                v1N = ( (m1-m2)*v1 + 2*m2*v2 )/(m1+m2);
                                v2N = ( 2*m1*v1 + (m2-m1)*v2 )/(m1+m2);
                                v1N = v1N*ver_v1 + v_cm;
                                v2N = v2N*ver_v1 + v_cm;
                                C_DATA1(i).TARGET.v_loss= C_DATA1(i).TARGET.v_loss+(v1N-C_DATA1(i).TARGET.vel);
                                C_DATA1(j).TARGET.v_loss= C_DATA1(j).TARGET.v_loss+(v2N-C_DATA1(j).TARGET.vel);
%                                 dbstop at 178 if (any(isnan(C_DATA1(i).TARGET.v_loss)) || any(isnan(C_DATA1(j).TARGET.v_loss)));
                                C_DATA1(i).IMPACTOR(i1)=[];
                                C_DATA1(j).IMPACTOR(j1)=[];
                                if isempty(C_DATA1(i).IMPACTOR)
                                    [v_NEW_i,~]=velocity_model_breakup(C_DATA1,i); % CG, GS 05-07-18
                                    if(C_DATA1(i).TARGET.type==1)
                                        ME_FR(C_DATA1(i).TARGET.ID).DYNAMICS_DATA.vel=  v_NEW_i;
                                    else
                                        FRAGMENTS_FR(C_DATA1(i).TARGET.ID).DYNAMICS_DATA.vel=  v_NEW_i;
                                        
                                        %%modification %{
                                        
                                        if FRAGMENTS_FR(C_DATA1(i).TARGET.ID).FRAGMENTATION_DATA.param_add2>2
                                            
                                            ll=length(BUBBLE)+1;
                                            BUBBLE(ll).object_ID=FRAGMENTS_FR(C_DATA1(i).TARGET.ID).object_ID;
                                            BUBBLE(ll).object_ID_index=ll;
                                            BUBBLE(ll).material_ID=FRAGMENTS_FR(C_DATA1(i).TARGET.ID).material_ID;
                                            BUBBLE(ll).GEOMETRY_DATA.shape_ID=9;
                                            BUBBLE(ll).GEOMETRY_DATA.dimensions=FRAGMENTS_FR(C_DATA1(i).TARGET.ID).GEOMETRY_DATA.dimensions; %FF, I would use "mean(dist_i)", since the bubble conveys average informations.
                                            BUBBLE(ll).GEOMETRY_DATA.thick=0; %kg
                                            BUBBLE(ll).GEOMETRY_DATA.mass0=FRAGMENTS_FR(C_DATA1(i).TARGET.ID).GEOMETRY_DATA.mass0; %kg
                                            BUBBLE(ll).GEOMETRY_DATA.mass=FRAGMENTS_FR(C_DATA1(i).TARGET.ID).GEOMETRY_DATA.mass0; %kg
                                            BUBBLE(ll).GEOMETRY_DATA.A_M_ratio=(pi*BUBBLE(ll).GEOMETRY_DATA.dimensions(1)^2)/BUBBLE(ll).GEOMETRY_DATA.mass; %FF, I assume Frontal Area -> took out the "4*"
                                            BUBBLE(ll).GEOMETRY_DATA.c_hull=[0, 0, 0];
                                            BUBBLE(ll).DYNAMICS_INITIAL_DATA.cm_coord0=FRAGMENTS_FR(C_DATA1(i).TARGET.ID).DYNAMICS_INITIAL_DATA.cm_coord0;
                                            BUBBLE(ll).DYNAMICS_INITIAL_DATA.quaternions0=FRAGMENTS_FR(C_DATA1(i).TARGET.ID).DYNAMICS_INITIAL_DATA.quaternions0;
                                            BUBBLE(ll).DYNAMICS_INITIAL_DATA.vel0=FRAGMENTS_FR(C_DATA1(i).TARGET.ID).DYNAMICS_INITIAL_DATA.vel0; %FF using cm Velocity instead of mean vel.
                                            BUBBLE(ll).DYNAMICS_INITIAL_DATA.w0=FRAGMENTS_FR(C_DATA1(i).TARGET.ID).DYNAMICS_INITIAL_DATA.w0;
                                            BUBBLE(ll).DYNAMICS_INITIAL_DATA.v_exp0=0;
                                            BUBBLE(ll).DYNAMICS_DATA.cm_coord=BUBBLE(ll).DYNAMICS_INITIAL_DATA.cm_coord0;
                                            BUBBLE(ll).DYNAMICS_DATA.quaternions=BUBBLE(ll).DYNAMICS_INITIAL_DATA.quaternions0;
                                            BUBBLE(ll).DYNAMICS_DATA.vel=BUBBLE(ll).DYNAMICS_INITIAL_DATA.vel0;
                                            BUBBLE(ll).DYNAMICS_DATA.w=BUBBLE(ll).DYNAMICS_INITIAL_DATA.w0;
                                            BUBBLE(ll).DYNAMICS_DATA.v_exp=BUBBLE(ll).DYNAMICS_INITIAL_DATA.v_exp0;
                                            BUBBLE(ll).DYNAMICS_DATA.virt_momentum=[0;0;0];
                                            BUBBLE(ll).FRAGMENTATION_DATA.failure_ID=1;
                                            BUBBLE(ll).FRAGMENTATION_DATA.threshold0=40;
                                            BUBBLE(ll).FRAGMENTATION_DATA.threshold=40;
                                            BUBBLE(ll).FRAGMENTATION_DATA.breakup_flag=0;
                                            BUBBLE(ll).FRAGMENTATION_DATA.ME_energy_transfer_coef=0;
                                            BUBBLE(ll).FRAGMENTATION_DATA.cMLOSS=0;
                                            BUBBLE(ll).FRAGMENTATION_DATA.cELOSS=0;
                                            BUBBLE(ll).FRAGMENTATION_DATA.c_EXPL=0;
                                            BUBBLE(ll).FRAGMENTATION_DATA.seeds_distribution_ID = 0;
                                            BUBBLE(ll).FRAGMENTATION_DATA.seeds_distribution_param1 = 0;
                                            BUBBLE(ll).FRAGMENTATION_DATA.seeds_distribution_param2 = 0;
                                            BUBBLE(ll).FRAGMENTATION_DATA.param_add1=FRAGMENTS_FR(C_DATA1(i).TARGET.ID).object_ID_index;
                                            BUBBLE(ll).FRAGMENTATION_DATA.param_add2=FRAGMENTS_FR(C_DATA1(i).TARGET.ID).FRAGMENTATION_DATA.param_add2;
                                            FRAGMENTS_FR(C_DATA1(i).TARGET.ID).GEOMETRY_DATA.mass=0;
                                            disp(['Repeated FL=0 impacts: created Bubble with FRAGMENTS(',num2str(C_DATA1(i).TARGET.ID), ').']);
                                        end
                                        
                                        %%modification %}
                                        
                                    end
                                    C_DATA1(i).TARGET=[];
                                end
                                if isempty(C_DATA1(j).IMPACTOR)
                                    [v_NEW_j,~]=velocity_model_breakup(C_DATA1,j);  % CG, GS 05-07-18
                                    if(C_DATA1(j).TARGET.type==1)
                                        ME_FR(C_DATA1(j).TARGET.ID).DYNAMICS_DATA.vel=  v_NEW_j;
                                    else
                                        FRAGMENTS_FR(C_DATA1(j).TARGET.ID).DYNAMICS_DATA.vel=  v_NEW_j;
                                    end
                                    C_DATA1(j).TARGET=[];
                                end
                            end
                        end
                    end
                end
            end
        end
    end
end
