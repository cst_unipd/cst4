%> @file  RotationMatrix.m
%> @brief Construct rotation (or direction cosine) matrices from quaternions
%>
%======================================================================
%> @brief Construct rotation (or direction cosine) matrices from quaternions
%>
%> Construct rotation (or direction cosine) matrices from quaternions
%>
%> @param q = quaternion array q0 (q1 q2 q3)
%>
%> @retval R = 3x3xN rotation (or direction cosine) matrices
%>
%======================================================================


function R = RotationMatrix( q )
% function R = RotationMatrix( q )  or  R = q.RotationMatrix
% Construct rotation (or direction cosine) matrices from quaternions
% Input:
%  q        quaternion array q0 (q1 q2 q3)
% Output:
%  R        3x3xN rotation (or direction cosine) matrices

q=[q(1) q(2) q(3) q(4)];


e11 = q(1)^2;
e12 = q(1) * q(2);
e13 = q(1) * q(3);
e14 = q(1) * q(4);
e22 = q(2)^2;
e23 = q(2) * q(3);
e24 = q(2) * q(4);
e33 = q(3)^2;
e34 = q(3) * q(4);
e44 = q(4)^2;

R  = ...
    [ e11 + e22 - e33 - e44, 2*(e23 - e14), 2*(e24 + e13); ...
    2*(e23 + e14), e11 - e22 + e33 - e44, 2*(e34 - e12); ...
    2*(e24 - e13), 2*(e34 + e12), e11 - e22 - e33 + e44 ];

end % RotationMatrix
