%> @file  fragmentation_mode_case2.m
%> @brief Full ME FRAGMENTSation Algorithm\BREAKUP THRESHOLD\fragmentation_mode_case2
%>
%======================================================================
%> @brief Defintion of FL in case of Fragmentation mode 3: EMR and volume
%>
%> This algorithm evaluates, in function of the selected frgmentation
%> failure type, the object fragmentation level
%>
%> @param E = energy to mass ratio of the impact
%> @param T_Threshold_OLD =original threshold
%> @param C_D = C_DATA(i), i-th investigated collision
%> 
%> @retval T_Threshold_NEW = updated threshold
%> @retval F_L = fragmentation level
%> @retval Threshold_update = flag for threshold update
%>
%======================================================================

function [F_L, T_Threshold_NEW,Threshold_update]=fragmentation_mode_case3(E,T_Threshold_OLD,C_D)
global ME
global FRAGMENTS
global MATERIAL_LIST

[~, F_L_min, ~, ~, ~, ~, ~, ~, ~, ~] = Parameters_Setup();
F_L_coeff = 0.5; % HARD CODED
T_Threshold_NEW=T_Threshold_OLD;
Threshold_update=0;

E_TOT=sum(E(:,1))*F_L_coeff^3;
E_PROJ=sum(E(:,2));

D_TOT=T_Threshold_OLD-E_TOT;
D_PROJ=T_Threshold_OLD-E_PROJ;

if D_TOT<=0 % Complete fragmentation
    T_Threshold_NEW=0;
    F_L=1;
else        % No complete fragmentation
    Threshold_update=1;
    
    if D_PROJ<=0    % Local fragmentation (locally, damage over threshold)
        F_L=E_TOT/(T_Threshold_OLD);
        if F_L>1
            F_L = 1;
        end
        coeff=1.1;
        c_0=1;
        c_EXPL=C_D.TARGET.c_EXPL;
        if C_D.TARGET.type==1
            MAT_ID_T=ME(C_D.TARGET.ID).material_ID;
        elseif C_D.TARGET.type==0
            MAT_ID_T=FRAGMENTS(C_D.TARGET.ID).material_ID;
        end
        vol_target=C_D.TARGET.mass/MATERIAL_LIST(MAT_ID_T).density;
        r_crat = (coeff*F_L*(1+c_0*c_EXPL)*(vol_target)/(pi*4/3))^(1/3);
        if length(C_D.IMPACTOR)==1
            t_eq=C_D.IMPACTOR.t_EQ;
        else
            t_eq=1;
        end
        
        if F_L<F_L_min && r_crat<0.5*t_eq
            F_L=0;
        end
    else            % No fragmentation
        F_L=0;
        Threshold_update=0;
    end
end