clear all; close all; clc;


Frag_ME.a = 1;
Frag_ME.b = 0.7;
Frag_ME.c = 2.5;
Frag_ME.t = Frag_ME.a-Frag_ME.b;

ri = Frag_ME.b; re = Frag_ME.a; h = Frag_ME.c; t = Frag_ME.t;
Frag_ME.cylinder_e = [re re 0 re re h re]; % [p1 p2 r]
Frag_ME.cylinder_i = [re re t re re h-t ri]; % [p1 p2 r]
center = [re re h/2];

nTheta_cyl = 21; nH_cyl = 2;            % <<<=== TUNING PARAMETERS

% external and internal cylinders (cartesian coordinates)
[Frag_ME.ve_cartc, ~, Frag_ME.fe_cartc] = cylinderMesh_npnts(Frag_ME.cylinder_e,nTheta_cyl,nH_cyl); % 21,2
Frag_ME.ve_cartc_down = Frag_ME.ve_cartc(1:2:end,:);
Frag_ME.ve_cartc_top = Frag_ME.ve_cartc(2:2:end,:);

[Frag_ME.vi_cartc, ~, Frag_ME.fi_cartc] = cylinderMesh_npnts(Frag_ME.cylinder_i,nTheta_cyl,nH_cyl); % 21,2
Frag_ME.vi_cartc_down = Frag_ME.vi_cartc(1:2:end,:);
Frag_ME.vi_cartc_top = Frag_ME.vi_cartc(2:2:end,:);

% external and internal ellipsoids
rxe = re; rye = re; rze = h/2;
rxi = rxe - t;   ryi = rye - t;   rzi = rze - t;

Frag_ME.ellipsoid_e = [rxe rye rze rxe rye rze];
Frag_ME.ellipsoid_i = [rxe rye rze rxi ryi rzi];

nTheta_elp = 10; nPhi_elp = 20;         % <<<=== TUNING PARAMETERS

% exdternal and internal ellipsoids (cartesian coordinates)
elli_dome = [Frag_ME.ellipsoid_e, [0 0 0]]; % [xc yc zc a b c phi theta psi]
[Frag_ME.ve_carte, ~, Frag_ME.fe_carte] = ellipsoidMesh(elli_dome,'nTheta',nTheta_elp,'nPhi',nPhi_elp);
Frag_ME.ve_carte = unique(Frag_ME.ve_carte,'rows');
Frag_ME.fe_carte = minConvexHull(Frag_ME.ve_carte,1e-6);

elli_domi = [Frag_ME.ellipsoid_i, [0 0 0]]; % [xc yc zc a b c phi theta psi]
[Frag_ME.vi_carte, ~, Frag_ME.fi_carte] = ellipsoidMesh(elli_domi,'nTheta',nTheta_elp,'nPhi',nPhi_elp);
Frag_ME.vi_carte = unique(Frag_ME.vi_carte,'rows');
Frag_ME.fi_carte = minConvexHull(Frag_ME.vi_carte,1e-6);


target_points = [3 4 2;3 4 3;3 4 4;3 4 5;3 4 6;3 4 7;3 4 8.001;3 4 9;3 4 10];

line = createLine3d(center.*ones(size(target_points,1),3),target_points); % [3 4 3]

points_c_e = intersectLinesCylinder_ext(line,Frag_ME.cylinder_e); points_c_e = points_c_e((end/2)+1:end,:);
points_c_i = intersectLinesCylinder_ext(line,Frag_ME.cylinder_i); points_c_i = points_c_i((end/2)+1:end,:);

points_e_e = intersectLineEllipsoid(line,elli_dome); points_e_e = points_e_e((end/2)+1:end,:);
points_e_i = intersectLineEllipsoid(line,elli_domi); points_e_i = points_e_i((end/2)+1:end,:);
points_e = points_e_i + 0.3*(points_e_e-points_e_i);

points_c = transfPointsElp2CylHollow(points_e,Frag_ME.cylinder_e,Frag_ME.cylinder_i,elli_dome,elli_domi);



%%
figure()
set(gcf,'color','white');
hold on; axis equal; axis off;
view(62,0);

% ------------
% plot Frag RF
pltassi(trasm(eye(3),[0 0 0]'),'b','',1,0.5);

% draw internal cylinder
drawMesh(Frag_ME.vi_cartc,Frag_ME.fi_cartc,'FaceColor',[0 0 0],'FaceAlpha',0.2,'EdgeAlpha',0.5);
pid = patch(Frag_ME.vi_cartc_down(:,1),Frag_ME.vi_cartc_down(:,2),Frag_ME.vi_cartc_down(:,3),'red');
set(pid,'FaceColor',[0 0 0],'FaceAlpha',0.2,'EdgeAlpha',0.5);
pit = patch(Frag_ME.vi_cartc_top(:,1),Frag_ME.vi_cartc_top(:,2),Frag_ME.vi_cartc_top(:,3),'red');
set(pit,'FaceColor',[0 0 0],'FaceAlpha',0.2,'EdgeAlpha',0.5);

% draw external cylinder
drawMesh(Frag_ME.ve_cartc,Frag_ME.fe_cartc, 'FaceColor',[0 0 1],'FaceAlpha',0.05,'EdgeAlpha',0.3);
ped = patch(Frag_ME.ve_cartc_down(:,1),Frag_ME.ve_cartc_down(:,2),Frag_ME.ve_cartc_down(:,3),'red');
set(ped,'FaceColor',[0 0 1],'FaceAlpha',0.05,'EdgeAlpha',0.3);
pet = patch(Frag_ME.ve_cartc_top(:,1),Frag_ME.ve_cartc_top(:,2),Frag_ME.ve_cartc_top(:,3),'red');
set(pet,'FaceColor',[0 0 1],'FaceAlpha',0.05,'EdgeAlpha',0.3);

% draw internal ellipsoid
drawMesh(Frag_ME.vi_carte,Frag_ME.fi_carte,'FaceColor',[0 0 0],'FaceAlpha',0.2,'EdgeAlpha',0.5);

% draw external ellipsoid
drawMesh(Frag_ME.ve_carte,Frag_ME.fe_carte,'FaceColor',[0 0 1],'FaceAlpha',0.05,'EdgeAlpha',0.3);

drawPoint3d(center,'.r');

drawVector3d(center.*ones(size(points_c_e,1),3),points_c_e-center,'m');
drawPoint3d(points_c_e,'og');
% drawVector3d(center,points_c_i-center,'k');
drawPoint3d(points_c_i,'og');
drawPoint3d(points_c,'*m');

drawVector3d(center.*ones(size(points_e_e,1),3),points_e_e-center,'m');
drawPoint3d(points_e_e,'ob');
% drawVector3d(center,points_e_i(2,:)-center,'k');
drawPoint3d(points_e_i,'ob');
drawPoint3d(points_e,'*r');

