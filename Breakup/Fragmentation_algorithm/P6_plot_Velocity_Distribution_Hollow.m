function P6_plot_Velocity_Distribution_Hollow(Frag_ME,Impact_Data,frags_in_volume)

global SHAPE_ID_LIST;

    figure()
    set(gcf,'color','white');
    hold on; axis equal; axis off;
    view(30,40);

    % ------------
    % plot Frag RF
    pltassi(trasm(eye(3),[0 0 0]'),'b','',1,0.5);

    % ------------------
    % plot impact points
    drawPoint3d(Impact_Data.I_POS_F,'pm');

    % ----------------------
    % plot impact velocities
    for i=1:Impact_Data.N_impact
        drawVector3d(Impact_Data.I_POS_F(i,:), Impact_Data.I_VEL_F(i,:),'Color','m','LineWidth',2);
    end

    % --------------------
    % plot the Impacted ME
    if Frag_ME.shape_ID == SHAPE_ID_LIST.HOLLOW_ELLIPSOID
        drawMesh(Frag_ME.vi_cart_cc,Frag_ME.fi_cart_cc, 'FaceColor',[1 0 0],'FaceAlpha',0.2,'EdgeAlpha',0.1);
        drawMesh(Frag_ME.ve_cart_cc,Frag_ME.fe_cart_cc, 'FaceColor',[1 0 1],'FaceAlpha',0.05,'EdgeAlpha',0.1);
    else
        drawMesh(Frag_ME.vi_cart,Frag_ME.fi_cart, 'FaceColor',[0 0 0],'FaceAlpha',0.2,'EdgeAlpha',0.1);
        drawMesh(Frag_ME.ve_cart,Frag_ME.fe_cart, 'FaceColor',[0 0 1],'FaceAlpha',0.05,'EdgeAlpha',0.1);            
    end
    
    % ---------------------------------------
    % plot fragments CoM and velocity vectors
    if Frag_ME.shape_ID == SHAPE_ID_LIST.HOLLOW_ELLIPSOID
        for i=1:Impact_Data.N_frag_vol % 1:2*N_impact
            for ii=1:frags_in_volume{i}.ncells
                % plot CoM
                drawPoint3d(frags_in_volume{i}.CoM_cyl,'.');
                %drawPoint3d(frags_in_volume{i}.CoM,'*');
                % plot velocity
                drawVector3d(frags_in_volume{i}.CoM_cyl(ii,:), 0.4*frags_in_volume{i}.vel(ii,:));
            end
        end
    else
        for i=1:Impact_Data.N_frag_vol % 1:2*N_impact
            for ii=1:frags_in_volume{i}.ncells
                % plot CoM
                drawPoint3d(frags_in_volume{i}.CoM,'*');
                % plot velocity
                drawVector3d(frags_in_volume{i}.CoM(ii,:), 0.4*frags_in_volume{i}.vel(ii,:));
            end
        end
    end

end
