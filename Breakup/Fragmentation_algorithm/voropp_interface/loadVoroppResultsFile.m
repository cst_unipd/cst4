%> @file   loadVoroppResultsFile.m
%> @brief  Import data from a Voro++ results file
%> @author Andrea Valmorbida (andrea.valmorbida@unipd.it)

%==========================================================================
%> @brief Function used to import data from a Voro++ results file
%>
%> @param neper_folder Path to the neper folder with the voro++ file
%> @param neper_tess_file Name of the voro++ data file to be loaded
%>
%> @retval fragments Structure with fragments
%> @retval domain Structure with the fragmentation domain
%>
%==========================================================================
function [fragments,domain] = loadVoroppResultsFile(neper_folder,voropp_file)


%% open voropp file

filename = fullfile(neper_folder, voropp_file);
fid = fopen(filename);


%% Parsing Voro++ results file

ncell = numel(importdata(filename));
fragments.ncells = ncell;
fragments.seeds = zeros(ncell,3);
fragments.vorvx = cell(1,ncell);

for j=1:ncell

  line = fgetl(fid);

  if ~isempty(line)

    c = textscan(line,'%s');
    
    % get seed coordinate
    fragments.seeds(j,:) = str2double(c{1}(2:4));
    
    % get the number of vertices
    nvertex = str2double(c{1}(5));

    % get vertex coordinates
    for i=1:nvertex
      aaa = c{1}(5+i);
      aa = aaa{1};
      vs = textscan(aa,'(%f %f %f)','Delimiter',',');
      for ii=1:3
        fragments.vorvx{j}(i,ii) = vs{ii};
      end
    end

  end
  
end


%% Close voropp file
fclose(fid);


%%

domain = [];


end