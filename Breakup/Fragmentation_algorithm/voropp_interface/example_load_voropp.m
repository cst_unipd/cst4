% example_load_voropp.m

voropp_file = 'node_list.vol';
[fragments,domain] = loadVoroppResultsFile('.',voropp_file);


%% PLOT

figure();
hold on;

for j=1:fragments.ncell
    plot3(fragments.vorvx{j}(:,1),fragments.vorvx{j}(:,2),fragments.vorvx{j}(:,3),'.r');
    plot3(fragments.seeds(j,1),fragments.seeds(j,2),fragments.seeds(j,3),'.k');
    faces = minConvexHull(fragments.vorvx{j});
    drawMesh(fragments.vorvx{j},faces, 'FaceColor', 'g','FaceAlpha',0.05);
end

% xlim([-2 2]);
axis equal;
view(30,40);

