// main function for voro++ calling
//
// Author   : Andrea Valmorbida (CISAS / University of Padova)
// Email    : andrea.valmorbida@unipd.it
// Date     : 5th February 2018
// Copyright 2018 CISAS - UNIVERSITY OF PADOVA

#include <cstdio>
#include <cstdlib>
#include <map>
#include <vector>
#include <iostream>
#include <iterator>
#include <string>
#include <algorithm>
#include <cmath>

#ifdef MATLAB_MEX_FILE /* Is this file being compiled as a MEX-file? */
    #include "mex.h"
#endif

#include "voro++.hh"

// #define DBG
// #define SIM_CRPT_CELLS

using namespace std;
using namespace voro;


int voropp_main(double con_bounding_box[3][2],
                int con_num_blocks[3],
                double domain[][4],
                double seeds[][3],
                int np, int ns,
                int *frag_id, double *frag_volume,
                double frag_centroid[][3], int *frag_nvert,
                int *nvert_tot_ptr,
                vector< vector<double> > *frag_chull)
{
    
    // np => number of plains that define the frag domain
    // ns => number of seeds
    
    #ifdef DBG
    printf("chull started\n");
    #endif
    
    // Set up constants for the container geometry
    double x_min = con_bounding_box[0][0];
    double x_max = con_bounding_box[0][1];
    double y_min = con_bounding_box[1][0];
    double y_max = con_bounding_box[1][1];
    double z_min = con_bounding_box[2][0];
    double z_max = con_bounding_box[2][1];
    
    // Set up the number of blocks that the container is divided into
    const int n_x = con_num_blocks[0];
    const int n_y = con_num_blocks[1];
    const int n_z = con_num_blocks[2];

   	int i,j,k;
	double x,y,z;

	// Create a container with the geometry given above, and make it
	// non-periodic in each of the three coordinates. Allocate space for 8
	// particles within each computational block.
	container con(x_min,x_max,y_min,y_max,z_min,z_max,n_x,n_y,n_z,
			false,false,false,8);
    #ifdef DBG
	printf("---> container created\n");
    #endif

	// Add plane walls to the container
    vector <wall_plane> wwplane;
    for(i=0;i<np;i++)
    {
        wwplane.push_back(wall_plane(domain[i][0],domain[i][1],domain[i][2],domain[i][3]));
    }
    for(i=0;i<np;i++)
    {
        con.add_wall(wwplane[i]);
    }
    #ifdef DBG
	printf("---> walls created\n");
    #endif

	// Insert particles into the container
    for(i=0;i<ns;i++)
    {
        con.put(i,seeds[i][0],seeds[i][1],seeds[i][2]);
    }
    #ifdef DBG
    printf("---> seeds added\n");
    #endif

	// Compute the total volume of the Voronoi cells => used for debugging
    #ifdef DBG
	double vvol = con.sum_cell_volumes();
	printf("===>> Voronoi cell volume  = %f\n",vvol);
    #endif
    
    
    // take fragments ID, centroid, volume and number of vertices
    c_loop_all vl(con);
    voronoicell_neighbor c;
    
//     int frag_id[ns];
//     double frag_volume[ns];
//     double frag_centroid[ns][3];
//     int frag_nvert[ns];
    int nvert_tot = 0;
    int pid;
    double xx,yy,zz,rr;
    double cx,cy,cz;
    
    i = 0;
    if(vl.start()) do {
        if(con.compute_cell(c,vl)) {
            
            vl.pos(pid,xx,yy,zz,rr); // take particle id and position x,y,z (seed)
            c.centroid(cx,cy,cz); // relative centroid coordinates
            
            #ifdef SIM_CRPT_CELLS
            if(i==1){
                frag_id[i] = -10;
                frag_volume[i] = -10.0;
                frag_centroid[i][0] = 0.0;
                frag_centroid[i][1] = 0.0;
                frag_centroid[i][2] = 0.0;                
                frag_nvert[i] = 0;
            }
            else{
            #endif
                frag_id[i] = pid;
                frag_volume[i] = c.volume();
                frag_centroid[i][0] = cx+xx;
                frag_centroid[i][1] = cy+yy;
                frag_centroid[i][2] = cz+zz;
                frag_nvert[i] = c.p;
                nvert_tot += c.p;
            #ifdef SIM_CRPT_CELLS
            }
            #endif
            
            #ifdef DBG
            printf("==> id %d\n\t\t%g,%g,%g , %g\n",pid,xx,yy,zz,frag_volume[i]);
            #endif
            //i++;
        }
        i++;
    } while(vl.inc());
    #ifdef DBG
    printf("\ntotal number of vertices: %d\n",nvert_tot);
    #endif
    
    // take the vertices of each fragment
    #ifdef DBG
    double frag_c_hull[nvert_tot][3];
    #endif

    std::vector<double> vv;
    vector<double> row;
    //printf("ok1\n");
    j = 0;
//     int ii = 0;
    if(vl.start())
    {
        do
        {
            //printf("ok2\n");
            if(con.compute_cell(c,vl)) {
                //printf("ok3\n");
                vl.pos(pid,xx,yy,zz,rr); // take particle position x,y,z (seed)
                vv.clear();
                c.vertices(xx,yy,zz,vv);
                #ifdef DBG
                c.centroid(cx,cy,cz);
                printf("==> cell id %d\n",pid);
                printf("       volume: %g\n",c.volume());
                printf("       seeds coordinates: %g , %g , %g\n",xx,yy,zz);
                printf("       absolute centroid coordinates: %g , %g , %g\n",cx+xx,cy+yy,cz+zz);
                printf("       number of vertices: %d\n",c.p);
                i=0;
                #endif
                
                for(int k=0;k<3*c.p;k+=3)
                {
                    #ifdef DBG
                	//printf("          v%d: %g , %g , %g\n",i,vv[k],vv[k+1],vv[k+2]);
                    frag_c_hull[j][0] = vv[k];
                    frag_c_hull[j][1] = vv[k+1];
                    frag_c_hull[j][2] = vv[k+2];
                    #endif
                    row.clear();
                    row.push_back(vv[k]);
                    row.push_back(vv[k+1]);
                    row.push_back(vv[k+2]);
                    frag_chull->push_back(row);
                	j++;
                    #ifdef DBG
                    i++;
                    #endif
                }
            }
//             ii++;
        } while(vl.inc());
    }
    
    *nvert_tot_ptr = nvert_tot;
    
    // PRINT OUTPUT
    #ifdef DBG
    printf("\n"); printf("\n");
    printf("OUTPUT:\n");
    
    printf("\n");
    printf("-->> frag_id:\n");
    for(i=0;i<ns;i++)
        printf("%d\n",frag_id[i]);
    
    printf("\n");
    printf("-->> frag_volume:\n");
    for(i=0;i<ns;i++)
        printf("%g\n",frag_volume[i]);
    
    printf("\n");
    printf("-->> frag_centroid:\n");
    for(i=0;i<ns;i++)
        printf("%g , %g , %g\n",frag_centroid[i][0],frag_centroid[i][1],frag_centroid[i][2]);
    
    printf("\n");
    printf("-->> frag_nvert:\n");
    for(i=0;i<ns;i++)
        printf("%d\n",frag_nvert[i]);
    
    printf("\n");
    printf("-->> nvert_tot:\n%d\n",nvert_tot);
    
    printf("\n");
    printf("-->> frag_c_hull:\n");
    for(j=0;j<nvert_tot;j++)
    {
        printf("v%3d: %g , %g , %g\n",j,frag_c_hull[j][0],frag_c_hull[j][1],frag_c_hull[j][2]);
    }
    printf("\n");
    #endif
   
    wwplane.clear();

    #ifdef DBG
	printf("chull concluded\n\n");
    #endif

}
