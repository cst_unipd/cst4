%> @file   GenerateVoroppBashFile.m
%> @brief  Generate Voro++ bash file
%> @author Andrea Valmorbida (andrea.valmorbida@unipd.it)

%==========================================================================
%> @brief Generate Voro++ bash file
%> 
%> This function creates in the neper_folder a bash file with name
%> %<test_name>.sh
%>
%> @param neper_folder Path to the neper folder
%> @param test_name name of the test.
%> @param nseeds number of seeds in the domain
%> @param dim number of dimensionz of the domain (2D or 3D)
%> @param domain file name of the Voro++ domain
%>
%==========================================================================
function [voropp_command ] = GenerateVoroppBashFile(neper_folder,test_name,normals,centros,vertex)

global voropp_folder;
global hostname;
file_name = [neper_folder , test_name '.sh'];
neper_folder_win=strrep(neper_folder,'\','/');

if(isunix)
    nodefilename = [test_name '_seeds.msh'];
else
    if(strncmp(hostname,'fwfranale-01',12)==1)
        nodefilename = [test_name '_seeds.msh'];
    else
        nodefilename = [neper_folder_win ,test_name '_seeds.msh'];
    end
end
num_planes = size(normals,1);
dvect = zeros(num_planes,1);
for i=1:num_planes
    dvect(i) = normals(i,:)*(centros(i,:)');
end

planes = [normals,dvect];
% planes = unique(planes,'rows');
planes = uniquetol(planes,1e-9,'ByRows',true);
num_planes = size(planes,1);
if(isunix)
    voropp_command=sprintf('voro++ -v -c "%%i %%q %%w %%P %%v %%C" ');
else
    voropp_gen_folder=fullfile('..','..','..','..','External_libs',voropp_folder);
    voropp_command=sprintf('%svoro++.exe -v -c "%%i %%q %%w %%P %%v %%C" ',voropp_gen_folder);

    if(strncmp(hostname,'fwfranale-01',12))
        voropp_command=sprintf('%svoro++.exe -v -c "%%i %%q %%w %%P %%v %%C" ',voropp_folder);
    end
end

for i=1:num_planes
    voropp_command=[voropp_command,sprintf('-wp %.12f %.12f %.12f %.12f ',...
            planes(i,1),planes(i,2),planes(i,3),planes(i,4))];
end
voropp_command=[voropp_command,sprintf('%.12f %.12f %.12f %.12f %.12f %.12f "%s"',...
                min(vertex(:,1)),max(vertex(:,1)),...
                min(vertex(:,2)),max(vertex(:,2)),...
                min(vertex(:,3)),max(vertex(:,3)),nodefilename )];

% open file
fileID = fopen(file_name,'w');

%%
% fileID = fopen('pippo.sh','w');
% nodefilename = ['prova' '_seeds'];

fprintf(fileID,'outformat="%%i %%q %%w %%P %%v %%C"\n');
fprintf(fileID,['nodefile="' nodefilename '"\n\n']);

fprintf(fileID,'voro++ -v -c "$outformat" \\\n');
for i=1:num_planes
    fprintf(fileID,'-wp %.12f %.12f %.12f %.12f \\\n',...
            planes(i,1),planes(i,2),planes(i,3),planes(i,4));
end
fprintf(fileID,'%.12f %.12f %.12f %.12f %.12f %.12f "$nodefile"\n\n',...
                min(vertex(:,1)),max(vertex(:,1)),...
                min(vertex(:,2)),max(vertex(:,2)),...
                min(vertex(:,3)),max(vertex(:,3)) );

% exit
fprintf(fileID,'exit 0\n');
fprintf(fileID,'\n');

% close file
fclose(fileID);

end
