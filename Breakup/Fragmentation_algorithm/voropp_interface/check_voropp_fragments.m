function [frag_id_,frag_volume_,frag_centroid_,frag_nvert_,nvert_tot_,frag_c_hull_] = ...
            check_voropp_fragments(frag_id,frag_volume,frag_centroid,frag_nvert,nvert_tot,frag_c_hull)

    frag_volume_toll = 1e-24;
    frag_id = frag_id + 1; % c to matlab indexes
    
    ns = length(frag_id);
    
    % check on corrupted fragments
    kill_idx = [];
    for i=1:ns
        if frag_id(i)<0 || frag_volume(i)<frag_volume_toll
            kill_idx = [kill_idx i];
        end
    end
    
    % update frag_id, frag_volume, frag_centroid and frag_nvert
    frag_id_ = frag_id;
    frag_volume_ = frag_volume;
    frag_centroid_ = frag_centroid;
    frag_nvert_ = frag_nvert;
    nvert_tot_ = nvert_tot;
    frag_c_hull_ = frag_c_hull;

    if ~isempty(kill_idx)
        
        % kill corrupted fragments
        frag_id_(kill_idx) = [];
        frag_volume_(kill_idx) = [];
        frag_centroid_(kill_idx,:) = [];
        frag_nvert_(kill_idx) = [];
        
        % update nvert_tot
        nvert_tot_ = sum(frag_nvert_);
        
        % frag_c_hull_ remains equal to frag_c_hull, since corrupted cells
        % are not considered during the computation of frag_chull in
        % cst_voropp_cpp.h (line 156: if(con.compute_cell(c,vl)))
        
        warning('check_voropp_fragments: we have some corrupted fragments.');
        
    end
    

    
end