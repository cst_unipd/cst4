function [bnd_box,planes,seeds] = getVoroppInput(Frag_Domain,shape_ID)

% input
if isSolidShape(shape_ID)
    normals = Frag_Domain.normals;
    centros = Frag_Domain.centros;
    vertex = Frag_Domain.v;
    seeds = Frag_Domain.seeds;
    if size(Frag_Domain.seeds,1)==1
        seeds = Frag_Domain.centroid;
    end
else
    normals = Frag_Domain.normals_sph;
    centros = Frag_Domain.centros_sph;
    vertex = Frag_Domain.v_sph;
    seeds = Frag_Domain.seeds_sph;
    if size(Frag_Domain.seeds,1)==1
        seeds = Frag_Domain.centroid_sph;
    end
end

num_planes = size(normals,1);
dvect = zeros(num_planes,1);
for j=1:num_planes
    dvect(j) = normals(j,:)*(centros(j,:)');
end

planes = [normals,dvect];
% planes = unique(planes,'rows');
planes = uniquetol(planes,1e-9,'ByRows',true);
%num_planes = size(planes,1);

bnd_box = [min(vertex(:,1)),max(vertex(:,1));...
           min(vertex(:,2)),max(vertex(:,2));...
           min(vertex(:,3)),max(vertex(:,3))];

end