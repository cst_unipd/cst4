function [T_VEL_NEW, F_VEL_NEW]=velocity_model_fragmentation(T_vel,F_vel,T_mass,F_mass,COLLISION_DATA)

global ME_FR
global FRAGMENTS_FR

%F_vel=T_vel; % Initial velocity is the same

switch COLLISION_DATA.TARGET.type
    case 0
        T_cELOSS = 0.5*FRAGMENTS_FR(COLLISION_DATA.TARGET.ID).FRAGMENTATION_DATA.cELOSS;
    case 1
        T_cELOSS = 0.5*ME_FR(COLLISION_DATA.TARGET.ID).FRAGMENTATION_DATA.cELOSS;
    otherwise
        T_cELOSS = 0.2;
end

T_Q=T_mass*T_vel;
F_Q=F_mass*F_vel;
V_CM=(T_Q+F_Q)/(T_mass+F_mass);
% V_R_T=T_vel-V_CM;
% V_R_F=F_vel-V_CM;
E_R_in=0.5*(T_mass*norm(T_vel)^2+F_mass*norm(F_vel)^2);
E_loss=0.5*T_cELOSS*(T_mass*norm(T_vel)^2+F_mass*norm(F_vel)^2);
E_R_fin=E_R_in-E_loss;
ver=V_CM/norm(V_CM); % versor of target velocity (positive)

%% CALCULATION OF V_LOSS for TARGET and IMPACTOR
if T_mass==0
    V_R_NEW_F1=F_vel-V_CM;
    V_R_NEW_T1=V_R_NEW_F1;
else
    V_R_NEW_F=sign(dot(F_vel,ver))*sqrt(2*E_R_fin/(F_mass*(1+F_mass/T_mass)));  %%%%%%%%%%%%%%DEFINE THE SIGN!!!!!!!!!!!
    V_R_NEW_T=-(F_mass/T_mass)*V_R_NEW_F;
    V_R_NEW_T1=V_R_NEW_T*ver;
    V_R_NEW_F1=V_R_NEW_F*ver;
end

T_VEL_NEW=(V_CM+V_R_NEW_T1);
F_VEL_NEW=(V_CM+V_R_NEW_F1);