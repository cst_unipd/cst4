%> @file   plate_cut2.m
%> @brief  Cut of plate sides
%> @author Lorenzo Olivieri (lorenzo.olivieria@unipd.it) and Giulia Sarego

%==========================================================================
%> @brief subroutine to cut the plate shape in case of lateral impacts
%>
%> For a detailed description of the algorithm see the CST Matlab document
%>
%> @param C    index of ME plate to be modified
%> @param P    impact point (single impact)
%> @param R    radius of the impact shape
%> @param a    plate size 1
%> @param b    plate size 2
%>
%> @retval V   Matrix with new plate verteces as columns
%>
%> nota bene: interseca la piastra con il quadrato circoscritto!
%>
%==========================================================================
function [V]=Plate_cut2(C,P,R,a,b)

% INPUT:
%   C = plate center
%   P = impact point
%   R = impact radius
%   [a,b] = plate sizes in x and y
% OUTPUT:
%  V = matrix of the new plate verteces

global plot_flag;

debug_mode=0;
switch debug_mode
    case 1 % impoact @ right
        C=[0 0  ]';
        P=[1 0  ]';
        R=1.3;
        a=2.8;
        b=1.2;
    case 2 % impoact @ right, not aligned
        C=[0 0  ]';
        P=[1 0.4  ]';
        R=1.3;
        a=2.8;
        b=1.2;
    case 3 % impoact @ right, not aligned, 2
        C=[0 0  ]';
        P=[1 1,2  ];
        R=1.3;
        a=2.8;
        b=1.2;
    case 4 % impoact @ left
        C=[0 0  ]';
        P=[-1 0  ];
        R=1.3;
        a=2.8;
        b=1.2;
    case 5 % radius too small
        C=[0 0  ]';
        P=[-1 0  ];
        R=0.4;
        a=2.8;
        b=1.2;
end


% Plate verteces
V1=C+[a/2 b/2  ]';
V2=C+[a/2 -b/2 ]';
V3=C+[-a/2 -b/2]';
V4=C+[-a/2 b/2 ]';
V=[V1 V2 V3 V4];

% Create the fragmentation volume and its circumscribed square
vx=[V(1,:) V(1,1)];
vy=[V(2,:) V(2,1)];
t=linspace(0,2*pi,1000);
cx=P(1)+R*cos(t);
cy=P(2)+R*sin(t);
x_min = min(cx);
x_max = max(cx);
y_min = min(cy);
y_max = max(cy);
Q = [x_min x_min x_max x_max x_min; y_max y_min y_min y_max y_max];
%[Pix,Piy]= polyxpoly (vx,vy,Q(1,:),Q(2,:)); %intersezione tra plate e quadrato circoscritto  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if ~exist(fullfile(matlabroot,'toolbox','map','map','polyxpoly.m'),'file')
    [PP, ~] = getBoundariesIntersection(V',Q');
    Pix = PP(:,1);Piy = PP(:,2);
else
    [Pix,Piy]= polyxpoly (vx,vy,Q(1,:),Q(2,:));
end
Pi=[Pix'; Piy'];

if plot_flag == 1
    plot(vx,vy,'-k',cx,cy,'-r',Q(1,:),Q(2,:),'b',P(1),P(2),'*r','MarkerSize',6,'LineWidth',2)
end


tol_abs = 1.e-6*min([a,b]);
P1=[];
P2=[];
if min(size(Pix))>0
    
    if abs(max(Pix)-min(Pix)-a)<tol_abs % cut along x, at constant y
        
        Ri=zeros(1,length(Pi(1,:)));
        for i=1:length(Pi(1,:))
            Ri(i)=norm(Pi(:,i)-C); % distance intersections vs plate center
        end
        ii=find(Ri==min(Ri),1);
        P1=Pi(:,ii(1)); % nel dubbio che siano piu' di uno
        
        if abs(P1(1))<tol_abs
            Pi(:,abs(Pi(1,:))<tol_abs)=[];
            if length(Pix)>1
                Pi(:,abs(Pi(2,:) - P1(2))>tol_abs)=[];
            end
            P2=Pi;
        elseif abs(P1(1) - a)<tol_abs
            Pi(:,abs(Pi(1,:) - a)<tol_abs)=[];
            if length(Pix)>1
                Pi(:,abs(Pi(2,:) - P1(2))>tol_abs)=[];
            end
            P2=Pi;
        else
            error('Error in Plate_cut2.m')
        end
        
    elseif abs(max(Piy)-min(Piy)-b)<tol_abs % cut along y, at constant x
        
        Ri=zeros(1,length(Pi(1,:)));
        for i=1:length(Pi(1,:))
            Ri(i)=norm(Pi(:,i)-C); % distance intersections vs plate center
        end
        % intersection points: nearest to the center of the plate
        
        ii=find(Ri==min(Ri),1);
        P1=Pi(:,ii(1));
        if abs(P1(2))<tol_abs
            Pi(:,abs(Pi(2,:))<tol_abs)=[];
            if length(Pix)>1
                Pi(:,abs(Pi(1,:) - P1(1))>tol_abs)=[];
            end
            P2=Pi;
        elseif abs(P1(2) - b)<tol_abs
            Pi(:,abs(Pi(2,:) - b)<tol_abs)=[];
            if length(Pix)>1
                Pi(:,abs(Pi(1,:) - P1(1))>tol_abs)=[];
            end
            P2=Pi;
        else
            warning('Error in Plate_cut2.m')
        end
        
    else
        
        disp('No need to cut')
        
    end
    
    if and(~isempty(P1),~isempty(P2))
        
        if abs(abs(P1(1)-P2(1))-a)<tol_abs
            Ty = (P2(2)+P1(2))/2;
            T1 = [0; Ty]; % Cut points
            T2 = [a; Ty];
            if abs(P(1))<tol_abs || abs(P(1) - a)<tol_abs
                V1 = V(:,and( V(1,:) == P(1), ((sign((T1(2)-P(2))*(V(2,:)-P(2))))>0) ));
                V2 = V(:,and( ((sign((T2(1)-P(1))*(V(1,:)-P(1))))>0) , ((sign((T2(2)-P(2))*(V(2,:)-P(2))))>0) ));
            else
                V1 = V(:,and( ((sign((T1(1)-P(1))*(V(1,:)-P(1))))>0) , ((sign((T1(2)-P(2))*(V(2,:)-P(2))))>0) ));
                V2 = V(:,and( ((sign((T2(1)-P(1))*(V(1,:)-P(1))))>0) , ((sign((T2(2)-P(2))*(V(2,:)-P(2))))>0) ));
            end
            
        elseif abs(abs(P1(2)-P2(2))-b)<tol_abs
            
            Tx = P1(1)+ (P2(1)-P1(1))/2;
            T1 = [Tx; 0]; % Cut points
            T2 = [Tx; b];
            if abs(P(2))<tol_abs || abs(P(2) - b)<tol_abs
                V1 = V(:,and( ((sign((T1(1)-P(1))*(V(1,:)-P(1))))>0) , V(2,:) == P(2) ));
                V2 = V(:,and( ((sign((T2(1)-P(1))*(V(1,:)-P(1))))>0) , ((sign((T2(2)-P(2))*(V(2,:)-P(2))))>0) ));
            else
                V1 = V(:,and( ((sign((T1(1)-P(1))*(V(1,:)-P(1))))>0) , ((sign((T1(2)-P(2))*(V(2,:)-P(2))))>0) ));
                V2 = V(:,and( ((sign((T2(1)-P(1))*(V(1,:)-P(1))))>0) , ((sign((T2(2)-P(2))*(V(2,:)-P(2))))>0) ));
            end
            
        else
            P1
            P2
            warning('Error in Plate_cut2.m')
        end
        
        V_new=[ V1 V2 T2 T1];
        V_c = convhull(V_new(1,:),V_new(2,:));
        if ~exist(fullfile(matlabroot,'toolbox','map','map','poly2cw.m'),'file')
            Vel_T=[V_new(1,V_c)' V_new(2,V_c)'];
            Vel_T=angleSort(Vel_T);
            Vel_T = Vel_T(end:-1:1,:);
            Vx=Vel_T(:,1);
            Vy=Vel_T(:,2);
        else
            [Vx, Vy] = poly2cw(V_new(1,V_c), V_new(2,V_c)); % Reordering verteces
        end
        V=[Vx(1:4);Vy(1:4)];
        
        if plot_flag == 1
            figure
            hold on
            subplot(2,1,1),plot(vx,vy,'b',cx,cy,'r',P(1),P(2),'k*',P1(1),P1(2),'r*',P2(1),P2(2),'r*');
            subplot(2,1,2),plot([Vx Vx(1)],[Vy Vy(1)],'b',cx,cy,'r',P1(1),P1(2),'k*',P2(1),P2(2),'k*');
        end
        disp('-> Performed cut')
    else
        disp('No need to cut')
    end
else
    disp('No need to cut')
end

end