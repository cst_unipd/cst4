function P1_plot_3Dscenario_FragFrame(Frag_ME,Impact_Data,Frag_Volume)

global SHAPE_ID_LIST;

%%
    figure()
    set(gcf,'color','white');
    hold on; axis equal; axis off;
    view(30,40);

    % ------------
    % plot Frag RF
    pltassi(trasm(eye(3),[0 0 0]'),'b','',1,0.5);

    % ------------------
    % plot impact points
    %
    drawPoint3d(Impact_Data.I_POS_F,'pm');
    drawPoint3d(Impact_Data.I_PNT_F,'pb');
    for i=1:Impact_Data.N_impact
        text(Impact_Data.I_POS_F(i,1)+0.1, ...
             Impact_Data.I_POS_F(i,2)-0.1, ...
             Impact_Data.I_POS_F(i,3),num2str(i), ...
             'color','k','FontSize',20);
    end
    %}
    
    % ----------------------
    % plot impact velocities
    %
    for i=1:Impact_Data.N_impact
%         drawVector3d(Impact_Data.I_POS_F(i,:), ...
%                      1e-3*Impact_Data.I_VEL_F(i,:), ...
%                      'Color','m','LineWidth',2);
    end
    %}

    % --------------------
    % plot the Impacted ME
    if isSolidShape(Frag_ME.shape_ID)
        drawMesh(Frag_ME.v, Frag_ME.f, 'FaceColor',[0 0 0],'FaceAlpha',0.02);
%         for i=1:size(Frag_ME.v,1)
%             text(Frag_ME.v(i,1)+0.1,Frag_ME.v(i,2)+0.1,Frag_ME.v(i,3)+0.1,num2str(i),'color','k','FontSize',14);
%         end
    else
        drawMesh(Frag_ME.vi_cart,Frag_ME.fi_cart, 'FaceColor',[0 0 0],'FaceAlpha',0.2,'EdgeAlpha',0.1);
        %drawPoint3d(Frag_ME.vi_cart,'*k');
        drawMesh(Frag_ME.ve_cart,Frag_ME.fe_cart, 'FaceColor',[0 0 1],'FaceAlpha',0.05,'EdgeAlpha',0.1);
        if Frag_ME.shape_ID == SHAPE_ID_LIST.HOLLOW_CYLINDER
            pid = patch(Frag_ME.vi_cart_down(:,1),Frag_ME.vi_cart_down(:,2),Frag_ME.vi_cart_down(:,3),'red');
            set(pid,'FaceColor',[0 0 0],'FaceAlpha',0.2,'EdgeAlpha',0.1);
            pid = patch(Frag_ME.vi_cart_top(:,1),Frag_ME.vi_cart_top(:,2),Frag_ME.vi_cart_top(:,3),'red');
            set(pid,'FaceColor',[0 0 0],'FaceAlpha',0.2,'EdgeAlpha',0.1);
            pid = patch(Frag_ME.ve_cart_down(:,1),Frag_ME.ve_cart_down(:,2),Frag_ME.ve_cart_down(:,3),'red');
            set(pid,'FaceColor',[0 0 1],'FaceAlpha',0.05,'EdgeAlpha',0.1);
            pid = patch(Frag_ME.ve_cart_top(:,1),Frag_ME.ve_cart_top(:,2),Frag_ME.ve_cart_top(:,3),'red');
            set(pid,'FaceColor',[0 0 1],'FaceAlpha',0.05,'EdgeAlpha',0.1);
        end
    end

    % ------------------------------
    % plot the Fragmentation Volumes
    %
    for i=1:Impact_Data.N_frag_vol
        if isSolidShape(Frag_ME.shape_ID)
            drawMesh(Frag_Volume{i}.v, Frag_Volume{i}.f, ...
                     'FaceColor', 'g','FaceAlpha',0.1);
        else
            drawPoint3d(Frag_Volume{i}.v_cart,'*');
        end
    end
    %}

    % ------------------------------
    % plot the Fragmentation Domains
    %{
    for i=1:N_frag_dom
        drawMesh(Frag_Domain{i}.v, Frag_Domain{i}.f, 'FaceColor', 'r','FaceAlpha',0.1);
        drawVector3d(Frag_Domain{i}.centros, Frag_Domain{i}.normals);
    end
    %}


end
