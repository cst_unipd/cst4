function P6_plot_Velocity_Distribution_Solid(Frag_ME,Impact_Data,Frag_Domain,domain,frags_in_volume)


    for k = 1:Impact_Data.N_frag_dom % 1:N_frag_dom

        figure()
        set(gcf,'Color','White');
        hold on; axis equal; axis off;
        view(30,40);

        title(['Frag Domain ' num2str(k)],'FontSize',15);

        if ~isempty(domain{k})
            for i=1:domain{k}.nedge
                % this plot the edges
                plot3(domain{k}.edges{i}(:,1),domain{k}.edges{i}(:,2),domain{k}.edges{i}(:,3),'k');
            end

            for i=1:domain{k}.nface
                % this plot the faces
                fill3(domain{k}.faces{i}(:,1),domain{k}.faces{i}(:,2),domain{k}.faces{i}(:,3),'k','FaceAlpha',0.05);
            end
        else
            drawMesh(Frag_Domain{k}.v,Frag_Domain{k}.f, 'FaceColor', 'r','FaceAlpha',0.1);
        end
        

%         for i=1:domain{k}.nvertex
%              % this plot the vertices
%             plot3(domain{k}.vertex(:,1),domain{k}.vertex(:,2),domain{k}.vertex(:,3),'*k');
%         end

        for j = 1:length(Impact_Data.intersect_groups{k}) % length(Impact_Data.intersect_groups{k})

            i = Impact_Data.intersect_groups{k}(j);
            col = rand(1,3); col = col/norm(col);

            % plot impact point
            plot3(Impact_Data.I_POS_F(i,1),Impact_Data.I_POS_F(i,2),Impact_Data.I_POS_F(i,3),'pm','LineWidth',2);

            % plot impact velocity
            %drawVector3d(Impact_Data.I_POS_F(i,:), 0.5*(Frag_ME.RGF*COLLISION_DATA(count).IMPACTOR(i).v_rel)','Color',col,'LineWidth',2);
            drawVector3d(Impact_Data.I_POS_F(i,:), 0.5*Impact_Data.I_VEL_F(i,:),'Color',col,'LineWidth',2);

            for ii=1:frags_in_volume{i}.ncells
                % plot CoM
                plot3(frags_in_volume{i}.CoM(ii,1),frags_in_volume{i}.CoM(ii,2),frags_in_volume{i}.CoM(ii,3),'*','Color',col);

                % plot velocity vectors applied at CoM's
                drawVector3d(frags_in_volume{i}.CoM(ii,:), 0.4*frags_in_volume{i}.vel(ii,:),'Color',col);
                
                if frags_in_volume{i}.kill_flag(ii) == 1
                    drawPoint3d(frags_in_volume{i}.CoM(ii,:),'dk');
                end
            end
            
        end

    end


end
