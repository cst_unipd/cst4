%> @file   getPointsWithinBoundary.m
%> @brief  Compute the points that are within a convex polytope
%> @author Andrea Valmorbida (andrea.valmorbida@unipd.it)

%==========================================================================
%> @brief Compute the points that are within a convex polytope
%>
%> @param pnts0 points (Nx3)
%> @param bnd convex hull of the convex polytope (Mx3)
%>
%> @retval pnts points among pnts0 that are within the polytope bnd
%>
%==========================================================================
function pnts = getPointsWithinBoundary(pnts0,bnd)
%Take points that are within a convex polytope

% tol = 1e-7;
% in = inhull(pnts0,bnd,[],tol);
in = inhull(pnts0,bnd);
u1 = 0;
pnts = [];
for i = 1:size(pnts0,1)
    if in(i) ==1
        u1 = u1 + 1;
        pnts(u1,:) = pnts0(i,:);
    else
        %disp(i);
    end
end

end

