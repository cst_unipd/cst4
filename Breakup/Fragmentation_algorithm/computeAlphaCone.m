%> @file   computeAlphaCone.m
%> @brief  Algorithm that computes the semi-aperture angle of the intersection cone
%> @author Andrea Valmorbida (andrea.valmorbida@unipd.it)

%==========================================================================
%> @brief Algorithm that computes the semi-aperture angle of the intersection cone
%>
%> Algorithm that computes the semi-aperture angle of the intersection cone
%>
%> @param Ek Kinetic energy of the impactor
%> @param c_EXPL the coefficient c_EXPL of the impacted ME
%> @param F_L the Fragmentation level
%> @param shape_ID the shape ID
%>
%> @retval alpha_cone the semi-aperture angle of the intersection cone
%>
%==========================================================================
function alpha_cone = computeAlphaCone(Ek,c_EXPL,F_L,shape_ID)
                                  
% This function computes the angle of semi-aperture of the intersection cone
% It is used in the Frag. Algo for hollow shapes, section A5

% The final algorithm implemented in this function will be provided at the
% conclusion of the tuning phase. For now, alpha_cone is set to 15 deg.

alpha_cone = deg2rad(15);

end
