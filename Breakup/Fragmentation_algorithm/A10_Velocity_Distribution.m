function [frags_in_volume Q_EXPL] = A10_Velocity_Distribution(COLLISION_DATA,Frag_ME,Impact_Data,Frag_Data,Frag_Volume,frags_in_volume,tuning_c_coeff)
%A10_Velocity_Distribution Velocity Distribution model
%
% Syntax:  frags_in_volume = A10_Velocity_Distribution(COLLISION_DATA,Frag_ME,Impact_Data,Frag_Data,frags_in_volume,tuning_c_coeff)
%
% Inputs:
%    COLLISION_DATA - COLLISION_DATA(count) structure
%    Frag_ME - Frag_ME structure
%    Impact_Data - Impact_Data structure
%    Frag_Data - Frag_Data structure
%    frags_in_volume - frags_in_volume structure
%    tuning_c_coeff - tuning_c_coeff structure
%
% Outputs:
%    frags_in_volume - frags_in_volume structure with updated field vel
%    Q_EXPL - Momentum of explosive contribution
%
% Other m-files required: computeAlphaParam_j_ik
% Subfunctions: none
% MAT-files required: zeros, norm
%
% See also:
%
% Author:   Andrea Valmorbida, Ph.D.
%           Center of Studies and Activities for Space "Giuseppe Colombo" (CISAS)
%           University of Padova
% Email address: andrea.valmorbida@unipd.it
% Date: 2018/08/02
% Revision: 2.0
% Copyright: 2017 CISAS - UNIVERSITY OF PADOVA
%
% HISTORY
% 2017/11/24 : first version by AV
% 2018/08/02 : second version by LO (velocity models)

%#codegen

% In this section, for each fragment resulting from the voronoi
% tessellation we compute the velocity vector applyed to its center of
% mass, according to a velocity model

% alpha_param_j_ik => influence that the j-th impact has on the velocity of
% the fragment k-th belonging to the i-th Frag Volume

global SHAPE_ID_LIST

k_am_1 = 1e-09;
k_am_2 = 0;
k_Vc_frag = 1.3;

% reset the frag velocities
for i=1:Impact_Data.N_frag_vol
    frags_in_volume{i}.vel = zeros(frags_in_volume{i}.ncells,3);
end

sum_frags_in_volume_mass=cell(1,Impact_Data.N_frag_vol);
for k = 1:Impact_Data.N_frag_vol
    sum_frags_in_volume_mass{k}=0;
    for i=1:frags_in_volume{k}.ncells
        if Frag_ME.shape_ID == SHAPE_ID_LIST.HOLLOW_ELLIPSOID
            sum_frags_in_volume_mass{k} = sum_frags_in_volume_mass{k}+frags_in_volume{k}.mass_cyl(i);
        else
            sum_frags_in_volume_mass{k} = sum_frags_in_volume_mass{k}+frags_in_volume{k}.mass(i);
        end
    end
end

for j = 1:Impact_Data.N_impact % j => j-th imapct
    
    % compute quantities that only depends on the impact conditions
    mass_p = COLLISION_DATA.IMPACTOR(j).mass;
    P = 1; % coupling factor
    
    V_REL = Frag_ME.RGF*COLLISION_DATA.IMPACTOR(j).v_rel; % relative velocity in the Frag RF
    V_IMP = max(Frag_ME.mass/(Frag_ME.mass+mass_p)*V_REL,0.1*V_REL);
       
    E_I_rel = 0.5*mass_p*Frag_ME.mass/(Frag_ME.mass+mass_p)*(norm(V_REL))^2;
    am =  3.3E-10*mass_p/(sum_frags_in_volume_mass{j}*P)*sqrt(E_I_rel);
    if sum_frags_in_volume_mass{j}==0 
        am = 0;
    end
    am = k_am_1 + k_am_2 *am;

    for i = 1:Impact_Data.N_frag_vol % i => i-th frag volume
        
        % compute the CoM of the i-th Fragmentation Volume
        if and(ne(Frag_ME.shape_ID,3),ne(Frag_ME.shape_ID,5))
            frag_vol_CoM = polyhedronCentroid(Frag_Volume{i}.v, Frag_Volume{i}.f);
        else
            frag_vol_CoM = Frag_Data(j,1:3); 
        end
        
        for k = 1:frags_in_volume{i}.ncells % k => k-th fragment within Frag Volume i-th
            [~, ~, ~, ~, ~, ~, ~, ~, Vc_frag_max, ~] = Parameters_Setup;
            Vc_frag =min([k_Vc_frag*norm(V_IMP)*sqrt( am / ( am + frags_in_volume{i}.mass(k,1) ) )      Vc_frag_max]);
            % Fragmentation point:
            vel_start_point=frag_vol_CoM;
            if COLLISION_DATA.F_L == 1
                c_P1=1;
            else
                c_P1 = tuning_c_coeff.c_P1;

                if(c_P1<0)
                    c_P1=abs(c_P1);
                    %-------------------  -> CG
                    %Defining velocity model starting point (CoM for perforating
                    %impact, crater bottom for no perforating impact; starting
                    %point will be then adjusted by using tuning_c_coeff.c_P1
                    % Check for perforating impact
                    Frag_Volume_i_centros = faceCentroids(Frag_Volume{i}.v, Frag_Volume{i}.f);
                    Delta_v = Frag_Volume_i_centros - Impact_Data.I_POS_F(i,:);
                    [~,imin] = min(vectorNorm3d(Delta_v));
                    normal = faceNormal(Frag_Volume{i}.v, Frag_Volume{i}.f);
                    normal = normal(imin,:);
                    alpha_angle = vectorAngle3d(normal,Impact_Data.I_VEL_F(i,:));
                    if alpha_angle>pi/2
                        normal = -normal;
                        alpha_angle = pi-alpha_angle;
                    end
                    
                    zv = normal;
                    if ~isParallel3d(zv,[1 0 0])
                        yv = vectorCross3d(zv,[1 0 0]);
                    else
                        yv = vectorCross3d(zv,[0 1 0]);
                    end
                    yv = yv/vectorNorm3d(yv);
                    xv = vectorCross3d(yv,zv);
                    RVF = [xv',yv',zv'];
                    pVF = Impact_Data.I_POS_F(i,:)';
                    TVF = trasm(RVF,pVF);
                    TFV = inv(TVF);
                    
                    if(Frag_Data(j,6)==0) %spherical hole
                        vel_start_point_temp=Frag_Data(j,1:3)+Frag_Data(j,4)*zv;
                        if(isPointWithinConvexHull(vel_start_point_temp,Frag_ME.v,Frag_ME.f,1e-7))
                            vel_start_point=Frag_Data(j,1:3)+Frag_Data(j,4)*zv;
                        end
                    else % ellipsoidal hole
                        vel_start_point_temp=Frag_Data(j,1:3)+Frag_Data(j,6)*zv;
                        if(isPointWithinConvexHull(vel_start_point_temp,Frag_ME.v,Frag_ME.f,1e-7))
                            vel_start_point=Frag_Data(j,1:3)+Frag_Data(j,6)*zv;
                        end
                    end
                    
                    %------------------
                end
            end
            P_FRAG= c_P1*Frag_Data(j,1:3) + (1-c_P1)*vel_start_point;
%              P_FRAG= c_P1*Frag_Data(j,1:3) + (1-c_P1)*frag_vol_CoM;
            
            R = (frags_in_volume{i}.CoM(k,:) - P_FRAG)';
            
            r_F = (R/norm(R)) + tuning_c_coeff.c_3*(V_IMP/norm(V_IMP));
            r_F = r_F/norm(r_F);
            
            V_Frag_REL = Vc_frag*r_F;
            
            % part of the velocity of the k-th fragment (in the i-th Frag
            % Volume) due to the j-th impact
            alpha_param_j_ik = computeAlphaParam_j_ik(Frag_ME.shape_ID,i,j,...
                frags_in_volume{i}.CoM(k,:),...
                Frag_Data);
            v_fk_j =  alpha_param_j_ik*tuning_c_coeff.c_1*V_Frag_REL';
            frags_in_volume{i}.vel(k,:) = frags_in_volume{i}.vel(k,:) + v_fk_j;
        end
    end
end

Q_EXPL=[0 0 0]';
% Here the explosion contribute is calculated (e.g. explosion of batteries)
if (Frag_ME.target_type == 1) && (COLLISION_DATA.TARGET.c_EXPL~=0)
    if Frag_ME.shape_ID == 1 % plate/box, battery
        m_int = 0.9*Frag_ME.mass;
        m_serb = 0.1*Frag_ME.mass;
        alpha = 0.7; % TBD
        Vol = m_int/Frag_ME.rho;
    elseif Frag_ME.shape_ID == 3 % hollow sphere
        Mm = 28;
        Temp = 300;
        Vol = 4/3*pi()*Frag_ME.b^3;
        m_int = COLLISION_DATA.TARGET.c_EXPL*Vol*Mm/(8.314*Temp);
        m_serb = Frag_ME.mass;
        alpha = 0.6;
        
    elseif Frag_ME.shape_ID == 5 % hollow cylinder
        Mm = 28;
        Temp = 300;
        Vol = pi()*Frag_ME.b^2*Frag_ME.c;
        m_int = COLLISION_DATA.TARGET.c_EXPL*Vol*Mm/(8.314*Temp);
        m_serb = Frag_ME.mass;
        alpha = 0.5;
    end
    
    E_int = COLLISION_DATA.TARGET.c_EXPL*Vol;
    R_int = (m_int/m_serb)*(1+alpha*m_int/m_serb);
    v_EXPL_mean = sqrt(2*E_int*R_int);
    
    for i = 1:Impact_Data.N_frag_vol % i => i-th frag volume
        for k = 1:frags_in_volume{i}.ncells
            % Explosion point:
            P_EXPL= (tuning_c_coeff.c_P2)*Frag_Data(i,1:3) + (1-tuning_c_coeff.c_P2)*Frag_ME.center;
            r_CM = frags_in_volume{i}.CoM(k,:) - P_EXPL;
            r_CM = r_CM/norm(r_CM);
            v_expl_kj = tuning_c_coeff.c_4*v_EXPL_mean*r_CM;
            frags_in_volume{i}.vel(k,:) = frags_in_volume{i}.vel(k,:) + v_expl_kj;
            Q_EXPL=Q_EXPL+frags_in_volume{i}.mass(k,1)*v_expl_kj';
        end
    end
end

end
