%> @file    InternalNormals.m
%> @brief   Compute the internal normal of a body with respect to an impact
%> @brief   point and its angle with respect to the veloctiy vector. It 
%> @brief   also gives the distance of the point of at the end of the
%> @brief   crater or the hole
%> @authors  Giulia Sarego (giulia.sarego@unipd.it), Andrea Valmorbida 
%>          (andrea.valmorbida@unipd.it) and Cinzia Giacomuzzo 
%>          (cinzia.giacomuzzo@unipd.it)
%> Copyright (c) 2018 CISAS "Giuseppe Colombo" - UNIVERSITY OF PADOVA. All rights reserved.
%======================================================================
function [normal,alpha_angle,h_ext_P3]=InternalNormals(I_POS_F,I_VEL_F,Frag_V,Frag_P_R)

%InternalNormals Compute the internal normal of a body with respect to an impact
% point and its angle with respect to the veloctiy vector. It 
% also gives the distance of the farthest point of the crater or the hole
%
% Syntax:  [normal,alpha_angle,h_ext_P3]=InternalNormals(I_POS_F,I_VEL_F,Frag_V,Frag_P_R)
%
% Inputs:
%    I_POS_F        - position of the impact point in the fragmentation reference
%                   system
%    I_VEL_F        - velocity of the impactor in the fragmentation reference
%                   system 
%    Frag_V         - Fragmentation volume
%    Frag_P_R       - 1x4 array, containing the impact pointcoorfinates in the
%                   global reference system and the fragmentation radius
%  
%
% Outputs:
%    normal         - vector of the normal of the impacted face
%    alpha_angle    - convex angle between the velocity vector and the normal 
%                   or the impacted face 
%    h_ext_P3       - distance from the impact point of the farthest point at the
%                   of the crater or the point on the opposite face of the body
%
% Other m-files required: createLine3d, faceNormal, faceCentroids, max,
%                         polyhedronCentroid, isnumeric, length, min,
%                         intersectLinePolygon3d, distancePoints3d,
%                         vectorAngle3d, intersectLineConvexHull, isempty,
%                         
% Subfunctions: none
% MAT-files required: none
%
% See also:
%
% Authors:  Giulia Sarego, Ph.D.
%           Department of Industrial Engineering DII, University of Padova
%           Andrea Valmorbida, Ph.D.
%           Center of Studies and Activities for Space "Giuseppe Colombo" (CISAS)
%           University of Padova
%           Cinzia Giacomuzzo, Ph.D.
%           Department of Industrial Engineering DII, University of Padova

% Email addresses: giulia.sarego@unipd.it, andrea.valmorbida@unipd.it,
%                  cinzia.giacomuzzo@unipd.it
% Date: 2018/12/19
%
% HISTORY
% 2018/12/19 : first version by GS, AV, CG

%#codegen

Frag_P=Frag_P_R(1:3);
Frag_R=Frag_P_R(4);
dist_j=[];
line_v=createLine3d(I_POS_F,I_VEL_F); %vel line through Impact Point
% intersecting line_v with bounding box faces
normals = faceNormal(Frag_V.v, Frag_V.f);
centros = faceCentroids(Frag_V.v, Frag_V.f);
centroid_F = polyhedronCentroid(Frag_V.v, Frag_V.f);
if isnumeric(Frag_V.f)
    for j=1:size(Frag_V.f,1)
        poly_bb = Frag_V.v(Frag_V.f(j,:),:);
        itsc_pnts = intersectLinePolygon3d(line_v, poly_bb);
        dist = distancePoints3d(itsc_pnts,Frag_P);
        dist_j=[dist_j,dist];
        vect_cc = centros(j,:)-centroid_F;
        dir = vect_cc*normals(j,:)';
        if dir>0
            normals(j,:) = -normals(j,:);
        end
    end
else
    for j=1:length(Frag_V.f)
        poly_bb = Frag_V.v(Frag_V.f{j},:);
        itsc_pnts = intersectLinePolygon3d(line_v, poly_bb);
        dist = distancePoints3d(itsc_pnts,Frag_P);
        dist_j=[dist_j,dist];
        vect_cc = centros(j,:)-centroid_F;
        dir = vect_cc*normals(j,:)';
        if dir>0
            normals(j,:) = -normals(j,:);
        end
    end
end

[~,imin] = min(dist_j);
normal = normals(imin,:);

alpha_angle = vectorAngle3d(normal,I_VEL_F);
if alpha_angle>pi/2
    alpha_angle = pi-alpha_angle;
end

P1 = Frag_P;
P2 = Frag_P+Frag_R*normal;
intersectionLine = createLine3d(P1,P2);
P3 = intersectLineConvexHull(intersectionLine,Frag_V.v);
if isempty(P3)
    P3=P2;
end
if(size(P3,1)==1 || distancePoints3d(P3(1,:),P3(2,:))<10*eps)
    P3(2,:)=P1+Frag_R*normal;
end

h_ext_P3=max([distancePoints3d(P1,P3)]);

