function [Frag_Volume,Frag_ME] = A6_compute_Seeds_Distrib_Frag_Volume(COLLISION_DATA,Frag_ME,Impact_Data,Frag_Volume,Frag_Data,tuning_c_coeff,count)
%A6_compute_Seeds_Distrib_Frag_Volume Compute the seeds distribution for each Fragmentation Volume
%
% Syntax:  [Frag_Volume,Frag_ME] = A6_compute_Seeds_Distrib_Frag_Volume(Frag_ME,Impact_Data,Frag_Volume)
%
% Inputs:
%    Frag_ME - Frag_ME structure
%    Impact_Data - Impact_Data structure
%    Frag_Volume - Frag_Volume structure
%    Frag_Data - Impact point and fragmentation volume radius (GS+FF)
%
% Outputs:
%    Frag_Volume - Frag_Volume structure with updated fields:
%                       seeds_distribution_ID, nseeds, seeds
%
% Other m-files required: SeedsDistrib_RandomCartesian_Solid,
%                         SeedsDistrib_RandomCartesian_Hollow,
%                         SeedsDistrib_RandomSpherical_Hollow
% Subfunctions: none
% MAT-files required: none
%
% See also:
%
% Author:   Andrea Valmorbida, Ph.D.
%           Center of Studies and Activities for Space "Giuseppe Colombo" (CISAS)
%           University of Padova
% Email address: andrea.valmorbida@unipd.it
% Date: 2017/11/24
% Revision: 1.0
% Copyright: 2017 CISAS - UNIVERSITY OF PADOVA
%
% HISTORY
% 2017/11/24 : first version by AV

%#codegen

global ME;
global FRAGMENTS;
global SHAPE_ID_LIST;
global PROPAGATION_THRESHOLD
global sim_title_dir
persistent multiplecount

% -----------------------------
% get the seeds_distribution_ID
switch Frag_ME.target_type
    case 0 % FRAGMENT
        seeds_distribution_ID = FRAGMENTS(Frag_ME.object_ID_index).FRAGMENTATION_DATA.seeds_distribution_ID;
    case 1 % ME
        seeds_distribution_ID = ME(Frag_ME.object_ID_index).FRAGMENTATION_DATA.seeds_distribution_ID;
end

% -------------------------------------------------------------------
% apply the seeds distribution according to the seeds distribution ID
% selected by the usar
switch seeds_distribution_ID
    
    case 0  % RANDOM IN CARTESIAN COORDINATES
        
        % get seeds distribution parameters according to the selected
        % seeds distribution
        switch Frag_ME.target_type
            case 0 % FRAGMENT
                n_seeds = max([2,round(FRAGMENTS(Frag_ME.object_ID_index).FRAGMENTATION_DATA.seeds_distribution_param1*COLLISION_DATA.F_L*FRAGMENTS(Frag_ME.object_ID_index).GEOMETRY_DATA.mass/FRAGMENTS(Frag_ME.object_ID_index).GEOMETRY_DATA.mass0)]);
                n_max_loop = FRAGMENTS(Frag_ME.object_ID_index).FRAGMENTATION_DATA.seeds_distribution_param2;
            case 1 % ME
                n_seeds = max([2,round(ME(Frag_ME.object_ID_index).FRAGMENTATION_DATA.seeds_distribution_param1*COLLISION_DATA.F_L*ME(Frag_ME.object_ID_index).GEOMETRY_DATA.mass/ME(Frag_ME.object_ID_index).GEOMETRY_DATA.mass0)]);
                n_max_loop = ME(Frag_ME.object_ID_index).FRAGMENTATION_DATA.seeds_distribution_param2;
        end
        
        df_min=PROPAGATION_THRESHOLD.fragment_radius_th;
        
        if isSolidShape(Frag_ME.shape_ID)
            Frag_Volume = SeedsDistrib_RandomCartesian_Solid(Frag_Volume,n_seeds,n_max_loop,Frag_Data,Impact_Data,df_min,Frag_ME);
        else
            Frag_Volume = SeedsDistrib_RandomCartesian_Hollow(Frag_ME,Frag_Volume,n_seeds,n_max_loop);
        end
        
    case 1  % GAUSSIAN IN CARTESIAN COORDINATES
        
        % if-cycle to decrease seed generation number
%         dbstop at 83
        [target_counter,multiplecount]=multiplecount_function(COLLISION_DATA,multiplecount);
        
        % get seeds distribution parameters according to the selected
        % seeds distribution

        switch Frag_ME.target_type
            case 0 % FRAGMENT
                n_seeds0=FRAGMENTS(Frag_ME.object_ID_index).FRAGMENTATION_DATA.seeds_distribution_param1;
                n_seeds = max([2,round(FRAGMENTS(Frag_ME.object_ID_index).FRAGMENTATION_DATA.seeds_distribution_param1*COLLISION_DATA.F_L*FRAGMENTS(Frag_ME.object_ID_index).GEOMETRY_DATA.mass/FRAGMENTS(Frag_ME.object_ID_index).GEOMETRY_DATA.mass0)]);
                n_max_loop = FRAGMENTS(Frag_ME.object_ID_index).FRAGMENTATION_DATA.seeds_distribution_param2;
                %                 L_MAX=max(FRAGMENTS(Frag_ME.object_ID_index).GEOMETRY_DATA.dimensions);
            case 1 % ME
                n_seeds0=ME(Frag_ME.object_ID_index).FRAGMENTATION_DATA.seeds_distribution_param1;
                n_seeds = max([2,round(ME(Frag_ME.object_ID_index).FRAGMENTATION_DATA.seeds_distribution_param1*COLLISION_DATA.F_L*ME(Frag_ME.object_ID_index).GEOMETRY_DATA.mass/ME(Frag_ME.object_ID_index).GEOMETRY_DATA.mass0)]);
                n_max_loop = ME(Frag_ME.object_ID_index).FRAGMENTATION_DATA.seeds_distribution_param2;
                %                 L_MAX=max(ME(Frag_ME.object_ID_index).GEOMETRY_DATA.dimensions);
        end
        L=length(COLLISION_DATA.IMPACTOR);
        n_seeds_vector=ones(1,L);
        
        if(n_seeds0==0)  % Using NISHIDA for the NUMBER OF SEEDS
             [~, ~, a_Ni, k_Ni, ~, ~, FRAGMENT_radius_th, n_seeds_max, ~, ~] = Parameters_Setup();
            n_seeds_vector=n_seeds_vector*0;
            dp=zeros(1,L);
            vel=zeros(1,L);
            N_Ni=zeros(1,L);
            %             size_coeff=min([L_MAX/1E-3 1]); % Filter on minimum number of seeds
            for i=1:L
                dp(i)=max([COLLISION_DATA.IMPACTOR(i).dp_EQ FRAGMENT_radius_th]);
                vel(i)=min([norm(COLLISION_DATA.IMPACTOR(i).v_rel)/1000 100]);
                %             a_d= a_Ni/dp(i);
                if dp(i) >= 1.6e-3
                    df_min = 0.0005;
                else
                    df_min = 0.0001;
                end
                a_d= df_min/dp(i);
                N_Ni(i)=k_Ni* vel(i)^(1.5) * (  151.6*exp(-10.6*a_d) + 18.0*exp(-3.24 *a_d)  );
                
                %             N_Ni=N_Ni*size_coeff;
                %             n_seeds_vector(i)=2.27^3*min([ceil(N_Ni)+1 n_seeds/L n_seeds_max]);
                %              n_seeds_vector(i)=min([ceil(N_Ni)+1 n_seeds/L n_seeds_max]);
                if COLLISION_DATA.IMPACTOR(i).type == 1 % ME
                    impactor_counter = ME(COLLISION_DATA.IMPACTOR(i).ID).object_ID;
                elseif COLLISION_DATA.IMPACTOR(i).type == 0 % FRAGMENTS
                    impactor_counter = FRAGMENTS(COLLISION_DATA.IMPACTOR(i).ID).object_ID;
                end
                [jjj, ~] = find(multiplecount(:,1) == min([impactor_counter,target_counter]));
                [jj, ~] = find(multiplecount(:,2) == max([impactor_counter,target_counter]));
                j = intersect(jj,jjj);
                n_seeds_vector(i)=ceil((N_Ni(i)+1)/exp(ceil(multiplecount(j,3)/2)-1));
                %             n_seeds_vector(i)=ceil(N_Ni(i)+1);
                
            end
            sim_data = dir(fullfile(sim_title_dir,'data',filesep));
            max_step = 0;
            if length(sim_data)>4
                for counter_step=1:length(sim_data)
                    if strfind(sim_data(counter_step).name,'step_')
                        new_step = str2double(sim_data(counter_step).name(strfind(sim_data(counter_step).name,'step_')+5:strfind(sim_data(counter_step).name,'.mat')-1));
                        if new_step > max_step
                            max_step = new_step;
                        end
                    end
                end
            end
            save([fullfile(sim_title_dir,'data',filesep),'step_',num2str(max_step),'.mat'],'multiplecount','-append')
        else
            df_min=PROPAGATION_THRESHOLD.fragment_radius_th;
            n_seeds_vector=n_seeds_vector*n_seeds;
        end
        %         size_coeff=min([L_MAX/1E-3 1]); % Filter on minimum number of seeds
        %          n_seeds_Ni=sum(N_Ni);%*size_coeff;
        %          n_seeds_Ni=0.5*n_seeds_Ni;
        %           n_seeds=min([ceil(n_seeds_Ni)+1 n_seeds n_seeds_max]);
        %                  n_seeds=min([ceil(n_seeds_Ni)+1 n_seeds_max]);
%         dbstop at 160
        if isSolidShape(Frag_ME.shape_ID)
            %            Frag_Volume = SeedsDistrib_GaussianCartesian_Solid(Frag_Volume,n_seeds,n_max_loop,Frag_Data);
            Frag_Volume = SeedsDistrib_GaussianCartesian_Solid(Frag_Volume,n_seeds_vector,n_max_loop,Frag_Data,Impact_Data,tuning_c_coeff,count,df_min,Frag_ME);
        else
            warning('Seed Distribution to be set to 4 for Hollow Shapes');
        end
        
    case 2 % GAUSSIAN IN CARTESIAN COORDINATES + NISHIDA for fragments
        % get seeds distribution parameters according to the selected
        % seeds distribution
        switch Frag_ME.target_type
            case 0 % FRAGMENT
                n_seeds = max([2,round(FRAGMENTS(Frag_ME.object_ID_index).FRAGMENTATION_DATA.seeds_distribution_param1*COLLISION_DATA.F_L*FRAGMENTS(Frag_ME.object_ID_index).GEOMETRY_DATA.mass/FRAGMENTS(Frag_ME.object_ID_index).GEOMETRY_DATA.mass0)]);
                n_max_loop = FRAGMENTS(Frag_ME.object_ID_index).FRAGMENTATION_DATA.seeds_distribution_param2;
            case 1 % ME
                n_seeds = max([2,round(ME(Frag_ME.object_ID_index).FRAGMENTATION_DATA.seeds_distribution_param1*COLLISION_DATA.F_L*ME(Frag_ME.object_ID_index).GEOMETRY_DATA.mass/ME(Frag_ME.object_ID_index).GEOMETRY_DATA.mass0)]);
                n_max_loop = ME(Frag_ME.object_ID_index).FRAGMENTATION_DATA.seeds_distribution_param2;
        end
        
        if isSolidShape(Frag_ME.shape_ID)
            Frag_Volume = SeedsDistrib_GaussianCartesian_Solid2(Frag_Volume,n_seeds,n_max_loop,Frag_Data);
        else 
            warning('Seed Distribution to be set to 4 for Hollow Shapes');
        end
        
        
    case 4   % RANDOM IN SPHERICAL COORDINATES - ONLY FOR HOLLOW SHAPES
        % In this case, the seeds are randomly distributed in the
        % sphereical coordinates. Cartesian seeds coordinates are
        % then computed
        
        % get seeds distribution parameters according to the selected
        % seeds distribution
        switch Frag_ME.target_type
            case 0 % FRAGMENT
                n_seeds = max([2,round(FRAGMENTS(Frag_ME.object_ID_index).FRAGMENTATION_DATA.seeds_distribution_param1*COLLISION_DATA.F_L*FRAGMENTS(Frag_ME.object_ID_index).GEOMETRY_DATA.mass/FRAGMENTS(Frag_ME.object_ID_index).GEOMETRY_DATA.mass0)]);
                n_max_loop = FRAGMENTS(Frag_ME.object_ID_index).FRAGMENTATION_DATA.seeds_distribution_param2;
            case 1 % ME
                n_seeds = max([2,round(ME(Frag_ME.object_ID_index).FRAGMENTATION_DATA.seeds_distribution_param1*COLLISION_DATA.F_L*ME(Frag_ME.object_ID_index).GEOMETRY_DATA.mass/ME(Frag_ME.object_ID_index).GEOMETRY_DATA.mass0)]);
                n_max_loop = ME(Frag_ME.object_ID_index).FRAGMENTATION_DATA.seeds_distribution_param2;
        end
        
        if isHollowShape(Frag_ME.shape_ID)
            Frag_Volume = SeedsDistrib_RandomSpherical_Hollow(Frag_ME,Frag_Volume,n_seeds,n_max_loop);
        else
            error('SEEDS DISTRIBUTION WITH ID 4, I.E. RANDOM SPHERICAL, IS AVAILABLE ONLY FOR HOLLOW SHAPES!!');
        end
        
        
    case 3  % Nishida with log spiral
        if isSolidShape(Frag_ME.shape_ID) % CG 24-07-18
                        [target_counter,multiplecount]=multiplecount_function(COLLISION_DATA,multiplecount);

%             n_seeds_vector(i)=ceil((N_Ni(i)+1)/exp(ceil(multiplecount(j,3)/2)-1));
            
            
            Frag_Volume = SeedsDistrib_LogSpiral_Solid_Nishida(COLLISION_DATA,Frag_ME,Frag_Volume,Frag_Data,Impact_Data,multiplecount,target_counter);
        else % CG 24-07-18
            switch Frag_ME.target_type
                case 0 % FRAGMENT
                    n_seeds=FRAGMENTS(Frag_ME.object_ID_index).FRAGMENTATION_DATA.seeds_distribution_param1;
                    n_max_loop = FRAGMENTS(Frag_ME.object_ID_index).FRAGMENTATION_DATA.seeds_distribution_param2;
                case 1 % ME
                    n_seeds=ME(Frag_ME.object_ID_index).FRAGMENTATION_DATA.seeds_distribution_param1;
                    n_max_loop = ME(Frag_ME.object_ID_index).FRAGMENTATION_DATA.seeds_distribution_param2;
            end
            
            sim_data = dir(fullfile(sim_title_dir,'data',filesep));
            max_step = 0;
            if length(sim_data)>4
                for counter_step=1:length(sim_data)
                    if strfind(sim_data(counter_step).name,'step_')
                        new_step = str2double(sim_data(counter_step).name(strfind(sim_data(counter_step).name,'step_')+5:strfind(sim_data(counter_step).name,'.mat')-1));
                        if new_step > max_step
                            max_step = new_step;
                        end
                    end
                end
            end
            save([fullfile(sim_title_dir,'data',filesep),'step_',num2str(max_step),'.mat'],'multiplecount','-append')
            
            
            Frag_Volume = SeedsDistrib_RandomSpherical_Hollow(Frag_ME,Frag_Volume,n_seeds,n_max_loop);
            disp('SEEDS DISTRIBUTION WITH ID 3 IS NOT AVAILABLE FOR HOLLOW SHAPES; RANDOM SPHERICAL SEEDS DISTRIBUTION SET.')
        end
end

% ---------------------------------------------------
% take the actual number of seeds in each Frag Volume
for i=1:Impact_Data.N_frag_vol
    if isSolidShape(Frag_ME.shape_ID)
        Frag_Volume{i}.nseeds = size(Frag_Volume{i}.seeds,1);
%          size(Frag_Volume{i}.seeds,1)
    else
        Frag_Volume{i}.nseeds = size(Frag_Volume{i}.seeds_cart,1);
    end
end

Frag_ME.seeds_distribution_ID = seeds_distribution_ID;

end
