function ret = isSolidShape(shape_ID)

ret = ~isHollowShape(shape_ID);

end