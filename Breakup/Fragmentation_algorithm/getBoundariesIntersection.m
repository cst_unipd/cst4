%> @file   getBoundariesIntersection.m
%> @brief  Compute the intersection of convex boundaries
%> @author Andrea Valmorbida (andrea.valmorbida@unipd.it)

%==========================================================================
%> @brief Compute the intersection of convex boundaries
%>
%> @param bnd1 convex hull of the first boundary (Nx3)
%> @param bnd2 convex hull of the second boundary (Mx3)
%>
%> @retval bnd_pnts convex hull of the intersection between bnd1 and bnd2 (Lx3)
%>
% History: 12/09/2018 add flag output (GS)
%==========================================================================
function [bnd_pnts, flag] = getBoundariesIntersection(bnd1,bnd2)

f1= minConvexHull(bnd1);
f2= minConvexHull(bnd2);

if sum(isPointWithinConvexHull(bnd1,bnd2,f2,1.e-10)) == size(bnd1,1)
    
    bnd_pnts = bnd1;
    flag = 1;
    
elseif sum(isPointWithinConvexHull(bnd2,bnd1,f1,1.e-10)) == size(bnd2,1)
    
    bnd_pnts = bnd2;
    flag = 1;
    
else
    
    [A_plate,b_plate] = vert2lcon(bnd2,1.e-14);
    [A_circle,b_circle] = vert2lcon(bnd1,1.e-14);
    
    A_bnd = [A_plate;A_circle];
    b_bnd = [b_plate;b_circle];
    
    S_bnd=unique([A_bnd, b_bnd],'rows');
    A_bnd1=S_bnd(:,1:end-1);
    b_bnd1=S_bnd(:,end);
    
    bnd_pnts = MY_con2vert(A_bnd1,b_bnd1);
    
    bnd_pnts = unique(bnd_pnts,'rows'); % CG 23-07-2018
    if(~isempty(bnd_pnts)) %CG 19-07-18
        K = convhull(bnd_pnts);
        bnd_pnts = unique(bnd_pnts(K,:),'rows');
        flag = 1;
    else
        flag = 0;
    end
end
end

