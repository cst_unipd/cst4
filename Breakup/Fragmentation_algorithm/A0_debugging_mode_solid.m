% A0_debugging_mode_solid.m

debug_verbose = 1;                      % <<=== SET THIS FOR DEBUGGING
frag_plot_flag = 1;                     % <<=== SET THIS FOR DEBUGGING

% count = shape ID (except for fragment)
% 1 - PLATE
% 2 - SPHERE
% 4 - CYLINDER
% 6 - CONVEX HULL
% 10- FRAGMENT
count = 2;                              % <<=== SET THIS FOR DEBUGGING

% test case: 1-2
test_case = 2;                          % <<=== SET THIS FOR DEBUGGING

if count>1 && test_case > 2
    error('invalid count - test_case combination');
end

fprintf('==>\tDEBUGGING MODE:\n');
switch count

    % SOLID BOX - PLATE
    case SHAPE_ID_LIST.SOLID_BOX 
        switch test_case
            case 1  % 1 impact
                nimpact = 1;
            case 2  % 4 impacts
                nimpact = 4;
            case 3 % 2 impacts (one lateral)
                nimpact = 2;
            case 4 % 2 impacts (one lateral)
                nimpact = 2;
        end
        fprintf('\tBOX / PLATE WITH %d IMPACT(S)\n\n',nimpact);

    % SOLID SPHERE
    case SHAPE_ID_LIST.SOLID_SPHERE 
        switch test_case
            case 1  % 1 impact
                nimpact = 1;
            case 2  % 2 impacts
                nimpact = 4;
        end
        fprintf('\tSOLID SPHERE WITH %d IMPACT(S)\n\n',nimpact);

    % SOLID CYLINDER
    case SHAPE_ID_LIST.SOLID_CYLINDER 
        switch test_case
            case 1  % 1 impact
                nimpact = 1;
            case 2  % 2 impacts
                nimpact = 4;
        end
        fprintf('\tSOLID CYLINDER WITH %d IMPACT(S)\n\n',nimpact);

    % CONVEX HULL
    case SHAPE_ID_LIST.CONVEX_HULL 
        switch test_case
            case 1  % 1 impact
                nimpact = 1;
            case 2  % 2 impacts
                nimpact = 4;
        end
        fprintf('\tCONVEX HULL WITH %d IMPACT(S)\n\n',nimpact);

    % FRAGMENT
    case 10 
        switch test_case
            case 1  % 1 impact
                nimpact = 1;
            case 2  % 2 impacts
                nimpact = 4;
        end
        fprintf('\tFRAGMENT WITH %d IMPACT(S)\n',nimpact);
    otherwise
        error('debugging:invalid count');
end

% displacement vector from SR
SR_delta = [0 0 0]';

switch count

    % SOLID BOX / PLATE
    case SHAPE_ID_LIST.SOLID_BOX 
        COLLISION_DATA(count).TARGET.ID = count;    % 1st ME
        COLLISION_DATA(count).TARGET.type = 1;  % ME
        object_ID_index = COLLISION_DATA(count).TARGET.ID;
        ME(object_ID_index).GEOMETRY_DATA.shape_ID = count;

        z_plane=2.685; %m
        %z_lower=-z_plane;
        long_side=3.47;
        short_side=1;
        thickness=0.05;
        body_diameter=1.925; %[m]
        r=long_side/2+body_diameter/2;
        npanel = 3-1;

        if test_case == 4
            ME(object_ID_index).GEOMETRY_DATA.dimensions(1) = long_side;
            ME(object_ID_index).GEOMETRY_DATA.dimensions(2) = short_side;
            ME(object_ID_index).GEOMETRY_DATA.dimensions(3) = short_side;
            ME(object_ID_index).GEOMETRY_DATA.thick = short_side;
        else
            ME(object_ID_index).GEOMETRY_DATA.dimensions(1) = long_side;
            ME(object_ID_index).GEOMETRY_DATA.dimensions(2) = short_side;
            ME(object_ID_index).GEOMETRY_DATA.dimensions(3) = thickness;
            ME(object_ID_index).GEOMETRY_DATA.thick = thickness;
        end

        ME(object_ID_index).DYNAMICS_DATA.cm_coord = 0*[r*cos(npanel*pi/3);r*sin(npanel*pi/3);z_plane]; %[2.6975 0 2.6850]';
        ME(object_ID_index).DYNAMICS_DATA.vel = 1e-1*[1 1 1]';
        
        
        z_rot = deg2rad(0);
        y_rot = deg2rad(0);
        x_rot = deg2rad(30);
        myquaternions = quatr(rot_tr(z_rot,y_rot,x_rot));
                 
        ME(object_ID_index).DYNAMICS_DATA.quaternions = [myquaternions(1);-myquaternions(2:4)];

        RMG = rquat(myquaternions);
        TMG = trasm(RMG,ME(object_ID_index).DYNAMICS_DATA.cm_coord);
        RGM = RMG'; TGM = inv(TMG);
        p_OF_M = [-ME(object_ID_index).GEOMETRY_DATA.dimensions(1)/2;...
                  -ME(object_ID_index).GEOMETRY_DATA.dimensions(2)/2;...
                  -ME(object_ID_index).GEOMETRY_DATA.dimensions(3)/2];
        RFM = eye(3); TFM = trasm(RFM,p_OF_M);
        RMF = RFM'; TMF = inv(TFM);
        RFG = RMG*RFM; TFG = TMG*TFM;
        RGF = RFG'; TGF = inv(TFG);


    % SOLID SPHERE
    case SHAPE_ID_LIST.SOLID_SPHERE 
        COLLISION_DATA(count).TARGET.ID = count;    % 2nd ME
        COLLISION_DATA(count).TARGET.type = 1;      % ME
        object_ID_index = COLLISION_DATA(count).TARGET.ID;
        ME(object_ID_index).GEOMETRY_DATA.shape_ID = count;
        ME(object_ID_index).GEOMETRY_DATA.dimensions(1) = 2; % radius
        ME(object_ID_index).GEOMETRY_DATA.dimensions(2) = 0;
        ME(object_ID_index).GEOMETRY_DATA.dimensions(3) = 0;
        ME(object_ID_index).DYNAMICS_DATA.cm_coord = [3.5 0 5.8]';
        ME(object_ID_index).DYNAMICS_DATA.vel = 1e-1*[1 1 1]';
        
        z_rot = deg2rad(0);
        y_rot = deg2rad(0);
        x_rot = deg2rad(0);
        myquaternions = quatr(rot_tr(z_rot,y_rot,x_rot));
        ME(object_ID_index).DYNAMICS_DATA.quaternions = [myquaternions(1);-myquaternions(2:4)];
        
        RMG = rquat(myquaternions);
        TMG = trasm(RMG,ME(object_ID_index).DYNAMICS_DATA.cm_coord);
        RGM = RMG'; TGM = inv(TMG);
        p_OF_M = [-ME(object_ID_index).GEOMETRY_DATA.dimensions(1);...
                  -ME(object_ID_index).GEOMETRY_DATA.dimensions(1);...
                  -ME(object_ID_index).GEOMETRY_DATA.dimensions(1)];
        RFM = eye(3); TFM = trasm(RFM,p_OF_M);
        RMF = RFM'; TMF = inv(TFM);
        RFG = RMG*RFM; TFG = TMG*TFM;
        RGF = RFG'; TGF = inv(TFG);

        ME(object_ID_index).DYNAMICS_DATA.quaternions(2:4) = -ME(object_ID_index).DYNAMICS_DATA.quaternions(2:4);

    % SOLID CYLINDER
    case SHAPE_ID_LIST.SOLID_CYLINDER 
        COLLISION_DATA(count).TARGET.ID = count;    % 4th ME
        COLLISION_DATA(count).TARGET.type = 1;      % ME
        object_ID_index = COLLISION_DATA(count).TARGET.ID;
        ME(object_ID_index).GEOMETRY_DATA.shape_ID = count;
        ME(object_ID_index).GEOMETRY_DATA.dimensions(1) = 1.3;    % radius
        ME(object_ID_index).GEOMETRY_DATA.dimensions(2) = 0;
        ME(object_ID_index).GEOMETRY_DATA.dimensions(3) = 2;      % height
        ME(object_ID_index).DYNAMICS_DATA.cm_coord = [3.5 0 5.8]';
        ME(object_ID_index).DYNAMICS_DATA.vel = 1e-1*[1 1 1]';
        
        z_rot = deg2rad(0);
        y_rot = deg2rad(0);
        x_rot = deg2rad(0);
        myquaternions = quatr(rot_tr(z_rot,y_rot,x_rot));
        ME(object_ID_index).DYNAMICS_DATA.quaternions = [myquaternions(1);-myquaternions(2:4)];
        
        RMG = rquat(myquaternions);
        TMG = trasm(RMG,ME(object_ID_index).DYNAMICS_DATA.cm_coord);
        RGM = RMG'; TGM = inv(TMG);
        p_OF_M = [-ME(object_ID_index).GEOMETRY_DATA.dimensions(1);...
                  -ME(object_ID_index).GEOMETRY_DATA.dimensions(1);...
                  -ME(object_ID_index).GEOMETRY_DATA.dimensions(3)/2];
        RFM = eye(3); TFM = trasm(RFM,p_OF_M);
        RMF = RFM'; TMF = inv(TFM);
        RFG = RMG*RFM; TFG = TMG*TFM;
        RGF = RFG'; TGF = inv(TFG);

        ME(object_ID_index).DYNAMICS_DATA.quaternions(2:4) = -ME(object_ID_index).DYNAMICS_DATA.quaternions(2:4);

    % CONVEX HULL
    case SHAPE_ID_LIST.CONVEX_HULL 
        COLLISION_DATA(count).TARGET.ID = count;    % 6th ME
        COLLISION_DATA(count).TARGET.type = 1;      % ME
        object_ID_index = COLLISION_DATA(count).TARGET.ID;
        ME(object_ID_index).GEOMETRY_DATA.shape_ID = count;
        %[ME(object_ID_index).GEOMETRY_DATA.c_hull,~,~] = createOctahedron;
        ME(object_ID_index).GEOMETRY_DATA.c_hull = createSphereTriangularMesh([0 0 0],1);
        ME(object_ID_index).DYNAMICS_DATA.cm_coord = [3.5 0 5.8]';
        ME(object_ID_index).DYNAMICS_DATA.vel = 1e-1*[1 1 1]';
        
        z_rot = deg2rad(0);
        y_rot = deg2rad(0);
        x_rot = deg2rad(0);
        myquaternions = quatr(rot_tr(z_rot,y_rot,x_rot));
        ME(object_ID_index).DYNAMICS_DATA.quaternions = [myquaternions(1);-myquaternions(2:4)];
        
        RMG = rquat(myquaternions);        
        TMG = trasm(RMG,ME(object_ID_index).DYNAMICS_DATA.cm_coord);
        RGM = RMG'; TGM = inv(TMG);
        c_hull_M = ME(object_ID_index).GEOMETRY_DATA.c_hull;
        a = max(c_hull_M(:,1))-min(c_hull_M(:,1));
        b = max(c_hull_M(:,2))-min(c_hull_M(:,2));
        c = max(c_hull_M(:,3))-min(c_hull_M(:,3));
        p_OF_M = [-a/2;...
                  -b/2;...
                  -c/2];
        RFM = eye(3); TFM = trasm(RFM,p_OF_M);
        RMF = RFM'; TMF = inv(TFM);
        RFG = RMG*RFM; TFG = TMG*TFM;
        RGF = RFG'; TGF = inv(TFG);

        ME(object_ID_index).DYNAMICS_DATA.quaternions(2:4) = -ME(object_ID_index).DYNAMICS_DATA.quaternions(2:4);

    % FRAGMENT
    case 10 
        COLLISION_DATA(count).TARGET.ID = count;        % FRAGMENT
        COLLISION_DATA(count).TARGET.type = 0;      % FRAGMENT
        object_ID_index = COLLISION_DATA(count).TARGET.ID;
        FRAGMENTS(object_ID_index).GEOMETRY_DATA.shape_ID = 0;
        [FRAGMENTS(object_ID_index).GEOMETRY_DATA.c_hull,~,~] = createOctahedron;
        FRAGMENTS(object_ID_index).DYNAMICS_DATA.cm_coord = [3.5 0 5.8]';
        FRAGMENTS(object_ID_index).DYNAMICS_DATA.vel = 1e-1*[1 1 1]';
        
        z_rot = deg2rad(0);
        y_rot = deg2rad(0);
        x_rot = deg2rad(0);
        myquaternions = quatr(rot_tr(z_rot,y_rot,x_rot));
        FRAGMENTS(object_ID_index).DYNAMICS_DATA.quaternions = [myquaternions(1);-myquaternions(2:4)];
        
        RMG = rquat(myquaternions);
        TMG = trasm(RMG,FRAGMENTS(object_ID_index).DYNAMICS_DATA.cm_coord);
        RGM = RMG'; TGM = inv(TMG);
        c_hull_M = FRAGMENTS(object_ID_index).GEOMETRY_DATA.c_hull;
        a = max(c_hull_M(:,1))-min(c_hull_M(:,1));
        b = max(c_hull_M(:,2))-min(c_hull_M(:,2));
        c = max(c_hull_M(:,3))-min(c_hull_M(:,3));
        p_OF_M = [-a/2;...
                  -b/2;...
                  -c/2];
        RFM = eye(3); TFM = trasm(RFM,p_OF_M);
        RMF = RFM'; TMF = inv(TFM);
        RFG = RMG*RFM; TFG = TMG*TFM;
        RGF = RFG'; TGF = inv(TFG);

        FRAGMENTS(object_ID_index).DYNAMICS_DATA.quaternions(2:4) = -FRAGMENTS(object_ID_index).DYNAMICS_DATA.quaternions(2:4);

    otherwise
        error('debugging:invalid count');
end

if count ~= 10
    ME(object_ID_index).FRAGMENTATION_DATA.breakup_flag = 1;
    ME(object_ID_index).FRAGMENTATION_DATA.cMLOSS = 0.15;
    ME(object_ID_index).FRAGMENTATION_DATA.cELOSS = 0.55;
    ME(object_ID_index).FRAGMENTATION_DATA.c_EXPL = 0;
    ME(object_ID_index).GEOMETRY_DATA.mass = 50;
    ME(object_ID_index).GEOMETRY_DATA.mass0 = 50;
    ME(object_ID_index).FRAGMENTATION_DATA.seeds_distribution_ID = 0; %<<<<=== 0 random, 3 McKnight, 4 uniform density
    ME(object_ID_index).FRAGMENTATION_DATA.seeds_distribution_param1 = 100;
    ME(object_ID_index).FRAGMENTATION_DATA.seeds_distribution_param2 = 5;    
    COLLISION_DATA(count).TARGET.mass = ME(object_ID_index).GEOMETRY_DATA.mass;
    COLLISION_DATA(count).TARGET.c_EXPL = ME(object_ID_index).FRAGMENTATION_DATA.c_EXPL;
else
    FRAGMENTS(object_ID_index).FRAGMENTATION_DATA.breakup_flag = 1;
    FRAGMENTS(object_ID_index).FRAGMENTATION_DATA.cMLOSS = 0.15;
    FRAGMENTS(object_ID_index).FRAGMENTATION_DATA.cELOSS = 0.55;
    FRAGMENTS(object_ID_index).FRAGMENTATION_DATA.c_EXPL = 0;
    FRAGMENTS(object_ID_index).GEOMETRY_DATA.mass = 50;
    FRAGMENTS(object_ID_index).GEOMETRY_DATA.mass0 = 50;
    FRAGMENTS(object_ID_index).FRAGMENTATION_DATA.seeds_distribution_ID = 0; %<<<<=== 0 random, 3 McKnight
    FRAGMENTS(object_ID_index).FRAGMENTATION_DATA.seeds_distribution_param1 = 100;
    FRAGMENTS(object_ID_index).FRAGMENTATION_DATA.seeds_distribution_param2 = 5;    
    COLLISION_DATA(count).TARGET.mass = FRAGMENTS(object_ID_index).GEOMETRY_DATA.mass;
    COLLISION_DATA(count).TARGET.c_EXPL = FRAGMENTS(object_ID_index).FRAGMENTATION_DATA.c_EXPL;
end

COLLISION_DATA(count).F_L = 0.5;
COLLISION_DATA(count).Ek_TOT = 1;
COLLISION_DATA(count).TARGET.Ek = 1;


% ------------
% IMPACT POINT
switch count

    % BOX/PLATE
    case SHAPE_ID_LIST.SOLID_BOX 
        a = ME(object_ID_index).GEOMETRY_DATA.dimensions(1);
        b = ME(object_ID_index).GEOMETRY_DATA.dimensions(2);
        c = ME(object_ID_index).GEOMETRY_DATA.dimensions(3);
        switch test_case
            case 1
                impact_points_F = [3.0 0.8 -0.1];
            case 2
                impact_points_F = [3.0 0.8 -0.1; ... % 3 0.8 -0.1
                                   2.8 0.3 -0.2; ...
                                   0.5 0.75 -0.15; ...
                                   1.8 0.9 0.1];
            case 3
                impact_points_F = [3.0 0.8 -0.1; ... % 3 0.8 -0.1
                                   3.5 0.5 0.0025];
            case 4
                impact_points_F = [3.0 0.8 -0.1; ... % 3 0.8 -0.1
                                   3.5 0.2 0.8];
        end

    % SOLID SPHERE
    case SHAPE_ID_LIST.SOLID_SPHERE 
        a = 2*ME(object_ID_index).GEOMETRY_DATA.dimensions(1);
        b = a;
        c = a;
        switch test_case
            case 1
                impact_points_F = [1.1*a b/2 c/2];
            case 2
                impact_points_F = [1.1*a b/2 c/2; ...
                                   a 0.25*b 0.6*c; ...
                                   -0.3 b/2 c/2; ...
                                   a 0.75*b 0.6*c];        
        end

    % SOLID CYLINDER    
    case SHAPE_ID_LIST.SOLID_CYLINDER 
        a = 2*ME(object_ID_index).GEOMETRY_DATA.dimensions(1); % diameter
        b = a;
        c = ME(object_ID_index).GEOMETRY_DATA.dimensions(3);
        switch test_case
            case 1
                impact_points_F = [a b/2 c/2+0.2];
            case 2
                impact_points_F = [a*1.1 b/2 c/2+0.2; ...
                                   a*1.1 b c/3; ...
                                   -0.2 b*2/3 c*0.4; ...
                                   a*0.6 b*0.2 c*1.1];
        end

    % CONVEX HULL    
    case SHAPE_ID_LIST.CONVEX_HULL 
        c_hull_M = ME(object_ID_index).GEOMETRY_DATA.c_hull;
        a = max(c_hull_M(:,1))-min(c_hull_M(:,1));
        b = max(c_hull_M(:,2))-min(c_hull_M(:,2));
        c = max(c_hull_M(:,3))-min(c_hull_M(:,3));
        switch test_case
            case 1
                impact_points_F = [a b c*0.8];
            case 2
                impact_points_F = [a b c*0.8; ...
                                   a 0 c*0.4];
        end

    % FRAGMENTS (as convex hull)    
    case 10 
        c_hull_M = FRAGMENTS(object_ID_index).GEOMETRY_DATA.c_hull;
        a = max(c_hull_M(:,1))-min(c_hull_M(:,1));
        b = max(c_hull_M(:,2))-min(c_hull_M(:,2));
        c = max(c_hull_M(:,3))-min(c_hull_M(:,3));
        switch test_case
            case 1
                impact_points_F = [a b c*0.8];
            case 2
                impact_points_F = [a b c*0.8; ...
                                   a 0 c*0.4];
        end

    otherwise
        error('debugginh:invalid count');
end


for i=1:size(impact_points_F,1)
    if count ~= 10
        COLLISION_DATA(count).IMPACTOR(i).POINT = trasfg_vectors(impact_points_F(i,:)',TFG)';
%                                     [ impact_points_F(i,:) + ...
%                                     - [a/2, b/2, c/2] + ...
%                                     + [ME(object_ID_index).DYNAMICS_DATA.cm_coord(1), ...
%                                        ME(object_ID_index).DYNAMICS_DATA.cm_coord(2), ...
%                                        ME(object_ID_index).DYNAMICS_DATA.cm_coord(3)] ]; % ### point is a row vector
    else
        COLLISION_DATA(count).IMPACTOR(i).POINT = [ impact_points_F(i,:) + ...
                                - [a/2, b/2, c/2] + ...
                                + [FRAGMENTS(object_ID_index).DYNAMICS_DATA.cm_coord(1), ...
                                   FRAGMENTS(object_ID_index).DYNAMICS_DATA.cm_coord(2), ...
                                   FRAGMENTS(object_ID_index).DYNAMICS_DATA.cm_coord(3)] ]; % ### point is a row vector
    end
    COLLISION_DATA(count).IMPACTOR(i).Ek = 1;
    COLLISION_DATA(count).IMPACTOR(i).mass = 1;
    COLLISION_DATA(count).IMPACTOR(i).type = 0;
end

% ---------------
% IMPACT VELOCITY
switch count

    % BOX / PLATE
    case SHAPE_ID_LIST.SOLID_BOX 
        switch test_case
            case 1
                COLLISION_DATA(count).IMPACTOR(1).v_rel = [0.3 0 1]';
            case 2
                COLLISION_DATA(count).IMPACTOR(1).v_rel = [0.3 0 1]';
                COLLISION_DATA(count).IMPACTOR(2).v_rel = [1 0 0.5]';
                COLLISION_DATA(count).IMPACTOR(3).v_rel = [0 0.2 1]';
                COLLISION_DATA(count).IMPACTOR(4).v_rel = -0.5*[1 1 1]';
            case 3
                COLLISION_DATA(count).IMPACTOR(1).v_rel = [-1 0 0.3]';
                COLLISION_DATA(count).IMPACTOR(2).v_rel = [-1 0 0]';
            case 4
                COLLISION_DATA(count).IMPACTOR(1).v_rel = [-0.7 0 0.4]';
                COLLISION_DATA(count).IMPACTOR(2).v_rel = [-0.5 0.5 -0.4]';
        end

        for i=1:length(COLLISION_DATA(count).IMPACTOR)
            COLLISION_DATA(count).IMPACTOR(1).v_rel = (RFG*(COLLISION_DATA(count).IMPACTOR(1).v_rel));
        end

    % SOLID SPHERE    
    case SHAPE_ID_LIST.SOLID_SPHERE 
        switch test_case
            case 1
                COLLISION_DATA(count).IMPACTOR(1).v_rel = [-1 0 0.3]';
            case 2
                COLLISION_DATA(count).IMPACTOR(1).v_rel = [-1 0 0.3]';
                COLLISION_DATA(count).IMPACTOR(2).v_rel = [-1 0 0.2]';
                COLLISION_DATA(count).IMPACTOR(3).v_rel = [1 0.5 -0.3]';
                COLLISION_DATA(count).IMPACTOR(4).v_rel = [-1 -1 -0.2]';
        end

    % SOLID CYLINDER    
    case SHAPE_ID_LIST.SOLID_CYLINDER 
        switch test_case
            case 1
                COLLISION_DATA(count).IMPACTOR(1).v_rel = [-1 0.2 -0.2]';
            case 2
                COLLISION_DATA(count).IMPACTOR(1).v_rel = [-1 0.2 -0.2]';
                COLLISION_DATA(count).IMPACTOR(2).v_rel = [-1 -1 0]';
                COLLISION_DATA(count).IMPACTOR(3).v_rel = [1 -0.2 0.3]';
                COLLISION_DATA(count).IMPACTOR(4).v_rel = [-0.2 0.1 -1]';
        end

    % CONVEX HULL    
    case SHAPE_ID_LIST.CONVEX_HULL 
        switch test_case
            case 1
                COLLISION_DATA(count).IMPACTOR(1).v_rel = [-1 -1 -0.3]';
            case 2
                COLLISION_DATA(count).IMPACTOR(1).v_rel = [-1 -1 -0.3]';
                COLLISION_DATA(count).IMPACTOR(2).v_rel = [-1 1 0.3]';
        end

    % FRAGMENT    
    case 10 
        switch test_case
            case 1
                COLLISION_DATA(count).IMPACTOR(1).v_rel = [-1 -1 -0.3]';
            case 2
                COLLISION_DATA(count).IMPACTOR(1).v_rel = [-1 -1 -0.3]';
                COLLISION_DATA(count).IMPACTOR(2).v_rel = [-1 1 0.3]';
        end

    otherwise
        error('debugginh:invalid count');
end

clear a b c;
if count == SHAPE_ID_LIST.SOLID_BOX
    clear RMG TMG RGM TGM p_OF_M RFM TFM RMF TMF RFG TFG RGF TGF
end






