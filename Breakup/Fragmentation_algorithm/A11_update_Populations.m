function COLLISION_DATA=A11_update_Populations(COLLISION_DATA,Frag_ME,Impact_Data,Frag_Data,Frag_Volume,frags_in_volume,debugging_mode,Q_EXPL)
%A11_update_Populations Update populatrions structures ME_FR, FRAGMENTS_FR and
%BUBBLE
%
% Syntax:  COLLISION_DATA=A11_update_Populations(COLLISION_DATA,Frag_ME,Impact_Data,Frag_Data,frags_in_volume,debugging_mode,Q_EXPL)
%
% Inputs:
%    COLLISION_DATA - COLLISION_DATA(count) structure
%    Frag_ME - Frag_ME structure
%    Impact_Data - Impact_Data structure
%    Frag_Data - Frag_Data structure
%    frags_in_volume - frags_in_volume structure
%    debugging_mode - debugging_mode flag
%    Q_EXPL - Momentum of explosive contribution
%
% Outputs:
%    Updated ME_FR, FRAGMENTS and BUBBLE populations (global variables)
%    COLLISION_DATA - update to velocity V_LOSS
%
% Other m-files required: population_empty_structure, trasfg_vectors
%                         bubble_threshold, plate_modification
% Subfunctions: none
% MAT-files required: length
%
% See also:
%
% Author:   Andrea Valmorbida, Ph.D.
%           Center of Studies and Activities for Space "Giuseppe Colombo" (CISAS)
%           University of Padova
% Email address: andrea.valmorbida@unipd.it
% Date: 2018/08/02
% Revision: 3.0
% Copyright: 2017 CISAS - UNIVERSITY OF PADOVA
%
% HISTORY
% 2017/11/24 : first version by AV
% 2018/06/15 : second version by GS & LO (Q_LOSS)
% 2018/08/02 : third version by LO & CG (velocity model, bubbles)

%#codegen

global ME_FR;
global FRAGMENTS_FR;
global BUBBLE;
global PROPAGATION_THRESHOLD;
global SR_delta;
global SHAPE_ID_LIST;
global CRATER_SHAPE_LIST;
global HOLES;
global HOLES_ANALYSIS_FLAG;
global MATERIAL_LIST

% ---------------------------------------
% Update FRAGMENTS and BUBBLE populations

% k => index referred to each impact
% i => index referred to each fragment

% mass_toll=1e-5; % tolerance in killed masses
length_fr=length(FRAGMENTS_FR);
% Vc_frag_max = 1.32e4;
[~, ~, ~, ~, ~, ~, ~, ~, Vc_frag_max, ~] = Parameters_Setup();

if debugging_mode == 1
    %     v_CM = [0 0 0]';
    material_ID = 1;
else
    %     v_CM = COLLISION_DATA.V_CM;
    switch Frag_ME.target_type
        case 0 % FRAGMENT
            material_ID = FRAGMENTS_FR(Frag_ME.object_ID_index).material_ID;
            %v_CM = FRAGMENTS_FR(Frag_ME.object_ID_index).DYNAMICS_DATA.vel;
            %             n_seeds = round(FRAGMENTS_FR(Frag_ME.object_ID_index).FRAGMENTATION_DATA.seeds_distribution_param1/FRAGMENTS_FR(Frag_ME.object_ID_index).GEOMETRY_DATA.mass0); % o mass? GS
        case 1 % ME_FR
            %v_CM = ME_FR(Frag_ME.object_ID_index).DYNAMICS_DATA.vel;
            material_ID = ME_FR(Frag_ME.object_ID_index).material_ID;
            %             n_seeds = round(ME_FR(Frag_ME.object_ID_index).FRAGMENTATION_DATA.seeds_distribution_param1/ME_FR(Frag_ME.object_ID_index).GEOMETRY_DATA.mass0);
    end
end

delta_pos = [0 0 0]';

if Frag_ME.target_type == 1
    if debugging_mode == 1
        SR_delta(:,Frag_ME.object_ID_index) = [0 0 0]';
    end
    delta_pos = SR_delta(:,Frag_ME.object_ID_index);
end

quaternion = [1 0 0 0];
total_fragments_mass = 0;
total_fragments_Q = [0 0 0]';
norm_total_fragments_Q = 0;
total_BUBBLE_Q = [0 0 0]';
norm_total_BUBBLE_Q = 0;
killed_fragments_Q = [0 0 0]';
norm_killed_fragments_Q = 0;
killed_fragments_mass = 0;
[v_NEW_0,~]=velocity_model_breakup(COLLISION_DATA,1);

FRAGMENTS_coll_TOT=struct('FRAGMENTS_coll',population_empty_structure(),'v_dot_coll',[]);

for k = 1:Impact_Data.N_frag_vol
    
    % Create a FRAGMENTS structure for each fragment of the Frag Volume
    FRAGMENTS_coll = population_empty_structure();
    
    kk = 1; %
    kk_killed=0;
    mass_frag_k=0;
    for i=1:frags_in_volume{k}.ncells
        if Frag_ME.shape_ID == SHAPE_ID_LIST.HOLLOW_ELLIPSOID
            mass_frag_k= mass_frag_k + frags_in_volume{k}.mass_cyl(i);
        else
            mass_frag_k= mass_frag_k + frags_in_volume{k}.mass(i);
        end
    end
    if mass_frag_k >0
        % calculation of velocities
        [v_CM_TAR,~]=velocity_model_breakup(COLLISION_DATA,1);
        m_T=COLLISION_DATA.TARGET.mass-mass_frag_k;
        V_CM_FRAG=v_CM_TAR; % Initial velocity is the same
        [V_CM_T, V_CM_FR]=velocity_model_fragmentationV2(v_CM_TAR,V_CM_FRAG,m_T,mass_frag_k,COLLISION_DATA);
        COLLISION_DATA.TARGET.v_loss = COLLISION_DATA.TARGET.v_loss +(v_CM_TAR - V_CM_T);
        dbstop at 123 if any(isnan(COLLISION_DATA.TARGET.v_loss));
        for i=1:frags_in_volume{k}.ncells
            
            if frags_in_volume{k}.kill_flag(i,1) == 0
                
                switch Frag_ME.target_type
                    case 0
                        FRAGMENTS_coll(kk) = FRAGMENTS_FR(Frag_ME.object_ID_index);
%                         FRAGMENTS_coll(kk).FRAGMENTATION_DATA.param_add1 = FRAGMENTS_FR(Frag_ME.object_ID_index).object_ID_index;
                    case 1
                        FRAGMENTS_coll(kk) = ME_FR(Frag_ME.object_ID_index);
                end
                
                FRAGMENTS_coll(kk).object_ID_index = length(FRAGMENTS_FR)+kk;
                FRAGMENTS_coll(kk).material_ID = material_ID;
                
                % GEOMETRY DATA
                FRAGMENTS_coll(kk).GEOMETRY_DATA.shape_ID = 0;
                FRAGMENTS_coll(kk).GEOMETRY_DATA.thick = 0;
                if Frag_ME.shape_ID == SHAPE_ID_LIST.HOLLOW_ELLIPSOID
                    FRAGMENTS_coll(kk).GEOMETRY_DATA.dimensions = [(frags_in_volume{k}.volume_cyl(i)/pi*3/4)^(1/3) 0 0]; % radius of the equivalent sphere
                    FRAGMENTS_coll(kk).GEOMETRY_DATA.mass0 = frags_in_volume{k}.mass_cyl(i);
                else
                    FRAGMENTS_coll(kk).GEOMETRY_DATA.dimensions = [(frags_in_volume{k}.volume(i)/pi*3/4)^(1/3) 0 0]; % radius of the equivalent sphere
                    FRAGMENTS_coll(kk).GEOMETRY_DATA.mass0 = frags_in_volume{k}.mass(i);
                end
                
                total_fragments_mass = total_fragments_mass + FRAGMENTS_coll(kk).GEOMETRY_DATA.mass0;
                
                FRAGMENTS_coll(kk).GEOMETRY_DATA.mass = FRAGMENTS_coll(kk).GEOMETRY_DATA.mass0;
                %             FRAGMENTS_coll(kk).GEOMETRY_DATA.A_M_ratio = pi*FRAGMENTS_coll(kk).GEOMETRY_DATA.dimensions(1)^2/FRAGMENTS_coll(kk).GEOMETRY_DATA.mass;
                if isSolidShape(Frag_ME.shape_ID)
                    FRAGMENTS_coll(kk).GEOMETRY_DATA.c_hull = (Frag_ME.RFG*(frags_in_volume{k}.vorvx{i} - frags_in_volume{k}.CoM(i,:))')'; % in the Body RF
                else
                    if Frag_ME.shape_ID == SHAPE_ID_LIST.HOLLOW_ELLIPSOID
                        FRAGMENTS_coll(kk).GEOMETRY_DATA.c_hull = (Frag_ME.RFG*(frags_in_volume{k}.vorvx_cart_cyl{i} - frags_in_volume{k}.CoM_cyl(i,:))')'; % in the Body RF
                    else
                        FRAGMENTS_coll(kk).GEOMETRY_DATA.c_hull = (Frag_ME.RFG*(frags_in_volume{k}.vorvx_cart{i} - frags_in_volume{k}.CoM(i,:))')'; % in the Body RF
                    end
                end
                lx = (max(FRAGMENTS_coll(kk).GEOMETRY_DATA.c_hull(:,1)) - min(FRAGMENTS_coll(kk).GEOMETRY_DATA.c_hull(:,1)));
                ly = (max(FRAGMENTS_coll(kk).GEOMETRY_DATA.c_hull(:,2)) - min(FRAGMENTS_coll(kk).GEOMETRY_DATA.c_hull(:,2)));
                lz = (max(FRAGMENTS_coll(kk).GEOMETRY_DATA.c_hull(:,3)) - min(FRAGMENTS_coll(kk).GEOMETRY_DATA.c_hull(:,3)));
                FRAGMENTS_coll(kk).GEOMETRY_DATA.A_M_ratio = mean([lx*ly;lx*lz;ly*lz])/FRAGMENTS_coll(kk).GEOMETRY_DATA.mass0;
                
                % DYNAMICS_DATA
                if Frag_ME.shape_ID == SHAPE_ID_LIST.HOLLOW_ELLIPSOID
                    FRAGMENTS_coll(kk).DYNAMICS_DATA.cm_coord = trasfg_vectors(frags_in_volume{k}.CoM_cyl(i,:)',Frag_ME.TFG) + delta_pos; % in the Global RF
                else
                    FRAGMENTS_coll(kk).DYNAMICS_DATA.cm_coord = trasfg_vectors(frags_in_volume{k}.CoM(i,:)',Frag_ME.TFG) + delta_pos; % in the Global RF
                end
                FRAGMENTS_coll(kk).DYNAMICS_DATA.vel = (Frag_ME.RFG*(frags_in_volume{k}.vel(i,:)')) + V_CM_FR;
                FRAGMENTS_coll(kk).DYNAMICS_DATA.quaternions = quaternion';
                FRAGMENTS_coll(kk).DYNAMICS_DATA.w = [0 0 0]';
                FRAGMENTS_coll(kk).DYNAMICS_DATA.virt_momentum = [0 0 0]';
                
                
                % DYNAMICS_INITIAL_DATA
                FRAGMENTS_coll(kk).DYNAMICS_INITIAL_DATA.cm_coord0 = FRAGMENTS_coll(kk).DYNAMICS_DATA.cm_coord;
                FRAGMENTS_coll(kk).DYNAMICS_INITIAL_DATA.vel0 = FRAGMENTS_coll(kk).DYNAMICS_DATA.vel;
                FRAGMENTS_coll(kk).DYNAMICS_INITIAL_DATA.quaternions0 = FRAGMENTS_coll(kk).DYNAMICS_DATA.quaternions;
                FRAGMENTS_coll(kk).DYNAMICS_INITIAL_DATA.w0 = FRAGMENTS_coll(kk).DYNAMICS_DATA.w;
                
                FRAGMENTS_coll(kk).FRAGMENTATION_DATA.threshold0 = k_frag(FRAGMENTS_coll(kk).material_ID,FRAGMENTS_coll(kk).material_ID)/MATERIAL_LIST(FRAGMENTS_coll(kk).material_ID).density;
                FRAGMENTS_coll(kk).FRAGMENTATION_DATA.threshold = FRAGMENTS_coll(kk).FRAGMENTATION_DATA.threshold0;
                FRAGMENTS_coll(kk).FRAGMENTATION_DATA.breakup_flag = 1;
                FRAGMENTS_coll(kk).FRAGMENTATION_DATA.failure_ID = 3;
                if debugging_mode ~= 1
                    energy=0.5*FRAGMENTS_coll(kk).GEOMETRY_DATA.mass*(norm(FRAGMENTS_coll(kk).DYNAMICS_DATA.vel)^2);
                    cond_m = PROPAGATION_THRESHOLD.fragment_mass_th_flag*(FRAGMENTS_coll(kk).GEOMETRY_DATA.mass<PROPAGATION_THRESHOLD.fragment_mass_th);
                    cond_d = PROPAGATION_THRESHOLD.fragment_radius_th_flag*(FRAGMENTS_coll(kk).GEOMETRY_DATA.dimensions(1)<PROPAGATION_THRESHOLD.fragment_radius_th);
                    cond_e = PROPAGATION_THRESHOLD.fragment_energy_th_flag*(energy<PROPAGATION_THRESHOLD.fragment_energy_th);
                    
                    if (cond_m && cond_d && cond_e) || (cond_m && cond_d  && PROPAGATION_THRESHOLD.fragment_energy_th_flag==0) || (cond_d && cond_e  && PROPAGATION_THRESHOLD.fragment_mass_th_flag==0) ...
                            || (cond_m && cond_e  && PROPAGATION_THRESHOLD.fragment_radius_th_flag==0) || (cond_m && PROPAGATION_THRESHOLD.fragment_radius_th_flag==0  && PROPAGATION_THRESHOLD.fragment_energy_th_flag==0) ...
                            || (cond_d && PROPAGATION_THRESHOLD.fragment_energy_th_flag==0  && PROPAGATION_THRESHOLD.fragment_mass_th_flag==0) || (cond_e && PROPAGATION_THRESHOLD.fragment_mass_th_flag==0  && PROPAGATION_THRESHOLD.fragment_radius_th_flag==0)
                        FRAGMENTS_coll(kk).FRAGMENTATION_DATA.breakup_flag = 0;
                        FRAGMENTS_coll(kk).FRAGMENTATION_DATA.threshold=0;
                    end
                end
                
                % Fragmentation DATA
                FRAGMENTS_coll(kk).FRAGMENTATION_DATA.c_EXPL=0;
                %
                if COLLISION_DATA.TARGET.mass>0
                    mass_fraction=FRAGMENTS_coll(kk).GEOMETRY_DATA.mass/COLLISION_DATA.TARGET.mass;
                else
                    mass_fraction=1;
                end
                FRAGMENTS_coll(kk).FRAGMENTATION_DATA.seeds_distribution_param1=max([1 ceil(FRAGMENTS_coll(kk).FRAGMENTATION_DATA.seeds_distribution_param1*mass_fraction)]);
                %              FRAGMENTS_coll(kk).FRAGMENTATION_DATA.seeds_distribution_ID = 3;
                %             FRAGMENTS_coll(kk).FRAGMENTATION_DATA.seeds_distribution_param1 = 0;
                %             FRAGMENTS_coll(kk).FRAGMENTATION_DATA.seeds_distribution_param2 = 0;
                
                total_fragments_Q = total_fragments_Q + FRAGMENTS_coll(kk).GEOMETRY_DATA.mass0*FRAGMENTS_coll(kk).DYNAMICS_DATA.vel;
                norm_total_fragments_Q = norm_total_fragments_Q + FRAGMENTS_coll(kk).GEOMETRY_DATA.mass0*norm(FRAGMENTS_coll(kk).DYNAMICS_DATA.vel);
                kk = kk + 1;
                
            else
                kk_killed=kk_killed+1;
                if Frag_ME.shape_ID == SHAPE_ID_LIST.HOLLOW_ELLIPSOID
                    frags_in_volume_k_mass = frags_in_volume{k}.mass_cyl(i);
                else
                    frags_in_volume_k_mass = frags_in_volume{k}.mass(i);
                end
                frags_in_volume_k_vel = (Frag_ME.RFG*(frags_in_volume{k}.vel(i,:)')) + V_CM_FR;
                
                killed_fragments_mass = killed_fragments_mass + frags_in_volume_k_mass;
                killed_fragments_Q = killed_fragments_Q + frags_in_volume_k_mass*frags_in_volume_k_vel;
                norm_killed_fragments_Q = norm_killed_fragments_Q + frags_in_volume_k_mass*norm(frags_in_volume_k_vel);
                
            end
            
        end
        FRAGMENTS_coll_TOT(k).FRAGMENTS_coll=[FRAGMENTS_coll];
    else
        FRAGMENTS_coll_TOT(k).FRAGMENTS_coll=[];
    end
end

%% UPDATE VELOCITIES E QLOSS
%  update velocity for linear momentum conservation of the target ME
m_new = Frag_ME.mass-total_fragments_mass-killed_fragments_mass;
% dbstop at 246 if (m_new<0)
m_new = max([0 m_new]);
[v_NEW,~]=velocity_model_breakup(COLLISION_DATA,1);
Q_old_target=COLLISION_DATA.TARGET.mass*v_NEW_0;
m_new=m_new+killed_fragments_mass;
if(m_new>0)
    COLLISION_DATA.TARGET.v_loss = COLLISION_DATA.TARGET.v_loss - killed_fragments_Q/(m_new);
    [v_NEW,~]=velocity_model_breakup(COLLISION_DATA,1);
end
if COLLISION_DATA.F_L == 1
    m_new=0;
end
Q_new_target=m_new*v_NEW;
Q_loss =  Q_old_target + Q_EXPL - (total_fragments_Q ) - Q_new_target;
Q_loss=-Q_loss;
% dbstop at 260 if any(isnan(COLLISION_DATA.TARGET.v_loss));
if ~isempty(FRAGMENTS_coll_TOT)
    for kk =1:length(FRAGMENTS_coll_TOT)
        for jj =1:length(FRAGMENTS_coll_TOT(kk).FRAGMENTS_coll)
            if(~isempty(FRAGMENTS_coll_TOT(kk).FRAGMENTS_coll(jj)))
                FRAGMENTS_coll_TOT(kk).FRAGMENTS_coll(jj).DYNAMICS_DATA.vel = FRAGMENTS_coll_TOT(kk).FRAGMENTS_coll(jj).DYNAMICS_DATA.vel - (Q_loss)*norm(FRAGMENTS_coll_TOT(kk).FRAGMENTS_coll(jj).DYNAMICS_DATA.vel)/(norm_total_fragments_Q); %/(total_fragments_mass)^2; % *(FRAGMENTS_coll_TOT(kk).FRAGMENTS_coll(jj).GEOMETRY_DATA.mass0) % norm(FRAGMENTS_coll_TOT(kk).FRAGMENTS_coll(jj).DYNAMICS_DATA.vel)/(norm_total_fragments_Q);
                unit_vector_vel = 1/norm(FRAGMENTS_coll_TOT(kk).FRAGMENTS_coll(jj).DYNAMICS_DATA.vel)*FRAGMENTS_coll_TOT(kk).FRAGMENTS_coll(jj).DYNAMICS_DATA.vel;
                norm_vel = min([norm(FRAGMENTS_coll_TOT(kk).FRAGMENTS_coll(jj).DYNAMICS_DATA.vel) Vc_frag_max]);
                FRAGMENTS_coll_TOT(kk).FRAGMENTS_coll(jj).DYNAMICS_DATA.vel = norm_vel*unit_vector_vel;
                FRAGMENTS_coll_TOT(kk).FRAGMENTS_coll(jj).DYNAMICS_INITIAL_DATA.vel0 = FRAGMENTS_coll_TOT(kk).FRAGMENTS_coll(jj).DYNAMICS_DATA.vel;
                FRAGMENTS_coll_TOT(kk).v_dot_coll(jj)=dot(FRAGMENTS_coll_TOT(kk).FRAGMENTS_coll(jj).DYNAMICS_DATA.vel,COLLISION_DATA.IMPACTOR(kk).vel);
            end
        end
    end
end

%% BUBBLES CREATION
for k = 1:Impact_Data.N_frag_vol
    if debugging_mode ~= 1
        % compute FRAGMENTS_temp and BUBBLE_temp for each Frag Volume
        [FRAGMENTS_temp, BUBBLE_temp] = bubble_threshold(FRAGMENTS_coll_TOT(k).FRAGMENTS_coll, FRAGMENTS_coll_TOT(k).v_dot_coll, PROPAGATION_THRESHOLD);
        
        % update FRAGMENTS_temp.object_ID_index
        if ~isempty(FRAGMENTS_temp)
            for i=1:length(FRAGMENTS_temp)
                FRAGMENTS_temp(i).object_ID_index = length(FRAGMENTS_FR)+i;
                FRAGMENTS_temp(i).object_ID = FRAGMENTS_temp(i).object_ID;
                dbstop at 289 if (norm(FRAGMENTS_temp(i).DYNAMICS_DATA.vel)>1.58e4)
            end
        end
        
        if ~isempty(BUBBLE_temp)
            disp(['Created Bubble with fragments generated from target of type ',num2str(Frag_ME.target_type), ' and ID ', num2str(Frag_ME.object_ID_index)]);
            for i=1:length(BUBBLE_temp)
                total_BUBBLE_Q = total_BUBBLE_Q + BUBBLE_temp(i).GEOMETRY_DATA.mass0*BUBBLE_temp(i).DYNAMICS_DATA.vel;
                norm_total_BUBBLE_Q = norm_total_BUBBLE_Q + BUBBLE_temp(i).GEOMETRY_DATA.mass0*norm(BUBBLE_temp(i).DYNAMICS_DATA.vel);
            end
        end
        % Update FRAGMENTS and BUBBLE populations
        if((isempty(FRAGMENTS_temp))<1)
            FRAGMENTS_FR = [FRAGMENTS_FR,FRAGMENTS_temp];
        end
        if((isempty(BUBBLE_temp))<1)
            for i_bb=1:length(BUBBLE_temp)
                BUBBLE_temp(i_bb).object_ID_index = length(BUBBLE)+1;
                BUBBLE = [BUBBLE,BUBBLE_temp(i_bb)];
            end
        end
    end
end

% -----------------------
% Update HOLES population
if (HOLES_ANALYSIS_FLAG>0) && (Frag_ME.target_type == 1) && isSolidShape(Frag_ME.shape_ID) && (COLLISION_DATA.F_L<1)
    for k = 1:Impact_Data.N_frag_vol
        
        [~,~,h_ext_P3]=InternalNormals(Impact_Data.I_POS_F(k,:),Impact_Data.I_VEL_F(k,:),Frag_Volume{k},Frag_Data(k,1:4));
        
        %         if ~isParallel3d(zv,[1 0 0])
        %             yv = vectorCross3d(zv,[1 0 0]);
        %         else
        %             yv = vectorCross3d(zv,[0 1 0]);
        %         end
        %         yv = yv/vectorNorm3d(yv);
        %         xv = vectorCross3d(yv,zv);
        %         RVF = [xv',yv',zv'];
        %         pVF = Impact_Data.I_POS_F(i,:)';
        %         TVF = trasm(RVF,pVF);
        %         TFV = inv(TVF);
        
        
        %         [~,volume] = polyhedronCentroidVolume(Frag_Volume{k}.v);
        %          if((volume/pi*3/4)^(1/3)>=min(ME_FR(Frag_ME.object_ID_index).GEOMETRY_DATA.dimensions(ME_FR(Frag_ME.object_ID_index).GEOMETRY_DATA.dimensions>0)))
        %             HOLES_temp = ME_FR(Frag_ME.object_ID_index);
        %             % index ID of the parent
        %             HOLES_temp.object_ID = Frag_ME.object_ID_index;
        %             HOLES_temp.object_ID_index=length(HOLES)+1;
        if Frag_Data(k,11) == CRATER_SHAPE_LIST.ELLIPSOID % ellipsoid
            % %             if(volume < 2/3*pi*Frag_Data(k,4)*Frag_Data(k,5)*Frag_Data(k,6))
            %             h_ext_max=Frag_Data(k,1:3)+Frag_Data(k,6)*zv;
            %             if(~isPointWithinConvexHull(h_ext_max,Frag_ME.v,Frag_ME.f,1e-7))
            
            if(h_ext_P3<h_ext_P2)
                HOLES_temp = ME_FR(Frag_ME.object_ID_index);
                % index ID of the parent
                HOLES_temp.object_ID = Frag_ME.object_ID_index;
                HOLES_temp.object_ID_index=length(HOLES)+1;
                % shape ID
                HOLES_temp.GEOMETRY_DATA.shape_ID = 11;
                % dimensions
                HOLES_temp.GEOMETRY_DATA.dimensions = [Frag_Data(k,4) Frag_Data(k,5) Frag_Data(k,6)];
                HOLES_temp.GEOMETRY_DATA.thick=0;
                % attitude quaternion
                RHF = rquat(Frag_Data(k,7:10));
                RHG = Frag_ME.RFG*RHF;
                HOLES_temp.DYNAMICS_DATA.quaternions = quatr(RHG)';
                HOLES_temp.DYNAMICS_INITIAL_DATA.quaternions0 = quatr(RHG)';
                % CoM in the Global RF
                HOLES_temp.DYNAMICS_INITIAL_DATA.cm_coord0 = trasfg_vectors(Frag_Data(k,1:3)',Frag_ME.TFG) + delta_pos;
                HOLES_temp.DYNAMICS_DATA.cm_coord = trasfg_vectors(Frag_Data(k,1:3)',Frag_ME.TFG) + delta_pos;
                % chull: attitude in the Global RF, position w.r.t. the CoM the Body RF;
                HOLES_temp.GEOMETRY_DATA.c_hull = (Frag_ME.RFG*(Frag_Volume{k}.v-Frag_Data(k,1:3))')';
                % velocity of the parent
                HOLES_temp.DYNAMICS_INITIAL_DATA.vel0 = ME_FR(Frag_ME.object_ID_index).DYNAMICS_DATA.vel;
                HOLES_temp.DYNAMICS_DATA.vel = ME_FR(Frag_ME.object_ID_index).DYNAMICS_DATA.vel;
                HOLES_temp.FRAGMENTATION_DATA.breakup_flag = 0;
                
                disp([' ** HOLES(',num2str(HOLES_temp.object_ID_index),') created on ME(',num2str(HOLES_temp.object_ID),')'])
                
                HOLES = [HOLES,HOLES_temp];
                
            end
            
        else % sphere
            % %             if(volume < 2/3*pi*Frag_Data(k,4)*Frag_Data(k,4)*Frag_Data(k,4))
            %             h_ext_max=Frag_Data(k,1:3)+Frag_Data(k,4)*zv;
            %             if(~isPointWithinConvexHull(h_ext_max,Frag_ME.v,Frag_ME.f,1e-7))
            if(h_ext_P3<Frag_Data(k,4))
                HOLES_temp = ME_FR(Frag_ME.object_ID_index);
                % index ID of the parent
                HOLES_temp.object_ID = Frag_ME.object_ID_index;
                HOLES_temp.object_ID_index=length(HOLES)+1;
                % shape ID
                HOLES_temp.GEOMETRY_DATA.shape_ID = 10;
                % dimensions
                HOLES_temp.GEOMETRY_DATA.dimensions = [Frag_Data(k,4) Frag_Data(k,4) Frag_Data(k,4)]; % radius
                HOLES_temp.GEOMETRY_DATA.thick=0;
                % attitude quaternion
                HOLES_temp.DYNAMICS_DATA.quaternions = [1 0 0 0];
                HOLES_temp.DYNAMICS_INITIAL_DATA.quaternions0 = [1 0 0 0];
                % CoM in the Global RF
                HOLES_temp.DYNAMICS_INITIAL_DATA.cm_coord0 = trasfg_vectors(Frag_Data(k,1:3)',Frag_ME.TFG) + delta_pos;
                HOLES_temp.DYNAMICS_DATA.cm_coord = trasfg_vectors(Frag_Data(k,1:3)',Frag_ME.TFG) + delta_pos;
                % chull: attitude in the Global RF, position w.r.t. the CoM the Body RF;
                HOLES_temp.GEOMETRY_DATA.c_hull = (Frag_ME.RFG*(Frag_Volume{k}.v-Frag_Data(k,1:3))')';
                % velocity of the parent
                HOLES_temp.DYNAMICS_INITIAL_DATA.vel0 = ME_FR(Frag_ME.object_ID_index).DYNAMICS_DATA.vel;
                HOLES_temp.DYNAMICS_DATA.vel = ME_FR(Frag_ME.object_ID_index).DYNAMICS_DATA.vel;
                HOLES_temp.FRAGMENTATION_DATA.breakup_flag = 0;
                
                disp([' ** HOLES(',num2str(HOLES_temp.object_ID_index),') created on ME(',num2str(HOLES_temp.object_ID),')'])
                
                HOLES = [HOLES,HOLES_temp];
                
            end
        end
        %             % CoM in the Global RF
        %             HOLES_temp.DYNAMICS_INITIAL_DATA.cm_coord0 = trasfg_vectors(Frag_Data(k,1:3)',Frag_ME.TFG) + delta_pos;
        %             HOLES_temp.DYNAMICS_DATA.cm_coord = trasfg_vectors(Frag_Data(k,1:3)',Frag_ME.TFG) + delta_pos;
        %             % chull: attitude in the Global RF, position w.r.t. the CoM the Body RF;
        %             HOLES_temp.GEOMETRY_DATA.c_hull = (Frag_ME.RFG*(Frag_Volume{k}.v-Frag_Data(k,1:3))')';
        %             % velocity of the parent
        %             HOLES_temp.DYNAMICS_INITIAL_DATA.vel0 = ME_FR(Frag_ME.object_ID_index).DYNAMICS_DATA.vel;
        %             HOLES_temp.DYNAMICS_DATA.vel = ME_FR(Frag_ME.object_ID_index).DYNAMICS_DATA.vel;
        %             HOLES_temp.FRAGMENTATION_DATA.breakup_flag = 0;
        %
        %             disp([' ** HOLES(',num2str(HOLES_temp.object_ID_index),') created on ME(',num2str(HOLES_temp.object_ID),')'])
        %
        %             HOLES = [HOLES,HOLES_temp];
    end
    % end
end
% --------------------
% Update ME population

% breakup_flag only for fragments

% Update the fragmented object when it is a ME
if Frag_ME.target_type == 1
    % if the target ME is completely fragmented,
    % then set its mass to zero,
    % otherwise update target ME properties
    if COLLISION_DATA.F_L<1 %abs(Frag_ME.mass-total_fragments_mass)<mass_toll
        % update the mass and the velocity
        ME_FR(Frag_ME.object_ID_index).GEOMETRY_DATA.mass = max([0 Frag_ME.mass-total_fragments_mass]);
        % defining a plate as a box with third dimensions < 0.1* sqrt(a*b)
        dimensions=ME_FR(Frag_ME.object_ID_index).GEOMETRY_DATA.dimensions;
        th=min(dimensions);
        i_th=find(dimensions==th,1);
        if (Impact_Data.N_impact == 1) && (Frag_ME.shape_ID == 1) && (i_th==3)% && exist(fullfile(matlabroot,'toolbox','map','map','poly2cw.m'),'file')
            a=dimensions(1);
            b=dimensions(2);
            if(th<0.1*sqrt(a*b) && COLLISION_DATA.F_L>0.3)
                %                   ID               , IMPACT POINT       , RADIUS     , DIMENSIONS
                plate_modification(Frag_ME.object_ID_index,(Frag_Data(1,1:2))',Frag_Data(1,4),a,b,th);
                
                for i_h=1:length(HOLES)
                    if(HOLES(i_h).object_ID==Frag_ME.object_ID_index && HOLES(i_h).GEOMETRY_DATA.mass>0)
                        bnd1_post=(rquat(ME_FR(Frag_ME.object_ID_index).DYNAMICS_DATA.quaternions)*ME_FR(Frag_ME.object_ID_index).GEOMETRY_DATA.c_hull')+ME_FR(Frag_ME.object_ID_index).DYNAMICS_DATA.cm_coord;  %CM_ME;
                        bnd2_post=HOLES(i_h).GEOMETRY_DATA.c_hull'+HOLES(i_h).DYNAMICS_DATA.cm_coord;
                        bnd_pnts_post=[];
                        [bnd_pnts_post] = getPointsWithinBoundary(bnd2_post',bnd1_post');
                        if(HOLES(i_h).GEOMETRY_DATA.mass>0 && isempty(bnd_pnts_post))
                            HOLES(i_h).GEOMETRY_DATA.mass=0;
                        end
                    end
                end
            end
        end
    end
    
    % set c_EXPL to zero if the ME is exploded
    if COLLISION_DATA.TARGET.c_EXPL~=0
        COLLISION_DATA.TARGET.c_EXPL=0;
        ME_FR(Frag_ME.object_ID_index).FRAGMENTATION_DATA.c_EXPL=0; % LO + FF + GS
    end
elseif  Frag_ME.target_type == 0
    if COLLISION_DATA.F_L<1 %abs(Frag_ME.mass-total_fragments_mass)<mass_toll%1e-3
        % update the mass and the velocity
        FRAGMENTS_FR(Frag_ME.object_ID_index).GEOMETRY_DATA.mass = max([0 Frag_ME.mass-total_fragments_mass]);
    end
end

if  Frag_ME.target_type==0 && COLLISION_DATA.F_L==1 && (length(FRAGMENTS_FR)-length_fr)==1 && norm(normalizeVector3d(FRAGMENTS_FR(Frag_ME.object_ID_index).DYNAMICS_DATA.vel')- normalizeVector3d(FRAGMENTS_FR(length(FRAGMENTS_FR)).DYNAMICS_INITIAL_DATA.vel0'))<sind(1/60)
    FRAGMENTS_FR(length(FRAGMENTS_FR)).FRAGMENTATION_DATA.param_add2=FRAGMENTS_FR(length(FRAGMENTS_FR)).FRAGMENTATION_DATA.param_add2+1;
    if FRAGMENTS_FR(length(FRAGMENTS_FR)).FRAGMENTATION_DATA.param_add2>1
        ll=length(BUBBLE)+1;
        BUBBLE(ll).object_ID=FRAGMENTS_FR(length(FRAGMENTS_FR)).object_ID;
        BUBBLE(ll).object_ID_index=ll;
        BUBBLE(ll).material_ID=FRAGMENTS_FR(length(FRAGMENTS_FR)).material_ID;
        BUBBLE(ll).GEOMETRY_DATA.shape_ID=9;
        BUBBLE(ll).GEOMETRY_DATA.dimensions=FRAGMENTS_FR(length(FRAGMENTS_FR)).GEOMETRY_DATA.dimensions; %FF, I would use "mean(dist_i)", since the bubble conveys average informations.
        BUBBLE(ll).GEOMETRY_DATA.thick=0; %kg
        BUBBLE(ll).GEOMETRY_DATA.mass0=FRAGMENTS_FR(length(FRAGMENTS_FR)).GEOMETRY_DATA.mass0; %kg
        BUBBLE(ll).GEOMETRY_DATA.mass=FRAGMENTS_FR(length(FRAGMENTS_FR)).GEOMETRY_DATA.mass0; %kg
        BUBBLE(ll).GEOMETRY_DATA.A_M_ratio=(pi*BUBBLE(ll).GEOMETRY_DATA.dimensions(1)^2)/BUBBLE(ll).GEOMETRY_DATA.mass; %FF, I assume Frontal Area -> took out the "4*"
        BUBBLE(ll).GEOMETRY_DATA.c_hull=[0, 0, 0];
        BUBBLE(ll).DYNAMICS_INITIAL_DATA.cm_coord0=FRAGMENTS_FR(length(FRAGMENTS_FR)).DYNAMICS_INITIAL_DATA.cm_coord0;
        BUBBLE(ll).DYNAMICS_INITIAL_DATA.quaternions0=FRAGMENTS_FR(length(FRAGMENTS_FR)).DYNAMICS_INITIAL_DATA.quaternions0;
        BUBBLE(ll).DYNAMICS_INITIAL_DATA.vel0=FRAGMENTS_FR(length(FRAGMENTS_FR)).DYNAMICS_INITIAL_DATA.vel0; %FF using cm Velocity instead of mean vel.
        BUBBLE(ll).DYNAMICS_INITIAL_DATA.w0=FRAGMENTS_FR(length(FRAGMENTS_FR)).DYNAMICS_INITIAL_DATA.w0;
        BUBBLE(ll).DYNAMICS_INITIAL_DATA.v_exp0=0;
        BUBBLE(ll).DYNAMICS_DATA.cm_coord=BUBBLE(ll).DYNAMICS_INITIAL_DATA.cm_coord0;
        BUBBLE(ll).DYNAMICS_DATA.quaternions=BUBBLE(ll).DYNAMICS_INITIAL_DATA.quaternions0;
        BUBBLE(ll).DYNAMICS_DATA.vel=BUBBLE(ll).DYNAMICS_INITIAL_DATA.vel0;
        BUBBLE(ll).DYNAMICS_DATA.w=BUBBLE(ll).DYNAMICS_INITIAL_DATA.w0;
        BUBBLE(ll).DYNAMICS_DATA.v_exp=BUBBLE(ll).DYNAMICS_INITIAL_DATA.v_exp0;
        BUBBLE(ll).DYNAMICS_DATA.virt_momentum=[0;0;0];
        BUBBLE(ll).FRAGMENTATION_DATA.failure_ID=1;
        BUBBLE(ll).FRAGMENTATION_DATA.threshold0=40;
        BUBBLE(ll).FRAGMENTATION_DATA.threshold=40;
        BUBBLE(ll).FRAGMENTATION_DATA.breakup_flag=0;
        BUBBLE(ll).FRAGMENTATION_DATA.ME_energy_transfer_coef=0;
        BUBBLE(ll).FRAGMENTATION_DATA.cMLOSS=0;
        BUBBLE(ll).FRAGMENTATION_DATA.cELOSS=0;
        BUBBLE(ll).FRAGMENTATION_DATA.c_EXPL=0;
        BUBBLE(ll).FRAGMENTATION_DATA.seeds_distribution_ID = 0;
        BUBBLE(ll).FRAGMENTATION_DATA.seeds_distribution_param1 = 0;
        BUBBLE(ll).FRAGMENTATION_DATA.seeds_distribution_param2 = 0;
        BUBBLE(ll).FRAGMENTATION_DATA.param_add1=FRAGMENTS_FR(length(FRAGMENTS_FR)).object_ID_index;
        BUBBLE(ll).FRAGMENTATION_DATA.param_add2=FRAGMENTS_FR(length(FRAGMENTS_FR)).FRAGMENTATION_DATA.param_add2;
        FRAGMENTS_FR(length(FRAGMENTS_FR)).GEOMETRY_DATA.mass=0;
        disp(['Created Bubble with FRAGMENTS(',num2str(length(FRAGMENTS_FR)), ').']);
    end
end
end
