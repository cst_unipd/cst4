function Impact_Data = A5_compute_Intersection_Couples_Hollow(Frag_ME,Impact_Data,Frag_Volume)
%A5_compute_Intersection_Couples_Hollow Compute the intersection couples
%for hollow shapes
% 
% Syntax:  Impact_Data = A5_compute_Intersection_Couples_Hollow(Frag_ME,Impact_Data,Frag_Volume)
%
% Inputs:
%    Frag_ME - Frag_ME structure
%    Impact_Data - Impact_Data structure
%    Frag_Volume - Frag_Volume structure
%
% Outputs:
%    Impact_Data - Impact_Data structure updated with intersect_couples
%                  field
%
% Other m-files required: cst_createConicBoundary, getPointsWithinBoundary,
%                         minConvexHull
% Subfunctions: none
% MAT-files required: none
%
% See also:
%
% Author:   Andrea Valmorbida, Ph.D.
%           Center of Studies and Activities for Space "Giuseppe Colombo" (CISAS)
%           University of Padova
% Email address: andrea.valmorbida@unipd.it
% Date: 2017/11/24
% Revision: 1.0
% Copyright: 2017 CISAS - UNIVERSITY OF PADOVA
%
% HISTORY
% 2017/11/24 : first version by AV

%#codegen

global SHAPE_ID_LIST;

% --------------------------------
% compute the intersection couples
intersect_couples = cell(1,Impact_Data.N_frag_vol);

switch Frag_ME.shape_ID
    case SHAPE_ID_LIST.HOLLOW_SPHERE
        h_cone = 2*Frag_ME.a;
    %
    case SHAPE_ID_LIST.HOLLOW_CYLINDER
        h_cone = max(2*Frag_ME.a,Frag_ME.c);
    %}
    case SHAPE_ID_LIST.HOLLOW_ELLIPSOID
        h_cone = 2*max([Frag_ME.a Frag_ME.b Frag_ME.c]);
end

hollow_center = Frag_ME.center;

if Impact_Data.N_impact > 1
    
    for i=1:Impact_Data.N_frag_vol

        if vectorNorm3d(Frag_Volume{i}.half_space_normal)==0
            points_cone1i = cst_createConicBoundary(Frag_Volume{i}.vi_cart,hollow_center,h_cone);
            points_cone1e = cst_createConicBoundary(Frag_Volume{i}.ve_cart,hollow_center,h_cone);
        else
            points_cone1i = Frag_Volume{i}.vi_cart + h_cone*Frag_Volume{i}.half_space_normal;
            points_cone1i = [Frag_Volume{i}.vi_cart;points_cone1i];

            points_cone1e = Frag_Volume{i}.ve_cart + h_cone*Frag_Volume{i}.half_space_normal;
            points_cone1e = [Frag_Volume{i}.ve_cart;points_cone1e];
        end

        for j=i:Impact_Data.N_frag_vol
            %fprintf('\ni=%d - j=%d',i,j);
            if j==i
                intersect_couples{i} = [intersect_couples{i},j];
                %fprintf(' => j==i\n');
            elseif ( isodd(i) && (j==i+1) ) || ( iseven(i) && (j==i-1) )
                % do nothing
                %fprintf(' => do nothing\n');
            else
                %fprintf(' => check intersection');
                pnts2i_in1i = getPointsWithinBoundary(Frag_Volume{j}.vi_cart,points_cone1i);
                pnts2e_in1e = getPointsWithinBoundary(Frag_Volume{j}.ve_cart,points_cone1e);

                if (~isempty(pnts2i_in1i)) || (~isempty(pnts2e_in1e))
                    intersect_couples{i} = [intersect_couples{i},j];
                    %fprintf(' => j is in i');
                else

                    if vectorNorm3d(Frag_Volume{j}.half_space_normal)==0
                        points_cone2i = cst_createConicBoundary(Frag_Volume{j}.vi_cart,hollow_center,h_cone);
                        points_cone2e = cst_createConicBoundary(Frag_Volume{j}.ve_cart,hollow_center,h_cone);
                    else
                        points_cone2i = Frag_Volume{j}.vi_cart + h_cone*Frag_Volume{j}.half_space_normal;
                        points_cone2i = [Frag_Volume{j}.vi_cart;points_cone2i];

                        points_cone2e = Frag_Volume{j}.ve_cart + h_cone*Frag_Volume{j}.half_space_normal;
                        points_cone2e = [Frag_Volume{j}.ve_cart;points_cone2e];
                    end

                    pnts1i_in2i = getPointsWithinBoundary(Frag_Volume{i}.vi_cart,points_cone2i);
                    pnts1e_in2e = getPointsWithinBoundary(Frag_Volume{i}.ve_cart,points_cone2e);

                    if (~isempty(pnts1i_in2i)) || (~isempty(pnts1e_in2e))
                        intersect_couples{i} = [intersect_couples{i},j];
                        %fprintf(' => i is in j');
                    else
                        %fprintf(' => no intersection');
                    end

                end

            end
            %intersect_couples{i}
        end
    end

else
    
    intersect_couples{1} = 1;
    intersect_couples{2} = 2;
    
end

Impact_Data.intersect_couples = intersect_couples;

end

