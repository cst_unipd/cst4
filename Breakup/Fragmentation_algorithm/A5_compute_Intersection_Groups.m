function Impact_Data = A5_compute_Intersection_Groups(Impact_Data,shape_ID)
%A5_compute_Intersection_Groups Compute the intersection groups
% 
% Syntax:  Impact_Data = A5_compute_Intersection_Groups(Impact_Data,shape_ID)
%
% Inputs:
%    Impact_Data - Impact_Data structure
%    shape_ID - shape ID
%
% Outputs:
%    Impact_Data - Impact_Data structure updated with fields N_frag_dom and
%                  intersect_groups
%
% Other m-files required: createSoccerBall_aug, getBoundariesIntersection,
%                         minConvexHull
% Subfunctions: none
% MAT-files required: none
%
% See also:
%
% Author:   Andrea Valmorbida, Ph.D.
%           Center of Studies and Activities for Space "Giuseppe Colombo" (CISAS)
%           University of Padova
% Email address: andrea.valmorbida@unipd.it
% Date: 2017/11/24
% Revision: 1.0
% Copyright: 2017 CISAS - UNIVERSITY OF PADOVA
%
% HISTORY
% 2017/11/24 : first version by AV

%#codegen

% -------------------------------
% compute the intersection groups
if isSolidShape(shape_ID) || Impact_Data.N_impact ~= 1
    intersect_groups = cell(0,0);
    intersect_groups{1} = Impact_Data.intersect_couples{1};
    N_frag_dom = 1;
else
    intersect_groups = cell(0,0);
    intersect_groups{1} = Impact_Data.intersect_couples{1};
    N_frag_dom = 1;
end

if Impact_Data.N_frag_vol > 1
    for i=2:Impact_Data.N_frag_vol 
        in_group_flag = 0;
        for j=1:N_frag_dom
            %fprintf('%d\t%d\t\t%d\n',i,j,N_frag_dom);
            if ( sum(ismember(Impact_Data.intersect_couples{i},intersect_groups{j})) >= 1 )
                intersect_groups{j} = unique([intersect_groups{j},Impact_Data.intersect_couples{i}]);
                in_group_flag = 1;
            end
        end
        if in_group_flag == 0
            N_frag_dom = N_frag_dom + 1;
            intersect_groups{j+1} = Impact_Data.intersect_couples{i};
            %fprintf('\t\tnew group => n_groups = %d\n',N_frag_dom);
        end
    end
    clear in_group_flag;
end

Impact_Data.N_frag_dom = N_frag_dom;
Impact_Data.intersect_groups = intersect_groups;

end
