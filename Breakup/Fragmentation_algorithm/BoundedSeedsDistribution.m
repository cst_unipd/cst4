function [seeds_pos] = BoundedSeedsDistribution(impact_circle,bnd_pnts)

%% Get xc,yc,rc
% impact_circle => [xc yx rc]
xc = impact_circle(1);
yc = impact_circle(2);
rc = impact_circle(3);

% G_DATA = [GEOM,a,b,c,rho];

%% Compute the regular positions of the seeds

[seeds_pos0,dtheta] = LogSpiral2D_SeedsDistribution(xc,yc,rc);
% IMPORTANT => dare in input alla funzione LogSpiral2D_SeedsDistribution
% anche i parametri che definiscono la morfologia della spirale o analoghi


%% Add a random component to the seeds regular positions

d_star = sqrt((seeds_pos0(:,1)-xc).^2+(seeds_pos0(:,2)-yc).^2)*dtheta;

perc_noise = 70;
seeds_pos = seeds_pos0 + ...
            perc_noise/100*d_star/2.*(-1+2*rand(size(seeds_pos0,1),2));


%% Take points that are in the boundary convex polytope

seeds_pos = getPointsWithinBoundary(seeds_pos,bnd_pnts);


end