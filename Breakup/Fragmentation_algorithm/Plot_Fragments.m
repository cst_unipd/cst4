function Plot_Fragments(Frag_ME,Frag_Domain,fragments)

figure
drawMesh(Frag_ME.v,Frag_ME.f,'facecolor','r','facealpha',0.3);
hold on;
pause
drawMesh(Frag_Domain.v,Frag_Domain.f,'facecolor','c','facealpha',0.1);
hold on;
pause
a=['m','b'];
for i=1:fragments.nseeds
    if ~isempty(fragments.vorvx{i})
        plot3(fragments.seeds(:,1),fragments.seeds(:,2),fragments.seeds(:,3),'*b')
        drawMesh(fragments.vorvx{i},fragments.vorfc{i},'facecolor',a(mod(i,2)+1),'facealpha',0.5)
    end
    pause(0.001)
    hold on;
end

for i=1:size(Frag_Data,1)
    hold on;
    plot3(Frag_Data(i,1),Frag_Data(i,2),Frag_Data(i,3),'*k')
    pause
end