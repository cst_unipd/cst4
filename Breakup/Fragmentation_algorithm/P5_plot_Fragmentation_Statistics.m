function P5_plot_Fragmentation_Statistics(Impact_Data,FragMatrix)

    mass_vect = cell(1,Impact_Data.N_frag_dom);
    CN_m = cell(1,Impact_Data.N_frag_dom);

    for k=1:Impact_Data.N_frag_dom

        mass_vect{k} = FragMatrix{k}(1:end,3);
        mass_vect{k} = sort(mass_vect{k});

        ll = length(mass_vect{k});
        CN_m{k} = ll:-1:1; % it corresponds to the cumulative number

        figure();
        loglog(mass_vect{k},CN_m{k},'pr'); hold on;
        grid on;
        xlabel('Fragment Mass [kg]');
        ylabel('Cumulative Number');
        title(['CN vs. m_f - D' num2str(k)]);

    end

end
