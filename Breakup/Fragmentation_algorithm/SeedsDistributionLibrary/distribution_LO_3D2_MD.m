%> @file  distribution_LO_3D.m
%> @brief calculation of seeds 3D spherical standard distributions
%> Copyright (c) 2018 CISAS - UNIVERSITY OF PADOVA - Dr L. Olivieri

function[seeds]=distribution_LO_3D2_MD(R,Rx_neg,Rx_pos,Ry_neg,Ry_pos,R_thick,s_number,k10_count_max,df_min)
%% Input data
% POINT: impact point, 3x1 or 1x3 vector
% R: fragmentation radius
% s_number: number of seeds

%% Output data
% seeds: [s_number x s] coordinates of seeds

debug_mode=1;

gauss_parameter1=1; % part of curve used in the distribution; default == 4
% gauss_parameter2=0.28; % exponent of gaussian curve; default == 2


% input check
% s_number=ceil(s_number/0.1);

%% DISTRIBUTION CALCULATION
% standard distribution, radius
% r=randn(s_number,1).^gauss_parameter2;

%k10=10;
V_R=(4/3*pi*R^3)*0.5;
V_R_thick=V_R-pi*(R-R_thick)^2*(R-(R-R_thick)/3);
k10=max(1,ceil(V_R/V_R_thick));
s_number_exp=ceil(s_number/k10);
% s_number=s_number*k10;
r=abs(randn(s_number,1));
% r=r.^gauss_parameter2;
r=r/max(r);
% r=r.^gauss_parameter2;
% rectangular distribution, angles
% az=2*pi*rand(s_number,1);
% el=pi*(1-2*rand(s_number,1));
az=2*pi*rand(s_number,1);
el=0.5*pi*(rand(s_number,1));
Rx =repmat([Rx_neg,Rx_pos],1,floor(length(az)/2))';
Ry =repmat([Ry_neg,Ry_pos],1,floor(length(az)/2))';
if length(Rx)<length(az)
    Rx=[Rx;Rx_neg];
    Ry=[Ry;Ry_neg];
end
seeds=zeros(s_number,3);
seeds_in=[];

k10_count=0;
while(~(size(seeds_in,1)>=s_number_exp))% || ~(k10_count>=k10_count_max))
    k10_count=k10_count+1;
    seeds_in0(:,1)=gauss_parameter1*Rx.*r.*cos(az).*cos(el);
    seeds_in0(:,2)=gauss_parameter1*Ry.*r.*sin(az).*cos(el);
    seeds_in0(:,3)=gauss_parameter1*R.*r.*sin(el);
    i_R_thick=find(seeds_in0(:,3)<=R_thick);
    i1=size(seeds_in,1);
    i2=length(i_R_thick);
    if(~isempty(i_R_thick))
        seeds_in(i1+1:i1+i2,1:3)=seeds_in0(i_R_thick,1:3);
%         seeds_in=[seeds_in,seeds_in0(i_R_thick,:)];
    end    
%     if(size(seeds_in,1)==0)
%         seeds_in=[seeds_in0(seeds_in0(:,3)<=R_thick,:)];
%     else
%         seeds_in=[seeds_in,seeds_in0(seeds_in0(:,3)<=R_thick,:)];
%     end
%     disp("Size seeds_in partial")
%     size(seeds_in)
%     size(seeds_in,1)
    if(k10_count>=k10_count_max)
        break;
    end
end
if(isempty(seeds_in))
    seeds_in=[0 0 0.5*R_thick];
end
% % seeds_in=seeds_in0;
%     disp("Size seeds_in TOTAL")
% 
% size(seeds_in)
seeds=seeds_in(1,:);
counter=2;

minAllowableDistance=df_min;
for i=2:size(seeds_in,1)%s_number
    seed_act=seeds_in(i,:);
%     distances = distancePoints3d(seed_act,seeds);%sqrt((seed_act(1,1)-seeds(:,1)).^2 + (seed_act(1,2)-seeds(:,2)).^2 + (seed_act(1,3)-seeds(:,3)).^2);
    distances = sqrt((seed_act(1,1)-seeds(:,1)).^2 + (seed_act(1,2)-seeds(:,2)).^2 + (seed_act(1,3)-seeds(:,3)).^2);
    minDistance = min(distances);
    if minDistance >= minAllowableDistance% && length(seeds)<=ceil((s_number/k10))
        seeds(counter,1) = seed_act(1,1);
        seeds(counter,2) = seed_act(1,2);
        seeds(counter,3) = seed_act(1,3);
        counter = counter + 1;
    end
end
%     disp("Size seeds TOTAL")
% 
% size(seeds)

if debug_mode==0
    plot3(seeds(:,1),seeds(:,2),seeds(:,3),'*')
end


% for count=1:s_number
% %     if abs(r(count))<=1/gauss_parameter1
% %         seeds(i,1)=POINT(1)+gauss_parameter1*R*r(count)*cos(az(count))*cos(el(count));
% %         seeds(i,2)=POINT(2)+gauss_parameter1*R*r(count)*sin(az(count))*cos(el(count));
% %         seeds(i,3)=POINT(3)+gauss_parameter1*R*r(count)*sin(el(count));
%
% %           if(seeds(i,3)> R_thick)
% %             seeds(i,3)= seeds(i,3)*R_thick/R;
% %           end
%         i=i+1;
% %     end
% end
%
% if debug_mode==1
%     plot3(seeds(:,1),seeds(:,2),seeds(:,3),'*')
% end
%


end