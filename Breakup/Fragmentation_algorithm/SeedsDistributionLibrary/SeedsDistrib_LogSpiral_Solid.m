function Frag_Volume = SeedsDistrib_LogSpiral_Solid(Frag_ME,Frag_Volume,Frag_Data)
%SeedsDistrib_LogSpiral_Solid Compute the seeds distribution for
%plates for a Logaritmic Spiral Distribution
% 
% Syntax:  Frag_Volume = SeedsDistrib_LogSpiral_Solid(Frag_ME,Frag_Volume,Frag_Data)
%
% Inputs:
%    Frag_ME - Frag_ME structure
%    Frag_Volume - Frag_Volume structure
%    Frag_Data - Impact point and fragmentation volume radius
%
% Outputs:
%    Frag_Volume - Frag_Volume structure with updated field seeds
%
% Other m-files required: getPointsWithinBoundary
% Subfunctions: none
% MAT-files required: unique
%
% See also:
%
% Author:   Andrea Valmorbida, Ph.D.
%           Center of Studies and Activities for Space "Giuseppe Colombo" (CISAS)
%           University of Padova
% Email address: andrea.valmorbida@unipd.it
% Date: 2018/04/11
% Revision: 1.0
% Copyright: 2018 CISAS - UNIVERSITY OF PADOVA
%
% HISTORY
% 2018/04/11 : first version by AV

%#codegen

% global p1 p2 p3 p4

% p1 = 3;
% p2 = 2;
% p3 = 1;
% p4 = 1;

% compute parameters used for the seeds distribution 
% mf_min = 1e-3; % kg - minimum mass of the fragments <<<=== TUNING PARAMETER
%df_min = (6*mf_min/rho/pi)^(1/3); %

% df_min = 0.001;
% k_a = 3;

global spiral_param_vector;

df_min = 0.0005;
k_a = spiral_param_vector(1);

h_ext = Frag_ME.c;
k_tot = ceil(h_ext/df_min) + 1;

% 
for i=1:length(Frag_Volume) % N_impact

    Frag_Volume{i}.seeds = [];
    
    % compute seeds layers
    seeds_layer = cell(1,k_tot);
    for k=1:k_tot
        seeds_layer{k} = GenerateSeedsLayer('irregular',k,k_a,df_min, ...
                                        Frag_Data(i,1),Frag_Data(i,2), ...
                                        Frag_Data(i,4),h_ext);
        seeds_layer{k} = getPointsWithinBoundary(seeds_layer{k},Frag_Volume{i}.v);
    end

    % compute seeds matrix
    seeds = [];
    for k=1:k_tot
        seeds = [seeds; seeds_layer{k}];
    end

    % get the seeds that are within the Frag_Volume
    seeds = getPointsWithinBoundary(seeds,Frag_Volume{i}.v);
    % seeds = unique(seeds,'rows');

    if isempty(seeds)
        [Frag_Volume{i}.seeds, ~] = polyhedronCentroidVolume(Frag_Volume{i}.v);
    else
        Frag_Volume{i}.seeds = unique(seeds,'rows');
        Frag_Volume{i}.seeds_layer = seeds_layer;
    end


end

% disp('ok');

% OUTPUT => Frag_Volume{i}.seeds, for i=1:

end
