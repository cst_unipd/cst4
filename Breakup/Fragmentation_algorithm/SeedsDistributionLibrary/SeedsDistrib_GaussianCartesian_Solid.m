function Frag_Volume = SeedsDistrib_GaussianCartesian_Solid(Frag_Volume,n_seeds_vector,n_max_loop,Frag_Data,Impact_Data,tuning_c_coeff,count,df_min_orig,Frag_ME)
%SeedsDistrib_GaussianCartesian_Solid Compute the seeds distribution for
%solid shapes for a gaussian distribution in cartesian coordinates
%
% Syntax:  Frag_Volume = SeedsDistrib_GaussianCartesian_Solid(Frag_Volume,n_seeds,n_max_loop,Frag_Data)
%
% Inputs:
%    Frag_Volume - Frag_Volume structure
%    n_seeds - maximum number of seeds within the Frag Volume
%    n_max_loop - maximum number of while loops to obtain the required
%                 number of seeds
%    Frag_Data - Impact point and fragmentation volume radius (GS+FF)
%
% Outputs:
%    Frag_Volume - Frag_Volume structure with updated field seeds
%
% Other m-files required: getPointsWithinBoundary
% Subfunctions: none
% MAT-files required: unique, rand
%
% See also:
%
% Author:   Andrea Valmorbida, Ph.D.
%           Center of Studies and Activities for Space "Giuseppe Colombo" (CISAS)
%           University of Padova
% Email address: andrea.valmorbida@unipd.it
% Date: 2017/11/24
% Revision: 1.0
% Copyright: 2017 CISAS - UNIVERSITY OF PADOVA
%
% HISTORY
% 2017/11/24 : first version by AV, FF, GS
% if exist(fullfile(matlabroot,'toolbox','matlab','ops','uniquetol.m'),'file')
%     param_seeds=1.5;
% else
param_seeds=1;
% end
%#codegen
debug_mode=0;
% generate a random distribution of n_seeds seeds
for i=1:length(Frag_Volume) % N_impact
    % Recover n_seeds
    if length(Frag_Volume)==length(n_seeds_vector) % Check if frag volume is duplicated
        n_seeds=n_seeds_vector(i);
    else
        n_seeds=n_seeds_vector(ceil(i/2));
    end

    [zv,alpha_angle,h_ext_P3]=InternalNormals(Impact_Data.I_POS_F(i,:),Impact_Data.I_VEL_F(i,:),Frag_Volume{i},Frag_Data(i,1:4));

    if ~isParallel3d(zv,[1 0 0])
        yv = vectorCross3d(zv,[1 0 0]);
    else
        yv = vectorCross3d(zv,[0 1 0]);
    end
    yv = yv/vectorNorm3d(yv);
    xv = vectorCross3d(yv,zv);
    RVF = [xv',yv',zv'];
    pVF = Impact_Data.I_POS_F(i,:)';
    TVF = trasm(RVF,pVF);
    TFV = inv(TVF);
    
    % Frag_Volume chull in the V RF % sbagliata, frammenti oblunghi? sdr
    % Fragmentation unico per gli impatti?
    data_impacted_face = sortrows(trasfg_vectors(Frag_ME.v',TFV)',3); %!! funziona solo per le piastre
    data_impacted_face(ceil(end/2)+1:end,:) = [];
    Rx_neg = min(abs(data_impacted_face(:,1)));
    Rx_pos = max(abs(data_impacted_face(:,1)));
    Ry_neg = min(abs(data_impacted_face(:,2)));
    Ry_pos = max(abs(data_impacted_face(:,2)));
    %     [~,index] = max([norm([Frag_ME.lx, 0, 0]*zv') norm([Frag_ME.lx, 0, 0]*zv') norm([Frag_ME.lx, 0, 0]*zv')]);
    
    
    %     n_seeds: depending also from volume
    %     [~,volume] = polyhedronCentroidVolume(Frag_Volume{i}.v);
    %
    %
    %     if(volume<pi*2/3*(Frag_Data(i,4))^3)
    %         n_seeds=ceil(n_seeds*(pi*2/3*(Frag_Data(i,4))^3)/volume);
    %     end
    Frag_Volume{i}.seeds = [];
    check_loops = 0;
    
    
    if Frag_Data(i,6)==0
        % spherical crater
        r_ext = Frag_Data(i,4);
        h_ext = Frag_Data(i,4);
%         dim=[Frag_ME.a Frag_ME.b Frag_ME.c];
%         dim(dim(:)==0)=[];
        if h_ext_P3>Frag_Data(i,4)
            h_ext_P3 = Frag_Data(i,4);
        end
        h_ext_eff=min(h_ext_P3,h_ext);
    else
        % ellipsoidal crater
        r_ext = max(Frag_Data(i,4:5))/cos(alpha_angle);
        h_ext = Frag_Data(i,6)*cos(alpha_angle);
        h_ext_eff=min(h_ext_P3,h_ext);
    end
    df_min=df_min_orig*2;
    seeds=[];
    a=0;
    %     while ( check_loops<10*n_max_loop && size(Frag_Volume{i}.seeds,1)< param_seeds*n_seeds )
    while size(seeds,1)<2
        df_min=df_min/2;
        seeds=distribution_LO_3D2_MD(r_ext,Rx_neg,Rx_pos,Ry_neg,Ry_pos,h_ext_eff,n_seeds,n_max_loop,df_min);
        
        %         R= r_ext; R_thick = h_ext_eff; s_number =n_seeds; k10_count_max=n_max_loop;
        
        % get the seeds that are within the Frag_Volume
        % seeds in the F RF
        seeds_F = trasfg_vectors(seeds',TVF)';
        seeds = getPointsWithinBoundary(seeds_F,Frag_Volume{i}.v);
        seeds = unique(seeds,'rows');
        a=a+1;
        if (a>5)
            n_seeds=n_seeds*2;
%             seeds=centroid;
        end
        if (a>10)
            seeds=polyhedronCentroid(Frag_Volume{i}.v,Frag_Volume{i}.f);
            break;
        end
        dbstop at 126 if (a==38)
        if debug_mode==1
            Frag_Volume_i_centros = faceCentroids(Frag_Volume{i}.v, Frag_Volume{i}.f);
            Delta_v = Frag_Volume_i_centros - Impact_Data.I_POS_F(i,:);
            Plot_Fragmentation_Gaussian_Cartesian(i,Frag_Volume,Frag_Data,Impact_Data,zv,seeds_F,Frag_Volume_i_centros,Delta_v(imin,:),imin,Frag_ME)
        end
        
        %         if size(seeds,1)<2 && size(seeds_F,1)>=2
        %             seeds_F(seeds_F(:,1)<0,1)=Frag_Data(i,1)/2;
        %             seeds_F(seeds_F(:,2)<0,2)=Frag_Data(i,2)/2;
        %             seeds_F(seeds_F(:,1)>Frag_ME.lx,1)=(Frag_ME.lx+Frag_Data(i,1))/2;
        %             seeds_F(seeds_F(:,2)>Frag_ME.ly,2)=(Frag_ME.ly+Frag_Data(i,2))/2;
        %         end
        
    end
    
   
    % add the new seeds to Frag_Volume{i}.seeds
    Frag_Volume{i}.seeds = [Frag_Volume{i}.seeds;seeds];
    
    %         %% CHECK for points too close
    %         if exist(fullfile(matlabroot,'toolbox','matlab','ops','uniquetol.m'),'file')
    %             [~, ~, ~, ~, ~, ~, FRAGMENT_radius_th, ~, ~, ~] = Parameters_Setup();
    %             toll=0.5*FRAGMENT_radius_th;
    %             seeds1=uniquetol(Frag_Volume{i}.seeds, toll,'DataScale', 1,'ByRows',true);
    %             if ~isempty(seeds1)
    %                 Frag_Volume{i}.seeds=seeds1;
    %             end
    %         end
    %         %
    %         check_loops = check_loops + 1;
    %     end
    
    if isempty(Frag_Volume{i}.seeds)
        [Frag_Volume{i}.seeds, ~] = polyhedronCentroidVolume(Frag_Volume{i}.v); % GS + FF
    else
        Frag_Volume{i}.seeds = unique(Frag_Volume{i}.seeds,'rows');
    end
    % take n_seeds seeds
    if (size(Frag_Volume{i}.seeds,1)>n_seeds)
        Frag_Volume{i}.seeds = Frag_Volume{i}.seeds(1:n_seeds,:);
    end
    
end

end
