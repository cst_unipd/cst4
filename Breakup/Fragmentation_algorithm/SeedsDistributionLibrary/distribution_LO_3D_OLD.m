%> @file  distribution_LO_3D.m
%> @brief calculation of seeds 3D spherical standard distributions
%> Copyright (c) 2018 CISAS - UNIVERSITY OF PADOVA - Dr L. Olivieri

function[seeds]=distribution_LO_3D(POINT,R,s_number)
%% Input data
% POINT: impact point, 3x1 or 1x3 vector
% R: fragmentation radius 
% s_number: numner of seeds

%% Output data
% seeds: [s_number x s] coordinates of seeds

debug_mode=1;


% input check
s_number=ceil(s_number);

%% DISTRIBUTION CALCULATION
% standard distribution, radius
r0=randn(s_number,1);
r=R*r0/max(r0);
% rectangular distribution, angles
az=2*pi*rand(s_number,1);
el=pi*(1-2*rand(s_number,1));

seeds=zeros(s_number,3);
for i=1:s_number
    seeds(i,1)=POINT(1)+r(i)*cos(az(i))*cos(el(i));
    seeds(i,2)=POINT(2)+r(i)*sin(az(i))*cos(el(i));
    seeds(i,3)=POINT(3)+r(i)*sin(el(i));
end

if debug_mode==1
    plot3(seeds(:,1),seeds(:,2),seeds(:,3),'*')
end

end