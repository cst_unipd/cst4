function [param_vector]= getNishidaSpiralParameters(Dp,vp)
% This function computes the parameters for the logarithmic spiral
% distribution: it checks if it is one of Nishida cases, so to load the
% parameters; if that's not the case, it computes them as a bilinear
% interpolation between the values of Nishida look-up table. If one of 
% the input is equal to one of the look-up table, then the interpolation
% becomes linear in the other input variable.
%
% Input:
%
% Dp            : projectile diameter
% vp            : projectile speed
%
% Output:
%
% [S1,S2,S3,S4] : spiral parameter
%
% See also:
%
% Author:   Giulia Sarego, Ph.D.
%           Department of Industrial Engineering
%           University of Padova
% Email address: giulia.sarego@unipd.it
% Date: 2018/06/12
% Revision: 1.0
% Copyright: 2018 UNIVERSITY OF PADOVA
%

% flag for plotting the points used for the interpolation
plot_flag = 0;

% load correct Nishida results
%                  TN  Dp  Vp   s1       s2       s3       s4    
GA_RESULTS_200 = [  2  3.2 1.91 1.141838 1.552032 1.136991 0.838965; ...
                    3  1.6 1.85 1.755484 1.558192 1.272294 0.827922; ...
                    5  3.2 3.01 1.136746 1.448108 1.029720 0.739014; ...
                    6  1.6 3.09 1.499650 1.056789 1.325117 0.814322; ...
                    7  3.2 4.14 1.455030 1.320553 1.027727 0.649057; ...
                    8  1.6 4.06 1.334137 1.291440 1.253838 0.757464; ...
                    11 1.6 2.50 1.035417 1.211411 1.224158 0.790349; ...
                    12 1.6 3.50 1.080683 1.462822 1.183471 0.801643; ...
                    13 2.4 2.00 1.122807 1.466516 1.282118 0.837401; ...
                    14 2.4 2.50 1.334955 1.495162 1.264006 0.843588; ...
                    15 2.4 3.00 1.032349 1.518564 1.041850 0.800538; ...
                    16 2.4 3.50 1.071921 1.397354 1.028603 0.780449; ...
                    17 2.4 4.00 1.071255 1.483894 1.033837 0.683386; ...
                    18 3.2 2.50 1.000816 1.397753 1.018520 0.811151; ...
                    19 3.2 3.50 1.000000 1.559633 1.019044 0.837899; ...
                    20 1.8 3.20 1.338546 1.509621 1.310013 0.760632; ...
                    21 1.6 3.20 1.043470 1.473874 1.173945 0.824401; ...
                    30 1.0 2.00 1.241215 1.403443 1.035348 0.833029; ...
                    31 1.0 2.50 1.512802 1.514154 1.031558 0.808277; ...
                    32 1.0 3.00 1.393867 1.115476 1.018495 0.812680; ...
                    33 1.0 3.50 1.966905 1.311366 1.036626 0.845000; ...
                    34 1.0 4.00 1.919362 1.368572 1.023687 0.779070; ...
                    35 1.0 4.50 1.825992 1.485581 1.011609 0.694272; ...
                    36 1.0 5.00 1.983308 1.340919 1.015259 0.702671; ...
                    40 0.5 2.00 1.040197 1.535014 1.134521 0.831199; ...
                    42 0.5 3.00 1.747587 1.510892 1.260486 0.826564; ...
                    44 0.5 4.00 1.380771 1.439608 1.031318 0.668357; ...
                    46 0.5 5.00 1.516179 1.541782 1.027189 0.836733; ...
                    56 1.6 5.00 1.964124 1.223084 1.268010 0.810990; ...
                    66 3.2 5.00 1.595000 1.417517 1.023395 0.774642; ...
                    70 4.6 2.00 1.047179 1.475452 1.034442 0.819743; ...
                    72 4.6 3.00 1.510490 1.517449 1.038843 0.788619; ...
                    74 4.6 4.00 1.792437 1.524425 1.027495 0.814143; ...
                    76 4.6 5.00 1.960100 1.000000 1.028625 0.826641; ...
                    80 6.0 2.00 1.143987 1.462176 1.032537 0.837848; ...
                    82 6.0 3.00 1.428884 1.432277 1.022225 0.769118; ...
                    84 6.0 4.00 1.909705 1.424987 1.023603 0.797041; ...
                    86 6.0 5.00 1.780449 1.180395 1.012308 0.764994];
GA_RESULTS = GA_RESULTS_200(:,2:end);

% compute the range
v_max = max(GA_RESULTS(:,2));
v_min = min(GA_RESULTS(:,2));
D_max = max(GA_RESULTS(:,1));
D_min = min(GA_RESULTS(:,1));

% check if it is one of Nishida cases
SS = find(GA_RESULTS(:,1)==Dp & GA_RESULTS(:,2)==vp);

if ~isempty(SS)
    
    S1 = GA_RESULTS(SS(1),3);
    S2 = GA_RESULTS(SS(1),4);
    S3 = GA_RESULTS(SS(1),5);
    S4 = GA_RESULTS(SS(1),6);
    
else
    
    % sort the lookup table in ascending order of the first column (projectile
    % diameter) and use the second to break any ties
    GA1 = sortrows(GA_RESULTS,[1 2]);
    
    if isempty(find((Dp >= D_min) & (Dp <= D_max) & (vp >= v_min) & (vp <= v_max),1))
        warning('The parameters are going to be computed by extrapolation.')
        
        if (Dp < D_min)
            
            if (vp < v_min)
                % testato per domini rettangolari
                p(1,:) = GA1(1,:);
                p(2,:) = GA1(2,:);
                V1 = find(GA1(:,1)>D_min);
                p(3,:) = GA1(V1(1),:);
                p(4,:) = GA1(V1(2),:);
                
            elseif vp > v_max
                % testato per domini rettangolari
                p(1,:) = GA1(find(GA1(:,2)==v_max & GA1(:,1)==D_min)-1,:);
                p(2,:) = GA1(find(GA1(:,2)==v_max & GA1(:,1)==D_min),:);
                V1 = find(GA1(:,1)>D_min);
                p(3,:) = GA1(find(GA1(:,1)==GA1(V1(1),1), 1,'last')-1,:);
                p(4,:) = GA1(find(GA1(:,1)==GA1(V1(1),1), 1,'last'),:);
                
            else % v_min <= vp <= v_max
                
                if any(ismember(vp,GA1(:,2)))
                    % testato per domini rettangolari
                    V1 = find(GA1(:,2)==vp);
                    p(1,:) = GA1(V1(1),:);
                    p(2,:) = GA1(V1(2),:);
                    
                else
                    % testato per domini rettangolari
                    V1 = find(GA1(:,2)<vp & GA1(:,1)==D_min);
                    p(1,:) = GA1(V1(end),:);
                    p(2,:) = GA1(V1(end)+1,:);
                    V2 = find(GA1(:,1)>D_min);
                    p(3,:) = GA1(find(GA1(:,1)==GA1(V2(1),1) & GA1(:,2)<vp, 1,'last'),:);
                    p(4,:) = GA1(find(GA1(:,1)==GA1(V2(1),1) & GA1(:,2)<vp, 1,'last')+1,:);
                    
                end
                
            end
            
        elseif Dp > D_max
            
            if (vp < v_min)
                % testato per domini rettangolari
                V1 = find(GA1(:,1)<D_max);
                p(1,:) = GA1(find(GA1(:,1)==GA1(V1(end),1),1),:);
                p(2,:) = GA1(find(GA1(:,1)==GA1(V1(end),1),1)+1,:);
                V2 = find(GA1(:,1)==D_max);
                p(3,:) = GA1(V2(1),:);
                p(4,:) = GA1(V2(2),:);
                
            elseif vp > v_max
                % testato per domini rettangolari
                V1 = find(GA1(:,2)<v_max & GA1(:,1)<D_max);
                p(1,:) = GA1(V1(end),:);
                p(2,:) = GA1(V1(end)+1,:);                
                p(3,:) = GA1(end-1,:);
                p(4,:) = GA1(end,:);
                
            else % v_min <= vp <= v_max
                
                if any(ismember(vp,GA1(:,2)))
                    % testato per domini rettangolari
                    V1 = find(GA1(:,2)==vp);
                    p(1,:) = GA1(V1(end-1),:);
                    p(2,:) = GA1(V1(end),:);
                    
                else
                    % testato per domini rettangolari
                    V1 = find(GA1(:,1)<D_max);
                    p(1,:) = GA1(find(GA1(:,1)==GA1(V1(end),1) & GA1(:,2)<vp, 1,'last'),:);
                    p(2,:) = GA1(find(GA1(:,1)==GA1(V1(end),1) & GA1(:,2)<vp, 1,'last')+1,:);
                    V2 = find(GA1(:,2)<vp & GA1(:,1)==D_max);
                    p(3,:) = GA1(V2(end),:);
                    p(4,:) = GA1(V2(end)+1,:);
                
                end
                
            end
            
        else % D_min <= Dp <= D_max
            
            if (vp < v_min)
                
                if any(ismember(Dp,GA1(:,1)))
                    % testato per domini rettangolari
                    V1 = find(GA1(:,1)==Dp);
                    p(1,:) = GA1(V1(1),:);
                    p(2,:) = GA1(V1(2),:);
                    
                else
                    % testato per domini rettangolari
                    V1 = find(GA1(:,1)<Dp);
                    p(1,:) = GA1(find(GA1(:,1)==GA1(V1(end),1),1),:);
                    p(2,:) = GA1(find(GA1(:,1)==GA1(V1(end),1),1)+1,:);
                    V2 = find(GA1(:,1)>Dp);
                    p(3,:) = GA1(V2(1),:);
                    p(4,:) = GA1(V2(2),:);
                
                end
                
            elseif vp > v_max
                
                if any(ismember(Dp,GA1(:,1)))
                    % testato per domini rettangolari
                    V1 = find(GA1(:,1)==Dp);
                    p(1,:) = GA1(V1(end-1),:);
                    p(2,:) = GA1(V1(end),:);
                                        
                else
                    % testato per domini rettangolari
                    V1 = find(GA1(:,1)<Dp);
                    p(1,:) = GA1(V1(end-1),:);
                    p(2,:) = GA1(V1(end),:);
                    V2 = find(GA1(:,1)>Dp);
                    p(3,:) = GA1(find(GA1(:,1)==GA1(V2(1),1),1,'last')-1,:);
                    p(4,:) = GA1(find(GA1(:,1)==GA1(V2(1),1),1,'last'),:);
                
                end
                
            end
            
        end
        
    else
        % testato per domini rettangolari
        I = find((GA1(:,1))<Dp & (GA1(:,2)<vp));
        p(1,:) = GA1(max(I),:);
        p(2,:) = GA1(max(I)+1,:);
        II = find((GA1(:,1))>Dp & (GA1(:,2)>vp));
        p(3,:) = GA1(min(II),:);
        p(4,:) = GA1(min(II)-1,:);
        
    end
    
    if size(p,1) == 4 % surface interpolation (4 points)
        
        S1_fun = fit([p(:,1),p(:,2)],p(:,3),'poly11');
        S1 = feval(S1_fun,[Dp vp]);
        S2_fun = fit([p(:,1),p(:,2)],p(:,4),'poly11');
        S2 = feval(S2_fun,[Dp vp]);
        S3_fun = fit([p(:,1),p(:,2)],p(:,5),'poly11');
        S3 = feval(S3_fun,[Dp vp]);
        S4_fun = fit([p(:,1),p(:,2)],p(:,6),'poly11');
        S4 = feval(S4_fun,[Dp vp]);
        
    elseif length(unique(p(:,1))) < length(unique(p(:,2))) % line interpolation (2 points at different vp)
        
        S1_fun = polyfit(p(:,2),p(:,3),1);
        S1 = polyval(S1_fun,vp);
        S2_fun = polyfit(p(:,2),p(:,4),1);
        S2 = polyval(S2_fun,vp);
        S3_fun = polyfit(p(:,2),p(:,5),1);
        S3 = polyval(S3_fun,vp);
        S4_fun = polyfit(p(:,2),p(:,6),1);
        S4 = polyval(S4_fun,vp);
        
    elseif length(unique(p(:,1))) > length(unique(p(:,2))) % line interpolation (2 points at different Dp)
        
        S1_fun = polyfit(p(:,1),p(:,3),1);
        S1 = polyval(S1_fun,Dp);
        S2_fun = polyfit(p(:,1),p(:,4),1);
        S2 = polyval(S2_fun,Dp);
        S3_fun = polyfit(p(:,1),p(:,5),1);
        S3 = polyval(S3_fun,Dp);
        S4_fun = polyfit(p(:,1),p(:,6),1);
        S4 = polyval(S4_fun,Dp);
        
    else
        
        error('Case not recognized!')
        
    end
        
    if plot_flag == 1
        
        plot(GA1(:,1),GA1(:,2),'ob','MarkerSize',10)
        hold on
        plot(p(:,1),p(:,2),'or','MarkerSize',10)
        hold on
        plot(Dp,vp,'*k','MarkerSize',10)
        
    end
	
	param_vector = [S1,S2,S3,S4];
    
end
