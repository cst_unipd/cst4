function Frag_Volume = SeedsDistrib_RandomCartesian_Solid(Frag_Volume,n_seeds,n_max_loop,Frag_Data,Impact_Data,df_min,Frag_ME)
%SeedsDistrib_RandomCartesian_Solid Compute the seeds distribution for
%solid shapes for a random distribution in cartesian coordinates
%
% Syntax:  Frag_Volume = SeedsDistrib_RandomCartesian_Solid(Frag_Volume,n_seeds,n_max_loop)
%
% Inputs:
%    Frag_Volume - Frag_Volume structure
%    n_seeds - maximum number of seeds within the Frag Volume
%    n_max_loop - maximum number of while loops to obtain the required
%                 number of seeds
%    df_min - minimum allowable distance between seeds
%
% Outputs:
%    Frag_Volume - Frag_Volume structure with updated field seeds
%
% Other m-files required: getPointsWithinBoundary
% Subfunctions: none
% MAT-files required: unique, rand
%
% See also:
%
% Author:   Andrea Valmorbida, Ph.D.
%           Center of Studies and Activities for Space "Giuseppe Colombo" (CISAS)
%           University of Padova
% Email address: andrea.valmorbida@unipd.it
% Date: 2017/11/24
% Revision: 1.0
% Copyright: 2017 CISAS - UNIVERSITY OF PADOVA
%
% HISTORY
% 2017/11/24 : first version by AV

%%#codegen

% generate a random distribution of n_seeds seeds
for i=1:length(Frag_Volume) % N_impact
    
    Frag_Volume{i}.seeds = [];
    check_loops = 0;
    
    [zv,alpha_angle,h_ext_P3]=InternalNormals(Impact_Data.I_POS_F(i,:),Impact_Data.I_VEL_F(i,:),Frag_Volume{i},Frag_Data(i,1:4));

%     if ~isParallel3d(zv,[1 0 0])
%         yv = vectorCross3d(zv,[1 0 0]);
%     else
%         yv = vectorCross3d(zv,[0 1 0]);
%     end
%     yv = yv/vectorNorm3d(yv);
%     xv = vectorCross3d(yv,zv);
%     RVF = [xv',yv',zv'];
%     pVF = Impact_Data.I_POS_F(i,:)';
%     TVF = trasm(RVF,pVF);
%     TFV = inv(TVF);

    if Frag_Data(i,6)==0
        % spherical crater
        %         r_ext = Frag_Data(i,4);
        h_ext = Frag_Data(i,4);
        h_ext_eff=min(h_ext_P3,h_ext);
    else
        % ellipsoidal crater
        %         r_ext = max(Frag_Data(i,4:5))/cos(alpha_angle);
        h_ext = Frag_Data(i,6)*cos(alpha_angle);
        h_ext_eff=min(h_ext_P3,h_ext);
    end
    
    while ( check_loops<n_max_loop && size(Frag_Volume{i}.seeds,1)< n_seeds )
        
        % generate n_seeds random
        ax = min(Frag_Volume{i}.v(:,1)); bx = max(Frag_Volume{i}.v(:,1));
        ay = min(Frag_Volume{i}.v(:,2)); by = max(Frag_Volume{i}.v(:,2));
        az = min(Frag_Volume{i}.v(:,3)); bz = max(Frag_Volume{i}.v(:,3));
        seeds_in0 = [ax+(bx-ax)*rand(n_seeds,1), ...
            ay+(by-ay)*rand(n_seeds,1), ...
            az+(bz-az)*rand(n_seeds,1)];
        
        %         seeds_in=seeds_in0(seeds_in0(:,3)<=h_ext_eff,:);
        seeds_in1=abs(seeds_in0)*zv';
        seeds_in=seeds_in0(seeds_in1<=h_ext_eff,:);
        counter=2;
        seeds=[];
        if ~isempty(seeds_in)
            % get the seeds that are within the Frag_Volume
            seeds_in = getPointsWithinBoundary(seeds_in,Frag_Volume{i}.v);
            seeds_in = unique(seeds_in,'rows');
%             seeds=[];
            if check_loops == 0 && ~isempty(seeds_in)
                Frag_Volume{i}.seeds(1,1:3) = seeds_in(1,1:3);
            end
            if ~isempty(Frag_Volume{i}.seeds)
                for kk=2:size(seeds_in,1)
                    seed_act=seeds_in(kk,:);
                    distances = sqrt((seed_act(1,1)-Frag_Volume{i}.seeds(:,1)).^2 + (seed_act(1,2)-Frag_Volume{i}.seeds(:,2)).^2 + (seed_act(1,3)-Frag_Volume{i}.seeds(:,3)).^2);
                    minDistance = min(distances);
                    if minDistance >= df_min
                        seeds(counter,1) = seed_act(1,1);
                        seeds(counter,2) = seed_act(1,2);
                        seeds(counter,3) = seed_act(1,3);
                        counter = counter + 1;
                    end
                end
            end
            
        end
        % add the new seeds to Frag_Volume{i}.seeds
        Frag_Volume{i}.seeds = [Frag_Volume{i}.seeds;seeds];
        check_loops = check_loops + 1;
    end
    
    if isempty(Frag_Volume{i}.seeds)
        [Frag_Volume{i}.seeds, ~] = polyhedronCentroidVolume(Frag_Volume{i}.v); % GS + FF
    end
    
    % take n_seeds seeds
    if (size(Frag_Volume{i}.seeds,1)>n_seeds)
        Frag_Volume{i}.seeds = Frag_Volume{i}.seeds(1:n_seeds,:);
    end
    
end

end
