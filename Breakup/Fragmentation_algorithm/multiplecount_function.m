%> @file multiplecount_function.m
%> @brief creates the multiplecount matrices for the implementation of the 
%> @law to decrease seed generation number due to subsequent impacts
%> @of fragments originated from the same couple of MEs.
%> @author Dr. G. Sarego (giulia.sarego@unipd.it), Dr. M. Duzzi (matteo.duzzi@phd.unipd.it)
%> @ & Dr. C. Giacomuzzo (cinzia.giacomuzzo@unipd.it)
%> Copyright (c) 2018 CISAS - UNIVERSITY OF PADOVA
%>======================================================================

function [target_counter, multiplecount] = multiplecount_function(COLLISION_DATA,multiplecount)

% function which determines how many times objects from the same couple of
% MEs have impacted
%
% Syntax: multiplecount_function(COLLISION_DATA,multiplecount)
%
% Inputs: 
%           COLLISION_DATA:   COLLISION_DATA structure
%           multiplecount:    nx3 matrix containing the indices of impacting
%                             couples of MEs and the numebr of times they
%                             have impacted 
%                             multiplecount=[index_ME1 index_ME2 n_impacts],
%                             where index_ME1<index_ME2
%
% Outputs: target_counter:    index of the target
%          multiplecount:     updated nx3 matrix containing the indices of 
%                             impacting couples of MEs and the numebr of 
%                             times they have impacted 
%                             multiplecount=[index_ME1 index_ME2 n_impacts],
%                             where index_ME1<index_ME2
%    
% Other m-files required: none
%                         
% Subfunctions: length, unique, sort, size, find, isempty, clear
% 
% MAT-files required: none
%
% See also:
%
% Authors:   
%
%            Giulia Sarego, PhD
%            Department of Industrial Engineering (DII)
%            University of Padova
%
%            Matteo Duzzi, PhD
%            Center of Studies and Activities for Space "Giuseppe Colombo" (CISAS)
%            University of Padova
%
%            Cinzia Giacomuzzo, PhD
%            Department of Industrial Engineering (DII)
%            University of Padova
%
% Email address: giulia.sarego@unipd.it, matteo.duzzi@phd.unipd.it, 
%                cinzia.giacomuzzo@unipd.it
% Date: 2018/10/12
% Revision: 1.0
% Copyright: 2018 CISAS - UNIVERSITY OF PADOVA
%
% HISTORY
% 2018/10/12 : first version by GS, MD, CG

%#codegen

global ME
global FRAGMENTS
global restart_flag restart_mat_name

% checking if it is a restart
if restart_flag == 1 && isempty(multiplecount)
    main_loop_count = str2double(restart_mat_name(strfind(restart_mat_name,'step_')+5:end-4))+1;    % count for the main while loop
    if main_loop_count>1 
        [~] = load([restart_mat_name(1:strfind(restart_mat_name,'step_')+4),num2str(main_loop_count-2),'.mat'],'multiplecount');
    else % if I'm starting from step_0; working also for step_1?
        multiplecount=[];
    end
end
    
if isempty(multiplecount) % creating the first couple of impacting MEs or
                          % impacting fragments
    
    % considering storing the target index in the first column
    if COLLISION_DATA.TARGET.type == 1  % saving in the first column
                                        % the original ME.object_ID if
                                        % target is a ME
        multiplecount(1,1)=ME(COLLISION_DATA.TARGET.ID).object_ID;
    elseif COLLISION_DATA.TARGET.type == 0  % saving in the first column
                                            % the original ME.object_ID if
                                            % target is a FRAGMENT
        multiplecount(1,1)=FRAGMENTS(COLLISION_DATA.TARGET.ID).object_ID;
    else % bubble should not fragment
        error('Bubble fragmenting!')
    end
    target_counter = multiplecount(1,1);
    
    
    % considering storing the first impactor index in the second column
    obj_ID=[];
    for i=1:length(COLLISION_DATA.IMPACTOR)
        if COLLISION_DATA.IMPACTOR(i).type == 1 % if impactor is ME
            obj_ID=[obj_ID; ME(COLLISION_DATA.IMPACTOR(i).ID).object_ID];
        elseif COLLISION_DATA.IMPACTOR(i).type == 0 % if impactor is FRAGMENT
            obj_ID=[obj_ID; FRAGMENTS(COLLISION_DATA.IMPACTOR(i).ID).object_ID];
        else % bubble should not fragment
            error('Bubble fragmenting!')
        end
    end
    obj_ID2 =  unique(obj_ID);
    multiplecount(1,2) = obj_ID2(1); % considering the first impactor index 
    multiplecount(1,1:2) = sort(multiplecount(1,1:2)); % sort to have in the 
                                                       % first column the ME-grandpa 
                                                       % with the lower index
    multiplecount(1,3) = 1; % first impact of the first couple
    
    
    % storing the subsequent rows of the matrix (in case of multiple impacts at the
    % first toi of the simulation)
    for j = 2 : length(obj_ID2)
        multiplecount(j,1) =  target_counter;
        multiplecount(j,2) = obj_ID2(j);
        multiplecount(j,1:2) = sort(multiplecount(j,1:2));
        multiplecount(j,3) = 1;
    end
    clear obj_ID2 obj_ID i j
    
else % if it is not first impact, multiplecount is not empty, 
     % we have to compare with the existing couple
    
    add_count=[];   % temporary storing variable (to be compared with multiplecount)
                    % the algorithm is equivalent to the previous part of
                    % the if-condition
    if COLLISION_DATA.TARGET.type == 1  % saving in the first column
                                        % the original ME.object_ID if
                                        % target is a ME
        add_count(1,1)=ME(COLLISION_DATA.TARGET.ID).object_ID;
    elseif COLLISION_DATA.TARGET.type == 0 	% saving in the first column
                                            % the original ME.object_ID if
                                            % target is a FRAGMENT
        add_count(1,1)=FRAGMENTS(COLLISION_DATA.TARGET.ID).object_ID;
    else % bubble should not fragment
        error('Bubble fragmenting!')
    end
    target_counter = add_count(1,1);
    
    % considering storing the impactor indices in obj_ID
    obj_ID=[];
    for i=1:length(COLLISION_DATA.IMPACTOR)
        if COLLISION_DATA.IMPACTOR(i).type == 1 % if impactor is ME
            obj_ID=[obj_ID; ME(COLLISION_DATA.IMPACTOR(i).ID).object_ID];
        elseif COLLISION_DATA.IMPACTOR(i).type == 0 % if impactor is FRAGMENT
            obj_ID=[obj_ID; FRAGMENTS(COLLISION_DATA.IMPACTOR(i).ID).object_ID];
        else % bubble should not fragment
            error('Bubble fragmenting!')
        end
    end
    obj_ID2 =  unique(obj_ID);
    add_count(1,2) = obj_ID2(1); % considering the first impactor index 
    add_count(1,1:2) = sort(add_count(1,1:2)); % sort to have in the 
                                                       % first column the ME-grandpa 
                                                       % with the lower index
    add_count(1,3) = 1;
    
    % storing the subsequent rows of the matrix in the temporary variable
    for j = 2 : length(obj_ID2)
        add_count(j,1) =  target_counter;
        add_count(j,2) = obj_ID2(j);
        add_count(j,1:2) = sort(add_count(j,1:2));
        add_count(j,3) = 1;
    end
    clear obj_ID2 obj_ID i j
    
    % updating multiplecount matrix
    for i = 1: size(add_count,1)
        [jjj, ~] = find(multiplecount(:,1) ==  add_count(i,1));
        [jj, ~] = find(multiplecount(:,2) == add_count(i,2));
        j = intersect(jj,jjj);
        if ~isempty(j)  % updating the number of impacts if couple of MEs 
                        % already present in multiplecount
            multiplecount(j,3) = multiplecount(j,3) + add_count(i,3);
        else % new couple to add to multiplecount
            multiplecount = [multiplecount; add_count(i,:)];
        end
    end
    clear add_count i j
    
end

end