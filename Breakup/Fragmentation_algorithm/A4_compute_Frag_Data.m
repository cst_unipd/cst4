function Frag_Data = A4_compute_Frag_Data(COLLISION_DATA,Frag_ME,Impact_Data,c_0)
%A4_compute_Frag_Data Compute the Frag_Data structure
%
% Syntax:  Frag_Data = A4_compute_Frag_Data(COLLISION_DATA,Frag_ME,Impact_Data,c_0)
%
% Inputs:
%    COLLISION_DATA - COLLISION_DATA(count)
%    Frag_ME - Frag_ME structure
%    Impact_Data - Impact_Data structure
%    c_0 - c_0 tuning coefficient
%
% Outputs:
%    Frag_Data - Frag_Data structure
%
% Other m-files required: none
% Subfunctions: none
% MAT-files required: none
%
% See also:
%
% Author:   Andrea Valmorbida, Ph.D.
%           Center of Studies and Activities for Space "Giuseppe Colombo" (CISAS)
%           University of Padova
% Email address: andrea.valmorbida@unipd.it
% Date: 2017/11/24
% Revision: 1.0
% Copyright: 2017 CISAS - UNIVERSITY OF PADOVA
%
% HISTORY
% 2017/11/24 : first version by AV

%#codegen

% -------------------------------------------------------------------
% Compute Frag_Data
% each row of Frag_Data defines a Fragmentation Volume (FV), which is
% an (approximated) sphere with center coordinates xc,yc,zc and radius rc
% row i-th => [xc,yc,zc,rc] for each FV

% global SHAPE_ID_LIST;
global CRATER_SHAPE_LIST;
global crater_shape_ID;

global ME_FR;
global FRAGMENTS_FR;
global MATERIAL_LIST;

% Frag_Data = zeros(Impact_Data.N_impact,4);
% 1  2  3  4  5  6  7  8  9  10 11
% cx cy cz rx ry rz q0 q1 q2 q3 ID
Frag_Data = zeros(Impact_Data.N_impact,11);

% volume of the target
vol_target = Frag_ME.vol;

% if frag_vol_shape_ID == 2 % ==>> SOLID SPHERE
if crater_shape_ID == CRATER_SHAPE_LIST.SPHERE % ==>> SOLID SPHERE
    
    for i=1:Impact_Data.N_impact
        % impact point
        Frag_Data(i,1:3) = Impact_Data.I_POS_F(i,:);
        
        Frag_Data_i_4_coeff=1.0;
        Frag_Data_i_4_coeff_corr=1;
        kr=1;
        if(COLLISION_DATA.F_L==1)
            %             Frag_Data_i_4_coeff=1.5;
            Frag_Data_i_4_coeff=1;% CG 25-10-18 %1.05; %CG 24-07-18
            Frag_Data_i_4_coeff_corr=1.0;
        end
        
        
        % correzione AF per thin plate
        if COLLISION_DATA.IMPACTOR(i).type==1
            MAT_ID_I=ME_FR(COLLISION_DATA.IMPACTOR(i).ID).material_ID;
        elseif COLLISION_DATA.IMPACTOR(i).type==0
            MAT_ID_I=FRAGMENTS_FR(COLLISION_DATA.IMPACTOR(i).ID).material_ID;
        end
        
        VTINT=COLLISION_DATA.IMPACTOR(i).V_tar*vol_target;
        vol_imp=COLLISION_DATA.IMPACTOR(i).mass/MATERIAL_LIST(MAT_ID_I).density;
        VPINT=COLLISION_DATA.IMPACTOR(i).V_imp*vol_imp;
        % t_D = COLLISION_DATA.IMPACTOR(i).t_EQ/(VPINT/COLLISION_DATA.IMPACTOR(i).A_inter*3/2);
        t_D = COLLISION_DATA.IMPACTOR(i).t_EQ/(COLLISION_DATA.IMPACTOR(i).dp_EQ);
        

        if t_D<1
            kr= (  ( VTINT/VPINT+VTINT/(COLLISION_DATA.F_L*vol_target) ) / (VTINT/VPINT+1)    )^(1/3);
        end
        if(COLLISION_DATA.F_L==1)%GC 05-12-18
            kr=1;%GC 03-12-18
        end
        %AV 26-07-18 - we need radius here
        Frag_Data(i,4) = (Frag_Data_i_4_coeff*COLLISION_DATA.F_L*(1+c_0*COLLISION_DATA.TARGET.c_EXPL)*...
            ((COLLISION_DATA.IMPACTOR(i).Ek+COLLISION_DATA.TARGET.Ek)/COLLISION_DATA.Ek_TOT)* ...
            (vol_target)/(pi*2/3))^(1/3);%!!!!!
        Frag_Data(i,4) = Frag_Data(i,4)*Frag_Data_i_4_coeff_corr*kr;

        [box_v, ~, box_f] = createBox(0,0,0,Frag_ME.lx,Frag_ME.ly,Frag_ME.lz);
        if(COLLISION_DATA.F_L==1)%  && i==1)
            bb_diag=[];
            for j=1:size(box_v,1)
                bb_diag = [bb_diag, distancePoints3d(box_v(j,:),Frag_Data(i,1:3))];
            end
            Frag_Data(i,4)=max([bb_diag])*110/100.6;
        else
            dist_j=[];
            line_v=createLine3d(Frag_Data(i,1:3),Impact_Data.I_VEL_F(i,:));
            for j=1:size(box_f,1)
                poly_bb = box_v(box_f(j,:),:);
                itsc_pnts = intersectLinePolygon3d(line_v, poly_bb);
                dist = distancePoints3d(itsc_pnts,Frag_Data(i,1:3));
                dist_j=[dist_j,dist];
            end
            [min_dist_j,j_min_dist_j]=min(dist_j);
            true_lylz=(isequal(box_v(box_f(j_min_dist_j,:),1),box_v(box_f(j_min_dist_j,1),1)*ones(size(box_v(box_f(j_min_dist_j,:),1)))));
            true_lxlz=(isequal(box_v(box_f(j_min_dist_j,:),2),box_v(box_f(j_min_dist_j,1),2)*ones(size(box_v(box_f(j_min_dist_j,:),2)))));
            true_lxly=(isequal(box_v(box_f(j_min_dist_j,:),3),box_v(box_f(j_min_dist_j,1),3)*ones(size(box_v(box_f(j_min_dist_j,:),3)))));
            
            m1=Frag_Data(i,1:3)+[Frag_Data(i,4),Frag_Data(i,4),Frag_Data(i,4)]-0.5*[Frag_ME.lx,Frag_ME.ly,Frag_ME.lz];
            m2=Frag_Data(i,1:3)-[Frag_Data(i,4),Frag_Data(i,4),Frag_Data(i,4)]-0.5*[Frag_ME.lx,Frag_ME.ly,Frag_ME.lz];
            lx_m1=0.5*Frag_ME.lx-abs(m1(1));
            ly_m1=0.5*Frag_ME.ly-abs(m1(2));
            lz_m1=0.5*Frag_ME.lz-abs(m1(3));
            lx_m2=0.5*Frag_ME.lx-abs(m2(1));
            ly_m2=0.5*Frag_ME.ly-abs(m2(2));
            lz_m2=0.5*Frag_ME.lz-abs(m2(3));
            
            array_m1m2=[lx_m1*(1-true_lylz),ly_m1*(1-true_lxlz),lz_m1*(1-true_lxly),lx_m2*(1-true_lylz),ly_m2*(1-true_lxlz),lz_m2*(1-true_lxly)];
            min_m1_m2=min(array_m1m2);
            
            if(min_m1_m2<0)
                Frag_Data(i,4) = Frag_Data(i,4) + abs(min_m1_m2);
            end
            
            %         if(COLLISION_DATA.F_L==1)
            %             i_array_mim2_pos=find(array_m1m2>0);
            %             if(~isempty(i_array_mim2_pos))
            %                 Frag_Data(i,4) = Frag_Data(i,4) + abs(max(array_m1m2(i_array_mim2_pos)));
            %             end
            %             array_thick=[Frag_ME.lx*(true_lylz),Frag_ME.ly*(true_lxlz),Frag_ME.lz*(true_lxly)];
            %             if(Frag_Data(i,4)<array_thick(array_thick>0))
            %                 Frag_Data(i,4)=array_thick(array_thick>0);
            %             end
        end
        
        Frag_Data(i,11) = crater_shape_ID;
        
    end
    
else % ===>>> SOLID ELLIPSOID
    for i=1:Impact_Data.N_impact
        
        % impact point
        Frag_Data(i,1:3) = Impact_Data.I_POS_F(i,:);
        
        % axes of the ellipsoid
        Frag_Data_i_4_coeff=1.0;
        if(COLLISION_DATA.F_L==1)
            %             Frag_Data_i_4_coeff=1.5;
            Frag_Data_i_4_coeff=1.05;%CG 24-07-18
        end
        
        % AV 26-07-18 - we need radius here
        radius_sph = (Frag_Data_i_4_coeff*COLLISION_DATA.F_L*(1+c_0*COLLISION_DATA.TARGET.c_EXPL)*...
            ((COLLISION_DATA.IMPACTOR(i).Ek+COLLISION_DATA.TARGET.Ek)/COLLISION_DATA.Ek_TOT)* ...
            (vol_target)/(pi*2/3))^(1/3);
        
        vol_sph = 4/3*pi*radius_sph^3;
        
        % frame of reference with z-axis along the velocity direction
        z_axis = Impact_Data.I_VEL_F(i,:)/vectorNorm3d(Impact_Data.I_VEL_F(i,:));
        if ~isParallel3d(z_axis,[0 0 1])
            x_axis = vectorCross3d([0 0 1],z_axis);
        else
            x_axis = vectorCross3d([1 0 0],z_axis);
        end
        x_axis = x_axis/vectorNorm3d(x_axis);
        y_axis = vectorCross3d(z_axis,x_axis);
        RBF = [x_axis',y_axis',z_axis'];
        quat_BF = quatr(RBF);
        TBF = trasm(RBF,Impact_Data.I_POS_F(i,:)');
        TFB = inv(TBF);
        
        % get the convex hull of the impactor in the Fragmentation FR
        switch COLLISION_DATA.IMPACTOR(i).type
            case 0 % FRAGMENT
                imp_c_hull_G = FRAGMENTS_FR(COLLISION_DATA.IMPACTOR(i).ID).GEOMETRY_DATA.c_hull;
            case 1 % ME
                imp_c_hull_G = ME_FR(COLLISION_DATA.IMPACTOR(i).ID).GEOMETRY_DATA.c_hull;
        end
        imp_c_hull_F = trasfg_vectors(imp_c_hull_G',Frag_ME.TGF)';
        imp_c_hull_B = trasfg_vectors(imp_c_hull_F',TFB)';
        
        %[XMIN XMAX YMIN YMAX ZMIN ZMAX]
        box = boundingBox3d(imp_c_hull_B);
        lx = abs(box(2)-box(1));
        ly = abs(box(4)-box(3));
        lz = abs(box(6)-box(5));
        rho_x = lx/lz; rho_y = ly/lz;
        rz = (3*vol_sph/(4*pi*rho_x*rho_y))^(1/3);
        rx = rho_x*rz; ry = rho_y*rz;
        
        %vol_elp = 4/3*pi*rx*ry*rz;
        
        Frag_Data(i,4) = rx;
        Frag_Data(i,5) = ry;
        Frag_Data(i,6) = rz;
        
        % update Frag_Data(i,7:10) = [q0 q1 q2 q3]
        Frag_Data(i,7:10) = quat_BF';
        
        % frag volume id
        Frag_Data(i,11) = crater_shape_ID;
        
    end
end

end

