function P3_plot_Seeds_Distribution(Frag_ME,Impact_Data,Frag_Volume,Frag_Domain)

%%
    figure()
    set(gcf,'color','white');
    hold on; axis equal; axis off;
    view(30,40);

    % plot Frag RF
%     pltassi(trasm(eye(3),[0 0 0]'),'b','',1,0.5);

    % plot impact point
    drawPoint3d(Impact_Data.I_POS_F,'pm');
    for i=1:Impact_Data.N_impact
        text(Impact_Data.I_POS_F(i,1)+0.1,...
             Impact_Data.I_POS_F(i,2)-0.1,...
             Impact_Data.I_POS_F(i,3),num2str(i),'color','k','FontSize',20);
    end

    % plot the Impacted ME
    if isSolidShape(Frag_ME.shape_ID)
        drawMesh(Frag_ME.v, Frag_ME.f, 'FaceColor',[0 0 0],'FaceAlpha',0.002);
    else
        drawMesh(Frag_ME.vi_cart,Frag_ME.fi_cart, 'FaceColor',[0 0 0],'FaceAlpha',0.2,'EdgeAlpha',0.1);
        drawMesh(Frag_ME.ve_cart,Frag_ME.fe_cart, 'FaceColor',[0 0 1],'FaceAlpha',0.05,'EdgeAlpha',0.1);
%         drawMesh(Frag_ME.vi_cart_ss,Frag_ME.fi_cart_ss, 'FaceColor',[0 0 0],'FaceAlpha',0.2,'EdgeAlpha',0.1);
%         drawMesh(Frag_ME.ve_cart_ss,Frag_ME.fe_cart_ss, 'FaceColor',[0 0 1],'FaceAlpha',0.05,'EdgeAlpha',0.1);
    end


    % plot the Fragmentation Volumes
    for i=1:Impact_Data.N_frag_vol
        if isSolidShape(Frag_ME.shape_ID)
            drawMesh(Frag_Volume{i}.v, Frag_Volume{i}.f, 'FaceColor', 'g','FaceAlpha',0.001);
            plot3(Frag_Volume{i}.seeds(:,1), ...
                  Frag_Volume{i}.seeds(:,2), ...
                  Frag_Volume{i}.seeds(:,3),'+');
        else
            drawPoint3d(Frag_Volume{i}.v_cart,'o');
%             v_cart_ss = transfPointsCyl2SphHollow(Frag_Volume{i}.v_cart,Frag_ME.cylinder_e,Frag_ME.cylinder_i,Frag_ME.sphere_e,Frag_ME.sphere_i);
%             drawPoint3d(v_cart_ss,'o');
            drawPoint3d(Frag_Volume{i}.seeds_cart,'+');
%             drawPoint3d(Frag_Volume{i}.seeds_cart_ss,'+');
        end
    end

    % plot the Fragmentation Domains
    %
    if isSolidShape(Frag_ME.shape_ID)
        for i=1:Impact_Data.N_frag_dom
             drawMesh(Frag_Domain{i}.v, Frag_Domain{i}.f, 'FaceColor', 'r','FaceAlpha',0.1);
             drawVector3d(Frag_Domain{i}.centros, Frag_Domain{i}.normals);
        end
    end
    %}

    %{
    % select Frag_Domain idx
    i=1;

    % select layer indexes
    k_plot = [1 2 3];
    % k_plot = 1:length(Frag_Domain{i}.seeds_layer);

    for j=1:length(k_plot)
        k = k_plot(j);
        plot3(Frag_Domain{i}.seeds_layer{k}(:,1), ...
              Frag_Domain{i}.seeds_layer{k}(:,2), ...
              Frag_Domain{i}.seeds_layer{k}(:,3),'-o');
    end

    drawMesh(Frag_Domain{i}.v, Frag_Domain{i}.f, 'FaceColor', 'k','FaceAlpha',0.005);
    %}


end