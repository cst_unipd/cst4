function R=rot_tr(a,b,g)

% Operatore di roto-traslazione con la sequenza di rotazioni:
% (alfa,z), (beta,y), (gamma,x)
% a=alfa, b=beta, g=gamma angoli in radianti.

sa=sin(a);
sb=sin(b);
sg=sin(g);
ca=cos(a);
cb=cos(b);
cg=cos(g);
R=[[ca*cb ca*sb*sg-sa*cg ca*sb*cg+sa*sg];...
   [sa*cb sa*sb*sg+ca*cg sa*sb*cg-ca*sg];...
   [-sb cb*sg cb*cg]];

end