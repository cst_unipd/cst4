%> @file  trasfg_vectors.m
%> @brief Apply a roto-translation operator to a group of vectors
%> @author Andrea Valmorbida (andrea.valmorbida@unipd.it)

%======================================================================
%> @brief Apply a roto-translation operator to a group of vectors
%>
%> @param Mvects 3xN matrix with the N vectors to be transformed
%> @param T roto-translation operator (4x4 matrix) to be applied to Mvects
%>
%> @retval Mvects_trasf transformed vectors (3xN)
%>
%======================================================================
function [Mvects_trasf] = trasfg_vectors(Mvects,T)

% transformed vectors (3xN)
Mvects_trasf = 0*Mvects;

for i=1:size(Mvects,2)
    [Xn,Yn,Zn] = trasfg(Mvects(1,i),Mvects(2,i),Mvects(3,i),T);
    Mvects_trasf(:,i) = [Xn,Yn,Zn]';
end

end