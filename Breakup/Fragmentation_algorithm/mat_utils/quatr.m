function q=quatr(RBA)

% Data la matrice di rotazione da B a A vengono determinati i 
% parametri di Eulero.
% Il segno di q0 � arbitrario, viene scelto >0, comunque in questa 
% funzione vi � anche il caso per q0>0 messo fra commenti per completezza
% di trattazione.

c=(trace(RBA)-1)/4;  % Costante

q0=sqrt(abs((trace(RBA)+1)/4));  % q0>0

%         Caso con q0>0
if q0>0
 q1=sqrt(abs(RBA(1,1)/2-c));
 q2=sqrt(abs(RBA(2,2)/2-c));
 q3=sqrt(abs(RBA(3,3)/2-c));
 if RBA(3,2)<RBA(2,3) q1=-q1; end
 if RBA(1,3)<RBA(3,1) q2=-q2; end
 if RBA(2,1)<RBA(1,2) q3=-q3; end
end

%         Caso con q0<0
%if q0<0
% q1=sqrt((abs(RBA(1,1)/2-c));
% q2=sqrt(abs(RBA(2,2)/2-c));
% q3=sqrt(abs(RBA(3,3)/2-c));
% if RBA(3,2)>RBA(2,3) q1=-q1; end
% if RBA(1,3)>RBA(3,1) q2=-q2; end
% if RBA(2,1)>RBA(1,2) q3=-q3; end
%end

%         Caso con q0=0
if q0==0
 q1=sqrt(abs((RBA(1,1)+1)/2));
 q2=sqrt(abs((RBA(2,2)+1)/2));
 q3=sqrt(abs((RBA(3,3)+1)/2));
end

q=[q0 q1 q2 q3]';


