%> @file  pltassi.m
%> @brief Plot a Reference Frame
%> @author Andrea Valmorbida (andrea.valmorbida@unipd.it)

%======================================================================
%> @brief Main function for plotting a generic reference frame (RF) defined
%> with respect to another reference frame.
%>
%> @param TT0 roto-translation matrix from RF T (transformed) to RF 0
%> @param color color of the RF (string)
%> @param nameaxes name of the RF (string)
%> @param names flag; if = 1 axes names are plotted, if = 0 axes names are
%> not plotted
%> @param mod axes length
%>
%>
%======================================================================
function [] = pltassi(TT0,color,nameaxes,names,mod)
% plot a generic reference frame (RF) defined wrt another reference frame
% TT0      : roto-translation matrix from RF T (transformed) to RF 0
% color    : string with the color of the RF
% nameaxes : string with the name of the RF
% names    : if = 1 axes names are plotted, if = 0 axes names are not plotted
% mod      : axes length

  plot3([TT0(1,4) TT0(1,4)+TT0(1,1)*mod],...
        [TT0(2,4) TT0(2,4)+TT0(2,1)*mod],...
        [TT0(3,4) TT0(3,4)+TT0(3,1)*mod],color);
% qui mettere hold on se usata da sola
  hold on
  plot3([TT0(1,4) TT0(1,4)+TT0(1,2)*mod],...
       [TT0(2,4) TT0(2,4)+TT0(2,2)*mod],...
       [TT0(3,4) TT0(3,4)+TT0(3,2)*mod],color);

  plot3([TT0(1,4) TT0(1,4)+TT0(1,3)*mod],...
        [TT0(2,4) TT0(2,4)+TT0(2,3)*mod],...
        [TT0(3,4) TT0(3,4)+TT0(3,3)*mod],color);
if names==1
   Pxlabel=TT0*[mod+mod/7 0 0 1]';
   Pylabel=TT0*[0 mod+mod/7 0 1]';
   Pzlabel=TT0*[0 0 mod+mod/7 1]';

  text(Pxlabel(1),Pxlabel(2),Pxlabel(3),['x ' nameaxes]);
  text(Pylabel(1),Pylabel(2),Pylabel(3),['y ' nameaxes]);
  text(Pzlabel(1),Pzlabel(2),Pzlabel(3),['z ' nameaxes]);
end

freccia(mod,6,TT0,color);

end