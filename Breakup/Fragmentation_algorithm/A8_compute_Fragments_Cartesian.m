function fragments_cart = A8_compute_Fragments_Cartesian(Frag_ME,Impact_Data,Frag_Domain,fragments_sph)
%A8_compute_Fragments_Cartesian Compute the fragments in cartesian coordinates
%
% Syntax:  fragments_cart = A8_compute_Fragments_Cartesian(Frag_ME,Impact_Data,Frag_Domain,fragments_sph)
%
% Inputs:
%    Frag_ME - Frag_ME structure
%    Impact_Data - Impact_Data structure
%    Frag_Domain - Frag_Domain structure
%    fragments_sph - fragments_sph structure output of Neper, spherical
%                    coordinates
%
% Outputs:
%    fragments_cart - fragments_cart structure, cartesian coordinates
%
% Other m-files required: cst_sph2cart_trasfg, cst_cyl2cart_trasfg,
%                         transfPointsSph2ElpHollow
% Subfunctions: none
% MAT-files required: unique
%
% See also:
%
% Author:   Andrea Valmorbida, Ph.D.
%           Center of Studies and Activities for Space "Giuseppe Colombo" (CISAS)
%           University of Padova
% Email address: andrea.valmorbida@unipd.it
% Date: 2017/11/24
% Revision: 1.0
% Copyright: 2017 CISAS - UNIVERSITY OF PADOVA
%
% HISTORY
% 2017/11/24 : first version by AV
% 2018/02/28 : voro++ case by AV

%#codegen

global SHAPE_ID_LIST;
global VORONOI_SOLVER_LIST;
global voronoi_solver_ID;

% ------------------------------------------
% Compute fragments in cartesian coordinates

% memory allocation
fragments_cart = fragments_sph;

if voronoi_solver_ID == VORONOI_SOLVER_LIST.VOROPP_MEX
    
    switch Frag_ME.shape_ID
        
        case SHAPE_ID_LIST.HOLLOW_SPHERE
            
            for k=1:Impact_Data.N_frag_dom
                
                for i=1:fragments_sph{k}.nseeds % ncells
                    %fragments_sph{k}.vorvx{i} = unique(fragments_sph{k}.vorvx{i},'rows');
                    if ~isempty(fragments_sph{k}.vorvx{i})
                        fragments_cart{k}.vorvx{i} = cst_sph2cart_trasfg(fragments_sph{k}.vorvx{i},Frag_Domain{k}.TSF,Frag_ME.a);
                        fragments_cart{k}.vorvx{i} = unique(fragments_cart{k}.vorvx{i},'rows');
                        fragments_cart{k}.vorfc{i} = minConvexHull(fragments_cart{k}.vorvx{i});
                        [fragments_cart{k}.CoM(i,:),fragments_cart{k}.volume(i)] = polyhedronCentroidVolume(fragments_cart{k}.vorvx{i});
                    end
                end
                
                fragments_cart{k}.seeds = cst_sph2cart_trasfg(fragments_sph{k}.seeds,Frag_Domain{k}.TSF,Frag_ME.a);
                
            end
            
        case SHAPE_ID_LIST.HOLLOW_CYLINDER
            
            for k=1:Impact_Data.N_frag_dom
                
                for i=1:fragments_sph{k}.nseeds % ncells
                    %fragments_sph{k}.vorvx{i} = unique(fragments_sph{k}.vorvx{i},'rows');
                    if ~isempty(fragments_sph{k}.vorvx{i})
                        fragments_cart{k}.vorvx{i} = cst_sph2cart_trasfg(fragments_sph{k}.vorvx{i},Frag_Domain{k}.TSF,Frag_ME.sphere_e(4),Frag_ME.center);
                        fragments_cart{k}.vorvx{i} = transfPointsSph2CylHollow(fragments_cart{k}.vorvx{i},Frag_ME.cylinder_e,Frag_ME.cylinder_i,Frag_ME.sphere_e,Frag_ME.sphere_i);
                        fragments_cart{k}.vorvx{i} = unique(fragments_cart{k}.vorvx{i},'rows');
                        fragments_cart{k}.vorfc{i} = minConvexHull(fragments_cart{k}.vorvx{i});
                        [fragments_cart{k}.CoM(i,:),fragments_cart{k}.volume(i)] = polyhedronCentroidVolume(fragments_cart{k}.vorvx{i});
                    end
                end
                
                fragments_cart{k}.seeds = cst_sph2cart_trasfg(fragments_sph{k}.seeds,Frag_Domain{k}.TSF,Frag_ME.sphere_e(4),Frag_ME.center);
                fragments_cart{k}.seeds = transfPointsSph2CylHollow(fragments_cart{k}.seeds,Frag_ME.cylinder_e,Frag_ME.cylinder_i,Frag_ME.sphere_e,Frag_ME.sphere_i);
                
            end
            
        case SHAPE_ID_LIST.HOLLOW_ELLIPSOID
            
            for k=1:Impact_Data.N_frag_dom
                
                for i=1:fragments_sph{k}.nseeds % ncells
                    %fragments_sph{k}.vorvx{i} = unique(fragments_sph{k}.vorvx{i},'rows');
                    if ~isempty(fragments_sph{k}.vorvx{i})
                        fragments_cart{k}.vorvx{i} = cst_sph2cart_trasfg(fragments_sph{k}.vorvx{i},Frag_Domain{k}.TSF,Frag_ME.sphere_e(4),Frag_ME.center);
                        fragments_cart{k}.vorvx{i} = transfPointsSph2ElpHollow(fragments_cart{k}.vorvx{i},Frag_ME.ellipsoid_e,Frag_ME.ellipsoid_i,Frag_ME.sphere_e,Frag_ME.sphere_i);
                        fragments_cart{k}.vorvx{i} = unique(fragments_cart{k}.vorvx{i},'rows');
                        fragments_cart{k}.vorfc{i} = minConvexHull(fragments_cart{k}.vorvx{i});
                        [fragments_cart{k}.CoM(i,:),fragments_cart{k}.volume(i)] = polyhedronCentroidVolume(fragments_cart{k}.vorvx{i});
                    end
                end
                
                fragments_cart{k}.seeds = cst_sph2cart_trasfg(fragments_sph{k}.seeds,Frag_Domain{k}.TSF,Frag_ME.sphere_e(4),Frag_ME.center);
                fragments_cart{k}.seeds = transfPointsSph2ElpHollow(fragments_cart{k}.seeds,Frag_ME.ellipsoid_e,Frag_ME.ellipsoid_i,Frag_ME.sphere_e,Frag_ME.sphere_i);
                
            end
            
    end
    
else % we are not using voro++ mex
    
    switch Frag_ME.shape_ID
        
        case SHAPE_ID_LIST.HOLLOW_SPHERE
            
            for k=1:Impact_Data.N_frag_dom
                
                if voronoi_solver_ID == VORONOI_SOLVER_LIST.NEPER_BASH
                    fragments_cart{k}.vertex = cst_sph2cart_trasfg(fragments_sph{k}.vertex,Frag_Domain{k}.TSF,Frag_ME.a);
                    
                    for i=1:fragments_sph{k}.nedge
                        fragments_cart{k}.edges{i} = cst_sph2cart_trasfg(fragments_sph{k}.edges{i},Frag_Domain{k}.TSF,Frag_ME.a);
                    end
                end
                
                for i=1:fragments_sph{k}.ncells
                    fragments_sph{k}.vorvx{i} = unique(fragments_sph{k}.vorvx{i},'rows');
                    fragments_cart{k}.vorvx{i} = cst_sph2cart_trasfg(fragments_sph{k}.vorvx{i},Frag_Domain{k}.TSF,Frag_ME.a);
                    fragments_cart{k}.vorvx{i} = unique(fragments_cart{k}.vorvx{i},'rows');
                end
                
                fragments_cart{k}.seeds = cst_sph2cart_trasfg(fragments_sph{k}.seeds,Frag_Domain{k}.TSF,Frag_ME.a);
                
            end
            
            %{
    case SHAPE_ID_LIST.HOLLOW_CYLINDER
    
        for k=1:Impact_Data.N_frag_dom

            fragments_cart{k}.vertex = cst_cyl2cart_trasfg(fragments_sph{k}.vertex,Frag_Domain{k}.TSF,Frag_ME.a,Frag_ME.c);

            for i=1:fragments_sph{k}.nedge
                fragments_cart{k}.edges{i} = cst_cyl2cart_trasfg(fragments_sph{k}.edges{i},Frag_Domain{k}.TSF,Frag_ME.a,Frag_ME.c);
            end

            for i=1:fragments_sph{k}.ncells
                fragments_sph{k}.vorvx{i} = unique(fragments_sph{k}.vorvx{i},'rows');
                fragments_cart{k}.vorvx{i} = cst_cyl2cart_trasfg(fragments_sph{k}.vorvx{i},Frag_Domain{k}.TSF,Frag_ME.a,Frag_ME.c);
                fragments_cart{k}.vorvx{i} = unique(fragments_cart{k}.vorvx{i},'rows');
            end

            fragments_cart{k}.seeds = cst_cyl2cart_trasfg(fragments_sph{k}.seeds,Frag_Domain{k}.TSF,Frag_ME.a,Frag_ME.c);

        end
            %}
            
        case SHAPE_ID_LIST.HOLLOW_ELLIPSOID
            
            for k=1:Impact_Data.N_frag_dom
                
                if voronoi_solver_ID == VORONOI_SOLVER_LIST.NEPER_BASH
                    fragments_cart{k}.vertex = cst_sph2cart_trasfg(fragments_sph{k}.vertex,Frag_Domain{k}.TSF,Frag_ME.sphere_e(4),Frag_ME.center);
                    fragments_cart{k}.vertex = transfPointsSph2ElpHollow(fragments_cart{k}.vertex,Frag_ME.ellipsoid_e,Frag_ME.ellipsoid_i,Frag_ME.sphere_e,Frag_ME.sphere_i);
                    
                    for i=1:fragments_sph{k}.nedge
                        fragments_cart{k}.edges{i} = cst_sph2cart_trasfg(fragments_sph{k}.edges{i},Frag_Domain{k}.TSF,Frag_ME.sphere_e(4),Frag_ME.center);
                        fragments_cart{k}.edges{i} = transfPointsSph2ElpHollow(fragments_cart{k}.edges{i},Frag_ME.ellipsoid_e,Frag_ME.ellipsoid_i,Frag_ME.sphere_e,Frag_ME.sphere_i);
                    end
                end
                
                for i=1:fragments_sph{k}.ncells
                    fragments_sph{k}.vorvx{i} = unique(fragments_sph{k}.vorvx{i},'rows');
                    fragments_cart{k}.vorvx{i} = cst_sph2cart_trasfg(fragments_sph{k}.vorvx{i},Frag_Domain{k}.TSF,Frag_ME.sphere_e(4),Frag_ME.center);
                    fragments_cart{k}.vorvx{i} = transfPointsSph2ElpHollow(fragments_cart{k}.vorvx{i},Frag_ME.ellipsoid_e,Frag_ME.ellipsoid_i,Frag_ME.sphere_e,Frag_ME.sphere_i);
                    fragments_cart{k}.vorvx{i} = unique(fragments_cart{k}.vorvx{i},'rows');
                end
                
                fragments_cart{k}.seeds = cst_sph2cart_trasfg(fragments_sph{k}.seeds,Frag_Domain{k}.TSF,Frag_ME.sphere_e(4),Frag_ME.center);
                fragments_cart{k}.seeds = transfPointsSph2ElpHollow(fragments_cart{k}.seeds,Frag_ME.ellipsoid_e,Frag_ME.ellipsoid_i,Frag_ME.sphere_e,Frag_ME.sphere_i);
                
            end
            
    end
    
end


end

