function ret = isHollowShape(shape_ID)

global SHAPE_ID_LIST;

if (shape_ID==SHAPE_ID_LIST.HOLLOW_SPHERE) || ...
   (shape_ID==SHAPE_ID_LIST.HOLLOW_CYLINDER) || ...
   (shape_ID==SHAPE_ID_LIST.HOLLOW_ELLIPSOID)
    ret = true;
else
    ret = false;
end

end