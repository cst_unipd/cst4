function [vert_cyl] = cst_cart2cyl(vert_cart,h,re)
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here

% allocate memory
vert_cyl = zeros(size(vert_cart,1),3);

% get X Y Y cartesian coordinates
X = vert_cart(:,1) - re;
Y = vert_cart(:,2) - re;
Z = vert_cart(:,3);

% compute r and theta
r = hypot(X,Y);
theta = atan2(Y,X);

% x
vert_cyl(:,1) = r.*theta + re*pi;

% y
vert_cyl(:,2) = h - Z;

% z
vert_cyl(:,3) = re - r;

end