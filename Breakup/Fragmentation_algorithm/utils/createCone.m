function cone = createCone(vertex,points)

cone.vertex = vertex;
cone.points = points;
cone.npnts = size(points,1);

cone.face_pnts = zeros(cone.npnts,3);
cone.face_vertex = cell(1,cone.npnts);

for i=1:cone.npnts
    
    if i==cone.npnts
        cone.face_pnts(i,:) = [0 cone.npnts 1];
    else
        cone.face_pnts(i,:) = [0 1+(i-1) 2+(i-1)];
    end
    
    for j=1:3
        idx = cone.face_pnts(i,j);
        if idx==0
            cone.face_vertex{i}(j,:) = cone.vertex;
        else
            cone.face_vertex{i}(j,:) = cone.points(idx,:);
        end
    end
    
end

end
