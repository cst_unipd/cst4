function [vert_cart] = cst_sph2cart(vert_sph,re,varargin)
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here

if ~isempty(varargin)
    center = varargin{1};
else
    center = re*ones(1,3);
end

% allocate memory
vert_cart = zeros(size(vert_sph,1),3);

vert_sph(:,1) = vert_sph(:,1) - re*pi;
vert_sph(:,2) = vert_sph(:,2) - re*pi/2;

% r phi and lambda
r = re - vert_sph(:,3);
phi = vert_sph(:,1)./r;
lam = vert_sph(:,2)./r;

% idx = find(lam==pi/2);
% phi(idx) = 0;
% idx = find(lam==-pi/2);
% phi(idx) = 0;

% X
vert_cart(:,1) = r.*cos(lam).*cos(phi) + center(1);

% Y
vert_cart(:,2) = r.*cos(lam).*sin(phi) + center(2);

% Z
vert_cart(:,3) = r.*sin(lam) + center(3);

end
