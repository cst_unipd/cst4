function vertex_cart = cst_cyl2cart_trasfg(vertex_sph,TSF,re,h)

vertex_cart = cst_cyl2cart(vertex_sph,h,re);
vertex_cart = trasfg_vectors((vertex_cart-[re re h/2])',TSF)';

end
