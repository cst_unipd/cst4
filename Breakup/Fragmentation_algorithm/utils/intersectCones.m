function ints_poits = intersectCones(cone1,cone2)

ints_poits = [];

for i=1:cone1.npnts % cone1.npnts
    ray = [cone1.vertex cone1.points(i,:)-cone1.vertex];
    for j=1:cone2.npnts % cone2.npnts => nface
        polygon3d = cone2.face_vertex{j};
        ip = intersectRayPolygon3d(ray, polygon3d);
        if sum(isnan(ip))==0
            ints_poits = [ints_poits;ip];
        end
    end
end
if size(ints_poits,1)>1
    ints_poits = uniquetol(ints_poits,1e-10,'ByRows',true);
end

%{
figure()
drawPolygon3d(polygon3d);
drawEdge3d(ray,'Color','r');
axis equal;
%}

end