function AR = getAspectRatio2D(CX,CY,xq,yq)

    nv = length(xq);
    dist_vect = zeros(nv,1);
    
    for i=1:nv
        dist_vect(i) = sqrt((CX-xq(i))^2+(CY-yq(i))^2);
    end
    
    AR = min(dist_vect)/max(dist_vect);

return