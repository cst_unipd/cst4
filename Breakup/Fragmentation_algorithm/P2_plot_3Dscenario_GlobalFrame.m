function P2_plot_3Dscenario_GlobalFrame(Frag_ME,Impact_Data)

    figure()
    set(gcf,'color','white');
    hold on; axis equal;
    view(30,40);

    % plot Global RF
    pltassi(trasm(eye(3),[0 0 0]'),'r','G',1,5);

    % plot ME RF
    pltassi(Frag_ME.TMG,'b','M',1,3);

    % plot Frag RF
    pltassi(Frag_ME.TFG,'g','F',1,2);

    % plot the impacted ME
    if isSolidShape(Frag_ME.shape_ID)
        Frag_ME_vG = trasfg_vectors(Frag_ME.v',Frag_ME.TFG)';
        drawMesh(Frag_ME_vG, Frag_ME.f, 'FaceColor',[0 0 0],'FaceAlpha',0.02);
    else
        Frag_ME_viG = trasfg_vectors(Frag_ME.vi_cart',Frag_ME.TFG)';
        Frag_ME_veG = trasfg_vectors(Frag_ME.ve_cart',Frag_ME.TFG)';
        drawMesh(Frag_ME_viG,Frag_ME.fi_cart, 'FaceColor',[0 0 0],'FaceAlpha',0.2,'EdgeAlpha',0.1);
        drawMesh(Frag_ME_veG,Frag_ME.fe_cart, 'FaceColor',[0 0 1],'FaceAlpha',0.05,'EdgeAlpha',0.1);
    end

    % plot Impact point
    drawPoint3d(Impact_Data.I_POS_G,'pm');

end
