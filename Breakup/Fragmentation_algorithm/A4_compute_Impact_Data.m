function [Impact_Data,impact_check_flag] = A4_compute_Impact_Data(COLLISION_DATA,Frag_ME)
%A4_compute_Impact_Data Compute the Impact_Data structure
% 
% Syntax:  Impact_Data = A4_compute_Impact_Data(COLLISION_DATA,Frag_ME)
%
% Inputs:
%    COLLISION_DATA - COLLISION_DATA(count)
%    Frag_ME - Frag_ME structure
%
% Outputs:
%    Impact_Data - Impact_Data structure
%
% Other m-files required: trasfg_vectors, createLine3d,
%                         intersectLineConvexHull, intersectLineBox,
%                         intersectLineSphere, intersectLineCylinder_ext, intersectLineEllipsoid
% Subfunctions: none
% MAT-files required: none
%
% See also:
%
% Author:   Andrea Valmorbida, Ph.D.
%           Center of Studies and Activities for Space "Giuseppe Colombo" (CISAS)
%           University of Padova
% Email address: andrea.valmorbida@unipd.it
% Date: 2017/11/24
% Revision: 1.0
% Copyright: 2017 CISAS - UNIVERSITY OF PADOVA
%
% HISTORY
% 2017/11/24 : first version by AV

%#codegen

global SHAPE_ID_LIST;
global cst_log;

% -----------------
% Number of impacts
Impact_Data.N_impact = length(COLLISION_DATA.IMPACTOR);

% ------------------
% Number of Frag Vol
if isSolidShape(Frag_ME.shape_ID)
    Impact_Data.N_frag_vol = Impact_Data.N_impact;
else
    Impact_Data.N_frag_vol = 2*Impact_Data.N_impact;
end

% ----------------------------------------------------
% Compute impact points and velocities in the three RF

I_POS_G = zeros(Impact_Data.N_impact,3);
I_VEL_G = zeros(Impact_Data.N_impact,3);

I_POS_F = zeros(Impact_Data.N_impact,3);
I_VEL_F = zeros(Impact_Data.N_impact,3);

% impact points from collision data;
% they may not be on the external surface of the impacted object 
I_PNT_F = zeros(Impact_Data.N_impact,3);

if Frag_ME.shape_ID==SHAPE_ID_LIST.HOLLOW_ELLIPSOID
    I_POS_CYL_G = zeros(Impact_Data.N_impact,3);
    I_VEL_CYL_G = zeros(Impact_Data.N_impact,3);

    I_POS_CYL_F = zeros(Impact_Data.N_impact,3);
    I_VEL_CYL_F = zeros(Impact_Data.N_impact,3);
end

% compute geometry_F
switch Frag_ME.shape_ID
    
    % BOX/PLATE
    case SHAPE_ID_LIST.SOLID_BOX 
        geometry_F = [0,0,0,Frag_ME.a,Frag_ME.b,Frag_ME.c]; % [x0 y0 z0 dx dy dz]
    
    % SPHERE
    case SHAPE_ID_LIST.SOLID_SPHERE 
        geometry_F = [Frag_ME.a Frag_ME.a Frag_ME.a Frag_ME.a]; % [xc yc zc r]
    
    % HOLLOW SPHERE
    case SHAPE_ID_LIST.HOLLOW_SPHERE 
        geometry_F = [Frag_ME.a Frag_ME.a Frag_ME.a Frag_ME.a]; % [xc yc zc r]
        
    % CYLINDER
    case SHAPE_ID_LIST.SOLID_CYLINDER 
        geometry_F = [Frag_ME.a Frag_ME.a 0 Frag_ME.a Frag_ME.a Frag_ME.c Frag_ME.a]; % [p1 p2]
        
    %
    % HOLLOW CYLINDER
    case SHAPE_ID_LIST.HOLLOW_CYLINDER 
        %geometry_F = [Frag_ME.a Frag_ME.a 0 Frag_ME.a Frag_ME.a Frag_ME.c Frag_ME.a]; % [p1 p2 re]
        geometry_F = Frag_ME.cylinder_e;
    %}
   
    %
    % HOLLOW ELLIPSOID
    case SHAPE_ID_LIST.HOLLOW_ELLIPSOID
        %geometry_F = [Frag_ME.a Frag_ME.b Frag_ME.c Frag_ME.a Frag_ME.b Frag_ME.c]; % [xc yc zc rx ry rz]
        geometry_F = Frag_ME.cylinder_e;
    %}
        
end

impact_check_flag = ones(1,Impact_Data.N_impact);

for i=1:Impact_Data.N_impact
    
    % get impact point and velocity in the Global RF from COLLISION_DATA
    % impact point from COLLISION_DATA could not be on the external surface
    % of the target object. For this reason, it is computed the
    % intersection between the trajectory of the impact point (given by the
    % velocity vector) and the external surface of the target object 
    I_pnt_G = COLLISION_DATA.IMPACTOR(i).POINT;
    I_vel_G = COLLISION_DATA.IMPACTOR(i).v_rel;
    
    % compute impact point and velocity in the Frag RF
    I_pnt_F = trasfg_vectors(I_pnt_G',Frag_ME.TGF)';
    I_vel_F = (Frag_ME.RGF*(I_vel_G))';
    
    % line of intersection
    line_F = createLine3d(I_pnt_F,I_vel_F(1)/norm(I_vel_F),I_vel_F(2)/norm(I_vel_F),I_vel_F(3)/norm(I_vel_F));    
    
    % compute the impact point on the external surface of the target object
    % intersection point
    switch Frag_ME.shape_ID
        
        % FRAGMENT
        case SHAPE_ID_LIST.FRAGMENT 
            itsc_pnt_F = intersectLineConvexHull(line_F, Frag_ME.v);
            if isempty(itsc_pnt_F)
%                 cst_log.warningMsg(['==>>>>> intersection point ' num2str(k) ' adjusted\n']);
                central_line = createLine3d(Frag_ME.center,I_pnt_F);
                itsc_pnt_F = intersectLineConvexHull(central_line, Frag_ME.v);
            end
        
        % BOX/PLATE
        case SHAPE_ID_LIST.SOLID_BOX 
            itsc_pnt_F = intersectLineBox(line_F, geometry_F);
            if isempty(itsc_pnt_F)
%                 cst_log.warningMsg(['==>>>>> intersection point ' num2str(k) ' adjusted\n']);
                central_line = createLine3d(Frag_ME.center,I_pnt_F);
                itsc_pnt_F = intersectLineBox(central_line, geometry_F);
            end
                
        % SPHERE
        case SHAPE_ID_LIST.SOLID_SPHERE 
            itsc_pnt_F = intersectLineSphere(line_F, geometry_F);
            if isempty(itsc_pnt_F)
%                 cst_log.warningMsg(['==>>>>> intersection point ' num2str(k) ' adjusted\n']);
                central_line = createLine3d(Frag_ME.center,I_pnt_F);
                itsc_pnt_F = intersectLineSphere(central_line, geometry_F);
            end
                
        % HOLLOW SPHERE
        case SHAPE_ID_LIST.HOLLOW_SPHERE 
            itsc_pnt_F = intersectLineSphere(line_F, geometry_F);
            if isempty(itsc_pnt_F)
%                 cst_log.warningMsg(['==>>>>> intersection point ' num2str(k) ' adjusted\n']);
                central_line = createLine3d(Frag_ME.center,I_pnt_F);
                itsc_pnt_F = intersectLineSphere(central_line, geometry_F);
            end
                
        % CYLINDER
        case SHAPE_ID_LIST.SOLID_CYLINDER 
            itsc_pnt_F = intersectLineCylinder_ext(line_F, geometry_F);
            if isempty(itsc_pnt_F)
%                 cst_log.warningMsg(['==>>>>> intersection point ' num2str(k) ' adjusted\n']);
                central_line = createLine3d(Frag_ME.center,I_pnt_F);
                itsc_pnt_F = intersectLineCylinder_ext(central_line, geometry_F);
            end
            if ~isPointWithinConvexHull(itsc_pnt_F,Frag_ME.v,Frag_ME.f,1e-10)
                central_line = createLine3d(Frag_ME.center,I_pnt_F);
                itsc_pnt_F = intersectLineConvexHull(central_line,Frag_ME.v);
            end
        
        %
        % HOLLOW CYLINDER
        case SHAPE_ID_LIST.HOLLOW_CYLINDER
            itsc_pnt_F = intersectLineCylinder_ext(line_F, geometry_F);
            if isempty(itsc_pnt_F)
%                 cst_log.warningMsg(['==>>>>> intersection point ' num2str(k) ' adjusted\n']);
                central_line = createLine3d(Frag_ME.center,I_pnt_F);
                itsc_pnt_F = intersectLineCylinder_ext(central_line, geometry_F);
            end
        %}
            
        % CONVEX HULL
        case SHAPE_ID_LIST.CONVEX_HULL 
            itsc_pnt_F = intersectLineConvexHull(line_F, Frag_ME.v);
            if isempty(itsc_pnt_F)
%                 cst_log.warningMsg(['==>>>>> intersection point ' num2str(k) ' adjusted\n']);
                central_line = createLine3d(Frag_ME.center,I_pnt_F);
                itsc_pnt_F = intersectLineConvexHull(central_line, Frag_ME.v);
            end
        
        %
        % HOLLOW ELLIPSOID
        case SHAPE_ID_LIST.HOLLOW_ELLIPSOID
            itsc_pnt_cyl_F = intersectLineCylinder_ext(line_F, geometry_F);
            if isempty(itsc_pnt_cyl_F)
%                 cst_log.warningMsg(['==>>>>> intersection point ' num2str(k) ' adjusted\n']);
                central_line = createLine3d(Frag_ME.center,I_pnt_F);
                itsc_pnt_cyl_F = intersectLineCylinder_ext(central_line, geometry_F);
            end
            if isempty(itsc_pnt_cyl_F)
                itsc_pnt_F = itsc_pnt_cyl_F;
            else
                % compute itsc_pnt_F and itsc_vel_F => ellipsoid
                p2c = itsc_pnt_cyl_F(end,:); p1c = itsc_pnt_cyl_F(1,:);
                elli_dome = [Frag_ME.ellipsoid_e, [0 0 0]];
                elli_domi = [Frag_ME.ellipsoid_i, [0 0 0]];
                p1e = transfPointsCyl2ElpHollow(p1c,Frag_ME.cylinder_e,Frag_ME.cylinder_i,elli_dome,elli_domi);
                p2e = transfPointsCyl2ElpHollow(p2c,Frag_ME.cylinder_e,Frag_ME.cylinder_i,elli_dome,elli_domi);
                vel_dir = (p2e-p1e); vel_dir = vel_dir/vectorNorm3d(vel_dir);
                itsc_pnt_F = [p1e;p2e];
                I_vel_cyl_F = vectorNorm3d(I_vel_F)*vel_dir;
            end
%             itsc_pnt_F = intersectLineEllipsoid(line_F,geometry_F);
%             if isempty(itsc_pnt_F)
%                 fprintf('intersection point adjusted\n');
%                 central_line = createLine3d(Frag_ME.center,I_pnt_F);
%                 itsc_pnt_F = intersectLineEllipsoid(central_line, geometry_F);
%                 if isempty(itsc_pnt_F)
%                     impact_check_flag(i) = 0;                    
%                     fprintf('==>>>>> no intersection point!\n');
%                 end
%             end
        %}
        
    end
   
    % CHECK IMPACT POINT
    if isempty(itsc_pnt_F)
        impact_check_flag(i) = 0;
        cst_log.warningMsg('==>>>>> no intersection point!');
    end
    
    % save impact points and velocities
    if impact_check_flag(i) == 1
        if Frag_ME.shape_ID == SHAPE_ID_LIST.HOLLOW_ELLIPSOID
            I_POS_F(i,:) = itsc_pnt_F(1,:);
            I_POS_G(i,:) = trasfg_vectors(I_POS_F(i,:)',Frag_ME.TFG)';
            I_POS_CYL_F(i,:) = itsc_pnt_cyl_F(1,:);
            I_POS_CYL_G(i,:) = trasfg_vectors(I_POS_CYL_F(i,:)',Frag_ME.TFG)';
            I_PNT_F(i,:) = I_pnt_F;
            I_VEL_F(i,:) = I_vel_cyl_F;
            I_VEL_G(i,:) = (Frag_ME.RFG*I_vel_cyl_F')';
            I_VEL_CYL_F(i,:) = I_vel_F;
            I_VEL_CYL_G(i,:) = I_vel_G;
        else
            I_POS_F(i,:) = itsc_pnt_F(1,:);
            I_POS_G(i,:) = trasfg_vectors(I_POS_F(i,:)',Frag_ME.TFG)';
            I_VEL_G(i,:) = I_vel_G;
            I_VEL_F(i,:) = I_vel_F;
            I_PNT_F(i,:) = I_pnt_F;
        end
    end

end


% save data in the Impact_Data structure
Impact_Data.I_POS_G = I_POS_G;
Impact_Data.I_VEL_G = I_VEL_G;
Impact_Data.I_POS_F = I_POS_F;
Impact_Data.I_VEL_F = I_VEL_F;
Impact_Data.I_PNT_F = I_PNT_F;

if Frag_ME.shape_ID == SHAPE_ID_LIST.HOLLOW_ELLIPSOID
    Impact_Data.I_POS_CYL_F = I_POS_CYL_F;
    Impact_Data.I_POS_CYL_G = I_POS_CYL_G;
    Impact_Data.I_VEL_CYL_F = I_VEL_CYL_F;
    Impact_Data.I_VEL_CYL_G = I_VEL_CYL_G;
end

end
