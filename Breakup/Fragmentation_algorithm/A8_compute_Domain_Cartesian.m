function domain_cart = A8_compute_Domain_Cartesian(Frag_ME,Impact_Data,Frag_Domain,domain_sph)
%A8_compute_Domain_Cartesian Compute the domain in cartesian coordinates
% 
% Syntax:  domain_cart = A8_compute_Domain_Cartesian(Frag_ME,Impact_Data,Frag_Domain,domain_sph)
%
% Inputs:
%    Frag_ME - Frag_ME structure
%    Impact_Data - Impact_Data structure
%    Frag_Domain - Frag_Domain structure
%    domain_sph - domain_sph structure output of Neper, spherical
%                 coordinates
%
% Outputs:
%    domain_cart - domain_cart structure, cartesian coordinates
%
% Other m-files required: cst_sph2cart_trasfg, cst_cyl2cart_trasfg,
%                         transfPointsSph2ElpHollow 
% Subfunctions: none
% MAT-files required: none
%
% See also:
%
% Author:   Andrea Valmorbida, Ph.D.
%           Center of Studies and Activities for Space "Giuseppe Colombo" (CISAS)
%           University of Padova
% Email address: andrea.valmorbida@unipd.it
% Date: 2017/11/24
% Revision: 1.0
% Copyright: 2017 CISAS - UNIVERSITY OF PADOVA
%
% HISTORY
% 2017/11/24 : first version by AV

%#codegen

global SHAPE_ID_LIST;
global VORONOI_SOLVER_LIST;
global voronoi_solver_ID;

% ------------------------------------------
% Compute domain in cartesian coordinates
% memory allocation
domain_cart = domain_sph;

if voronoi_solver_ID==VORONOI_SOLVER_LIST.NEPER_BASH
    switch Frag_ME.shape_ID
        case SHAPE_ID_LIST.HOLLOW_SPHERE
            for k=1:Impact_Data.N_frag_dom
                domain_cart{k}.vertex = cst_sph2cart_trasfg(domain_sph{k}.vertex,Frag_Domain{k}.TSF,Frag_ME.a);
                for i=1:domain_sph{k}.nedge
                    domain_cart{k}.edges{i} = cst_sph2cart_trasfg(domain_sph{k}.edges{i},Frag_Domain{k}.TSF,Frag_ME.a);
                end
            end
        %{    
        case SHAPE_ID_LIST.HOLLOW_CYLINDER
            for k=1:Impact_Data.N_frag_dom
                domain_cart{k}.vertex = cst_cyl2cart_trasfg(domain_sph{k}.vertex,Frag_Domain{k}.TSF,Frag_ME.a,Frag_ME.c);
                for i=1:domain_sph{k}.nedge
                    domain_cart{k}.edges{i} = cst_cyl2cart_trasfg(domain_sph{k}.edges{i},Frag_Domain{k}.TSF,Frag_ME.a,Frag_ME.c);
                end
            end
        %}    
        case SHAPE_ID_LIST.HOLLOW_ELLIPSOID
            for k=1:Impact_Data.N_frag_dom
                domain_cart{k}.vertex = cst_sph2cart_trasfg(domain_sph{k}.vertex,Frag_Domain{k}.TSF,Frag_ME.sphere_e(4),Frag_ME.center);
                domain_cart{k}.vertex = transfPointsSph2ElpHollow(domain_cart{k}.vertex,Frag_ME.ellipsoid_e,Frag_ME.ellipsoid_i,Frag_ME.sphere_e,Frag_ME.sphere_i);
                for i=1:domain_sph{k}.nedge
                    domain_cart{k}.edges{i} = cst_sph2cart_trasfg(domain_sph{k}.edges{i},Frag_Domain{k}.TSF,Frag_ME.sphere_e(4),Frag_ME.center);
                    domain_cart{k}.edges{i} = transfPointsSph2ElpHollow(domain_cart{k}.edges{i},Frag_ME.ellipsoid_e,Frag_ME.ellipsoid_i,Frag_ME.sphere_e,Frag_ME.sphere_i);
                end
            end
    end
end


end
