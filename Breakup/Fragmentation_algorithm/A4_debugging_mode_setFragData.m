% A4_debugging_mode_setFragData.m

if isSolidShape(shape_ID)
                                                            % SOLID SHAPES
    switch count
        case SHAPE_ID_LIST.SOLID_BOX % BOX / PLATE
            switch test_case
                case 1
                    Frag_Data(1,4) = 1.1;
                case 2
                    Frag_Data(1,4) = 0.6;
                    Frag_Data(2,4) = 0.5;
                    Frag_Data(3,4) = 0.4;
                    Frag_Data(4,4) = 0.6;
                case 3
                    Frag_Data(1,4) = 0.6;
                    Frag_Data(2,4) = 0.4;
            end
        case SHAPE_ID_LIST.SOLID_SPHERE % SOLID SPHERE
            switch test_case
                case 1
                    Frag_Data(1,4) = 0.8;
                case 2
                    Frag_Data(1,4) = 0.6;
                    Frag_Data(2,4) = 0.8;
                    Frag_Data(3,4) = 0.4;
                    Frag_Data(4,4) = 1.1;
            end
        case SHAPE_ID_LIST.SOLID_CYLINDER % SOLID CYLINDER
            switch test_case
                case 1
                    Frag_Data(1,4) = 1;
                case 2
                    Frag_Data(1,4) = 1;
                    Frag_Data(2,4) = 0.5;
                    Frag_Data(3,4) = 1.1;
                    Frag_Data(4,4) = 0.5;
            end
        case SHAPE_ID_LIST.CONVEX_HULL  % CONVEX HULL
            switch test_case
                case 1
                    Frag_Data(1,4) = 0.65;
                case 2
                    Frag_Data(1,4) = 0.6;
                    Frag_Data(2,4) = 0.5;
            end
        case 10 % FRAGMENT
            switch test_case
                case 1
                    Frag_Data(1,4) = 0.6;
                case 2
                    Frag_Data(1,4) = 0.6;
                    Frag_Data(2,4) = 0.5;
            end
    end
    
    for i=1:size(Frag_Data,1)
        if Frag_Data(i,11)==CRATER_SHAPE_LIST.ELLIPSOID
            Frag_Data(i,5) = Frag_Data(i,4);
            Frag_Data(i,6) = 1.1*Frag_Data(i,4);
        end
    end

else
                                                            % HOLLOW SHAPES
    switch shape_ID
        case SHAPE_ID_LIST.HOLLOW_SPHERE % HOLLOW SPHERE
            switch test_case
                case 1
                    Frag_Data(1,4) = 0.2*Frag_ME.b;
                case 2
                    Frag_Data(1,4) = 0.4*Frag_ME.b;
                    Frag_Data(2,4) = 0.3*Frag_ME.b;
                case 3
                    Frag_Data(1,4) = 0.52*Frag_ME.b;
                case 4
                    Frag_Data(1,4) = 0.52*Frag_ME.b;
                case 5
                    Frag_Data(1,4) = 2.5*Frag_ME.a;
                case 6
                    Frag_Data(1,4) = 2.5*Frag_ME.a;
                    Frag_Data(2,4) = 0.4*Frag_ME.b;
                case 9
                    Frag_Data(1,4) = 2.5*Frag_ME.a;
                    Frag_Data(2,4) = 0.4*Frag_ME.b;
                case 10
                    Frag_Data(1,4) = 2.5*Frag_ME.a;
                    Frag_Data(2,4) = 2.7*Frag_ME.b;
            end
        %
        case SHAPE_ID_LIST.HOLLOW_CYLINDER % HOLLOW CYLINDER
            switch test_case
                case 1
                    Frag_Data(1,4) = 0.3*Frag_ME.b;
                case 2
                    Frag_Data(1,4) = 0.3*Frag_ME.b;
                    Frag_Data(2,4) = 0.4*Frag_ME.b;
                case 3
                    Frag_Data(1,4) = 0.48*Frag_ME.b;
                case 4
                    Frag_Data(1,4) = 0.48*Frag_ME.b;
                case 5
                    Frag_Data(1,4) = 3.5*max([Frag_ME.b, Frag_ME.c/2]);
                case 6
                    error('todo');
                case 7
                    Frag_Data(1,4) = 0.3*Frag_ME.b;
                case 8
                    Frag_Data(1,4) = 0.52*Frag_ME.b;
                case 11
                    Frag_Data(1,4) = 3*max([Frag_ME.b, Frag_ME.c/2]);
            end
        %}
        case SHAPE_ID_LIST.HOLLOW_ELLIPSOID % HOLLOW ELLIPSOID
            switch test_case
                case 1
                    Frag_Data(1,4) = 0.21*(Frag_ME.a-Frag_ME.t);
                case 2
                    Frag_Data(1,4) = 0.3*(Frag_ME.a-Frag_ME.t);
                    Frag_Data(2,4) = 0.4*(Frag_ME.a-Frag_ME.t);
                case 3
                    Frag_Data(1,4) = 0.5*(Frag_ME.a-Frag_ME.t);
                case 4
                    Frag_Data(1,4) = 0.6*(Frag_ME.a-Frag_ME.t);
                case 5
                    Frag_Data(1,4) = 3.5*(Frag_ME.a-Frag_ME.t);
                case 6
                    Frag_Data(1,4) = 3.5*(Frag_ME.a-Frag_ME.t);
                    Frag_Data(2,4) = 0.35*(Frag_ME.a-Frag_ME.t);
                case 9
                    Frag_Data(1,4) = 3.5*(Frag_ME.a-Frag_ME.t);
                    Frag_Data(2,4) = 0.35*(Frag_ME.a-Frag_ME.t);
                case 10
                    Frag_Data(1,4) = 3.5*(Frag_ME.a-Frag_ME.t);
                    Frag_Data(2,4) = 3.35*(Frag_ME.a-Frag_ME.t);
            end
    end
end

