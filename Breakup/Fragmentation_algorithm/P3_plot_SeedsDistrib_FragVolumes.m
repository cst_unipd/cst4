function P3_plot_SeedsDistrib_FragVolumes(Frag_ME,Impact_Data,Frag_Volume)
%%
    for k=1:Impact_Data.N_frag_vol  % 1:2*N_impact
        
        figure()
        set(gcf,'color','white');
        hold on; axis equal;
        view(30,40);

        title(['Frag Volume #' num2str(k)]);
        
        % plot RFs
%         pltassi(trasm(eye(3),[0 0 0]'),'k','',1,3);
        %pltassi(trasm(eye(3),[Frag_ME.l1x/2 Frag_ME.l1y/2 0]'),'r','',1,2);

        % plot domain
        drawMesh(Frag_ME.v_sph, Frag_ME.f_sph, 'FaceColor',[0 0 0],'FaceAlpha',0.002);
        
        % plot Frag Volume        
        drawMesh(Frag_Volume{k}.v_sph, Frag_Volume{k}.f_sph, 'FaceColor', 'k','FaceAlpha',0.05);
        
        % plot seeds
        drawPoint3d(Frag_Volume{k}.seeds_sph,'.r');

    end

end
