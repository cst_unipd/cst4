function P1_plot_FragDomains(Frag_ME,Impact_Data,Frag_Volume,Frag_Domain)

    for k=1:Impact_Data.N_frag_dom  % 1:N_frag_dom
    
        figure()
        set(gcf,'color','white');
        hold on; axis equal; axis off
        view(30,40);

        title(['Frag Domain #' num2str(k)]);

        % plot RFs
        pltassi(trasm(eye(3),[0 0 0]'),'k','_S',1,2);
        %pltassi(trasm(eye(3),[Frag_ME.l1x/2 Frag_ME.l1y/2 0]'),'r','',1,2);

        % plot domain
        drawMesh(Frag_ME.v_sph, Frag_ME.f_sph, 'FaceColor',[0 0 0],'FaceAlpha',0.002);

        % plot fragmentation domain with normals
        drawMesh(Frag_Domain{k}.v_sph, Frag_Domain{k}.f_sph, 'FaceColor', 'r','FaceAlpha',0.05);
%         drawPoint3d(Frag_Domain{k}.v_sph,'or');
%         for i=1:size(Frag_Volume{k}.v_sph,1)
%             text(Frag_Volume{k}.v_sph(i,1)+0.1,Frag_Volume{k}.v_sph(i,2),Frag_Volume{k}.v_sph(i,3),...
%                  num2str(i),'color','k','FontSize',10);
%         end
        drawVector3d(Frag_Domain{k}.centros_sph, Frag_Domain{k}.normals_sph);
        
%         drawPoint3d(Frag_Domain{k}.centros_sph,'dm');
%         for i=1:size(Frag_Domain{k}.centros_sph,1)
%             text(Frag_Domain{k}.centros_sph(i,1)+0.1,Frag_Domain{k}.centros_sph(i,2),Frag_Domain{k}.centros_sph(i,3),...
%                  num2str(i),'color','k','FontSize',10);
%         end

        % plot Frag_Domain{k}.Volume{j}
        if ~isfield(Frag_Domain{k},'idx_complete_frag')
            Frag_Domain{k}.idx_complete_frag = [];
        end
        if (length(Impact_Data.intersect_groups{k})>1) && (length(Frag_Domain{k}.idx_complete_frag)<=1)
            for j=1:length(Frag_Domain{k}.Volume)
                %drawMesh(Frag_Domain{k}.Volume{j}.v_sph, Frag_Domain{k}.Volume{j}.f_sph, 'FaceColor', 'g','FaceAlpha',0.005);
                drawPoint3d(Frag_Domain{k}.Volume{j}.v_sph,'*');
            end
        end

    end

end
