function P4_plot_Fragmentation_Spherical_Hollow(Frag_ME,Impact_Data,Frag_Domain,frags_in_volume,domain_sph,fragments_sph)

%%
global VORONOI_SOLVER_LIST;
global voronoi_solver_ID;

    for k=1:Impact_Data.N_frag_dom % N_frag_dom

        figure()
        set(gcf,'Color','White');
        hold on; axis equal; axis off;
        view(0,90);
        
        title(['Frag Domain ' num2str(k)],'FontSize',15);
        
        % plot RFs
%         pltassi(trasm(eye(3),[0 0 0]'),'k','',1,3);
%         pltassi(trasm(eye(3),[Frag_ME.l1x/2 Frag_ME.l1y/2 0]'),'r','',1,2);

        % plot ME domain
%         drawMesh(Frag_ME.v_sph, Frag_ME.f_sph, 'FaceColor',[0 0 0],'FaceAlpha',0.002);

        if voronoi_solver_ID == VORONOI_SOLVER_LIST.NEPER_BASH
            % plot domain edges
            for i=1:domain_sph{k}.nedge
                % this plot the edges
                plot3(domain_sph{k}.edges{i}(:,1),domain_sph{k}.edges{i}(:,2),domain_sph{k}.edges{i}(:,3),...
                      'k','LineWidth',1);
            end

            % plot domain faces
            for i=1:domain_sph{k}.nface
                % this plot the faces
                fill3(domain_sph{k}.faces{i}(:,1),domain_sph{k}.faces{i}(:,2),domain_sph{k}.faces{i}(:,3), ...
                      'k','FaceAlpha',0.1);
            end

            % plot fragments edges
            %
            for i=1:fragments_sph{k}.nedge
                % this plot the edges
                plot3(fragments_sph{k}.edges{i}(:,1),fragments_sph{k}.edges{i}(:,2),fragments_sph{k}.edges{i}(:,3),...
                      'r','LineWidth',0.5);
            end
            %}
        else
            % plot ME domain
            drawMesh(Frag_ME.v_sph, Frag_ME.f_sph, 'FaceColor',[0 0 0],'FaceAlpha',0.002);
        end

        % plot fragments seeds and CoM
        %
        if length(Impact_Data.intersect_groups{k})==1
            i = Impact_Data.intersect_groups{k};
            for ii=1:frags_in_volume{i}.ncells   % 1:frags_in_volume{i}.ncells
%                 drawMesh(frags_in_volume{i}.vorvx_sph{ii}, frags_in_volume{i}.vorfc_sph{ii}, ...
%                          'EdgeColor',[1 0 0],'FaceColor',[1 0 0],'FaceAlpha',0.02);
                drawPoint3d(frags_in_volume{i}.seeds_sph(ii,:),'*k');
                drawPoint3d(frags_in_volume{i}.CoM_sph(ii,:),'*r');
            end
        else
            %
            for j = 1:length(Impact_Data.intersect_groups{k}) % 1:length(intersect_groups{k})
                i = Impact_Data.intersect_groups{k}(j);
                col = rand(1,3); col = col/norm(col);
                
                % plot fragments
                for ii=1:frags_in_volume{i}.ncells   % ii=1:frags_in_volume{i}.ncells
                    if frags_in_volume{i}.kill_flag(ii,1)==0
%                     drawMesh(frags_in_volume{i}.vorvx_sph{ii}, frags_in_volume{i}.vorfc_sph{ii}, ...
%                              'FaceColor',[0 0 0],'FaceAlpha',0.002);
%                     plot3(frags_in_volume{i}.seeds_sph(ii,1),frags_in_volume{i}.seeds_sph(ii,2),frags_in_volume{i}.seeds_sph(ii,3),...
%                           '*','Color',col);
                    plot3(frags_in_volume{i}.CoM_sph(ii,1),frags_in_volume{i}.CoM_sph(ii,2),frags_in_volume{i}.CoM_sph(ii,3),...
                          '*','Color',col);
                    end
                end
                
                % plot Frag Volume
                if ~isfield(Frag_Domain{k},'idx_complete_frag')
                    Frag_Domain{k}.idx_complete_frag = [];
                end
                if length(Frag_Domain{k}.idx_complete_frag)<=1
                drawMesh(Frag_Domain{k}.Volume{j}.v_sph, Frag_Domain{k}.Volume{j}.f_sph,...
                         'FaceColor', 'g','FaceAlpha',0.1);
                end
                
            end
            %
        end
        %}
    end

end

