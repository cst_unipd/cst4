function Frag_ME = A2_compute_Frag_ME(object_ID_index,target_type,shape_ID)
%A2_compute_Frag_ME Compute Frag_ME structure
% 
% Syntax:  Frag_ME = A2_compute_Frag_ME(object_ID_index,target_type,shape_ID)
%
% Inputs:
%    object_ID_index - object ID
%    target_type - target type
%    shape_ID - shape ID
%
% Outputs:
%    Frag_ME - Frag_ME structure
%
% Other m-files required: none
% Subfunctions: none
% MAT-files required: none
%
% See also:
%
% Author:   Andrea Valmorbida, Ph.D.
%           Center of Studies and Activities for Space "Giuseppe Colombo" (CISAS)
%           University of Padova
% Email address: andrea.valmorbida@unipd.it
% Date: 2017/11/24
% Revision: 1.0
% Copyright: 2017 CISAS - UNIVERSITY OF PADOVA
%
% HISTORY
% 2017/11/24 : first version by AV

%#codegen

global ME;
global FRAGMENTS;
global SHAPE_ID_LIST;

% Roto-Translation matrices for frames Global-ME
switch target_type
    case 0 % FRAGMENT
        quat = FRAGMENTS(object_ID_index).DYNAMICS_DATA.quaternions'; % -> CG trasposta: il campo quaternions ha size 4 1
         quat(1,2:4) = -quat(1,2:4);
        Frag_ME.RMG = rquat(quat);
        Frag_ME.TMG = trasm(Frag_ME.RMG,FRAGMENTS(object_ID_index).DYNAMICS_DATA.cm_coord);
    otherwise % ME
        quat = ME(object_ID_index).DYNAMICS_DATA.quaternions';% -> CG trasposta: il campo quaternions ha size 4 1
         quat(1,2:4) = -quat(1,2:4);
        Frag_ME.RMG = rquat(quat);
        Frag_ME.TMG = trasm(Frag_ME.RMG,ME(object_ID_index).DYNAMICS_DATA.cm_coord);
end

Frag_ME.RGM = Frag_ME.RMG';
Frag_ME.TGM = inv(Frag_ME.TMG);

% compute Frag_ME fields: a,b,c,lx,ly,lz; compute p_OF_M
switch shape_ID
    
    % FRAGMENT
    case SHAPE_ID_LIST.FRAGMENT
        c_hull_M = FRAGMENTS(object_ID_index).GEOMETRY_DATA.c_hull;
        Frag_ME.a = max(c_hull_M(:,1))-min(c_hull_M(:,1));
        Frag_ME.b = max(c_hull_M(:,2))-min(c_hull_M(:,2));
        Frag_ME.c = max(c_hull_M(:,3))-min(c_hull_M(:,3));
        Frag_ME.lx = Frag_ME.a;
        Frag_ME.ly = Frag_ME.b;
        Frag_ME.lz = Frag_ME.c;
        p_OF_M = [-Frag_ME.a/2 -Frag_ME.b/2 -Frag_ME.c/2];
    
    % SOLID BOX / PLATE
    case SHAPE_ID_LIST.SOLID_BOX  
        Frag_ME.a = ME(object_ID_index).GEOMETRY_DATA.dimensions(1);
        Frag_ME.b = ME(object_ID_index).GEOMETRY_DATA.dimensions(2);
        Frag_ME.c = ME(object_ID_index).GEOMETRY_DATA.dimensions(3);
        Frag_ME.lx = Frag_ME.a;
        Frag_ME.ly = Frag_ME.b;
        Frag_ME.lz = Frag_ME.c;
        p_OF_M = [-Frag_ME.a/2 -Frag_ME.b/2 -Frag_ME.c/2];
    
    % SOLID SPHERE
    case SHAPE_ID_LIST.SOLID_SPHERE  
        Frag_ME.a = ME(object_ID_index).GEOMETRY_DATA.dimensions(1); % radius
        Frag_ME.b = 0;
        Frag_ME.c = 0;
        Frag_ME.lx = 2*Frag_ME.a;
        Frag_ME.ly = 2*Frag_ME.a;
        Frag_ME.lz = 2*Frag_ME.a;
        p_OF_M = -Frag_ME.a*[1 1 1];
    
    % HOLLOW SPHERE
    case SHAPE_ID_LIST.HOLLOW_SPHERE
        Frag_ME.a = ME(object_ID_index).GEOMETRY_DATA.dimensions(1);  % external radius
        Frag_ME.c = ME(object_ID_index).GEOMETRY_DATA.thick;          % thickness
        Frag_ME.b = Frag_ME.a - Frag_ME.c;                      % internal radius: ri = re - thick
        Frag_ME.lx = 2*Frag_ME.a;
        Frag_ME.ly = 2*Frag_ME.a;
        Frag_ME.lz = 2*Frag_ME.a;
        p_OF_M = -Frag_ME.a*[1 1 1];
    
    % SOLID CYLINDER
    case SHAPE_ID_LIST.SOLID_CYLINDER  
        Frag_ME.a = ME(object_ID_index).GEOMETRY_DATA.dimensions(1); % radius
        Frag_ME.b = 0;
        Frag_ME.c = ME(object_ID_index).GEOMETRY_DATA.dimensions(3); % height
        Frag_ME.lx = 2*Frag_ME.a;
        Frag_ME.ly = 2*Frag_ME.a;
        Frag_ME.lz = Frag_ME.c;
        p_OF_M = [-Frag_ME.a -Frag_ME.a -Frag_ME.c/2];
    
    % HOLLOW CYLINDER
    %
    case SHAPE_ID_LIST.HOLLOW_CYLINDER  
        Frag_ME.a = ME(object_ID_index).GEOMETRY_DATA.dimensions(1); % external radius
        Frag_ME.b = Frag_ME.a - ME(object_ID_index).GEOMETRY_DATA.thick; % internal radius: ri = re - thick
        Frag_ME.c = ME(object_ID_index).GEOMETRY_DATA.dimensions(3); % height
        Frag_ME.lx = 2*Frag_ME.a;
        Frag_ME.ly = 2*Frag_ME.a;
        Frag_ME.lz = Frag_ME.c;
        p_OF_M = [-Frag_ME.a -Frag_ME.a -Frag_ME.c/2];
    %}
    
    % CONVEX HULL
    case SHAPE_ID_LIST.CONVEX_HULL  
        c_hull_M = ME(object_ID_index).GEOMETRY_DATA.c_hull;
        Frag_ME.a = max(c_hull_M(:,1))-min(c_hull_M(:,1));
        Frag_ME.b = max(c_hull_M(:,2))-min(c_hull_M(:,2));
        Frag_ME.c = max(c_hull_M(:,3))-min(c_hull_M(:,3));
        Frag_ME.lx = Frag_ME.a;
        Frag_ME.ly = Frag_ME.b;
        Frag_ME.lz = Frag_ME.c;
        p_OF_M = [-Frag_ME.a/2 -Frag_ME.b/2 -Frag_ME.c/2];
    
    % HOLLOW ELLIPSOID
    case SHAPE_ID_LIST.HOLLOW_ELLIPSOID  
        
%         Frag_ME.a = ME(object_ID_index).GEOMETRY_DATA.dimensions(1);  % external x semi-axis
%         Frag_ME.b = ME(object_ID_index).GEOMETRY_DATA.dimensions(2);  % external y semi-axis
%         Frag_ME.c = ME(object_ID_index).GEOMETRY_DATA.dimensions(3);  % external z semi-axis
%         Frag_ME.t = ME(object_ID_index).GEOMETRY_DATA.thick;          % thickness

        re = ME(object_ID_index).GEOMETRY_DATA.dimensions(1);
        h = ME(object_ID_index).GEOMETRY_DATA.dimensions(3);
        t = ME(object_ID_index).GEOMETRY_DATA.thick;
        ri = re - t;
        % solving this equation for rze:
        % vuolume of hollow cylinder = volume of hollow ellipsoid
        % pi*re^2*h - pi*ri^2*(h-2*t) = 4/3*pi*( re^2*rze - ri^2*(rze-t) )
        % you get:
        % rze = (3/4*re^2*h-3/4*ri^2*(h-2*t)-ri^2*t)/(re^2-ri^2);
        rze = h/2;
        
        Frag_ME.a = re;     % external x semi-axis
        Frag_ME.b = re;     % external y semi-axis
        Frag_ME.c = rze;    % external z semi-axis
        Frag_ME.t = t;      % thickness
        
        Frag_ME.lx = 2*Frag_ME.a;
        Frag_ME.ly = 2*Frag_ME.b;
        Frag_ME.lz = 2*Frag_ME.c;
        p_OF_M = [-Frag_ME.a -Frag_ME.b -Frag_ME.c];
    
    otherwise
        error('INVALID OBJECT TYPE');
end

% center in Frag RF
Frag_ME.center = -p_OF_M;

% Roto-Translation matrices for frames Frag-ME
Frag_ME.RFM = eye(3);
Frag_ME.TFM = trasm(Frag_ME.RFM,p_OF_M');

Frag_ME.RMF = Frag_ME.RFM';
Frag_ME.TMF = inv(Frag_ME.TFM);

% Roto-Translation matrices for frames Frag-G
Frag_ME.RFG = Frag_ME.RMG*Frag_ME.RFM;
Frag_ME.TFG = Frag_ME.TMG*Frag_ME.TFM;

Frag_ME.RGF = Frag_ME.RFG';
Frag_ME.TGF = inv(Frag_ME.TFG);

% set Frag_ME fields object_ID_index, target_type and shape_ID
Frag_ME.object_ID_index = object_ID_index;
Frag_ME.target_type = target_type;
Frag_ME.shape_ID = shape_ID;


end