%> @file   computeAlphaParam_j_ik.m
%> @brief  Compute the Alpha Parameter used in the velocity model
%> @author Andrea Valmorbida (andrea.valmorbida@unipd.it)

%==========================================================================
%> @brief Compute the Alpha Parameter used in the velocity model
%>
%> Compute the Alpha Parameter used in the velocity model
%>
%> @param shape_ID Shape ID
%> @param i i-th Frag. Volume
%> @param j j-th impact
%> @param frag_CoM Center of Mass of the k-th fragment
%> @param Frag_Data Frag_Data structure (see Fragmerntation Algorithm variable list)
%> @param Frag_ME Frag_ME structure (see Fragmerntation Algorithm variable list)
%>
%> @retval alpha_param_j_ik Alpha Parameter
%>
%==========================================================================
function alpha_param_j_ik = computeAlphaParam_j_ik(shape_ID,i,j,frag_CoM,Frag_Data,Frag_ME)

global SHAPE_ID_LIST;

% i => i-th frag volume
% j => j-th imapct

if (shape_ID==SHAPE_ID_LIST.HOLLOW_SPHERE) || ...
   (shape_ID==SHAPE_ID_LIST.HOLLOW_CYLINDER) || ...
   (shape_ID==SHAPE_ID_LIST.HOLLOW_ELLIPSOID)
    % HOLLOW SHAPES
    if (i==2*j)
        % forward Frag. Vol. associated to the j-th impact
        alpha_param_j_ik = 1;                           % <<<<==== TUNING
    elseif (i==(2*(j-1)+1))
        % backward Frag. Vol. associated to the j-th impact
        alpha_param_j_ik = 1;                           % <<<<==== TUNING
    else
        d = vectorNorm3d(frag_CoM - Frag_Data(j,1:3));
        d1 = Frag_Data(j,4);
        Kh = 1.3;                            	% <<<<==== TUNING PARAMETER
        d2 = Kh*d1;
        alpha_param_j_ik = windowFunction(d,d1,d2);
    end
else
    % SOLID SHAPES
    if i==j
        alpha_param_j_ik = 1;                           % <<<<==== TUNING
    else
        d = vectorNorm3d(frag_CoM - Frag_Data(j,1:3));
        d1 = Frag_Data(j,4);
        Ks = 1.3;                               % <<<<==== TUNING PARAMETER
        d2 = Ks*d1;
        alpha_param_j_ik = windowFunction(d,d1,d2);
    end
end


end

