%> @file   GenerateNeperBashFile.m
%> @brief  Generate Neper bash file
%> @author Andrea Valmorbida (andrea.valmorbida@unipd.it)

%==========================================================================
%> @brief Generate Neper bash file
%> 
%> Thsi function creates in the neper_folder a bash file with name
%> %<test_name>.sh
%>
%> @param neper_folder Path to the neper folder with the .tess file
%> @param test_name name of the test.
%> @param nseeds number of seeds in the domain
%> @param dim number of dimensionz of the domain (2D or 3D)
%> @param domain file name of the Neper domain
%>
%==========================================================================
function [ ] = GenerateNeperBashFile(neper_folder,test_name,nseeds,dim,domain)
%GenerateNeperBashFile

% neper file name
% if isunix
%     neper_file_name = [neper_folder '/' test_name '.sh'];
% else
%     neper_file_name = [neper_folder '\' test_name '.sh'];
% end

neper_file_name = [neper_folder , test_name '.sh'];


fileID = fopen(neper_file_name,'w');

if(isunix)
    fprintf(fileID,'#!/bin/bash\n');
    fprintf(fileID,'\n');
    fprintf(fileID,'NEPER="neper"\n');
    fprintf(fileID,'\n');

    % TV module cmd
    % nseeds = 20;
    % dim = 2;
    T_cmd_line = '$NEPER -T -n %d -dim %d -morphooptiini "coo:file(';
    seeds_file_name = [test_name '_seeds.msh'];
    T_cmd_line = [T_cmd_line seeds_file_name ')" -domain "' domain '" -o ' test_name '\n'];

else
    fprintf(fileID,'#!/bin/bash\n');
    fprintf(fileID,'\n');
    %fprintf(fileID,'NEPER="neper"\n');
    % fprintf(fileID,'NEPER="C:\\cygwin64\\usr\\local\\bin\\neper"\n');
    fprintf(fileID,'NEPER="/usr/local/bin/neper"\n');

    fprintf(fileID,'\n');

    % TV module cmd
    % nseeds = 20;
    % dim = 2;
    T_cmd_line = '$NEPER -T -n %d -dim %d -morphooptiini \"coo:file(';
    neper_folder_win=strrep(neper_folder,'\','/');
    seeds_file_name = [neper_folder_win,test_name '_seeds.msh'];
    domain_file_name = [neper_folder_win,test_name '_domain.msh'];
    domain_string=['planes(',domain_file_name,')'];
    T_cmd_line = [T_cmd_line seeds_file_name ') \" -domain \"' domain_string '\" -o ' [neper_folder_win, test_name] '\n'];
end
fprintf(fileID,T_cmd_line,nseeds,dim);
fprintf(fileID,'\n');

% V module cmd
%{
if V_mod_enabled==1
    cell_transparency = 0.5;
    img_size = 1000;
    C_line = 'C="-datacellcol id -datacelltrs ';
    C_line = [C_line num2str(cell_transparency) ' -cameraangle 8 -imagesize ' num2str(img_size) ':' num2str(img_size) '"\n'];
    % -cameraangle 8 -cameralookat 0.5:0.5:0.05
    fprintf(fileID,C_line);
    fprintf(fileID,'\n');
    V_cmd_line = ['$NEPER -V ' test_name '.tess $C -print ' test_name '\n'];
    fprintf(fileID,V_cmd_line);
    fprintf(fileID,'\n');
end
%}

% exit
fprintf(fileID,'exit 0\n');
fprintf(fileID,'\n');

fclose(fileID);

end

