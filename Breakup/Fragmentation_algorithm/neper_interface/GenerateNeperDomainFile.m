%> @file   GenerateNeperDomainFile.m
%> @brief  Generate Neper domain file
%> @author Andrea Valmorbida (andrea.valmorbida@unipd.it)

%==========================================================================
%> @brief Generate Neper domain file
%> 
%> Thsi function creates in the neper_folder a domain .msh file with name
%> %<test_name>_domain.msh
%>
%> @param neper_folder Path to the neper folder with the .tess file
%> @param test_name name of the test.
%> @param normals Nx3 matrix with versors normal to the N planes that
%> define the domain
%> @param points Nx3 matrix with a point belonging to each plane
%>
%==========================================================================
function [ ] = GenerateNeperDomainFile(neper_folder,test_name,normals,points)
%GenerateNeperDomainFile

% neper file name
if isunix
    neper_file_name = [neper_folder '/' test_name '_domain' '.msh'];
else
    neper_file_name = [neper_folder '\' test_name '_domain' '.msh'];
end
    
num_planes = size(normals,1);
dvect = zeros(num_planes,1);
for i=1:num_planes
    dvect(i) = normals(i,:)*(points(i,:)');
end

planes = [dvect,normals];
% planes = unique(planes,'rows');
planes = uniquetol(planes,1e-9,'ByRows',true);
num_planes = size(planes,1);

% 
fileID = fopen(neper_file_name,'w+');

fprintf(fileID,'%d\n',num_planes);

for i=1:num_planes
    fprintf(fileID,'%.12f\t%.12f\t%.12f\t%.12f\n',...
                    planes(i,1),planes(i,2),planes(i,3),planes(i,4));
                    %dvect(i),normals(i,1),normals(i,2),normals(i,3));
end


fclose(fileID);

end

