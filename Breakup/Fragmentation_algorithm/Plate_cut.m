%> @file   plate_cut.m
%> @brief  Cut of plate sides
%> @author Lorenzo Olivieri (lorenzo.olivieria@unipd.it)

%==========================================================================
%> @brief subroutine to cut the plate shape in case of lateral impacts
%>
%> For a detailed description of the algorithm see the CST Matlab document
%>
%> @param C    index of ME plate to be modified
%> @param P    impact point (single impact)
%> @param R    radius of the impact shape
%> @param a    plate size 1
%> @param b    plate size 2
%>
%> @retval V   Matrix with new plate verteces as columns
%>
%==========================================================================
function [V]=Plate_cut(C,P,R,a,b)

% INPUT: 
%   C = plate center
%   P = impact point
%   R = impact radius
%   [a,b] = plate sizes in x and y
% OUTPUT:
%  V = matrix of the new plate verteces

global plot_flag;

debug_mode=0;
switch debug_mode
case 1 % impoact @ right
C=[0 0  ]';
P=[1 0  ]';
R=1.3;
a=2.8;
b=1.2;
case 2 % impoact @ right, not aligned
C=[0 0  ]';
P=[1 0.4  ]';
R=1.3;
a=2.8;
b=1.2;
case 3 % impoact @ right, not aligned, 2
C=[0 0  ]';
P=[1 1,2  ];
R=1.3;
a=2.8;
b=1.2;
case 4 % impoact @ left
C=[0 0  ]';
P=[-1 0  ];
R=1.3;
a=2.8;
b=1.2;
case 5 % radius too small
C=[0 0  ]';
P=[-1 0  ];
R=0.4;
a=2.8;
b=1.2;
end


% Plate verteces
V1=C+[a/2 b/2  ]';
V2=C+[a/2 -b/2 ]';
V3=C+[-a/2 -b/2]';
V4=C+[-a/2 b/2 ]';
V=[V1 V2 V3 V4];
L=C-P;
% Defining if we need to cut
if R > b/2
     if (norm(L(1))+R) > a/2
%     if (norm(L(1))+R) < a/2 %-> CINZIA G.
        % Identifying the intersections
        % Pi is a matrix with 2 to 4 columns, each one an intersection point
        %Pi=intersection(a,b,R,P);  % TO BE DEFINED
        vx=[V(1,:) V(1,1)];
        vy=[V(2,:) V(2,1)];
        t=linspace(0,2*pi);
        cx=P(1)+R*cos(t);
        cy=P(2)+R*sin(t);
        [Pix,Piy]= polyxpoly (vx,vy,cx,cy);
        Pi=[Pix'; Piy'];
        Ri=zeros(1,length(Pi(1,:)));
        for i=1:length(Pi(1,:))
            Ri(i)=norm(Pi(:,i)-C); % distance intersections vs plate center
        end
        % intersection points: nearest to the center of the plate

        ii=find(Ri==min(Ri),1);
        P1=Pi(:,ii);
        Ri(ii)=[];
        Pi(:,ii)=[];
        ii=find(Ri==min(Ri),1);
        P2=Pi(:,ii);
        
        % Cut points
        Tx=P1(1)+ (P2(1)-P1(1))/2;
        T1=[Tx; 0];
        T2=[Tx; b];
        % defining which part of the plate I should save
            for i=1:4
                Ri(i)=norm(V(:,i)-P); % distance verteces vs impact point
            end
            % verteces farthest from impact point

            ii=find(Ri==max(Ri),1);
            V1=V(:,ii);
            Ri(ii)=[];
            V(:,ii)=[];
            ii=find(Ri==max(Ri),1);
            V2=V(:,ii);
            V=[V1 V2 T2 T1 V1];
            
            [Vx, Vy] = poly2cw(V(1,:), V(2,:)); % Reordering verteces
            V=[Vx(1:4);Vy(1:4)]; 
            
            if plot_flag == 1
                figure
                hold on
                subplot(2,1,1),plot(vx,vy,'b',cx,cy,'r',P(1),P(2),'k*',P1(1),P1(2),'r*',P2(1),P2(2),'r*');
                %xlim(1.2*[-a/2 a/2])
                %axis equal
                subplot(2,1,2),plot([Vx Vx(1)],[Vy Vy(1)],'b',cx,cy,'r',P1(1),P1(2),'k*',P2(1),P2(2),'k*');
                %xlim(1.2*[-a/2 a/2]);
                %axis equal
            end
    else
        if plot_flag == 1
            plot([V(1,:) V(1,1)],[V(2,:) V(2,1)],'b');
            display('no need to cut');
        end
    end
else
    if plot_flag == 1
        plot([V(1,:) V(1,1)],[V(2,:) V(2,1)],'b');
        display('no need to cut')
    end
end

