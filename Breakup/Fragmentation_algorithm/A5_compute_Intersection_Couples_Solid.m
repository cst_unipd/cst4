function Impact_Data = A5_compute_Intersection_Couples_Solid(Frag_ME,Impact_Data,Frag_Volume)
%A5_compute_Intersection_Couples_Solid Compute the intersection couples for solid
%shapes
% 
% Syntax:  Impact_Data = A5_compute_Intersection_Couples_Solid(Frag_ME,Impact_Data,Frag_Volume)
%
% Inputs:
%    Frag_ME - Frag_ME structure
%    Impact_Data - Impact_Data structure
%    Frag_Volume - Frag_Volume structure
%
% Outputs:
%    Impact_Data - Impact_Data structure updated with intersect_couples
%                  field
%
% Other m-files required: createSoccerBall_aug, getBoundariesIntersection,
%                         minConvexHull
% Subfunctions: none
% MAT-files required: none
%
% See also:
%
% Author:   Andrea Valmorbida, Ph.D.
%           Center of Studies and Activities for Space "Giuseppe Colombo" (CISAS)
%           University of Padova
% Email address: andrea.valmorbida@unipd.it
% Date: 2017/11/24
% Revision: 1.0
% Copyright: 2017 CISAS - UNIVERSITY OF PADOVA
%
% HISTORY
% 2017/11/24 : first version by AV

%#codegen

% --------------------------------
% compute the intersection couples
intersect_couples = cell(1,Impact_Data.N_frag_vol);

for i=1:Impact_Data.N_frag_vol
    for j=1:Impact_Data.N_frag_vol
        if j==i
            intersect_couples{i} = [intersect_couples{i},j];
        else
            check_points = Frag_Volume{j}.v;
            if (Frag_ME.shape_ID == 1)
                int_pnt_flag = 0;
                for k=1:size(check_points,1)
                    if (check_points(k,3) > 1e-3*Frag_ME.c) || ...
                       (check_points(k,3) < (1-1e-3)*Frag_ME.c)
                        int_pnt_flag = 1;
                    end
                end
                if int_pnt_flag == 0
                    check_points(:,3) = Frag_ME.c/2+0*check_points(:,3);
                end
            end
            
            % check and cancel NaN in in_vect
            in_vect = inhull(check_points,Frag_Volume{i}.v,[],1.e-13*mean(abs(Frag_Volume{i}.v(:))));
            %in_vect = [NaN 1 0 1 0 0 1];
            %in_vect = [NaN NaN NaN];
            in_vect_kill_idx = [];
            for k=1:length(in_vect)
                if isnan(in_vect(k))
                    in_vect_kill_idx = [in_vect_kill_idx k];
                end
            end
            in_vect(in_vect_kill_idx) = [];
            
            if ( sum(in_vect) >= 1 )
                intersect_couples{i} = [intersect_couples{i},j];
            end
        end
    end
end

Impact_Data.intersect_couples = intersect_couples;

end
