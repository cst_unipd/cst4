%> @file   windowFunction.m
%> @brief  Window Function used in the velocity model
%> @author Andrea Valmorbida (andrea.valmorbida@unipd.it)

%==========================================================================
%> @brief Window Function used in the velocity model
%>
%> Window Function used in the velocity model
%>
%> @param d Distance of the k-th fragment from the j-th impact point 
%> @param d1 d1 parameter
%> @param d2 d2 parameter
%>
%> @retval alpha Alpha parameter
%>
%==========================================================================
function alpha = windowFunction(d,d1,d2)
    if d<d1
        alpha = 1;
    elseif d>d2
        alpha = 0;
    else
        T = 2*(d2-d1);
        alpha = 0.5*(1+cos(2*pi*(d-d1)/T));
    end

end

%%
%{
xx = 0:0.1:10; 
d1 = 3;
d2 = 7;
alpha = zeros(length(xx),1);
for i=1:length(xx)
    alpha(i) = windowFunction(xx(i),d1,d2);
end
plot(xx,alpha,'+');
%}

